﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Kolibri2d.Poly2Tri.ITriangulatable
struct ITriangulatable_t1F37AD256C77B0E1CFDA7C785C2950EFE0D74AE7;
// Kolibri2d.Poly2Tri.Point2D
struct Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D;
// Kolibri2d.Poly2Tri.Rect2D
struct Rect2D_t337FF4CBB07E16CB6E029205BF01F1984DDE5353;
// Kolibri2d.Poly2Tri.TriangulationContext
struct TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B;
// Kolibri2d.Poly2Tri.TriangulationDebugContext
struct TriangulationDebugContext_t23D041A57AAB2224970A201625386E8BEB37D9A4;
// Mono.Xml.IHasXmlSchemaInfo
struct IHasXmlSchemaInfo_tA3B2C478D5F5102CB79A687B2F03ED13BC529461;
// Mono.Xml.Schema.XdtAnyAtomicType
struct XdtAnyAtomicType_t922A677221DBA603ACF1F4D2024F9222102416D7;
// Mono.Xml.Schema.XdtDayTimeDuration
struct XdtDayTimeDuration_t14B50E7DF624721215FA0E49D3672BAAFF59027F;
// Mono.Xml.Schema.XdtUntypedAtomic
struct XdtUntypedAtomic_t8E0FD4A48C677AA7C2D946690BE85E92CE81CDED;
// Mono.Xml.Schema.XdtYearMonthDuration
struct XdtYearMonthDuration_t940953B7258AE5DEC65A9F65D90EC70D9786EA68;
// Mono.Xml.Schema.XmlSchemaValidatingReader
struct XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08;
// Mono.Xml.Schema.XsdAnySimpleType
struct XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0;
// Mono.Xml.Schema.XsdAnyURI
struct XsdAnyURI_t049F3807506793286A90A0A279CADC8C39D4647A;
// Mono.Xml.Schema.XsdBase64Binary
struct XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F;
// Mono.Xml.Schema.XsdBoolean
struct XsdBoolean_tAA5FDC2ACA9F0A05A686F75BFE68CBB53A2F9959;
// Mono.Xml.Schema.XsdByte
struct XsdByte_t048838D9FF5BFD18C9FC056F11C699E7E08FC351;
// Mono.Xml.Schema.XsdDate
struct XsdDate_t5536E073BDA86CBF86DE829550F870C6D7F89964;
// Mono.Xml.Schema.XsdDateTime
struct XsdDateTime_tC31D6EF56B47537535EEC097FEBF63553C479E5D;
// Mono.Xml.Schema.XsdDecimal
struct XsdDecimal_t1CB6398C98F43AF142671E2E9BDAB05D58AEE9EE;
// Mono.Xml.Schema.XsdDouble
struct XsdDouble_tAF166F167258B898C01C97CE94720CA2EDB5E269;
// Mono.Xml.Schema.XsdDuration
struct XsdDuration_tA3253842BF902691A000BBFD87382247985D5E78;
// Mono.Xml.Schema.XsdEntities
struct XsdEntities_tE41F357D8F259BBCCB58DF1E99D66720E8F2CF1C;
// Mono.Xml.Schema.XsdEntity
struct XsdEntity_t426A9BE787702C55FF7FE5FE0FCDF582D5E50371;
// Mono.Xml.Schema.XsdFloat
struct XsdFloat_t02419495B090187B99AC33E40CD6601F7608D9B8;
// Mono.Xml.Schema.XsdGDay
struct XsdGDay_tEC936DF85B2878EB240E093D4B56BBDDC75A463B;
// Mono.Xml.Schema.XsdGMonth
struct XsdGMonth_t6E98934D7FAFFBCFA788D4068EB7DAE6706F4312;
// Mono.Xml.Schema.XsdGMonthDay
struct XsdGMonthDay_t25E4C24A7DF2B6F4528C0271813BF8AF052F5669;
// Mono.Xml.Schema.XsdGYear
struct XsdGYear_t3A3FECDF3BF0A8EDC0776CA5D478A2394CC894F2;
// Mono.Xml.Schema.XsdGYearMonth
struct XsdGYearMonth_t775E38939959BAF7304B4212BD8406456781C385;
// Mono.Xml.Schema.XsdHexBinary
struct XsdHexBinary_t542DD47AB586F148DAE0698D4FC956D0412C4229;
// Mono.Xml.Schema.XsdID
struct XsdID_tFC394CE8EE4F16A40F0063659AB8C788C712E77B;
// Mono.Xml.Schema.XsdIDManager
struct XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E;
// Mono.Xml.Schema.XsdIDRef
struct XsdIDRef_tC8B88E328BA8745548A89206641A6C113AE9F09B;
// Mono.Xml.Schema.XsdIDRefs
struct XsdIDRefs_t3598EB6F29A7BEC87962148242C3904C8FF5BB9F;
// Mono.Xml.Schema.XsdIdentityField
struct XsdIdentityField_t47467B5E2857C75A53026E207DC4063868D49DC7;
// Mono.Xml.Schema.XsdIdentityField[]
struct XsdIdentityFieldU5BU5D_tFC0450B63854F4164FF29197CBCB165727475092;
// Mono.Xml.Schema.XsdIdentityPath
struct XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE;
// Mono.Xml.Schema.XsdIdentityPath[]
struct XsdIdentityPathU5BU5D_tF8744EE69E53D965E12925A86B3E073CCA04DD2F;
// Mono.Xml.Schema.XsdIdentitySelector
struct XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8;
// Mono.Xml.Schema.XsdIdentityStep[]
struct XsdIdentityStepU5BU5D_t86F66436C951723412A544B608105218C570B61C;
// Mono.Xml.Schema.XsdInt
struct XsdInt_t6F6E692FF9941520F897A726ABDE415CCEFC984F;
// Mono.Xml.Schema.XsdInteger
struct XsdInteger_tE61DFB650D9B7BD3F6CA2C7DD4331667015CA348;
// Mono.Xml.Schema.XsdInvalidValidationState
struct XsdInvalidValidationState_t3532998F32C0C00F8E0F0C199D2308C867084FFF;
// Mono.Xml.Schema.XsdKeyEntry
struct XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3;
// Mono.Xml.Schema.XsdKeyEntryCollection
struct XsdKeyEntryCollection_tF37F133E46ECFF1F08CBB56894E1B18CB931B265;
// Mono.Xml.Schema.XsdKeyEntryFieldCollection
struct XsdKeyEntryFieldCollection_t8536E011BAA035EF7757EE7BBE9843160ABBF4EC;
// Mono.Xml.Schema.XsdKeyTable
struct XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6;
// Mono.Xml.Schema.XsdLanguage
struct XsdLanguage_t392B00FB85420B861413B7295BC68EA7C0982D85;
// Mono.Xml.Schema.XsdLong
struct XsdLong_t6FCDD94BFBBEC0874CD9166C160CF7B60D36B5FC;
// Mono.Xml.Schema.XsdNCName
struct XsdNCName_t7C66E8DC7486C5D92AF7739B71EC88C1FD11E2F7;
// Mono.Xml.Schema.XsdNMToken
struct XsdNMToken_tED82C02272B75D49D76B0FC44858D09D8F06AB91;
// Mono.Xml.Schema.XsdNMTokens
struct XsdNMTokens_tA3A587C5F1FF49445F75C961B0280F58A57D1095;
// Mono.Xml.Schema.XsdName
struct XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8;
// Mono.Xml.Schema.XsdNegativeInteger
struct XsdNegativeInteger_t5BF8A2EE42840B88A3C798FC6704B5AA9EDA715B;
// Mono.Xml.Schema.XsdNonNegativeInteger
struct XsdNonNegativeInteger_t0F4F93B0464B81E0BBF21CFC2F08343966734B30;
// Mono.Xml.Schema.XsdNonPositiveInteger
struct XsdNonPositiveInteger_t960302EB6DDE4ECE0961BC6BBE4FAFA3C3E5BE62;
// Mono.Xml.Schema.XsdNormalizedString
struct XsdNormalizedString_tEAAA641AA303B7A5227D53147AACC07A82EE13C0;
// Mono.Xml.Schema.XsdNotation
struct XsdNotation_t619CA8B98E6B358DAEE751C97934638D691DB5A3;
// Mono.Xml.Schema.XsdParticleStateManager
struct XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68;
// Mono.Xml.Schema.XsdPositiveInteger
struct XsdPositiveInteger_t8089829DDE14DBD3EEAD11DD4EBA8F4658FA04B2;
// Mono.Xml.Schema.XsdQName
struct XsdQName_t6E0BD13D8BFE16A59C49FAA8D331CA5FF5066DA0;
// Mono.Xml.Schema.XsdShort
struct XsdShort_tAF41351D06667202EC3FDBC76501A2D091518332;
// Mono.Xml.Schema.XsdString
struct XsdString_t604253BF4F01F3A649275313E7AED2D8BCB854AD;
// Mono.Xml.Schema.XsdTime
struct XsdTime_tDB0973DB314925C984AC13CB795FD91589AB462C;
// Mono.Xml.Schema.XsdToken
struct XsdToken_t4A120CAA60CECDF6DCDC15717ABCA63187B0D60C;
// Mono.Xml.Schema.XsdUnsignedByte
struct XsdUnsignedByte_tA702674830BFF7B5B4ADF325F30A781366A45256;
// Mono.Xml.Schema.XsdUnsignedInt
struct XsdUnsignedInt_t01075FD84457AE05E045BB23BD0E3A82AC30CE32;
// Mono.Xml.Schema.XsdUnsignedLong
struct XsdUnsignedLong_t7B886E29EBA85082057263BFE0B9F644FF4D59B9;
// Mono.Xml.Schema.XsdUnsignedShort
struct XsdUnsignedShort_tA832C283CB5937E31E78F3E4BBC85F90F92CD785;
// Mono.Xml.Schema.XsdValidationContext
struct XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD;
// Mono.Xml.Schema.XsdValidationState
struct XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74;
// System.Byte[]
struct ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8;
// System.Char[]
struct CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744;
// System.Collections.ArrayList
struct ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1;
// System.Collections.Generic.IList`1<Kolibri2d.Poly2Tri.Point2D>
struct IList_1_tB1C625055F059B0F197383D8F97283673C7B9E8F;
// System.Collections.Generic.List`1<Kolibri2d.Poly2Tri.DTSweepConstraint>
struct List_1_tC49DC5D4CE8A9D94F5703BC3CC0D1DCA924BD98D;
// System.Collections.Generic.List`1<Kolibri2d.Poly2Tri.DelaunayTriangle>
struct List_1_tA161C1D023FB6D4167CAE9ED2A7AB1554978B9D9;
// System.Collections.Generic.List`1<Kolibri2d.Poly2Tri.Point2D>
struct List_1_tFD3B51F91BAFEAD7A39B0EF917EC5B58423E32AE;
// System.Collections.Generic.List`1<Kolibri2d.Poly2Tri.TriangulationPoint>
struct List_1_tAAF0D3178D082F2377FDDDC38ED8BC5C83A1FF81;
// System.Collections.Hashtable
struct Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82;
// System.Collections.Specialized.StringCollection
struct StringCollection_t1921D1DDEF33942CB6DF73338C52ECD8C6ABC5E9;
// System.Collections.Stack
struct Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Uri/UriScheme[]
struct UriSchemeU5BU5D_tA21320F0E7A6C8B70FD19E286075856BFB5DB2E6;
// System.UriParser
struct UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A;
// System.Xml.IXmlLineInfo
struct IXmlLineInfo_t4DFB3B0D35B53EFBE9B6D120BEA11AD88DA2F3F3;
// System.Xml.IXmlNamespaceResolver
struct IXmlNamespaceResolver_tDB505F93FA770FCD1D11A52E37E473B9144318BD;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B;
// System.Xml.Schema.XmlSchemaAll
struct XmlSchemaAll_tD45D6713E3AF00578B100499CE5A37067F862EAB;
// System.Xml.Schema.XmlSchemaAny
struct XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF;
// System.Xml.Schema.XmlSchemaAttribute[]
struct XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95;
// System.Xml.Schema.XmlSchemaChoice
struct XmlSchemaChoice_t6872795EE02FB1510551BFC1F5657111504F33D5;
// System.Xml.Schema.XmlSchemaElement
struct XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64;
// System.Xml.Schema.XmlSchemaIdentityConstraint
struct XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9;
// System.Xml.Schema.XmlSchemaInfo
struct XmlSchemaInfo_t14DC1A0ED52BD1B9A2C0EC58D00D4C01D254795C;
// System.Xml.Schema.XmlSchemaObject
struct XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747;
// System.Xml.Schema.XmlSchemaSequence
struct XmlSchemaSequence_tC5A4D3A17AC43E34770AC810A3DDAFBC0D0C9483;
// System.Xml.Schema.XmlSchemaSet
struct XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D;
// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE;
// System.Xml.Schema.XmlSchemaValidator
struct XmlSchemaValidator_t35307305B4F219F9884E731DFE828451623423A2;
// System.Xml.Schema.XmlValueGetter
struct XmlValueGetter_tA66DE0B622718F2997C0BC6CA5967B1E5EC8DA22;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004;
// System.Xml.XmlReader
struct XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293;
// System.Xml.XmlReaderBinarySupport
struct XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB;
// System.Xml.XmlResolver
struct XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C;




#ifndef U3CMODULEU3E_T59A29D11DE086BC643857AFBADB05C659B348470_H
#define U3CMODULEU3E_T59A29D11DE086BC643857AFBADB05C659B348470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t59A29D11DE086BC643857AFBADB05C659B348470 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T59A29D11DE086BC643857AFBADB05C659B348470_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef EDGE_TFB6F8F74D888B19F8D03A97A9CA65E60889DC672_H
#define EDGE_TFB6F8F74D888B19F8D03A97A9CA65E60889DC672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.Edge
struct  Edge_tFB6F8F74D888B19F8D03A97A9CA65E60889DC672  : public RuntimeObject
{
public:
	// Kolibri2d.Poly2Tri.Point2D Kolibri2d.Poly2Tri.Edge::mP
	Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D * ___mP_0;
	// Kolibri2d.Poly2Tri.Point2D Kolibri2d.Poly2Tri.Edge::mQ
	Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D * ___mQ_1;

public:
	inline static int32_t get_offset_of_mP_0() { return static_cast<int32_t>(offsetof(Edge_tFB6F8F74D888B19F8D03A97A9CA65E60889DC672, ___mP_0)); }
	inline Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D * get_mP_0() const { return ___mP_0; }
	inline Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D ** get_address_of_mP_0() { return &___mP_0; }
	inline void set_mP_0(Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D * value)
	{
		___mP_0 = value;
		Il2CppCodeGenWriteBarrier((&___mP_0), value);
	}

	inline static int32_t get_offset_of_mQ_1() { return static_cast<int32_t>(offsetof(Edge_tFB6F8F74D888B19F8D03A97A9CA65E60889DC672, ___mQ_1)); }
	inline Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D * get_mQ_1() const { return ___mQ_1; }
	inline Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D ** get_address_of_mQ_1() { return &___mQ_1; }
	inline void set_mQ_1(Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D * value)
	{
		___mQ_1 = value;
		Il2CppCodeGenWriteBarrier((&___mQ_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDGE_TFB6F8F74D888B19F8D03A97A9CA65E60889DC672_H
#ifndef MATHUTIL_T489F8CC3DE74CF32CBFE2DEAD4FB8C1401C3A9B3_H
#define MATHUTIL_T489F8CC3DE74CF32CBFE2DEAD4FB8C1401C3A9B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.MathUtil
struct  MathUtil_t489F8CC3DE74CF32CBFE2DEAD4FB8C1401C3A9B3  : public RuntimeObject
{
public:

public:
};

struct MathUtil_t489F8CC3DE74CF32CBFE2DEAD4FB8C1401C3A9B3_StaticFields
{
public:
	// System.Double Kolibri2d.Poly2Tri.MathUtil::EPSILON
	double ___EPSILON_0;

public:
	inline static int32_t get_offset_of_EPSILON_0() { return static_cast<int32_t>(offsetof(MathUtil_t489F8CC3DE74CF32CBFE2DEAD4FB8C1401C3A9B3_StaticFields, ___EPSILON_0)); }
	inline double get_EPSILON_0() const { return ___EPSILON_0; }
	inline double* get_address_of_EPSILON_0() { return &___EPSILON_0; }
	inline void set_EPSILON_0(double value)
	{
		___EPSILON_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHUTIL_T489F8CC3DE74CF32CBFE2DEAD4FB8C1401C3A9B3_H
#ifndef POINT2D_T6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D_H
#define POINT2D_T6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.Point2D
struct  Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D  : public RuntimeObject
{
public:
	// System.Double Kolibri2d.Poly2Tri.Point2D::mX
	double ___mX_0;
	// System.Double Kolibri2d.Poly2Tri.Point2D::mY
	double ___mY_1;

public:
	inline static int32_t get_offset_of_mX_0() { return static_cast<int32_t>(offsetof(Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D, ___mX_0)); }
	inline double get_mX_0() const { return ___mX_0; }
	inline double* get_address_of_mX_0() { return &___mX_0; }
	inline void set_mX_0(double value)
	{
		___mX_0 = value;
	}

	inline static int32_t get_offset_of_mY_1() { return static_cast<int32_t>(offsetof(Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D, ___mY_1)); }
	inline double get_mY_1() const { return ___mY_1; }
	inline double* get_address_of_mY_1() { return &___mY_1; }
	inline void set_mY_1(double value)
	{
		___mY_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINT2D_T6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D_H
#ifndef POINT2DENUMERATOR_TA5536230BB56A3AE41E00C65BBCF4811679A9C15_H
#define POINT2DENUMERATOR_TA5536230BB56A3AE41E00C65BBCF4811679A9C15_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.Point2DEnumerator
struct  Point2DEnumerator_tA5536230BB56A3AE41E00C65BBCF4811679A9C15  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<Kolibri2d.Poly2Tri.Point2D> Kolibri2d.Poly2Tri.Point2DEnumerator::mPoints
	RuntimeObject* ___mPoints_0;
	// System.Int32 Kolibri2d.Poly2Tri.Point2DEnumerator::position
	int32_t ___position_1;

public:
	inline static int32_t get_offset_of_mPoints_0() { return static_cast<int32_t>(offsetof(Point2DEnumerator_tA5536230BB56A3AE41E00C65BBCF4811679A9C15, ___mPoints_0)); }
	inline RuntimeObject* get_mPoints_0() const { return ___mPoints_0; }
	inline RuntimeObject** get_address_of_mPoints_0() { return &___mPoints_0; }
	inline void set_mPoints_0(RuntimeObject* value)
	{
		___mPoints_0 = value;
		Il2CppCodeGenWriteBarrier((&___mPoints_0), value);
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(Point2DEnumerator_tA5536230BB56A3AE41E00C65BBCF4811679A9C15, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINT2DENUMERATOR_TA5536230BB56A3AE41E00C65BBCF4811679A9C15_H
#ifndef RECT2D_T337FF4CBB07E16CB6E029205BF01F1984DDE5353_H
#define RECT2D_T337FF4CBB07E16CB6E029205BF01F1984DDE5353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.Rect2D
struct  Rect2D_t337FF4CBB07E16CB6E029205BF01F1984DDE5353  : public RuntimeObject
{
public:
	// System.Double Kolibri2d.Poly2Tri.Rect2D::mMinX
	double ___mMinX_0;
	// System.Double Kolibri2d.Poly2Tri.Rect2D::mMaxX
	double ___mMaxX_1;
	// System.Double Kolibri2d.Poly2Tri.Rect2D::mMinY
	double ___mMinY_2;
	// System.Double Kolibri2d.Poly2Tri.Rect2D::mMaxY
	double ___mMaxY_3;

public:
	inline static int32_t get_offset_of_mMinX_0() { return static_cast<int32_t>(offsetof(Rect2D_t337FF4CBB07E16CB6E029205BF01F1984DDE5353, ___mMinX_0)); }
	inline double get_mMinX_0() const { return ___mMinX_0; }
	inline double* get_address_of_mMinX_0() { return &___mMinX_0; }
	inline void set_mMinX_0(double value)
	{
		___mMinX_0 = value;
	}

	inline static int32_t get_offset_of_mMaxX_1() { return static_cast<int32_t>(offsetof(Rect2D_t337FF4CBB07E16CB6E029205BF01F1984DDE5353, ___mMaxX_1)); }
	inline double get_mMaxX_1() const { return ___mMaxX_1; }
	inline double* get_address_of_mMaxX_1() { return &___mMaxX_1; }
	inline void set_mMaxX_1(double value)
	{
		___mMaxX_1 = value;
	}

	inline static int32_t get_offset_of_mMinY_2() { return static_cast<int32_t>(offsetof(Rect2D_t337FF4CBB07E16CB6E029205BF01F1984DDE5353, ___mMinY_2)); }
	inline double get_mMinY_2() const { return ___mMinY_2; }
	inline double* get_address_of_mMinY_2() { return &___mMinY_2; }
	inline void set_mMinY_2(double value)
	{
		___mMinY_2 = value;
	}

	inline static int32_t get_offset_of_mMaxY_3() { return static_cast<int32_t>(offsetof(Rect2D_t337FF4CBB07E16CB6E029205BF01F1984DDE5353, ___mMaxY_3)); }
	inline double get_mMaxY_3() const { return ___mMaxY_3; }
	inline double* get_address_of_mMaxY_3() { return &___mMaxY_3; }
	inline void set_mMaxY_3(double value)
	{
		___mMaxY_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT2D_T337FF4CBB07E16CB6E029205BF01F1984DDE5353_H
#ifndef TRIANGULATIONDEBUGCONTEXT_T23D041A57AAB2224970A201625386E8BEB37D9A4_H
#define TRIANGULATIONDEBUGCONTEXT_T23D041A57AAB2224970A201625386E8BEB37D9A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.TriangulationDebugContext
struct  TriangulationDebugContext_t23D041A57AAB2224970A201625386E8BEB37D9A4  : public RuntimeObject
{
public:
	// Kolibri2d.Poly2Tri.TriangulationContext Kolibri2d.Poly2Tri.TriangulationDebugContext::_tcx
	TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B * ____tcx_0;

public:
	inline static int32_t get_offset_of__tcx_0() { return static_cast<int32_t>(offsetof(TriangulationDebugContext_t23D041A57AAB2224970A201625386E8BEB37D9A4, ____tcx_0)); }
	inline TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B * get__tcx_0() const { return ____tcx_0; }
	inline TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B ** get_address_of__tcx_0() { return &____tcx_0; }
	inline void set__tcx_0(TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B * value)
	{
		____tcx_0 = value;
		Il2CppCodeGenWriteBarrier((&____tcx_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATIONDEBUGCONTEXT_T23D041A57AAB2224970A201625386E8BEB37D9A4_H
#ifndef TRIANGULATIONPOINTENUMERATOR_TC844E157B82CBD3122C315202426AB3BCDB471CE_H
#define TRIANGULATIONPOINTENUMERATOR_TC844E157B82CBD3122C315202426AB3BCDB471CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.TriangulationPointEnumerator
struct  TriangulationPointEnumerator_tC844E157B82CBD3122C315202426AB3BCDB471CE  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<Kolibri2d.Poly2Tri.Point2D> Kolibri2d.Poly2Tri.TriangulationPointEnumerator::mPoints
	RuntimeObject* ___mPoints_0;
	// System.Int32 Kolibri2d.Poly2Tri.TriangulationPointEnumerator::position
	int32_t ___position_1;

public:
	inline static int32_t get_offset_of_mPoints_0() { return static_cast<int32_t>(offsetof(TriangulationPointEnumerator_tC844E157B82CBD3122C315202426AB3BCDB471CE, ___mPoints_0)); }
	inline RuntimeObject* get_mPoints_0() const { return ___mPoints_0; }
	inline RuntimeObject** get_address_of_mPoints_0() { return &___mPoints_0; }
	inline void set_mPoints_0(RuntimeObject* value)
	{
		___mPoints_0 = value;
		Il2CppCodeGenWriteBarrier((&___mPoints_0), value);
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(TriangulationPointEnumerator_tC844E157B82CBD3122C315202426AB3BCDB471CE, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATIONPOINTENUMERATOR_TC844E157B82CBD3122C315202426AB3BCDB471CE_H
#ifndef TRIANGULATIONUTIL_TDA62C695D2152FE6541ADA0857DFB2258E0DA846_H
#define TRIANGULATIONUTIL_TDA62C695D2152FE6541ADA0857DFB2258E0DA846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.TriangulationUtil
struct  TriangulationUtil_tDA62C695D2152FE6541ADA0857DFB2258E0DA846  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATIONUTIL_TDA62C695D2152FE6541ADA0857DFB2258E0DA846_H
#ifndef U3CXMLSCHEMAVALIDATINGREADERU3EC__ANONSTOREY4_TD158F70A2443132E0678A7CA420F56DB9CC0DF75_H
#define U3CXMLSCHEMAVALIDATINGREADERU3EC__ANONSTOREY4_TD158F70A2443132E0678A7CA420F56DB9CC0DF75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XmlSchemaValidatingReader/<XmlSchemaValidatingReader>c__AnonStorey4
struct  U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_tD158F70A2443132E0678A7CA420F56DB9CC0DF75  : public RuntimeObject
{
public:
	// System.Xml.XmlReaderSettings Mono.Xml.Schema.XmlSchemaValidatingReader/<XmlSchemaValidatingReader>c__AnonStorey4::settings
	XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB * ___settings_0;
	// Mono.Xml.Schema.XmlSchemaValidatingReader Mono.Xml.Schema.XmlSchemaValidatingReader/<XmlSchemaValidatingReader>c__AnonStorey4::<>f__this
	XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_settings_0() { return static_cast<int32_t>(offsetof(U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_tD158F70A2443132E0678A7CA420F56DB9CC0DF75, ___settings_0)); }
	inline XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB * get_settings_0() const { return ___settings_0; }
	inline XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB ** get_address_of_settings_0() { return &___settings_0; }
	inline void set_settings_0(XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB * value)
	{
		___settings_0 = value;
		Il2CppCodeGenWriteBarrier((&___settings_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_tD158F70A2443132E0678A7CA420F56DB9CC0DF75, ___U3CU3Ef__this_1)); }
	inline XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CXMLSCHEMAVALIDATINGREADERU3EC__ANONSTOREY4_TD158F70A2443132E0678A7CA420F56DB9CC0DF75_H
#ifndef XSDIDMANAGER_T1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E_H
#define XSDIDMANAGER_T1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIDManager
struct  XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E  : public RuntimeObject
{
public:
	// System.Collections.Hashtable Mono.Xml.Schema.XsdIDManager::idList
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___idList_0;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdIDManager::missingIDReferences
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___missingIDReferences_1;
	// System.String Mono.Xml.Schema.XsdIDManager::thisElementId
	String_t* ___thisElementId_2;

public:
	inline static int32_t get_offset_of_idList_0() { return static_cast<int32_t>(offsetof(XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E, ___idList_0)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_idList_0() const { return ___idList_0; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_idList_0() { return &___idList_0; }
	inline void set_idList_0(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___idList_0 = value;
		Il2CppCodeGenWriteBarrier((&___idList_0), value);
	}

	inline static int32_t get_offset_of_missingIDReferences_1() { return static_cast<int32_t>(offsetof(XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E, ___missingIDReferences_1)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_missingIDReferences_1() const { return ___missingIDReferences_1; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_missingIDReferences_1() { return &___missingIDReferences_1; }
	inline void set_missingIDReferences_1(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___missingIDReferences_1 = value;
		Il2CppCodeGenWriteBarrier((&___missingIDReferences_1), value);
	}

	inline static int32_t get_offset_of_thisElementId_2() { return static_cast<int32_t>(offsetof(XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E, ___thisElementId_2)); }
	inline String_t* get_thisElementId_2() const { return ___thisElementId_2; }
	inline String_t** get_address_of_thisElementId_2() { return &___thisElementId_2; }
	inline void set_thisElementId_2(String_t* value)
	{
		___thisElementId_2 = value;
		Il2CppCodeGenWriteBarrier((&___thisElementId_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDMANAGER_T1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E_H
#ifndef XSDIDENTITYFIELD_T47467B5E2857C75A53026E207DC4063868D49DC7_H
#define XSDIDENTITYFIELD_T47467B5E2857C75A53026E207DC4063868D49DC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIdentityField
struct  XsdIdentityField_t47467B5E2857C75A53026E207DC4063868D49DC7  : public RuntimeObject
{
public:
	// Mono.Xml.Schema.XsdIdentityPath[] Mono.Xml.Schema.XsdIdentityField::fieldPaths
	XsdIdentityPathU5BU5D_tF8744EE69E53D965E12925A86B3E073CCA04DD2F* ___fieldPaths_0;
	// System.Int32 Mono.Xml.Schema.XsdIdentityField::index
	int32_t ___index_1;

public:
	inline static int32_t get_offset_of_fieldPaths_0() { return static_cast<int32_t>(offsetof(XsdIdentityField_t47467B5E2857C75A53026E207DC4063868D49DC7, ___fieldPaths_0)); }
	inline XsdIdentityPathU5BU5D_tF8744EE69E53D965E12925A86B3E073CCA04DD2F* get_fieldPaths_0() const { return ___fieldPaths_0; }
	inline XsdIdentityPathU5BU5D_tF8744EE69E53D965E12925A86B3E073CCA04DD2F** get_address_of_fieldPaths_0() { return &___fieldPaths_0; }
	inline void set_fieldPaths_0(XsdIdentityPathU5BU5D_tF8744EE69E53D965E12925A86B3E073CCA04DD2F* value)
	{
		___fieldPaths_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldPaths_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(XsdIdentityField_t47467B5E2857C75A53026E207DC4063868D49DC7, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDENTITYFIELD_T47467B5E2857C75A53026E207DC4063868D49DC7_H
#ifndef XSDIDENTITYPATH_TF37220F64B3F2E405306A9813980F5F02A788BFE_H
#define XSDIDENTITYPATH_TF37220F64B3F2E405306A9813980F5F02A788BFE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIdentityPath
struct  XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE  : public RuntimeObject
{
public:
	// Mono.Xml.Schema.XsdIdentityStep[] Mono.Xml.Schema.XsdIdentityPath::OrderedSteps
	XsdIdentityStepU5BU5D_t86F66436C951723412A544B608105218C570B61C* ___OrderedSteps_0;
	// System.Boolean Mono.Xml.Schema.XsdIdentityPath::Descendants
	bool ___Descendants_1;

public:
	inline static int32_t get_offset_of_OrderedSteps_0() { return static_cast<int32_t>(offsetof(XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE, ___OrderedSteps_0)); }
	inline XsdIdentityStepU5BU5D_t86F66436C951723412A544B608105218C570B61C* get_OrderedSteps_0() const { return ___OrderedSteps_0; }
	inline XsdIdentityStepU5BU5D_t86F66436C951723412A544B608105218C570B61C** get_address_of_OrderedSteps_0() { return &___OrderedSteps_0; }
	inline void set_OrderedSteps_0(XsdIdentityStepU5BU5D_t86F66436C951723412A544B608105218C570B61C* value)
	{
		___OrderedSteps_0 = value;
		Il2CppCodeGenWriteBarrier((&___OrderedSteps_0), value);
	}

	inline static int32_t get_offset_of_Descendants_1() { return static_cast<int32_t>(offsetof(XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE, ___Descendants_1)); }
	inline bool get_Descendants_1() const { return ___Descendants_1; }
	inline bool* get_address_of_Descendants_1() { return &___Descendants_1; }
	inline void set_Descendants_1(bool value)
	{
		___Descendants_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDENTITYPATH_TF37220F64B3F2E405306A9813980F5F02A788BFE_H
#ifndef XSDIDENTITYSELECTOR_T6ABC7388F5B0EECB1374E1B6137E2ACC506699D8_H
#define XSDIDENTITYSELECTOR_T6ABC7388F5B0EECB1374E1B6137E2ACC506699D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIdentitySelector
struct  XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8  : public RuntimeObject
{
public:
	// Mono.Xml.Schema.XsdIdentityPath[] Mono.Xml.Schema.XsdIdentitySelector::selectorPaths
	XsdIdentityPathU5BU5D_tF8744EE69E53D965E12925A86B3E073CCA04DD2F* ___selectorPaths_0;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdIdentitySelector::fields
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___fields_1;
	// Mono.Xml.Schema.XsdIdentityField[] Mono.Xml.Schema.XsdIdentitySelector::cachedFields
	XsdIdentityFieldU5BU5D_tFC0450B63854F4164FF29197CBCB165727475092* ___cachedFields_2;

public:
	inline static int32_t get_offset_of_selectorPaths_0() { return static_cast<int32_t>(offsetof(XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8, ___selectorPaths_0)); }
	inline XsdIdentityPathU5BU5D_tF8744EE69E53D965E12925A86B3E073CCA04DD2F* get_selectorPaths_0() const { return ___selectorPaths_0; }
	inline XsdIdentityPathU5BU5D_tF8744EE69E53D965E12925A86B3E073CCA04DD2F** get_address_of_selectorPaths_0() { return &___selectorPaths_0; }
	inline void set_selectorPaths_0(XsdIdentityPathU5BU5D_tF8744EE69E53D965E12925A86B3E073CCA04DD2F* value)
	{
		___selectorPaths_0 = value;
		Il2CppCodeGenWriteBarrier((&___selectorPaths_0), value);
	}

	inline static int32_t get_offset_of_fields_1() { return static_cast<int32_t>(offsetof(XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8, ___fields_1)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_fields_1() const { return ___fields_1; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_fields_1() { return &___fields_1; }
	inline void set_fields_1(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___fields_1 = value;
		Il2CppCodeGenWriteBarrier((&___fields_1), value);
	}

	inline static int32_t get_offset_of_cachedFields_2() { return static_cast<int32_t>(offsetof(XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8, ___cachedFields_2)); }
	inline XsdIdentityFieldU5BU5D_tFC0450B63854F4164FF29197CBCB165727475092* get_cachedFields_2() const { return ___cachedFields_2; }
	inline XsdIdentityFieldU5BU5D_tFC0450B63854F4164FF29197CBCB165727475092** get_address_of_cachedFields_2() { return &___cachedFields_2; }
	inline void set_cachedFields_2(XsdIdentityFieldU5BU5D_tFC0450B63854F4164FF29197CBCB165727475092* value)
	{
		___cachedFields_2 = value;
		Il2CppCodeGenWriteBarrier((&___cachedFields_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDENTITYSELECTOR_T6ABC7388F5B0EECB1374E1B6137E2ACC506699D8_H
#ifndef XSDIDENTITYSTEP_T870B49AFC712ECC1798E341EFD170C1B841ED48D_H
#define XSDIDENTITYSTEP_T870B49AFC712ECC1798E341EFD170C1B841ED48D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIdentityStep
struct  XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D  : public RuntimeObject
{
public:
	// System.Boolean Mono.Xml.Schema.XsdIdentityStep::IsCurrent
	bool ___IsCurrent_0;
	// System.Boolean Mono.Xml.Schema.XsdIdentityStep::IsAttribute
	bool ___IsAttribute_1;
	// System.Boolean Mono.Xml.Schema.XsdIdentityStep::IsAnyName
	bool ___IsAnyName_2;
	// System.String Mono.Xml.Schema.XsdIdentityStep::NsName
	String_t* ___NsName_3;
	// System.String Mono.Xml.Schema.XsdIdentityStep::Name
	String_t* ___Name_4;
	// System.String Mono.Xml.Schema.XsdIdentityStep::Namespace
	String_t* ___Namespace_5;

public:
	inline static int32_t get_offset_of_IsCurrent_0() { return static_cast<int32_t>(offsetof(XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D, ___IsCurrent_0)); }
	inline bool get_IsCurrent_0() const { return ___IsCurrent_0; }
	inline bool* get_address_of_IsCurrent_0() { return &___IsCurrent_0; }
	inline void set_IsCurrent_0(bool value)
	{
		___IsCurrent_0 = value;
	}

	inline static int32_t get_offset_of_IsAttribute_1() { return static_cast<int32_t>(offsetof(XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D, ___IsAttribute_1)); }
	inline bool get_IsAttribute_1() const { return ___IsAttribute_1; }
	inline bool* get_address_of_IsAttribute_1() { return &___IsAttribute_1; }
	inline void set_IsAttribute_1(bool value)
	{
		___IsAttribute_1 = value;
	}

	inline static int32_t get_offset_of_IsAnyName_2() { return static_cast<int32_t>(offsetof(XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D, ___IsAnyName_2)); }
	inline bool get_IsAnyName_2() const { return ___IsAnyName_2; }
	inline bool* get_address_of_IsAnyName_2() { return &___IsAnyName_2; }
	inline void set_IsAnyName_2(bool value)
	{
		___IsAnyName_2 = value;
	}

	inline static int32_t get_offset_of_NsName_3() { return static_cast<int32_t>(offsetof(XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D, ___NsName_3)); }
	inline String_t* get_NsName_3() const { return ___NsName_3; }
	inline String_t** get_address_of_NsName_3() { return &___NsName_3; }
	inline void set_NsName_3(String_t* value)
	{
		___NsName_3 = value;
		Il2CppCodeGenWriteBarrier((&___NsName_3), value);
	}

	inline static int32_t get_offset_of_Name_4() { return static_cast<int32_t>(offsetof(XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D, ___Name_4)); }
	inline String_t* get_Name_4() const { return ___Name_4; }
	inline String_t** get_address_of_Name_4() { return &___Name_4; }
	inline void set_Name_4(String_t* value)
	{
		___Name_4 = value;
		Il2CppCodeGenWriteBarrier((&___Name_4), value);
	}

	inline static int32_t get_offset_of_Namespace_5() { return static_cast<int32_t>(offsetof(XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D, ___Namespace_5)); }
	inline String_t* get_Namespace_5() const { return ___Namespace_5; }
	inline String_t** get_address_of_Namespace_5() { return &___Namespace_5; }
	inline void set_Namespace_5(String_t* value)
	{
		___Namespace_5 = value;
		Il2CppCodeGenWriteBarrier((&___Namespace_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDENTITYSTEP_T870B49AFC712ECC1798E341EFD170C1B841ED48D_H
#ifndef XSDKEYENTRY_T1A916C808B855D066198566E0DD1D93B3C8722B3_H
#define XSDKEYENTRY_T1A916C808B855D066198566E0DD1D93B3C8722B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdKeyEntry
struct  XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3  : public RuntimeObject
{
public:
	// System.Int32 Mono.Xml.Schema.XsdKeyEntry::StartDepth
	int32_t ___StartDepth_0;
	// System.Int32 Mono.Xml.Schema.XsdKeyEntry::SelectorLineNumber
	int32_t ___SelectorLineNumber_1;
	// System.Int32 Mono.Xml.Schema.XsdKeyEntry::SelectorLinePosition
	int32_t ___SelectorLinePosition_2;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntry::SelectorHasLineInfo
	bool ___SelectorHasLineInfo_3;
	// Mono.Xml.Schema.XsdKeyEntryFieldCollection Mono.Xml.Schema.XsdKeyEntry::KeyFields
	XsdKeyEntryFieldCollection_t8536E011BAA035EF7757EE7BBE9843160ABBF4EC * ___KeyFields_4;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntry::KeyRefFound
	bool ___KeyRefFound_5;
	// Mono.Xml.Schema.XsdKeyTable Mono.Xml.Schema.XsdKeyEntry::OwnerSequence
	XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6 * ___OwnerSequence_6;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntry::keyFound
	bool ___keyFound_7;

public:
	inline static int32_t get_offset_of_StartDepth_0() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3, ___StartDepth_0)); }
	inline int32_t get_StartDepth_0() const { return ___StartDepth_0; }
	inline int32_t* get_address_of_StartDepth_0() { return &___StartDepth_0; }
	inline void set_StartDepth_0(int32_t value)
	{
		___StartDepth_0 = value;
	}

	inline static int32_t get_offset_of_SelectorLineNumber_1() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3, ___SelectorLineNumber_1)); }
	inline int32_t get_SelectorLineNumber_1() const { return ___SelectorLineNumber_1; }
	inline int32_t* get_address_of_SelectorLineNumber_1() { return &___SelectorLineNumber_1; }
	inline void set_SelectorLineNumber_1(int32_t value)
	{
		___SelectorLineNumber_1 = value;
	}

	inline static int32_t get_offset_of_SelectorLinePosition_2() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3, ___SelectorLinePosition_2)); }
	inline int32_t get_SelectorLinePosition_2() const { return ___SelectorLinePosition_2; }
	inline int32_t* get_address_of_SelectorLinePosition_2() { return &___SelectorLinePosition_2; }
	inline void set_SelectorLinePosition_2(int32_t value)
	{
		___SelectorLinePosition_2 = value;
	}

	inline static int32_t get_offset_of_SelectorHasLineInfo_3() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3, ___SelectorHasLineInfo_3)); }
	inline bool get_SelectorHasLineInfo_3() const { return ___SelectorHasLineInfo_3; }
	inline bool* get_address_of_SelectorHasLineInfo_3() { return &___SelectorHasLineInfo_3; }
	inline void set_SelectorHasLineInfo_3(bool value)
	{
		___SelectorHasLineInfo_3 = value;
	}

	inline static int32_t get_offset_of_KeyFields_4() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3, ___KeyFields_4)); }
	inline XsdKeyEntryFieldCollection_t8536E011BAA035EF7757EE7BBE9843160ABBF4EC * get_KeyFields_4() const { return ___KeyFields_4; }
	inline XsdKeyEntryFieldCollection_t8536E011BAA035EF7757EE7BBE9843160ABBF4EC ** get_address_of_KeyFields_4() { return &___KeyFields_4; }
	inline void set_KeyFields_4(XsdKeyEntryFieldCollection_t8536E011BAA035EF7757EE7BBE9843160ABBF4EC * value)
	{
		___KeyFields_4 = value;
		Il2CppCodeGenWriteBarrier((&___KeyFields_4), value);
	}

	inline static int32_t get_offset_of_KeyRefFound_5() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3, ___KeyRefFound_5)); }
	inline bool get_KeyRefFound_5() const { return ___KeyRefFound_5; }
	inline bool* get_address_of_KeyRefFound_5() { return &___KeyRefFound_5; }
	inline void set_KeyRefFound_5(bool value)
	{
		___KeyRefFound_5 = value;
	}

	inline static int32_t get_offset_of_OwnerSequence_6() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3, ___OwnerSequence_6)); }
	inline XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6 * get_OwnerSequence_6() const { return ___OwnerSequence_6; }
	inline XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6 ** get_address_of_OwnerSequence_6() { return &___OwnerSequence_6; }
	inline void set_OwnerSequence_6(XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6 * value)
	{
		___OwnerSequence_6 = value;
		Il2CppCodeGenWriteBarrier((&___OwnerSequence_6), value);
	}

	inline static int32_t get_offset_of_keyFound_7() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3, ___keyFound_7)); }
	inline bool get_keyFound_7() const { return ___keyFound_7; }
	inline bool* get_address_of_keyFound_7() { return &___keyFound_7; }
	inline void set_keyFound_7(bool value)
	{
		___keyFound_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDKEYENTRY_T1A916C808B855D066198566E0DD1D93B3C8722B3_H
#ifndef XSDKEYENTRYFIELD_T46A4E7087BCB9D206208679EA739175225D551D2_H
#define XSDKEYENTRYFIELD_T46A4E7087BCB9D206208679EA739175225D551D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdKeyEntryField
struct  XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2  : public RuntimeObject
{
public:
	// Mono.Xml.Schema.XsdKeyEntry Mono.Xml.Schema.XsdKeyEntryField::entry
	XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3 * ___entry_0;
	// Mono.Xml.Schema.XsdIdentityField Mono.Xml.Schema.XsdKeyEntryField::field
	XsdIdentityField_t47467B5E2857C75A53026E207DC4063868D49DC7 * ___field_1;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntryField::FieldFound
	bool ___FieldFound_2;
	// System.Int32 Mono.Xml.Schema.XsdKeyEntryField::FieldLineNumber
	int32_t ___FieldLineNumber_3;
	// System.Int32 Mono.Xml.Schema.XsdKeyEntryField::FieldLinePosition
	int32_t ___FieldLinePosition_4;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntryField::FieldHasLineInfo
	bool ___FieldHasLineInfo_5;
	// Mono.Xml.Schema.XsdAnySimpleType Mono.Xml.Schema.XsdKeyEntryField::FieldType
	XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0 * ___FieldType_6;
	// System.Object Mono.Xml.Schema.XsdKeyEntryField::Identity
	RuntimeObject * ___Identity_7;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntryField::IsXsiNil
	bool ___IsXsiNil_8;
	// System.Int32 Mono.Xml.Schema.XsdKeyEntryField::FieldFoundDepth
	int32_t ___FieldFoundDepth_9;
	// Mono.Xml.Schema.XsdIdentityPath Mono.Xml.Schema.XsdKeyEntryField::FieldFoundPath
	XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE * ___FieldFoundPath_10;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntryField::Consuming
	bool ___Consuming_11;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntryField::Consumed
	bool ___Consumed_12;

public:
	inline static int32_t get_offset_of_entry_0() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___entry_0)); }
	inline XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3 * get_entry_0() const { return ___entry_0; }
	inline XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3 ** get_address_of_entry_0() { return &___entry_0; }
	inline void set_entry_0(XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3 * value)
	{
		___entry_0 = value;
		Il2CppCodeGenWriteBarrier((&___entry_0), value);
	}

	inline static int32_t get_offset_of_field_1() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___field_1)); }
	inline XsdIdentityField_t47467B5E2857C75A53026E207DC4063868D49DC7 * get_field_1() const { return ___field_1; }
	inline XsdIdentityField_t47467B5E2857C75A53026E207DC4063868D49DC7 ** get_address_of_field_1() { return &___field_1; }
	inline void set_field_1(XsdIdentityField_t47467B5E2857C75A53026E207DC4063868D49DC7 * value)
	{
		___field_1 = value;
		Il2CppCodeGenWriteBarrier((&___field_1), value);
	}

	inline static int32_t get_offset_of_FieldFound_2() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___FieldFound_2)); }
	inline bool get_FieldFound_2() const { return ___FieldFound_2; }
	inline bool* get_address_of_FieldFound_2() { return &___FieldFound_2; }
	inline void set_FieldFound_2(bool value)
	{
		___FieldFound_2 = value;
	}

	inline static int32_t get_offset_of_FieldLineNumber_3() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___FieldLineNumber_3)); }
	inline int32_t get_FieldLineNumber_3() const { return ___FieldLineNumber_3; }
	inline int32_t* get_address_of_FieldLineNumber_3() { return &___FieldLineNumber_3; }
	inline void set_FieldLineNumber_3(int32_t value)
	{
		___FieldLineNumber_3 = value;
	}

	inline static int32_t get_offset_of_FieldLinePosition_4() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___FieldLinePosition_4)); }
	inline int32_t get_FieldLinePosition_4() const { return ___FieldLinePosition_4; }
	inline int32_t* get_address_of_FieldLinePosition_4() { return &___FieldLinePosition_4; }
	inline void set_FieldLinePosition_4(int32_t value)
	{
		___FieldLinePosition_4 = value;
	}

	inline static int32_t get_offset_of_FieldHasLineInfo_5() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___FieldHasLineInfo_5)); }
	inline bool get_FieldHasLineInfo_5() const { return ___FieldHasLineInfo_5; }
	inline bool* get_address_of_FieldHasLineInfo_5() { return &___FieldHasLineInfo_5; }
	inline void set_FieldHasLineInfo_5(bool value)
	{
		___FieldHasLineInfo_5 = value;
	}

	inline static int32_t get_offset_of_FieldType_6() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___FieldType_6)); }
	inline XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0 * get_FieldType_6() const { return ___FieldType_6; }
	inline XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0 ** get_address_of_FieldType_6() { return &___FieldType_6; }
	inline void set_FieldType_6(XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0 * value)
	{
		___FieldType_6 = value;
		Il2CppCodeGenWriteBarrier((&___FieldType_6), value);
	}

	inline static int32_t get_offset_of_Identity_7() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___Identity_7)); }
	inline RuntimeObject * get_Identity_7() const { return ___Identity_7; }
	inline RuntimeObject ** get_address_of_Identity_7() { return &___Identity_7; }
	inline void set_Identity_7(RuntimeObject * value)
	{
		___Identity_7 = value;
		Il2CppCodeGenWriteBarrier((&___Identity_7), value);
	}

	inline static int32_t get_offset_of_IsXsiNil_8() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___IsXsiNil_8)); }
	inline bool get_IsXsiNil_8() const { return ___IsXsiNil_8; }
	inline bool* get_address_of_IsXsiNil_8() { return &___IsXsiNil_8; }
	inline void set_IsXsiNil_8(bool value)
	{
		___IsXsiNil_8 = value;
	}

	inline static int32_t get_offset_of_FieldFoundDepth_9() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___FieldFoundDepth_9)); }
	inline int32_t get_FieldFoundDepth_9() const { return ___FieldFoundDepth_9; }
	inline int32_t* get_address_of_FieldFoundDepth_9() { return &___FieldFoundDepth_9; }
	inline void set_FieldFoundDepth_9(int32_t value)
	{
		___FieldFoundDepth_9 = value;
	}

	inline static int32_t get_offset_of_FieldFoundPath_10() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___FieldFoundPath_10)); }
	inline XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE * get_FieldFoundPath_10() const { return ___FieldFoundPath_10; }
	inline XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE ** get_address_of_FieldFoundPath_10() { return &___FieldFoundPath_10; }
	inline void set_FieldFoundPath_10(XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE * value)
	{
		___FieldFoundPath_10 = value;
		Il2CppCodeGenWriteBarrier((&___FieldFoundPath_10), value);
	}

	inline static int32_t get_offset_of_Consuming_11() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___Consuming_11)); }
	inline bool get_Consuming_11() const { return ___Consuming_11; }
	inline bool* get_address_of_Consuming_11() { return &___Consuming_11; }
	inline void set_Consuming_11(bool value)
	{
		___Consuming_11 = value;
	}

	inline static int32_t get_offset_of_Consumed_12() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___Consumed_12)); }
	inline bool get_Consumed_12() const { return ___Consumed_12; }
	inline bool* get_address_of_Consumed_12() { return &___Consumed_12; }
	inline void set_Consumed_12(bool value)
	{
		___Consumed_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDKEYENTRYFIELD_T46A4E7087BCB9D206208679EA739175225D551D2_H
#ifndef XSDKEYTABLE_T75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6_H
#define XSDKEYTABLE_T75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdKeyTable
struct  XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6  : public RuntimeObject
{
public:
	// System.Boolean Mono.Xml.Schema.XsdKeyTable::alwaysTrue
	bool ___alwaysTrue_0;
	// Mono.Xml.Schema.XsdIdentitySelector Mono.Xml.Schema.XsdKeyTable::selector
	XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8 * ___selector_1;
	// System.Xml.Schema.XmlSchemaIdentityConstraint Mono.Xml.Schema.XsdKeyTable::source
	XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9 * ___source_2;
	// System.Xml.XmlQualifiedName Mono.Xml.Schema.XsdKeyTable::qname
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___qname_3;
	// System.Xml.XmlQualifiedName Mono.Xml.Schema.XsdKeyTable::refKeyName
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___refKeyName_4;
	// Mono.Xml.Schema.XsdKeyEntryCollection Mono.Xml.Schema.XsdKeyTable::Entries
	XsdKeyEntryCollection_tF37F133E46ECFF1F08CBB56894E1B18CB931B265 * ___Entries_5;
	// Mono.Xml.Schema.XsdKeyEntryCollection Mono.Xml.Schema.XsdKeyTable::FinishedEntries
	XsdKeyEntryCollection_tF37F133E46ECFF1F08CBB56894E1B18CB931B265 * ___FinishedEntries_6;
	// System.Int32 Mono.Xml.Schema.XsdKeyTable::StartDepth
	int32_t ___StartDepth_7;
	// Mono.Xml.Schema.XsdKeyTable Mono.Xml.Schema.XsdKeyTable::ReferencedKey
	XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6 * ___ReferencedKey_8;

public:
	inline static int32_t get_offset_of_alwaysTrue_0() { return static_cast<int32_t>(offsetof(XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6, ___alwaysTrue_0)); }
	inline bool get_alwaysTrue_0() const { return ___alwaysTrue_0; }
	inline bool* get_address_of_alwaysTrue_0() { return &___alwaysTrue_0; }
	inline void set_alwaysTrue_0(bool value)
	{
		___alwaysTrue_0 = value;
	}

	inline static int32_t get_offset_of_selector_1() { return static_cast<int32_t>(offsetof(XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6, ___selector_1)); }
	inline XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8 * get_selector_1() const { return ___selector_1; }
	inline XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8 ** get_address_of_selector_1() { return &___selector_1; }
	inline void set_selector_1(XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8 * value)
	{
		___selector_1 = value;
		Il2CppCodeGenWriteBarrier((&___selector_1), value);
	}

	inline static int32_t get_offset_of_source_2() { return static_cast<int32_t>(offsetof(XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6, ___source_2)); }
	inline XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9 * get_source_2() const { return ___source_2; }
	inline XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9 ** get_address_of_source_2() { return &___source_2; }
	inline void set_source_2(XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9 * value)
	{
		___source_2 = value;
		Il2CppCodeGenWriteBarrier((&___source_2), value);
	}

	inline static int32_t get_offset_of_qname_3() { return static_cast<int32_t>(offsetof(XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6, ___qname_3)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_qname_3() const { return ___qname_3; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_qname_3() { return &___qname_3; }
	inline void set_qname_3(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___qname_3 = value;
		Il2CppCodeGenWriteBarrier((&___qname_3), value);
	}

	inline static int32_t get_offset_of_refKeyName_4() { return static_cast<int32_t>(offsetof(XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6, ___refKeyName_4)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_refKeyName_4() const { return ___refKeyName_4; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_refKeyName_4() { return &___refKeyName_4; }
	inline void set_refKeyName_4(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___refKeyName_4 = value;
		Il2CppCodeGenWriteBarrier((&___refKeyName_4), value);
	}

	inline static int32_t get_offset_of_Entries_5() { return static_cast<int32_t>(offsetof(XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6, ___Entries_5)); }
	inline XsdKeyEntryCollection_tF37F133E46ECFF1F08CBB56894E1B18CB931B265 * get_Entries_5() const { return ___Entries_5; }
	inline XsdKeyEntryCollection_tF37F133E46ECFF1F08CBB56894E1B18CB931B265 ** get_address_of_Entries_5() { return &___Entries_5; }
	inline void set_Entries_5(XsdKeyEntryCollection_tF37F133E46ECFF1F08CBB56894E1B18CB931B265 * value)
	{
		___Entries_5 = value;
		Il2CppCodeGenWriteBarrier((&___Entries_5), value);
	}

	inline static int32_t get_offset_of_FinishedEntries_6() { return static_cast<int32_t>(offsetof(XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6, ___FinishedEntries_6)); }
	inline XsdKeyEntryCollection_tF37F133E46ECFF1F08CBB56894E1B18CB931B265 * get_FinishedEntries_6() const { return ___FinishedEntries_6; }
	inline XsdKeyEntryCollection_tF37F133E46ECFF1F08CBB56894E1B18CB931B265 ** get_address_of_FinishedEntries_6() { return &___FinishedEntries_6; }
	inline void set_FinishedEntries_6(XsdKeyEntryCollection_tF37F133E46ECFF1F08CBB56894E1B18CB931B265 * value)
	{
		___FinishedEntries_6 = value;
		Il2CppCodeGenWriteBarrier((&___FinishedEntries_6), value);
	}

	inline static int32_t get_offset_of_StartDepth_7() { return static_cast<int32_t>(offsetof(XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6, ___StartDepth_7)); }
	inline int32_t get_StartDepth_7() const { return ___StartDepth_7; }
	inline int32_t* get_address_of_StartDepth_7() { return &___StartDepth_7; }
	inline void set_StartDepth_7(int32_t value)
	{
		___StartDepth_7 = value;
	}

	inline static int32_t get_offset_of_ReferencedKey_8() { return static_cast<int32_t>(offsetof(XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6, ___ReferencedKey_8)); }
	inline XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6 * get_ReferencedKey_8() const { return ___ReferencedKey_8; }
	inline XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6 ** get_address_of_ReferencedKey_8() { return &___ReferencedKey_8; }
	inline void set_ReferencedKey_8(XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6 * value)
	{
		___ReferencedKey_8 = value;
		Il2CppCodeGenWriteBarrier((&___ReferencedKey_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDKEYTABLE_T75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6_H
#ifndef XSDVALIDATIONCONTEXT_TC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD_H
#define XSDVALIDATIONCONTEXT_TC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdValidationContext
struct  XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD  : public RuntimeObject
{
public:
	// System.Object Mono.Xml.Schema.XsdValidationContext::xsi_type
	RuntimeObject * ___xsi_type_0;
	// Mono.Xml.Schema.XsdValidationState Mono.Xml.Schema.XsdValidationContext::State
	XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 * ___State_1;
	// System.Collections.Stack Mono.Xml.Schema.XsdValidationContext::element_stack
	Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * ___element_stack_2;

public:
	inline static int32_t get_offset_of_xsi_type_0() { return static_cast<int32_t>(offsetof(XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD, ___xsi_type_0)); }
	inline RuntimeObject * get_xsi_type_0() const { return ___xsi_type_0; }
	inline RuntimeObject ** get_address_of_xsi_type_0() { return &___xsi_type_0; }
	inline void set_xsi_type_0(RuntimeObject * value)
	{
		___xsi_type_0 = value;
		Il2CppCodeGenWriteBarrier((&___xsi_type_0), value);
	}

	inline static int32_t get_offset_of_State_1() { return static_cast<int32_t>(offsetof(XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD, ___State_1)); }
	inline XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 * get_State_1() const { return ___State_1; }
	inline XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 ** get_address_of_State_1() { return &___State_1; }
	inline void set_State_1(XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 * value)
	{
		___State_1 = value;
		Il2CppCodeGenWriteBarrier((&___State_1), value);
	}

	inline static int32_t get_offset_of_element_stack_2() { return static_cast<int32_t>(offsetof(XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD, ___element_stack_2)); }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * get_element_stack_2() const { return ___element_stack_2; }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 ** get_address_of_element_stack_2() { return &___element_stack_2; }
	inline void set_element_stack_2(Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * value)
	{
		___element_stack_2 = value;
		Il2CppCodeGenWriteBarrier((&___element_stack_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDVALIDATIONCONTEXT_TC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD_H
#ifndef XSDVALIDATIONSTATE_T6CEE1D803E048D746FBDC7439403F070F3D66C74_H
#define XSDVALIDATIONSTATE_T6CEE1D803E048D746FBDC7439403F070F3D66C74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdValidationState
struct  XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74  : public RuntimeObject
{
public:
	// System.Int32 Mono.Xml.Schema.XsdValidationState::occured
	int32_t ___occured_1;
	// Mono.Xml.Schema.XsdParticleStateManager Mono.Xml.Schema.XsdValidationState::manager
	XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68 * ___manager_2;

public:
	inline static int32_t get_offset_of_occured_1() { return static_cast<int32_t>(offsetof(XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74, ___occured_1)); }
	inline int32_t get_occured_1() const { return ___occured_1; }
	inline int32_t* get_address_of_occured_1() { return &___occured_1; }
	inline void set_occured_1(int32_t value)
	{
		___occured_1 = value;
	}

	inline static int32_t get_offset_of_manager_2() { return static_cast<int32_t>(offsetof(XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74, ___manager_2)); }
	inline XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68 * get_manager_2() const { return ___manager_2; }
	inline XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68 ** get_address_of_manager_2() { return &___manager_2; }
	inline void set_manager_2(XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68 * value)
	{
		___manager_2 = value;
		Il2CppCodeGenWriteBarrier((&___manager_2), value);
	}
};

struct XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74_StaticFields
{
public:
	// Mono.Xml.Schema.XsdInvalidValidationState Mono.Xml.Schema.XsdValidationState::invalid
	XsdInvalidValidationState_t3532998F32C0C00F8E0F0C199D2308C867084FFF * ___invalid_0;

public:
	inline static int32_t get_offset_of_invalid_0() { return static_cast<int32_t>(offsetof(XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74_StaticFields, ___invalid_0)); }
	inline XsdInvalidValidationState_t3532998F32C0C00F8E0F0C199D2308C867084FFF * get_invalid_0() const { return ___invalid_0; }
	inline XsdInvalidValidationState_t3532998F32C0C00F8E0F0C199D2308C867084FFF ** get_address_of_invalid_0() { return &___invalid_0; }
	inline void set_invalid_0(XsdInvalidValidationState_t3532998F32C0C00F8E0F0C199D2308C867084FFF * value)
	{
		___invalid_0 = value;
		Il2CppCodeGenWriteBarrier((&___invalid_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDVALIDATIONSTATE_T6CEE1D803E048D746FBDC7439403F070F3D66C74_H
#ifndef ATTRIBUTE_T60F25EB48D5935E4C6C2BAF7F90F57A43528E469_H
#define ATTRIBUTE_T60F25EB48D5935E4C6C2BAF7F90F57A43528E469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t60F25EB48D5935E4C6C2BAF7F90F57A43528E469  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T60F25EB48D5935E4C6C2BAF7F90F57A43528E469_H
#ifndef COLLECTIONBASE_TC3F24120B23471E7FDEE72107D1D541E6456744F_H
#define COLLECTIONBASE_TC3F24120B23471E7FDEE72107D1D541E6456744F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.CollectionBase
struct  CollectionBase_tC3F24120B23471E7FDEE72107D1D541E6456744F  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.CollectionBase::list
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(CollectionBase_tC3F24120B23471E7FDEE72107D1D541E6456744F, ___list_0)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_list_0() const { return ___list_0; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONBASE_TC3F24120B23471E7FDEE72107D1D541E6456744F_H
#ifndef URI_TF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_H
#define URI_TF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri
struct  Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E  : public RuntimeObject
{
public:
	// System.Boolean System.Uri::isUnixFilePath
	bool ___isUnixFilePath_0;
	// System.String System.Uri::source
	String_t* ___source_1;
	// System.String System.Uri::scheme
	String_t* ___scheme_2;
	// System.String System.Uri::host
	String_t* ___host_3;
	// System.Int32 System.Uri::port
	int32_t ___port_4;
	// System.String System.Uri::path
	String_t* ___path_5;
	// System.String System.Uri::query
	String_t* ___query_6;
	// System.String System.Uri::fragment
	String_t* ___fragment_7;
	// System.String System.Uri::userinfo
	String_t* ___userinfo_8;
	// System.Boolean System.Uri::isUnc
	bool ___isUnc_9;
	// System.Boolean System.Uri::isOpaquePart
	bool ___isOpaquePart_10;
	// System.Boolean System.Uri::isAbsoluteUri
	bool ___isAbsoluteUri_11;
	// System.Boolean System.Uri::userEscaped
	bool ___userEscaped_12;
	// System.String System.Uri::cachedAbsoluteUri
	String_t* ___cachedAbsoluteUri_13;
	// System.String System.Uri::cachedToString
	String_t* ___cachedToString_14;
	// System.String System.Uri::cachedLocalPath
	String_t* ___cachedLocalPath_15;
	// System.Int32 System.Uri::cachedHashCode
	int32_t ___cachedHashCode_16;
	// System.UriParser System.Uri::parser
	UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A * ___parser_30;

public:
	inline static int32_t get_offset_of_isUnixFilePath_0() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___isUnixFilePath_0)); }
	inline bool get_isUnixFilePath_0() const { return ___isUnixFilePath_0; }
	inline bool* get_address_of_isUnixFilePath_0() { return &___isUnixFilePath_0; }
	inline void set_isUnixFilePath_0(bool value)
	{
		___isUnixFilePath_0 = value;
	}

	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___source_1)); }
	inline String_t* get_source_1() const { return ___source_1; }
	inline String_t** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(String_t* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_scheme_2() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___scheme_2)); }
	inline String_t* get_scheme_2() const { return ___scheme_2; }
	inline String_t** get_address_of_scheme_2() { return &___scheme_2; }
	inline void set_scheme_2(String_t* value)
	{
		___scheme_2 = value;
		Il2CppCodeGenWriteBarrier((&___scheme_2), value);
	}

	inline static int32_t get_offset_of_host_3() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___host_3)); }
	inline String_t* get_host_3() const { return ___host_3; }
	inline String_t** get_address_of_host_3() { return &___host_3; }
	inline void set_host_3(String_t* value)
	{
		___host_3 = value;
		Il2CppCodeGenWriteBarrier((&___host_3), value);
	}

	inline static int32_t get_offset_of_port_4() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___port_4)); }
	inline int32_t get_port_4() const { return ___port_4; }
	inline int32_t* get_address_of_port_4() { return &___port_4; }
	inline void set_port_4(int32_t value)
	{
		___port_4 = value;
	}

	inline static int32_t get_offset_of_path_5() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___path_5)); }
	inline String_t* get_path_5() const { return ___path_5; }
	inline String_t** get_address_of_path_5() { return &___path_5; }
	inline void set_path_5(String_t* value)
	{
		___path_5 = value;
		Il2CppCodeGenWriteBarrier((&___path_5), value);
	}

	inline static int32_t get_offset_of_query_6() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___query_6)); }
	inline String_t* get_query_6() const { return ___query_6; }
	inline String_t** get_address_of_query_6() { return &___query_6; }
	inline void set_query_6(String_t* value)
	{
		___query_6 = value;
		Il2CppCodeGenWriteBarrier((&___query_6), value);
	}

	inline static int32_t get_offset_of_fragment_7() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___fragment_7)); }
	inline String_t* get_fragment_7() const { return ___fragment_7; }
	inline String_t** get_address_of_fragment_7() { return &___fragment_7; }
	inline void set_fragment_7(String_t* value)
	{
		___fragment_7 = value;
		Il2CppCodeGenWriteBarrier((&___fragment_7), value);
	}

	inline static int32_t get_offset_of_userinfo_8() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___userinfo_8)); }
	inline String_t* get_userinfo_8() const { return ___userinfo_8; }
	inline String_t** get_address_of_userinfo_8() { return &___userinfo_8; }
	inline void set_userinfo_8(String_t* value)
	{
		___userinfo_8 = value;
		Il2CppCodeGenWriteBarrier((&___userinfo_8), value);
	}

	inline static int32_t get_offset_of_isUnc_9() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___isUnc_9)); }
	inline bool get_isUnc_9() const { return ___isUnc_9; }
	inline bool* get_address_of_isUnc_9() { return &___isUnc_9; }
	inline void set_isUnc_9(bool value)
	{
		___isUnc_9 = value;
	}

	inline static int32_t get_offset_of_isOpaquePart_10() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___isOpaquePart_10)); }
	inline bool get_isOpaquePart_10() const { return ___isOpaquePart_10; }
	inline bool* get_address_of_isOpaquePart_10() { return &___isOpaquePart_10; }
	inline void set_isOpaquePart_10(bool value)
	{
		___isOpaquePart_10 = value;
	}

	inline static int32_t get_offset_of_isAbsoluteUri_11() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___isAbsoluteUri_11)); }
	inline bool get_isAbsoluteUri_11() const { return ___isAbsoluteUri_11; }
	inline bool* get_address_of_isAbsoluteUri_11() { return &___isAbsoluteUri_11; }
	inline void set_isAbsoluteUri_11(bool value)
	{
		___isAbsoluteUri_11 = value;
	}

	inline static int32_t get_offset_of_userEscaped_12() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___userEscaped_12)); }
	inline bool get_userEscaped_12() const { return ___userEscaped_12; }
	inline bool* get_address_of_userEscaped_12() { return &___userEscaped_12; }
	inline void set_userEscaped_12(bool value)
	{
		___userEscaped_12 = value;
	}

	inline static int32_t get_offset_of_cachedAbsoluteUri_13() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___cachedAbsoluteUri_13)); }
	inline String_t* get_cachedAbsoluteUri_13() const { return ___cachedAbsoluteUri_13; }
	inline String_t** get_address_of_cachedAbsoluteUri_13() { return &___cachedAbsoluteUri_13; }
	inline void set_cachedAbsoluteUri_13(String_t* value)
	{
		___cachedAbsoluteUri_13 = value;
		Il2CppCodeGenWriteBarrier((&___cachedAbsoluteUri_13), value);
	}

	inline static int32_t get_offset_of_cachedToString_14() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___cachedToString_14)); }
	inline String_t* get_cachedToString_14() const { return ___cachedToString_14; }
	inline String_t** get_address_of_cachedToString_14() { return &___cachedToString_14; }
	inline void set_cachedToString_14(String_t* value)
	{
		___cachedToString_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedToString_14), value);
	}

	inline static int32_t get_offset_of_cachedLocalPath_15() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___cachedLocalPath_15)); }
	inline String_t* get_cachedLocalPath_15() const { return ___cachedLocalPath_15; }
	inline String_t** get_address_of_cachedLocalPath_15() { return &___cachedLocalPath_15; }
	inline void set_cachedLocalPath_15(String_t* value)
	{
		___cachedLocalPath_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedLocalPath_15), value);
	}

	inline static int32_t get_offset_of_cachedHashCode_16() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___cachedHashCode_16)); }
	inline int32_t get_cachedHashCode_16() const { return ___cachedHashCode_16; }
	inline int32_t* get_address_of_cachedHashCode_16() { return &___cachedHashCode_16; }
	inline void set_cachedHashCode_16(int32_t value)
	{
		___cachedHashCode_16 = value;
	}

	inline static int32_t get_offset_of_parser_30() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___parser_30)); }
	inline UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A * get_parser_30() const { return ___parser_30; }
	inline UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A ** get_address_of_parser_30() { return &___parser_30; }
	inline void set_parser_30(UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A * value)
	{
		___parser_30 = value;
		Il2CppCodeGenWriteBarrier((&___parser_30), value);
	}
};

struct Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields
{
public:
	// System.String System.Uri::hexUpperChars
	String_t* ___hexUpperChars_17;
	// System.String System.Uri::SchemeDelimiter
	String_t* ___SchemeDelimiter_18;
	// System.String System.Uri::UriSchemeFile
	String_t* ___UriSchemeFile_19;
	// System.String System.Uri::UriSchemeFtp
	String_t* ___UriSchemeFtp_20;
	// System.String System.Uri::UriSchemeGopher
	String_t* ___UriSchemeGopher_21;
	// System.String System.Uri::UriSchemeHttp
	String_t* ___UriSchemeHttp_22;
	// System.String System.Uri::UriSchemeHttps
	String_t* ___UriSchemeHttps_23;
	// System.String System.Uri::UriSchemeMailto
	String_t* ___UriSchemeMailto_24;
	// System.String System.Uri::UriSchemeNews
	String_t* ___UriSchemeNews_25;
	// System.String System.Uri::UriSchemeNntp
	String_t* ___UriSchemeNntp_26;
	// System.String System.Uri::UriSchemeNetPipe
	String_t* ___UriSchemeNetPipe_27;
	// System.String System.Uri::UriSchemeNetTcp
	String_t* ___UriSchemeNetTcp_28;
	// System.Uri/UriScheme[] System.Uri::schemes
	UriSchemeU5BU5D_tA21320F0E7A6C8B70FD19E286075856BFB5DB2E6* ___schemes_29;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map12
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map12_31;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map13
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map13_32;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map14
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map14_33;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map15
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map15_34;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map16
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map16_35;

public:
	inline static int32_t get_offset_of_hexUpperChars_17() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___hexUpperChars_17)); }
	inline String_t* get_hexUpperChars_17() const { return ___hexUpperChars_17; }
	inline String_t** get_address_of_hexUpperChars_17() { return &___hexUpperChars_17; }
	inline void set_hexUpperChars_17(String_t* value)
	{
		___hexUpperChars_17 = value;
		Il2CppCodeGenWriteBarrier((&___hexUpperChars_17), value);
	}

	inline static int32_t get_offset_of_SchemeDelimiter_18() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___SchemeDelimiter_18)); }
	inline String_t* get_SchemeDelimiter_18() const { return ___SchemeDelimiter_18; }
	inline String_t** get_address_of_SchemeDelimiter_18() { return &___SchemeDelimiter_18; }
	inline void set_SchemeDelimiter_18(String_t* value)
	{
		___SchemeDelimiter_18 = value;
		Il2CppCodeGenWriteBarrier((&___SchemeDelimiter_18), value);
	}

	inline static int32_t get_offset_of_UriSchemeFile_19() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeFile_19)); }
	inline String_t* get_UriSchemeFile_19() const { return ___UriSchemeFile_19; }
	inline String_t** get_address_of_UriSchemeFile_19() { return &___UriSchemeFile_19; }
	inline void set_UriSchemeFile_19(String_t* value)
	{
		___UriSchemeFile_19 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFile_19), value);
	}

	inline static int32_t get_offset_of_UriSchemeFtp_20() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeFtp_20)); }
	inline String_t* get_UriSchemeFtp_20() const { return ___UriSchemeFtp_20; }
	inline String_t** get_address_of_UriSchemeFtp_20() { return &___UriSchemeFtp_20; }
	inline void set_UriSchemeFtp_20(String_t* value)
	{
		___UriSchemeFtp_20 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFtp_20), value);
	}

	inline static int32_t get_offset_of_UriSchemeGopher_21() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeGopher_21)); }
	inline String_t* get_UriSchemeGopher_21() const { return ___UriSchemeGopher_21; }
	inline String_t** get_address_of_UriSchemeGopher_21() { return &___UriSchemeGopher_21; }
	inline void set_UriSchemeGopher_21(String_t* value)
	{
		___UriSchemeGopher_21 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeGopher_21), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttp_22() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeHttp_22)); }
	inline String_t* get_UriSchemeHttp_22() const { return ___UriSchemeHttp_22; }
	inline String_t** get_address_of_UriSchemeHttp_22() { return &___UriSchemeHttp_22; }
	inline void set_UriSchemeHttp_22(String_t* value)
	{
		___UriSchemeHttp_22 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttp_22), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttps_23() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeHttps_23)); }
	inline String_t* get_UriSchemeHttps_23() const { return ___UriSchemeHttps_23; }
	inline String_t** get_address_of_UriSchemeHttps_23() { return &___UriSchemeHttps_23; }
	inline void set_UriSchemeHttps_23(String_t* value)
	{
		___UriSchemeHttps_23 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttps_23), value);
	}

	inline static int32_t get_offset_of_UriSchemeMailto_24() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeMailto_24)); }
	inline String_t* get_UriSchemeMailto_24() const { return ___UriSchemeMailto_24; }
	inline String_t** get_address_of_UriSchemeMailto_24() { return &___UriSchemeMailto_24; }
	inline void set_UriSchemeMailto_24(String_t* value)
	{
		___UriSchemeMailto_24 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeMailto_24), value);
	}

	inline static int32_t get_offset_of_UriSchemeNews_25() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeNews_25)); }
	inline String_t* get_UriSchemeNews_25() const { return ___UriSchemeNews_25; }
	inline String_t** get_address_of_UriSchemeNews_25() { return &___UriSchemeNews_25; }
	inline void set_UriSchemeNews_25(String_t* value)
	{
		___UriSchemeNews_25 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNews_25), value);
	}

	inline static int32_t get_offset_of_UriSchemeNntp_26() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeNntp_26)); }
	inline String_t* get_UriSchemeNntp_26() const { return ___UriSchemeNntp_26; }
	inline String_t** get_address_of_UriSchemeNntp_26() { return &___UriSchemeNntp_26; }
	inline void set_UriSchemeNntp_26(String_t* value)
	{
		___UriSchemeNntp_26 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNntp_26), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetPipe_27() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeNetPipe_27)); }
	inline String_t* get_UriSchemeNetPipe_27() const { return ___UriSchemeNetPipe_27; }
	inline String_t** get_address_of_UriSchemeNetPipe_27() { return &___UriSchemeNetPipe_27; }
	inline void set_UriSchemeNetPipe_27(String_t* value)
	{
		___UriSchemeNetPipe_27 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetPipe_27), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetTcp_28() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeNetTcp_28)); }
	inline String_t* get_UriSchemeNetTcp_28() const { return ___UriSchemeNetTcp_28; }
	inline String_t** get_address_of_UriSchemeNetTcp_28() { return &___UriSchemeNetTcp_28; }
	inline void set_UriSchemeNetTcp_28(String_t* value)
	{
		___UriSchemeNetTcp_28 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetTcp_28), value);
	}

	inline static int32_t get_offset_of_schemes_29() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___schemes_29)); }
	inline UriSchemeU5BU5D_tA21320F0E7A6C8B70FD19E286075856BFB5DB2E6* get_schemes_29() const { return ___schemes_29; }
	inline UriSchemeU5BU5D_tA21320F0E7A6C8B70FD19E286075856BFB5DB2E6** get_address_of_schemes_29() { return &___schemes_29; }
	inline void set_schemes_29(UriSchemeU5BU5D_tA21320F0E7A6C8B70FD19E286075856BFB5DB2E6* value)
	{
		___schemes_29 = value;
		Il2CppCodeGenWriteBarrier((&___schemes_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map12_31() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___U3CU3Ef__switchU24map12_31)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map12_31() const { return ___U3CU3Ef__switchU24map12_31; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map12_31() { return &___U3CU3Ef__switchU24map12_31; }
	inline void set_U3CU3Ef__switchU24map12_31(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map12_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map12_31), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map13_32() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___U3CU3Ef__switchU24map13_32)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map13_32() const { return ___U3CU3Ef__switchU24map13_32; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map13_32() { return &___U3CU3Ef__switchU24map13_32; }
	inline void set_U3CU3Ef__switchU24map13_32(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map13_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map13_32), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map14_33() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___U3CU3Ef__switchU24map14_33)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map14_33() const { return ___U3CU3Ef__switchU24map14_33; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map14_33() { return &___U3CU3Ef__switchU24map14_33; }
	inline void set_U3CU3Ef__switchU24map14_33(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map14_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map14_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map15_34() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___U3CU3Ef__switchU24map15_34)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map15_34() const { return ___U3CU3Ef__switchU24map15_34; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map15_34() { return &___U3CU3Ef__switchU24map15_34; }
	inline void set_U3CU3Ef__switchU24map15_34(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map15_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map15_34), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map16_35() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___U3CU3Ef__switchU24map16_35)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map16_35() const { return ___U3CU3Ef__switchU24map16_35; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map16_35() { return &___U3CU3Ef__switchU24map16_35; }
	inline void set_U3CU3Ef__switchU24map16_35(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map16_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map16_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URI_TF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_H
#ifndef VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#define VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_com
{
};
#endif // VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifndef XMLREADER_TC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293_H
#define XMLREADER_TC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReader
struct  XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293  : public RuntimeObject
{
public:
	// System.Xml.XmlReaderBinarySupport System.Xml.XmlReader::binary
	XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A * ___binary_0;
	// System.Xml.XmlReaderSettings System.Xml.XmlReader::settings
	XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB * ___settings_1;

public:
	inline static int32_t get_offset_of_binary_0() { return static_cast<int32_t>(offsetof(XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293, ___binary_0)); }
	inline XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A * get_binary_0() const { return ___binary_0; }
	inline XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A ** get_address_of_binary_0() { return &___binary_0; }
	inline void set_binary_0(XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A * value)
	{
		___binary_0 = value;
		Il2CppCodeGenWriteBarrier((&___binary_0), value);
	}

	inline static int32_t get_offset_of_settings_1() { return static_cast<int32_t>(offsetof(XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293, ___settings_1)); }
	inline XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB * get_settings_1() const { return ___settings_1; }
	inline XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB ** get_address_of_settings_1() { return &___settings_1; }
	inline void set_settings_1(XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB * value)
	{
		___settings_1 = value;
		Il2CppCodeGenWriteBarrier((&___settings_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADER_TC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293_H
#ifndef FIXEDBITARRAY3_T4B1347148FCA790BDC0EE6FD932E04291F68382D_H
#define FIXEDBITARRAY3_T4B1347148FCA790BDC0EE6FD932E04291F68382D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.FixedBitArray3
struct  FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D 
{
public:
	// System.Boolean Kolibri2d.Poly2Tri.FixedBitArray3::_0
	bool ____0_0;
	// System.Boolean Kolibri2d.Poly2Tri.FixedBitArray3::_1
	bool ____1_1;
	// System.Boolean Kolibri2d.Poly2Tri.FixedBitArray3::_2
	bool ____2_2;

public:
	inline static int32_t get_offset_of__0_0() { return static_cast<int32_t>(offsetof(FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D, ____0_0)); }
	inline bool get__0_0() const { return ____0_0; }
	inline bool* get_address_of__0_0() { return &____0_0; }
	inline void set__0_0(bool value)
	{
		____0_0 = value;
	}

	inline static int32_t get_offset_of__1_1() { return static_cast<int32_t>(offsetof(FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D, ____1_1)); }
	inline bool get__1_1() const { return ____1_1; }
	inline bool* get_address_of__1_1() { return &____1_1; }
	inline void set__1_1(bool value)
	{
		____1_1 = value;
	}

	inline static int32_t get_offset_of__2_2() { return static_cast<int32_t>(offsetof(FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D, ____2_2)); }
	inline bool get__2_2() const { return ____2_2; }
	inline bool* get_address_of__2_2() { return &____2_2; }
	inline void set__2_2(bool value)
	{
		____2_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Kolibri2d.Poly2Tri.FixedBitArray3
struct FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D_marshaled_pinvoke
{
	int32_t ____0_0;
	int32_t ____1_1;
	int32_t ____2_2;
};
// Native definition for COM marshalling of Kolibri2d.Poly2Tri.FixedBitArray3
struct FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D_marshaled_com
{
	int32_t ____0_0;
	int32_t ____1_1;
	int32_t ____2_2;
};
#endif // FIXEDBITARRAY3_T4B1347148FCA790BDC0EE6FD932E04291F68382D_H
#ifndef TRIANGULATIONCONSTRAINT_T1A7BE017E0DA33DF3619D2E27A1EA9E4A6A3C14C_H
#define TRIANGULATIONCONSTRAINT_T1A7BE017E0DA33DF3619D2E27A1EA9E4A6A3C14C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.TriangulationConstraint
struct  TriangulationConstraint_t1A7BE017E0DA33DF3619D2E27A1EA9E4A6A3C14C  : public Edge_tFB6F8F74D888B19F8D03A97A9CA65E60889DC672
{
public:
	// System.UInt32 Kolibri2d.Poly2Tri.TriangulationConstraint::mContraintCode
	uint32_t ___mContraintCode_2;

public:
	inline static int32_t get_offset_of_mContraintCode_2() { return static_cast<int32_t>(offsetof(TriangulationConstraint_t1A7BE017E0DA33DF3619D2E27A1EA9E4A6A3C14C, ___mContraintCode_2)); }
	inline uint32_t get_mContraintCode_2() const { return ___mContraintCode_2; }
	inline uint32_t* get_address_of_mContraintCode_2() { return &___mContraintCode_2; }
	inline void set_mContraintCode_2(uint32_t value)
	{
		___mContraintCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATIONCONSTRAINT_T1A7BE017E0DA33DF3619D2E27A1EA9E4A6A3C14C_H
#ifndef TRIANGULATIONPOINT_TBBB89E93E69AAF59B2A8B7662D60C619242E0F8E_H
#define TRIANGULATIONPOINT_TBBB89E93E69AAF59B2A8B7662D60C619242E0F8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.TriangulationPoint
struct  TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E  : public Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D
{
public:
	// System.UInt32 Kolibri2d.Poly2Tri.TriangulationPoint::mVertexCode
	uint32_t ___mVertexCode_3;
	// System.Collections.Generic.List`1<Kolibri2d.Poly2Tri.DTSweepConstraint> Kolibri2d.Poly2Tri.TriangulationPoint::<Edges>k__BackingField
	List_1_tC49DC5D4CE8A9D94F5703BC3CC0D1DCA924BD98D * ___U3CEdgesU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_mVertexCode_3() { return static_cast<int32_t>(offsetof(TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E, ___mVertexCode_3)); }
	inline uint32_t get_mVertexCode_3() const { return ___mVertexCode_3; }
	inline uint32_t* get_address_of_mVertexCode_3() { return &___mVertexCode_3; }
	inline void set_mVertexCode_3(uint32_t value)
	{
		___mVertexCode_3 = value;
	}

	inline static int32_t get_offset_of_U3CEdgesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E, ___U3CEdgesU3Ek__BackingField_4)); }
	inline List_1_tC49DC5D4CE8A9D94F5703BC3CC0D1DCA924BD98D * get_U3CEdgesU3Ek__BackingField_4() const { return ___U3CEdgesU3Ek__BackingField_4; }
	inline List_1_tC49DC5D4CE8A9D94F5703BC3CC0D1DCA924BD98D ** get_address_of_U3CEdgesU3Ek__BackingField_4() { return &___U3CEdgesU3Ek__BackingField_4; }
	inline void set_U3CEdgesU3Ek__BackingField_4(List_1_tC49DC5D4CE8A9D94F5703BC3CC0D1DCA924BD98D * value)
	{
		___U3CEdgesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEdgesU3Ek__BackingField_4), value);
	}
};

struct TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E_StaticFields
{
public:
	// System.Double Kolibri2d.Poly2Tri.TriangulationPoint::kVertexCodeDefaultPrecision
	double ___kVertexCodeDefaultPrecision_2;

public:
	inline static int32_t get_offset_of_kVertexCodeDefaultPrecision_2() { return static_cast<int32_t>(offsetof(TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E_StaticFields, ___kVertexCodeDefaultPrecision_2)); }
	inline double get_kVertexCodeDefaultPrecision_2() const { return ___kVertexCodeDefaultPrecision_2; }
	inline double* get_address_of_kVertexCodeDefaultPrecision_2() { return &___kVertexCodeDefaultPrecision_2; }
	inline void set_kVertexCodeDefaultPrecision_2(double value)
	{
		___kVertexCodeDefaultPrecision_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATIONPOINT_TBBB89E93E69AAF59B2A8B7662D60C619242E0F8E_H
#ifndef XMLSCHEMAURI_T93E9CFD7A5D337C08D82EE3A3D83BA447B760FE0_H
#define XMLSCHEMAURI_T93E9CFD7A5D337C08D82EE3A3D83BA447B760FE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XmlSchemaUri
struct  XmlSchemaUri_t93E9CFD7A5D337C08D82EE3A3D83BA447B760FE0  : public Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E
{
public:
	// System.String Mono.Xml.Schema.XmlSchemaUri::value
	String_t* ___value_36;

public:
	inline static int32_t get_offset_of_value_36() { return static_cast<int32_t>(offsetof(XmlSchemaUri_t93E9CFD7A5D337C08D82EE3A3D83BA447B760FE0, ___value_36)); }
	inline String_t* get_value_36() const { return ___value_36; }
	inline String_t** get_address_of_value_36() { return &___value_36; }
	inline void set_value_36(String_t* value)
	{
		___value_36 = value;
		Il2CppCodeGenWriteBarrier((&___value_36), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAURI_T93E9CFD7A5D337C08D82EE3A3D83BA447B760FE0_H
#ifndef XSDALLVALIDATIONSTATE_T4ED82B2BF3F8403103526301F3BB097F4F951758_H
#define XSDALLVALIDATIONSTATE_T4ED82B2BF3F8403103526301F3BB097F4F951758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdAllValidationState
struct  XsdAllValidationState_t4ED82B2BF3F8403103526301F3BB097F4F951758  : public XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74
{
public:
	// System.Xml.Schema.XmlSchemaAll Mono.Xml.Schema.XsdAllValidationState::all
	XmlSchemaAll_tD45D6713E3AF00578B100499CE5A37067F862EAB * ___all_3;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdAllValidationState::consumed
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___consumed_4;

public:
	inline static int32_t get_offset_of_all_3() { return static_cast<int32_t>(offsetof(XsdAllValidationState_t4ED82B2BF3F8403103526301F3BB097F4F951758, ___all_3)); }
	inline XmlSchemaAll_tD45D6713E3AF00578B100499CE5A37067F862EAB * get_all_3() const { return ___all_3; }
	inline XmlSchemaAll_tD45D6713E3AF00578B100499CE5A37067F862EAB ** get_address_of_all_3() { return &___all_3; }
	inline void set_all_3(XmlSchemaAll_tD45D6713E3AF00578B100499CE5A37067F862EAB * value)
	{
		___all_3 = value;
		Il2CppCodeGenWriteBarrier((&___all_3), value);
	}

	inline static int32_t get_offset_of_consumed_4() { return static_cast<int32_t>(offsetof(XsdAllValidationState_t4ED82B2BF3F8403103526301F3BB097F4F951758, ___consumed_4)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_consumed_4() const { return ___consumed_4; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_consumed_4() { return &___consumed_4; }
	inline void set_consumed_4(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___consumed_4 = value;
		Il2CppCodeGenWriteBarrier((&___consumed_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDALLVALIDATIONSTATE_T4ED82B2BF3F8403103526301F3BB097F4F951758_H
#ifndef XSDANYVALIDATIONSTATE_T6ABF8A32C88C9171AA284E91FB0ADED6F91267A3_H
#define XSDANYVALIDATIONSTATE_T6ABF8A32C88C9171AA284E91FB0ADED6F91267A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdAnyValidationState
struct  XsdAnyValidationState_t6ABF8A32C88C9171AA284E91FB0ADED6F91267A3  : public XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74
{
public:
	// System.Xml.Schema.XmlSchemaAny Mono.Xml.Schema.XsdAnyValidationState::any
	XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF * ___any_3;

public:
	inline static int32_t get_offset_of_any_3() { return static_cast<int32_t>(offsetof(XsdAnyValidationState_t6ABF8A32C88C9171AA284E91FB0ADED6F91267A3, ___any_3)); }
	inline XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF * get_any_3() const { return ___any_3; }
	inline XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF ** get_address_of_any_3() { return &___any_3; }
	inline void set_any_3(XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF * value)
	{
		___any_3 = value;
		Il2CppCodeGenWriteBarrier((&___any_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDANYVALIDATIONSTATE_T6ABF8A32C88C9171AA284E91FB0ADED6F91267A3_H
#ifndef XSDAPPENDEDVALIDATIONSTATE_TF78BEDEB51F43A455D52D86B17F4E77A28D2BFCD_H
#define XSDAPPENDEDVALIDATIONSTATE_TF78BEDEB51F43A455D52D86B17F4E77A28D2BFCD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdAppendedValidationState
struct  XsdAppendedValidationState_tF78BEDEB51F43A455D52D86B17F4E77A28D2BFCD  : public XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74
{
public:
	// Mono.Xml.Schema.XsdValidationState Mono.Xml.Schema.XsdAppendedValidationState::head
	XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 * ___head_3;
	// Mono.Xml.Schema.XsdValidationState Mono.Xml.Schema.XsdAppendedValidationState::rest
	XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 * ___rest_4;

public:
	inline static int32_t get_offset_of_head_3() { return static_cast<int32_t>(offsetof(XsdAppendedValidationState_tF78BEDEB51F43A455D52D86B17F4E77A28D2BFCD, ___head_3)); }
	inline XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 * get_head_3() const { return ___head_3; }
	inline XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 ** get_address_of_head_3() { return &___head_3; }
	inline void set_head_3(XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 * value)
	{
		___head_3 = value;
		Il2CppCodeGenWriteBarrier((&___head_3), value);
	}

	inline static int32_t get_offset_of_rest_4() { return static_cast<int32_t>(offsetof(XsdAppendedValidationState_tF78BEDEB51F43A455D52D86B17F4E77A28D2BFCD, ___rest_4)); }
	inline XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 * get_rest_4() const { return ___rest_4; }
	inline XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 ** get_address_of_rest_4() { return &___rest_4; }
	inline void set_rest_4(XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 * value)
	{
		___rest_4 = value;
		Il2CppCodeGenWriteBarrier((&___rest_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDAPPENDEDVALIDATIONSTATE_TF78BEDEB51F43A455D52D86B17F4E77A28D2BFCD_H
#ifndef XSDCHOICEVALIDATIONSTATE_T6A72354CC424E101B3DB7472BBD8ADA7F61ACFC2_H
#define XSDCHOICEVALIDATIONSTATE_T6A72354CC424E101B3DB7472BBD8ADA7F61ACFC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdChoiceValidationState
struct  XsdChoiceValidationState_t6A72354CC424E101B3DB7472BBD8ADA7F61ACFC2  : public XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74
{
public:
	// System.Xml.Schema.XmlSchemaChoice Mono.Xml.Schema.XsdChoiceValidationState::choice
	XmlSchemaChoice_t6872795EE02FB1510551BFC1F5657111504F33D5 * ___choice_3;
	// System.Boolean Mono.Xml.Schema.XsdChoiceValidationState::emptiable
	bool ___emptiable_4;
	// System.Boolean Mono.Xml.Schema.XsdChoiceValidationState::emptiableComputed
	bool ___emptiableComputed_5;

public:
	inline static int32_t get_offset_of_choice_3() { return static_cast<int32_t>(offsetof(XsdChoiceValidationState_t6A72354CC424E101B3DB7472BBD8ADA7F61ACFC2, ___choice_3)); }
	inline XmlSchemaChoice_t6872795EE02FB1510551BFC1F5657111504F33D5 * get_choice_3() const { return ___choice_3; }
	inline XmlSchemaChoice_t6872795EE02FB1510551BFC1F5657111504F33D5 ** get_address_of_choice_3() { return &___choice_3; }
	inline void set_choice_3(XmlSchemaChoice_t6872795EE02FB1510551BFC1F5657111504F33D5 * value)
	{
		___choice_3 = value;
		Il2CppCodeGenWriteBarrier((&___choice_3), value);
	}

	inline static int32_t get_offset_of_emptiable_4() { return static_cast<int32_t>(offsetof(XsdChoiceValidationState_t6A72354CC424E101B3DB7472BBD8ADA7F61ACFC2, ___emptiable_4)); }
	inline bool get_emptiable_4() const { return ___emptiable_4; }
	inline bool* get_address_of_emptiable_4() { return &___emptiable_4; }
	inline void set_emptiable_4(bool value)
	{
		___emptiable_4 = value;
	}

	inline static int32_t get_offset_of_emptiableComputed_5() { return static_cast<int32_t>(offsetof(XsdChoiceValidationState_t6A72354CC424E101B3DB7472BBD8ADA7F61ACFC2, ___emptiableComputed_5)); }
	inline bool get_emptiableComputed_5() const { return ___emptiableComputed_5; }
	inline bool* get_address_of_emptiableComputed_5() { return &___emptiableComputed_5; }
	inline void set_emptiableComputed_5(bool value)
	{
		___emptiableComputed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDCHOICEVALIDATIONSTATE_T6A72354CC424E101B3DB7472BBD8ADA7F61ACFC2_H
#ifndef XSDELEMENTVALIDATIONSTATE_TE3F398F18D4968EA5F91FEE92C646B19389C1C13_H
#define XSDELEMENTVALIDATIONSTATE_TE3F398F18D4968EA5F91FEE92C646B19389C1C13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdElementValidationState
struct  XsdElementValidationState_tE3F398F18D4968EA5F91FEE92C646B19389C1C13  : public XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74
{
public:
	// System.Xml.Schema.XmlSchemaElement Mono.Xml.Schema.XsdElementValidationState::element
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 * ___element_3;

public:
	inline static int32_t get_offset_of_element_3() { return static_cast<int32_t>(offsetof(XsdElementValidationState_tE3F398F18D4968EA5F91FEE92C646B19389C1C13, ___element_3)); }
	inline XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 * get_element_3() const { return ___element_3; }
	inline XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 ** get_address_of_element_3() { return &___element_3; }
	inline void set_element_3(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 * value)
	{
		___element_3 = value;
		Il2CppCodeGenWriteBarrier((&___element_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDELEMENTVALIDATIONSTATE_TE3F398F18D4968EA5F91FEE92C646B19389C1C13_H
#ifndef XSDEMPTYVALIDATIONSTATE_T9F4AC1CC30BFB15E1B14F72386E85F2E78EFD045_H
#define XSDEMPTYVALIDATIONSTATE_T9F4AC1CC30BFB15E1B14F72386E85F2E78EFD045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdEmptyValidationState
struct  XsdEmptyValidationState_t9F4AC1CC30BFB15E1B14F72386E85F2E78EFD045  : public XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDEMPTYVALIDATIONSTATE_T9F4AC1CC30BFB15E1B14F72386E85F2E78EFD045_H
#ifndef XSDINVALIDVALIDATIONSTATE_T3532998F32C0C00F8E0F0C199D2308C867084FFF_H
#define XSDINVALIDVALIDATIONSTATE_T3532998F32C0C00F8E0F0C199D2308C867084FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdInvalidValidationState
struct  XsdInvalidValidationState_t3532998F32C0C00F8E0F0C199D2308C867084FFF  : public XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDINVALIDVALIDATIONSTATE_T3532998F32C0C00F8E0F0C199D2308C867084FFF_H
#ifndef XSDKEYENTRYCOLLECTION_TF37F133E46ECFF1F08CBB56894E1B18CB931B265_H
#define XSDKEYENTRYCOLLECTION_TF37F133E46ECFF1F08CBB56894E1B18CB931B265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdKeyEntryCollection
struct  XsdKeyEntryCollection_tF37F133E46ECFF1F08CBB56894E1B18CB931B265  : public CollectionBase_tC3F24120B23471E7FDEE72107D1D541E6456744F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDKEYENTRYCOLLECTION_TF37F133E46ECFF1F08CBB56894E1B18CB931B265_H
#ifndef XSDKEYENTRYFIELDCOLLECTION_T8536E011BAA035EF7757EE7BBE9843160ABBF4EC_H
#define XSDKEYENTRYFIELDCOLLECTION_T8536E011BAA035EF7757EE7BBE9843160ABBF4EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdKeyEntryFieldCollection
struct  XsdKeyEntryFieldCollection_t8536E011BAA035EF7757EE7BBE9843160ABBF4EC  : public CollectionBase_tC3F24120B23471E7FDEE72107D1D541E6456744F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDKEYENTRYFIELDCOLLECTION_T8536E011BAA035EF7757EE7BBE9843160ABBF4EC_H
#ifndef XSDSEQUENCEVALIDATIONSTATE_TB6D68439C7FBDA9D3510C267EF0038D735D6658E_H
#define XSDSEQUENCEVALIDATIONSTATE_TB6D68439C7FBDA9D3510C267EF0038D735D6658E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdSequenceValidationState
struct  XsdSequenceValidationState_tB6D68439C7FBDA9D3510C267EF0038D735D6658E  : public XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74
{
public:
	// System.Xml.Schema.XmlSchemaSequence Mono.Xml.Schema.XsdSequenceValidationState::seq
	XmlSchemaSequence_tC5A4D3A17AC43E34770AC810A3DDAFBC0D0C9483 * ___seq_3;
	// System.Int32 Mono.Xml.Schema.XsdSequenceValidationState::current
	int32_t ___current_4;
	// Mono.Xml.Schema.XsdValidationState Mono.Xml.Schema.XsdSequenceValidationState::currentAutomata
	XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 * ___currentAutomata_5;
	// System.Boolean Mono.Xml.Schema.XsdSequenceValidationState::emptiable
	bool ___emptiable_6;

public:
	inline static int32_t get_offset_of_seq_3() { return static_cast<int32_t>(offsetof(XsdSequenceValidationState_tB6D68439C7FBDA9D3510C267EF0038D735D6658E, ___seq_3)); }
	inline XmlSchemaSequence_tC5A4D3A17AC43E34770AC810A3DDAFBC0D0C9483 * get_seq_3() const { return ___seq_3; }
	inline XmlSchemaSequence_tC5A4D3A17AC43E34770AC810A3DDAFBC0D0C9483 ** get_address_of_seq_3() { return &___seq_3; }
	inline void set_seq_3(XmlSchemaSequence_tC5A4D3A17AC43E34770AC810A3DDAFBC0D0C9483 * value)
	{
		___seq_3 = value;
		Il2CppCodeGenWriteBarrier((&___seq_3), value);
	}

	inline static int32_t get_offset_of_current_4() { return static_cast<int32_t>(offsetof(XsdSequenceValidationState_tB6D68439C7FBDA9D3510C267EF0038D735D6658E, ___current_4)); }
	inline int32_t get_current_4() const { return ___current_4; }
	inline int32_t* get_address_of_current_4() { return &___current_4; }
	inline void set_current_4(int32_t value)
	{
		___current_4 = value;
	}

	inline static int32_t get_offset_of_currentAutomata_5() { return static_cast<int32_t>(offsetof(XsdSequenceValidationState_tB6D68439C7FBDA9D3510C267EF0038D735D6658E, ___currentAutomata_5)); }
	inline XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 * get_currentAutomata_5() const { return ___currentAutomata_5; }
	inline XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 ** get_address_of_currentAutomata_5() { return &___currentAutomata_5; }
	inline void set_currentAutomata_5(XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 * value)
	{
		___currentAutomata_5 = value;
		Il2CppCodeGenWriteBarrier((&___currentAutomata_5), value);
	}

	inline static int32_t get_offset_of_emptiable_6() { return static_cast<int32_t>(offsetof(XsdSequenceValidationState_tB6D68439C7FBDA9D3510C267EF0038D735D6658E, ___emptiable_6)); }
	inline bool get_emptiable_6() const { return ___emptiable_6; }
	inline bool* get_address_of_emptiable_6() { return &___emptiable_6; }
	inline void set_emptiable_6(bool value)
	{
		___emptiable_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDSEQUENCEVALIDATIONSTATE_TB6D68439C7FBDA9D3510C267EF0038D735D6658E_H
#ifndef XMLFILTERREADER_T3FFE2DE5E14FCE552BB0914DFEB6E0A1467B22F0_H
#define XMLFILTERREADER_T3FFE2DE5E14FCE552BB0914DFEB6E0A1467B22F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.XmlFilterReader
struct  XmlFilterReader_t3FFE2DE5E14FCE552BB0914DFEB6E0A1467B22F0  : public XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293
{
public:
	// System.Xml.XmlReader Mono.Xml.XmlFilterReader::reader
	XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * ___reader_2;
	// System.Xml.XmlReaderSettings Mono.Xml.XmlFilterReader::settings
	XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB * ___settings_3;
	// System.Xml.IXmlLineInfo Mono.Xml.XmlFilterReader::lineInfo
	RuntimeObject* ___lineInfo_4;

public:
	inline static int32_t get_offset_of_reader_2() { return static_cast<int32_t>(offsetof(XmlFilterReader_t3FFE2DE5E14FCE552BB0914DFEB6E0A1467B22F0, ___reader_2)); }
	inline XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * get_reader_2() const { return ___reader_2; }
	inline XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 ** get_address_of_reader_2() { return &___reader_2; }
	inline void set_reader_2(XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * value)
	{
		___reader_2 = value;
		Il2CppCodeGenWriteBarrier((&___reader_2), value);
	}

	inline static int32_t get_offset_of_settings_3() { return static_cast<int32_t>(offsetof(XmlFilterReader_t3FFE2DE5E14FCE552BB0914DFEB6E0A1467B22F0, ___settings_3)); }
	inline XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB * get_settings_3() const { return ___settings_3; }
	inline XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB ** get_address_of_settings_3() { return &___settings_3; }
	inline void set_settings_3(XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB * value)
	{
		___settings_3 = value;
		Il2CppCodeGenWriteBarrier((&___settings_3), value);
	}

	inline static int32_t get_offset_of_lineInfo_4() { return static_cast<int32_t>(offsetof(XmlFilterReader_t3FFE2DE5E14FCE552BB0914DFEB6E0A1467B22F0, ___lineInfo_4)); }
	inline RuntimeObject* get_lineInfo_4() const { return ___lineInfo_4; }
	inline RuntimeObject** get_address_of_lineInfo_4() { return &___lineInfo_4; }
	inline void set_lineInfo_4(RuntimeObject* value)
	{
		___lineInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___lineInfo_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLFILTERREADER_T3FFE2DE5E14FCE552BB0914DFEB6E0A1467B22F0_H
#ifndef ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#define ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF  : public ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C
{
public:

public:
};

struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_com
{
};
#endif // ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifndef MONOTODOATTRIBUTE_T2D21BFE6EE9459D34164629D1D797DBB606A57A0_H
#define MONOTODOATTRIBUTE_T2D21BFE6EE9459D34164629D1D797DBB606A57A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MonoTODOAttribute
struct  MonoTODOAttribute_t2D21BFE6EE9459D34164629D1D797DBB606A57A0  : public Attribute_t60F25EB48D5935E4C6C2BAF7F90F57A43528E469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTODOATTRIBUTE_T2D21BFE6EE9459D34164629D1D797DBB606A57A0_H
#ifndef U3CENUMERATEU3ED__10_T76D57E352E6F38113430F0395804829B504236AA_H
#define U3CENUMERATEU3ED__10_T76D57E352E6F38113430F0395804829B504236AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.FixedBitArray3/<Enumerate>d__10
struct  U3CEnumerateU3Ed__10_t76D57E352E6F38113430F0395804829B504236AA  : public RuntimeObject
{
public:
	// System.Int32 Kolibri2d.Poly2Tri.FixedBitArray3/<Enumerate>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Boolean Kolibri2d.Poly2Tri.FixedBitArray3/<Enumerate>d__10::<>2__current
	bool ___U3CU3E2__current_1;
	// System.Int32 Kolibri2d.Poly2Tri.FixedBitArray3/<Enumerate>d__10::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Kolibri2d.Poly2Tri.FixedBitArray3 Kolibri2d.Poly2Tri.FixedBitArray3/<Enumerate>d__10::<>4__this
	FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D  ___U3CU3E4__this_3;
	// Kolibri2d.Poly2Tri.FixedBitArray3 Kolibri2d.Poly2Tri.FixedBitArray3/<Enumerate>d__10::<>3__<>4__this
	FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D  ___U3CU3E3__U3CU3E4__this_4;
	// System.Int32 Kolibri2d.Poly2Tri.FixedBitArray3/<Enumerate>d__10::<i>5__2
	int32_t ___U3CiU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CEnumerateU3Ed__10_t76D57E352E6F38113430F0395804829B504236AA, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CEnumerateU3Ed__10_t76D57E352E6F38113430F0395804829B504236AA, ___U3CU3E2__current_1)); }
	inline bool get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline bool* get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(bool value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CEnumerateU3Ed__10_t76D57E352E6F38113430F0395804829B504236AA, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CEnumerateU3Ed__10_t76D57E352E6F38113430F0395804829B504236AA, ___U3CU3E4__this_3)); }
	inline FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D  get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D * get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D  value)
	{
		___U3CU3E4__this_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CEnumerateU3Ed__10_t76D57E352E6F38113430F0395804829B504236AA, ___U3CU3E3__U3CU3E4__this_4)); }
	inline FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D  get_U3CU3E3__U3CU3E4__this_4() const { return ___U3CU3E3__U3CU3E4__this_4; }
	inline FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D * get_address_of_U3CU3E3__U3CU3E4__this_4() { return &___U3CU3E3__U3CU3E4__this_4; }
	inline void set_U3CU3E3__U3CU3E4__this_4(FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D  value)
	{
		___U3CU3E3__U3CU3E4__this_4 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CEnumerateU3Ed__10_t76D57E352E6F38113430F0395804829B504236AA, ___U3CiU3E5__2_5)); }
	inline int32_t get_U3CiU3E5__2_5() const { return ___U3CiU3E5__2_5; }
	inline int32_t* get_address_of_U3CiU3E5__2_5() { return &___U3CiU3E5__2_5; }
	inline void set_U3CiU3E5__2_5(int32_t value)
	{
		___U3CiU3E5__2_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CENUMERATEU3ED__10_T76D57E352E6F38113430F0395804829B504236AA_H
#ifndef WINDINGORDERTYPE_TF976AF91C062E008942E81B1895115A047A1CB0A_H
#define WINDINGORDERTYPE_TF976AF91C062E008942E81B1895115A047A1CB0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.Point2DList/WindingOrderType
struct  WindingOrderType_tF976AF91C062E008942E81B1895115A047A1CB0A 
{
public:
	// System.Int32 Kolibri2d.Poly2Tri.Point2DList/WindingOrderType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WindingOrderType_tF976AF91C062E008942E81B1895115A047A1CB0A, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDINGORDERTYPE_TF976AF91C062E008942E81B1895115A047A1CB0A_H
#ifndef POLYGONPOINT_T94267F9D8AC5851730C1D804609D51ABC86911E4_H
#define POLYGONPOINT_T94267F9D8AC5851730C1D804609D51ABC86911E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.PolygonPoint
struct  PolygonPoint_t94267F9D8AC5851730C1D804609D51ABC86911E4  : public TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E
{
public:
	// Kolibri2d.Poly2Tri.PolygonPoint Kolibri2d.Poly2Tri.PolygonPoint::<Next>k__BackingField
	PolygonPoint_t94267F9D8AC5851730C1D804609D51ABC86911E4 * ___U3CNextU3Ek__BackingField_5;
	// Kolibri2d.Poly2Tri.PolygonPoint Kolibri2d.Poly2Tri.PolygonPoint::<Previous>k__BackingField
	PolygonPoint_t94267F9D8AC5851730C1D804609D51ABC86911E4 * ___U3CPreviousU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CNextU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PolygonPoint_t94267F9D8AC5851730C1D804609D51ABC86911E4, ___U3CNextU3Ek__BackingField_5)); }
	inline PolygonPoint_t94267F9D8AC5851730C1D804609D51ABC86911E4 * get_U3CNextU3Ek__BackingField_5() const { return ___U3CNextU3Ek__BackingField_5; }
	inline PolygonPoint_t94267F9D8AC5851730C1D804609D51ABC86911E4 ** get_address_of_U3CNextU3Ek__BackingField_5() { return &___U3CNextU3Ek__BackingField_5; }
	inline void set_U3CNextU3Ek__BackingField_5(PolygonPoint_t94267F9D8AC5851730C1D804609D51ABC86911E4 * value)
	{
		___U3CNextU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNextU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CPreviousU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PolygonPoint_t94267F9D8AC5851730C1D804609D51ABC86911E4, ___U3CPreviousU3Ek__BackingField_6)); }
	inline PolygonPoint_t94267F9D8AC5851730C1D804609D51ABC86911E4 * get_U3CPreviousU3Ek__BackingField_6() const { return ___U3CPreviousU3Ek__BackingField_6; }
	inline PolygonPoint_t94267F9D8AC5851730C1D804609D51ABC86911E4 ** get_address_of_U3CPreviousU3Ek__BackingField_6() { return &___U3CPreviousU3Ek__BackingField_6; }
	inline void set_U3CPreviousU3Ek__BackingField_6(PolygonPoint_t94267F9D8AC5851730C1D804609D51ABC86911E4 * value)
	{
		___U3CPreviousU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPreviousU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGONPOINT_T94267F9D8AC5851730C1D804609D51ABC86911E4_H
#ifndef TRIANGULATIONMODE_TF47A940AB02AFB9DC5163BB1B9AE1A85CD37764C_H
#define TRIANGULATIONMODE_TF47A940AB02AFB9DC5163BB1B9AE1A85CD37764C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.TriangulationMode
struct  TriangulationMode_tF47A940AB02AFB9DC5163BB1B9AE1A85CD37764C 
{
public:
	// System.Int32 Kolibri2d.Poly2Tri.TriangulationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriangulationMode_tF47A940AB02AFB9DC5163BB1B9AE1A85CD37764C, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATIONMODE_TF47A940AB02AFB9DC5163BB1B9AE1A85CD37764C_H
#ifndef XSDORDERING_T27E88D8D2082F10D686D9004720EBEA75EAA05E8_H
#define XSDORDERING_T27E88D8D2082F10D686D9004720EBEA75EAA05E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdOrdering
struct  XsdOrdering_t27E88D8D2082F10D686D9004720EBEA75EAA05E8 
{
public:
	// System.Int32 Mono.Xml.Schema.XsdOrdering::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XsdOrdering_t27E88D8D2082F10D686D9004720EBEA75EAA05E8, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDORDERING_T27E88D8D2082F10D686D9004720EBEA75EAA05E8_H
#ifndef XSDWHITESPACEFACET_TCADC752FD2332AC9C1304C86653374FF61C0B91A_H
#define XSDWHITESPACEFACET_TCADC752FD2332AC9C1304C86653374FF61C0B91A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdWhitespaceFacet
struct  XsdWhitespaceFacet_tCADC752FD2332AC9C1304C86653374FF61C0B91A 
{
public:
	// System.Int32 Mono.Xml.Schema.XsdWhitespaceFacet::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XsdWhitespaceFacet_tCADC752FD2332AC9C1304C86653374FF61C0B91A, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDWHITESPACEFACET_TCADC752FD2332AC9C1304C86653374FF61C0B91A_H
#ifndef XMLSCHEMACONTENTPROCESSING_T392DB8F98BDBDD400CD2370DE517690F99622330_H
#define XMLSCHEMACONTENTPROCESSING_T392DB8F98BDBDD400CD2370DE517690F99622330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaContentProcessing
struct  XmlSchemaContentProcessing_t392DB8F98BDBDD400CD2370DE517690F99622330 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaContentProcessing::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaContentProcessing_t392DB8F98BDBDD400CD2370DE517690F99622330, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACONTENTPROCESSING_T392DB8F98BDBDD400CD2370DE517690F99622330_H
#ifndef FACET_TC3A3E144DFA7F8AED31FB1073E83B5EF9F75A68B_H
#define FACET_TC3A3E144DFA7F8AED31FB1073E83B5EF9F75A68B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaFacet/Facet
struct  Facet_tC3A3E144DFA7F8AED31FB1073E83B5EF9F75A68B 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaFacet/Facet::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Facet_tC3A3E144DFA7F8AED31FB1073E83B5EF9F75A68B, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACET_TC3A3E144DFA7F8AED31FB1073E83B5EF9F75A68B_H
#ifndef XMLSCHEMAVALIDATIONFLAGS_TECE1A6138368A7455AC47A24FC5FD4920CDB8ACA_H
#define XMLSCHEMAVALIDATIONFLAGS_TECE1A6138368A7455AC47A24FC5FD4920CDB8ACA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaValidationFlags
struct  XmlSchemaValidationFlags_tECE1A6138368A7455AC47A24FC5FD4920CDB8ACA 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaValidationFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaValidationFlags_tECE1A6138368A7455AC47A24FC5FD4920CDB8ACA, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAVALIDATIONFLAGS_TECE1A6138368A7455AC47A24FC5FD4920CDB8ACA_H
#ifndef VALIDATIONTYPE_T31525A080D050BCFADE54364812432A4C5BDBE63_H
#define VALIDATIONTYPE_T31525A080D050BCFADE54364812432A4C5BDBE63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ValidationType
struct  ValidationType_t31525A080D050BCFADE54364812432A4C5BDBE63 
{
public:
	// System.Int32 System.Xml.ValidationType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ValidationType_t31525A080D050BCFADE54364812432A4C5BDBE63, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONTYPE_T31525A080D050BCFADE54364812432A4C5BDBE63_H
#ifndef POINT2DLIST_T1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0_H
#define POINT2DLIST_T1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.Point2DList
struct  Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Kolibri2d.Poly2Tri.Point2D> Kolibri2d.Poly2Tri.Point2DList::mPoints
	List_1_tFD3B51F91BAFEAD7A39B0EF917EC5B58423E32AE * ___mPoints_3;
	// Kolibri2d.Poly2Tri.Rect2D Kolibri2d.Poly2Tri.Point2DList::mBoundingBox
	Rect2D_t337FF4CBB07E16CB6E029205BF01F1984DDE5353 * ___mBoundingBox_4;
	// Kolibri2d.Poly2Tri.Point2DList/WindingOrderType Kolibri2d.Poly2Tri.Point2DList::mWindingOrder
	int32_t ___mWindingOrder_5;
	// System.Double Kolibri2d.Poly2Tri.Point2DList::mEpsilon
	double ___mEpsilon_6;

public:
	inline static int32_t get_offset_of_mPoints_3() { return static_cast<int32_t>(offsetof(Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0, ___mPoints_3)); }
	inline List_1_tFD3B51F91BAFEAD7A39B0EF917EC5B58423E32AE * get_mPoints_3() const { return ___mPoints_3; }
	inline List_1_tFD3B51F91BAFEAD7A39B0EF917EC5B58423E32AE ** get_address_of_mPoints_3() { return &___mPoints_3; }
	inline void set_mPoints_3(List_1_tFD3B51F91BAFEAD7A39B0EF917EC5B58423E32AE * value)
	{
		___mPoints_3 = value;
		Il2CppCodeGenWriteBarrier((&___mPoints_3), value);
	}

	inline static int32_t get_offset_of_mBoundingBox_4() { return static_cast<int32_t>(offsetof(Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0, ___mBoundingBox_4)); }
	inline Rect2D_t337FF4CBB07E16CB6E029205BF01F1984DDE5353 * get_mBoundingBox_4() const { return ___mBoundingBox_4; }
	inline Rect2D_t337FF4CBB07E16CB6E029205BF01F1984DDE5353 ** get_address_of_mBoundingBox_4() { return &___mBoundingBox_4; }
	inline void set_mBoundingBox_4(Rect2D_t337FF4CBB07E16CB6E029205BF01F1984DDE5353 * value)
	{
		___mBoundingBox_4 = value;
		Il2CppCodeGenWriteBarrier((&___mBoundingBox_4), value);
	}

	inline static int32_t get_offset_of_mWindingOrder_5() { return static_cast<int32_t>(offsetof(Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0, ___mWindingOrder_5)); }
	inline int32_t get_mWindingOrder_5() const { return ___mWindingOrder_5; }
	inline int32_t* get_address_of_mWindingOrder_5() { return &___mWindingOrder_5; }
	inline void set_mWindingOrder_5(int32_t value)
	{
		___mWindingOrder_5 = value;
	}

	inline static int32_t get_offset_of_mEpsilon_6() { return static_cast<int32_t>(offsetof(Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0, ___mEpsilon_6)); }
	inline double get_mEpsilon_6() const { return ___mEpsilon_6; }
	inline double* get_address_of_mEpsilon_6() { return &___mEpsilon_6; }
	inline void set_mEpsilon_6(double value)
	{
		___mEpsilon_6 = value;
	}
};

struct Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0_StaticFields
{
public:
	// System.Int32 Kolibri2d.Poly2Tri.Point2DList::kMaxPolygonVertices
	int32_t ___kMaxPolygonVertices_0;
	// System.Double Kolibri2d.Poly2Tri.Point2DList::kLinearSlop
	double ___kLinearSlop_1;
	// System.Double Kolibri2d.Poly2Tri.Point2DList::kAngularSlop
	double ___kAngularSlop_2;

public:
	inline static int32_t get_offset_of_kMaxPolygonVertices_0() { return static_cast<int32_t>(offsetof(Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0_StaticFields, ___kMaxPolygonVertices_0)); }
	inline int32_t get_kMaxPolygonVertices_0() const { return ___kMaxPolygonVertices_0; }
	inline int32_t* get_address_of_kMaxPolygonVertices_0() { return &___kMaxPolygonVertices_0; }
	inline void set_kMaxPolygonVertices_0(int32_t value)
	{
		___kMaxPolygonVertices_0 = value;
	}

	inline static int32_t get_offset_of_kLinearSlop_1() { return static_cast<int32_t>(offsetof(Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0_StaticFields, ___kLinearSlop_1)); }
	inline double get_kLinearSlop_1() const { return ___kLinearSlop_1; }
	inline double* get_address_of_kLinearSlop_1() { return &___kLinearSlop_1; }
	inline void set_kLinearSlop_1(double value)
	{
		___kLinearSlop_1 = value;
	}

	inline static int32_t get_offset_of_kAngularSlop_2() { return static_cast<int32_t>(offsetof(Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0_StaticFields, ___kAngularSlop_2)); }
	inline double get_kAngularSlop_2() const { return ___kAngularSlop_2; }
	inline double* get_address_of_kAngularSlop_2() { return &___kAngularSlop_2; }
	inline void set_kAngularSlop_2(double value)
	{
		___kAngularSlop_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINT2DLIST_T1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0_H
#ifndef TRIANGULATIONCONTEXT_TC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B_H
#define TRIANGULATIONCONTEXT_TC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.TriangulationContext
struct  TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B  : public RuntimeObject
{
public:
	// Kolibri2d.Poly2Tri.TriangulationDebugContext Kolibri2d.Poly2Tri.TriangulationContext::<DebugContext>k__BackingField
	TriangulationDebugContext_t23D041A57AAB2224970A201625386E8BEB37D9A4 * ___U3CDebugContextU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<Kolibri2d.Poly2Tri.DelaunayTriangle> Kolibri2d.Poly2Tri.TriangulationContext::Triangles
	List_1_tA161C1D023FB6D4167CAE9ED2A7AB1554978B9D9 * ___Triangles_1;
	// System.Collections.Generic.List`1<Kolibri2d.Poly2Tri.TriangulationPoint> Kolibri2d.Poly2Tri.TriangulationContext::Points
	List_1_tAAF0D3178D082F2377FDDDC38ED8BC5C83A1FF81 * ___Points_2;
	// Kolibri2d.Poly2Tri.TriangulationMode Kolibri2d.Poly2Tri.TriangulationContext::<TriangulationMode>k__BackingField
	int32_t ___U3CTriangulationModeU3Ek__BackingField_3;
	// Kolibri2d.Poly2Tri.ITriangulatable Kolibri2d.Poly2Tri.TriangulationContext::<Triangulatable>k__BackingField
	RuntimeObject* ___U3CTriangulatableU3Ek__BackingField_4;
	// System.Int32 Kolibri2d.Poly2Tri.TriangulationContext::<StepCount>k__BackingField
	int32_t ___U3CStepCountU3Ek__BackingField_5;
	// System.Boolean Kolibri2d.Poly2Tri.TriangulationContext::<IsDebugEnabled>k__BackingField
	bool ___U3CIsDebugEnabledU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CDebugContextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B, ___U3CDebugContextU3Ek__BackingField_0)); }
	inline TriangulationDebugContext_t23D041A57AAB2224970A201625386E8BEB37D9A4 * get_U3CDebugContextU3Ek__BackingField_0() const { return ___U3CDebugContextU3Ek__BackingField_0; }
	inline TriangulationDebugContext_t23D041A57AAB2224970A201625386E8BEB37D9A4 ** get_address_of_U3CDebugContextU3Ek__BackingField_0() { return &___U3CDebugContextU3Ek__BackingField_0; }
	inline void set_U3CDebugContextU3Ek__BackingField_0(TriangulationDebugContext_t23D041A57AAB2224970A201625386E8BEB37D9A4 * value)
	{
		___U3CDebugContextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDebugContextU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_Triangles_1() { return static_cast<int32_t>(offsetof(TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B, ___Triangles_1)); }
	inline List_1_tA161C1D023FB6D4167CAE9ED2A7AB1554978B9D9 * get_Triangles_1() const { return ___Triangles_1; }
	inline List_1_tA161C1D023FB6D4167CAE9ED2A7AB1554978B9D9 ** get_address_of_Triangles_1() { return &___Triangles_1; }
	inline void set_Triangles_1(List_1_tA161C1D023FB6D4167CAE9ED2A7AB1554978B9D9 * value)
	{
		___Triangles_1 = value;
		Il2CppCodeGenWriteBarrier((&___Triangles_1), value);
	}

	inline static int32_t get_offset_of_Points_2() { return static_cast<int32_t>(offsetof(TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B, ___Points_2)); }
	inline List_1_tAAF0D3178D082F2377FDDDC38ED8BC5C83A1FF81 * get_Points_2() const { return ___Points_2; }
	inline List_1_tAAF0D3178D082F2377FDDDC38ED8BC5C83A1FF81 ** get_address_of_Points_2() { return &___Points_2; }
	inline void set_Points_2(List_1_tAAF0D3178D082F2377FDDDC38ED8BC5C83A1FF81 * value)
	{
		___Points_2 = value;
		Il2CppCodeGenWriteBarrier((&___Points_2), value);
	}

	inline static int32_t get_offset_of_U3CTriangulationModeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B, ___U3CTriangulationModeU3Ek__BackingField_3)); }
	inline int32_t get_U3CTriangulationModeU3Ek__BackingField_3() const { return ___U3CTriangulationModeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CTriangulationModeU3Ek__BackingField_3() { return &___U3CTriangulationModeU3Ek__BackingField_3; }
	inline void set_U3CTriangulationModeU3Ek__BackingField_3(int32_t value)
	{
		___U3CTriangulationModeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CTriangulatableU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B, ___U3CTriangulatableU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CTriangulatableU3Ek__BackingField_4() const { return ___U3CTriangulatableU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CTriangulatableU3Ek__BackingField_4() { return &___U3CTriangulatableU3Ek__BackingField_4; }
	inline void set_U3CTriangulatableU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CTriangulatableU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTriangulatableU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CStepCountU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B, ___U3CStepCountU3Ek__BackingField_5)); }
	inline int32_t get_U3CStepCountU3Ek__BackingField_5() const { return ___U3CStepCountU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CStepCountU3Ek__BackingField_5() { return &___U3CStepCountU3Ek__BackingField_5; }
	inline void set_U3CStepCountU3Ek__BackingField_5(int32_t value)
	{
		___U3CStepCountU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CIsDebugEnabledU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B, ___U3CIsDebugEnabledU3Ek__BackingField_6)); }
	inline bool get_U3CIsDebugEnabledU3Ek__BackingField_6() const { return ___U3CIsDebugEnabledU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CIsDebugEnabledU3Ek__BackingField_6() { return &___U3CIsDebugEnabledU3Ek__BackingField_6; }
	inline void set_U3CIsDebugEnabledU3Ek__BackingField_6(bool value)
	{
		___U3CIsDebugEnabledU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATIONCONTEXT_TC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B_H
#ifndef XMLSCHEMAVALIDATINGREADER_T3C8E4816133B5AD717E222C6FE5CD6D2F2929B08_H
#define XMLSCHEMAVALIDATINGREADER_T3C8E4816133B5AD717E222C6FE5CD6D2F2929B08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XmlSchemaValidatingReader
struct  XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08  : public XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293
{
public:
	// System.Xml.XmlReader Mono.Xml.Schema.XmlSchemaValidatingReader::reader
	XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * ___reader_3;
	// System.Xml.Schema.XmlSchemaValidationFlags Mono.Xml.Schema.XmlSchemaValidatingReader::options
	int32_t ___options_4;
	// System.Xml.Schema.XmlSchemaValidator Mono.Xml.Schema.XmlSchemaValidatingReader::v
	XmlSchemaValidator_t35307305B4F219F9884E731DFE828451623423A2 * ___v_5;
	// System.Xml.Schema.XmlValueGetter Mono.Xml.Schema.XmlSchemaValidatingReader::getter
	XmlValueGetter_tA66DE0B622718F2997C0BC6CA5967B1E5EC8DA22 * ___getter_6;
	// System.Xml.Schema.XmlSchemaInfo Mono.Xml.Schema.XmlSchemaValidatingReader::xsinfo
	XmlSchemaInfo_t14DC1A0ED52BD1B9A2C0EC58D00D4C01D254795C * ___xsinfo_7;
	// System.Xml.IXmlLineInfo Mono.Xml.Schema.XmlSchemaValidatingReader::readerLineInfo
	RuntimeObject* ___readerLineInfo_8;
	// System.Xml.IXmlNamespaceResolver Mono.Xml.Schema.XmlSchemaValidatingReader::nsResolver
	RuntimeObject* ___nsResolver_9;
	// System.Xml.Schema.XmlSchemaAttribute[] Mono.Xml.Schema.XmlSchemaValidatingReader::defaultAttributes
	XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95* ___defaultAttributes_10;
	// System.Int32 Mono.Xml.Schema.XmlSchemaValidatingReader::currentDefaultAttribute
	int32_t ___currentDefaultAttribute_11;
	// System.Collections.ArrayList Mono.Xml.Schema.XmlSchemaValidatingReader::defaultAttributesCache
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___defaultAttributesCache_12;
	// System.Boolean Mono.Xml.Schema.XmlSchemaValidatingReader::defaultAttributeConsumed
	bool ___defaultAttributeConsumed_13;
	// System.Xml.Schema.XmlSchemaType Mono.Xml.Schema.XmlSchemaValidatingReader::currentAttrType
	XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE * ___currentAttrType_14;
	// System.Boolean Mono.Xml.Schema.XmlSchemaValidatingReader::validationDone
	bool ___validationDone_15;
	// System.Xml.Schema.XmlSchemaElement Mono.Xml.Schema.XmlSchemaValidatingReader::element
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 * ___element_16;

public:
	inline static int32_t get_offset_of_reader_3() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08, ___reader_3)); }
	inline XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * get_reader_3() const { return ___reader_3; }
	inline XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 ** get_address_of_reader_3() { return &___reader_3; }
	inline void set_reader_3(XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * value)
	{
		___reader_3 = value;
		Il2CppCodeGenWriteBarrier((&___reader_3), value);
	}

	inline static int32_t get_offset_of_options_4() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08, ___options_4)); }
	inline int32_t get_options_4() const { return ___options_4; }
	inline int32_t* get_address_of_options_4() { return &___options_4; }
	inline void set_options_4(int32_t value)
	{
		___options_4 = value;
	}

	inline static int32_t get_offset_of_v_5() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08, ___v_5)); }
	inline XmlSchemaValidator_t35307305B4F219F9884E731DFE828451623423A2 * get_v_5() const { return ___v_5; }
	inline XmlSchemaValidator_t35307305B4F219F9884E731DFE828451623423A2 ** get_address_of_v_5() { return &___v_5; }
	inline void set_v_5(XmlSchemaValidator_t35307305B4F219F9884E731DFE828451623423A2 * value)
	{
		___v_5 = value;
		Il2CppCodeGenWriteBarrier((&___v_5), value);
	}

	inline static int32_t get_offset_of_getter_6() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08, ___getter_6)); }
	inline XmlValueGetter_tA66DE0B622718F2997C0BC6CA5967B1E5EC8DA22 * get_getter_6() const { return ___getter_6; }
	inline XmlValueGetter_tA66DE0B622718F2997C0BC6CA5967B1E5EC8DA22 ** get_address_of_getter_6() { return &___getter_6; }
	inline void set_getter_6(XmlValueGetter_tA66DE0B622718F2997C0BC6CA5967B1E5EC8DA22 * value)
	{
		___getter_6 = value;
		Il2CppCodeGenWriteBarrier((&___getter_6), value);
	}

	inline static int32_t get_offset_of_xsinfo_7() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08, ___xsinfo_7)); }
	inline XmlSchemaInfo_t14DC1A0ED52BD1B9A2C0EC58D00D4C01D254795C * get_xsinfo_7() const { return ___xsinfo_7; }
	inline XmlSchemaInfo_t14DC1A0ED52BD1B9A2C0EC58D00D4C01D254795C ** get_address_of_xsinfo_7() { return &___xsinfo_7; }
	inline void set_xsinfo_7(XmlSchemaInfo_t14DC1A0ED52BD1B9A2C0EC58D00D4C01D254795C * value)
	{
		___xsinfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___xsinfo_7), value);
	}

	inline static int32_t get_offset_of_readerLineInfo_8() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08, ___readerLineInfo_8)); }
	inline RuntimeObject* get_readerLineInfo_8() const { return ___readerLineInfo_8; }
	inline RuntimeObject** get_address_of_readerLineInfo_8() { return &___readerLineInfo_8; }
	inline void set_readerLineInfo_8(RuntimeObject* value)
	{
		___readerLineInfo_8 = value;
		Il2CppCodeGenWriteBarrier((&___readerLineInfo_8), value);
	}

	inline static int32_t get_offset_of_nsResolver_9() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08, ___nsResolver_9)); }
	inline RuntimeObject* get_nsResolver_9() const { return ___nsResolver_9; }
	inline RuntimeObject** get_address_of_nsResolver_9() { return &___nsResolver_9; }
	inline void set_nsResolver_9(RuntimeObject* value)
	{
		___nsResolver_9 = value;
		Il2CppCodeGenWriteBarrier((&___nsResolver_9), value);
	}

	inline static int32_t get_offset_of_defaultAttributes_10() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08, ___defaultAttributes_10)); }
	inline XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95* get_defaultAttributes_10() const { return ___defaultAttributes_10; }
	inline XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95** get_address_of_defaultAttributes_10() { return &___defaultAttributes_10; }
	inline void set_defaultAttributes_10(XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95* value)
	{
		___defaultAttributes_10 = value;
		Il2CppCodeGenWriteBarrier((&___defaultAttributes_10), value);
	}

	inline static int32_t get_offset_of_currentDefaultAttribute_11() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08, ___currentDefaultAttribute_11)); }
	inline int32_t get_currentDefaultAttribute_11() const { return ___currentDefaultAttribute_11; }
	inline int32_t* get_address_of_currentDefaultAttribute_11() { return &___currentDefaultAttribute_11; }
	inline void set_currentDefaultAttribute_11(int32_t value)
	{
		___currentDefaultAttribute_11 = value;
	}

	inline static int32_t get_offset_of_defaultAttributesCache_12() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08, ___defaultAttributesCache_12)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_defaultAttributesCache_12() const { return ___defaultAttributesCache_12; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_defaultAttributesCache_12() { return &___defaultAttributesCache_12; }
	inline void set_defaultAttributesCache_12(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___defaultAttributesCache_12 = value;
		Il2CppCodeGenWriteBarrier((&___defaultAttributesCache_12), value);
	}

	inline static int32_t get_offset_of_defaultAttributeConsumed_13() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08, ___defaultAttributeConsumed_13)); }
	inline bool get_defaultAttributeConsumed_13() const { return ___defaultAttributeConsumed_13; }
	inline bool* get_address_of_defaultAttributeConsumed_13() { return &___defaultAttributeConsumed_13; }
	inline void set_defaultAttributeConsumed_13(bool value)
	{
		___defaultAttributeConsumed_13 = value;
	}

	inline static int32_t get_offset_of_currentAttrType_14() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08, ___currentAttrType_14)); }
	inline XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE * get_currentAttrType_14() const { return ___currentAttrType_14; }
	inline XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE ** get_address_of_currentAttrType_14() { return &___currentAttrType_14; }
	inline void set_currentAttrType_14(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE * value)
	{
		___currentAttrType_14 = value;
		Il2CppCodeGenWriteBarrier((&___currentAttrType_14), value);
	}

	inline static int32_t get_offset_of_validationDone_15() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08, ___validationDone_15)); }
	inline bool get_validationDone_15() const { return ___validationDone_15; }
	inline bool* get_address_of_validationDone_15() { return &___validationDone_15; }
	inline void set_validationDone_15(bool value)
	{
		___validationDone_15 = value;
	}

	inline static int32_t get_offset_of_element_16() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08, ___element_16)); }
	inline XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 * get_element_16() const { return ___element_16; }
	inline XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 ** get_address_of_element_16() { return &___element_16; }
	inline void set_element_16(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 * value)
	{
		___element_16 = value;
		Il2CppCodeGenWriteBarrier((&___element_16), value);
	}
};

struct XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaAttribute[] Mono.Xml.Schema.XmlSchemaValidatingReader::emptyAttributeArray
	XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95* ___emptyAttributeArray_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XmlSchemaValidatingReader::<>f__switch$map0
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map0_17;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XmlSchemaValidatingReader::<>f__switch$map1
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map1_18;

public:
	inline static int32_t get_offset_of_emptyAttributeArray_2() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08_StaticFields, ___emptyAttributeArray_2)); }
	inline XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95* get_emptyAttributeArray_2() const { return ___emptyAttributeArray_2; }
	inline XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95** get_address_of_emptyAttributeArray_2() { return &___emptyAttributeArray_2; }
	inline void set_emptyAttributeArray_2(XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95* value)
	{
		___emptyAttributeArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___emptyAttributeArray_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_17() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08_StaticFields, ___U3CU3Ef__switchU24map0_17)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map0_17() const { return ___U3CU3Ef__switchU24map0_17; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map0_17() { return &___U3CU3Ef__switchU24map0_17; }
	inline void set_U3CU3Ef__switchU24map0_17(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map0_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_17), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_18() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08_StaticFields, ___U3CU3Ef__switchU24map1_18)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map1_18() const { return ___U3CU3Ef__switchU24map1_18; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map1_18() { return &___U3CU3Ef__switchU24map1_18; }
	inline void set_U3CU3Ef__switchU24map1_18(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map1_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAVALIDATINGREADER_T3C8E4816133B5AD717E222C6FE5CD6D2F2929B08_H
#ifndef XSDPARTICLESTATEMANAGER_T9C730C7B40880A4D7409B129A8D041BD47438E68_H
#define XSDPARTICLESTATEMANAGER_T9C730C7B40880A4D7409B129A8D041BD47438E68_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdParticleStateManager
struct  XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68  : public RuntimeObject
{
public:
	// System.Collections.Hashtable Mono.Xml.Schema.XsdParticleStateManager::table
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___table_0;
	// System.Xml.Schema.XmlSchemaContentProcessing Mono.Xml.Schema.XsdParticleStateManager::processContents
	int32_t ___processContents_1;
	// System.Xml.Schema.XmlSchemaElement Mono.Xml.Schema.XsdParticleStateManager::CurrentElement
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 * ___CurrentElement_2;
	// System.Collections.Stack Mono.Xml.Schema.XsdParticleStateManager::ContextStack
	Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * ___ContextStack_3;
	// Mono.Xml.Schema.XsdValidationContext Mono.Xml.Schema.XsdParticleStateManager::Context
	XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD * ___Context_4;

public:
	inline static int32_t get_offset_of_table_0() { return static_cast<int32_t>(offsetof(XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68, ___table_0)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_table_0() const { return ___table_0; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_table_0() { return &___table_0; }
	inline void set_table_0(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___table_0 = value;
		Il2CppCodeGenWriteBarrier((&___table_0), value);
	}

	inline static int32_t get_offset_of_processContents_1() { return static_cast<int32_t>(offsetof(XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68, ___processContents_1)); }
	inline int32_t get_processContents_1() const { return ___processContents_1; }
	inline int32_t* get_address_of_processContents_1() { return &___processContents_1; }
	inline void set_processContents_1(int32_t value)
	{
		___processContents_1 = value;
	}

	inline static int32_t get_offset_of_CurrentElement_2() { return static_cast<int32_t>(offsetof(XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68, ___CurrentElement_2)); }
	inline XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 * get_CurrentElement_2() const { return ___CurrentElement_2; }
	inline XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 ** get_address_of_CurrentElement_2() { return &___CurrentElement_2; }
	inline void set_CurrentElement_2(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 * value)
	{
		___CurrentElement_2 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentElement_2), value);
	}

	inline static int32_t get_offset_of_ContextStack_3() { return static_cast<int32_t>(offsetof(XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68, ___ContextStack_3)); }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * get_ContextStack_3() const { return ___ContextStack_3; }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 ** get_address_of_ContextStack_3() { return &___ContextStack_3; }
	inline void set_ContextStack_3(Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * value)
	{
		___ContextStack_3 = value;
		Il2CppCodeGenWriteBarrier((&___ContextStack_3), value);
	}

	inline static int32_t get_offset_of_Context_4() { return static_cast<int32_t>(offsetof(XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68, ___Context_4)); }
	inline XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD * get_Context_4() const { return ___Context_4; }
	inline XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD ** get_address_of_Context_4() { return &___Context_4; }
	inline void set_Context_4(XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD * value)
	{
		___Context_4 = value;
		Il2CppCodeGenWriteBarrier((&___Context_4), value);
	}
};

struct XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XsdParticleStateManager::<>f__switch$map2
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map2_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2_5() { return static_cast<int32_t>(offsetof(XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68_StaticFields, ___U3CU3Ef__switchU24map2_5)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map2_5() const { return ___U3CU3Ef__switchU24map2_5; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map2_5() { return &___U3CU3Ef__switchU24map2_5; }
	inline void set_U3CU3Ef__switchU24map2_5(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDPARTICLESTATEMANAGER_T9C730C7B40880A4D7409B129A8D041BD47438E68_H
#ifndef XSDVALIDATINGREADER_T2C211F16BDE384F56FB73B293505DEADE5AFCEB3_H
#define XSDVALIDATINGREADER_T2C211F16BDE384F56FB73B293505DEADE5AFCEB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdValidatingReader
struct  XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3  : public XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293
{
public:
	// System.Xml.XmlReader Mono.Xml.Schema.XsdValidatingReader::reader
	XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * ___reader_3;
	// System.Xml.XmlResolver Mono.Xml.Schema.XsdValidatingReader::resolver
	XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * ___resolver_4;
	// Mono.Xml.IHasXmlSchemaInfo Mono.Xml.Schema.XsdValidatingReader::sourceReaderSchemaInfo
	RuntimeObject* ___sourceReaderSchemaInfo_5;
	// System.Xml.IXmlLineInfo Mono.Xml.Schema.XsdValidatingReader::readerLineInfo
	RuntimeObject* ___readerLineInfo_6;
	// System.Xml.ValidationType Mono.Xml.Schema.XsdValidatingReader::validationType
	int32_t ___validationType_7;
	// System.Xml.Schema.XmlSchemaSet Mono.Xml.Schema.XsdValidatingReader::schemas
	XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D * ___schemas_8;
	// System.Boolean Mono.Xml.Schema.XsdValidatingReader::namespaces
	bool ___namespaces_9;
	// System.Boolean Mono.Xml.Schema.XsdValidatingReader::validationStarted
	bool ___validationStarted_10;
	// System.Boolean Mono.Xml.Schema.XsdValidatingReader::checkIdentity
	bool ___checkIdentity_11;
	// Mono.Xml.Schema.XsdIDManager Mono.Xml.Schema.XsdValidatingReader::idManager
	XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E * ___idManager_12;
	// System.Boolean Mono.Xml.Schema.XsdValidatingReader::checkKeyConstraints
	bool ___checkKeyConstraints_13;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdValidatingReader::keyTables
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___keyTables_14;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdValidatingReader::currentKeyFieldConsumers
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___currentKeyFieldConsumers_15;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdValidatingReader::tmpKeyrefPool
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___tmpKeyrefPool_16;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdValidatingReader::elementQNameStack
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___elementQNameStack_17;
	// Mono.Xml.Schema.XsdParticleStateManager Mono.Xml.Schema.XsdValidatingReader::state
	XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68 * ___state_18;
	// System.Int32 Mono.Xml.Schema.XsdValidatingReader::skipValidationDepth
	int32_t ___skipValidationDepth_19;
	// System.Int32 Mono.Xml.Schema.XsdValidatingReader::xsiNilDepth
	int32_t ___xsiNilDepth_20;
	// System.Text.StringBuilder Mono.Xml.Schema.XsdValidatingReader::storedCharacters
	StringBuilder_t * ___storedCharacters_21;
	// System.Boolean Mono.Xml.Schema.XsdValidatingReader::shouldValidateCharacters
	bool ___shouldValidateCharacters_22;
	// System.Xml.Schema.XmlSchemaAttribute[] Mono.Xml.Schema.XsdValidatingReader::defaultAttributes
	XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95* ___defaultAttributes_23;
	// System.Int32 Mono.Xml.Schema.XsdValidatingReader::currentDefaultAttribute
	int32_t ___currentDefaultAttribute_24;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdValidatingReader::defaultAttributesCache
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___defaultAttributesCache_25;
	// System.Boolean Mono.Xml.Schema.XsdValidatingReader::defaultAttributeConsumed
	bool ___defaultAttributeConsumed_26;
	// System.Object Mono.Xml.Schema.XsdValidatingReader::currentAttrType
	RuntimeObject * ___currentAttrType_27;
	// System.Xml.Schema.ValidationEventHandler Mono.Xml.Schema.XsdValidatingReader::ValidationEventHandler
	ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B * ___ValidationEventHandler_28;

public:
	inline static int32_t get_offset_of_reader_3() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___reader_3)); }
	inline XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * get_reader_3() const { return ___reader_3; }
	inline XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 ** get_address_of_reader_3() { return &___reader_3; }
	inline void set_reader_3(XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * value)
	{
		___reader_3 = value;
		Il2CppCodeGenWriteBarrier((&___reader_3), value);
	}

	inline static int32_t get_offset_of_resolver_4() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___resolver_4)); }
	inline XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * get_resolver_4() const { return ___resolver_4; }
	inline XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C ** get_address_of_resolver_4() { return &___resolver_4; }
	inline void set_resolver_4(XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * value)
	{
		___resolver_4 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_4), value);
	}

	inline static int32_t get_offset_of_sourceReaderSchemaInfo_5() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___sourceReaderSchemaInfo_5)); }
	inline RuntimeObject* get_sourceReaderSchemaInfo_5() const { return ___sourceReaderSchemaInfo_5; }
	inline RuntimeObject** get_address_of_sourceReaderSchemaInfo_5() { return &___sourceReaderSchemaInfo_5; }
	inline void set_sourceReaderSchemaInfo_5(RuntimeObject* value)
	{
		___sourceReaderSchemaInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___sourceReaderSchemaInfo_5), value);
	}

	inline static int32_t get_offset_of_readerLineInfo_6() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___readerLineInfo_6)); }
	inline RuntimeObject* get_readerLineInfo_6() const { return ___readerLineInfo_6; }
	inline RuntimeObject** get_address_of_readerLineInfo_6() { return &___readerLineInfo_6; }
	inline void set_readerLineInfo_6(RuntimeObject* value)
	{
		___readerLineInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___readerLineInfo_6), value);
	}

	inline static int32_t get_offset_of_validationType_7() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___validationType_7)); }
	inline int32_t get_validationType_7() const { return ___validationType_7; }
	inline int32_t* get_address_of_validationType_7() { return &___validationType_7; }
	inline void set_validationType_7(int32_t value)
	{
		___validationType_7 = value;
	}

	inline static int32_t get_offset_of_schemas_8() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___schemas_8)); }
	inline XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D * get_schemas_8() const { return ___schemas_8; }
	inline XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D ** get_address_of_schemas_8() { return &___schemas_8; }
	inline void set_schemas_8(XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D * value)
	{
		___schemas_8 = value;
		Il2CppCodeGenWriteBarrier((&___schemas_8), value);
	}

	inline static int32_t get_offset_of_namespaces_9() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___namespaces_9)); }
	inline bool get_namespaces_9() const { return ___namespaces_9; }
	inline bool* get_address_of_namespaces_9() { return &___namespaces_9; }
	inline void set_namespaces_9(bool value)
	{
		___namespaces_9 = value;
	}

	inline static int32_t get_offset_of_validationStarted_10() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___validationStarted_10)); }
	inline bool get_validationStarted_10() const { return ___validationStarted_10; }
	inline bool* get_address_of_validationStarted_10() { return &___validationStarted_10; }
	inline void set_validationStarted_10(bool value)
	{
		___validationStarted_10 = value;
	}

	inline static int32_t get_offset_of_checkIdentity_11() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___checkIdentity_11)); }
	inline bool get_checkIdentity_11() const { return ___checkIdentity_11; }
	inline bool* get_address_of_checkIdentity_11() { return &___checkIdentity_11; }
	inline void set_checkIdentity_11(bool value)
	{
		___checkIdentity_11 = value;
	}

	inline static int32_t get_offset_of_idManager_12() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___idManager_12)); }
	inline XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E * get_idManager_12() const { return ___idManager_12; }
	inline XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E ** get_address_of_idManager_12() { return &___idManager_12; }
	inline void set_idManager_12(XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E * value)
	{
		___idManager_12 = value;
		Il2CppCodeGenWriteBarrier((&___idManager_12), value);
	}

	inline static int32_t get_offset_of_checkKeyConstraints_13() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___checkKeyConstraints_13)); }
	inline bool get_checkKeyConstraints_13() const { return ___checkKeyConstraints_13; }
	inline bool* get_address_of_checkKeyConstraints_13() { return &___checkKeyConstraints_13; }
	inline void set_checkKeyConstraints_13(bool value)
	{
		___checkKeyConstraints_13 = value;
	}

	inline static int32_t get_offset_of_keyTables_14() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___keyTables_14)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_keyTables_14() const { return ___keyTables_14; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_keyTables_14() { return &___keyTables_14; }
	inline void set_keyTables_14(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___keyTables_14 = value;
		Il2CppCodeGenWriteBarrier((&___keyTables_14), value);
	}

	inline static int32_t get_offset_of_currentKeyFieldConsumers_15() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___currentKeyFieldConsumers_15)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_currentKeyFieldConsumers_15() const { return ___currentKeyFieldConsumers_15; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_currentKeyFieldConsumers_15() { return &___currentKeyFieldConsumers_15; }
	inline void set_currentKeyFieldConsumers_15(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___currentKeyFieldConsumers_15 = value;
		Il2CppCodeGenWriteBarrier((&___currentKeyFieldConsumers_15), value);
	}

	inline static int32_t get_offset_of_tmpKeyrefPool_16() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___tmpKeyrefPool_16)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_tmpKeyrefPool_16() const { return ___tmpKeyrefPool_16; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_tmpKeyrefPool_16() { return &___tmpKeyrefPool_16; }
	inline void set_tmpKeyrefPool_16(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___tmpKeyrefPool_16 = value;
		Il2CppCodeGenWriteBarrier((&___tmpKeyrefPool_16), value);
	}

	inline static int32_t get_offset_of_elementQNameStack_17() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___elementQNameStack_17)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_elementQNameStack_17() const { return ___elementQNameStack_17; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_elementQNameStack_17() { return &___elementQNameStack_17; }
	inline void set_elementQNameStack_17(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___elementQNameStack_17 = value;
		Il2CppCodeGenWriteBarrier((&___elementQNameStack_17), value);
	}

	inline static int32_t get_offset_of_state_18() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___state_18)); }
	inline XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68 * get_state_18() const { return ___state_18; }
	inline XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68 ** get_address_of_state_18() { return &___state_18; }
	inline void set_state_18(XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68 * value)
	{
		___state_18 = value;
		Il2CppCodeGenWriteBarrier((&___state_18), value);
	}

	inline static int32_t get_offset_of_skipValidationDepth_19() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___skipValidationDepth_19)); }
	inline int32_t get_skipValidationDepth_19() const { return ___skipValidationDepth_19; }
	inline int32_t* get_address_of_skipValidationDepth_19() { return &___skipValidationDepth_19; }
	inline void set_skipValidationDepth_19(int32_t value)
	{
		___skipValidationDepth_19 = value;
	}

	inline static int32_t get_offset_of_xsiNilDepth_20() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___xsiNilDepth_20)); }
	inline int32_t get_xsiNilDepth_20() const { return ___xsiNilDepth_20; }
	inline int32_t* get_address_of_xsiNilDepth_20() { return &___xsiNilDepth_20; }
	inline void set_xsiNilDepth_20(int32_t value)
	{
		___xsiNilDepth_20 = value;
	}

	inline static int32_t get_offset_of_storedCharacters_21() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___storedCharacters_21)); }
	inline StringBuilder_t * get_storedCharacters_21() const { return ___storedCharacters_21; }
	inline StringBuilder_t ** get_address_of_storedCharacters_21() { return &___storedCharacters_21; }
	inline void set_storedCharacters_21(StringBuilder_t * value)
	{
		___storedCharacters_21 = value;
		Il2CppCodeGenWriteBarrier((&___storedCharacters_21), value);
	}

	inline static int32_t get_offset_of_shouldValidateCharacters_22() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___shouldValidateCharacters_22)); }
	inline bool get_shouldValidateCharacters_22() const { return ___shouldValidateCharacters_22; }
	inline bool* get_address_of_shouldValidateCharacters_22() { return &___shouldValidateCharacters_22; }
	inline void set_shouldValidateCharacters_22(bool value)
	{
		___shouldValidateCharacters_22 = value;
	}

	inline static int32_t get_offset_of_defaultAttributes_23() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___defaultAttributes_23)); }
	inline XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95* get_defaultAttributes_23() const { return ___defaultAttributes_23; }
	inline XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95** get_address_of_defaultAttributes_23() { return &___defaultAttributes_23; }
	inline void set_defaultAttributes_23(XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95* value)
	{
		___defaultAttributes_23 = value;
		Il2CppCodeGenWriteBarrier((&___defaultAttributes_23), value);
	}

	inline static int32_t get_offset_of_currentDefaultAttribute_24() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___currentDefaultAttribute_24)); }
	inline int32_t get_currentDefaultAttribute_24() const { return ___currentDefaultAttribute_24; }
	inline int32_t* get_address_of_currentDefaultAttribute_24() { return &___currentDefaultAttribute_24; }
	inline void set_currentDefaultAttribute_24(int32_t value)
	{
		___currentDefaultAttribute_24 = value;
	}

	inline static int32_t get_offset_of_defaultAttributesCache_25() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___defaultAttributesCache_25)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_defaultAttributesCache_25() const { return ___defaultAttributesCache_25; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_defaultAttributesCache_25() { return &___defaultAttributesCache_25; }
	inline void set_defaultAttributesCache_25(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___defaultAttributesCache_25 = value;
		Il2CppCodeGenWriteBarrier((&___defaultAttributesCache_25), value);
	}

	inline static int32_t get_offset_of_defaultAttributeConsumed_26() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___defaultAttributeConsumed_26)); }
	inline bool get_defaultAttributeConsumed_26() const { return ___defaultAttributeConsumed_26; }
	inline bool* get_address_of_defaultAttributeConsumed_26() { return &___defaultAttributeConsumed_26; }
	inline void set_defaultAttributeConsumed_26(bool value)
	{
		___defaultAttributeConsumed_26 = value;
	}

	inline static int32_t get_offset_of_currentAttrType_27() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___currentAttrType_27)); }
	inline RuntimeObject * get_currentAttrType_27() const { return ___currentAttrType_27; }
	inline RuntimeObject ** get_address_of_currentAttrType_27() { return &___currentAttrType_27; }
	inline void set_currentAttrType_27(RuntimeObject * value)
	{
		___currentAttrType_27 = value;
		Il2CppCodeGenWriteBarrier((&___currentAttrType_27), value);
	}

	inline static int32_t get_offset_of_ValidationEventHandler_28() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___ValidationEventHandler_28)); }
	inline ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B * get_ValidationEventHandler_28() const { return ___ValidationEventHandler_28; }
	inline ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B ** get_address_of_ValidationEventHandler_28() { return &___ValidationEventHandler_28; }
	inline void set_ValidationEventHandler_28(ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B * value)
	{
		___ValidationEventHandler_28 = value;
		Il2CppCodeGenWriteBarrier((&___ValidationEventHandler_28), value);
	}
};

struct XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaAttribute[] Mono.Xml.Schema.XsdValidatingReader::emptyAttributeArray
	XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95* ___emptyAttributeArray_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XsdValidatingReader::<>f__switch$map3
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map3_29;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XsdValidatingReader::<>f__switch$map4
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map4_30;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XsdValidatingReader::<>f__switch$map5
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map5_31;

public:
	inline static int32_t get_offset_of_emptyAttributeArray_2() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3_StaticFields, ___emptyAttributeArray_2)); }
	inline XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95* get_emptyAttributeArray_2() const { return ___emptyAttributeArray_2; }
	inline XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95** get_address_of_emptyAttributeArray_2() { return &___emptyAttributeArray_2; }
	inline void set_emptyAttributeArray_2(XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95* value)
	{
		___emptyAttributeArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___emptyAttributeArray_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map3_29() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3_StaticFields, ___U3CU3Ef__switchU24map3_29)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map3_29() const { return ___U3CU3Ef__switchU24map3_29; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map3_29() { return &___U3CU3Ef__switchU24map3_29; }
	inline void set_U3CU3Ef__switchU24map3_29(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map3_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map3_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4_30() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3_StaticFields, ___U3CU3Ef__switchU24map4_30)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map4_30() const { return ___U3CU3Ef__switchU24map4_30; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map4_30() { return &___U3CU3Ef__switchU24map4_30; }
	inline void set_U3CU3Ef__switchU24map4_30(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map4_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map4_30), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map5_31() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3_StaticFields, ___U3CU3Ef__switchU24map5_31)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map5_31() const { return ___U3CU3Ef__switchU24map5_31; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map5_31() { return &___U3CU3Ef__switchU24map5_31; }
	inline void set_U3CU3Ef__switchU24map5_31(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map5_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map5_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDVALIDATINGREADER_T2C211F16BDE384F56FB73B293505DEADE5AFCEB3_H
#ifndef XSDWILDCARD_T320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7_H
#define XSDWILDCARD_T320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdWildcard
struct  XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7  : public RuntimeObject
{
public:
	// System.Xml.Schema.XmlSchemaObject Mono.Xml.Schema.XsdWildcard::xsobj
	XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 * ___xsobj_0;
	// System.Xml.Schema.XmlSchemaContentProcessing Mono.Xml.Schema.XsdWildcard::ResolvedProcessing
	int32_t ___ResolvedProcessing_1;
	// System.String Mono.Xml.Schema.XsdWildcard::TargetNamespace
	String_t* ___TargetNamespace_2;
	// System.Boolean Mono.Xml.Schema.XsdWildcard::SkipCompile
	bool ___SkipCompile_3;
	// System.Boolean Mono.Xml.Schema.XsdWildcard::HasValueAny
	bool ___HasValueAny_4;
	// System.Boolean Mono.Xml.Schema.XsdWildcard::HasValueLocal
	bool ___HasValueLocal_5;
	// System.Boolean Mono.Xml.Schema.XsdWildcard::HasValueOther
	bool ___HasValueOther_6;
	// System.Boolean Mono.Xml.Schema.XsdWildcard::HasValueTargetNamespace
	bool ___HasValueTargetNamespace_7;
	// System.Collections.Specialized.StringCollection Mono.Xml.Schema.XsdWildcard::ResolvedNamespaces
	StringCollection_t1921D1DDEF33942CB6DF73338C52ECD8C6ABC5E9 * ___ResolvedNamespaces_8;

public:
	inline static int32_t get_offset_of_xsobj_0() { return static_cast<int32_t>(offsetof(XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7, ___xsobj_0)); }
	inline XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 * get_xsobj_0() const { return ___xsobj_0; }
	inline XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 ** get_address_of_xsobj_0() { return &___xsobj_0; }
	inline void set_xsobj_0(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 * value)
	{
		___xsobj_0 = value;
		Il2CppCodeGenWriteBarrier((&___xsobj_0), value);
	}

	inline static int32_t get_offset_of_ResolvedProcessing_1() { return static_cast<int32_t>(offsetof(XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7, ___ResolvedProcessing_1)); }
	inline int32_t get_ResolvedProcessing_1() const { return ___ResolvedProcessing_1; }
	inline int32_t* get_address_of_ResolvedProcessing_1() { return &___ResolvedProcessing_1; }
	inline void set_ResolvedProcessing_1(int32_t value)
	{
		___ResolvedProcessing_1 = value;
	}

	inline static int32_t get_offset_of_TargetNamespace_2() { return static_cast<int32_t>(offsetof(XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7, ___TargetNamespace_2)); }
	inline String_t* get_TargetNamespace_2() const { return ___TargetNamespace_2; }
	inline String_t** get_address_of_TargetNamespace_2() { return &___TargetNamespace_2; }
	inline void set_TargetNamespace_2(String_t* value)
	{
		___TargetNamespace_2 = value;
		Il2CppCodeGenWriteBarrier((&___TargetNamespace_2), value);
	}

	inline static int32_t get_offset_of_SkipCompile_3() { return static_cast<int32_t>(offsetof(XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7, ___SkipCompile_3)); }
	inline bool get_SkipCompile_3() const { return ___SkipCompile_3; }
	inline bool* get_address_of_SkipCompile_3() { return &___SkipCompile_3; }
	inline void set_SkipCompile_3(bool value)
	{
		___SkipCompile_3 = value;
	}

	inline static int32_t get_offset_of_HasValueAny_4() { return static_cast<int32_t>(offsetof(XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7, ___HasValueAny_4)); }
	inline bool get_HasValueAny_4() const { return ___HasValueAny_4; }
	inline bool* get_address_of_HasValueAny_4() { return &___HasValueAny_4; }
	inline void set_HasValueAny_4(bool value)
	{
		___HasValueAny_4 = value;
	}

	inline static int32_t get_offset_of_HasValueLocal_5() { return static_cast<int32_t>(offsetof(XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7, ___HasValueLocal_5)); }
	inline bool get_HasValueLocal_5() const { return ___HasValueLocal_5; }
	inline bool* get_address_of_HasValueLocal_5() { return &___HasValueLocal_5; }
	inline void set_HasValueLocal_5(bool value)
	{
		___HasValueLocal_5 = value;
	}

	inline static int32_t get_offset_of_HasValueOther_6() { return static_cast<int32_t>(offsetof(XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7, ___HasValueOther_6)); }
	inline bool get_HasValueOther_6() const { return ___HasValueOther_6; }
	inline bool* get_address_of_HasValueOther_6() { return &___HasValueOther_6; }
	inline void set_HasValueOther_6(bool value)
	{
		___HasValueOther_6 = value;
	}

	inline static int32_t get_offset_of_HasValueTargetNamespace_7() { return static_cast<int32_t>(offsetof(XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7, ___HasValueTargetNamespace_7)); }
	inline bool get_HasValueTargetNamespace_7() const { return ___HasValueTargetNamespace_7; }
	inline bool* get_address_of_HasValueTargetNamespace_7() { return &___HasValueTargetNamespace_7; }
	inline void set_HasValueTargetNamespace_7(bool value)
	{
		___HasValueTargetNamespace_7 = value;
	}

	inline static int32_t get_offset_of_ResolvedNamespaces_8() { return static_cast<int32_t>(offsetof(XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7, ___ResolvedNamespaces_8)); }
	inline StringCollection_t1921D1DDEF33942CB6DF73338C52ECD8C6ABC5E9 * get_ResolvedNamespaces_8() const { return ___ResolvedNamespaces_8; }
	inline StringCollection_t1921D1DDEF33942CB6DF73338C52ECD8C6ABC5E9 ** get_address_of_ResolvedNamespaces_8() { return &___ResolvedNamespaces_8; }
	inline void set_ResolvedNamespaces_8(StringCollection_t1921D1DDEF33942CB6DF73338C52ECD8C6ABC5E9 * value)
	{
		___ResolvedNamespaces_8 = value;
		Il2CppCodeGenWriteBarrier((&___ResolvedNamespaces_8), value);
	}
};

struct XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XsdWildcard::<>f__switch$map6
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map6_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map6_9() { return static_cast<int32_t>(offsetof(XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7_StaticFields, ___U3CU3Ef__switchU24map6_9)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map6_9() const { return ___U3CU3Ef__switchU24map6_9; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map6_9() { return &___U3CU3Ef__switchU24map6_9; }
	inline void set_U3CU3Ef__switchU24map6_9(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map6_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map6_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDWILDCARD_T320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7_H
#ifndef XMLSCHEMADATATYPE_T3384DFF75456A27E693C24453B91B2B7A0CC2A19_H
#define XMLSCHEMADATATYPE_T3384DFF75456A27E693C24453B91B2B7A0CC2A19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDatatype
struct  XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19  : public RuntimeObject
{
public:
	// Mono.Xml.Schema.XsdWhitespaceFacet System.Xml.Schema.XmlSchemaDatatype::WhitespaceValue
	int32_t ___WhitespaceValue_0;
	// System.Text.StringBuilder System.Xml.Schema.XmlSchemaDatatype::sb
	StringBuilder_t * ___sb_2;

public:
	inline static int32_t get_offset_of_WhitespaceValue_0() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19, ___WhitespaceValue_0)); }
	inline int32_t get_WhitespaceValue_0() const { return ___WhitespaceValue_0; }
	inline int32_t* get_address_of_WhitespaceValue_0() { return &___WhitespaceValue_0; }
	inline void set_WhitespaceValue_0(int32_t value)
	{
		___WhitespaceValue_0 = value;
	}

	inline static int32_t get_offset_of_sb_2() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19, ___sb_2)); }
	inline StringBuilder_t * get_sb_2() const { return ___sb_2; }
	inline StringBuilder_t ** get_address_of_sb_2() { return &___sb_2; }
	inline void set_sb_2(StringBuilder_t * value)
	{
		___sb_2 = value;
		Il2CppCodeGenWriteBarrier((&___sb_2), value);
	}
};

struct XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields
{
public:
	// System.Char[] System.Xml.Schema.XmlSchemaDatatype::wsChars
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___wsChars_1;
	// Mono.Xml.Schema.XsdAnySimpleType System.Xml.Schema.XmlSchemaDatatype::datatypeAnySimpleType
	XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0 * ___datatypeAnySimpleType_3;
	// Mono.Xml.Schema.XsdString System.Xml.Schema.XmlSchemaDatatype::datatypeString
	XsdString_t604253BF4F01F3A649275313E7AED2D8BCB854AD * ___datatypeString_4;
	// Mono.Xml.Schema.XsdNormalizedString System.Xml.Schema.XmlSchemaDatatype::datatypeNormalizedString
	XsdNormalizedString_tEAAA641AA303B7A5227D53147AACC07A82EE13C0 * ___datatypeNormalizedString_5;
	// Mono.Xml.Schema.XsdToken System.Xml.Schema.XmlSchemaDatatype::datatypeToken
	XsdToken_t4A120CAA60CECDF6DCDC15717ABCA63187B0D60C * ___datatypeToken_6;
	// Mono.Xml.Schema.XsdLanguage System.Xml.Schema.XmlSchemaDatatype::datatypeLanguage
	XsdLanguage_t392B00FB85420B861413B7295BC68EA7C0982D85 * ___datatypeLanguage_7;
	// Mono.Xml.Schema.XsdNMToken System.Xml.Schema.XmlSchemaDatatype::datatypeNMToken
	XsdNMToken_tED82C02272B75D49D76B0FC44858D09D8F06AB91 * ___datatypeNMToken_8;
	// Mono.Xml.Schema.XsdNMTokens System.Xml.Schema.XmlSchemaDatatype::datatypeNMTokens
	XsdNMTokens_tA3A587C5F1FF49445F75C961B0280F58A57D1095 * ___datatypeNMTokens_9;
	// Mono.Xml.Schema.XsdName System.Xml.Schema.XmlSchemaDatatype::datatypeName
	XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8 * ___datatypeName_10;
	// Mono.Xml.Schema.XsdNCName System.Xml.Schema.XmlSchemaDatatype::datatypeNCName
	XsdNCName_t7C66E8DC7486C5D92AF7739B71EC88C1FD11E2F7 * ___datatypeNCName_11;
	// Mono.Xml.Schema.XsdID System.Xml.Schema.XmlSchemaDatatype::datatypeID
	XsdID_tFC394CE8EE4F16A40F0063659AB8C788C712E77B * ___datatypeID_12;
	// Mono.Xml.Schema.XsdIDRef System.Xml.Schema.XmlSchemaDatatype::datatypeIDRef
	XsdIDRef_tC8B88E328BA8745548A89206641A6C113AE9F09B * ___datatypeIDRef_13;
	// Mono.Xml.Schema.XsdIDRefs System.Xml.Schema.XmlSchemaDatatype::datatypeIDRefs
	XsdIDRefs_t3598EB6F29A7BEC87962148242C3904C8FF5BB9F * ___datatypeIDRefs_14;
	// Mono.Xml.Schema.XsdEntity System.Xml.Schema.XmlSchemaDatatype::datatypeEntity
	XsdEntity_t426A9BE787702C55FF7FE5FE0FCDF582D5E50371 * ___datatypeEntity_15;
	// Mono.Xml.Schema.XsdEntities System.Xml.Schema.XmlSchemaDatatype::datatypeEntities
	XsdEntities_tE41F357D8F259BBCCB58DF1E99D66720E8F2CF1C * ___datatypeEntities_16;
	// Mono.Xml.Schema.XsdNotation System.Xml.Schema.XmlSchemaDatatype::datatypeNotation
	XsdNotation_t619CA8B98E6B358DAEE751C97934638D691DB5A3 * ___datatypeNotation_17;
	// Mono.Xml.Schema.XsdDecimal System.Xml.Schema.XmlSchemaDatatype::datatypeDecimal
	XsdDecimal_t1CB6398C98F43AF142671E2E9BDAB05D58AEE9EE * ___datatypeDecimal_18;
	// Mono.Xml.Schema.XsdInteger System.Xml.Schema.XmlSchemaDatatype::datatypeInteger
	XsdInteger_tE61DFB650D9B7BD3F6CA2C7DD4331667015CA348 * ___datatypeInteger_19;
	// Mono.Xml.Schema.XsdLong System.Xml.Schema.XmlSchemaDatatype::datatypeLong
	XsdLong_t6FCDD94BFBBEC0874CD9166C160CF7B60D36B5FC * ___datatypeLong_20;
	// Mono.Xml.Schema.XsdInt System.Xml.Schema.XmlSchemaDatatype::datatypeInt
	XsdInt_t6F6E692FF9941520F897A726ABDE415CCEFC984F * ___datatypeInt_21;
	// Mono.Xml.Schema.XsdShort System.Xml.Schema.XmlSchemaDatatype::datatypeShort
	XsdShort_tAF41351D06667202EC3FDBC76501A2D091518332 * ___datatypeShort_22;
	// Mono.Xml.Schema.XsdByte System.Xml.Schema.XmlSchemaDatatype::datatypeByte
	XsdByte_t048838D9FF5BFD18C9FC056F11C699E7E08FC351 * ___datatypeByte_23;
	// Mono.Xml.Schema.XsdNonNegativeInteger System.Xml.Schema.XmlSchemaDatatype::datatypeNonNegativeInteger
	XsdNonNegativeInteger_t0F4F93B0464B81E0BBF21CFC2F08343966734B30 * ___datatypeNonNegativeInteger_24;
	// Mono.Xml.Schema.XsdPositiveInteger System.Xml.Schema.XmlSchemaDatatype::datatypePositiveInteger
	XsdPositiveInteger_t8089829DDE14DBD3EEAD11DD4EBA8F4658FA04B2 * ___datatypePositiveInteger_25;
	// Mono.Xml.Schema.XsdUnsignedLong System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedLong
	XsdUnsignedLong_t7B886E29EBA85082057263BFE0B9F644FF4D59B9 * ___datatypeUnsignedLong_26;
	// Mono.Xml.Schema.XsdUnsignedInt System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedInt
	XsdUnsignedInt_t01075FD84457AE05E045BB23BD0E3A82AC30CE32 * ___datatypeUnsignedInt_27;
	// Mono.Xml.Schema.XsdUnsignedShort System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedShort
	XsdUnsignedShort_tA832C283CB5937E31E78F3E4BBC85F90F92CD785 * ___datatypeUnsignedShort_28;
	// Mono.Xml.Schema.XsdUnsignedByte System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedByte
	XsdUnsignedByte_tA702674830BFF7B5B4ADF325F30A781366A45256 * ___datatypeUnsignedByte_29;
	// Mono.Xml.Schema.XsdNonPositiveInteger System.Xml.Schema.XmlSchemaDatatype::datatypeNonPositiveInteger
	XsdNonPositiveInteger_t960302EB6DDE4ECE0961BC6BBE4FAFA3C3E5BE62 * ___datatypeNonPositiveInteger_30;
	// Mono.Xml.Schema.XsdNegativeInteger System.Xml.Schema.XmlSchemaDatatype::datatypeNegativeInteger
	XsdNegativeInteger_t5BF8A2EE42840B88A3C798FC6704B5AA9EDA715B * ___datatypeNegativeInteger_31;
	// Mono.Xml.Schema.XsdFloat System.Xml.Schema.XmlSchemaDatatype::datatypeFloat
	XsdFloat_t02419495B090187B99AC33E40CD6601F7608D9B8 * ___datatypeFloat_32;
	// Mono.Xml.Schema.XsdDouble System.Xml.Schema.XmlSchemaDatatype::datatypeDouble
	XsdDouble_tAF166F167258B898C01C97CE94720CA2EDB5E269 * ___datatypeDouble_33;
	// Mono.Xml.Schema.XsdBase64Binary System.Xml.Schema.XmlSchemaDatatype::datatypeBase64Binary
	XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F * ___datatypeBase64Binary_34;
	// Mono.Xml.Schema.XsdBoolean System.Xml.Schema.XmlSchemaDatatype::datatypeBoolean
	XsdBoolean_tAA5FDC2ACA9F0A05A686F75BFE68CBB53A2F9959 * ___datatypeBoolean_35;
	// Mono.Xml.Schema.XsdAnyURI System.Xml.Schema.XmlSchemaDatatype::datatypeAnyURI
	XsdAnyURI_t049F3807506793286A90A0A279CADC8C39D4647A * ___datatypeAnyURI_36;
	// Mono.Xml.Schema.XsdDuration System.Xml.Schema.XmlSchemaDatatype::datatypeDuration
	XsdDuration_tA3253842BF902691A000BBFD87382247985D5E78 * ___datatypeDuration_37;
	// Mono.Xml.Schema.XsdDateTime System.Xml.Schema.XmlSchemaDatatype::datatypeDateTime
	XsdDateTime_tC31D6EF56B47537535EEC097FEBF63553C479E5D * ___datatypeDateTime_38;
	// Mono.Xml.Schema.XsdDate System.Xml.Schema.XmlSchemaDatatype::datatypeDate
	XsdDate_t5536E073BDA86CBF86DE829550F870C6D7F89964 * ___datatypeDate_39;
	// Mono.Xml.Schema.XsdTime System.Xml.Schema.XmlSchemaDatatype::datatypeTime
	XsdTime_tDB0973DB314925C984AC13CB795FD91589AB462C * ___datatypeTime_40;
	// Mono.Xml.Schema.XsdHexBinary System.Xml.Schema.XmlSchemaDatatype::datatypeHexBinary
	XsdHexBinary_t542DD47AB586F148DAE0698D4FC956D0412C4229 * ___datatypeHexBinary_41;
	// Mono.Xml.Schema.XsdQName System.Xml.Schema.XmlSchemaDatatype::datatypeQName
	XsdQName_t6E0BD13D8BFE16A59C49FAA8D331CA5FF5066DA0 * ___datatypeQName_42;
	// Mono.Xml.Schema.XsdGYearMonth System.Xml.Schema.XmlSchemaDatatype::datatypeGYearMonth
	XsdGYearMonth_t775E38939959BAF7304B4212BD8406456781C385 * ___datatypeGYearMonth_43;
	// Mono.Xml.Schema.XsdGMonthDay System.Xml.Schema.XmlSchemaDatatype::datatypeGMonthDay
	XsdGMonthDay_t25E4C24A7DF2B6F4528C0271813BF8AF052F5669 * ___datatypeGMonthDay_44;
	// Mono.Xml.Schema.XsdGYear System.Xml.Schema.XmlSchemaDatatype::datatypeGYear
	XsdGYear_t3A3FECDF3BF0A8EDC0776CA5D478A2394CC894F2 * ___datatypeGYear_45;
	// Mono.Xml.Schema.XsdGMonth System.Xml.Schema.XmlSchemaDatatype::datatypeGMonth
	XsdGMonth_t6E98934D7FAFFBCFA788D4068EB7DAE6706F4312 * ___datatypeGMonth_46;
	// Mono.Xml.Schema.XsdGDay System.Xml.Schema.XmlSchemaDatatype::datatypeGDay
	XsdGDay_tEC936DF85B2878EB240E093D4B56BBDDC75A463B * ___datatypeGDay_47;
	// Mono.Xml.Schema.XdtAnyAtomicType System.Xml.Schema.XmlSchemaDatatype::datatypeAnyAtomicType
	XdtAnyAtomicType_t922A677221DBA603ACF1F4D2024F9222102416D7 * ___datatypeAnyAtomicType_48;
	// Mono.Xml.Schema.XdtUntypedAtomic System.Xml.Schema.XmlSchemaDatatype::datatypeUntypedAtomic
	XdtUntypedAtomic_t8E0FD4A48C677AA7C2D946690BE85E92CE81CDED * ___datatypeUntypedAtomic_49;
	// Mono.Xml.Schema.XdtDayTimeDuration System.Xml.Schema.XmlSchemaDatatype::datatypeDayTimeDuration
	XdtDayTimeDuration_t14B50E7DF624721215FA0E49D3672BAAFF59027F * ___datatypeDayTimeDuration_50;
	// Mono.Xml.Schema.XdtYearMonthDuration System.Xml.Schema.XmlSchemaDatatype::datatypeYearMonthDuration
	XdtYearMonthDuration_t940953B7258AE5DEC65A9F65D90EC70D9786EA68 * ___datatypeYearMonthDuration_51;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaDatatype::<>f__switch$map2A
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map2A_52;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaDatatype::<>f__switch$map2B
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map2B_53;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaDatatype::<>f__switch$map2C
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map2C_54;

public:
	inline static int32_t get_offset_of_wsChars_1() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___wsChars_1)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_wsChars_1() const { return ___wsChars_1; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_wsChars_1() { return &___wsChars_1; }
	inline void set_wsChars_1(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___wsChars_1 = value;
		Il2CppCodeGenWriteBarrier((&___wsChars_1), value);
	}

	inline static int32_t get_offset_of_datatypeAnySimpleType_3() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeAnySimpleType_3)); }
	inline XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0 * get_datatypeAnySimpleType_3() const { return ___datatypeAnySimpleType_3; }
	inline XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0 ** get_address_of_datatypeAnySimpleType_3() { return &___datatypeAnySimpleType_3; }
	inline void set_datatypeAnySimpleType_3(XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0 * value)
	{
		___datatypeAnySimpleType_3 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeAnySimpleType_3), value);
	}

	inline static int32_t get_offset_of_datatypeString_4() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeString_4)); }
	inline XsdString_t604253BF4F01F3A649275313E7AED2D8BCB854AD * get_datatypeString_4() const { return ___datatypeString_4; }
	inline XsdString_t604253BF4F01F3A649275313E7AED2D8BCB854AD ** get_address_of_datatypeString_4() { return &___datatypeString_4; }
	inline void set_datatypeString_4(XsdString_t604253BF4F01F3A649275313E7AED2D8BCB854AD * value)
	{
		___datatypeString_4 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeString_4), value);
	}

	inline static int32_t get_offset_of_datatypeNormalizedString_5() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeNormalizedString_5)); }
	inline XsdNormalizedString_tEAAA641AA303B7A5227D53147AACC07A82EE13C0 * get_datatypeNormalizedString_5() const { return ___datatypeNormalizedString_5; }
	inline XsdNormalizedString_tEAAA641AA303B7A5227D53147AACC07A82EE13C0 ** get_address_of_datatypeNormalizedString_5() { return &___datatypeNormalizedString_5; }
	inline void set_datatypeNormalizedString_5(XsdNormalizedString_tEAAA641AA303B7A5227D53147AACC07A82EE13C0 * value)
	{
		___datatypeNormalizedString_5 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNormalizedString_5), value);
	}

	inline static int32_t get_offset_of_datatypeToken_6() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeToken_6)); }
	inline XsdToken_t4A120CAA60CECDF6DCDC15717ABCA63187B0D60C * get_datatypeToken_6() const { return ___datatypeToken_6; }
	inline XsdToken_t4A120CAA60CECDF6DCDC15717ABCA63187B0D60C ** get_address_of_datatypeToken_6() { return &___datatypeToken_6; }
	inline void set_datatypeToken_6(XsdToken_t4A120CAA60CECDF6DCDC15717ABCA63187B0D60C * value)
	{
		___datatypeToken_6 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeToken_6), value);
	}

	inline static int32_t get_offset_of_datatypeLanguage_7() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeLanguage_7)); }
	inline XsdLanguage_t392B00FB85420B861413B7295BC68EA7C0982D85 * get_datatypeLanguage_7() const { return ___datatypeLanguage_7; }
	inline XsdLanguage_t392B00FB85420B861413B7295BC68EA7C0982D85 ** get_address_of_datatypeLanguage_7() { return &___datatypeLanguage_7; }
	inline void set_datatypeLanguage_7(XsdLanguage_t392B00FB85420B861413B7295BC68EA7C0982D85 * value)
	{
		___datatypeLanguage_7 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeLanguage_7), value);
	}

	inline static int32_t get_offset_of_datatypeNMToken_8() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeNMToken_8)); }
	inline XsdNMToken_tED82C02272B75D49D76B0FC44858D09D8F06AB91 * get_datatypeNMToken_8() const { return ___datatypeNMToken_8; }
	inline XsdNMToken_tED82C02272B75D49D76B0FC44858D09D8F06AB91 ** get_address_of_datatypeNMToken_8() { return &___datatypeNMToken_8; }
	inline void set_datatypeNMToken_8(XsdNMToken_tED82C02272B75D49D76B0FC44858D09D8F06AB91 * value)
	{
		___datatypeNMToken_8 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNMToken_8), value);
	}

	inline static int32_t get_offset_of_datatypeNMTokens_9() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeNMTokens_9)); }
	inline XsdNMTokens_tA3A587C5F1FF49445F75C961B0280F58A57D1095 * get_datatypeNMTokens_9() const { return ___datatypeNMTokens_9; }
	inline XsdNMTokens_tA3A587C5F1FF49445F75C961B0280F58A57D1095 ** get_address_of_datatypeNMTokens_9() { return &___datatypeNMTokens_9; }
	inline void set_datatypeNMTokens_9(XsdNMTokens_tA3A587C5F1FF49445F75C961B0280F58A57D1095 * value)
	{
		___datatypeNMTokens_9 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNMTokens_9), value);
	}

	inline static int32_t get_offset_of_datatypeName_10() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeName_10)); }
	inline XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8 * get_datatypeName_10() const { return ___datatypeName_10; }
	inline XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8 ** get_address_of_datatypeName_10() { return &___datatypeName_10; }
	inline void set_datatypeName_10(XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8 * value)
	{
		___datatypeName_10 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeName_10), value);
	}

	inline static int32_t get_offset_of_datatypeNCName_11() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeNCName_11)); }
	inline XsdNCName_t7C66E8DC7486C5D92AF7739B71EC88C1FD11E2F7 * get_datatypeNCName_11() const { return ___datatypeNCName_11; }
	inline XsdNCName_t7C66E8DC7486C5D92AF7739B71EC88C1FD11E2F7 ** get_address_of_datatypeNCName_11() { return &___datatypeNCName_11; }
	inline void set_datatypeNCName_11(XsdNCName_t7C66E8DC7486C5D92AF7739B71EC88C1FD11E2F7 * value)
	{
		___datatypeNCName_11 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNCName_11), value);
	}

	inline static int32_t get_offset_of_datatypeID_12() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeID_12)); }
	inline XsdID_tFC394CE8EE4F16A40F0063659AB8C788C712E77B * get_datatypeID_12() const { return ___datatypeID_12; }
	inline XsdID_tFC394CE8EE4F16A40F0063659AB8C788C712E77B ** get_address_of_datatypeID_12() { return &___datatypeID_12; }
	inline void set_datatypeID_12(XsdID_tFC394CE8EE4F16A40F0063659AB8C788C712E77B * value)
	{
		___datatypeID_12 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeID_12), value);
	}

	inline static int32_t get_offset_of_datatypeIDRef_13() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeIDRef_13)); }
	inline XsdIDRef_tC8B88E328BA8745548A89206641A6C113AE9F09B * get_datatypeIDRef_13() const { return ___datatypeIDRef_13; }
	inline XsdIDRef_tC8B88E328BA8745548A89206641A6C113AE9F09B ** get_address_of_datatypeIDRef_13() { return &___datatypeIDRef_13; }
	inline void set_datatypeIDRef_13(XsdIDRef_tC8B88E328BA8745548A89206641A6C113AE9F09B * value)
	{
		___datatypeIDRef_13 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeIDRef_13), value);
	}

	inline static int32_t get_offset_of_datatypeIDRefs_14() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeIDRefs_14)); }
	inline XsdIDRefs_t3598EB6F29A7BEC87962148242C3904C8FF5BB9F * get_datatypeIDRefs_14() const { return ___datatypeIDRefs_14; }
	inline XsdIDRefs_t3598EB6F29A7BEC87962148242C3904C8FF5BB9F ** get_address_of_datatypeIDRefs_14() { return &___datatypeIDRefs_14; }
	inline void set_datatypeIDRefs_14(XsdIDRefs_t3598EB6F29A7BEC87962148242C3904C8FF5BB9F * value)
	{
		___datatypeIDRefs_14 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeIDRefs_14), value);
	}

	inline static int32_t get_offset_of_datatypeEntity_15() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeEntity_15)); }
	inline XsdEntity_t426A9BE787702C55FF7FE5FE0FCDF582D5E50371 * get_datatypeEntity_15() const { return ___datatypeEntity_15; }
	inline XsdEntity_t426A9BE787702C55FF7FE5FE0FCDF582D5E50371 ** get_address_of_datatypeEntity_15() { return &___datatypeEntity_15; }
	inline void set_datatypeEntity_15(XsdEntity_t426A9BE787702C55FF7FE5FE0FCDF582D5E50371 * value)
	{
		___datatypeEntity_15 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeEntity_15), value);
	}

	inline static int32_t get_offset_of_datatypeEntities_16() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeEntities_16)); }
	inline XsdEntities_tE41F357D8F259BBCCB58DF1E99D66720E8F2CF1C * get_datatypeEntities_16() const { return ___datatypeEntities_16; }
	inline XsdEntities_tE41F357D8F259BBCCB58DF1E99D66720E8F2CF1C ** get_address_of_datatypeEntities_16() { return &___datatypeEntities_16; }
	inline void set_datatypeEntities_16(XsdEntities_tE41F357D8F259BBCCB58DF1E99D66720E8F2CF1C * value)
	{
		___datatypeEntities_16 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeEntities_16), value);
	}

	inline static int32_t get_offset_of_datatypeNotation_17() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeNotation_17)); }
	inline XsdNotation_t619CA8B98E6B358DAEE751C97934638D691DB5A3 * get_datatypeNotation_17() const { return ___datatypeNotation_17; }
	inline XsdNotation_t619CA8B98E6B358DAEE751C97934638D691DB5A3 ** get_address_of_datatypeNotation_17() { return &___datatypeNotation_17; }
	inline void set_datatypeNotation_17(XsdNotation_t619CA8B98E6B358DAEE751C97934638D691DB5A3 * value)
	{
		___datatypeNotation_17 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNotation_17), value);
	}

	inline static int32_t get_offset_of_datatypeDecimal_18() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeDecimal_18)); }
	inline XsdDecimal_t1CB6398C98F43AF142671E2E9BDAB05D58AEE9EE * get_datatypeDecimal_18() const { return ___datatypeDecimal_18; }
	inline XsdDecimal_t1CB6398C98F43AF142671E2E9BDAB05D58AEE9EE ** get_address_of_datatypeDecimal_18() { return &___datatypeDecimal_18; }
	inline void set_datatypeDecimal_18(XsdDecimal_t1CB6398C98F43AF142671E2E9BDAB05D58AEE9EE * value)
	{
		___datatypeDecimal_18 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDecimal_18), value);
	}

	inline static int32_t get_offset_of_datatypeInteger_19() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeInteger_19)); }
	inline XsdInteger_tE61DFB650D9B7BD3F6CA2C7DD4331667015CA348 * get_datatypeInteger_19() const { return ___datatypeInteger_19; }
	inline XsdInteger_tE61DFB650D9B7BD3F6CA2C7DD4331667015CA348 ** get_address_of_datatypeInteger_19() { return &___datatypeInteger_19; }
	inline void set_datatypeInteger_19(XsdInteger_tE61DFB650D9B7BD3F6CA2C7DD4331667015CA348 * value)
	{
		___datatypeInteger_19 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeInteger_19), value);
	}

	inline static int32_t get_offset_of_datatypeLong_20() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeLong_20)); }
	inline XsdLong_t6FCDD94BFBBEC0874CD9166C160CF7B60D36B5FC * get_datatypeLong_20() const { return ___datatypeLong_20; }
	inline XsdLong_t6FCDD94BFBBEC0874CD9166C160CF7B60D36B5FC ** get_address_of_datatypeLong_20() { return &___datatypeLong_20; }
	inline void set_datatypeLong_20(XsdLong_t6FCDD94BFBBEC0874CD9166C160CF7B60D36B5FC * value)
	{
		___datatypeLong_20 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeLong_20), value);
	}

	inline static int32_t get_offset_of_datatypeInt_21() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeInt_21)); }
	inline XsdInt_t6F6E692FF9941520F897A726ABDE415CCEFC984F * get_datatypeInt_21() const { return ___datatypeInt_21; }
	inline XsdInt_t6F6E692FF9941520F897A726ABDE415CCEFC984F ** get_address_of_datatypeInt_21() { return &___datatypeInt_21; }
	inline void set_datatypeInt_21(XsdInt_t6F6E692FF9941520F897A726ABDE415CCEFC984F * value)
	{
		___datatypeInt_21 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeInt_21), value);
	}

	inline static int32_t get_offset_of_datatypeShort_22() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeShort_22)); }
	inline XsdShort_tAF41351D06667202EC3FDBC76501A2D091518332 * get_datatypeShort_22() const { return ___datatypeShort_22; }
	inline XsdShort_tAF41351D06667202EC3FDBC76501A2D091518332 ** get_address_of_datatypeShort_22() { return &___datatypeShort_22; }
	inline void set_datatypeShort_22(XsdShort_tAF41351D06667202EC3FDBC76501A2D091518332 * value)
	{
		___datatypeShort_22 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeShort_22), value);
	}

	inline static int32_t get_offset_of_datatypeByte_23() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeByte_23)); }
	inline XsdByte_t048838D9FF5BFD18C9FC056F11C699E7E08FC351 * get_datatypeByte_23() const { return ___datatypeByte_23; }
	inline XsdByte_t048838D9FF5BFD18C9FC056F11C699E7E08FC351 ** get_address_of_datatypeByte_23() { return &___datatypeByte_23; }
	inline void set_datatypeByte_23(XsdByte_t048838D9FF5BFD18C9FC056F11C699E7E08FC351 * value)
	{
		___datatypeByte_23 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeByte_23), value);
	}

	inline static int32_t get_offset_of_datatypeNonNegativeInteger_24() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeNonNegativeInteger_24)); }
	inline XsdNonNegativeInteger_t0F4F93B0464B81E0BBF21CFC2F08343966734B30 * get_datatypeNonNegativeInteger_24() const { return ___datatypeNonNegativeInteger_24; }
	inline XsdNonNegativeInteger_t0F4F93B0464B81E0BBF21CFC2F08343966734B30 ** get_address_of_datatypeNonNegativeInteger_24() { return &___datatypeNonNegativeInteger_24; }
	inline void set_datatypeNonNegativeInteger_24(XsdNonNegativeInteger_t0F4F93B0464B81E0BBF21CFC2F08343966734B30 * value)
	{
		___datatypeNonNegativeInteger_24 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNonNegativeInteger_24), value);
	}

	inline static int32_t get_offset_of_datatypePositiveInteger_25() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypePositiveInteger_25)); }
	inline XsdPositiveInteger_t8089829DDE14DBD3EEAD11DD4EBA8F4658FA04B2 * get_datatypePositiveInteger_25() const { return ___datatypePositiveInteger_25; }
	inline XsdPositiveInteger_t8089829DDE14DBD3EEAD11DD4EBA8F4658FA04B2 ** get_address_of_datatypePositiveInteger_25() { return &___datatypePositiveInteger_25; }
	inline void set_datatypePositiveInteger_25(XsdPositiveInteger_t8089829DDE14DBD3EEAD11DD4EBA8F4658FA04B2 * value)
	{
		___datatypePositiveInteger_25 = value;
		Il2CppCodeGenWriteBarrier((&___datatypePositiveInteger_25), value);
	}

	inline static int32_t get_offset_of_datatypeUnsignedLong_26() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeUnsignedLong_26)); }
	inline XsdUnsignedLong_t7B886E29EBA85082057263BFE0B9F644FF4D59B9 * get_datatypeUnsignedLong_26() const { return ___datatypeUnsignedLong_26; }
	inline XsdUnsignedLong_t7B886E29EBA85082057263BFE0B9F644FF4D59B9 ** get_address_of_datatypeUnsignedLong_26() { return &___datatypeUnsignedLong_26; }
	inline void set_datatypeUnsignedLong_26(XsdUnsignedLong_t7B886E29EBA85082057263BFE0B9F644FF4D59B9 * value)
	{
		___datatypeUnsignedLong_26 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUnsignedLong_26), value);
	}

	inline static int32_t get_offset_of_datatypeUnsignedInt_27() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeUnsignedInt_27)); }
	inline XsdUnsignedInt_t01075FD84457AE05E045BB23BD0E3A82AC30CE32 * get_datatypeUnsignedInt_27() const { return ___datatypeUnsignedInt_27; }
	inline XsdUnsignedInt_t01075FD84457AE05E045BB23BD0E3A82AC30CE32 ** get_address_of_datatypeUnsignedInt_27() { return &___datatypeUnsignedInt_27; }
	inline void set_datatypeUnsignedInt_27(XsdUnsignedInt_t01075FD84457AE05E045BB23BD0E3A82AC30CE32 * value)
	{
		___datatypeUnsignedInt_27 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUnsignedInt_27), value);
	}

	inline static int32_t get_offset_of_datatypeUnsignedShort_28() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeUnsignedShort_28)); }
	inline XsdUnsignedShort_tA832C283CB5937E31E78F3E4BBC85F90F92CD785 * get_datatypeUnsignedShort_28() const { return ___datatypeUnsignedShort_28; }
	inline XsdUnsignedShort_tA832C283CB5937E31E78F3E4BBC85F90F92CD785 ** get_address_of_datatypeUnsignedShort_28() { return &___datatypeUnsignedShort_28; }
	inline void set_datatypeUnsignedShort_28(XsdUnsignedShort_tA832C283CB5937E31E78F3E4BBC85F90F92CD785 * value)
	{
		___datatypeUnsignedShort_28 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUnsignedShort_28), value);
	}

	inline static int32_t get_offset_of_datatypeUnsignedByte_29() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeUnsignedByte_29)); }
	inline XsdUnsignedByte_tA702674830BFF7B5B4ADF325F30A781366A45256 * get_datatypeUnsignedByte_29() const { return ___datatypeUnsignedByte_29; }
	inline XsdUnsignedByte_tA702674830BFF7B5B4ADF325F30A781366A45256 ** get_address_of_datatypeUnsignedByte_29() { return &___datatypeUnsignedByte_29; }
	inline void set_datatypeUnsignedByte_29(XsdUnsignedByte_tA702674830BFF7B5B4ADF325F30A781366A45256 * value)
	{
		___datatypeUnsignedByte_29 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUnsignedByte_29), value);
	}

	inline static int32_t get_offset_of_datatypeNonPositiveInteger_30() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeNonPositiveInteger_30)); }
	inline XsdNonPositiveInteger_t960302EB6DDE4ECE0961BC6BBE4FAFA3C3E5BE62 * get_datatypeNonPositiveInteger_30() const { return ___datatypeNonPositiveInteger_30; }
	inline XsdNonPositiveInteger_t960302EB6DDE4ECE0961BC6BBE4FAFA3C3E5BE62 ** get_address_of_datatypeNonPositiveInteger_30() { return &___datatypeNonPositiveInteger_30; }
	inline void set_datatypeNonPositiveInteger_30(XsdNonPositiveInteger_t960302EB6DDE4ECE0961BC6BBE4FAFA3C3E5BE62 * value)
	{
		___datatypeNonPositiveInteger_30 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNonPositiveInteger_30), value);
	}

	inline static int32_t get_offset_of_datatypeNegativeInteger_31() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeNegativeInteger_31)); }
	inline XsdNegativeInteger_t5BF8A2EE42840B88A3C798FC6704B5AA9EDA715B * get_datatypeNegativeInteger_31() const { return ___datatypeNegativeInteger_31; }
	inline XsdNegativeInteger_t5BF8A2EE42840B88A3C798FC6704B5AA9EDA715B ** get_address_of_datatypeNegativeInteger_31() { return &___datatypeNegativeInteger_31; }
	inline void set_datatypeNegativeInteger_31(XsdNegativeInteger_t5BF8A2EE42840B88A3C798FC6704B5AA9EDA715B * value)
	{
		___datatypeNegativeInteger_31 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNegativeInteger_31), value);
	}

	inline static int32_t get_offset_of_datatypeFloat_32() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeFloat_32)); }
	inline XsdFloat_t02419495B090187B99AC33E40CD6601F7608D9B8 * get_datatypeFloat_32() const { return ___datatypeFloat_32; }
	inline XsdFloat_t02419495B090187B99AC33E40CD6601F7608D9B8 ** get_address_of_datatypeFloat_32() { return &___datatypeFloat_32; }
	inline void set_datatypeFloat_32(XsdFloat_t02419495B090187B99AC33E40CD6601F7608D9B8 * value)
	{
		___datatypeFloat_32 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeFloat_32), value);
	}

	inline static int32_t get_offset_of_datatypeDouble_33() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeDouble_33)); }
	inline XsdDouble_tAF166F167258B898C01C97CE94720CA2EDB5E269 * get_datatypeDouble_33() const { return ___datatypeDouble_33; }
	inline XsdDouble_tAF166F167258B898C01C97CE94720CA2EDB5E269 ** get_address_of_datatypeDouble_33() { return &___datatypeDouble_33; }
	inline void set_datatypeDouble_33(XsdDouble_tAF166F167258B898C01C97CE94720CA2EDB5E269 * value)
	{
		___datatypeDouble_33 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDouble_33), value);
	}

	inline static int32_t get_offset_of_datatypeBase64Binary_34() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeBase64Binary_34)); }
	inline XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F * get_datatypeBase64Binary_34() const { return ___datatypeBase64Binary_34; }
	inline XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F ** get_address_of_datatypeBase64Binary_34() { return &___datatypeBase64Binary_34; }
	inline void set_datatypeBase64Binary_34(XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F * value)
	{
		___datatypeBase64Binary_34 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeBase64Binary_34), value);
	}

	inline static int32_t get_offset_of_datatypeBoolean_35() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeBoolean_35)); }
	inline XsdBoolean_tAA5FDC2ACA9F0A05A686F75BFE68CBB53A2F9959 * get_datatypeBoolean_35() const { return ___datatypeBoolean_35; }
	inline XsdBoolean_tAA5FDC2ACA9F0A05A686F75BFE68CBB53A2F9959 ** get_address_of_datatypeBoolean_35() { return &___datatypeBoolean_35; }
	inline void set_datatypeBoolean_35(XsdBoolean_tAA5FDC2ACA9F0A05A686F75BFE68CBB53A2F9959 * value)
	{
		___datatypeBoolean_35 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeBoolean_35), value);
	}

	inline static int32_t get_offset_of_datatypeAnyURI_36() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeAnyURI_36)); }
	inline XsdAnyURI_t049F3807506793286A90A0A279CADC8C39D4647A * get_datatypeAnyURI_36() const { return ___datatypeAnyURI_36; }
	inline XsdAnyURI_t049F3807506793286A90A0A279CADC8C39D4647A ** get_address_of_datatypeAnyURI_36() { return &___datatypeAnyURI_36; }
	inline void set_datatypeAnyURI_36(XsdAnyURI_t049F3807506793286A90A0A279CADC8C39D4647A * value)
	{
		___datatypeAnyURI_36 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeAnyURI_36), value);
	}

	inline static int32_t get_offset_of_datatypeDuration_37() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeDuration_37)); }
	inline XsdDuration_tA3253842BF902691A000BBFD87382247985D5E78 * get_datatypeDuration_37() const { return ___datatypeDuration_37; }
	inline XsdDuration_tA3253842BF902691A000BBFD87382247985D5E78 ** get_address_of_datatypeDuration_37() { return &___datatypeDuration_37; }
	inline void set_datatypeDuration_37(XsdDuration_tA3253842BF902691A000BBFD87382247985D5E78 * value)
	{
		___datatypeDuration_37 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDuration_37), value);
	}

	inline static int32_t get_offset_of_datatypeDateTime_38() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeDateTime_38)); }
	inline XsdDateTime_tC31D6EF56B47537535EEC097FEBF63553C479E5D * get_datatypeDateTime_38() const { return ___datatypeDateTime_38; }
	inline XsdDateTime_tC31D6EF56B47537535EEC097FEBF63553C479E5D ** get_address_of_datatypeDateTime_38() { return &___datatypeDateTime_38; }
	inline void set_datatypeDateTime_38(XsdDateTime_tC31D6EF56B47537535EEC097FEBF63553C479E5D * value)
	{
		___datatypeDateTime_38 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDateTime_38), value);
	}

	inline static int32_t get_offset_of_datatypeDate_39() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeDate_39)); }
	inline XsdDate_t5536E073BDA86CBF86DE829550F870C6D7F89964 * get_datatypeDate_39() const { return ___datatypeDate_39; }
	inline XsdDate_t5536E073BDA86CBF86DE829550F870C6D7F89964 ** get_address_of_datatypeDate_39() { return &___datatypeDate_39; }
	inline void set_datatypeDate_39(XsdDate_t5536E073BDA86CBF86DE829550F870C6D7F89964 * value)
	{
		___datatypeDate_39 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDate_39), value);
	}

	inline static int32_t get_offset_of_datatypeTime_40() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeTime_40)); }
	inline XsdTime_tDB0973DB314925C984AC13CB795FD91589AB462C * get_datatypeTime_40() const { return ___datatypeTime_40; }
	inline XsdTime_tDB0973DB314925C984AC13CB795FD91589AB462C ** get_address_of_datatypeTime_40() { return &___datatypeTime_40; }
	inline void set_datatypeTime_40(XsdTime_tDB0973DB314925C984AC13CB795FD91589AB462C * value)
	{
		___datatypeTime_40 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeTime_40), value);
	}

	inline static int32_t get_offset_of_datatypeHexBinary_41() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeHexBinary_41)); }
	inline XsdHexBinary_t542DD47AB586F148DAE0698D4FC956D0412C4229 * get_datatypeHexBinary_41() const { return ___datatypeHexBinary_41; }
	inline XsdHexBinary_t542DD47AB586F148DAE0698D4FC956D0412C4229 ** get_address_of_datatypeHexBinary_41() { return &___datatypeHexBinary_41; }
	inline void set_datatypeHexBinary_41(XsdHexBinary_t542DD47AB586F148DAE0698D4FC956D0412C4229 * value)
	{
		___datatypeHexBinary_41 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeHexBinary_41), value);
	}

	inline static int32_t get_offset_of_datatypeQName_42() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeQName_42)); }
	inline XsdQName_t6E0BD13D8BFE16A59C49FAA8D331CA5FF5066DA0 * get_datatypeQName_42() const { return ___datatypeQName_42; }
	inline XsdQName_t6E0BD13D8BFE16A59C49FAA8D331CA5FF5066DA0 ** get_address_of_datatypeQName_42() { return &___datatypeQName_42; }
	inline void set_datatypeQName_42(XsdQName_t6E0BD13D8BFE16A59C49FAA8D331CA5FF5066DA0 * value)
	{
		___datatypeQName_42 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeQName_42), value);
	}

	inline static int32_t get_offset_of_datatypeGYearMonth_43() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeGYearMonth_43)); }
	inline XsdGYearMonth_t775E38939959BAF7304B4212BD8406456781C385 * get_datatypeGYearMonth_43() const { return ___datatypeGYearMonth_43; }
	inline XsdGYearMonth_t775E38939959BAF7304B4212BD8406456781C385 ** get_address_of_datatypeGYearMonth_43() { return &___datatypeGYearMonth_43; }
	inline void set_datatypeGYearMonth_43(XsdGYearMonth_t775E38939959BAF7304B4212BD8406456781C385 * value)
	{
		___datatypeGYearMonth_43 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGYearMonth_43), value);
	}

	inline static int32_t get_offset_of_datatypeGMonthDay_44() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeGMonthDay_44)); }
	inline XsdGMonthDay_t25E4C24A7DF2B6F4528C0271813BF8AF052F5669 * get_datatypeGMonthDay_44() const { return ___datatypeGMonthDay_44; }
	inline XsdGMonthDay_t25E4C24A7DF2B6F4528C0271813BF8AF052F5669 ** get_address_of_datatypeGMonthDay_44() { return &___datatypeGMonthDay_44; }
	inline void set_datatypeGMonthDay_44(XsdGMonthDay_t25E4C24A7DF2B6F4528C0271813BF8AF052F5669 * value)
	{
		___datatypeGMonthDay_44 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGMonthDay_44), value);
	}

	inline static int32_t get_offset_of_datatypeGYear_45() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeGYear_45)); }
	inline XsdGYear_t3A3FECDF3BF0A8EDC0776CA5D478A2394CC894F2 * get_datatypeGYear_45() const { return ___datatypeGYear_45; }
	inline XsdGYear_t3A3FECDF3BF0A8EDC0776CA5D478A2394CC894F2 ** get_address_of_datatypeGYear_45() { return &___datatypeGYear_45; }
	inline void set_datatypeGYear_45(XsdGYear_t3A3FECDF3BF0A8EDC0776CA5D478A2394CC894F2 * value)
	{
		___datatypeGYear_45 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGYear_45), value);
	}

	inline static int32_t get_offset_of_datatypeGMonth_46() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeGMonth_46)); }
	inline XsdGMonth_t6E98934D7FAFFBCFA788D4068EB7DAE6706F4312 * get_datatypeGMonth_46() const { return ___datatypeGMonth_46; }
	inline XsdGMonth_t6E98934D7FAFFBCFA788D4068EB7DAE6706F4312 ** get_address_of_datatypeGMonth_46() { return &___datatypeGMonth_46; }
	inline void set_datatypeGMonth_46(XsdGMonth_t6E98934D7FAFFBCFA788D4068EB7DAE6706F4312 * value)
	{
		___datatypeGMonth_46 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGMonth_46), value);
	}

	inline static int32_t get_offset_of_datatypeGDay_47() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeGDay_47)); }
	inline XsdGDay_tEC936DF85B2878EB240E093D4B56BBDDC75A463B * get_datatypeGDay_47() const { return ___datatypeGDay_47; }
	inline XsdGDay_tEC936DF85B2878EB240E093D4B56BBDDC75A463B ** get_address_of_datatypeGDay_47() { return &___datatypeGDay_47; }
	inline void set_datatypeGDay_47(XsdGDay_tEC936DF85B2878EB240E093D4B56BBDDC75A463B * value)
	{
		___datatypeGDay_47 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGDay_47), value);
	}

	inline static int32_t get_offset_of_datatypeAnyAtomicType_48() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeAnyAtomicType_48)); }
	inline XdtAnyAtomicType_t922A677221DBA603ACF1F4D2024F9222102416D7 * get_datatypeAnyAtomicType_48() const { return ___datatypeAnyAtomicType_48; }
	inline XdtAnyAtomicType_t922A677221DBA603ACF1F4D2024F9222102416D7 ** get_address_of_datatypeAnyAtomicType_48() { return &___datatypeAnyAtomicType_48; }
	inline void set_datatypeAnyAtomicType_48(XdtAnyAtomicType_t922A677221DBA603ACF1F4D2024F9222102416D7 * value)
	{
		___datatypeAnyAtomicType_48 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeAnyAtomicType_48), value);
	}

	inline static int32_t get_offset_of_datatypeUntypedAtomic_49() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeUntypedAtomic_49)); }
	inline XdtUntypedAtomic_t8E0FD4A48C677AA7C2D946690BE85E92CE81CDED * get_datatypeUntypedAtomic_49() const { return ___datatypeUntypedAtomic_49; }
	inline XdtUntypedAtomic_t8E0FD4A48C677AA7C2D946690BE85E92CE81CDED ** get_address_of_datatypeUntypedAtomic_49() { return &___datatypeUntypedAtomic_49; }
	inline void set_datatypeUntypedAtomic_49(XdtUntypedAtomic_t8E0FD4A48C677AA7C2D946690BE85E92CE81CDED * value)
	{
		___datatypeUntypedAtomic_49 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUntypedAtomic_49), value);
	}

	inline static int32_t get_offset_of_datatypeDayTimeDuration_50() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeDayTimeDuration_50)); }
	inline XdtDayTimeDuration_t14B50E7DF624721215FA0E49D3672BAAFF59027F * get_datatypeDayTimeDuration_50() const { return ___datatypeDayTimeDuration_50; }
	inline XdtDayTimeDuration_t14B50E7DF624721215FA0E49D3672BAAFF59027F ** get_address_of_datatypeDayTimeDuration_50() { return &___datatypeDayTimeDuration_50; }
	inline void set_datatypeDayTimeDuration_50(XdtDayTimeDuration_t14B50E7DF624721215FA0E49D3672BAAFF59027F * value)
	{
		___datatypeDayTimeDuration_50 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDayTimeDuration_50), value);
	}

	inline static int32_t get_offset_of_datatypeYearMonthDuration_51() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeYearMonthDuration_51)); }
	inline XdtYearMonthDuration_t940953B7258AE5DEC65A9F65D90EC70D9786EA68 * get_datatypeYearMonthDuration_51() const { return ___datatypeYearMonthDuration_51; }
	inline XdtYearMonthDuration_t940953B7258AE5DEC65A9F65D90EC70D9786EA68 ** get_address_of_datatypeYearMonthDuration_51() { return &___datatypeYearMonthDuration_51; }
	inline void set_datatypeYearMonthDuration_51(XdtYearMonthDuration_t940953B7258AE5DEC65A9F65D90EC70D9786EA68 * value)
	{
		___datatypeYearMonthDuration_51 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeYearMonthDuration_51), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2A_52() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___U3CU3Ef__switchU24map2A_52)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map2A_52() const { return ___U3CU3Ef__switchU24map2A_52; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map2A_52() { return &___U3CU3Ef__switchU24map2A_52; }
	inline void set_U3CU3Ef__switchU24map2A_52(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map2A_52 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2A_52), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2B_53() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___U3CU3Ef__switchU24map2B_53)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map2B_53() const { return ___U3CU3Ef__switchU24map2B_53; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map2B_53() { return &___U3CU3Ef__switchU24map2B_53; }
	inline void set_U3CU3Ef__switchU24map2B_53(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map2B_53 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2B_53), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2C_54() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___U3CU3Ef__switchU24map2C_54)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map2C_54() const { return ___U3CU3Ef__switchU24map2C_54; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map2C_54() { return &___U3CU3Ef__switchU24map2C_54; }
	inline void set_U3CU3Ef__switchU24map2C_54(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map2C_54 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2C_54), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADATATYPE_T3384DFF75456A27E693C24453B91B2B7A0CC2A19_H
#ifndef XSDANYSIMPLETYPE_T457003328EC5C522A2396822982C317B8DC684F0_H
#define XSDANYSIMPLETYPE_T457003328EC5C522A2396822982C317B8DC684F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdAnySimpleType
struct  XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0  : public XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19
{
public:

public:
};

struct XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields
{
public:
	// Mono.Xml.Schema.XsdAnySimpleType Mono.Xml.Schema.XsdAnySimpleType::instance
	XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0 * ___instance_55;
	// System.Char[] Mono.Xml.Schema.XsdAnySimpleType::whitespaceArray
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___whitespaceArray_56;
	// System.Xml.Schema.XmlSchemaFacet/Facet Mono.Xml.Schema.XsdAnySimpleType::booleanAllowedFacets
	int32_t ___booleanAllowedFacets_57;
	// System.Xml.Schema.XmlSchemaFacet/Facet Mono.Xml.Schema.XsdAnySimpleType::decimalAllowedFacets
	int32_t ___decimalAllowedFacets_58;
	// System.Xml.Schema.XmlSchemaFacet/Facet Mono.Xml.Schema.XsdAnySimpleType::durationAllowedFacets
	int32_t ___durationAllowedFacets_59;
	// System.Xml.Schema.XmlSchemaFacet/Facet Mono.Xml.Schema.XsdAnySimpleType::stringAllowedFacets
	int32_t ___stringAllowedFacets_60;

public:
	inline static int32_t get_offset_of_instance_55() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields, ___instance_55)); }
	inline XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0 * get_instance_55() const { return ___instance_55; }
	inline XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0 ** get_address_of_instance_55() { return &___instance_55; }
	inline void set_instance_55(XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0 * value)
	{
		___instance_55 = value;
		Il2CppCodeGenWriteBarrier((&___instance_55), value);
	}

	inline static int32_t get_offset_of_whitespaceArray_56() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields, ___whitespaceArray_56)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_whitespaceArray_56() const { return ___whitespaceArray_56; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_whitespaceArray_56() { return &___whitespaceArray_56; }
	inline void set_whitespaceArray_56(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___whitespaceArray_56 = value;
		Il2CppCodeGenWriteBarrier((&___whitespaceArray_56), value);
	}

	inline static int32_t get_offset_of_booleanAllowedFacets_57() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields, ___booleanAllowedFacets_57)); }
	inline int32_t get_booleanAllowedFacets_57() const { return ___booleanAllowedFacets_57; }
	inline int32_t* get_address_of_booleanAllowedFacets_57() { return &___booleanAllowedFacets_57; }
	inline void set_booleanAllowedFacets_57(int32_t value)
	{
		___booleanAllowedFacets_57 = value;
	}

	inline static int32_t get_offset_of_decimalAllowedFacets_58() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields, ___decimalAllowedFacets_58)); }
	inline int32_t get_decimalAllowedFacets_58() const { return ___decimalAllowedFacets_58; }
	inline int32_t* get_address_of_decimalAllowedFacets_58() { return &___decimalAllowedFacets_58; }
	inline void set_decimalAllowedFacets_58(int32_t value)
	{
		___decimalAllowedFacets_58 = value;
	}

	inline static int32_t get_offset_of_durationAllowedFacets_59() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields, ___durationAllowedFacets_59)); }
	inline int32_t get_durationAllowedFacets_59() const { return ___durationAllowedFacets_59; }
	inline int32_t* get_address_of_durationAllowedFacets_59() { return &___durationAllowedFacets_59; }
	inline void set_durationAllowedFacets_59(int32_t value)
	{
		___durationAllowedFacets_59 = value;
	}

	inline static int32_t get_offset_of_stringAllowedFacets_60() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields, ___stringAllowedFacets_60)); }
	inline int32_t get_stringAllowedFacets_60() const { return ___stringAllowedFacets_60; }
	inline int32_t* get_address_of_stringAllowedFacets_60() { return &___stringAllowedFacets_60; }
	inline void set_stringAllowedFacets_60(int32_t value)
	{
		___stringAllowedFacets_60 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDANYSIMPLETYPE_T457003328EC5C522A2396822982C317B8DC684F0_H
#ifndef XDTANYATOMICTYPE_T922A677221DBA603ACF1F4D2024F9222102416D7_H
#define XDTANYATOMICTYPE_T922A677221DBA603ACF1F4D2024F9222102416D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XdtAnyAtomicType
struct  XdtAnyAtomicType_t922A677221DBA603ACF1F4D2024F9222102416D7  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDTANYATOMICTYPE_T922A677221DBA603ACF1F4D2024F9222102416D7_H
#ifndef XSDBOOLEAN_TAA5FDC2ACA9F0A05A686F75BFE68CBB53A2F9959_H
#define XSDBOOLEAN_TAA5FDC2ACA9F0A05A686F75BFE68CBB53A2F9959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdBoolean
struct  XsdBoolean_tAA5FDC2ACA9F0A05A686F75BFE68CBB53A2F9959  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDBOOLEAN_TAA5FDC2ACA9F0A05A686F75BFE68CBB53A2F9959_H
#ifndef XSDDATE_T5536E073BDA86CBF86DE829550F870C6D7F89964_H
#define XSDDATE_T5536E073BDA86CBF86DE829550F870C6D7F89964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDate
struct  XsdDate_t5536E073BDA86CBF86DE829550F870C6D7F89964  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDATE_T5536E073BDA86CBF86DE829550F870C6D7F89964_H
#ifndef XSDDATETIME_TC31D6EF56B47537535EEC097FEBF63553C479E5D_H
#define XSDDATETIME_TC31D6EF56B47537535EEC097FEBF63553C479E5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDateTime
struct  XsdDateTime_tC31D6EF56B47537535EEC097FEBF63553C479E5D  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDATETIME_TC31D6EF56B47537535EEC097FEBF63553C479E5D_H
#ifndef XSDDECIMAL_T1CB6398C98F43AF142671E2E9BDAB05D58AEE9EE_H
#define XSDDECIMAL_T1CB6398C98F43AF142671E2E9BDAB05D58AEE9EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDecimal
struct  XsdDecimal_t1CB6398C98F43AF142671E2E9BDAB05D58AEE9EE  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDECIMAL_T1CB6398C98F43AF142671E2E9BDAB05D58AEE9EE_H
#ifndef XSDDOUBLE_TAF166F167258B898C01C97CE94720CA2EDB5E269_H
#define XSDDOUBLE_TAF166F167258B898C01C97CE94720CA2EDB5E269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDouble
struct  XsdDouble_tAF166F167258B898C01C97CE94720CA2EDB5E269  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDOUBLE_TAF166F167258B898C01C97CE94720CA2EDB5E269_H
#ifndef XSDDURATION_TA3253842BF902691A000BBFD87382247985D5E78_H
#define XSDDURATION_TA3253842BF902691A000BBFD87382247985D5E78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDuration
struct  XsdDuration_tA3253842BF902691A000BBFD87382247985D5E78  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDURATION_TA3253842BF902691A000BBFD87382247985D5E78_H
#ifndef XSDFLOAT_T02419495B090187B99AC33E40CD6601F7608D9B8_H
#define XSDFLOAT_T02419495B090187B99AC33E40CD6601F7608D9B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdFloat
struct  XsdFloat_t02419495B090187B99AC33E40CD6601F7608D9B8  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDFLOAT_T02419495B090187B99AC33E40CD6601F7608D9B8_H
#ifndef XSDGMONTH_T6E98934D7FAFFBCFA788D4068EB7DAE6706F4312_H
#define XSDGMONTH_T6E98934D7FAFFBCFA788D4068EB7DAE6706F4312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdGMonth
struct  XsdGMonth_t6E98934D7FAFFBCFA788D4068EB7DAE6706F4312  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDGMONTH_T6E98934D7FAFFBCFA788D4068EB7DAE6706F4312_H
#ifndef XSDGMONTHDAY_T25E4C24A7DF2B6F4528C0271813BF8AF052F5669_H
#define XSDGMONTHDAY_T25E4C24A7DF2B6F4528C0271813BF8AF052F5669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdGMonthDay
struct  XsdGMonthDay_t25E4C24A7DF2B6F4528C0271813BF8AF052F5669  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDGMONTHDAY_T25E4C24A7DF2B6F4528C0271813BF8AF052F5669_H
#ifndef XSDGYEAR_T3A3FECDF3BF0A8EDC0776CA5D478A2394CC894F2_H
#define XSDGYEAR_T3A3FECDF3BF0A8EDC0776CA5D478A2394CC894F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdGYear
struct  XsdGYear_t3A3FECDF3BF0A8EDC0776CA5D478A2394CC894F2  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDGYEAR_T3A3FECDF3BF0A8EDC0776CA5D478A2394CC894F2_H
#ifndef XSDGYEARMONTH_T775E38939959BAF7304B4212BD8406456781C385_H
#define XSDGYEARMONTH_T775E38939959BAF7304B4212BD8406456781C385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdGYearMonth
struct  XsdGYearMonth_t775E38939959BAF7304B4212BD8406456781C385  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDGYEARMONTH_T775E38939959BAF7304B4212BD8406456781C385_H
#ifndef XSDHEXBINARY_T542DD47AB586F148DAE0698D4FC956D0412C4229_H
#define XSDHEXBINARY_T542DD47AB586F148DAE0698D4FC956D0412C4229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdHexBinary
struct  XsdHexBinary_t542DD47AB586F148DAE0698D4FC956D0412C4229  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDHEXBINARY_T542DD47AB586F148DAE0698D4FC956D0412C4229_H
#ifndef XSDNOTATION_T619CA8B98E6B358DAEE751C97934638D691DB5A3_H
#define XSDNOTATION_T619CA8B98E6B358DAEE751C97934638D691DB5A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNotation
struct  XsdNotation_t619CA8B98E6B358DAEE751C97934638D691DB5A3  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNOTATION_T619CA8B98E6B358DAEE751C97934638D691DB5A3_H
#ifndef XSDSTRING_T604253BF4F01F3A649275313E7AED2D8BCB854AD_H
#define XSDSTRING_T604253BF4F01F3A649275313E7AED2D8BCB854AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdString
struct  XsdString_t604253BF4F01F3A649275313E7AED2D8BCB854AD  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDSTRING_T604253BF4F01F3A649275313E7AED2D8BCB854AD_H
#ifndef XSDTIME_TDB0973DB314925C984AC13CB795FD91589AB462C_H
#define XSDTIME_TDB0973DB314925C984AC13CB795FD91589AB462C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdTime
struct  XsdTime_tDB0973DB314925C984AC13CB795FD91589AB462C  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

struct XsdTime_tDB0973DB314925C984AC13CB795FD91589AB462C_StaticFields
{
public:
	// System.String[] Mono.Xml.Schema.XsdTime::timeFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___timeFormats_61;

public:
	inline static int32_t get_offset_of_timeFormats_61() { return static_cast<int32_t>(offsetof(XsdTime_tDB0973DB314925C984AC13CB795FD91589AB462C_StaticFields, ___timeFormats_61)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_timeFormats_61() const { return ___timeFormats_61; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_timeFormats_61() { return &___timeFormats_61; }
	inline void set_timeFormats_61(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___timeFormats_61 = value;
		Il2CppCodeGenWriteBarrier((&___timeFormats_61), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDTIME_TDB0973DB314925C984AC13CB795FD91589AB462C_H
#ifndef XDTDAYTIMEDURATION_T14B50E7DF624721215FA0E49D3672BAAFF59027F_H
#define XDTDAYTIMEDURATION_T14B50E7DF624721215FA0E49D3672BAAFF59027F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XdtDayTimeDuration
struct  XdtDayTimeDuration_t14B50E7DF624721215FA0E49D3672BAAFF59027F  : public XsdDuration_tA3253842BF902691A000BBFD87382247985D5E78
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDTDAYTIMEDURATION_T14B50E7DF624721215FA0E49D3672BAAFF59027F_H
#ifndef XDTUNTYPEDATOMIC_T8E0FD4A48C677AA7C2D946690BE85E92CE81CDED_H
#define XDTUNTYPEDATOMIC_T8E0FD4A48C677AA7C2D946690BE85E92CE81CDED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XdtUntypedAtomic
struct  XdtUntypedAtomic_t8E0FD4A48C677AA7C2D946690BE85E92CE81CDED  : public XdtAnyAtomicType_t922A677221DBA603ACF1F4D2024F9222102416D7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDTUNTYPEDATOMIC_T8E0FD4A48C677AA7C2D946690BE85E92CE81CDED_H
#ifndef XDTYEARMONTHDURATION_T940953B7258AE5DEC65A9F65D90EC70D9786EA68_H
#define XDTYEARMONTHDURATION_T940953B7258AE5DEC65A9F65D90EC70D9786EA68_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XdtYearMonthDuration
struct  XdtYearMonthDuration_t940953B7258AE5DEC65A9F65D90EC70D9786EA68  : public XsdDuration_tA3253842BF902691A000BBFD87382247985D5E78
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDTYEARMONTHDURATION_T940953B7258AE5DEC65A9F65D90EC70D9786EA68_H
#ifndef XSDANYURI_T049F3807506793286A90A0A279CADC8C39D4647A_H
#define XSDANYURI_T049F3807506793286A90A0A279CADC8C39D4647A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdAnyURI
struct  XsdAnyURI_t049F3807506793286A90A0A279CADC8C39D4647A  : public XsdString_t604253BF4F01F3A649275313E7AED2D8BCB854AD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDANYURI_T049F3807506793286A90A0A279CADC8C39D4647A_H
#ifndef XSDBASE64BINARY_T2EBF4685DF4837949C9AA51C1DAA031B7259405F_H
#define XSDBASE64BINARY_T2EBF4685DF4837949C9AA51C1DAA031B7259405F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdBase64Binary
struct  XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F  : public XsdString_t604253BF4F01F3A649275313E7AED2D8BCB854AD
{
public:

public:
};

struct XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F_StaticFields
{
public:
	// System.String Mono.Xml.Schema.XsdBase64Binary::ALPHABET
	String_t* ___ALPHABET_61;
	// System.Byte[] Mono.Xml.Schema.XsdBase64Binary::decodeTable
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___decodeTable_62;

public:
	inline static int32_t get_offset_of_ALPHABET_61() { return static_cast<int32_t>(offsetof(XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F_StaticFields, ___ALPHABET_61)); }
	inline String_t* get_ALPHABET_61() const { return ___ALPHABET_61; }
	inline String_t** get_address_of_ALPHABET_61() { return &___ALPHABET_61; }
	inline void set_ALPHABET_61(String_t* value)
	{
		___ALPHABET_61 = value;
		Il2CppCodeGenWriteBarrier((&___ALPHABET_61), value);
	}

	inline static int32_t get_offset_of_decodeTable_62() { return static_cast<int32_t>(offsetof(XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F_StaticFields, ___decodeTable_62)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_decodeTable_62() const { return ___decodeTable_62; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_decodeTable_62() { return &___decodeTable_62; }
	inline void set_decodeTable_62(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___decodeTable_62 = value;
		Il2CppCodeGenWriteBarrier((&___decodeTable_62), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDBASE64BINARY_T2EBF4685DF4837949C9AA51C1DAA031B7259405F_H
#ifndef XSDINTEGER_TE61DFB650D9B7BD3F6CA2C7DD4331667015CA348_H
#define XSDINTEGER_TE61DFB650D9B7BD3F6CA2C7DD4331667015CA348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdInteger
struct  XsdInteger_tE61DFB650D9B7BD3F6CA2C7DD4331667015CA348  : public XsdDecimal_t1CB6398C98F43AF142671E2E9BDAB05D58AEE9EE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDINTEGER_TE61DFB650D9B7BD3F6CA2C7DD4331667015CA348_H
#ifndef XSDNORMALIZEDSTRING_TEAAA641AA303B7A5227D53147AACC07A82EE13C0_H
#define XSDNORMALIZEDSTRING_TEAAA641AA303B7A5227D53147AACC07A82EE13C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNormalizedString
struct  XsdNormalizedString_tEAAA641AA303B7A5227D53147AACC07A82EE13C0  : public XsdString_t604253BF4F01F3A649275313E7AED2D8BCB854AD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNORMALIZEDSTRING_TEAAA641AA303B7A5227D53147AACC07A82EE13C0_H
#ifndef XSDLONG_T6FCDD94BFBBEC0874CD9166C160CF7B60D36B5FC_H
#define XSDLONG_T6FCDD94BFBBEC0874CD9166C160CF7B60D36B5FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdLong
struct  XsdLong_t6FCDD94BFBBEC0874CD9166C160CF7B60D36B5FC  : public XsdInteger_tE61DFB650D9B7BD3F6CA2C7DD4331667015CA348
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDLONG_T6FCDD94BFBBEC0874CD9166C160CF7B60D36B5FC_H
#ifndef XSDNONNEGATIVEINTEGER_T0F4F93B0464B81E0BBF21CFC2F08343966734B30_H
#define XSDNONNEGATIVEINTEGER_T0F4F93B0464B81E0BBF21CFC2F08343966734B30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNonNegativeInteger
struct  XsdNonNegativeInteger_t0F4F93B0464B81E0BBF21CFC2F08343966734B30  : public XsdInteger_tE61DFB650D9B7BD3F6CA2C7DD4331667015CA348
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNONNEGATIVEINTEGER_T0F4F93B0464B81E0BBF21CFC2F08343966734B30_H
#ifndef XSDNONPOSITIVEINTEGER_T960302EB6DDE4ECE0961BC6BBE4FAFA3C3E5BE62_H
#define XSDNONPOSITIVEINTEGER_T960302EB6DDE4ECE0961BC6BBE4FAFA3C3E5BE62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNonPositiveInteger
struct  XsdNonPositiveInteger_t960302EB6DDE4ECE0961BC6BBE4FAFA3C3E5BE62  : public XsdInteger_tE61DFB650D9B7BD3F6CA2C7DD4331667015CA348
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNONPOSITIVEINTEGER_T960302EB6DDE4ECE0961BC6BBE4FAFA3C3E5BE62_H
#ifndef XSDTOKEN_T4A120CAA60CECDF6DCDC15717ABCA63187B0D60C_H
#define XSDTOKEN_T4A120CAA60CECDF6DCDC15717ABCA63187B0D60C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdToken
struct  XsdToken_t4A120CAA60CECDF6DCDC15717ABCA63187B0D60C  : public XsdNormalizedString_tEAAA641AA303B7A5227D53147AACC07A82EE13C0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDTOKEN_T4A120CAA60CECDF6DCDC15717ABCA63187B0D60C_H
#ifndef XSDINT_T6F6E692FF9941520F897A726ABDE415CCEFC984F_H
#define XSDINT_T6F6E692FF9941520F897A726ABDE415CCEFC984F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdInt
struct  XsdInt_t6F6E692FF9941520F897A726ABDE415CCEFC984F  : public XsdLong_t6FCDD94BFBBEC0874CD9166C160CF7B60D36B5FC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDINT_T6F6E692FF9941520F897A726ABDE415CCEFC984F_H
#ifndef XSDLANGUAGE_T392B00FB85420B861413B7295BC68EA7C0982D85_H
#define XSDLANGUAGE_T392B00FB85420B861413B7295BC68EA7C0982D85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdLanguage
struct  XsdLanguage_t392B00FB85420B861413B7295BC68EA7C0982D85  : public XsdToken_t4A120CAA60CECDF6DCDC15717ABCA63187B0D60C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDLANGUAGE_T392B00FB85420B861413B7295BC68EA7C0982D85_H
#ifndef XSDNMTOKEN_TED82C02272B75D49D76B0FC44858D09D8F06AB91_H
#define XSDNMTOKEN_TED82C02272B75D49D76B0FC44858D09D8F06AB91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNMToken
struct  XsdNMToken_tED82C02272B75D49D76B0FC44858D09D8F06AB91  : public XsdToken_t4A120CAA60CECDF6DCDC15717ABCA63187B0D60C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNMTOKEN_TED82C02272B75D49D76B0FC44858D09D8F06AB91_H
#ifndef XSDNAME_T52D491BF6C35EDF8B16CE44E5677C37B75184CB8_H
#define XSDNAME_T52D491BF6C35EDF8B16CE44E5677C37B75184CB8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdName
struct  XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8  : public XsdToken_t4A120CAA60CECDF6DCDC15717ABCA63187B0D60C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNAME_T52D491BF6C35EDF8B16CE44E5677C37B75184CB8_H
#ifndef XSDNEGATIVEINTEGER_T5BF8A2EE42840B88A3C798FC6704B5AA9EDA715B_H
#define XSDNEGATIVEINTEGER_T5BF8A2EE42840B88A3C798FC6704B5AA9EDA715B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNegativeInteger
struct  XsdNegativeInteger_t5BF8A2EE42840B88A3C798FC6704B5AA9EDA715B  : public XsdNonPositiveInteger_t960302EB6DDE4ECE0961BC6BBE4FAFA3C3E5BE62
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNEGATIVEINTEGER_T5BF8A2EE42840B88A3C798FC6704B5AA9EDA715B_H
#ifndef XSDPOSITIVEINTEGER_T8089829DDE14DBD3EEAD11DD4EBA8F4658FA04B2_H
#define XSDPOSITIVEINTEGER_T8089829DDE14DBD3EEAD11DD4EBA8F4658FA04B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdPositiveInteger
struct  XsdPositiveInteger_t8089829DDE14DBD3EEAD11DD4EBA8F4658FA04B2  : public XsdNonNegativeInteger_t0F4F93B0464B81E0BBF21CFC2F08343966734B30
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDPOSITIVEINTEGER_T8089829DDE14DBD3EEAD11DD4EBA8F4658FA04B2_H
#ifndef XSDUNSIGNEDLONG_T7B886E29EBA85082057263BFE0B9F644FF4D59B9_H
#define XSDUNSIGNEDLONG_T7B886E29EBA85082057263BFE0B9F644FF4D59B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdUnsignedLong
struct  XsdUnsignedLong_t7B886E29EBA85082057263BFE0B9F644FF4D59B9  : public XsdNonNegativeInteger_t0F4F93B0464B81E0BBF21CFC2F08343966734B30
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDUNSIGNEDLONG_T7B886E29EBA85082057263BFE0B9F644FF4D59B9_H
#ifndef XSDENTITIES_TE41F357D8F259BBCCB58DF1E99D66720E8F2CF1C_H
#define XSDENTITIES_TE41F357D8F259BBCCB58DF1E99D66720E8F2CF1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdEntities
struct  XsdEntities_tE41F357D8F259BBCCB58DF1E99D66720E8F2CF1C  : public XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDENTITIES_TE41F357D8F259BBCCB58DF1E99D66720E8F2CF1C_H
#ifndef XSDENTITY_T426A9BE787702C55FF7FE5FE0FCDF582D5E50371_H
#define XSDENTITY_T426A9BE787702C55FF7FE5FE0FCDF582D5E50371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdEntity
struct  XsdEntity_t426A9BE787702C55FF7FE5FE0FCDF582D5E50371  : public XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDENTITY_T426A9BE787702C55FF7FE5FE0FCDF582D5E50371_H
#ifndef XSDID_TFC394CE8EE4F16A40F0063659AB8C788C712E77B_H
#define XSDID_TFC394CE8EE4F16A40F0063659AB8C788C712E77B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdID
struct  XsdID_tFC394CE8EE4F16A40F0063659AB8C788C712E77B  : public XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDID_TFC394CE8EE4F16A40F0063659AB8C788C712E77B_H
#ifndef XSDIDREF_TC8B88E328BA8745548A89206641A6C113AE9F09B_H
#define XSDIDREF_TC8B88E328BA8745548A89206641A6C113AE9F09B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIDRef
struct  XsdIDRef_tC8B88E328BA8745548A89206641A6C113AE9F09B  : public XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDREF_TC8B88E328BA8745548A89206641A6C113AE9F09B_H
#ifndef XSDIDREFS_T3598EB6F29A7BEC87962148242C3904C8FF5BB9F_H
#define XSDIDREFS_T3598EB6F29A7BEC87962148242C3904C8FF5BB9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIDRefs
struct  XsdIDRefs_t3598EB6F29A7BEC87962148242C3904C8FF5BB9F  : public XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDREFS_T3598EB6F29A7BEC87962148242C3904C8FF5BB9F_H
#ifndef XSDNCNAME_T7C66E8DC7486C5D92AF7739B71EC88C1FD11E2F7_H
#define XSDNCNAME_T7C66E8DC7486C5D92AF7739B71EC88C1FD11E2F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNCName
struct  XsdNCName_t7C66E8DC7486C5D92AF7739B71EC88C1FD11E2F7  : public XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNCNAME_T7C66E8DC7486C5D92AF7739B71EC88C1FD11E2F7_H
#ifndef XSDNMTOKENS_TA3A587C5F1FF49445F75C961B0280F58A57D1095_H
#define XSDNMTOKENS_TA3A587C5F1FF49445F75C961B0280F58A57D1095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNMTokens
struct  XsdNMTokens_tA3A587C5F1FF49445F75C961B0280F58A57D1095  : public XsdNMToken_tED82C02272B75D49D76B0FC44858D09D8F06AB91
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNMTOKENS_TA3A587C5F1FF49445F75C961B0280F58A57D1095_H
#ifndef XSDQNAME_T6E0BD13D8BFE16A59C49FAA8D331CA5FF5066DA0_H
#define XSDQNAME_T6E0BD13D8BFE16A59C49FAA8D331CA5FF5066DA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdQName
struct  XsdQName_t6E0BD13D8BFE16A59C49FAA8D331CA5FF5066DA0  : public XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDQNAME_T6E0BD13D8BFE16A59C49FAA8D331CA5FF5066DA0_H
#ifndef XSDSHORT_TAF41351D06667202EC3FDBC76501A2D091518332_H
#define XSDSHORT_TAF41351D06667202EC3FDBC76501A2D091518332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdShort
struct  XsdShort_tAF41351D06667202EC3FDBC76501A2D091518332  : public XsdInt_t6F6E692FF9941520F897A726ABDE415CCEFC984F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDSHORT_TAF41351D06667202EC3FDBC76501A2D091518332_H
#ifndef XSDUNSIGNEDINT_T01075FD84457AE05E045BB23BD0E3A82AC30CE32_H
#define XSDUNSIGNEDINT_T01075FD84457AE05E045BB23BD0E3A82AC30CE32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdUnsignedInt
struct  XsdUnsignedInt_t01075FD84457AE05E045BB23BD0E3A82AC30CE32  : public XsdUnsignedLong_t7B886E29EBA85082057263BFE0B9F644FF4D59B9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDUNSIGNEDINT_T01075FD84457AE05E045BB23BD0E3A82AC30CE32_H
#ifndef XSDBYTE_T048838D9FF5BFD18C9FC056F11C699E7E08FC351_H
#define XSDBYTE_T048838D9FF5BFD18C9FC056F11C699E7E08FC351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdByte
struct  XsdByte_t048838D9FF5BFD18C9FC056F11C699E7E08FC351  : public XsdShort_tAF41351D06667202EC3FDBC76501A2D091518332
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDBYTE_T048838D9FF5BFD18C9FC056F11C699E7E08FC351_H
#ifndef XSDUNSIGNEDSHORT_TA832C283CB5937E31E78F3E4BBC85F90F92CD785_H
#define XSDUNSIGNEDSHORT_TA832C283CB5937E31E78F3E4BBC85F90F92CD785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdUnsignedShort
struct  XsdUnsignedShort_tA832C283CB5937E31E78F3E4BBC85F90F92CD785  : public XsdUnsignedInt_t01075FD84457AE05E045BB23BD0E3A82AC30CE32
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDUNSIGNEDSHORT_TA832C283CB5937E31E78F3E4BBC85F90F92CD785_H
#ifndef XSDUNSIGNEDBYTE_TA702674830BFF7B5B4ADF325F30A781366A45256_H
#define XSDUNSIGNEDBYTE_TA702674830BFF7B5B4ADF325F30A781366A45256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdUnsignedByte
struct  XsdUnsignedByte_tA702674830BFF7B5B4ADF325F30A781366A45256  : public XsdUnsignedShort_tA832C283CB5937E31E78F3E4BBC85F90F92CD785
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDUNSIGNEDBYTE_TA702674830BFF7B5B4ADF325F30A781366A45256_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (PolygonPoint_t94267F9D8AC5851730C1D804609D51ABC86911E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2100[2] = 
{
	PolygonPoint_t94267F9D8AC5851730C1D804609D51ABC86911E4::get_offset_of_U3CNextU3Ek__BackingField_5(),
	PolygonPoint_t94267F9D8AC5851730C1D804609D51ABC86911E4::get_offset_of_U3CPreviousU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (Edge_tFB6F8F74D888B19F8D03A97A9CA65E60889DC672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2101[2] = 
{
	Edge_tFB6F8F74D888B19F8D03A97A9CA65E60889DC672::get_offset_of_mP_0(),
	Edge_tFB6F8F74D888B19F8D03A97A9CA65E60889DC672::get_offset_of_mQ_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (TriangulationConstraint_t1A7BE017E0DA33DF3619D2E27A1EA9E4A6A3C14C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2102[1] = 
{
	TriangulationConstraint_t1A7BE017E0DA33DF3619D2E27A1EA9E4A6A3C14C::get_offset_of_mContraintCode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2103[7] = 
{
	TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B::get_offset_of_U3CDebugContextU3Ek__BackingField_0(),
	TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B::get_offset_of_Triangles_1(),
	TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B::get_offset_of_Points_2(),
	TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B::get_offset_of_U3CTriangulationModeU3Ek__BackingField_3(),
	TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B::get_offset_of_U3CTriangulatableU3Ek__BackingField_4(),
	TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B::get_offset_of_U3CStepCountU3Ek__BackingField_5(),
	TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B::get_offset_of_U3CIsDebugEnabledU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (TriangulationDebugContext_t23D041A57AAB2224970A201625386E8BEB37D9A4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2104[1] = 
{
	TriangulationDebugContext_t23D041A57AAB2224970A201625386E8BEB37D9A4::get_offset_of__tcx_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (TriangulationMode_tF47A940AB02AFB9DC5163BB1B9AE1A85CD37764C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2105[4] = 
{
	TriangulationMode_tF47A940AB02AFB9DC5163BB1B9AE1A85CD37764C::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E), -1, sizeof(TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2106[3] = 
{
	TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E_StaticFields::get_offset_of_kVertexCodeDefaultPrecision_2(),
	TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E::get_offset_of_mVertexCode_3(),
	TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E::get_offset_of_U3CEdgesU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (TriangulationPointEnumerator_tC844E157B82CBD3122C315202426AB3BCDB471CE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2107[2] = 
{
	TriangulationPointEnumerator_tC844E157B82CBD3122C315202426AB3BCDB471CE::get_offset_of_mPoints_0(),
	TriangulationPointEnumerator_tC844E157B82CBD3122C315202426AB3BCDB471CE::get_offset_of_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (TriangulationUtil_tDA62C695D2152FE6541ADA0857DFB2258E0DA846), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2109[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2110[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D)+ sizeof (RuntimeObject), sizeof(FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2111[3] = 
{
	FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D::get_offset_of__0_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D::get_offset_of__1_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D::get_offset_of__2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (U3CEnumerateU3Ed__10_t76D57E352E6F38113430F0395804829B504236AA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2112[6] = 
{
	U3CEnumerateU3Ed__10_t76D57E352E6F38113430F0395804829B504236AA::get_offset_of_U3CU3E1__state_0(),
	U3CEnumerateU3Ed__10_t76D57E352E6F38113430F0395804829B504236AA::get_offset_of_U3CU3E2__current_1(),
	U3CEnumerateU3Ed__10_t76D57E352E6F38113430F0395804829B504236AA::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CEnumerateU3Ed__10_t76D57E352E6F38113430F0395804829B504236AA::get_offset_of_U3CU3E4__this_3(),
	U3CEnumerateU3Ed__10_t76D57E352E6F38113430F0395804829B504236AA::get_offset_of_U3CU3E3__U3CU3E4__this_4(),
	U3CEnumerateU3Ed__10_t76D57E352E6F38113430F0395804829B504236AA::get_offset_of_U3CiU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (MathUtil_t489F8CC3DE74CF32CBFE2DEAD4FB8C1401C3A9B3), -1, sizeof(MathUtil_t489F8CC3DE74CF32CBFE2DEAD4FB8C1401C3A9B3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2113[1] = 
{
	MathUtil_t489F8CC3DE74CF32CBFE2DEAD4FB8C1401C3A9B3_StaticFields::get_offset_of_EPSILON_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2114[2] = 
{
	Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D::get_offset_of_mX_0(),
	Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D::get_offset_of_mY_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (Point2DEnumerator_tA5536230BB56A3AE41E00C65BBCF4811679A9C15), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2115[2] = 
{
	Point2DEnumerator_tA5536230BB56A3AE41E00C65BBCF4811679A9C15::get_offset_of_mPoints_0(),
	Point2DEnumerator_tA5536230BB56A3AE41E00C65BBCF4811679A9C15::get_offset_of_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0), -1, sizeof(Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2116[7] = 
{
	Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0_StaticFields::get_offset_of_kMaxPolygonVertices_0(),
	Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0_StaticFields::get_offset_of_kLinearSlop_1(),
	Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0_StaticFields::get_offset_of_kAngularSlop_2(),
	Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0::get_offset_of_mPoints_3(),
	Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0::get_offset_of_mBoundingBox_4(),
	Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0::get_offset_of_mWindingOrder_5(),
	Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0::get_offset_of_mEpsilon_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (WindingOrderType_tF976AF91C062E008942E81B1895115A047A1CB0A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2117[5] = 
{
	WindingOrderType_tF976AF91C062E008942E81B1895115A047A1CB0A::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (Rect2D_t337FF4CBB07E16CB6E029205BF01F1984DDE5353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2118[4] = 
{
	Rect2D_t337FF4CBB07E16CB6E029205BF01F1984DDE5353::get_offset_of_mMinX_0(),
	Rect2D_t337FF4CBB07E16CB6E029205BF01F1984DDE5353::get_offset_of_mMaxX_1(),
	Rect2D_t337FF4CBB07E16CB6E029205BF01F1984DDE5353::get_offset_of_mMinY_2(),
	Rect2D_t337FF4CBB07E16CB6E029205BF01F1984DDE5353::get_offset_of_mMaxY_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (U3CModuleU3E_t59A29D11DE086BC643857AFBADB05C659B348470), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (MonoTODOAttribute_t2D21BFE6EE9459D34164629D1D797DBB606A57A0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08), -1, sizeof(XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2121[17] = 
{
	XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08_StaticFields::get_offset_of_emptyAttributeArray_2(),
	XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08::get_offset_of_reader_3(),
	XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08::get_offset_of_options_4(),
	XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08::get_offset_of_v_5(),
	XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08::get_offset_of_getter_6(),
	XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08::get_offset_of_xsinfo_7(),
	XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08::get_offset_of_readerLineInfo_8(),
	XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08::get_offset_of_nsResolver_9(),
	XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08::get_offset_of_defaultAttributes_10(),
	XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08::get_offset_of_currentDefaultAttribute_11(),
	XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08::get_offset_of_defaultAttributesCache_12(),
	XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08::get_offset_of_defaultAttributeConsumed_13(),
	XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08::get_offset_of_currentAttrType_14(),
	XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08::get_offset_of_validationDone_15(),
	XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08::get_offset_of_element_16(),
	XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_17(),
	XmlSchemaValidatingReader_t3C8E4816133B5AD717E222C6FE5CD6D2F2929B08_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_tD158F70A2443132E0678A7CA420F56DB9CC0DF75), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2122[2] = 
{
	U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_tD158F70A2443132E0678A7CA420F56DB9CC0DF75::get_offset_of_settings_0(),
	U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_tD158F70A2443132E0678A7CA420F56DB9CC0DF75::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2123[3] = 
{
	XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8::get_offset_of_selectorPaths_0(),
	XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8::get_offset_of_fields_1(),
	XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8::get_offset_of_cachedFields_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (XsdIdentityField_t47467B5E2857C75A53026E207DC4063868D49DC7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2124[2] = 
{
	XsdIdentityField_t47467B5E2857C75A53026E207DC4063868D49DC7::get_offset_of_fieldPaths_0(),
	XsdIdentityField_t47467B5E2857C75A53026E207DC4063868D49DC7::get_offset_of_index_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2125[2] = 
{
	XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE::get_offset_of_OrderedSteps_0(),
	XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE::get_offset_of_Descendants_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2126[6] = 
{
	XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D::get_offset_of_IsCurrent_0(),
	XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D::get_offset_of_IsAttribute_1(),
	XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D::get_offset_of_IsAnyName_2(),
	XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D::get_offset_of_NsName_3(),
	XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D::get_offset_of_Name_4(),
	XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D::get_offset_of_Namespace_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2127[13] = 
{
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_entry_0(),
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_field_1(),
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_FieldFound_2(),
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_FieldLineNumber_3(),
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_FieldLinePosition_4(),
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_FieldHasLineInfo_5(),
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_FieldType_6(),
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_Identity_7(),
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_IsXsiNil_8(),
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_FieldFoundDepth_9(),
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_FieldFoundPath_10(),
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_Consuming_11(),
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_Consumed_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (XsdKeyEntryFieldCollection_t8536E011BAA035EF7757EE7BBE9843160ABBF4EC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2129[8] = 
{
	XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3::get_offset_of_StartDepth_0(),
	XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3::get_offset_of_SelectorLineNumber_1(),
	XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3::get_offset_of_SelectorLinePosition_2(),
	XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3::get_offset_of_SelectorHasLineInfo_3(),
	XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3::get_offset_of_KeyFields_4(),
	XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3::get_offset_of_KeyRefFound_5(),
	XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3::get_offset_of_OwnerSequence_6(),
	XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3::get_offset_of_keyFound_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (XsdKeyEntryCollection_tF37F133E46ECFF1F08CBB56894E1B18CB931B265), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2131[9] = 
{
	XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6::get_offset_of_alwaysTrue_0(),
	XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6::get_offset_of_selector_1(),
	XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6::get_offset_of_source_2(),
	XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6::get_offset_of_qname_3(),
	XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6::get_offset_of_refKeyName_4(),
	XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6::get_offset_of_Entries_5(),
	XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6::get_offset_of_FinishedEntries_6(),
	XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6::get_offset_of_StartDepth_7(),
	XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6::get_offset_of_ReferencedKey_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68), -1, sizeof(XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2132[6] = 
{
	XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68::get_offset_of_table_0(),
	XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68::get_offset_of_processContents_1(),
	XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68::get_offset_of_CurrentElement_2(),
	XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68::get_offset_of_ContextStack_3(),
	XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68::get_offset_of_Context_4(),
	XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74), -1, sizeof(XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2133[3] = 
{
	XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74_StaticFields::get_offset_of_invalid_0(),
	XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74::get_offset_of_occured_1(),
	XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74::get_offset_of_manager_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (XsdElementValidationState_tE3F398F18D4968EA5F91FEE92C646B19389C1C13), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2134[1] = 
{
	XsdElementValidationState_tE3F398F18D4968EA5F91FEE92C646B19389C1C13::get_offset_of_element_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (XsdSequenceValidationState_tB6D68439C7FBDA9D3510C267EF0038D735D6658E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2135[4] = 
{
	XsdSequenceValidationState_tB6D68439C7FBDA9D3510C267EF0038D735D6658E::get_offset_of_seq_3(),
	XsdSequenceValidationState_tB6D68439C7FBDA9D3510C267EF0038D735D6658E::get_offset_of_current_4(),
	XsdSequenceValidationState_tB6D68439C7FBDA9D3510C267EF0038D735D6658E::get_offset_of_currentAutomata_5(),
	XsdSequenceValidationState_tB6D68439C7FBDA9D3510C267EF0038D735D6658E::get_offset_of_emptiable_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (XsdChoiceValidationState_t6A72354CC424E101B3DB7472BBD8ADA7F61ACFC2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2136[3] = 
{
	XsdChoiceValidationState_t6A72354CC424E101B3DB7472BBD8ADA7F61ACFC2::get_offset_of_choice_3(),
	XsdChoiceValidationState_t6A72354CC424E101B3DB7472BBD8ADA7F61ACFC2::get_offset_of_emptiable_4(),
	XsdChoiceValidationState_t6A72354CC424E101B3DB7472BBD8ADA7F61ACFC2::get_offset_of_emptiableComputed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (XsdAllValidationState_t4ED82B2BF3F8403103526301F3BB097F4F951758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2137[2] = 
{
	XsdAllValidationState_t4ED82B2BF3F8403103526301F3BB097F4F951758::get_offset_of_all_3(),
	XsdAllValidationState_t4ED82B2BF3F8403103526301F3BB097F4F951758::get_offset_of_consumed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (XsdAnyValidationState_t6ABF8A32C88C9171AA284E91FB0ADED6F91267A3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2138[1] = 
{
	XsdAnyValidationState_t6ABF8A32C88C9171AA284E91FB0ADED6F91267A3::get_offset_of_any_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (XsdAppendedValidationState_tF78BEDEB51F43A455D52D86B17F4E77A28D2BFCD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2139[2] = 
{
	XsdAppendedValidationState_tF78BEDEB51F43A455D52D86B17F4E77A28D2BFCD::get_offset_of_head_3(),
	XsdAppendedValidationState_tF78BEDEB51F43A455D52D86B17F4E77A28D2BFCD::get_offset_of_rest_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (XsdEmptyValidationState_t9F4AC1CC30BFB15E1B14F72386E85F2E78EFD045), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (XsdInvalidValidationState_t3532998F32C0C00F8E0F0C199D2308C867084FFF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3), -1, sizeof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2142[30] = 
{
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3_StaticFields::get_offset_of_emptyAttributeArray_2(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_reader_3(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_resolver_4(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_sourceReaderSchemaInfo_5(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_readerLineInfo_6(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_validationType_7(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_schemas_8(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_namespaces_9(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_validationStarted_10(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_checkIdentity_11(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_idManager_12(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_checkKeyConstraints_13(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_keyTables_14(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_currentKeyFieldConsumers_15(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_tmpKeyrefPool_16(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_elementQNameStack_17(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_state_18(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_skipValidationDepth_19(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_xsiNilDepth_20(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_storedCharacters_21(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_shouldValidateCharacters_22(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_defaultAttributes_23(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_currentDefaultAttribute_24(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_defaultAttributesCache_25(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_defaultAttributeConsumed_26(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_currentAttrType_27(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_ValidationEventHandler_28(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3_StaticFields::get_offset_of_U3CU3Ef__switchU24map3_29(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3_StaticFields::get_offset_of_U3CU3Ef__switchU24map4_30(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2143[3] = 
{
	XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD::get_offset_of_xsi_type_0(),
	XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD::get_offset_of_State_1(),
	XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD::get_offset_of_element_stack_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2144[3] = 
{
	XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E::get_offset_of_idList_0(),
	XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E::get_offset_of_missingIDReferences_1(),
	XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E::get_offset_of_thisElementId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7), -1, sizeof(XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2145[10] = 
{
	XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7::get_offset_of_xsobj_0(),
	XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7::get_offset_of_ResolvedProcessing_1(),
	XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7::get_offset_of_TargetNamespace_2(),
	XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7::get_offset_of_SkipCompile_3(),
	XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7::get_offset_of_HasValueAny_4(),
	XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7::get_offset_of_HasValueLocal_5(),
	XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7::get_offset_of_HasValueOther_6(),
	XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7::get_offset_of_HasValueTargetNamespace_7(),
	XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7::get_offset_of_ResolvedNamespaces_8(),
	XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (XmlFilterReader_t3FFE2DE5E14FCE552BB0914DFEB6E0A1467B22F0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2148[3] = 
{
	XmlFilterReader_t3FFE2DE5E14FCE552BB0914DFEB6E0A1467B22F0::get_offset_of_reader_2(),
	XmlFilterReader_t3FFE2DE5E14FCE552BB0914DFEB6E0A1467B22F0::get_offset_of_settings_3(),
	XmlFilterReader_t3FFE2DE5E14FCE552BB0914DFEB6E0A1467B22F0::get_offset_of_lineInfo_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (XsdWhitespaceFacet_tCADC752FD2332AC9C1304C86653374FF61C0B91A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2149[4] = 
{
	XsdWhitespaceFacet_tCADC752FD2332AC9C1304C86653374FF61C0B91A::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (XsdOrdering_t27E88D8D2082F10D686D9004720EBEA75EAA05E8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2150[5] = 
{
	XsdOrdering_t27E88D8D2082F10D686D9004720EBEA75EAA05E8::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0), -1, sizeof(XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2151[6] = 
{
	XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields::get_offset_of_instance_55(),
	XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields::get_offset_of_whitespaceArray_56(),
	XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields::get_offset_of_booleanAllowedFacets_57(),
	XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields::get_offset_of_decimalAllowedFacets_58(),
	XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields::get_offset_of_durationAllowedFacets_59(),
	XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields::get_offset_of_stringAllowedFacets_60(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (XdtAnyAtomicType_t922A677221DBA603ACF1F4D2024F9222102416D7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (XdtUntypedAtomic_t8E0FD4A48C677AA7C2D946690BE85E92CE81CDED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (XsdString_t604253BF4F01F3A649275313E7AED2D8BCB854AD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (XsdNormalizedString_tEAAA641AA303B7A5227D53147AACC07A82EE13C0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (XsdToken_t4A120CAA60CECDF6DCDC15717ABCA63187B0D60C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (XsdLanguage_t392B00FB85420B861413B7295BC68EA7C0982D85), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (XsdNMToken_tED82C02272B75D49D76B0FC44858D09D8F06AB91), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (XsdNMTokens_tA3A587C5F1FF49445F75C961B0280F58A57D1095), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (XsdNCName_t7C66E8DC7486C5D92AF7739B71EC88C1FD11E2F7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (XsdID_tFC394CE8EE4F16A40F0063659AB8C788C712E77B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (XsdIDRef_tC8B88E328BA8745548A89206641A6C113AE9F09B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (XsdIDRefs_t3598EB6F29A7BEC87962148242C3904C8FF5BB9F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (XsdEntity_t426A9BE787702C55FF7FE5FE0FCDF582D5E50371), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (XsdEntities_tE41F357D8F259BBCCB58DF1E99D66720E8F2CF1C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (XsdNotation_t619CA8B98E6B358DAEE751C97934638D691DB5A3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (XsdDecimal_t1CB6398C98F43AF142671E2E9BDAB05D58AEE9EE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (XsdInteger_tE61DFB650D9B7BD3F6CA2C7DD4331667015CA348), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (XsdLong_t6FCDD94BFBBEC0874CD9166C160CF7B60D36B5FC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (XsdInt_t6F6E692FF9941520F897A726ABDE415CCEFC984F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (XsdShort_tAF41351D06667202EC3FDBC76501A2D091518332), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (XsdByte_t048838D9FF5BFD18C9FC056F11C699E7E08FC351), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (XsdNonNegativeInteger_t0F4F93B0464B81E0BBF21CFC2F08343966734B30), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (XsdUnsignedLong_t7B886E29EBA85082057263BFE0B9F644FF4D59B9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (XsdUnsignedInt_t01075FD84457AE05E045BB23BD0E3A82AC30CE32), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (XsdUnsignedShort_tA832C283CB5937E31E78F3E4BBC85F90F92CD785), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (XsdUnsignedByte_tA702674830BFF7B5B4ADF325F30A781366A45256), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (XsdPositiveInteger_t8089829DDE14DBD3EEAD11DD4EBA8F4658FA04B2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (XsdNonPositiveInteger_t960302EB6DDE4ECE0961BC6BBE4FAFA3C3E5BE62), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (XsdNegativeInteger_t5BF8A2EE42840B88A3C798FC6704B5AA9EDA715B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (XsdFloat_t02419495B090187B99AC33E40CD6601F7608D9B8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (XsdDouble_tAF166F167258B898C01C97CE94720CA2EDB5E269), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F), -1, sizeof(XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2184[2] = 
{
	XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F_StaticFields::get_offset_of_ALPHABET_61(),
	XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F_StaticFields::get_offset_of_decodeTable_62(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (XsdHexBinary_t542DD47AB586F148DAE0698D4FC956D0412C4229), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (XsdQName_t6E0BD13D8BFE16A59C49FAA8D331CA5FF5066DA0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (XsdBoolean_tAA5FDC2ACA9F0A05A686F75BFE68CBB53A2F9959), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (XsdAnyURI_t049F3807506793286A90A0A279CADC8C39D4647A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (XmlSchemaUri_t93E9CFD7A5D337C08D82EE3A3D83BA447B760FE0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2189[1] = 
{
	XmlSchemaUri_t93E9CFD7A5D337C08D82EE3A3D83BA447B760FE0::get_offset_of_value_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (XsdDuration_tA3253842BF902691A000BBFD87382247985D5E78), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (XdtDayTimeDuration_t14B50E7DF624721215FA0E49D3672BAAFF59027F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (XdtYearMonthDuration_t940953B7258AE5DEC65A9F65D90EC70D9786EA68), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (XsdDateTime_tC31D6EF56B47537535EEC097FEBF63553C479E5D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (XsdDate_t5536E073BDA86CBF86DE829550F870C6D7F89964), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (XsdTime_tDB0973DB314925C984AC13CB795FD91589AB462C), -1, sizeof(XsdTime_tDB0973DB314925C984AC13CB795FD91589AB462C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2195[1] = 
{
	XsdTime_tDB0973DB314925C984AC13CB795FD91589AB462C_StaticFields::get_offset_of_timeFormats_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (XsdGYearMonth_t775E38939959BAF7304B4212BD8406456781C385), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (XsdGMonthDay_t25E4C24A7DF2B6F4528C0271813BF8AF052F5669), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (XsdGYear_t3A3FECDF3BF0A8EDC0776CA5D478A2394CC894F2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (XsdGMonth_t6E98934D7FAFFBCFA788D4068EB7DAE6706F4312), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
