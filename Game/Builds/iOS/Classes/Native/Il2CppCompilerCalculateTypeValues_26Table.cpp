﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// ClipperLib.Clipper
struct Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F;
// ClipperLib.PolyNode
struct PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB;
// ClipperLib.PolyTree
struct PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5;
// FadeEffect
struct FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6;
// Hero
struct Hero_t39258A796E56A51462429C62F9A05CB012E66B5D;
// ImportSettings
struct ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3;
// Matrix
struct Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78;
// Noise3D
struct Noise3D_t288ADB7A6ECA2CA06F240738A7A7FDF20E7D5B20;
// SimplySVG.CascadeContext
struct CascadeContext_tD6A26BF562AC92D19625EDC2D5D625387A04384C;
// SimplySVG.ClipPathElement
struct ClipPathElement_t33F2F0C15FFA86FCE2BB3857AE271A01D7CA561E;
// SimplySVG.ClipPathElement/RecurseSVGElements
struct RecurseSVGElements_t7025179A40653E0F0B00540527D136A2C1D5A1F3;
// SimplySVG.CollisionShapeData
struct CollisionShapeData_t09D2AF9F5D9E3B0C513049F97C117CF4EB9B2C6B;
// SimplySVG.GraphicalAttributes
struct GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D;
// SimplySVG.GraphicalElement/PolyTreeRecurse
struct PolyTreeRecurse_t1392592EADEA5FBAC613340FE5FDE2518273715A;
// SimplySVG.SVGDocument
struct SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7;
// SimplySVG.SVGElement
struct SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888;
// SimplySVG.TransformAttributes
struct TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3;
// StateMachine
struct StateMachine_tA4A39BBD7B24E4B1C7B690D3C6BE73E7023EFD01;
// System.Action
struct Action_tC9F24EBA5075EA0322C6E0DC6BE2C7BDF962FB21;
// System.AsyncCallback
struct AsyncCallback_t74ABD1277F711E7FBDCB00E169A63DEFD39E86A2;
// System.Char[]
struct CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744;
// System.Collections.Generic.Dictionary`2<System.String,SimplySVG.SVGElement>
struct Dictionary_2_t4B3761F17E62E59722A97B59E80883A20F1E6448;
// System.Collections.Generic.List`1<ClipperLib.IntPoint>
struct List_1_tD323E55C055A7CA7984664415A9E8332BB24408B;
// System.Collections.Generic.List`1<SimplySVG.CollisionShapeData/Polygon>
struct List_1_t62A212C062502261FA3CC4B1B311DCF5D334D32B;
// System.Collections.Generic.List`1<SimplySVG.GraphicalElement/ContourPath>
struct List_1_tB78032FDEAD332482A68D6E7AA788282C763C40F;
// System.Collections.Generic.List`1<SimplySVG.PathElement/PathComponent>
struct List_1_tE439CCB1205D0896BE69BB06B2ADDCB72C59163E;
// System.Collections.Generic.List`1<SimplySVG.PathElement/SubPath>
struct List_1_tB0ADE6520BA0060AD719C7FA0DBA8FF95A2559A5;
// System.Collections.Generic.List`1<SimplySVG.SVGElement>
struct List_1_tDACA481967F32AB5A83EA055EF5C33BB4E3148ED;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t4A961510635950236678F1B3B2436ECCAF28713A;
// System.Collections.Generic.List`1<UnityEngine.Collider2D>
struct List_1_tCD80C1FE60E02F5DF3796693CF5D4E659D11F0F8;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955;
// System.DelegateData
struct DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t3AD4B0F443BFD399C4AC2D6EE99FFE3BC0970017;
// System.IAsyncResult
struct IAsyncResult_tDA33C24465239FB383C4C2CDAAC43B9AD3CB7F05;
// System.Int32[]
struct Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Single[]
struct SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5;
// System.String
struct String_t;
// System.Void
struct Void_tDB81A15FA2AB53E2401A76B745D215397B29F783;
// TouchControlsManager
struct TouchControlsManager_t073F81430C963A33027AB789335B536141D833E5;
// Trigger
struct Trigger_tB7343D9B8651521A7BC56AFD7F1BD6B05F65B7F3;
// TriggerChecker
struct TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.CircleCollider2D
struct CircleCollider2D_t862AED067B612149EF365A560B26406F6088641F;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.Events.UnityAction`1<UnityEngine.Collider2D>
struct UnityAction_1_t3CDA73FA209A649D52861ABBAF9EC5B1847526C0;
// UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>
struct UnityAction_1_tC29A4BDE80E6622E5794F177E3DAA65BB4342B44;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.MeshFilter
struct MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.PolygonCollider2D
struct PolygonCollider2D_t360CC66FD7BAF5F4FADE54EB23E58E95D0B8CD81;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t5F37B944987342C401FA9A231A75AD2991A66165;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57;
// UnityEngine.RectOffset
struct RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Renderer[]
struct RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Texture3D
struct Texture3D_t041D3C554E80910E92D1EAAA85E0F70655FD66B4;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4;
// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D;
// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE;
// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F;
// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE;
// UnityEngine.WheelJoint2D
struct WheelJoint2D_tB59E7B24356F45BDE10574AAE8DBC9CBD773E6AE;




#ifndef U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#define U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef NOISE3D_T288ADB7A6ECA2CA06F240738A7A7FDF20E7D5B20_H
#define NOISE3D_T288ADB7A6ECA2CA06F240738A7A7FDF20E7D5B20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Noise3D
struct  Noise3D_t288ADB7A6ECA2CA06F240738A7A7FDF20E7D5B20  : public RuntimeObject
{
public:
	// UnityEngine.Texture3D Noise3D::m_3DTex
	Texture3D_t041D3C554E80910E92D1EAAA85E0F70655FD66B4 * ___m_3DTex_0;
	// System.Int32 Noise3D::m_Size
	int32_t ___m_Size_1;

public:
	inline static int32_t get_offset_of_m_3DTex_0() { return static_cast<int32_t>(offsetof(Noise3D_t288ADB7A6ECA2CA06F240738A7A7FDF20E7D5B20, ___m_3DTex_0)); }
	inline Texture3D_t041D3C554E80910E92D1EAAA85E0F70655FD66B4 * get_m_3DTex_0() const { return ___m_3DTex_0; }
	inline Texture3D_t041D3C554E80910E92D1EAAA85E0F70655FD66B4 ** get_address_of_m_3DTex_0() { return &___m_3DTex_0; }
	inline void set_m_3DTex_0(Texture3D_t041D3C554E80910E92D1EAAA85E0F70655FD66B4 * value)
	{
		___m_3DTex_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_3DTex_0), value);
	}

	inline static int32_t get_offset_of_m_Size_1() { return static_cast<int32_t>(offsetof(Noise3D_t288ADB7A6ECA2CA06F240738A7A7FDF20E7D5B20, ___m_Size_1)); }
	inline int32_t get_m_Size_1() const { return ___m_Size_1; }
	inline int32_t* get_address_of_m_Size_1() { return &___m_Size_1; }
	inline void set_m_Size_1(int32_t value)
	{
		___m_Size_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOISE3D_T288ADB7A6ECA2CA06F240738A7A7FDF20E7D5B20_H
#ifndef PERLIN_T9FD22982AE95029048B11554B511F2A308111C49_H
#define PERLIN_T9FD22982AE95029048B11554B511F2A308111C49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Perlin
struct  Perlin_t9FD22982AE95029048B11554B511F2A308111C49  : public RuntimeObject
{
public:

public:
};

struct Perlin_t9FD22982AE95029048B11554B511F2A308111C49_StaticFields
{
public:
	// System.Int32[] Perlin::perm
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___perm_0;

public:
	inline static int32_t get_offset_of_perm_0() { return static_cast<int32_t>(offsetof(Perlin_t9FD22982AE95029048B11554B511F2A308111C49_StaticFields, ___perm_0)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_perm_0() const { return ___perm_0; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_perm_0() { return &___perm_0; }
	inline void set_perm_0(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___perm_0 = value;
		Il2CppCodeGenWriteBarrier((&___perm_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERLIN_T9FD22982AE95029048B11554B511F2A308111C49_H
#ifndef CASCADECONTEXT_TD6A26BF562AC92D19625EDC2D5D625387A04384C_H
#define CASCADECONTEXT_TD6A26BF562AC92D19625EDC2D5D625387A04384C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.CascadeContext
struct  CascadeContext_tD6A26BF562AC92D19625EDC2D5D625387A04384C  : public RuntimeObject
{
public:
	// SimplySVG.GraphicalAttributes SimplySVG.CascadeContext::graphicalAttributes
	GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D * ___graphicalAttributes_0;
	// SimplySVG.TransformAttributes SimplySVG.CascadeContext::transformAttributes
	TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3 * ___transformAttributes_1;
	// ClipperLib.PolyTree SimplySVG.CascadeContext::clipStencil
	PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5 * ___clipStencil_2;

public:
	inline static int32_t get_offset_of_graphicalAttributes_0() { return static_cast<int32_t>(offsetof(CascadeContext_tD6A26BF562AC92D19625EDC2D5D625387A04384C, ___graphicalAttributes_0)); }
	inline GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D * get_graphicalAttributes_0() const { return ___graphicalAttributes_0; }
	inline GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D ** get_address_of_graphicalAttributes_0() { return &___graphicalAttributes_0; }
	inline void set_graphicalAttributes_0(GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D * value)
	{
		___graphicalAttributes_0 = value;
		Il2CppCodeGenWriteBarrier((&___graphicalAttributes_0), value);
	}

	inline static int32_t get_offset_of_transformAttributes_1() { return static_cast<int32_t>(offsetof(CascadeContext_tD6A26BF562AC92D19625EDC2D5D625387A04384C, ___transformAttributes_1)); }
	inline TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3 * get_transformAttributes_1() const { return ___transformAttributes_1; }
	inline TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3 ** get_address_of_transformAttributes_1() { return &___transformAttributes_1; }
	inline void set_transformAttributes_1(TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3 * value)
	{
		___transformAttributes_1 = value;
		Il2CppCodeGenWriteBarrier((&___transformAttributes_1), value);
	}

	inline static int32_t get_offset_of_clipStencil_2() { return static_cast<int32_t>(offsetof(CascadeContext_tD6A26BF562AC92D19625EDC2D5D625387A04384C, ___clipStencil_2)); }
	inline PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5 * get_clipStencil_2() const { return ___clipStencil_2; }
	inline PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5 ** get_address_of_clipStencil_2() { return &___clipStencil_2; }
	inline void set_clipStencil_2(PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5 * value)
	{
		___clipStencil_2 = value;
		Il2CppCodeGenWriteBarrier((&___clipStencil_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CASCADECONTEXT_TD6A26BF562AC92D19625EDC2D5D625387A04384C_H
#ifndef U3CGETSTENCILU3EC__ANONSTOREY0_TC0F2C4FF08AE04C08BA27CD042D0043EAFF2E13B_H
#define U3CGETSTENCILU3EC__ANONSTOREY0_TC0F2C4FF08AE04C08BA27CD042D0043EAFF2E13B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.ClipPathElement/<GetStencil>c__AnonStorey0
struct  U3CGetStencilU3Ec__AnonStorey0_tC0F2C4FF08AE04C08BA27CD042D0043EAFF2E13B  : public RuntimeObject
{
public:
	// ClipperLib.Clipper SimplySVG.ClipPathElement/<GetStencil>c__AnonStorey0::stencilClipper
	Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F * ___stencilClipper_0;
	// SimplySVG.ClipPathElement/RecurseSVGElements SimplySVG.ClipPathElement/<GetStencil>c__AnonStorey0::recurse
	RecurseSVGElements_t7025179A40653E0F0B00540527D136A2C1D5A1F3 * ___recurse_1;
	// SimplySVG.ClipPathElement SimplySVG.ClipPathElement/<GetStencil>c__AnonStorey0::$this
	ClipPathElement_t33F2F0C15FFA86FCE2BB3857AE271A01D7CA561E * ___U24this_2;

public:
	inline static int32_t get_offset_of_stencilClipper_0() { return static_cast<int32_t>(offsetof(U3CGetStencilU3Ec__AnonStorey0_tC0F2C4FF08AE04C08BA27CD042D0043EAFF2E13B, ___stencilClipper_0)); }
	inline Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F * get_stencilClipper_0() const { return ___stencilClipper_0; }
	inline Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F ** get_address_of_stencilClipper_0() { return &___stencilClipper_0; }
	inline void set_stencilClipper_0(Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F * value)
	{
		___stencilClipper_0 = value;
		Il2CppCodeGenWriteBarrier((&___stencilClipper_0), value);
	}

	inline static int32_t get_offset_of_recurse_1() { return static_cast<int32_t>(offsetof(U3CGetStencilU3Ec__AnonStorey0_tC0F2C4FF08AE04C08BA27CD042D0043EAFF2E13B, ___recurse_1)); }
	inline RecurseSVGElements_t7025179A40653E0F0B00540527D136A2C1D5A1F3 * get_recurse_1() const { return ___recurse_1; }
	inline RecurseSVGElements_t7025179A40653E0F0B00540527D136A2C1D5A1F3 ** get_address_of_recurse_1() { return &___recurse_1; }
	inline void set_recurse_1(RecurseSVGElements_t7025179A40653E0F0B00540527D136A2C1D5A1F3 * value)
	{
		___recurse_1 = value;
		Il2CppCodeGenWriteBarrier((&___recurse_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CGetStencilU3Ec__AnonStorey0_tC0F2C4FF08AE04C08BA27CD042D0043EAFF2E13B, ___U24this_2)); }
	inline ClipPathElement_t33F2F0C15FFA86FCE2BB3857AE271A01D7CA561E * get_U24this_2() const { return ___U24this_2; }
	inline ClipPathElement_t33F2F0C15FFA86FCE2BB3857AE271A01D7CA561E ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ClipPathElement_t33F2F0C15FFA86FCE2BB3857AE271A01D7CA561E * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETSTENCILU3EC__ANONSTOREY0_TC0F2C4FF08AE04C08BA27CD042D0043EAFF2E13B_H
#ifndef POLYGON_T12BBDCB595FE6F844811B0E4FE7399BDDF4F77FC_H
#define POLYGON_T12BBDCB595FE6F844811B0E4FE7399BDDF4F77FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.CollisionShapeData/Polygon
struct  Polygon_t12BBDCB595FE6F844811B0E4FE7399BDDF4F77FC  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector2> SimplySVG.CollisionShapeData/Polygon::points
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___points_0;

public:
	inline static int32_t get_offset_of_points_0() { return static_cast<int32_t>(offsetof(Polygon_t12BBDCB595FE6F844811B0E4FE7399BDDF4F77FC, ___points_0)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_points_0() const { return ___points_0; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_points_0() { return &___points_0; }
	inline void set_points_0(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___points_0 = value;
		Il2CppCodeGenWriteBarrier((&___points_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGON_T12BBDCB595FE6F844811B0E4FE7399BDDF4F77FC_H
#ifndef CONVEXHULLUTILITY_T9E7A2B2B8A2FE0D3AD40BD279D90816210158D85_H
#define CONVEXHULLUTILITY_T9E7A2B2B8A2FE0D3AD40BD279D90816210158D85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.ConvexHullUtility
struct  ConvexHullUtility_t9E7A2B2B8A2FE0D3AD40BD279D90816210158D85  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVEXHULLUTILITY_T9E7A2B2B8A2FE0D3AD40BD279D90816210158D85_H
#ifndef U3CMAKEELLIPSOIDCONTOURPOINTSU3EC__ANONSTOREY0_T17222FE283B193808F145DC9CFA7BADA0346B784_H
#define U3CMAKEELLIPSOIDCONTOURPOINTSU3EC__ANONSTOREY0_T17222FE283B193808F145DC9CFA7BADA0346B784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.EllipseElement/<MakeEllipsoidContourPoints>c__AnonStorey0
struct  U3CMakeEllipsoidContourPointsU3Ec__AnonStorey0_t17222FE283B193808F145DC9CFA7BADA0346B784  : public RuntimeObject
{
public:
	// System.Single SimplySVG.EllipseElement/<MakeEllipsoidContourPoints>c__AnonStorey0::angle
	float ___angle_0;
	// System.Single SimplySVG.EllipseElement/<MakeEllipsoidContourPoints>c__AnonStorey0::rx
	float ___rx_1;
	// System.Single SimplySVG.EllipseElement/<MakeEllipsoidContourPoints>c__AnonStorey0::cx
	float ___cx_2;
	// System.Single SimplySVG.EllipseElement/<MakeEllipsoidContourPoints>c__AnonStorey0::ry
	float ___ry_3;
	// System.Single SimplySVG.EllipseElement/<MakeEllipsoidContourPoints>c__AnonStorey0::cy
	float ___cy_4;

public:
	inline static int32_t get_offset_of_angle_0() { return static_cast<int32_t>(offsetof(U3CMakeEllipsoidContourPointsU3Ec__AnonStorey0_t17222FE283B193808F145DC9CFA7BADA0346B784, ___angle_0)); }
	inline float get_angle_0() const { return ___angle_0; }
	inline float* get_address_of_angle_0() { return &___angle_0; }
	inline void set_angle_0(float value)
	{
		___angle_0 = value;
	}

	inline static int32_t get_offset_of_rx_1() { return static_cast<int32_t>(offsetof(U3CMakeEllipsoidContourPointsU3Ec__AnonStorey0_t17222FE283B193808F145DC9CFA7BADA0346B784, ___rx_1)); }
	inline float get_rx_1() const { return ___rx_1; }
	inline float* get_address_of_rx_1() { return &___rx_1; }
	inline void set_rx_1(float value)
	{
		___rx_1 = value;
	}

	inline static int32_t get_offset_of_cx_2() { return static_cast<int32_t>(offsetof(U3CMakeEllipsoidContourPointsU3Ec__AnonStorey0_t17222FE283B193808F145DC9CFA7BADA0346B784, ___cx_2)); }
	inline float get_cx_2() const { return ___cx_2; }
	inline float* get_address_of_cx_2() { return &___cx_2; }
	inline void set_cx_2(float value)
	{
		___cx_2 = value;
	}

	inline static int32_t get_offset_of_ry_3() { return static_cast<int32_t>(offsetof(U3CMakeEllipsoidContourPointsU3Ec__AnonStorey0_t17222FE283B193808F145DC9CFA7BADA0346B784, ___ry_3)); }
	inline float get_ry_3() const { return ___ry_3; }
	inline float* get_address_of_ry_3() { return &___ry_3; }
	inline void set_ry_3(float value)
	{
		___ry_3 = value;
	}

	inline static int32_t get_offset_of_cy_4() { return static_cast<int32_t>(offsetof(U3CMakeEllipsoidContourPointsU3Ec__AnonStorey0_t17222FE283B193808F145DC9CFA7BADA0346B784, ___cy_4)); }
	inline float get_cy_4() const { return ___cy_4; }
	inline float* get_address_of_cy_4() { return &___cy_4; }
	inline void set_cy_4(float value)
	{
		___cy_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMAKEELLIPSOIDCONTOURPOINTSU3EC__ANONSTOREY0_T17222FE283B193808F145DC9CFA7BADA0346B784_H
#ifndef U3CAPPLYTRANSFORMATIONU3EC__ANONSTOREY0_T7D29C559666E3163C110AB9F4F5BCF4C8E66E9C3_H
#define U3CAPPLYTRANSFORMATIONU3EC__ANONSTOREY0_T7D29C559666E3163C110AB9F4F5BCF4C8E66E9C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.GraphicalElement/<ApplyTransformation>c__AnonStorey0
struct  U3CApplyTransformationU3Ec__AnonStorey0_t7D29C559666E3163C110AB9F4F5BCF4C8E66E9C3  : public RuntimeObject
{
public:
	// Matrix SimplySVG.GraphicalElement/<ApplyTransformation>c__AnonStorey0::transformation
	Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78 * ___transformation_0;
	// SimplySVG.GraphicalElement/PolyTreeRecurse SimplySVG.GraphicalElement/<ApplyTransformation>c__AnonStorey0::recurse
	PolyTreeRecurse_t1392592EADEA5FBAC613340FE5FDE2518273715A * ___recurse_1;

public:
	inline static int32_t get_offset_of_transformation_0() { return static_cast<int32_t>(offsetof(U3CApplyTransformationU3Ec__AnonStorey0_t7D29C559666E3163C110AB9F4F5BCF4C8E66E9C3, ___transformation_0)); }
	inline Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78 * get_transformation_0() const { return ___transformation_0; }
	inline Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78 ** get_address_of_transformation_0() { return &___transformation_0; }
	inline void set_transformation_0(Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78 * value)
	{
		___transformation_0 = value;
		Il2CppCodeGenWriteBarrier((&___transformation_0), value);
	}

	inline static int32_t get_offset_of_recurse_1() { return static_cast<int32_t>(offsetof(U3CApplyTransformationU3Ec__AnonStorey0_t7D29C559666E3163C110AB9F4F5BCF4C8E66E9C3, ___recurse_1)); }
	inline PolyTreeRecurse_t1392592EADEA5FBAC613340FE5FDE2518273715A * get_recurse_1() const { return ___recurse_1; }
	inline PolyTreeRecurse_t1392592EADEA5FBAC613340FE5FDE2518273715A ** get_address_of_recurse_1() { return &___recurse_1; }
	inline void set_recurse_1(PolyTreeRecurse_t1392592EADEA5FBAC613340FE5FDE2518273715A * value)
	{
		___recurse_1 = value;
		Il2CppCodeGenWriteBarrier((&___recurse_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAPPLYTRANSFORMATIONU3EC__ANONSTOREY0_T7D29C559666E3163C110AB9F4F5BCF4C8E66E9C3_H
#ifndef CONTOURPATH_T9DAB1CD8D360AE05DEB804F7BB49378FC5449BA6_H
#define CONTOURPATH_T9DAB1CD8D360AE05DEB804F7BB49378FC5449BA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.GraphicalElement/ContourPath
struct  ContourPath_t9DAB1CD8D360AE05DEB804F7BB49378FC5449BA6  : public RuntimeObject
{
public:
	// System.Boolean SimplySVG.GraphicalElement/ContourPath::closed
	bool ___closed_0;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> SimplySVG.GraphicalElement/ContourPath::path
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___path_1;
	// System.Collections.Generic.List`1<ClipperLib.IntPoint> SimplySVG.GraphicalElement/ContourPath::clipperPath
	List_1_tD323E55C055A7CA7984664415A9E8332BB24408B * ___clipperPath_2;

public:
	inline static int32_t get_offset_of_closed_0() { return static_cast<int32_t>(offsetof(ContourPath_t9DAB1CD8D360AE05DEB804F7BB49378FC5449BA6, ___closed_0)); }
	inline bool get_closed_0() const { return ___closed_0; }
	inline bool* get_address_of_closed_0() { return &___closed_0; }
	inline void set_closed_0(bool value)
	{
		___closed_0 = value;
	}

	inline static int32_t get_offset_of_path_1() { return static_cast<int32_t>(offsetof(ContourPath_t9DAB1CD8D360AE05DEB804F7BB49378FC5449BA6, ___path_1)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_path_1() const { return ___path_1; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_path_1() { return &___path_1; }
	inline void set_path_1(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___path_1 = value;
		Il2CppCodeGenWriteBarrier((&___path_1), value);
	}

	inline static int32_t get_offset_of_clipperPath_2() { return static_cast<int32_t>(offsetof(ContourPath_t9DAB1CD8D360AE05DEB804F7BB49378FC5449BA6, ___clipperPath_2)); }
	inline List_1_tD323E55C055A7CA7984664415A9E8332BB24408B * get_clipperPath_2() const { return ___clipperPath_2; }
	inline List_1_tD323E55C055A7CA7984664415A9E8332BB24408B ** get_address_of_clipperPath_2() { return &___clipperPath_2; }
	inline void set_clipperPath_2(List_1_tD323E55C055A7CA7984664415A9E8332BB24408B * value)
	{
		___clipperPath_2 = value;
		Il2CppCodeGenWriteBarrier((&___clipperPath_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTOURPATH_T9DAB1CD8D360AE05DEB804F7BB49378FC5449BA6_H
#ifndef U3CBUILDSHAPEU3EC__ANONSTOREY0_TC188B4701BAECA5BEAADBC952D3F269E39C2CB30_H
#define U3CBUILDSHAPEU3EC__ANONSTOREY0_TC188B4701BAECA5BEAADBC952D3F269E39C2CB30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.PathElement/<BuildShape>c__AnonStorey0
struct  U3CBuildShapeU3Ec__AnonStorey0_tC188B4701BAECA5BEAADBC952D3F269E39C2CB30  : public RuntimeObject
{
public:
	// System.Single SimplySVG.PathElement/<BuildShape>c__AnonStorey0::theta1
	float ___theta1_0;
	// System.Single SimplySVG.PathElement/<BuildShape>c__AnonStorey0::dtheta
	float ___dtheta_1;
	// System.Single SimplySVG.PathElement/<BuildShape>c__AnonStorey0::rho
	float ___rho_2;
	// System.Single SimplySVG.PathElement/<BuildShape>c__AnonStorey0::rx
	float ___rx_3;
	// System.Single SimplySVG.PathElement/<BuildShape>c__AnonStorey0::ry
	float ___ry_4;
	// System.Single SimplySVG.PathElement/<BuildShape>c__AnonStorey0::cx
	float ___cx_5;
	// System.Single SimplySVG.PathElement/<BuildShape>c__AnonStorey0::cy
	float ___cy_6;

public:
	inline static int32_t get_offset_of_theta1_0() { return static_cast<int32_t>(offsetof(U3CBuildShapeU3Ec__AnonStorey0_tC188B4701BAECA5BEAADBC952D3F269E39C2CB30, ___theta1_0)); }
	inline float get_theta1_0() const { return ___theta1_0; }
	inline float* get_address_of_theta1_0() { return &___theta1_0; }
	inline void set_theta1_0(float value)
	{
		___theta1_0 = value;
	}

	inline static int32_t get_offset_of_dtheta_1() { return static_cast<int32_t>(offsetof(U3CBuildShapeU3Ec__AnonStorey0_tC188B4701BAECA5BEAADBC952D3F269E39C2CB30, ___dtheta_1)); }
	inline float get_dtheta_1() const { return ___dtheta_1; }
	inline float* get_address_of_dtheta_1() { return &___dtheta_1; }
	inline void set_dtheta_1(float value)
	{
		___dtheta_1 = value;
	}

	inline static int32_t get_offset_of_rho_2() { return static_cast<int32_t>(offsetof(U3CBuildShapeU3Ec__AnonStorey0_tC188B4701BAECA5BEAADBC952D3F269E39C2CB30, ___rho_2)); }
	inline float get_rho_2() const { return ___rho_2; }
	inline float* get_address_of_rho_2() { return &___rho_2; }
	inline void set_rho_2(float value)
	{
		___rho_2 = value;
	}

	inline static int32_t get_offset_of_rx_3() { return static_cast<int32_t>(offsetof(U3CBuildShapeU3Ec__AnonStorey0_tC188B4701BAECA5BEAADBC952D3F269E39C2CB30, ___rx_3)); }
	inline float get_rx_3() const { return ___rx_3; }
	inline float* get_address_of_rx_3() { return &___rx_3; }
	inline void set_rx_3(float value)
	{
		___rx_3 = value;
	}

	inline static int32_t get_offset_of_ry_4() { return static_cast<int32_t>(offsetof(U3CBuildShapeU3Ec__AnonStorey0_tC188B4701BAECA5BEAADBC952D3F269E39C2CB30, ___ry_4)); }
	inline float get_ry_4() const { return ___ry_4; }
	inline float* get_address_of_ry_4() { return &___ry_4; }
	inline void set_ry_4(float value)
	{
		___ry_4 = value;
	}

	inline static int32_t get_offset_of_cx_5() { return static_cast<int32_t>(offsetof(U3CBuildShapeU3Ec__AnonStorey0_tC188B4701BAECA5BEAADBC952D3F269E39C2CB30, ___cx_5)); }
	inline float get_cx_5() const { return ___cx_5; }
	inline float* get_address_of_cx_5() { return &___cx_5; }
	inline void set_cx_5(float value)
	{
		___cx_5 = value;
	}

	inline static int32_t get_offset_of_cy_6() { return static_cast<int32_t>(offsetof(U3CBuildShapeU3Ec__AnonStorey0_tC188B4701BAECA5BEAADBC952D3F269E39C2CB30, ___cy_6)); }
	inline float get_cy_6() const { return ___cy_6; }
	inline float* get_address_of_cy_6() { return &___cy_6; }
	inline void set_cy_6(float value)
	{
		___cy_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBUILDSHAPEU3EC__ANONSTOREY0_TC188B4701BAECA5BEAADBC952D3F269E39C2CB30_H
#ifndef SUBPATH_T96958462F98C903C3B5FE837612402A70921E627_H
#define SUBPATH_T96958462F98C903C3B5FE837612402A70921E627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.PathElement/SubPath
struct  SubPath_t96958462F98C903C3B5FE837612402A70921E627  : public RuntimeObject
{
public:
	// System.Boolean SimplySVG.PathElement/SubPath::closed
	bool ___closed_0;
	// System.Collections.Generic.List`1<SimplySVG.PathElement/PathComponent> SimplySVG.PathElement/SubPath::path
	List_1_tE439CCB1205D0896BE69BB06B2ADDCB72C59163E * ___path_1;

public:
	inline static int32_t get_offset_of_closed_0() { return static_cast<int32_t>(offsetof(SubPath_t96958462F98C903C3B5FE837612402A70921E627, ___closed_0)); }
	inline bool get_closed_0() const { return ___closed_0; }
	inline bool* get_address_of_closed_0() { return &___closed_0; }
	inline void set_closed_0(bool value)
	{
		___closed_0 = value;
	}

	inline static int32_t get_offset_of_path_1() { return static_cast<int32_t>(offsetof(SubPath_t96958462F98C903C3B5FE837612402A70921E627, ___path_1)); }
	inline List_1_tE439CCB1205D0896BE69BB06B2ADDCB72C59163E * get_path_1() const { return ___path_1; }
	inline List_1_tE439CCB1205D0896BE69BB06B2ADDCB72C59163E ** get_address_of_path_1() { return &___path_1; }
	inline void set_path_1(List_1_tE439CCB1205D0896BE69BB06B2ADDCB72C59163E * value)
	{
		___path_1 = value;
		Il2CppCodeGenWriteBarrier((&___path_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBPATH_T96958462F98C903C3B5FE837612402A70921E627_H
#ifndef SVGDOCUMENT_TC755869E81E64D4CBB5A30C21524F4176A3850C7_H
#define SVGDOCUMENT_TC755869E81E64D4CBB5A30C21524F4176A3850C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.SVGDocument
struct  SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7  : public RuntimeObject
{
public:
	// SimplySVG.SVGElement SimplySVG.SVGDocument::rootElement
	SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888 * ___rootElement_0;
	// ImportSettings SimplySVG.SVGDocument::importSettings
	ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3 * ___importSettings_1;
	// System.Collections.Generic.Dictionary`2<System.String,SimplySVG.SVGElement> SimplySVG.SVGDocument::elementsById
	Dictionary_2_t4B3761F17E62E59722A97B59E80883A20F1E6448 * ___elementsById_2;

public:
	inline static int32_t get_offset_of_rootElement_0() { return static_cast<int32_t>(offsetof(SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7, ___rootElement_0)); }
	inline SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888 * get_rootElement_0() const { return ___rootElement_0; }
	inline SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888 ** get_address_of_rootElement_0() { return &___rootElement_0; }
	inline void set_rootElement_0(SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888 * value)
	{
		___rootElement_0 = value;
		Il2CppCodeGenWriteBarrier((&___rootElement_0), value);
	}

	inline static int32_t get_offset_of_importSettings_1() { return static_cast<int32_t>(offsetof(SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7, ___importSettings_1)); }
	inline ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3 * get_importSettings_1() const { return ___importSettings_1; }
	inline ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3 ** get_address_of_importSettings_1() { return &___importSettings_1; }
	inline void set_importSettings_1(ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3 * value)
	{
		___importSettings_1 = value;
		Il2CppCodeGenWriteBarrier((&___importSettings_1), value);
	}

	inline static int32_t get_offset_of_elementsById_2() { return static_cast<int32_t>(offsetof(SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7, ___elementsById_2)); }
	inline Dictionary_2_t4B3761F17E62E59722A97B59E80883A20F1E6448 * get_elementsById_2() const { return ___elementsById_2; }
	inline Dictionary_2_t4B3761F17E62E59722A97B59E80883A20F1E6448 ** get_address_of_elementsById_2() { return &___elementsById_2; }
	inline void set_elementsById_2(Dictionary_2_t4B3761F17E62E59722A97B59E80883A20F1E6448 * value)
	{
		___elementsById_2 = value;
		Il2CppCodeGenWriteBarrier((&___elementsById_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVGDOCUMENT_TC755869E81E64D4CBB5A30C21524F4176A3850C7_H
#ifndef SVGELEMENT_T83C50C9210FA55853195E3B39B9E5AC78E9A9888_H
#define SVGELEMENT_T83C50C9210FA55853195E3B39B9E5AC78E9A9888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.SVGElement
struct  SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888  : public RuntimeObject
{
public:
	// System.String SimplySVG.SVGElement::id
	String_t* ___id_0;
	// SimplySVG.SVGDocument SimplySVG.SVGElement::ownerDocument
	SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7 * ___ownerDocument_1;
	// SimplySVG.SVGElement SimplySVG.SVGElement::parent
	SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888 * ___parent_2;
	// System.Collections.Generic.List`1<SimplySVG.SVGElement> SimplySVG.SVGElement::children
	List_1_tDACA481967F32AB5A83EA055EF5C33BB4E3148ED * ___children_3;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_ownerDocument_1() { return static_cast<int32_t>(offsetof(SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888, ___ownerDocument_1)); }
	inline SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7 * get_ownerDocument_1() const { return ___ownerDocument_1; }
	inline SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7 ** get_address_of_ownerDocument_1() { return &___ownerDocument_1; }
	inline void set_ownerDocument_1(SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7 * value)
	{
		___ownerDocument_1 = value;
		Il2CppCodeGenWriteBarrier((&___ownerDocument_1), value);
	}

	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888, ___parent_2)); }
	inline SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888 * get_parent_2() const { return ___parent_2; }
	inline SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((&___parent_2), value);
	}

	inline static int32_t get_offset_of_children_3() { return static_cast<int32_t>(offsetof(SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888, ___children_3)); }
	inline List_1_tDACA481967F32AB5A83EA055EF5C33BB4E3148ED * get_children_3() const { return ___children_3; }
	inline List_1_tDACA481967F32AB5A83EA055EF5C33BB4E3148ED ** get_address_of_children_3() { return &___children_3; }
	inline void set_children_3(List_1_tDACA481967F32AB5A83EA055EF5C33BB4E3148ED * value)
	{
		___children_3 = value;
		Il2CppCodeGenWriteBarrier((&___children_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVGELEMENT_T83C50C9210FA55853195E3B39B9E5AC78E9A9888_H
#ifndef VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#define VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_com
{
};
#endif // VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifndef U3CDELAYU3EC__ITERATOR0_T6351117B347A09EF481280CB9742B31C2EAA2828_H
#define U3CDELAYU3EC__ITERATOR0_T6351117B347A09EF481280CB9742B31C2EAA2828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeUtils/<Delay>c__Iterator0
struct  U3CDelayU3Ec__Iterator0_t6351117B347A09EF481280CB9742B31C2EAA2828  : public RuntimeObject
{
public:
	// System.Single TimeUtils/<Delay>c__Iterator0::time
	float ___time_0;
	// System.Action TimeUtils/<Delay>c__Iterator0::func
	Action_tC9F24EBA5075EA0322C6E0DC6BE2C7BDF962FB21 * ___func_1;
	// System.Object TimeUtils/<Delay>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TimeUtils/<Delay>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TimeUtils/<Delay>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(U3CDelayU3Ec__Iterator0_t6351117B347A09EF481280CB9742B31C2EAA2828, ___time_0)); }
	inline float get_time_0() const { return ___time_0; }
	inline float* get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(float value)
	{
		___time_0 = value;
	}

	inline static int32_t get_offset_of_func_1() { return static_cast<int32_t>(offsetof(U3CDelayU3Ec__Iterator0_t6351117B347A09EF481280CB9742B31C2EAA2828, ___func_1)); }
	inline Action_tC9F24EBA5075EA0322C6E0DC6BE2C7BDF962FB21 * get_func_1() const { return ___func_1; }
	inline Action_tC9F24EBA5075EA0322C6E0DC6BE2C7BDF962FB21 ** get_address_of_func_1() { return &___func_1; }
	inline void set_func_1(Action_tC9F24EBA5075EA0322C6E0DC6BE2C7BDF962FB21 * value)
	{
		___func_1 = value;
		Il2CppCodeGenWriteBarrier((&___func_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDelayU3Ec__Iterator0_t6351117B347A09EF481280CB9742B31C2EAA2828, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDelayU3Ec__Iterator0_t6351117B347A09EF481280CB9742B31C2EAA2828, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDelayU3Ec__Iterator0_t6351117B347A09EF481280CB9742B31C2EAA2828, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYU3EC__ITERATOR0_T6351117B347A09EF481280CB9742B31C2EAA2828_H
#ifndef U3CDELAYFRAMESU3EC__ITERATOR1_T862B5A2F6BD951EFD1E7A38ADCFA8BF4934E0DE9_H
#define U3CDELAYFRAMESU3EC__ITERATOR1_T862B5A2F6BD951EFD1E7A38ADCFA8BF4934E0DE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeUtils/<DelayFrames>c__Iterator1
struct  U3CDelayFramesU3Ec__Iterator1_t862B5A2F6BD951EFD1E7A38ADCFA8BF4934E0DE9  : public RuntimeObject
{
public:
	// System.Int16 TimeUtils/<DelayFrames>c__Iterator1::frames
	int16_t ___frames_0;
	// System.Action TimeUtils/<DelayFrames>c__Iterator1::func
	Action_tC9F24EBA5075EA0322C6E0DC6BE2C7BDF962FB21 * ___func_1;
	// System.Object TimeUtils/<DelayFrames>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TimeUtils/<DelayFrames>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 TimeUtils/<DelayFrames>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_frames_0() { return static_cast<int32_t>(offsetof(U3CDelayFramesU3Ec__Iterator1_t862B5A2F6BD951EFD1E7A38ADCFA8BF4934E0DE9, ___frames_0)); }
	inline int16_t get_frames_0() const { return ___frames_0; }
	inline int16_t* get_address_of_frames_0() { return &___frames_0; }
	inline void set_frames_0(int16_t value)
	{
		___frames_0 = value;
	}

	inline static int32_t get_offset_of_func_1() { return static_cast<int32_t>(offsetof(U3CDelayFramesU3Ec__Iterator1_t862B5A2F6BD951EFD1E7A38ADCFA8BF4934E0DE9, ___func_1)); }
	inline Action_tC9F24EBA5075EA0322C6E0DC6BE2C7BDF962FB21 * get_func_1() const { return ___func_1; }
	inline Action_tC9F24EBA5075EA0322C6E0DC6BE2C7BDF962FB21 ** get_address_of_func_1() { return &___func_1; }
	inline void set_func_1(Action_tC9F24EBA5075EA0322C6E0DC6BE2C7BDF962FB21 * value)
	{
		___func_1 = value;
		Il2CppCodeGenWriteBarrier((&___func_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDelayFramesU3Ec__Iterator1_t862B5A2F6BD951EFD1E7A38ADCFA8BF4934E0DE9, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDelayFramesU3Ec__Iterator1_t862B5A2F6BD951EFD1E7A38ADCFA8BF4934E0DE9, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDelayFramesU3Ec__Iterator1_t862B5A2F6BD951EFD1E7A38ADCFA8BF4934E0DE9, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYFRAMESU3EC__ITERATOR1_T862B5A2F6BD951EFD1E7A38ADCFA8BF4934E0DE9_H
#ifndef BASEVERTEXEFFECT_T1EF95AB1FC33A027710E7DC86D19F700156C4F6A_H
#define BASEVERTEXEFFECT_T1EF95AB1FC33A027710E7DC86D19F700156C4F6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_t1EF95AB1FC33A027710E7DC86D19F700156C4F6A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVERTEXEFFECT_T1EF95AB1FC33A027710E7DC86D19F700156C4F6A_H
#ifndef REFLECTIONMETHODSCACHE_TBDADDC80D50C5F10BD00965217980B9A8D24BE8A_H
#define REFLECTIONMETHODSCACHE_TBDADDC80D50C5F10BD00965217980B9A8D24BE8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache
struct  ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A  : public RuntimeObject
{
public:
	// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback UnityEngine.UI.ReflectionMethodsCache::raycast3D
	Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F * ___raycast3D_0;
	// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback UnityEngine.UI.ReflectionMethodsCache::raycast3DAll
	RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE * ___raycast3DAll_1;
	// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback UnityEngine.UI.ReflectionMethodsCache::raycast2D
	Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE * ___raycast2D_2;
	// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAll
	GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 * ___getRayIntersectionAll_3;
	// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAllNonAlloc
	GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 * ___getRayIntersectionAllNonAlloc_4;
	// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRaycastNonAlloc
	GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D * ___getRaycastNonAlloc_5;

public:
	inline static int32_t get_offset_of_raycast3D_0() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___raycast3D_0)); }
	inline Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F * get_raycast3D_0() const { return ___raycast3D_0; }
	inline Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F ** get_address_of_raycast3D_0() { return &___raycast3D_0; }
	inline void set_raycast3D_0(Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F * value)
	{
		___raycast3D_0 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3D_0), value);
	}

	inline static int32_t get_offset_of_raycast3DAll_1() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___raycast3DAll_1)); }
	inline RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE * get_raycast3DAll_1() const { return ___raycast3DAll_1; }
	inline RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE ** get_address_of_raycast3DAll_1() { return &___raycast3DAll_1; }
	inline void set_raycast3DAll_1(RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE * value)
	{
		___raycast3DAll_1 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3DAll_1), value);
	}

	inline static int32_t get_offset_of_raycast2D_2() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___raycast2D_2)); }
	inline Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE * get_raycast2D_2() const { return ___raycast2D_2; }
	inline Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE ** get_address_of_raycast2D_2() { return &___raycast2D_2; }
	inline void set_raycast2D_2(Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE * value)
	{
		___raycast2D_2 = value;
		Il2CppCodeGenWriteBarrier((&___raycast2D_2), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAll_3() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___getRayIntersectionAll_3)); }
	inline GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 * get_getRayIntersectionAll_3() const { return ___getRayIntersectionAll_3; }
	inline GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 ** get_address_of_getRayIntersectionAll_3() { return &___getRayIntersectionAll_3; }
	inline void set_getRayIntersectionAll_3(GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 * value)
	{
		___getRayIntersectionAll_3 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAll_3), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAllNonAlloc_4() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___getRayIntersectionAllNonAlloc_4)); }
	inline GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 * get_getRayIntersectionAllNonAlloc_4() const { return ___getRayIntersectionAllNonAlloc_4; }
	inline GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 ** get_address_of_getRayIntersectionAllNonAlloc_4() { return &___getRayIntersectionAllNonAlloc_4; }
	inline void set_getRayIntersectionAllNonAlloc_4(GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 * value)
	{
		___getRayIntersectionAllNonAlloc_4 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAllNonAlloc_4), value);
	}

	inline static int32_t get_offset_of_getRaycastNonAlloc_5() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___getRaycastNonAlloc_5)); }
	inline GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D * get_getRaycastNonAlloc_5() const { return ___getRaycastNonAlloc_5; }
	inline GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D ** get_address_of_getRaycastNonAlloc_5() { return &___getRaycastNonAlloc_5; }
	inline void set_getRaycastNonAlloc_5(GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D * value)
	{
		___getRaycastNonAlloc_5 = value;
		Il2CppCodeGenWriteBarrier((&___getRaycastNonAlloc_5), value);
	}
};

struct ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields
{
public:
	// UnityEngine.UI.ReflectionMethodsCache UnityEngine.UI.ReflectionMethodsCache::s_ReflectionMethodsCache
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A * ___s_ReflectionMethodsCache_6;

public:
	inline static int32_t get_offset_of_s_ReflectionMethodsCache_6() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields, ___s_ReflectionMethodsCache_6)); }
	inline ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A * get_s_ReflectionMethodsCache_6() const { return ___s_ReflectionMethodsCache_6; }
	inline ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A ** get_address_of_s_ReflectionMethodsCache_6() { return &___s_ReflectionMethodsCache_6; }
	inline void set_s_ReflectionMethodsCache_6(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A * value)
	{
		___s_ReflectionMethodsCache_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_ReflectionMethodsCache_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONMETHODSCACHE_TBDADDC80D50C5F10BD00965217980B9A8D24BE8A_H
#ifndef U24ARRAYTYPEU3D12_T25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199_H
#define U24ARRAYTYPEU3D12_T25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199_H
#ifndef CLIPPATHELEMENT_T33F2F0C15FFA86FCE2BB3857AE271A01D7CA561E_H
#define CLIPPATHELEMENT_T33F2F0C15FFA86FCE2BB3857AE271A01D7CA561E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.ClipPathElement
struct  ClipPathElement_t33F2F0C15FFA86FCE2BB3857AE271A01D7CA561E  : public SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888
{
public:
	// SimplySVG.GraphicalAttributes SimplySVG.ClipPathElement::localGraphicalAttributes
	GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D * ___localGraphicalAttributes_4;
	// SimplySVG.TransformAttributes SimplySVG.ClipPathElement::localTransformAttributes
	TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3 * ___localTransformAttributes_5;
	// ClipperLib.PolyTree SimplySVG.ClipPathElement::stencil
	PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5 * ___stencil_6;

public:
	inline static int32_t get_offset_of_localGraphicalAttributes_4() { return static_cast<int32_t>(offsetof(ClipPathElement_t33F2F0C15FFA86FCE2BB3857AE271A01D7CA561E, ___localGraphicalAttributes_4)); }
	inline GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D * get_localGraphicalAttributes_4() const { return ___localGraphicalAttributes_4; }
	inline GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D ** get_address_of_localGraphicalAttributes_4() { return &___localGraphicalAttributes_4; }
	inline void set_localGraphicalAttributes_4(GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D * value)
	{
		___localGraphicalAttributes_4 = value;
		Il2CppCodeGenWriteBarrier((&___localGraphicalAttributes_4), value);
	}

	inline static int32_t get_offset_of_localTransformAttributes_5() { return static_cast<int32_t>(offsetof(ClipPathElement_t33F2F0C15FFA86FCE2BB3857AE271A01D7CA561E, ___localTransformAttributes_5)); }
	inline TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3 * get_localTransformAttributes_5() const { return ___localTransformAttributes_5; }
	inline TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3 ** get_address_of_localTransformAttributes_5() { return &___localTransformAttributes_5; }
	inline void set_localTransformAttributes_5(TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3 * value)
	{
		___localTransformAttributes_5 = value;
		Il2CppCodeGenWriteBarrier((&___localTransformAttributes_5), value);
	}

	inline static int32_t get_offset_of_stencil_6() { return static_cast<int32_t>(offsetof(ClipPathElement_t33F2F0C15FFA86FCE2BB3857AE271A01D7CA561E, ___stencil_6)); }
	inline PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5 * get_stencil_6() const { return ___stencil_6; }
	inline PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5 ** get_address_of_stencil_6() { return &___stencil_6; }
	inline void set_stencil_6(PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5 * value)
	{
		___stencil_6 = value;
		Il2CppCodeGenWriteBarrier((&___stencil_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPATHELEMENT_T33F2F0C15FFA86FCE2BB3857AE271A01D7CA561E_H
#ifndef DEFSELEMENT_T89F1B8B79F84DBAEDDBF170EE2B324AF2730AD9C_H
#define DEFSELEMENT_T89F1B8B79F84DBAEDDBF170EE2B324AF2730AD9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.DefsElement
struct  DefsElement_t89F1B8B79F84DBAEDDBF170EE2B324AF2730AD9C  : public SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFSELEMENT_T89F1B8B79F84DBAEDDBF170EE2B324AF2730AD9C_H
#ifndef GRAPHICALELEMENT_TDCF600D6023CE5078883348F9B0A76C990D3131D_H
#define GRAPHICALELEMENT_TDCF600D6023CE5078883348F9B0A76C990D3131D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.GraphicalElement
struct  GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D  : public SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888
{
public:
	// SimplySVG.GraphicalAttributes SimplySVG.GraphicalElement::localGraphicalAttributes
	GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D * ___localGraphicalAttributes_5;
	// SimplySVG.TransformAttributes SimplySVG.GraphicalElement::localTransformAttributes
	TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3 * ___localTransformAttributes_6;
	// System.Collections.Generic.List`1<SimplySVG.GraphicalElement/ContourPath> SimplySVG.GraphicalElement::contourPaths
	List_1_tB78032FDEAD332482A68D6E7AA788282C763C40F * ___contourPaths_7;
	// ClipperLib.PolyTree SimplySVG.GraphicalElement::stencilTree
	PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5 * ___stencilTree_8;
	// ClipperLib.PolyTree SimplySVG.GraphicalElement::fillTree
	PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5 * ___fillTree_9;
	// ClipperLib.PolyTree SimplySVG.GraphicalElement::openStrokeTree
	PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5 * ___openStrokeTree_10;
	// ClipperLib.PolyTree SimplySVG.GraphicalElement::closedStrokeTree
	PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5 * ___closedStrokeTree_11;

public:
	inline static int32_t get_offset_of_localGraphicalAttributes_5() { return static_cast<int32_t>(offsetof(GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D, ___localGraphicalAttributes_5)); }
	inline GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D * get_localGraphicalAttributes_5() const { return ___localGraphicalAttributes_5; }
	inline GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D ** get_address_of_localGraphicalAttributes_5() { return &___localGraphicalAttributes_5; }
	inline void set_localGraphicalAttributes_5(GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D * value)
	{
		___localGraphicalAttributes_5 = value;
		Il2CppCodeGenWriteBarrier((&___localGraphicalAttributes_5), value);
	}

	inline static int32_t get_offset_of_localTransformAttributes_6() { return static_cast<int32_t>(offsetof(GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D, ___localTransformAttributes_6)); }
	inline TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3 * get_localTransformAttributes_6() const { return ___localTransformAttributes_6; }
	inline TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3 ** get_address_of_localTransformAttributes_6() { return &___localTransformAttributes_6; }
	inline void set_localTransformAttributes_6(TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3 * value)
	{
		___localTransformAttributes_6 = value;
		Il2CppCodeGenWriteBarrier((&___localTransformAttributes_6), value);
	}

	inline static int32_t get_offset_of_contourPaths_7() { return static_cast<int32_t>(offsetof(GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D, ___contourPaths_7)); }
	inline List_1_tB78032FDEAD332482A68D6E7AA788282C763C40F * get_contourPaths_7() const { return ___contourPaths_7; }
	inline List_1_tB78032FDEAD332482A68D6E7AA788282C763C40F ** get_address_of_contourPaths_7() { return &___contourPaths_7; }
	inline void set_contourPaths_7(List_1_tB78032FDEAD332482A68D6E7AA788282C763C40F * value)
	{
		___contourPaths_7 = value;
		Il2CppCodeGenWriteBarrier((&___contourPaths_7), value);
	}

	inline static int32_t get_offset_of_stencilTree_8() { return static_cast<int32_t>(offsetof(GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D, ___stencilTree_8)); }
	inline PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5 * get_stencilTree_8() const { return ___stencilTree_8; }
	inline PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5 ** get_address_of_stencilTree_8() { return &___stencilTree_8; }
	inline void set_stencilTree_8(PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5 * value)
	{
		___stencilTree_8 = value;
		Il2CppCodeGenWriteBarrier((&___stencilTree_8), value);
	}

	inline static int32_t get_offset_of_fillTree_9() { return static_cast<int32_t>(offsetof(GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D, ___fillTree_9)); }
	inline PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5 * get_fillTree_9() const { return ___fillTree_9; }
	inline PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5 ** get_address_of_fillTree_9() { return &___fillTree_9; }
	inline void set_fillTree_9(PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5 * value)
	{
		___fillTree_9 = value;
		Il2CppCodeGenWriteBarrier((&___fillTree_9), value);
	}

	inline static int32_t get_offset_of_openStrokeTree_10() { return static_cast<int32_t>(offsetof(GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D, ___openStrokeTree_10)); }
	inline PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5 * get_openStrokeTree_10() const { return ___openStrokeTree_10; }
	inline PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5 ** get_address_of_openStrokeTree_10() { return &___openStrokeTree_10; }
	inline void set_openStrokeTree_10(PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5 * value)
	{
		___openStrokeTree_10 = value;
		Il2CppCodeGenWriteBarrier((&___openStrokeTree_10), value);
	}

	inline static int32_t get_offset_of_closedStrokeTree_11() { return static_cast<int32_t>(offsetof(GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D, ___closedStrokeTree_11)); }
	inline PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5 * get_closedStrokeTree_11() const { return ___closedStrokeTree_11; }
	inline PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5 ** get_address_of_closedStrokeTree_11() { return &___closedStrokeTree_11; }
	inline void set_closedStrokeTree_11(PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5 * value)
	{
		___closedStrokeTree_11 = value;
		Il2CppCodeGenWriteBarrier((&___closedStrokeTree_11), value);
	}
};

struct GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D_StaticFields
{
public:
	// System.Double SimplySVG.GraphicalElement::clipperCoordinateScale
	double ___clipperCoordinateScale_4;

public:
	inline static int32_t get_offset_of_clipperCoordinateScale_4() { return static_cast<int32_t>(offsetof(GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D_StaticFields, ___clipperCoordinateScale_4)); }
	inline double get_clipperCoordinateScale_4() const { return ___clipperCoordinateScale_4; }
	inline double* get_address_of_clipperCoordinateScale_4() { return &___clipperCoordinateScale_4; }
	inline void set_clipperCoordinateScale_4(double value)
	{
		___clipperCoordinateScale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHICALELEMENT_TDCF600D6023CE5078883348F9B0A76C990D3131D_H
#ifndef GROUPELEMENT_T8F6CB728CEC1D10D2E14197B062FF41F02E1CC72_H
#define GROUPELEMENT_T8F6CB728CEC1D10D2E14197B062FF41F02E1CC72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.GroupElement
struct  GroupElement_t8F6CB728CEC1D10D2E14197B062FF41F02E1CC72  : public SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888
{
public:
	// SimplySVG.GraphicalAttributes SimplySVG.GroupElement::localGraphicalAttributes
	GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D * ___localGraphicalAttributes_4;
	// SimplySVG.TransformAttributes SimplySVG.GroupElement::localTransformAttributes
	TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3 * ___localTransformAttributes_5;

public:
	inline static int32_t get_offset_of_localGraphicalAttributes_4() { return static_cast<int32_t>(offsetof(GroupElement_t8F6CB728CEC1D10D2E14197B062FF41F02E1CC72, ___localGraphicalAttributes_4)); }
	inline GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D * get_localGraphicalAttributes_4() const { return ___localGraphicalAttributes_4; }
	inline GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D ** get_address_of_localGraphicalAttributes_4() { return &___localGraphicalAttributes_4; }
	inline void set_localGraphicalAttributes_4(GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D * value)
	{
		___localGraphicalAttributes_4 = value;
		Il2CppCodeGenWriteBarrier((&___localGraphicalAttributes_4), value);
	}

	inline static int32_t get_offset_of_localTransformAttributes_5() { return static_cast<int32_t>(offsetof(GroupElement_t8F6CB728CEC1D10D2E14197B062FF41F02E1CC72, ___localTransformAttributes_5)); }
	inline TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3 * get_localTransformAttributes_5() const { return ___localTransformAttributes_5; }
	inline TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3 ** get_address_of_localTransformAttributes_5() { return &___localTransformAttributes_5; }
	inline void set_localTransformAttributes_5(TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3 * value)
	{
		___localTransformAttributes_5 = value;
		Il2CppCodeGenWriteBarrier((&___localTransformAttributes_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPELEMENT_T8F6CB728CEC1D10D2E14197B062FF41F02E1CC72_H
#ifndef ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#define ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF  : public ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C
{
public:

public:
};

struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_com
{
};
#endif // ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifndef INT32_TC16F64335CE8B56D99229DE94BB3A876ED55FE87_H
#define INT32_TC16F64335CE8B56D99229DE94BB3A876ED55FE87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_tC16F64335CE8B56D99229DE94BB3A876ED55FE87 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_tC16F64335CE8B56D99229DE94BB3A876ED55FE87, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_TC16F64335CE8B56D99229DE94BB3A876ED55FE87_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T40336B80652F698D83C0B52D98440BC0C0929136_H
#define NULLABLE_1_T40336B80652F698D83C0B52D98440BC0C0929136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t40336B80652F698D83C0B52D98440BC0C0929136 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t40336B80652F698D83C0B52D98440BC0C0929136, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t40336B80652F698D83C0B52D98440BC0C0929136, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T40336B80652F698D83C0B52D98440BC0C0929136_H
#ifndef NULLABLE_1_TE4EDC8D5ED2772A911F67696644E6C77FA716DC0_H
#define NULLABLE_1_TE4EDC8D5ED2772A911F67696644E6C77FA716DC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Single>
struct  Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 
{
public:
	// T System.Nullable`1::value
	float ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TE4EDC8D5ED2772A911F67696644E6C77FA716DC0_H
#ifndef SINGLE_TF2613CF3F3AF0E8BEBC3CE8AD41479C44E6075DB_H
#define SINGLE_TF2613CF3F3AF0E8BEBC3CE8AD41479C44E6075DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tF2613CF3F3AF0E8BEBC3CE8AD41479C44E6075DB 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_tF2613CF3F3AF0E8BEBC3CE8AD41479C44E6075DB, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TF2613CF3F3AF0E8BEBC3CE8AD41479C44E6075DB_H
#ifndef VOID_TDB81A15FA2AB53E2401A76B745D215397B29F783_H
#define VOID_TDB81A15FA2AB53E2401A76B745D215397B29F783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_tDB81A15FA2AB53E2401A76B745D215397B29F783 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_TDB81A15FA2AB53E2401A76B745D215397B29F783_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#define DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#define VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___zeroVector_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___oneVector_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TC8332394FBFEEB4B73459A35E182942340DA3537_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TC8332394FBFEEB4B73459A35E182942340DA3537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TC8332394FBFEEB4B73459A35E182942340DA3537_H
#ifndef POLYFILLTYPE_T2EDC6B8C9F60F9513818AA9D517973836E0623B7_H
#define POLYFILLTYPE_T2EDC6B8C9F60F9513818AA9D517973836E0623B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.PolyFillType
struct  PolyFillType_t2EDC6B8C9F60F9513818AA9D517973836E0623B7 
{
public:
	// System.Int32 ClipperLib.PolyFillType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PolyFillType_t2EDC6B8C9F60F9513818AA9D517973836E0623B7, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYFILLTYPE_T2EDC6B8C9F60F9513818AA9D517973836E0623B7_H
#ifndef ETRANSPARENTMODE_T371F435EE5FDB18BBCC5DACA1C309E541954CCDA_H
#define ETRANSPARENTMODE_T371F435EE5FDB18BBCC5DACA1C309E541954CCDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Electricity/ETransparentMode
struct  ETransparentMode_t371F435EE5FDB18BBCC5DACA1C309E541954CCDA 
{
public:
	// System.Int32 Electricity/ETransparentMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ETransparentMode_t371F435EE5FDB18BBCC5DACA1C309E541954CCDA, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ETRANSPARENTMODE_T371F435EE5FDB18BBCC5DACA1C309E541954CCDA_H
#ifndef CIRCLEELEMENT_T6728507302B4FCFC11507336EACCB8C925E59C17_H
#define CIRCLEELEMENT_T6728507302B4FCFC11507336EACCB8C925E59C17_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.CircleElement
struct  CircleElement_t6728507302B4FCFC11507336EACCB8C925E59C17  : public GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D
{
public:
	// System.Single SimplySVG.CircleElement::cx
	float ___cx_12;
	// System.Single SimplySVG.CircleElement::cy
	float ___cy_13;
	// System.Single SimplySVG.CircleElement::r
	float ___r_14;

public:
	inline static int32_t get_offset_of_cx_12() { return static_cast<int32_t>(offsetof(CircleElement_t6728507302B4FCFC11507336EACCB8C925E59C17, ___cx_12)); }
	inline float get_cx_12() const { return ___cx_12; }
	inline float* get_address_of_cx_12() { return &___cx_12; }
	inline void set_cx_12(float value)
	{
		___cx_12 = value;
	}

	inline static int32_t get_offset_of_cy_13() { return static_cast<int32_t>(offsetof(CircleElement_t6728507302B4FCFC11507336EACCB8C925E59C17, ___cy_13)); }
	inline float get_cy_13() const { return ___cy_13; }
	inline float* get_address_of_cy_13() { return &___cy_13; }
	inline void set_cy_13(float value)
	{
		___cy_13 = value;
	}

	inline static int32_t get_offset_of_r_14() { return static_cast<int32_t>(offsetof(CircleElement_t6728507302B4FCFC11507336EACCB8C925E59C17, ___r_14)); }
	inline float get_r_14() const { return ___r_14; }
	inline float* get_address_of_r_14() { return &___r_14; }
	inline void set_r_14(float value)
	{
		___r_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIRCLEELEMENT_T6728507302B4FCFC11507336EACCB8C925E59C17_H
#ifndef ELLIPSEELEMENT_TBA913A69B5B46788D424D865C36E80416847159D_H
#define ELLIPSEELEMENT_TBA913A69B5B46788D424D865C36E80416847159D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.EllipseElement
struct  EllipseElement_tBA913A69B5B46788D424D865C36E80416847159D  : public GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D
{
public:
	// System.Single SimplySVG.EllipseElement::cx
	float ___cx_12;
	// System.Single SimplySVG.EllipseElement::cy
	float ___cy_13;
	// System.Single SimplySVG.EllipseElement::rx
	float ___rx_14;
	// System.Single SimplySVG.EllipseElement::ry
	float ___ry_15;

public:
	inline static int32_t get_offset_of_cx_12() { return static_cast<int32_t>(offsetof(EllipseElement_tBA913A69B5B46788D424D865C36E80416847159D, ___cx_12)); }
	inline float get_cx_12() const { return ___cx_12; }
	inline float* get_address_of_cx_12() { return &___cx_12; }
	inline void set_cx_12(float value)
	{
		___cx_12 = value;
	}

	inline static int32_t get_offset_of_cy_13() { return static_cast<int32_t>(offsetof(EllipseElement_tBA913A69B5B46788D424D865C36E80416847159D, ___cy_13)); }
	inline float get_cy_13() const { return ___cy_13; }
	inline float* get_address_of_cy_13() { return &___cy_13; }
	inline void set_cy_13(float value)
	{
		___cy_13 = value;
	}

	inline static int32_t get_offset_of_rx_14() { return static_cast<int32_t>(offsetof(EllipseElement_tBA913A69B5B46788D424D865C36E80416847159D, ___rx_14)); }
	inline float get_rx_14() const { return ___rx_14; }
	inline float* get_address_of_rx_14() { return &___rx_14; }
	inline void set_rx_14(float value)
	{
		___rx_14 = value;
	}

	inline static int32_t get_offset_of_ry_15() { return static_cast<int32_t>(offsetof(EllipseElement_tBA913A69B5B46788D424D865C36E80416847159D, ___ry_15)); }
	inline float get_ry_15() const { return ___ry_15; }
	inline float* get_address_of_ry_15() { return &___ry_15; }
	inline void set_ry_15(float value)
	{
		___ry_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELLIPSEELEMENT_TBA913A69B5B46788D424D865C36E80416847159D_H
#ifndef LINEELEMENT_TA17BDE9B13B5BBD50226B780E30B61E53AF93F73_H
#define LINEELEMENT_TA17BDE9B13B5BBD50226B780E30B61E53AF93F73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.LineElement
struct  LineElement_tA17BDE9B13B5BBD50226B780E30B61E53AF93F73  : public GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D
{
public:
	// System.Single SimplySVG.LineElement::x1
	float ___x1_12;
	// System.Single SimplySVG.LineElement::y1
	float ___y1_13;
	// System.Single SimplySVG.LineElement::x2
	float ___x2_14;
	// System.Single SimplySVG.LineElement::y2
	float ___y2_15;

public:
	inline static int32_t get_offset_of_x1_12() { return static_cast<int32_t>(offsetof(LineElement_tA17BDE9B13B5BBD50226B780E30B61E53AF93F73, ___x1_12)); }
	inline float get_x1_12() const { return ___x1_12; }
	inline float* get_address_of_x1_12() { return &___x1_12; }
	inline void set_x1_12(float value)
	{
		___x1_12 = value;
	}

	inline static int32_t get_offset_of_y1_13() { return static_cast<int32_t>(offsetof(LineElement_tA17BDE9B13B5BBD50226B780E30B61E53AF93F73, ___y1_13)); }
	inline float get_y1_13() const { return ___y1_13; }
	inline float* get_address_of_y1_13() { return &___y1_13; }
	inline void set_y1_13(float value)
	{
		___y1_13 = value;
	}

	inline static int32_t get_offset_of_x2_14() { return static_cast<int32_t>(offsetof(LineElement_tA17BDE9B13B5BBD50226B780E30B61E53AF93F73, ___x2_14)); }
	inline float get_x2_14() const { return ___x2_14; }
	inline float* get_address_of_x2_14() { return &___x2_14; }
	inline void set_x2_14(float value)
	{
		___x2_14 = value;
	}

	inline static int32_t get_offset_of_y2_15() { return static_cast<int32_t>(offsetof(LineElement_tA17BDE9B13B5BBD50226B780E30B61E53AF93F73, ___y2_15)); }
	inline float get_y2_15() const { return ___y2_15; }
	inline float* get_address_of_y2_15() { return &___y2_15; }
	inline void set_y2_15(float value)
	{
		___y2_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEELEMENT_TA17BDE9B13B5BBD50226B780E30B61E53AF93F73_H
#ifndef PATHELEMENT_TDAFCC03770733653E4E344DAD73CC3E7AF9D7025_H
#define PATHELEMENT_TDAFCC03770733653E4E344DAD73CC3E7AF9D7025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.PathElement
struct  PathElement_tDAFCC03770733653E4E344DAD73CC3E7AF9D7025  : public GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D
{
public:
	// System.Collections.Generic.List`1<SimplySVG.PathElement/SubPath> SimplySVG.PathElement::subPaths
	List_1_tB0ADE6520BA0060AD719C7FA0DBA8FF95A2559A5 * ___subPaths_12;

public:
	inline static int32_t get_offset_of_subPaths_12() { return static_cast<int32_t>(offsetof(PathElement_tDAFCC03770733653E4E344DAD73CC3E7AF9D7025, ___subPaths_12)); }
	inline List_1_tB0ADE6520BA0060AD719C7FA0DBA8FF95A2559A5 * get_subPaths_12() const { return ___subPaths_12; }
	inline List_1_tB0ADE6520BA0060AD719C7FA0DBA8FF95A2559A5 ** get_address_of_subPaths_12() { return &___subPaths_12; }
	inline void set_subPaths_12(List_1_tB0ADE6520BA0060AD719C7FA0DBA8FF95A2559A5 * value)
	{
		___subPaths_12 = value;
		Il2CppCodeGenWriteBarrier((&___subPaths_12), value);
	}
};

struct PathElement_tDAFCC03770733653E4E344DAD73CC3E7AF9D7025_StaticFields
{
public:
	// System.Func`2<System.String,System.Boolean> SimplySVG.PathElement::<>f__am$cache0
	Func_2_t3AD4B0F443BFD399C4AC2D6EE99FFE3BC0970017 * ___U3CU3Ef__amU24cache0_13;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_13() { return static_cast<int32_t>(offsetof(PathElement_tDAFCC03770733653E4E344DAD73CC3E7AF9D7025_StaticFields, ___U3CU3Ef__amU24cache0_13)); }
	inline Func_2_t3AD4B0F443BFD399C4AC2D6EE99FFE3BC0970017 * get_U3CU3Ef__amU24cache0_13() const { return ___U3CU3Ef__amU24cache0_13; }
	inline Func_2_t3AD4B0F443BFD399C4AC2D6EE99FFE3BC0970017 ** get_address_of_U3CU3Ef__amU24cache0_13() { return &___U3CU3Ef__amU24cache0_13; }
	inline void set_U3CU3Ef__amU24cache0_13(Func_2_t3AD4B0F443BFD399C4AC2D6EE99FFE3BC0970017 * value)
	{
		___U3CU3Ef__amU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHELEMENT_TDAFCC03770733653E4E344DAD73CC3E7AF9D7025_H
#ifndef U3CBUILDSHAPEU3EC__ANONSTOREY1_T578BE985F5CE829848E84EB8798614B40AA7B534_H
#define U3CBUILDSHAPEU3EC__ANONSTOREY1_T578BE985F5CE829848E84EB8798614B40AA7B534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.PathElement/<BuildShape>c__AnonStorey1
struct  U3CBuildShapeU3Ec__AnonStorey1_t578BE985F5CE829848E84EB8798614B40AA7B534  : public RuntimeObject
{
public:
	// UnityEngine.Vector2 SimplySVG.PathElement/<BuildShape>c__AnonStorey1::p_0
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___p_0_0;
	// UnityEngine.Vector2 SimplySVG.PathElement/<BuildShape>c__AnonStorey1::p_1
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___p_1_1;
	// UnityEngine.Vector2 SimplySVG.PathElement/<BuildShape>c__AnonStorey1::p_2
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___p_2_2;
	// UnityEngine.Vector2 SimplySVG.PathElement/<BuildShape>c__AnonStorey1::p_3
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___p_3_3;

public:
	inline static int32_t get_offset_of_p_0_0() { return static_cast<int32_t>(offsetof(U3CBuildShapeU3Ec__AnonStorey1_t578BE985F5CE829848E84EB8798614B40AA7B534, ___p_0_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_p_0_0() const { return ___p_0_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_p_0_0() { return &___p_0_0; }
	inline void set_p_0_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___p_0_0 = value;
	}

	inline static int32_t get_offset_of_p_1_1() { return static_cast<int32_t>(offsetof(U3CBuildShapeU3Ec__AnonStorey1_t578BE985F5CE829848E84EB8798614B40AA7B534, ___p_1_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_p_1_1() const { return ___p_1_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_p_1_1() { return &___p_1_1; }
	inline void set_p_1_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___p_1_1 = value;
	}

	inline static int32_t get_offset_of_p_2_2() { return static_cast<int32_t>(offsetof(U3CBuildShapeU3Ec__AnonStorey1_t578BE985F5CE829848E84EB8798614B40AA7B534, ___p_2_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_p_2_2() const { return ___p_2_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_p_2_2() { return &___p_2_2; }
	inline void set_p_2_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___p_2_2 = value;
	}

	inline static int32_t get_offset_of_p_3_3() { return static_cast<int32_t>(offsetof(U3CBuildShapeU3Ec__AnonStorey1_t578BE985F5CE829848E84EB8798614B40AA7B534, ___p_3_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_p_3_3() const { return ___p_3_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_p_3_3() { return &___p_3_3; }
	inline void set_p_3_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___p_3_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBUILDSHAPEU3EC__ANONSTOREY1_T578BE985F5CE829848E84EB8798614B40AA7B534_H
#ifndef U3CBUILDSHAPEU3EC__ANONSTOREY2_T432E5413D51C5401FAA8E6CB5A2E4E11022F605B_H
#define U3CBUILDSHAPEU3EC__ANONSTOREY2_T432E5413D51C5401FAA8E6CB5A2E4E11022F605B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.PathElement/<BuildShape>c__AnonStorey2
struct  U3CBuildShapeU3Ec__AnonStorey2_t432E5413D51C5401FAA8E6CB5A2E4E11022F605B  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 SimplySVG.PathElement/<BuildShape>c__AnonStorey2::p_0
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___p_0_0;
	// UnityEngine.Vector3 SimplySVG.PathElement/<BuildShape>c__AnonStorey2::p_1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___p_1_1;
	// UnityEngine.Vector3 SimplySVG.PathElement/<BuildShape>c__AnonStorey2::p_2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___p_2_2;

public:
	inline static int32_t get_offset_of_p_0_0() { return static_cast<int32_t>(offsetof(U3CBuildShapeU3Ec__AnonStorey2_t432E5413D51C5401FAA8E6CB5A2E4E11022F605B, ___p_0_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_p_0_0() const { return ___p_0_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_p_0_0() { return &___p_0_0; }
	inline void set_p_0_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___p_0_0 = value;
	}

	inline static int32_t get_offset_of_p_1_1() { return static_cast<int32_t>(offsetof(U3CBuildShapeU3Ec__AnonStorey2_t432E5413D51C5401FAA8E6CB5A2E4E11022F605B, ___p_1_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_p_1_1() const { return ___p_1_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_p_1_1() { return &___p_1_1; }
	inline void set_p_1_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___p_1_1 = value;
	}

	inline static int32_t get_offset_of_p_2_2() { return static_cast<int32_t>(offsetof(U3CBuildShapeU3Ec__AnonStorey2_t432E5413D51C5401FAA8E6CB5A2E4E11022F605B, ___p_2_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_p_2_2() const { return ___p_2_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_p_2_2() { return &___p_2_2; }
	inline void set_p_2_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___p_2_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBUILDSHAPEU3EC__ANONSTOREY2_T432E5413D51C5401FAA8E6CB5A2E4E11022F605B_H
#ifndef SEGMENTTYPE_T50BD7FD0D2405225267ADAFB03820A0824E04926_H
#define SEGMENTTYPE_T50BD7FD0D2405225267ADAFB03820A0824E04926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.PathElement/PathComponent/SegmentType
struct  SegmentType_t50BD7FD0D2405225267ADAFB03820A0824E04926 
{
public:
	// System.Int32 SimplySVG.PathElement/PathComponent/SegmentType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SegmentType_t50BD7FD0D2405225267ADAFB03820A0824E04926, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEGMENTTYPE_T50BD7FD0D2405225267ADAFB03820A0824E04926_H
#ifndef POLYGONELEMENT_T8AFB041E1A75C7DB015FF9F5CE41C80DABB7098D_H
#define POLYGONELEMENT_T8AFB041E1A75C7DB015FF9F5CE41C80DABB7098D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.PolygonElement
struct  PolygonElement_t8AFB041E1A75C7DB015FF9F5CE41C80DABB7098D  : public GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector2> SimplySVG.PolygonElement::points
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___points_12;

public:
	inline static int32_t get_offset_of_points_12() { return static_cast<int32_t>(offsetof(PolygonElement_t8AFB041E1A75C7DB015FF9F5CE41C80DABB7098D, ___points_12)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_points_12() const { return ___points_12; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_points_12() { return &___points_12; }
	inline void set_points_12(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___points_12 = value;
		Il2CppCodeGenWriteBarrier((&___points_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGONELEMENT_T8AFB041E1A75C7DB015FF9F5CE41C80DABB7098D_H
#ifndef POLYLINEELEMENT_T5AE0317375BCE446D91905B2CCBFF8386324F5B1_H
#define POLYLINEELEMENT_T5AE0317375BCE446D91905B2CCBFF8386324F5B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.PolylineElement
struct  PolylineElement_t5AE0317375BCE446D91905B2CCBFF8386324F5B1  : public GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector2> SimplySVG.PolylineElement::points
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___points_12;

public:
	inline static int32_t get_offset_of_points_12() { return static_cast<int32_t>(offsetof(PolylineElement_t5AE0317375BCE446D91905B2CCBFF8386324F5B1, ___points_12)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_points_12() const { return ___points_12; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_points_12() { return &___points_12; }
	inline void set_points_12(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___points_12 = value;
		Il2CppCodeGenWriteBarrier((&___points_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYLINEELEMENT_T5AE0317375BCE446D91905B2CCBFF8386324F5B1_H
#ifndef RECTELEMENT_T9B126A04ED3550785335C61B6F2524E442CA1DD0_H
#define RECTELEMENT_T9B126A04ED3550785335C61B6F2524E442CA1DD0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.RectElement
struct  RectElement_t9B126A04ED3550785335C61B6F2524E442CA1DD0  : public GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D
{
public:
	// System.Single SimplySVG.RectElement::x
	float ___x_12;
	// System.Single SimplySVG.RectElement::y
	float ___y_13;
	// System.Single SimplySVG.RectElement::width
	float ___width_14;
	// System.Single SimplySVG.RectElement::height
	float ___height_15;

public:
	inline static int32_t get_offset_of_x_12() { return static_cast<int32_t>(offsetof(RectElement_t9B126A04ED3550785335C61B6F2524E442CA1DD0, ___x_12)); }
	inline float get_x_12() const { return ___x_12; }
	inline float* get_address_of_x_12() { return &___x_12; }
	inline void set_x_12(float value)
	{
		___x_12 = value;
	}

	inline static int32_t get_offset_of_y_13() { return static_cast<int32_t>(offsetof(RectElement_t9B126A04ED3550785335C61B6F2524E442CA1DD0, ___y_13)); }
	inline float get_y_13() const { return ___y_13; }
	inline float* get_address_of_y_13() { return &___y_13; }
	inline void set_y_13(float value)
	{
		___y_13 = value;
	}

	inline static int32_t get_offset_of_width_14() { return static_cast<int32_t>(offsetof(RectElement_t9B126A04ED3550785335C61B6F2524E442CA1DD0, ___width_14)); }
	inline float get_width_14() const { return ___width_14; }
	inline float* get_address_of_width_14() { return &___width_14; }
	inline void set_width_14(float value)
	{
		___width_14 = value;
	}

	inline static int32_t get_offset_of_height_15() { return static_cast<int32_t>(offsetof(RectElement_t9B126A04ED3550785335C61B6F2524E442CA1DD0, ___height_15)); }
	inline float get_height_15() const { return ___height_15; }
	inline float* get_address_of_height_15() { return &___height_15; }
	inline void set_height_15(float value)
	{
		___height_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTELEMENT_T9B126A04ED3550785335C61B6F2524E442CA1DD0_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_8)); }
	inline DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 * get_data_8() const { return ___data_8; }
	inline DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T_H
#ifndef NULLABLE_1_TB988CB5DAB19DC0D2A86D474B7B4B8A87AE764E9_H
#define NULLABLE_1_TB988CB5DAB19DC0D2A86D474B7B4B8A87AE764E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Color>
struct  Nullable_1_tB988CB5DAB19DC0D2A86D474B7B4B8A87AE764E9 
{
public:
	// T System.Nullable`1::value
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tB988CB5DAB19DC0D2A86D474B7B4B8A87AE764E9, ___value_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_value_0() const { return ___value_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tB988CB5DAB19DC0D2A86D474B7B4B8A87AE764E9, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TB988CB5DAB19DC0D2A86D474B7B4B8A87AE764E9_H
#ifndef TOUCHTYPES_TCEE6B2D4570D8BA114C071D5680AF688C3CCFC2F_H
#define TOUCHTYPES_TCEE6B2D4570D8BA114C071D5680AF688C3CCFC2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchTypes
struct  TouchTypes_tCEE6B2D4570D8BA114C071D5680AF688C3CCFC2F 
{
public:
	// System.Int32 TouchTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchTypes_tCEE6B2D4570D8BA114C071D5680AF688C3CCFC2F, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPES_TCEE6B2D4570D8BA114C071D5680AF688C3CCFC2F_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#define RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Origin_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Direction_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#ifndef RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#define RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Point_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Normal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_UV_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#ifndef RAYCASTHIT2D_T5E8A7F96317BAF2033362FC780F4D72DC72764BE_H
#define RAYCASTHIT2D_T5E8A7F96317BAF2033362FC780F4D72DC72764BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// System.Int32 UnityEngine.RaycastHit2D::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Centroid_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Point_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Normal_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT2D_T5E8A7F96317BAF2033362FC780F4D72DC72764BE_H
#ifndef TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#define TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_tEC19034D476659A5E05366C63564F34DD30E7C57 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAnchor_tEC19034D476659A5E05366C63564F34DD30E7C57, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#ifndef VERTEXHELPER_T27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_H
#define VERTEXHELPER_T27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VertexHelper
struct  VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_t4A961510635950236678F1B3B2436ECCAF28713A * ___m_Indices_8;
	// System.Boolean UnityEngine.UI.VertexHelper::m_ListsInitalized
	bool ___m_ListsInitalized_11;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Positions_0)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Positions_0), value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Colors_1)); }
	inline List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Colors_1), value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv0S_2)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv0S_2), value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv1S_3)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv1S_3), value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv2S_4)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv2S_4), value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv3S_5)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv3S_5), value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Normals_6)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normals_6), value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Tangents_7)); }
	inline List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tangents_7), value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Indices_8)); }
	inline List_1_t4A961510635950236678F1B3B2436ECCAF28713A * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_t4A961510635950236678F1B3B2436ECCAF28713A ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_t4A961510635950236678F1B3B2436ECCAF28713A * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Indices_8), value);
	}

	inline static int32_t get_offset_of_m_ListsInitalized_11() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_ListsInitalized_11)); }
	inline bool get_m_ListsInitalized_11() const { return ___m_ListsInitalized_11; }
	inline bool* get_address_of_m_ListsInitalized_11() { return &___m_ListsInitalized_11; }
	inline void set_m_ListsInitalized_11(bool value)
	{
		___m_ListsInitalized_11 = value;
	}
};

struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___s_DefaultNormal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPER_T27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_H
#ifndef PATHCOMPONENT_T34B34D47FB4CDCAA01D53BECAC8F852ADC801020_H
#define PATHCOMPONENT_T34B34D47FB4CDCAA01D53BECAC8F852ADC801020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.PathElement/PathComponent
struct  PathComponent_t34B34D47FB4CDCAA01D53BECAC8F852ADC801020  : public RuntimeObject
{
public:
	// SimplySVG.PathElement/PathComponent/SegmentType SimplySVG.PathElement/PathComponent::segmentType
	int32_t ___segmentType_0;
	// UnityEngine.Vector3 SimplySVG.PathElement/PathComponent::pos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pos_1;
	// UnityEngine.Vector3 SimplySVG.PathElement/PathComponent::startCurvePos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___startCurvePos_2;
	// UnityEngine.Vector3 SimplySVG.PathElement/PathComponent::endCurvePos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___endCurvePos_3;
	// System.Boolean SimplySVG.PathElement/PathComponent::useStartCurvePos
	bool ___useStartCurvePos_4;
	// System.Boolean SimplySVG.PathElement/PathComponent::useEndCurvePos
	bool ___useEndCurvePos_5;
	// UnityEngine.Vector2 SimplySVG.PathElement/PathComponent::arcRadius
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___arcRadius_6;
	// System.Single SimplySVG.PathElement/PathComponent::arcRotation
	float ___arcRotation_7;
	// System.Boolean SimplySVG.PathElement/PathComponent::arcLarge
	bool ___arcLarge_8;
	// System.Boolean SimplySVG.PathElement/PathComponent::arcSweep
	bool ___arcSweep_9;

public:
	inline static int32_t get_offset_of_segmentType_0() { return static_cast<int32_t>(offsetof(PathComponent_t34B34D47FB4CDCAA01D53BECAC8F852ADC801020, ___segmentType_0)); }
	inline int32_t get_segmentType_0() const { return ___segmentType_0; }
	inline int32_t* get_address_of_segmentType_0() { return &___segmentType_0; }
	inline void set_segmentType_0(int32_t value)
	{
		___segmentType_0 = value;
	}

	inline static int32_t get_offset_of_pos_1() { return static_cast<int32_t>(offsetof(PathComponent_t34B34D47FB4CDCAA01D53BECAC8F852ADC801020, ___pos_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_pos_1() const { return ___pos_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_pos_1() { return &___pos_1; }
	inline void set_pos_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___pos_1 = value;
	}

	inline static int32_t get_offset_of_startCurvePos_2() { return static_cast<int32_t>(offsetof(PathComponent_t34B34D47FB4CDCAA01D53BECAC8F852ADC801020, ___startCurvePos_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_startCurvePos_2() const { return ___startCurvePos_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_startCurvePos_2() { return &___startCurvePos_2; }
	inline void set_startCurvePos_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___startCurvePos_2 = value;
	}

	inline static int32_t get_offset_of_endCurvePos_3() { return static_cast<int32_t>(offsetof(PathComponent_t34B34D47FB4CDCAA01D53BECAC8F852ADC801020, ___endCurvePos_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_endCurvePos_3() const { return ___endCurvePos_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_endCurvePos_3() { return &___endCurvePos_3; }
	inline void set_endCurvePos_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___endCurvePos_3 = value;
	}

	inline static int32_t get_offset_of_useStartCurvePos_4() { return static_cast<int32_t>(offsetof(PathComponent_t34B34D47FB4CDCAA01D53BECAC8F852ADC801020, ___useStartCurvePos_4)); }
	inline bool get_useStartCurvePos_4() const { return ___useStartCurvePos_4; }
	inline bool* get_address_of_useStartCurvePos_4() { return &___useStartCurvePos_4; }
	inline void set_useStartCurvePos_4(bool value)
	{
		___useStartCurvePos_4 = value;
	}

	inline static int32_t get_offset_of_useEndCurvePos_5() { return static_cast<int32_t>(offsetof(PathComponent_t34B34D47FB4CDCAA01D53BECAC8F852ADC801020, ___useEndCurvePos_5)); }
	inline bool get_useEndCurvePos_5() const { return ___useEndCurvePos_5; }
	inline bool* get_address_of_useEndCurvePos_5() { return &___useEndCurvePos_5; }
	inline void set_useEndCurvePos_5(bool value)
	{
		___useEndCurvePos_5 = value;
	}

	inline static int32_t get_offset_of_arcRadius_6() { return static_cast<int32_t>(offsetof(PathComponent_t34B34D47FB4CDCAA01D53BECAC8F852ADC801020, ___arcRadius_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_arcRadius_6() const { return ___arcRadius_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_arcRadius_6() { return &___arcRadius_6; }
	inline void set_arcRadius_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___arcRadius_6 = value;
	}

	inline static int32_t get_offset_of_arcRotation_7() { return static_cast<int32_t>(offsetof(PathComponent_t34B34D47FB4CDCAA01D53BECAC8F852ADC801020, ___arcRotation_7)); }
	inline float get_arcRotation_7() const { return ___arcRotation_7; }
	inline float* get_address_of_arcRotation_7() { return &___arcRotation_7; }
	inline void set_arcRotation_7(float value)
	{
		___arcRotation_7 = value;
	}

	inline static int32_t get_offset_of_arcLarge_8() { return static_cast<int32_t>(offsetof(PathComponent_t34B34D47FB4CDCAA01D53BECAC8F852ADC801020, ___arcLarge_8)); }
	inline bool get_arcLarge_8() const { return ___arcLarge_8; }
	inline bool* get_address_of_arcLarge_8() { return &___arcLarge_8; }
	inline void set_arcLarge_8(bool value)
	{
		___arcLarge_8 = value;
	}

	inline static int32_t get_offset_of_arcSweep_9() { return static_cast<int32_t>(offsetof(PathComponent_t34B34D47FB4CDCAA01D53BECAC8F852ADC801020, ___arcSweep_9)); }
	inline bool get_arcSweep_9() const { return ___arcSweep_9; }
	inline bool* get_address_of_arcSweep_9() { return &___arcSweep_9; }
	inline void set_arcSweep_9(bool value)
	{
		___arcSweep_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHCOMPONENT_T34B34D47FB4CDCAA01D53BECAC8F852ADC801020_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef NULLABLE_1_T78C1DB17B9A75FEEB42DE7DF9A20B105244755FF_H
#define NULLABLE_1_T78C1DB17B9A75FEEB42DE7DF9A20B105244755FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<ClipperLib.PolyFillType>
struct  Nullable_1_t78C1DB17B9A75FEEB42DE7DF9A20B105244755FF 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t78C1DB17B9A75FEEB42DE7DF9A20B105244755FF, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t78C1DB17B9A75FEEB42DE7DF9A20B105244755FF, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T78C1DB17B9A75FEEB42DE7DF9A20B105244755FF_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef RECURSESVGELEMENTS_T7025179A40653E0F0B00540527D136A2C1D5A1F3_H
#define RECURSESVGELEMENTS_T7025179A40653E0F0B00540527D136A2C1D5A1F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.ClipPathElement/RecurseSVGElements
struct  RecurseSVGElements_t7025179A40653E0F0B00540527D136A2C1D5A1F3  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECURSESVGELEMENTS_T7025179A40653E0F0B00540527D136A2C1D5A1F3_H
#ifndef COLLISIONSHAPEDATA_T09D2AF9F5D9E3B0C513049F97C117CF4EB9B2C6B_H
#define COLLISIONSHAPEDATA_T09D2AF9F5D9E3B0C513049F97C117CF4EB9B2C6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.CollisionShapeData
struct  CollisionShapeData_t09D2AF9F5D9E3B0C513049F97C117CF4EB9B2C6B  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<SimplySVG.CollisionShapeData/Polygon> SimplySVG.CollisionShapeData::collisionPolygons
	List_1_t62A212C062502261FA3CC4B1B311DCF5D334D32B * ___collisionPolygons_4;

public:
	inline static int32_t get_offset_of_collisionPolygons_4() { return static_cast<int32_t>(offsetof(CollisionShapeData_t09D2AF9F5D9E3B0C513049F97C117CF4EB9B2C6B, ___collisionPolygons_4)); }
	inline List_1_t62A212C062502261FA3CC4B1B311DCF5D334D32B * get_collisionPolygons_4() const { return ___collisionPolygons_4; }
	inline List_1_t62A212C062502261FA3CC4B1B311DCF5D334D32B ** get_address_of_collisionPolygons_4() { return &___collisionPolygons_4; }
	inline void set_collisionPolygons_4(List_1_t62A212C062502261FA3CC4B1B311DCF5D334D32B * value)
	{
		___collisionPolygons_4 = value;
		Il2CppCodeGenWriteBarrier((&___collisionPolygons_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISIONSHAPEDATA_T09D2AF9F5D9E3B0C513049F97C117CF4EB9B2C6B_H
#ifndef GRAPHICALATTRIBUTES_T22A39FF65774E208391CDCA2A02C776F6E2DF80D_H
#define GRAPHICALATTRIBUTES_T22A39FF65774E208391CDCA2A02C776F6E2DF80D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.GraphicalAttributes
struct  GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D  : public RuntimeObject
{
public:
	// System.Nullable`1<System.Single> SimplySVG.GraphicalAttributes::opacity
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___opacity_0;
	// System.Nullable`1<System.Boolean> SimplySVG.GraphicalAttributes::useFill
	Nullable_1_t40336B80652F698D83C0B52D98440BC0C0929136  ___useFill_1;
	// System.Nullable`1<UnityEngine.Color> SimplySVG.GraphicalAttributes::fillColor
	Nullable_1_tB988CB5DAB19DC0D2A86D474B7B4B8A87AE764E9  ___fillColor_2;
	// System.Nullable`1<System.Single> SimplySVG.GraphicalAttributes::fillOpacity
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___fillOpacity_3;
	// System.Nullable`1<System.Boolean> SimplySVG.GraphicalAttributes::useStroke
	Nullable_1_t40336B80652F698D83C0B52D98440BC0C0929136  ___useStroke_4;
	// System.Nullable`1<System.Single> SimplySVG.GraphicalAttributes::strokeWidth
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___strokeWidth_5;
	// System.Nullable`1<UnityEngine.Color> SimplySVG.GraphicalAttributes::strokeColor
	Nullable_1_tB988CB5DAB19DC0D2A86D474B7B4B8A87AE764E9  ___strokeColor_6;
	// System.Nullable`1<System.Single> SimplySVG.GraphicalAttributes::strokeOpacity
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___strokeOpacity_7;
	// System.Nullable`1<System.Single> SimplySVG.GraphicalAttributes::strokeMiterLimit
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___strokeMiterLimit_8;
	// System.Nullable`1<ClipperLib.PolyFillType> SimplySVG.GraphicalAttributes::fillRule
	Nullable_1_t78C1DB17B9A75FEEB42DE7DF9A20B105244755FF  ___fillRule_9;
	// System.String SimplySVG.GraphicalAttributes::clipPath
	String_t* ___clipPath_10;
	// System.Nullable`1<ClipperLib.PolyFillType> SimplySVG.GraphicalAttributes::clipRule
	Nullable_1_t78C1DB17B9A75FEEB42DE7DF9A20B105244755FF  ___clipRule_11;

public:
	inline static int32_t get_offset_of_opacity_0() { return static_cast<int32_t>(offsetof(GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D, ___opacity_0)); }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  get_opacity_0() const { return ___opacity_0; }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 * get_address_of_opacity_0() { return &___opacity_0; }
	inline void set_opacity_0(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  value)
	{
		___opacity_0 = value;
	}

	inline static int32_t get_offset_of_useFill_1() { return static_cast<int32_t>(offsetof(GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D, ___useFill_1)); }
	inline Nullable_1_t40336B80652F698D83C0B52D98440BC0C0929136  get_useFill_1() const { return ___useFill_1; }
	inline Nullable_1_t40336B80652F698D83C0B52D98440BC0C0929136 * get_address_of_useFill_1() { return &___useFill_1; }
	inline void set_useFill_1(Nullable_1_t40336B80652F698D83C0B52D98440BC0C0929136  value)
	{
		___useFill_1 = value;
	}

	inline static int32_t get_offset_of_fillColor_2() { return static_cast<int32_t>(offsetof(GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D, ___fillColor_2)); }
	inline Nullable_1_tB988CB5DAB19DC0D2A86D474B7B4B8A87AE764E9  get_fillColor_2() const { return ___fillColor_2; }
	inline Nullable_1_tB988CB5DAB19DC0D2A86D474B7B4B8A87AE764E9 * get_address_of_fillColor_2() { return &___fillColor_2; }
	inline void set_fillColor_2(Nullable_1_tB988CB5DAB19DC0D2A86D474B7B4B8A87AE764E9  value)
	{
		___fillColor_2 = value;
	}

	inline static int32_t get_offset_of_fillOpacity_3() { return static_cast<int32_t>(offsetof(GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D, ___fillOpacity_3)); }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  get_fillOpacity_3() const { return ___fillOpacity_3; }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 * get_address_of_fillOpacity_3() { return &___fillOpacity_3; }
	inline void set_fillOpacity_3(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  value)
	{
		___fillOpacity_3 = value;
	}

	inline static int32_t get_offset_of_useStroke_4() { return static_cast<int32_t>(offsetof(GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D, ___useStroke_4)); }
	inline Nullable_1_t40336B80652F698D83C0B52D98440BC0C0929136  get_useStroke_4() const { return ___useStroke_4; }
	inline Nullable_1_t40336B80652F698D83C0B52D98440BC0C0929136 * get_address_of_useStroke_4() { return &___useStroke_4; }
	inline void set_useStroke_4(Nullable_1_t40336B80652F698D83C0B52D98440BC0C0929136  value)
	{
		___useStroke_4 = value;
	}

	inline static int32_t get_offset_of_strokeWidth_5() { return static_cast<int32_t>(offsetof(GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D, ___strokeWidth_5)); }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  get_strokeWidth_5() const { return ___strokeWidth_5; }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 * get_address_of_strokeWidth_5() { return &___strokeWidth_5; }
	inline void set_strokeWidth_5(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  value)
	{
		___strokeWidth_5 = value;
	}

	inline static int32_t get_offset_of_strokeColor_6() { return static_cast<int32_t>(offsetof(GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D, ___strokeColor_6)); }
	inline Nullable_1_tB988CB5DAB19DC0D2A86D474B7B4B8A87AE764E9  get_strokeColor_6() const { return ___strokeColor_6; }
	inline Nullable_1_tB988CB5DAB19DC0D2A86D474B7B4B8A87AE764E9 * get_address_of_strokeColor_6() { return &___strokeColor_6; }
	inline void set_strokeColor_6(Nullable_1_tB988CB5DAB19DC0D2A86D474B7B4B8A87AE764E9  value)
	{
		___strokeColor_6 = value;
	}

	inline static int32_t get_offset_of_strokeOpacity_7() { return static_cast<int32_t>(offsetof(GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D, ___strokeOpacity_7)); }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  get_strokeOpacity_7() const { return ___strokeOpacity_7; }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 * get_address_of_strokeOpacity_7() { return &___strokeOpacity_7; }
	inline void set_strokeOpacity_7(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  value)
	{
		___strokeOpacity_7 = value;
	}

	inline static int32_t get_offset_of_strokeMiterLimit_8() { return static_cast<int32_t>(offsetof(GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D, ___strokeMiterLimit_8)); }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  get_strokeMiterLimit_8() const { return ___strokeMiterLimit_8; }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 * get_address_of_strokeMiterLimit_8() { return &___strokeMiterLimit_8; }
	inline void set_strokeMiterLimit_8(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  value)
	{
		___strokeMiterLimit_8 = value;
	}

	inline static int32_t get_offset_of_fillRule_9() { return static_cast<int32_t>(offsetof(GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D, ___fillRule_9)); }
	inline Nullable_1_t78C1DB17B9A75FEEB42DE7DF9A20B105244755FF  get_fillRule_9() const { return ___fillRule_9; }
	inline Nullable_1_t78C1DB17B9A75FEEB42DE7DF9A20B105244755FF * get_address_of_fillRule_9() { return &___fillRule_9; }
	inline void set_fillRule_9(Nullable_1_t78C1DB17B9A75FEEB42DE7DF9A20B105244755FF  value)
	{
		___fillRule_9 = value;
	}

	inline static int32_t get_offset_of_clipPath_10() { return static_cast<int32_t>(offsetof(GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D, ___clipPath_10)); }
	inline String_t* get_clipPath_10() const { return ___clipPath_10; }
	inline String_t** get_address_of_clipPath_10() { return &___clipPath_10; }
	inline void set_clipPath_10(String_t* value)
	{
		___clipPath_10 = value;
		Il2CppCodeGenWriteBarrier((&___clipPath_10), value);
	}

	inline static int32_t get_offset_of_clipRule_11() { return static_cast<int32_t>(offsetof(GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D, ___clipRule_11)); }
	inline Nullable_1_t78C1DB17B9A75FEEB42DE7DF9A20B105244755FF  get_clipRule_11() const { return ___clipRule_11; }
	inline Nullable_1_t78C1DB17B9A75FEEB42DE7DF9A20B105244755FF * get_address_of_clipRule_11() { return &___clipRule_11; }
	inline void set_clipRule_11(Nullable_1_t78C1DB17B9A75FEEB42DE7DF9A20B105244755FF  value)
	{
		___clipRule_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHICALATTRIBUTES_T22A39FF65774E208391CDCA2A02C776F6E2DF80D_H
#ifndef POLYTREERECURSE_T1392592EADEA5FBAC613340FE5FDE2518273715A_H
#define POLYTREERECURSE_T1392592EADEA5FBAC613340FE5FDE2518273715A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.GraphicalElement/PolyTreeRecurse
struct  PolyTreeRecurse_t1392592EADEA5FBAC613340FE5FDE2518273715A  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYTREERECURSE_T1392592EADEA5FBAC613340FE5FDE2518273715A_H
#ifndef MAKEPATHPOINTDELEGATE_T9450E94A77BBF3351AE2573993C7FB4A1F10ECD0_H
#define MAKEPATHPOINTDELEGATE_T9450E94A77BBF3351AE2573993C7FB4A1F10ECD0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.PathElement/MakePathPointDelegate
struct  MakePathPointDelegate_t9450E94A77BBF3351AE2573993C7FB4A1F10ECD0  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAKEPATHPOINTDELEGATE_T9450E94A77BBF3351AE2573993C7FB4A1F10ECD0_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef GETRAYINTERSECTIONALLCALLBACK_T68C2581CCF05E868297EBD3F3361274954845095_H
#define GETRAYINTERSECTIONALLCALLBACK_T68C2581CCF05E868297EBD3F3361274954845095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct  GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLCALLBACK_T68C2581CCF05E868297EBD3F3361274954845095_H
#ifndef GETRAYINTERSECTIONALLNONALLOCCALLBACK_TAD7508D45DB6679B6394983579AD18D967CC2AD4_H
#define GETRAYINTERSECTIONALLNONALLOCCALLBACK_TAD7508D45DB6679B6394983579AD18D967CC2AD4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct  GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLNONALLOCCALLBACK_TAD7508D45DB6679B6394983579AD18D967CC2AD4_H
#ifndef GETRAYCASTNONALLOCCALLBACK_TC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D_H
#define GETRAYCASTNONALLOCCALLBACK_TC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct  GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYCASTNONALLOCCALLBACK_TC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D_H
#ifndef RAYCAST2DCALLBACK_TE99ABF9ABC3A380677949E8C05A3E477889B82BE_H
#define RAYCAST2DCALLBACK_TE99ABF9ABC3A380677949E8C05A3E477889B82BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct  Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST2DCALLBACK_TE99ABF9ABC3A380677949E8C05A3E477889B82BE_H
#ifndef RAYCAST3DCALLBACK_T83483916473C9710AEDB316A65CBE62C58935C5F_H
#define RAYCAST3DCALLBACK_T83483916473C9710AEDB316A65CBE62C58935C5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct  Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST3DCALLBACK_T83483916473C9710AEDB316A65CBE62C58935C5F_H
#ifndef RAYCASTALLCALLBACK_T751407A44270E02FAA43D0846A58EE6A8C4AE1CE_H
#define RAYCASTALLCALLBACK_T751407A44270E02FAA43D0846A58EE6A8C4AE1CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct  RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTALLCALLBACK_T751407A44270E02FAA43D0846A58EE6A8C4AE1CE_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef BACKGROUNDSPRITE_TDF2701B45FEB226EEA8435126B95867AD931DF9D_H
#define BACKGROUNDSPRITE_TDF2701B45FEB226EEA8435126B95867AD931DF9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackgroundSprite
struct  BackgroundSprite_tDF2701B45FEB226EEA8435126B95867AD931DF9D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKGROUNDSPRITE_TDF2701B45FEB226EEA8435126B95867AD931DF9D_H
#ifndef BETWEEN_T8AF05A448BE9EEF66260593E8CC15F84E6F3DA37_H
#define BETWEEN_T8AF05A448BE9EEF66260593E8CC15F84E6F3DA37_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Between
struct  Between_t8AF05A448BE9EEF66260593E8CC15F84E6F3DA37  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BETWEEN_T8AF05A448BE9EEF66260593E8CC15F84E6F3DA37_H
#ifndef CAMERAANDCARMOVEMENT_T75FA6CE2E014F8A189589F575D6EB7C038D2739B_H
#define CAMERAANDCARMOVEMENT_T75FA6CE2E014F8A189589F575D6EB7C038D2739B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraAndCarMovement
struct  CameraAndCarMovement_t75FA6CE2E014F8A189589F575D6EB7C038D2739B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform CameraAndCarMovement::cameraTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___cameraTransform_4;
	// UnityEngine.Transform CameraAndCarMovement::carTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___carTransform_5;
	// UnityEngine.WheelJoint2D CameraAndCarMovement::frontWheel
	WheelJoint2D_tB59E7B24356F45BDE10574AAE8DBC9CBD773E6AE * ___frontWheel_6;
	// UnityEngine.WheelJoint2D CameraAndCarMovement::backWheel
	WheelJoint2D_tB59E7B24356F45BDE10574AAE8DBC9CBD773E6AE * ___backWheel_7;
	// UnityEngine.Camera CameraAndCarMovement::carCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___carCamera_8;
	// System.Single CameraAndCarMovement::maxZoom
	float ___maxZoom_9;
	// System.Single CameraAndCarMovement::minZoom
	float ___minZoom_10;
	// System.Single CameraAndCarMovement::zoomSpeed
	float ___zoomSpeed_11;
	// System.Single CameraAndCarMovement::minZoomCameraY
	float ___minZoomCameraY_12;
	// System.Single CameraAndCarMovement::maxZoomCameraY
	float ___maxZoomCameraY_13;

public:
	inline static int32_t get_offset_of_cameraTransform_4() { return static_cast<int32_t>(offsetof(CameraAndCarMovement_t75FA6CE2E014F8A189589F575D6EB7C038D2739B, ___cameraTransform_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_cameraTransform_4() const { return ___cameraTransform_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_cameraTransform_4() { return &___cameraTransform_4; }
	inline void set_cameraTransform_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___cameraTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_4), value);
	}

	inline static int32_t get_offset_of_carTransform_5() { return static_cast<int32_t>(offsetof(CameraAndCarMovement_t75FA6CE2E014F8A189589F575D6EB7C038D2739B, ___carTransform_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_carTransform_5() const { return ___carTransform_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_carTransform_5() { return &___carTransform_5; }
	inline void set_carTransform_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___carTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___carTransform_5), value);
	}

	inline static int32_t get_offset_of_frontWheel_6() { return static_cast<int32_t>(offsetof(CameraAndCarMovement_t75FA6CE2E014F8A189589F575D6EB7C038D2739B, ___frontWheel_6)); }
	inline WheelJoint2D_tB59E7B24356F45BDE10574AAE8DBC9CBD773E6AE * get_frontWheel_6() const { return ___frontWheel_6; }
	inline WheelJoint2D_tB59E7B24356F45BDE10574AAE8DBC9CBD773E6AE ** get_address_of_frontWheel_6() { return &___frontWheel_6; }
	inline void set_frontWheel_6(WheelJoint2D_tB59E7B24356F45BDE10574AAE8DBC9CBD773E6AE * value)
	{
		___frontWheel_6 = value;
		Il2CppCodeGenWriteBarrier((&___frontWheel_6), value);
	}

	inline static int32_t get_offset_of_backWheel_7() { return static_cast<int32_t>(offsetof(CameraAndCarMovement_t75FA6CE2E014F8A189589F575D6EB7C038D2739B, ___backWheel_7)); }
	inline WheelJoint2D_tB59E7B24356F45BDE10574AAE8DBC9CBD773E6AE * get_backWheel_7() const { return ___backWheel_7; }
	inline WheelJoint2D_tB59E7B24356F45BDE10574AAE8DBC9CBD773E6AE ** get_address_of_backWheel_7() { return &___backWheel_7; }
	inline void set_backWheel_7(WheelJoint2D_tB59E7B24356F45BDE10574AAE8DBC9CBD773E6AE * value)
	{
		___backWheel_7 = value;
		Il2CppCodeGenWriteBarrier((&___backWheel_7), value);
	}

	inline static int32_t get_offset_of_carCamera_8() { return static_cast<int32_t>(offsetof(CameraAndCarMovement_t75FA6CE2E014F8A189589F575D6EB7C038D2739B, ___carCamera_8)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_carCamera_8() const { return ___carCamera_8; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_carCamera_8() { return &___carCamera_8; }
	inline void set_carCamera_8(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___carCamera_8 = value;
		Il2CppCodeGenWriteBarrier((&___carCamera_8), value);
	}

	inline static int32_t get_offset_of_maxZoom_9() { return static_cast<int32_t>(offsetof(CameraAndCarMovement_t75FA6CE2E014F8A189589F575D6EB7C038D2739B, ___maxZoom_9)); }
	inline float get_maxZoom_9() const { return ___maxZoom_9; }
	inline float* get_address_of_maxZoom_9() { return &___maxZoom_9; }
	inline void set_maxZoom_9(float value)
	{
		___maxZoom_9 = value;
	}

	inline static int32_t get_offset_of_minZoom_10() { return static_cast<int32_t>(offsetof(CameraAndCarMovement_t75FA6CE2E014F8A189589F575D6EB7C038D2739B, ___minZoom_10)); }
	inline float get_minZoom_10() const { return ___minZoom_10; }
	inline float* get_address_of_minZoom_10() { return &___minZoom_10; }
	inline void set_minZoom_10(float value)
	{
		___minZoom_10 = value;
	}

	inline static int32_t get_offset_of_zoomSpeed_11() { return static_cast<int32_t>(offsetof(CameraAndCarMovement_t75FA6CE2E014F8A189589F575D6EB7C038D2739B, ___zoomSpeed_11)); }
	inline float get_zoomSpeed_11() const { return ___zoomSpeed_11; }
	inline float* get_address_of_zoomSpeed_11() { return &___zoomSpeed_11; }
	inline void set_zoomSpeed_11(float value)
	{
		___zoomSpeed_11 = value;
	}

	inline static int32_t get_offset_of_minZoomCameraY_12() { return static_cast<int32_t>(offsetof(CameraAndCarMovement_t75FA6CE2E014F8A189589F575D6EB7C038D2739B, ___minZoomCameraY_12)); }
	inline float get_minZoomCameraY_12() const { return ___minZoomCameraY_12; }
	inline float* get_address_of_minZoomCameraY_12() { return &___minZoomCameraY_12; }
	inline void set_minZoomCameraY_12(float value)
	{
		___minZoomCameraY_12 = value;
	}

	inline static int32_t get_offset_of_maxZoomCameraY_13() { return static_cast<int32_t>(offsetof(CameraAndCarMovement_t75FA6CE2E014F8A189589F575D6EB7C038D2739B, ___maxZoomCameraY_13)); }
	inline float get_maxZoomCameraY_13() const { return ___maxZoomCameraY_13; }
	inline float* get_address_of_maxZoomCameraY_13() { return &___maxZoomCameraY_13; }
	inline void set_maxZoomCameraY_13(float value)
	{
		___maxZoomCameraY_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAANDCARMOVEMENT_T75FA6CE2E014F8A189589F575D6EB7C038D2739B_H
#ifndef COLORFUL_TD51FF6AEC7B62A96900FAD18EC6CD8D78882617A_H
#define COLORFUL_TD51FF6AEC7B62A96900FAD18EC6CD8D78882617A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful
struct  Colorful_tD51FF6AEC7B62A96900FAD18EC6CD8D78882617A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Colorful::m_HueModifyMin
	float ___m_HueModifyMin_4;
	// System.Single Colorful::m_HueModifyMax
	float ___m_HueModifyMax_5;
	// System.Single Colorful::m_HueOffset
	float ___m_HueOffset_6;
	// System.Single Colorful::m_SaturationOffset
	float ___m_SaturationOffset_7;
	// System.Single Colorful::m_ValueOffset
	float ___m_ValueOffset_8;
	// UnityEngine.Renderer[] Colorful::m_Rdrs
	RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* ___m_Rdrs_9;

public:
	inline static int32_t get_offset_of_m_HueModifyMin_4() { return static_cast<int32_t>(offsetof(Colorful_tD51FF6AEC7B62A96900FAD18EC6CD8D78882617A, ___m_HueModifyMin_4)); }
	inline float get_m_HueModifyMin_4() const { return ___m_HueModifyMin_4; }
	inline float* get_address_of_m_HueModifyMin_4() { return &___m_HueModifyMin_4; }
	inline void set_m_HueModifyMin_4(float value)
	{
		___m_HueModifyMin_4 = value;
	}

	inline static int32_t get_offset_of_m_HueModifyMax_5() { return static_cast<int32_t>(offsetof(Colorful_tD51FF6AEC7B62A96900FAD18EC6CD8D78882617A, ___m_HueModifyMax_5)); }
	inline float get_m_HueModifyMax_5() const { return ___m_HueModifyMax_5; }
	inline float* get_address_of_m_HueModifyMax_5() { return &___m_HueModifyMax_5; }
	inline void set_m_HueModifyMax_5(float value)
	{
		___m_HueModifyMax_5 = value;
	}

	inline static int32_t get_offset_of_m_HueOffset_6() { return static_cast<int32_t>(offsetof(Colorful_tD51FF6AEC7B62A96900FAD18EC6CD8D78882617A, ___m_HueOffset_6)); }
	inline float get_m_HueOffset_6() const { return ___m_HueOffset_6; }
	inline float* get_address_of_m_HueOffset_6() { return &___m_HueOffset_6; }
	inline void set_m_HueOffset_6(float value)
	{
		___m_HueOffset_6 = value;
	}

	inline static int32_t get_offset_of_m_SaturationOffset_7() { return static_cast<int32_t>(offsetof(Colorful_tD51FF6AEC7B62A96900FAD18EC6CD8D78882617A, ___m_SaturationOffset_7)); }
	inline float get_m_SaturationOffset_7() const { return ___m_SaturationOffset_7; }
	inline float* get_address_of_m_SaturationOffset_7() { return &___m_SaturationOffset_7; }
	inline void set_m_SaturationOffset_7(float value)
	{
		___m_SaturationOffset_7 = value;
	}

	inline static int32_t get_offset_of_m_ValueOffset_8() { return static_cast<int32_t>(offsetof(Colorful_tD51FF6AEC7B62A96900FAD18EC6CD8D78882617A, ___m_ValueOffset_8)); }
	inline float get_m_ValueOffset_8() const { return ___m_ValueOffset_8; }
	inline float* get_address_of_m_ValueOffset_8() { return &___m_ValueOffset_8; }
	inline void set_m_ValueOffset_8(float value)
	{
		___m_ValueOffset_8 = value;
	}

	inline static int32_t get_offset_of_m_Rdrs_9() { return static_cast<int32_t>(offsetof(Colorful_tD51FF6AEC7B62A96900FAD18EC6CD8D78882617A, ___m_Rdrs_9)); }
	inline RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* get_m_Rdrs_9() const { return ___m_Rdrs_9; }
	inline RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF** get_address_of_m_Rdrs_9() { return &___m_Rdrs_9; }
	inline void set_m_Rdrs_9(RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* value)
	{
		___m_Rdrs_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rdrs_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORFUL_TD51FF6AEC7B62A96900FAD18EC6CD8D78882617A_H
#ifndef ELECTRICITY_T1F7DFD13FABFEFC103A769012CDF55C743549E4C_H
#define ELECTRICITY_T1F7DFD13FABFEFC103A769012CDF55C743549E4C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Electricity
struct  Electricity_t1F7DFD13FABFEFC103A769012CDF55C743549E4C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Electricity/ETransparentMode Electricity::m_TransMode
	int32_t ___m_TransMode_4;
	// UnityEngine.Color Electricity::m_Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_Color_5;
	// Noise3D Electricity::m_Noise
	Noise3D_t288ADB7A6ECA2CA06F240738A7A7FDF20E7D5B20 * ___m_Noise_6;
	// UnityEngine.Renderer Electricity::m_Rd
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___m_Rd_7;

public:
	inline static int32_t get_offset_of_m_TransMode_4() { return static_cast<int32_t>(offsetof(Electricity_t1F7DFD13FABFEFC103A769012CDF55C743549E4C, ___m_TransMode_4)); }
	inline int32_t get_m_TransMode_4() const { return ___m_TransMode_4; }
	inline int32_t* get_address_of_m_TransMode_4() { return &___m_TransMode_4; }
	inline void set_m_TransMode_4(int32_t value)
	{
		___m_TransMode_4 = value;
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Electricity_t1F7DFD13FABFEFC103A769012CDF55C743549E4C, ___m_Color_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_Noise_6() { return static_cast<int32_t>(offsetof(Electricity_t1F7DFD13FABFEFC103A769012CDF55C743549E4C, ___m_Noise_6)); }
	inline Noise3D_t288ADB7A6ECA2CA06F240738A7A7FDF20E7D5B20 * get_m_Noise_6() const { return ___m_Noise_6; }
	inline Noise3D_t288ADB7A6ECA2CA06F240738A7A7FDF20E7D5B20 ** get_address_of_m_Noise_6() { return &___m_Noise_6; }
	inline void set_m_Noise_6(Noise3D_t288ADB7A6ECA2CA06F240738A7A7FDF20E7D5B20 * value)
	{
		___m_Noise_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Noise_6), value);
	}

	inline static int32_t get_offset_of_m_Rd_7() { return static_cast<int32_t>(offsetof(Electricity_t1F7DFD13FABFEFC103A769012CDF55C743549E4C, ___m_Rd_7)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_m_Rd_7() const { return ___m_Rd_7; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_m_Rd_7() { return &___m_Rd_7; }
	inline void set_m_Rd_7(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___m_Rd_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rd_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELECTRICITY_T1F7DFD13FABFEFC103A769012CDF55C743549E4C_H
#ifndef FADEEFFECT_TED731F6EC346B384F24D34AD119A2ADB69AFDBE6_H
#define FADEEFFECT_TED731F6EC346B384F24D34AD119A2ADB69AFDBE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadeEffect
struct  FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.AnimationCurve FadeEffect::fadeCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___fadeCurve_4;
	// UnityEngine.Events.UnityAction FadeEffect::onEndCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onEndCallback_5;
	// System.Single FadeEffect::startAlpha
	float ___startAlpha_6;
	// System.Single FadeEffect::_alpha
	float ____alpha_7;
	// UnityEngine.Texture2D FadeEffect::_texture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ____texture_8;
	// System.Boolean FadeEffect::_done
	bool ____done_9;
	// System.Single FadeEffect::_time
	float ____time_10;

public:
	inline static int32_t get_offset_of_fadeCurve_4() { return static_cast<int32_t>(offsetof(FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6, ___fadeCurve_4)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_fadeCurve_4() const { return ___fadeCurve_4; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_fadeCurve_4() { return &___fadeCurve_4; }
	inline void set_fadeCurve_4(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___fadeCurve_4 = value;
		Il2CppCodeGenWriteBarrier((&___fadeCurve_4), value);
	}

	inline static int32_t get_offset_of_onEndCallback_5() { return static_cast<int32_t>(offsetof(FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6, ___onEndCallback_5)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onEndCallback_5() const { return ___onEndCallback_5; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onEndCallback_5() { return &___onEndCallback_5; }
	inline void set_onEndCallback_5(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onEndCallback_5 = value;
		Il2CppCodeGenWriteBarrier((&___onEndCallback_5), value);
	}

	inline static int32_t get_offset_of_startAlpha_6() { return static_cast<int32_t>(offsetof(FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6, ___startAlpha_6)); }
	inline float get_startAlpha_6() const { return ___startAlpha_6; }
	inline float* get_address_of_startAlpha_6() { return &___startAlpha_6; }
	inline void set_startAlpha_6(float value)
	{
		___startAlpha_6 = value;
	}

	inline static int32_t get_offset_of__alpha_7() { return static_cast<int32_t>(offsetof(FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6, ____alpha_7)); }
	inline float get__alpha_7() const { return ____alpha_7; }
	inline float* get_address_of__alpha_7() { return &____alpha_7; }
	inline void set__alpha_7(float value)
	{
		____alpha_7 = value;
	}

	inline static int32_t get_offset_of__texture_8() { return static_cast<int32_t>(offsetof(FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6, ____texture_8)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get__texture_8() const { return ____texture_8; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of__texture_8() { return &____texture_8; }
	inline void set__texture_8(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		____texture_8 = value;
		Il2CppCodeGenWriteBarrier((&____texture_8), value);
	}

	inline static int32_t get_offset_of__done_9() { return static_cast<int32_t>(offsetof(FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6, ____done_9)); }
	inline bool get__done_9() const { return ____done_9; }
	inline bool* get_address_of__done_9() { return &____done_9; }
	inline void set__done_9(bool value)
	{
		____done_9 = value;
	}

	inline static int32_t get_offset_of__time_10() { return static_cast<int32_t>(offsetof(FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6, ____time_10)); }
	inline float get__time_10() const { return ____time_10; }
	inline float* get_address_of__time_10() { return &____time_10; }
	inline void set__time_10(float value)
	{
		____time_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADEEFFECT_TED731F6EC346B384F24D34AD119A2ADB69AFDBE6_H
#ifndef FILTERS_T6332699A87E0C307D1D29E72E4EFB591D66FF449_H
#define FILTERS_T6332699A87E0C307D1D29E72E4EFB591D66FF449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Filters
struct  Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Filters::m_Enable
	bool ___m_Enable_4;
	// System.Boolean Filters::m_EnableQuantize
	bool ___m_EnableQuantize_5;
	// System.Int32 Filters::m_RBits
	int32_t ___m_RBits_6;
	// System.Int32 Filters::m_GBits
	int32_t ___m_GBits_7;
	// System.Int32 Filters::m_BBits
	int32_t ___m_BBits_8;
	// System.Boolean Filters::m_EnableTvCurvature
	bool ___m_EnableTvCurvature_9;
	// System.Single Filters::m_TvCurvature
	float ___m_TvCurvature_10;
	// System.Boolean Filters::m_EnablePixelMask
	bool ___m_EnablePixelMask_11;
	// UnityEngine.Texture2D Filters::m_PixelMask
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___m_PixelMask_12;
	// UnityEngine.Vector4 Filters::m_PixelMaskParams
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___m_PixelMaskParams_13;
	// System.Boolean Filters::m_EnableRollingFlicker
	bool ___m_EnableRollingFlicker_14;
	// System.Single Filters::m_RollingFlickerAmount
	float ___m_RollingFlickerAmount_15;
	// System.Single Filters::m_RollingFlickerOffset
	float ___m_RollingFlickerOffset_16;
	// System.Single Filters::m_RollingFlickerVsynctime
	float ___m_RollingFlickerVsynctime_17;
	// System.Boolean Filters::m_EnableGameboy
	bool ___m_EnableGameboy_18;
	// System.Boolean Filters::m_EnablePixelate
	bool ___m_EnablePixelate_19;
	// System.Single Filters::m_PixelateSize
	float ___m_PixelateSize_20;
	// UnityEngine.Material Filters::m_Mat
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_Mat_21;

public:
	inline static int32_t get_offset_of_m_Enable_4() { return static_cast<int32_t>(offsetof(Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449, ___m_Enable_4)); }
	inline bool get_m_Enable_4() const { return ___m_Enable_4; }
	inline bool* get_address_of_m_Enable_4() { return &___m_Enable_4; }
	inline void set_m_Enable_4(bool value)
	{
		___m_Enable_4 = value;
	}

	inline static int32_t get_offset_of_m_EnableQuantize_5() { return static_cast<int32_t>(offsetof(Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449, ___m_EnableQuantize_5)); }
	inline bool get_m_EnableQuantize_5() const { return ___m_EnableQuantize_5; }
	inline bool* get_address_of_m_EnableQuantize_5() { return &___m_EnableQuantize_5; }
	inline void set_m_EnableQuantize_5(bool value)
	{
		___m_EnableQuantize_5 = value;
	}

	inline static int32_t get_offset_of_m_RBits_6() { return static_cast<int32_t>(offsetof(Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449, ___m_RBits_6)); }
	inline int32_t get_m_RBits_6() const { return ___m_RBits_6; }
	inline int32_t* get_address_of_m_RBits_6() { return &___m_RBits_6; }
	inline void set_m_RBits_6(int32_t value)
	{
		___m_RBits_6 = value;
	}

	inline static int32_t get_offset_of_m_GBits_7() { return static_cast<int32_t>(offsetof(Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449, ___m_GBits_7)); }
	inline int32_t get_m_GBits_7() const { return ___m_GBits_7; }
	inline int32_t* get_address_of_m_GBits_7() { return &___m_GBits_7; }
	inline void set_m_GBits_7(int32_t value)
	{
		___m_GBits_7 = value;
	}

	inline static int32_t get_offset_of_m_BBits_8() { return static_cast<int32_t>(offsetof(Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449, ___m_BBits_8)); }
	inline int32_t get_m_BBits_8() const { return ___m_BBits_8; }
	inline int32_t* get_address_of_m_BBits_8() { return &___m_BBits_8; }
	inline void set_m_BBits_8(int32_t value)
	{
		___m_BBits_8 = value;
	}

	inline static int32_t get_offset_of_m_EnableTvCurvature_9() { return static_cast<int32_t>(offsetof(Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449, ___m_EnableTvCurvature_9)); }
	inline bool get_m_EnableTvCurvature_9() const { return ___m_EnableTvCurvature_9; }
	inline bool* get_address_of_m_EnableTvCurvature_9() { return &___m_EnableTvCurvature_9; }
	inline void set_m_EnableTvCurvature_9(bool value)
	{
		___m_EnableTvCurvature_9 = value;
	}

	inline static int32_t get_offset_of_m_TvCurvature_10() { return static_cast<int32_t>(offsetof(Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449, ___m_TvCurvature_10)); }
	inline float get_m_TvCurvature_10() const { return ___m_TvCurvature_10; }
	inline float* get_address_of_m_TvCurvature_10() { return &___m_TvCurvature_10; }
	inline void set_m_TvCurvature_10(float value)
	{
		___m_TvCurvature_10 = value;
	}

	inline static int32_t get_offset_of_m_EnablePixelMask_11() { return static_cast<int32_t>(offsetof(Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449, ___m_EnablePixelMask_11)); }
	inline bool get_m_EnablePixelMask_11() const { return ___m_EnablePixelMask_11; }
	inline bool* get_address_of_m_EnablePixelMask_11() { return &___m_EnablePixelMask_11; }
	inline void set_m_EnablePixelMask_11(bool value)
	{
		___m_EnablePixelMask_11 = value;
	}

	inline static int32_t get_offset_of_m_PixelMask_12() { return static_cast<int32_t>(offsetof(Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449, ___m_PixelMask_12)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_m_PixelMask_12() const { return ___m_PixelMask_12; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_m_PixelMask_12() { return &___m_PixelMask_12; }
	inline void set_m_PixelMask_12(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___m_PixelMask_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_PixelMask_12), value);
	}

	inline static int32_t get_offset_of_m_PixelMaskParams_13() { return static_cast<int32_t>(offsetof(Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449, ___m_PixelMaskParams_13)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_m_PixelMaskParams_13() const { return ___m_PixelMaskParams_13; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_m_PixelMaskParams_13() { return &___m_PixelMaskParams_13; }
	inline void set_m_PixelMaskParams_13(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___m_PixelMaskParams_13 = value;
	}

	inline static int32_t get_offset_of_m_EnableRollingFlicker_14() { return static_cast<int32_t>(offsetof(Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449, ___m_EnableRollingFlicker_14)); }
	inline bool get_m_EnableRollingFlicker_14() const { return ___m_EnableRollingFlicker_14; }
	inline bool* get_address_of_m_EnableRollingFlicker_14() { return &___m_EnableRollingFlicker_14; }
	inline void set_m_EnableRollingFlicker_14(bool value)
	{
		___m_EnableRollingFlicker_14 = value;
	}

	inline static int32_t get_offset_of_m_RollingFlickerAmount_15() { return static_cast<int32_t>(offsetof(Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449, ___m_RollingFlickerAmount_15)); }
	inline float get_m_RollingFlickerAmount_15() const { return ___m_RollingFlickerAmount_15; }
	inline float* get_address_of_m_RollingFlickerAmount_15() { return &___m_RollingFlickerAmount_15; }
	inline void set_m_RollingFlickerAmount_15(float value)
	{
		___m_RollingFlickerAmount_15 = value;
	}

	inline static int32_t get_offset_of_m_RollingFlickerOffset_16() { return static_cast<int32_t>(offsetof(Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449, ___m_RollingFlickerOffset_16)); }
	inline float get_m_RollingFlickerOffset_16() const { return ___m_RollingFlickerOffset_16; }
	inline float* get_address_of_m_RollingFlickerOffset_16() { return &___m_RollingFlickerOffset_16; }
	inline void set_m_RollingFlickerOffset_16(float value)
	{
		___m_RollingFlickerOffset_16 = value;
	}

	inline static int32_t get_offset_of_m_RollingFlickerVsynctime_17() { return static_cast<int32_t>(offsetof(Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449, ___m_RollingFlickerVsynctime_17)); }
	inline float get_m_RollingFlickerVsynctime_17() const { return ___m_RollingFlickerVsynctime_17; }
	inline float* get_address_of_m_RollingFlickerVsynctime_17() { return &___m_RollingFlickerVsynctime_17; }
	inline void set_m_RollingFlickerVsynctime_17(float value)
	{
		___m_RollingFlickerVsynctime_17 = value;
	}

	inline static int32_t get_offset_of_m_EnableGameboy_18() { return static_cast<int32_t>(offsetof(Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449, ___m_EnableGameboy_18)); }
	inline bool get_m_EnableGameboy_18() const { return ___m_EnableGameboy_18; }
	inline bool* get_address_of_m_EnableGameboy_18() { return &___m_EnableGameboy_18; }
	inline void set_m_EnableGameboy_18(bool value)
	{
		___m_EnableGameboy_18 = value;
	}

	inline static int32_t get_offset_of_m_EnablePixelate_19() { return static_cast<int32_t>(offsetof(Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449, ___m_EnablePixelate_19)); }
	inline bool get_m_EnablePixelate_19() const { return ___m_EnablePixelate_19; }
	inline bool* get_address_of_m_EnablePixelate_19() { return &___m_EnablePixelate_19; }
	inline void set_m_EnablePixelate_19(bool value)
	{
		___m_EnablePixelate_19 = value;
	}

	inline static int32_t get_offset_of_m_PixelateSize_20() { return static_cast<int32_t>(offsetof(Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449, ___m_PixelateSize_20)); }
	inline float get_m_PixelateSize_20() const { return ___m_PixelateSize_20; }
	inline float* get_address_of_m_PixelateSize_20() { return &___m_PixelateSize_20; }
	inline void set_m_PixelateSize_20(float value)
	{
		___m_PixelateSize_20 = value;
	}

	inline static int32_t get_offset_of_m_Mat_21() { return static_cast<int32_t>(offsetof(Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449, ___m_Mat_21)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_Mat_21() const { return ___m_Mat_21; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_Mat_21() { return &___m_Mat_21; }
	inline void set_m_Mat_21(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_Mat_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_Mat_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERS_T6332699A87E0C307D1D29E72E4EFB591D66FF449_H
#ifndef FIRE_T04C1C4B27F6275E3994BBB5F9117B7CB13A5054F_H
#define FIRE_T04C1C4B27F6275E3994BBB5F9117B7CB13A5054F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Fire
struct  Fire_t04C1C4B27F6275E3994BBB5F9117B7CB13A5054F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector4 Fire::m_LayerSpeed
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___m_LayerSpeed_4;
	// UnityEngine.Vector4 Fire::m_DistortionStrength
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___m_DistortionStrength_5;

public:
	inline static int32_t get_offset_of_m_LayerSpeed_4() { return static_cast<int32_t>(offsetof(Fire_t04C1C4B27F6275E3994BBB5F9117B7CB13A5054F, ___m_LayerSpeed_4)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_m_LayerSpeed_4() const { return ___m_LayerSpeed_4; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_m_LayerSpeed_4() { return &___m_LayerSpeed_4; }
	inline void set_m_LayerSpeed_4(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___m_LayerSpeed_4 = value;
	}

	inline static int32_t get_offset_of_m_DistortionStrength_5() { return static_cast<int32_t>(offsetof(Fire_t04C1C4B27F6275E3994BBB5F9117B7CB13A5054F, ___m_DistortionStrength_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_m_DistortionStrength_5() const { return ___m_DistortionStrength_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_m_DistortionStrength_5() { return &___m_DistortionStrength_5; }
	inline void set_m_DistortionStrength_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___m_DistortionStrength_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIRE_T04C1C4B27F6275E3994BBB5F9117B7CB13A5054F_H
#ifndef GLITCH_T6594C2956BB4993FF0D4641FCC32C65A565F5908_H
#define GLITCH_T6594C2956BB4993FF0D4641FCC32C65A565F5908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Glitch
struct  Glitch_t6594C2956BB4993FF0D4641FCC32C65A565F5908  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Glitch::m_IntervalSeconds
	float ___m_IntervalSeconds_4;
	// System.Single Glitch::m_DispProbability
	float ___m_DispProbability_5;
	// System.Single Glitch::m_DispIntensity
	float ___m_DispIntensity_6;
	// System.Single Glitch::m_ColorProbability
	float ___m_ColorProbability_7;
	// System.Single Glitch::m_ColorIntensity
	float ___m_ColorIntensity_8;
	// UnityEngine.Material Glitch::m_Mat
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_Mat_9;

public:
	inline static int32_t get_offset_of_m_IntervalSeconds_4() { return static_cast<int32_t>(offsetof(Glitch_t6594C2956BB4993FF0D4641FCC32C65A565F5908, ___m_IntervalSeconds_4)); }
	inline float get_m_IntervalSeconds_4() const { return ___m_IntervalSeconds_4; }
	inline float* get_address_of_m_IntervalSeconds_4() { return &___m_IntervalSeconds_4; }
	inline void set_m_IntervalSeconds_4(float value)
	{
		___m_IntervalSeconds_4 = value;
	}

	inline static int32_t get_offset_of_m_DispProbability_5() { return static_cast<int32_t>(offsetof(Glitch_t6594C2956BB4993FF0D4641FCC32C65A565F5908, ___m_DispProbability_5)); }
	inline float get_m_DispProbability_5() const { return ___m_DispProbability_5; }
	inline float* get_address_of_m_DispProbability_5() { return &___m_DispProbability_5; }
	inline void set_m_DispProbability_5(float value)
	{
		___m_DispProbability_5 = value;
	}

	inline static int32_t get_offset_of_m_DispIntensity_6() { return static_cast<int32_t>(offsetof(Glitch_t6594C2956BB4993FF0D4641FCC32C65A565F5908, ___m_DispIntensity_6)); }
	inline float get_m_DispIntensity_6() const { return ___m_DispIntensity_6; }
	inline float* get_address_of_m_DispIntensity_6() { return &___m_DispIntensity_6; }
	inline void set_m_DispIntensity_6(float value)
	{
		___m_DispIntensity_6 = value;
	}

	inline static int32_t get_offset_of_m_ColorProbability_7() { return static_cast<int32_t>(offsetof(Glitch_t6594C2956BB4993FF0D4641FCC32C65A565F5908, ___m_ColorProbability_7)); }
	inline float get_m_ColorProbability_7() const { return ___m_ColorProbability_7; }
	inline float* get_address_of_m_ColorProbability_7() { return &___m_ColorProbability_7; }
	inline void set_m_ColorProbability_7(float value)
	{
		___m_ColorProbability_7 = value;
	}

	inline static int32_t get_offset_of_m_ColorIntensity_8() { return static_cast<int32_t>(offsetof(Glitch_t6594C2956BB4993FF0D4641FCC32C65A565F5908, ___m_ColorIntensity_8)); }
	inline float get_m_ColorIntensity_8() const { return ___m_ColorIntensity_8; }
	inline float* get_address_of_m_ColorIntensity_8() { return &___m_ColorIntensity_8; }
	inline void set_m_ColorIntensity_8(float value)
	{
		___m_ColorIntensity_8 = value;
	}

	inline static int32_t get_offset_of_m_Mat_9() { return static_cast<int32_t>(offsetof(Glitch_t6594C2956BB4993FF0D4641FCC32C65A565F5908, ___m_Mat_9)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_Mat_9() const { return ___m_Mat_9; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_Mat_9() { return &___m_Mat_9; }
	inline void set_m_Mat_9(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_Mat_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Mat_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLITCH_T6594C2956BB4993FF0D4641FCC32C65A565F5908_H
#ifndef HERO_T39258A796E56A51462429C62F9A05CB012E66B5D_H
#define HERO_T39258A796E56A51462429C62F9A05CB012E66B5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Hero
struct  Hero_t39258A796E56A51462429C62F9A05CB012E66B5D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Hero::walkVelocity
	float ___walkVelocity_4;
	// System.Single Hero::runVelocity
	float ___runVelocity_5;
	// StateMachine Hero::stateMachine
	StateMachine_tA4A39BBD7B24E4B1C7B690D3C6BE73E7023EFD01 * ___stateMachine_6;
	// UnityEngine.Rigidbody2D Hero::body
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___body_7;
	// Trigger Hero::groundTrigger
	Trigger_tB7343D9B8651521A7BC56AFD7F1BD6B05F65B7F3 * ___groundTrigger_8;

public:
	inline static int32_t get_offset_of_walkVelocity_4() { return static_cast<int32_t>(offsetof(Hero_t39258A796E56A51462429C62F9A05CB012E66B5D, ___walkVelocity_4)); }
	inline float get_walkVelocity_4() const { return ___walkVelocity_4; }
	inline float* get_address_of_walkVelocity_4() { return &___walkVelocity_4; }
	inline void set_walkVelocity_4(float value)
	{
		___walkVelocity_4 = value;
	}

	inline static int32_t get_offset_of_runVelocity_5() { return static_cast<int32_t>(offsetof(Hero_t39258A796E56A51462429C62F9A05CB012E66B5D, ___runVelocity_5)); }
	inline float get_runVelocity_5() const { return ___runVelocity_5; }
	inline float* get_address_of_runVelocity_5() { return &___runVelocity_5; }
	inline void set_runVelocity_5(float value)
	{
		___runVelocity_5 = value;
	}

	inline static int32_t get_offset_of_stateMachine_6() { return static_cast<int32_t>(offsetof(Hero_t39258A796E56A51462429C62F9A05CB012E66B5D, ___stateMachine_6)); }
	inline StateMachine_tA4A39BBD7B24E4B1C7B690D3C6BE73E7023EFD01 * get_stateMachine_6() const { return ___stateMachine_6; }
	inline StateMachine_tA4A39BBD7B24E4B1C7B690D3C6BE73E7023EFD01 ** get_address_of_stateMachine_6() { return &___stateMachine_6; }
	inline void set_stateMachine_6(StateMachine_tA4A39BBD7B24E4B1C7B690D3C6BE73E7023EFD01 * value)
	{
		___stateMachine_6 = value;
		Il2CppCodeGenWriteBarrier((&___stateMachine_6), value);
	}

	inline static int32_t get_offset_of_body_7() { return static_cast<int32_t>(offsetof(Hero_t39258A796E56A51462429C62F9A05CB012E66B5D, ___body_7)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_body_7() const { return ___body_7; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_body_7() { return &___body_7; }
	inline void set_body_7(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___body_7 = value;
		Il2CppCodeGenWriteBarrier((&___body_7), value);
	}

	inline static int32_t get_offset_of_groundTrigger_8() { return static_cast<int32_t>(offsetof(Hero_t39258A796E56A51462429C62F9A05CB012E66B5D, ___groundTrigger_8)); }
	inline Trigger_tB7343D9B8651521A7BC56AFD7F1BD6B05F65B7F3 * get_groundTrigger_8() const { return ___groundTrigger_8; }
	inline Trigger_tB7343D9B8651521A7BC56AFD7F1BD6B05F65B7F3 ** get_address_of_groundTrigger_8() { return &___groundTrigger_8; }
	inline void set_groundTrigger_8(Trigger_tB7343D9B8651521A7BC56AFD7F1BD6B05F65B7F3 * value)
	{
		___groundTrigger_8 = value;
		Il2CppCodeGenWriteBarrier((&___groundTrigger_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HERO_T39258A796E56A51462429C62F9A05CB012E66B5D_H
#ifndef HEROSTATELANDED_T684EA939292822247AC6C7565F6D115BB396183A_H
#define HEROSTATELANDED_T684EA939292822247AC6C7565F6D115BB396183A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroStateLanded
struct  HeroStateLanded_t684EA939292822247AC6C7565F6D115BB396183A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROSTATELANDED_T684EA939292822247AC6C7565F6D115BB396183A_H
#ifndef LEVELLOADER_T528DC9F40D2C5040043E386207C89B1400AE0343_H
#define LEVELLOADER_T528DC9F40D2C5040043E386207C89B1400AE0343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelLoader
struct  LevelLoader_t528DC9F40D2C5040043E386207C89B1400AE0343  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// FadeEffect LevelLoader::faderIn
	FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6 * ___faderIn_4;

public:
	inline static int32_t get_offset_of_faderIn_4() { return static_cast<int32_t>(offsetof(LevelLoader_t528DC9F40D2C5040043E386207C89B1400AE0343, ___faderIn_4)); }
	inline FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6 * get_faderIn_4() const { return ___faderIn_4; }
	inline FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6 ** get_address_of_faderIn_4() { return &___faderIn_4; }
	inline void set_faderIn_4(FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6 * value)
	{
		___faderIn_4 = value;
		Il2CppCodeGenWriteBarrier((&___faderIn_4), value);
	}
};

struct LevelLoader_t528DC9F40D2C5040043E386207C89B1400AE0343_StaticFields
{
public:
	// UnityEngine.Events.UnityAction LevelLoader::<>f__am$cache0
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___U3CU3Ef__amU24cache0_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(LevelLoader_t528DC9F40D2C5040043E386207C89B1400AE0343_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELLOADER_T528DC9F40D2C5040043E386207C89B1400AE0343_H
#ifndef PULSE_TD3C350CD2F7D32DD3CD582813B2571DA6C0F387F_H
#define PULSE_TD3C350CD2F7D32DD3CD582813B2571DA6C0F387F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pulse
struct  Pulse_tD3C350CD2F7D32DD3CD582813B2571DA6C0F387F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Pulse::scaleFrom
	float ___scaleFrom_4;
	// System.Single Pulse::scaleTo
	float ___scaleTo_5;
	// System.Single Pulse::velocityExpand
	float ___velocityExpand_6;
	// System.Single Pulse::velocityBack
	float ___velocityBack_7;
	// System.Boolean Pulse::isExpanding
	bool ___isExpanding_8;

public:
	inline static int32_t get_offset_of_scaleFrom_4() { return static_cast<int32_t>(offsetof(Pulse_tD3C350CD2F7D32DD3CD582813B2571DA6C0F387F, ___scaleFrom_4)); }
	inline float get_scaleFrom_4() const { return ___scaleFrom_4; }
	inline float* get_address_of_scaleFrom_4() { return &___scaleFrom_4; }
	inline void set_scaleFrom_4(float value)
	{
		___scaleFrom_4 = value;
	}

	inline static int32_t get_offset_of_scaleTo_5() { return static_cast<int32_t>(offsetof(Pulse_tD3C350CD2F7D32DD3CD582813B2571DA6C0F387F, ___scaleTo_5)); }
	inline float get_scaleTo_5() const { return ___scaleTo_5; }
	inline float* get_address_of_scaleTo_5() { return &___scaleTo_5; }
	inline void set_scaleTo_5(float value)
	{
		___scaleTo_5 = value;
	}

	inline static int32_t get_offset_of_velocityExpand_6() { return static_cast<int32_t>(offsetof(Pulse_tD3C350CD2F7D32DD3CD582813B2571DA6C0F387F, ___velocityExpand_6)); }
	inline float get_velocityExpand_6() const { return ___velocityExpand_6; }
	inline float* get_address_of_velocityExpand_6() { return &___velocityExpand_6; }
	inline void set_velocityExpand_6(float value)
	{
		___velocityExpand_6 = value;
	}

	inline static int32_t get_offset_of_velocityBack_7() { return static_cast<int32_t>(offsetof(Pulse_tD3C350CD2F7D32DD3CD582813B2571DA6C0F387F, ___velocityBack_7)); }
	inline float get_velocityBack_7() const { return ___velocityBack_7; }
	inline float* get_address_of_velocityBack_7() { return &___velocityBack_7; }
	inline void set_velocityBack_7(float value)
	{
		___velocityBack_7 = value;
	}

	inline static int32_t get_offset_of_isExpanding_8() { return static_cast<int32_t>(offsetof(Pulse_tD3C350CD2F7D32DD3CD582813B2571DA6C0F387F, ___isExpanding_8)); }
	inline bool get_isExpanding_8() const { return ___isExpanding_8; }
	inline bool* get_address_of_isExpanding_8() { return &___isExpanding_8; }
	inline void set_isExpanding_8(bool value)
	{
		___isExpanding_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PULSE_TD3C350CD2F7D32DD3CD582813B2571DA6C0F387F_H
#ifndef RANDOMSCALE_T6E4823A4B88513C67AC1568E5B5E746545871150_H
#define RANDOMSCALE_T6E4823A4B88513C67AC1568E5B5E746545871150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RandomScale
struct  RandomScale_t6E4823A4B88513C67AC1568E5B5E746545871150  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single RandomScale::invokeRate
	float ___invokeRate_4;
	// System.Single RandomScale::maxDtScaleX
	float ___maxDtScaleX_5;
	// System.Single RandomScale::maxDtScaleY
	float ___maxDtScaleY_6;
	// UnityEngine.Vector3 RandomScale::startScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___startScale_7;

public:
	inline static int32_t get_offset_of_invokeRate_4() { return static_cast<int32_t>(offsetof(RandomScale_t6E4823A4B88513C67AC1568E5B5E746545871150, ___invokeRate_4)); }
	inline float get_invokeRate_4() const { return ___invokeRate_4; }
	inline float* get_address_of_invokeRate_4() { return &___invokeRate_4; }
	inline void set_invokeRate_4(float value)
	{
		___invokeRate_4 = value;
	}

	inline static int32_t get_offset_of_maxDtScaleX_5() { return static_cast<int32_t>(offsetof(RandomScale_t6E4823A4B88513C67AC1568E5B5E746545871150, ___maxDtScaleX_5)); }
	inline float get_maxDtScaleX_5() const { return ___maxDtScaleX_5; }
	inline float* get_address_of_maxDtScaleX_5() { return &___maxDtScaleX_5; }
	inline void set_maxDtScaleX_5(float value)
	{
		___maxDtScaleX_5 = value;
	}

	inline static int32_t get_offset_of_maxDtScaleY_6() { return static_cast<int32_t>(offsetof(RandomScale_t6E4823A4B88513C67AC1568E5B5E746545871150, ___maxDtScaleY_6)); }
	inline float get_maxDtScaleY_6() const { return ___maxDtScaleY_6; }
	inline float* get_address_of_maxDtScaleY_6() { return &___maxDtScaleY_6; }
	inline void set_maxDtScaleY_6(float value)
	{
		___maxDtScaleY_6 = value;
	}

	inline static int32_t get_offset_of_startScale_7() { return static_cast<int32_t>(offsetof(RandomScale_t6E4823A4B88513C67AC1568E5B5E746545871150, ___startScale_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_startScale_7() const { return ___startScale_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_startScale_7() { return &___startScale_7; }
	inline void set_startScale_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___startScale_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMSCALE_T6E4823A4B88513C67AC1568E5B5E746545871150_H
#ifndef ROTATEOBJECT_TBED621143EDF174654659FF578290495BBF4CFA9_H
#define ROTATEOBJECT_TBED621143EDF174654659FF578290495BBF4CFA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotateObject
struct  RotateObject_tBED621143EDF174654659FF578290495BBF4CFA9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single RotateObject::rotationVelocity
	float ___rotationVelocity_4;

public:
	inline static int32_t get_offset_of_rotationVelocity_4() { return static_cast<int32_t>(offsetof(RotateObject_tBED621143EDF174654659FF578290495BBF4CFA9, ___rotationVelocity_4)); }
	inline float get_rotationVelocity_4() const { return ___rotationVelocity_4; }
	inline float* get_address_of_rotationVelocity_4() { return &___rotationVelocity_4; }
	inline void set_rotationVelocity_4(float value)
	{
		___rotationVelocity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEOBJECT_TBED621143EDF174654659FF578290495BBF4CFA9_H
#ifndef RUNTIMEIMPORT_T7CD3697F47C08BD16904ECDE7B6F6543803D19D8_H
#define RUNTIMEIMPORT_T7CD3697F47C08BD16904ECDE7B6F6543803D19D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RuntimeImport
struct  RuntimeImport_t7CD3697F47C08BD16904ECDE7B6F6543803D19D8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String RuntimeImport::svgData
	String_t* ___svgData_4;
	// UnityEngine.MeshFilter RuntimeImport::meshFilter
	MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * ___meshFilter_5;

public:
	inline static int32_t get_offset_of_svgData_4() { return static_cast<int32_t>(offsetof(RuntimeImport_t7CD3697F47C08BD16904ECDE7B6F6543803D19D8, ___svgData_4)); }
	inline String_t* get_svgData_4() const { return ___svgData_4; }
	inline String_t** get_address_of_svgData_4() { return &___svgData_4; }
	inline void set_svgData_4(String_t* value)
	{
		___svgData_4 = value;
		Il2CppCodeGenWriteBarrier((&___svgData_4), value);
	}

	inline static int32_t get_offset_of_meshFilter_5() { return static_cast<int32_t>(offsetof(RuntimeImport_t7CD3697F47C08BD16904ECDE7B6F6543803D19D8, ___meshFilter_5)); }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * get_meshFilter_5() const { return ___meshFilter_5; }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 ** get_address_of_meshFilter_5() { return &___meshFilter_5; }
	inline void set_meshFilter_5(MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * value)
	{
		___meshFilter_5 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEIMPORT_T7CD3697F47C08BD16904ECDE7B6F6543803D19D8_H
#ifndef COLLIDERHELPER_T04617AD58EF978484019FB3298FBD37B17159BEF_H
#define COLLIDERHELPER_T04617AD58EF978484019FB3298FBD37B17159BEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.ColliderHelper
struct  ColliderHelper_t04617AD58EF978484019FB3298FBD37B17159BEF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// SimplySVG.CollisionShapeData SimplySVG.ColliderHelper::collisionShapeData
	CollisionShapeData_t09D2AF9F5D9E3B0C513049F97C117CF4EB9B2C6B * ___collisionShapeData_4;
	// System.Boolean SimplySVG.ColliderHelper::autoUpdateOnAwake
	bool ___autoUpdateOnAwake_5;
	// UnityEngine.PolygonCollider2D SimplySVG.ColliderHelper::polygonCollider
	PolygonCollider2D_t360CC66FD7BAF5F4FADE54EB23E58E95D0B8CD81 * ___polygonCollider_6;

public:
	inline static int32_t get_offset_of_collisionShapeData_4() { return static_cast<int32_t>(offsetof(ColliderHelper_t04617AD58EF978484019FB3298FBD37B17159BEF, ___collisionShapeData_4)); }
	inline CollisionShapeData_t09D2AF9F5D9E3B0C513049F97C117CF4EB9B2C6B * get_collisionShapeData_4() const { return ___collisionShapeData_4; }
	inline CollisionShapeData_t09D2AF9F5D9E3B0C513049F97C117CF4EB9B2C6B ** get_address_of_collisionShapeData_4() { return &___collisionShapeData_4; }
	inline void set_collisionShapeData_4(CollisionShapeData_t09D2AF9F5D9E3B0C513049F97C117CF4EB9B2C6B * value)
	{
		___collisionShapeData_4 = value;
		Il2CppCodeGenWriteBarrier((&___collisionShapeData_4), value);
	}

	inline static int32_t get_offset_of_autoUpdateOnAwake_5() { return static_cast<int32_t>(offsetof(ColliderHelper_t04617AD58EF978484019FB3298FBD37B17159BEF, ___autoUpdateOnAwake_5)); }
	inline bool get_autoUpdateOnAwake_5() const { return ___autoUpdateOnAwake_5; }
	inline bool* get_address_of_autoUpdateOnAwake_5() { return &___autoUpdateOnAwake_5; }
	inline void set_autoUpdateOnAwake_5(bool value)
	{
		___autoUpdateOnAwake_5 = value;
	}

	inline static int32_t get_offset_of_polygonCollider_6() { return static_cast<int32_t>(offsetof(ColliderHelper_t04617AD58EF978484019FB3298FBD37B17159BEF, ___polygonCollider_6)); }
	inline PolygonCollider2D_t360CC66FD7BAF5F4FADE54EB23E58E95D0B8CD81 * get_polygonCollider_6() const { return ___polygonCollider_6; }
	inline PolygonCollider2D_t360CC66FD7BAF5F4FADE54EB23E58E95D0B8CD81 ** get_address_of_polygonCollider_6() { return &___polygonCollider_6; }
	inline void set_polygonCollider_6(PolygonCollider2D_t360CC66FD7BAF5F4FADE54EB23E58E95D0B8CD81 * value)
	{
		___polygonCollider_6 = value;
		Il2CppCodeGenWriteBarrier((&___polygonCollider_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDERHELPER_T04617AD58EF978484019FB3298FBD37B17159BEF_H
#ifndef SKY_T49010BFAA5667EB627116AB4805D37D4A721C2EB_H
#define SKY_T49010BFAA5667EB627116AB4805D37D4A721C2EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sky
struct  Sky_t49010BFAA5667EB627116AB4805D37D4A721C2EB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Sky::m_TimeOfDay
	float ___m_TimeOfDay_4;
	// System.Single Sky::m_BottomSunLevel
	float ___m_BottomSunLevel_5;
	// System.Single Sky::m_SunSize
	float ___m_SunSize_6;
	// System.Single Sky::m_CloudSpeed
	float ___m_CloudSpeed_7;
	// System.Single Sky::m_CloudDensity
	float ___m_CloudDensity_8;
	// System.Single Sky::m_CloudSharpness
	float ___m_CloudSharpness_9;
	// UnityEngine.Color Sky::m_CloudColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_CloudColor_10;
	// System.Boolean Sky::m_UseThickCloud
	bool ___m_UseThickCloud_11;
	// System.Single Sky::m_StarDensity
	float ___m_StarDensity_12;
	// UnityEngine.SpriteRenderer Sky::m_Spr
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___m_Spr_13;

public:
	inline static int32_t get_offset_of_m_TimeOfDay_4() { return static_cast<int32_t>(offsetof(Sky_t49010BFAA5667EB627116AB4805D37D4A721C2EB, ___m_TimeOfDay_4)); }
	inline float get_m_TimeOfDay_4() const { return ___m_TimeOfDay_4; }
	inline float* get_address_of_m_TimeOfDay_4() { return &___m_TimeOfDay_4; }
	inline void set_m_TimeOfDay_4(float value)
	{
		___m_TimeOfDay_4 = value;
	}

	inline static int32_t get_offset_of_m_BottomSunLevel_5() { return static_cast<int32_t>(offsetof(Sky_t49010BFAA5667EB627116AB4805D37D4A721C2EB, ___m_BottomSunLevel_5)); }
	inline float get_m_BottomSunLevel_5() const { return ___m_BottomSunLevel_5; }
	inline float* get_address_of_m_BottomSunLevel_5() { return &___m_BottomSunLevel_5; }
	inline void set_m_BottomSunLevel_5(float value)
	{
		___m_BottomSunLevel_5 = value;
	}

	inline static int32_t get_offset_of_m_SunSize_6() { return static_cast<int32_t>(offsetof(Sky_t49010BFAA5667EB627116AB4805D37D4A721C2EB, ___m_SunSize_6)); }
	inline float get_m_SunSize_6() const { return ___m_SunSize_6; }
	inline float* get_address_of_m_SunSize_6() { return &___m_SunSize_6; }
	inline void set_m_SunSize_6(float value)
	{
		___m_SunSize_6 = value;
	}

	inline static int32_t get_offset_of_m_CloudSpeed_7() { return static_cast<int32_t>(offsetof(Sky_t49010BFAA5667EB627116AB4805D37D4A721C2EB, ___m_CloudSpeed_7)); }
	inline float get_m_CloudSpeed_7() const { return ___m_CloudSpeed_7; }
	inline float* get_address_of_m_CloudSpeed_7() { return &___m_CloudSpeed_7; }
	inline void set_m_CloudSpeed_7(float value)
	{
		___m_CloudSpeed_7 = value;
	}

	inline static int32_t get_offset_of_m_CloudDensity_8() { return static_cast<int32_t>(offsetof(Sky_t49010BFAA5667EB627116AB4805D37D4A721C2EB, ___m_CloudDensity_8)); }
	inline float get_m_CloudDensity_8() const { return ___m_CloudDensity_8; }
	inline float* get_address_of_m_CloudDensity_8() { return &___m_CloudDensity_8; }
	inline void set_m_CloudDensity_8(float value)
	{
		___m_CloudDensity_8 = value;
	}

	inline static int32_t get_offset_of_m_CloudSharpness_9() { return static_cast<int32_t>(offsetof(Sky_t49010BFAA5667EB627116AB4805D37D4A721C2EB, ___m_CloudSharpness_9)); }
	inline float get_m_CloudSharpness_9() const { return ___m_CloudSharpness_9; }
	inline float* get_address_of_m_CloudSharpness_9() { return &___m_CloudSharpness_9; }
	inline void set_m_CloudSharpness_9(float value)
	{
		___m_CloudSharpness_9 = value;
	}

	inline static int32_t get_offset_of_m_CloudColor_10() { return static_cast<int32_t>(offsetof(Sky_t49010BFAA5667EB627116AB4805D37D4A721C2EB, ___m_CloudColor_10)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_CloudColor_10() const { return ___m_CloudColor_10; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_CloudColor_10() { return &___m_CloudColor_10; }
	inline void set_m_CloudColor_10(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_CloudColor_10 = value;
	}

	inline static int32_t get_offset_of_m_UseThickCloud_11() { return static_cast<int32_t>(offsetof(Sky_t49010BFAA5667EB627116AB4805D37D4A721C2EB, ___m_UseThickCloud_11)); }
	inline bool get_m_UseThickCloud_11() const { return ___m_UseThickCloud_11; }
	inline bool* get_address_of_m_UseThickCloud_11() { return &___m_UseThickCloud_11; }
	inline void set_m_UseThickCloud_11(bool value)
	{
		___m_UseThickCloud_11 = value;
	}

	inline static int32_t get_offset_of_m_StarDensity_12() { return static_cast<int32_t>(offsetof(Sky_t49010BFAA5667EB627116AB4805D37D4A721C2EB, ___m_StarDensity_12)); }
	inline float get_m_StarDensity_12() const { return ___m_StarDensity_12; }
	inline float* get_address_of_m_StarDensity_12() { return &___m_StarDensity_12; }
	inline void set_m_StarDensity_12(float value)
	{
		___m_StarDensity_12 = value;
	}

	inline static int32_t get_offset_of_m_Spr_13() { return static_cast<int32_t>(offsetof(Sky_t49010BFAA5667EB627116AB4805D37D4A721C2EB, ___m_Spr_13)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_m_Spr_13() const { return ___m_Spr_13; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_m_Spr_13() { return &___m_Spr_13; }
	inline void set_m_Spr_13(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___m_Spr_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Spr_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKY_T49010BFAA5667EB627116AB4805D37D4A721C2EB_H
#ifndef SPRITEALPHABLINK_TE51DC575219556D6B2D78290C68C3F23160E4449_H
#define SPRITEALPHABLINK_TE51DC575219556D6B2D78290C68C3F23160E4449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpriteAlphaBlink
struct  SpriteAlphaBlink_tE51DC575219556D6B2D78290C68C3F23160E4449  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single[] SpriteAlphaBlink::alphaValues
	SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* ___alphaValues_4;
	// System.Single SpriteAlphaBlink::invokeRate
	float ___invokeRate_5;
	// UnityEngine.SpriteRenderer SpriteAlphaBlink::blinkSpriteRender
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___blinkSpriteRender_6;

public:
	inline static int32_t get_offset_of_alphaValues_4() { return static_cast<int32_t>(offsetof(SpriteAlphaBlink_tE51DC575219556D6B2D78290C68C3F23160E4449, ___alphaValues_4)); }
	inline SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* get_alphaValues_4() const { return ___alphaValues_4; }
	inline SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5** get_address_of_alphaValues_4() { return &___alphaValues_4; }
	inline void set_alphaValues_4(SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* value)
	{
		___alphaValues_4 = value;
		Il2CppCodeGenWriteBarrier((&___alphaValues_4), value);
	}

	inline static int32_t get_offset_of_invokeRate_5() { return static_cast<int32_t>(offsetof(SpriteAlphaBlink_tE51DC575219556D6B2D78290C68C3F23160E4449, ___invokeRate_5)); }
	inline float get_invokeRate_5() const { return ___invokeRate_5; }
	inline float* get_address_of_invokeRate_5() { return &___invokeRate_5; }
	inline void set_invokeRate_5(float value)
	{
		___invokeRate_5 = value;
	}

	inline static int32_t get_offset_of_blinkSpriteRender_6() { return static_cast<int32_t>(offsetof(SpriteAlphaBlink_tE51DC575219556D6B2D78290C68C3F23160E4449, ___blinkSpriteRender_6)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_blinkSpriteRender_6() const { return ___blinkSpriteRender_6; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_blinkSpriteRender_6() { return &___blinkSpriteRender_6; }
	inline void set_blinkSpriteRender_6(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___blinkSpriteRender_6 = value;
		Il2CppCodeGenWriteBarrier((&___blinkSpriteRender_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEALPHABLINK_TE51DC575219556D6B2D78290C68C3F23160E4449_H
#ifndef STATE_TC5BA2DC909C73846EB01A3E0806E5D6347274619_H
#define STATE_TC5BA2DC909C73846EB01A3E0806E5D6347274619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// State
struct  State_tC5BA2DC909C73846EB01A3E0806E5D6347274619  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Events.UnityAction State::onEnterAction
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onEnterAction_4;
	// UnityEngine.Events.UnityAction State::onExitAction
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___onExitAction_5;
	// System.String State::previousState
	String_t* ___previousState_6;
	// StateMachine State::stateMachine
	StateMachine_tA4A39BBD7B24E4B1C7B690D3C6BE73E7023EFD01 * ___stateMachine_7;
	// UnityEngine.Object State::sharedData
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___sharedData_8;

public:
	inline static int32_t get_offset_of_onEnterAction_4() { return static_cast<int32_t>(offsetof(State_tC5BA2DC909C73846EB01A3E0806E5D6347274619, ___onEnterAction_4)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onEnterAction_4() const { return ___onEnterAction_4; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onEnterAction_4() { return &___onEnterAction_4; }
	inline void set_onEnterAction_4(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onEnterAction_4 = value;
		Il2CppCodeGenWriteBarrier((&___onEnterAction_4), value);
	}

	inline static int32_t get_offset_of_onExitAction_5() { return static_cast<int32_t>(offsetof(State_tC5BA2DC909C73846EB01A3E0806E5D6347274619, ___onExitAction_5)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_onExitAction_5() const { return ___onExitAction_5; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_onExitAction_5() { return &___onExitAction_5; }
	inline void set_onExitAction_5(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___onExitAction_5 = value;
		Il2CppCodeGenWriteBarrier((&___onExitAction_5), value);
	}

	inline static int32_t get_offset_of_previousState_6() { return static_cast<int32_t>(offsetof(State_tC5BA2DC909C73846EB01A3E0806E5D6347274619, ___previousState_6)); }
	inline String_t* get_previousState_6() const { return ___previousState_6; }
	inline String_t** get_address_of_previousState_6() { return &___previousState_6; }
	inline void set_previousState_6(String_t* value)
	{
		___previousState_6 = value;
		Il2CppCodeGenWriteBarrier((&___previousState_6), value);
	}

	inline static int32_t get_offset_of_stateMachine_7() { return static_cast<int32_t>(offsetof(State_tC5BA2DC909C73846EB01A3E0806E5D6347274619, ___stateMachine_7)); }
	inline StateMachine_tA4A39BBD7B24E4B1C7B690D3C6BE73E7023EFD01 * get_stateMachine_7() const { return ___stateMachine_7; }
	inline StateMachine_tA4A39BBD7B24E4B1C7B690D3C6BE73E7023EFD01 ** get_address_of_stateMachine_7() { return &___stateMachine_7; }
	inline void set_stateMachine_7(StateMachine_tA4A39BBD7B24E4B1C7B690D3C6BE73E7023EFD01 * value)
	{
		___stateMachine_7 = value;
		Il2CppCodeGenWriteBarrier((&___stateMachine_7), value);
	}

	inline static int32_t get_offset_of_sharedData_8() { return static_cast<int32_t>(offsetof(State_tC5BA2DC909C73846EB01A3E0806E5D6347274619, ___sharedData_8)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get_sharedData_8() const { return ___sharedData_8; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of_sharedData_8() { return &___sharedData_8; }
	inline void set_sharedData_8(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		___sharedData_8 = value;
		Il2CppCodeGenWriteBarrier((&___sharedData_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_TC5BA2DC909C73846EB01A3E0806E5D6347274619_H
#ifndef STATEMACHINE_TA4A39BBD7B24E4B1C7B690D3C6BE73E7023EFD01_H
#define STATEMACHINE_TA4A39BBD7B24E4B1C7B690D3C6BE73E7023EFD01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StateMachine
struct  StateMachine_tA4A39BBD7B24E4B1C7B690D3C6BE73E7023EFD01  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String StateMachine::currentStateName
	String_t* ___currentStateName_4;

public:
	inline static int32_t get_offset_of_currentStateName_4() { return static_cast<int32_t>(offsetof(StateMachine_tA4A39BBD7B24E4B1C7B690D3C6BE73E7023EFD01, ___currentStateName_4)); }
	inline String_t* get_currentStateName_4() const { return ___currentStateName_4; }
	inline String_t** get_address_of_currentStateName_4() { return &___currentStateName_4; }
	inline void set_currentStateName_4(String_t* value)
	{
		___currentStateName_4 = value;
		Il2CppCodeGenWriteBarrier((&___currentStateName_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEMACHINE_TA4A39BBD7B24E4B1C7B690D3C6BE73E7023EFD01_H
#ifndef STATICS_T95A18A72EC5A85C490E5129598C5FFCD300070DC_H
#define STATICS_T95A18A72EC5A85C490E5129598C5FFCD300070DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Statics
struct  Statics_t95A18A72EC5A85C490E5129598C5FFCD300070DC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Statics_t95A18A72EC5A85C490E5129598C5FFCD300070DC_StaticFields
{
public:
	// System.Int16 Statics::currentLevel
	int16_t ___currentLevel_4;
	// System.String Statics::sceneToSwitch
	String_t* ___sceneToSwitch_5;

public:
	inline static int32_t get_offset_of_currentLevel_4() { return static_cast<int32_t>(offsetof(Statics_t95A18A72EC5A85C490E5129598C5FFCD300070DC_StaticFields, ___currentLevel_4)); }
	inline int16_t get_currentLevel_4() const { return ___currentLevel_4; }
	inline int16_t* get_address_of_currentLevel_4() { return &___currentLevel_4; }
	inline void set_currentLevel_4(int16_t value)
	{
		___currentLevel_4 = value;
	}

	inline static int32_t get_offset_of_sceneToSwitch_5() { return static_cast<int32_t>(offsetof(Statics_t95A18A72EC5A85C490E5129598C5FFCD300070DC_StaticFields, ___sceneToSwitch_5)); }
	inline String_t* get_sceneToSwitch_5() const { return ___sceneToSwitch_5; }
	inline String_t** get_address_of_sceneToSwitch_5() { return &___sceneToSwitch_5; }
	inline void set_sceneToSwitch_5(String_t* value)
	{
		___sceneToSwitch_5 = value;
		Il2CppCodeGenWriteBarrier((&___sceneToSwitch_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATICS_T95A18A72EC5A85C490E5129598C5FFCD300070DC_H
#ifndef TIMEUTILS_T75BA8DA980BA5920FF9F9B62D23E95A15972239F_H
#define TIMEUTILS_T75BA8DA980BA5920FF9F9B62D23E95A15972239F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeUtils
struct  TimeUtils_t75BA8DA980BA5920FF9F9B62D23E95A15972239F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEUTILS_T75BA8DA980BA5920FF9F9B62D23E95A15972239F_H
#ifndef TOUCHCONTROLSMANAGER_T073F81430C963A33027AB789335B536141D833E5_H
#define TOUCHCONTROLSMANAGER_T073F81430C963A33027AB789335B536141D833E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchControlsManager
struct  TouchControlsManager_t073F81430C963A33027AB789335B536141D833E5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Events.UnityAction`1<UnityEngine.Vector2> TouchControlsManager::_leftTouchBeganCallbacks
	UnityAction_1_tC29A4BDE80E6622E5794F177E3DAA65BB4342B44 * ____leftTouchBeganCallbacks_4;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Vector2> TouchControlsManager::_leftTouchEndedCallbacks
	UnityAction_1_tC29A4BDE80E6622E5794F177E3DAA65BB4342B44 * ____leftTouchEndedCallbacks_5;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Vector2> TouchControlsManager::_rightTouchBeganCallbacks
	UnityAction_1_tC29A4BDE80E6622E5794F177E3DAA65BB4342B44 * ____rightTouchBeganCallbacks_6;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Vector2> TouchControlsManager::_rightTouchEndedCallbacks
	UnityAction_1_tC29A4BDE80E6622E5794F177E3DAA65BB4342B44 * ____rightTouchEndedCallbacks_7;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Vector2> TouchControlsManager::_touchMovedCallbacks
	UnityAction_1_tC29A4BDE80E6622E5794F177E3DAA65BB4342B44 * ____touchMovedCallbacks_8;
	// System.Boolean TouchControlsManager::isLeftTouch
	bool ___isLeftTouch_9;
	// System.Boolean TouchControlsManager::isRightTouch
	bool ___isRightTouch_10;

public:
	inline static int32_t get_offset_of__leftTouchBeganCallbacks_4() { return static_cast<int32_t>(offsetof(TouchControlsManager_t073F81430C963A33027AB789335B536141D833E5, ____leftTouchBeganCallbacks_4)); }
	inline UnityAction_1_tC29A4BDE80E6622E5794F177E3DAA65BB4342B44 * get__leftTouchBeganCallbacks_4() const { return ____leftTouchBeganCallbacks_4; }
	inline UnityAction_1_tC29A4BDE80E6622E5794F177E3DAA65BB4342B44 ** get_address_of__leftTouchBeganCallbacks_4() { return &____leftTouchBeganCallbacks_4; }
	inline void set__leftTouchBeganCallbacks_4(UnityAction_1_tC29A4BDE80E6622E5794F177E3DAA65BB4342B44 * value)
	{
		____leftTouchBeganCallbacks_4 = value;
		Il2CppCodeGenWriteBarrier((&____leftTouchBeganCallbacks_4), value);
	}

	inline static int32_t get_offset_of__leftTouchEndedCallbacks_5() { return static_cast<int32_t>(offsetof(TouchControlsManager_t073F81430C963A33027AB789335B536141D833E5, ____leftTouchEndedCallbacks_5)); }
	inline UnityAction_1_tC29A4BDE80E6622E5794F177E3DAA65BB4342B44 * get__leftTouchEndedCallbacks_5() const { return ____leftTouchEndedCallbacks_5; }
	inline UnityAction_1_tC29A4BDE80E6622E5794F177E3DAA65BB4342B44 ** get_address_of__leftTouchEndedCallbacks_5() { return &____leftTouchEndedCallbacks_5; }
	inline void set__leftTouchEndedCallbacks_5(UnityAction_1_tC29A4BDE80E6622E5794F177E3DAA65BB4342B44 * value)
	{
		____leftTouchEndedCallbacks_5 = value;
		Il2CppCodeGenWriteBarrier((&____leftTouchEndedCallbacks_5), value);
	}

	inline static int32_t get_offset_of__rightTouchBeganCallbacks_6() { return static_cast<int32_t>(offsetof(TouchControlsManager_t073F81430C963A33027AB789335B536141D833E5, ____rightTouchBeganCallbacks_6)); }
	inline UnityAction_1_tC29A4BDE80E6622E5794F177E3DAA65BB4342B44 * get__rightTouchBeganCallbacks_6() const { return ____rightTouchBeganCallbacks_6; }
	inline UnityAction_1_tC29A4BDE80E6622E5794F177E3DAA65BB4342B44 ** get_address_of__rightTouchBeganCallbacks_6() { return &____rightTouchBeganCallbacks_6; }
	inline void set__rightTouchBeganCallbacks_6(UnityAction_1_tC29A4BDE80E6622E5794F177E3DAA65BB4342B44 * value)
	{
		____rightTouchBeganCallbacks_6 = value;
		Il2CppCodeGenWriteBarrier((&____rightTouchBeganCallbacks_6), value);
	}

	inline static int32_t get_offset_of__rightTouchEndedCallbacks_7() { return static_cast<int32_t>(offsetof(TouchControlsManager_t073F81430C963A33027AB789335B536141D833E5, ____rightTouchEndedCallbacks_7)); }
	inline UnityAction_1_tC29A4BDE80E6622E5794F177E3DAA65BB4342B44 * get__rightTouchEndedCallbacks_7() const { return ____rightTouchEndedCallbacks_7; }
	inline UnityAction_1_tC29A4BDE80E6622E5794F177E3DAA65BB4342B44 ** get_address_of__rightTouchEndedCallbacks_7() { return &____rightTouchEndedCallbacks_7; }
	inline void set__rightTouchEndedCallbacks_7(UnityAction_1_tC29A4BDE80E6622E5794F177E3DAA65BB4342B44 * value)
	{
		____rightTouchEndedCallbacks_7 = value;
		Il2CppCodeGenWriteBarrier((&____rightTouchEndedCallbacks_7), value);
	}

	inline static int32_t get_offset_of__touchMovedCallbacks_8() { return static_cast<int32_t>(offsetof(TouchControlsManager_t073F81430C963A33027AB789335B536141D833E5, ____touchMovedCallbacks_8)); }
	inline UnityAction_1_tC29A4BDE80E6622E5794F177E3DAA65BB4342B44 * get__touchMovedCallbacks_8() const { return ____touchMovedCallbacks_8; }
	inline UnityAction_1_tC29A4BDE80E6622E5794F177E3DAA65BB4342B44 ** get_address_of__touchMovedCallbacks_8() { return &____touchMovedCallbacks_8; }
	inline void set__touchMovedCallbacks_8(UnityAction_1_tC29A4BDE80E6622E5794F177E3DAA65BB4342B44 * value)
	{
		____touchMovedCallbacks_8 = value;
		Il2CppCodeGenWriteBarrier((&____touchMovedCallbacks_8), value);
	}

	inline static int32_t get_offset_of_isLeftTouch_9() { return static_cast<int32_t>(offsetof(TouchControlsManager_t073F81430C963A33027AB789335B536141D833E5, ___isLeftTouch_9)); }
	inline bool get_isLeftTouch_9() const { return ___isLeftTouch_9; }
	inline bool* get_address_of_isLeftTouch_9() { return &___isLeftTouch_9; }
	inline void set_isLeftTouch_9(bool value)
	{
		___isLeftTouch_9 = value;
	}

	inline static int32_t get_offset_of_isRightTouch_10() { return static_cast<int32_t>(offsetof(TouchControlsManager_t073F81430C963A33027AB789335B536141D833E5, ___isRightTouch_10)); }
	inline bool get_isRightTouch_10() const { return ___isRightTouch_10; }
	inline bool* get_address_of_isRightTouch_10() { return &___isRightTouch_10; }
	inline void set_isRightTouch_10(bool value)
	{
		___isRightTouch_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHCONTROLSMANAGER_T073F81430C963A33027AB789335B536141D833E5_H
#ifndef TREMBLE_TCB9E4DDD06C2C00AA46B62F34E1236025FC79C91_H
#define TREMBLE_TCB9E4DDD06C2C00AA46B62F34E1236025FC79C91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tremble
struct  Tremble_tCB9E4DDD06C2C00AA46B62F34E1236025FC79C91  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Tremble::trembleDeltaMax
	float ___trembleDeltaMax_4;
	// System.Single Tremble::invokeRate
	float ___invokeRate_5;
	// System.Boolean Tremble::moveBack
	bool ___moveBack_6;
	// System.Single Tremble::trembleDeltaX
	float ___trembleDeltaX_7;
	// System.Single Tremble::trembleDeltaY
	float ___trembleDeltaY_8;

public:
	inline static int32_t get_offset_of_trembleDeltaMax_4() { return static_cast<int32_t>(offsetof(Tremble_tCB9E4DDD06C2C00AA46B62F34E1236025FC79C91, ___trembleDeltaMax_4)); }
	inline float get_trembleDeltaMax_4() const { return ___trembleDeltaMax_4; }
	inline float* get_address_of_trembleDeltaMax_4() { return &___trembleDeltaMax_4; }
	inline void set_trembleDeltaMax_4(float value)
	{
		___trembleDeltaMax_4 = value;
	}

	inline static int32_t get_offset_of_invokeRate_5() { return static_cast<int32_t>(offsetof(Tremble_tCB9E4DDD06C2C00AA46B62F34E1236025FC79C91, ___invokeRate_5)); }
	inline float get_invokeRate_5() const { return ___invokeRate_5; }
	inline float* get_address_of_invokeRate_5() { return &___invokeRate_5; }
	inline void set_invokeRate_5(float value)
	{
		___invokeRate_5 = value;
	}

	inline static int32_t get_offset_of_moveBack_6() { return static_cast<int32_t>(offsetof(Tremble_tCB9E4DDD06C2C00AA46B62F34E1236025FC79C91, ___moveBack_6)); }
	inline bool get_moveBack_6() const { return ___moveBack_6; }
	inline bool* get_address_of_moveBack_6() { return &___moveBack_6; }
	inline void set_moveBack_6(bool value)
	{
		___moveBack_6 = value;
	}

	inline static int32_t get_offset_of_trembleDeltaX_7() { return static_cast<int32_t>(offsetof(Tremble_tCB9E4DDD06C2C00AA46B62F34E1236025FC79C91, ___trembleDeltaX_7)); }
	inline float get_trembleDeltaX_7() const { return ___trembleDeltaX_7; }
	inline float* get_address_of_trembleDeltaX_7() { return &___trembleDeltaX_7; }
	inline void set_trembleDeltaX_7(float value)
	{
		___trembleDeltaX_7 = value;
	}

	inline static int32_t get_offset_of_trembleDeltaY_8() { return static_cast<int32_t>(offsetof(Tremble_tCB9E4DDD06C2C00AA46B62F34E1236025FC79C91, ___trembleDeltaY_8)); }
	inline float get_trembleDeltaY_8() const { return ___trembleDeltaY_8; }
	inline float* get_address_of_trembleDeltaY_8() { return &___trembleDeltaY_8; }
	inline void set_trembleDeltaY_8(float value)
	{
		___trembleDeltaY_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TREMBLE_TCB9E4DDD06C2C00AA46B62F34E1236025FC79C91_H
#ifndef TRIGGER_TB7343D9B8651521A7BC56AFD7F1BD6B05F65B7F3_H
#define TRIGGER_TB7343D9B8651521A7BC56AFD7F1BD6B05F65B7F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Trigger
struct  Trigger_tB7343D9B8651521A7BC56AFD7F1BD6B05F65B7F3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Events.UnityAction`1<UnityEngine.Collider2D> Trigger::onEnter
	UnityAction_1_t3CDA73FA209A649D52861ABBAF9EC5B1847526C0 * ___onEnter_4;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Collider2D> Trigger::onExit
	UnityAction_1_t3CDA73FA209A649D52861ABBAF9EC5B1847526C0 * ___onExit_5;

public:
	inline static int32_t get_offset_of_onEnter_4() { return static_cast<int32_t>(offsetof(Trigger_tB7343D9B8651521A7BC56AFD7F1BD6B05F65B7F3, ___onEnter_4)); }
	inline UnityAction_1_t3CDA73FA209A649D52861ABBAF9EC5B1847526C0 * get_onEnter_4() const { return ___onEnter_4; }
	inline UnityAction_1_t3CDA73FA209A649D52861ABBAF9EC5B1847526C0 ** get_address_of_onEnter_4() { return &___onEnter_4; }
	inline void set_onEnter_4(UnityAction_1_t3CDA73FA209A649D52861ABBAF9EC5B1847526C0 * value)
	{
		___onEnter_4 = value;
		Il2CppCodeGenWriteBarrier((&___onEnter_4), value);
	}

	inline static int32_t get_offset_of_onExit_5() { return static_cast<int32_t>(offsetof(Trigger_tB7343D9B8651521A7BC56AFD7F1BD6B05F65B7F3, ___onExit_5)); }
	inline UnityAction_1_t3CDA73FA209A649D52861ABBAF9EC5B1847526C0 * get_onExit_5() const { return ___onExit_5; }
	inline UnityAction_1_t3CDA73FA209A649D52861ABBAF9EC5B1847526C0 ** get_address_of_onExit_5() { return &___onExit_5; }
	inline void set_onExit_5(UnityAction_1_t3CDA73FA209A649D52861ABBAF9EC5B1847526C0 * value)
	{
		___onExit_5 = value;
		Il2CppCodeGenWriteBarrier((&___onExit_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_TB7343D9B8651521A7BC56AFD7F1BD6B05F65B7F3_H
#ifndef TRIGGERCHECKER_T70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9_H
#define TRIGGERCHECKER_T70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriggerChecker
struct  TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Events.UnityAction TriggerChecker::enterActions
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___enterActions_4;
	// UnityEngine.Events.UnityAction TriggerChecker::exitActions
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___exitActions_5;
	// System.Collections.Generic.List`1<UnityEngine.Collider2D> TriggerChecker::collidersIn
	List_1_tCD80C1FE60E02F5DF3796693CF5D4E659D11F0F8 * ___collidersIn_6;

public:
	inline static int32_t get_offset_of_enterActions_4() { return static_cast<int32_t>(offsetof(TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9, ___enterActions_4)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_enterActions_4() const { return ___enterActions_4; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_enterActions_4() { return &___enterActions_4; }
	inline void set_enterActions_4(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___enterActions_4 = value;
		Il2CppCodeGenWriteBarrier((&___enterActions_4), value);
	}

	inline static int32_t get_offset_of_exitActions_5() { return static_cast<int32_t>(offsetof(TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9, ___exitActions_5)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_exitActions_5() const { return ___exitActions_5; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_exitActions_5() { return &___exitActions_5; }
	inline void set_exitActions_5(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___exitActions_5 = value;
		Il2CppCodeGenWriteBarrier((&___exitActions_5), value);
	}

	inline static int32_t get_offset_of_collidersIn_6() { return static_cast<int32_t>(offsetof(TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9, ___collidersIn_6)); }
	inline List_1_tCD80C1FE60E02F5DF3796693CF5D4E659D11F0F8 * get_collidersIn_6() const { return ___collidersIn_6; }
	inline List_1_tCD80C1FE60E02F5DF3796693CF5D4E659D11F0F8 ** get_address_of_collidersIn_6() { return &___collidersIn_6; }
	inline void set_collidersIn_6(List_1_tCD80C1FE60E02F5DF3796693CF5D4E659D11F0F8 * value)
	{
		___collidersIn_6 = value;
		Il2CppCodeGenWriteBarrier((&___collidersIn_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERCHECKER_T70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9_H
#ifndef TRIGGERCOLLECTOR_T9CCA0DBEFFA8AA280F464C98117DD43F72E8B23F_H
#define TRIGGERCOLLECTOR_T9CCA0DBEFFA8AA280F464C98117DD43F72E8B23F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriggerCollector
struct  TriggerCollector_t9CCA0DBEFFA8AA280F464C98117DD43F72E8B23F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Collider2D> TriggerCollector::collidersIn
	List_1_tCD80C1FE60E02F5DF3796693CF5D4E659D11F0F8 * ___collidersIn_4;

public:
	inline static int32_t get_offset_of_collidersIn_4() { return static_cast<int32_t>(offsetof(TriggerCollector_t9CCA0DBEFFA8AA280F464C98117DD43F72E8B23F, ___collidersIn_4)); }
	inline List_1_tCD80C1FE60E02F5DF3796693CF5D4E659D11F0F8 * get_collidersIn_4() const { return ___collidersIn_4; }
	inline List_1_tCD80C1FE60E02F5DF3796693CF5D4E659D11F0F8 ** get_address_of_collidersIn_4() { return &___collidersIn_4; }
	inline void set_collidersIn_4(List_1_tCD80C1FE60E02F5DF3796693CF5D4E659D11F0F8 * value)
	{
		___collidersIn_4 = value;
		Il2CppCodeGenWriteBarrier((&___collidersIn_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERCOLLECTOR_T9CCA0DBEFFA8AA280F464C98117DD43F72E8B23F_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef CIRCLETRIGGERCOLLECTOR_T712D0845A66149785F544AB14CC7A56A77CFC970_H
#define CIRCLETRIGGERCOLLECTOR_T712D0845A66149785F544AB14CC7A56A77CFC970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CircleTriggerCollector
struct  CircleTriggerCollector_t712D0845A66149785F544AB14CC7A56A77CFC970  : public TriggerCollector_t9CCA0DBEFFA8AA280F464C98117DD43F72E8B23F
{
public:
	// System.Single CircleTriggerCollector::radius
	float ___radius_5;
	// UnityEngine.Vector2 CircleTriggerCollector::offset
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___offset_6;

public:
	inline static int32_t get_offset_of_radius_5() { return static_cast<int32_t>(offsetof(CircleTriggerCollector_t712D0845A66149785F544AB14CC7A56A77CFC970, ___radius_5)); }
	inline float get_radius_5() const { return ___radius_5; }
	inline float* get_address_of_radius_5() { return &___radius_5; }
	inline void set_radius_5(float value)
	{
		___radius_5 = value;
	}

	inline static int32_t get_offset_of_offset_6() { return static_cast<int32_t>(offsetof(CircleTriggerCollector_t712D0845A66149785F544AB14CC7A56A77CFC970, ___offset_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_offset_6() const { return ___offset_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_offset_6() { return &___offset_6; }
	inline void set_offset_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___offset_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIRCLETRIGGERCOLLECTOR_T712D0845A66149785F544AB14CC7A56A77CFC970_H
#ifndef GAMESTATEFADE_T31A9B8F0A111B6789D1379DDEC4488DED53464DC_H
#define GAMESTATEFADE_T31A9B8F0A111B6789D1379DDEC4488DED53464DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameStateFade
struct  GameStateFade_t31A9B8F0A111B6789D1379DDEC4488DED53464DC  : public State_tC5BA2DC909C73846EB01A3E0806E5D6347274619
{
public:
	// FadeEffect GameStateFade::faderIn
	FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6 * ___faderIn_9;

public:
	inline static int32_t get_offset_of_faderIn_9() { return static_cast<int32_t>(offsetof(GameStateFade_t31A9B8F0A111B6789D1379DDEC4488DED53464DC, ___faderIn_9)); }
	inline FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6 * get_faderIn_9() const { return ___faderIn_9; }
	inline FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6 ** get_address_of_faderIn_9() { return &___faderIn_9; }
	inline void set_faderIn_9(FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6 * value)
	{
		___faderIn_9 = value;
		Il2CppCodeGenWriteBarrier((&___faderIn_9), value);
	}
};

struct GameStateFade_t31A9B8F0A111B6789D1379DDEC4488DED53464DC_StaticFields
{
public:
	// UnityEngine.Events.UnityAction GameStateFade::<>f__am$cache0
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___U3CU3Ef__amU24cache0_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_10() { return static_cast<int32_t>(offsetof(GameStateFade_t31A9B8F0A111B6789D1379DDEC4488DED53464DC_StaticFields, ___U3CU3Ef__amU24cache0_10)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_U3CU3Ef__amU24cache0_10() const { return ___U3CU3Ef__amU24cache0_10; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_U3CU3Ef__amU24cache0_10() { return &___U3CU3Ef__amU24cache0_10; }
	inline void set_U3CU3Ef__amU24cache0_10(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___U3CU3Ef__amU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESTATEFADE_T31A9B8F0A111B6789D1379DDEC4488DED53464DC_H
#ifndef GAMESTATELEVEL_T84AF52399373535B9561E8FA60043B91459DA476_H
#define GAMESTATELEVEL_T84AF52399373535B9561E8FA60043B91459DA476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameStateLevel
struct  GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476  : public State_tC5BA2DC909C73846EB01A3E0806E5D6347274619
{
public:
	// UnityEngine.Transform GameStateLevel::target
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target_9;
	// UnityEngine.Transform GameStateLevel::follower
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___follower_10;
	// System.Single GameStateLevel::trembleDeltaMin
	float ___trembleDeltaMin_11;
	// System.Single GameStateLevel::trembleDeltaMax
	float ___trembleDeltaMax_12;
	// UnityEngine.Transform GameStateLevel::bgTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bgTransform_13;
	// FadeEffect GameStateLevel::faderOut
	FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6 * ___faderOut_14;
	// System.Single GameStateLevel::trembleDeltaX
	float ___trembleDeltaX_15;
	// System.Single GameStateLevel::trembleDeltaY
	float ___trembleDeltaY_16;
	// System.Single GameStateLevel::deltaX
	float ___deltaX_17;
	// UnityEngine.Transform GameStateLevel::sky
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___sky_18;
	// UnityEngine.Transform GameStateLevel::sky1
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___sky1_19;
	// UnityEngine.Transform GameStateLevel::sunTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___sunTransform_20;
	// System.Single GameStateLevel::previousCameraPositionX
	float ___previousCameraPositionX_21;
	// System.Single GameStateLevel::previousCameraPositionY
	float ___previousCameraPositionY_22;
	// UnityEngine.Rigidbody2D GameStateLevel::bord
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___bord_23;
	// StateMachine GameStateLevel::heroStateMachine
	StateMachine_tA4A39BBD7B24E4B1C7B690D3C6BE73E7023EFD01 * ___heroStateMachine_24;

public:
	inline static int32_t get_offset_of_target_9() { return static_cast<int32_t>(offsetof(GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476, ___target_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_target_9() const { return ___target_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_target_9() { return &___target_9; }
	inline void set_target_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___target_9 = value;
		Il2CppCodeGenWriteBarrier((&___target_9), value);
	}

	inline static int32_t get_offset_of_follower_10() { return static_cast<int32_t>(offsetof(GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476, ___follower_10)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_follower_10() const { return ___follower_10; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_follower_10() { return &___follower_10; }
	inline void set_follower_10(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___follower_10 = value;
		Il2CppCodeGenWriteBarrier((&___follower_10), value);
	}

	inline static int32_t get_offset_of_trembleDeltaMin_11() { return static_cast<int32_t>(offsetof(GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476, ___trembleDeltaMin_11)); }
	inline float get_trembleDeltaMin_11() const { return ___trembleDeltaMin_11; }
	inline float* get_address_of_trembleDeltaMin_11() { return &___trembleDeltaMin_11; }
	inline void set_trembleDeltaMin_11(float value)
	{
		___trembleDeltaMin_11 = value;
	}

	inline static int32_t get_offset_of_trembleDeltaMax_12() { return static_cast<int32_t>(offsetof(GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476, ___trembleDeltaMax_12)); }
	inline float get_trembleDeltaMax_12() const { return ___trembleDeltaMax_12; }
	inline float* get_address_of_trembleDeltaMax_12() { return &___trembleDeltaMax_12; }
	inline void set_trembleDeltaMax_12(float value)
	{
		___trembleDeltaMax_12 = value;
	}

	inline static int32_t get_offset_of_bgTransform_13() { return static_cast<int32_t>(offsetof(GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476, ___bgTransform_13)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bgTransform_13() const { return ___bgTransform_13; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bgTransform_13() { return &___bgTransform_13; }
	inline void set_bgTransform_13(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bgTransform_13 = value;
		Il2CppCodeGenWriteBarrier((&___bgTransform_13), value);
	}

	inline static int32_t get_offset_of_faderOut_14() { return static_cast<int32_t>(offsetof(GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476, ___faderOut_14)); }
	inline FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6 * get_faderOut_14() const { return ___faderOut_14; }
	inline FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6 ** get_address_of_faderOut_14() { return &___faderOut_14; }
	inline void set_faderOut_14(FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6 * value)
	{
		___faderOut_14 = value;
		Il2CppCodeGenWriteBarrier((&___faderOut_14), value);
	}

	inline static int32_t get_offset_of_trembleDeltaX_15() { return static_cast<int32_t>(offsetof(GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476, ___trembleDeltaX_15)); }
	inline float get_trembleDeltaX_15() const { return ___trembleDeltaX_15; }
	inline float* get_address_of_trembleDeltaX_15() { return &___trembleDeltaX_15; }
	inline void set_trembleDeltaX_15(float value)
	{
		___trembleDeltaX_15 = value;
	}

	inline static int32_t get_offset_of_trembleDeltaY_16() { return static_cast<int32_t>(offsetof(GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476, ___trembleDeltaY_16)); }
	inline float get_trembleDeltaY_16() const { return ___trembleDeltaY_16; }
	inline float* get_address_of_trembleDeltaY_16() { return &___trembleDeltaY_16; }
	inline void set_trembleDeltaY_16(float value)
	{
		___trembleDeltaY_16 = value;
	}

	inline static int32_t get_offset_of_deltaX_17() { return static_cast<int32_t>(offsetof(GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476, ___deltaX_17)); }
	inline float get_deltaX_17() const { return ___deltaX_17; }
	inline float* get_address_of_deltaX_17() { return &___deltaX_17; }
	inline void set_deltaX_17(float value)
	{
		___deltaX_17 = value;
	}

	inline static int32_t get_offset_of_sky_18() { return static_cast<int32_t>(offsetof(GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476, ___sky_18)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_sky_18() const { return ___sky_18; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_sky_18() { return &___sky_18; }
	inline void set_sky_18(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___sky_18 = value;
		Il2CppCodeGenWriteBarrier((&___sky_18), value);
	}

	inline static int32_t get_offset_of_sky1_19() { return static_cast<int32_t>(offsetof(GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476, ___sky1_19)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_sky1_19() const { return ___sky1_19; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_sky1_19() { return &___sky1_19; }
	inline void set_sky1_19(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___sky1_19 = value;
		Il2CppCodeGenWriteBarrier((&___sky1_19), value);
	}

	inline static int32_t get_offset_of_sunTransform_20() { return static_cast<int32_t>(offsetof(GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476, ___sunTransform_20)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_sunTransform_20() const { return ___sunTransform_20; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_sunTransform_20() { return &___sunTransform_20; }
	inline void set_sunTransform_20(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___sunTransform_20 = value;
		Il2CppCodeGenWriteBarrier((&___sunTransform_20), value);
	}

	inline static int32_t get_offset_of_previousCameraPositionX_21() { return static_cast<int32_t>(offsetof(GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476, ___previousCameraPositionX_21)); }
	inline float get_previousCameraPositionX_21() const { return ___previousCameraPositionX_21; }
	inline float* get_address_of_previousCameraPositionX_21() { return &___previousCameraPositionX_21; }
	inline void set_previousCameraPositionX_21(float value)
	{
		___previousCameraPositionX_21 = value;
	}

	inline static int32_t get_offset_of_previousCameraPositionY_22() { return static_cast<int32_t>(offsetof(GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476, ___previousCameraPositionY_22)); }
	inline float get_previousCameraPositionY_22() const { return ___previousCameraPositionY_22; }
	inline float* get_address_of_previousCameraPositionY_22() { return &___previousCameraPositionY_22; }
	inline void set_previousCameraPositionY_22(float value)
	{
		___previousCameraPositionY_22 = value;
	}

	inline static int32_t get_offset_of_bord_23() { return static_cast<int32_t>(offsetof(GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476, ___bord_23)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_bord_23() const { return ___bord_23; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_bord_23() { return &___bord_23; }
	inline void set_bord_23(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___bord_23 = value;
		Il2CppCodeGenWriteBarrier((&___bord_23), value);
	}

	inline static int32_t get_offset_of_heroStateMachine_24() { return static_cast<int32_t>(offsetof(GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476, ___heroStateMachine_24)); }
	inline StateMachine_tA4A39BBD7B24E4B1C7B690D3C6BE73E7023EFD01 * get_heroStateMachine_24() const { return ___heroStateMachine_24; }
	inline StateMachine_tA4A39BBD7B24E4B1C7B690D3C6BE73E7023EFD01 ** get_address_of_heroStateMachine_24() { return &___heroStateMachine_24; }
	inline void set_heroStateMachine_24(StateMachine_tA4A39BBD7B24E4B1C7B690D3C6BE73E7023EFD01 * value)
	{
		___heroStateMachine_24 = value;
		Il2CppCodeGenWriteBarrier((&___heroStateMachine_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESTATELEVEL_T84AF52399373535B9561E8FA60043B91459DA476_H
#ifndef HEROSTATE_T4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559_H
#define HEROSTATE_T4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroState
struct  HeroState_t4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559  : public State_tC5BA2DC909C73846EB01A3E0806E5D6347274619
{
public:
	// Hero HeroState::_hero
	Hero_t39258A796E56A51462429C62F9A05CB012E66B5D * ____hero_9;
	// TouchControlsManager HeroState::_touchControls
	TouchControlsManager_t073F81430C963A33027AB789335B536141D833E5 * ____touchControls_10;
	// TriggerChecker HeroState::_crushTriggerCheckerLeft
	TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9 * ____crushTriggerCheckerLeft_11;
	// TriggerChecker HeroState::_crushTriggerHead
	TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9 * ____crushTriggerHead_12;
	// UnityEngine.CircleCollider2D HeroState::_crushColliderLeft
	CircleCollider2D_t862AED067B612149EF365A560B26406F6088641F * ____crushColliderLeft_13;
	// UnityEngine.Rigidbody2D HeroState::_body
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ____body_14;
	// TriggerChecker HeroState::_bordTriggerChecker
	TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9 * ____bordTriggerChecker_15;
	// TriggerChecker HeroState::_leaveGroundTriggerChecker
	TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9 * ____leaveGroundTriggerChecker_16;

public:
	inline static int32_t get_offset_of__hero_9() { return static_cast<int32_t>(offsetof(HeroState_t4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559, ____hero_9)); }
	inline Hero_t39258A796E56A51462429C62F9A05CB012E66B5D * get__hero_9() const { return ____hero_9; }
	inline Hero_t39258A796E56A51462429C62F9A05CB012E66B5D ** get_address_of__hero_9() { return &____hero_9; }
	inline void set__hero_9(Hero_t39258A796E56A51462429C62F9A05CB012E66B5D * value)
	{
		____hero_9 = value;
		Il2CppCodeGenWriteBarrier((&____hero_9), value);
	}

	inline static int32_t get_offset_of__touchControls_10() { return static_cast<int32_t>(offsetof(HeroState_t4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559, ____touchControls_10)); }
	inline TouchControlsManager_t073F81430C963A33027AB789335B536141D833E5 * get__touchControls_10() const { return ____touchControls_10; }
	inline TouchControlsManager_t073F81430C963A33027AB789335B536141D833E5 ** get_address_of__touchControls_10() { return &____touchControls_10; }
	inline void set__touchControls_10(TouchControlsManager_t073F81430C963A33027AB789335B536141D833E5 * value)
	{
		____touchControls_10 = value;
		Il2CppCodeGenWriteBarrier((&____touchControls_10), value);
	}

	inline static int32_t get_offset_of__crushTriggerCheckerLeft_11() { return static_cast<int32_t>(offsetof(HeroState_t4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559, ____crushTriggerCheckerLeft_11)); }
	inline TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9 * get__crushTriggerCheckerLeft_11() const { return ____crushTriggerCheckerLeft_11; }
	inline TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9 ** get_address_of__crushTriggerCheckerLeft_11() { return &____crushTriggerCheckerLeft_11; }
	inline void set__crushTriggerCheckerLeft_11(TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9 * value)
	{
		____crushTriggerCheckerLeft_11 = value;
		Il2CppCodeGenWriteBarrier((&____crushTriggerCheckerLeft_11), value);
	}

	inline static int32_t get_offset_of__crushTriggerHead_12() { return static_cast<int32_t>(offsetof(HeroState_t4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559, ____crushTriggerHead_12)); }
	inline TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9 * get__crushTriggerHead_12() const { return ____crushTriggerHead_12; }
	inline TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9 ** get_address_of__crushTriggerHead_12() { return &____crushTriggerHead_12; }
	inline void set__crushTriggerHead_12(TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9 * value)
	{
		____crushTriggerHead_12 = value;
		Il2CppCodeGenWriteBarrier((&____crushTriggerHead_12), value);
	}

	inline static int32_t get_offset_of__crushColliderLeft_13() { return static_cast<int32_t>(offsetof(HeroState_t4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559, ____crushColliderLeft_13)); }
	inline CircleCollider2D_t862AED067B612149EF365A560B26406F6088641F * get__crushColliderLeft_13() const { return ____crushColliderLeft_13; }
	inline CircleCollider2D_t862AED067B612149EF365A560B26406F6088641F ** get_address_of__crushColliderLeft_13() { return &____crushColliderLeft_13; }
	inline void set__crushColliderLeft_13(CircleCollider2D_t862AED067B612149EF365A560B26406F6088641F * value)
	{
		____crushColliderLeft_13 = value;
		Il2CppCodeGenWriteBarrier((&____crushColliderLeft_13), value);
	}

	inline static int32_t get_offset_of__body_14() { return static_cast<int32_t>(offsetof(HeroState_t4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559, ____body_14)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get__body_14() const { return ____body_14; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of__body_14() { return &____body_14; }
	inline void set__body_14(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		____body_14 = value;
		Il2CppCodeGenWriteBarrier((&____body_14), value);
	}

	inline static int32_t get_offset_of__bordTriggerChecker_15() { return static_cast<int32_t>(offsetof(HeroState_t4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559, ____bordTriggerChecker_15)); }
	inline TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9 * get__bordTriggerChecker_15() const { return ____bordTriggerChecker_15; }
	inline TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9 ** get_address_of__bordTriggerChecker_15() { return &____bordTriggerChecker_15; }
	inline void set__bordTriggerChecker_15(TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9 * value)
	{
		____bordTriggerChecker_15 = value;
		Il2CppCodeGenWriteBarrier((&____bordTriggerChecker_15), value);
	}

	inline static int32_t get_offset_of__leaveGroundTriggerChecker_16() { return static_cast<int32_t>(offsetof(HeroState_t4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559, ____leaveGroundTriggerChecker_16)); }
	inline TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9 * get__leaveGroundTriggerChecker_16() const { return ____leaveGroundTriggerChecker_16; }
	inline TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9 ** get_address_of__leaveGroundTriggerChecker_16() { return &____leaveGroundTriggerChecker_16; }
	inline void set__leaveGroundTriggerChecker_16(TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9 * value)
	{
		____leaveGroundTriggerChecker_16 = value;
		Il2CppCodeGenWriteBarrier((&____leaveGroundTriggerChecker_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROSTATE_T4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559_H
#ifndef BASEMESHEFFECT_T72759F31F9D204D7EFB6B45097873809D4524BA5_H
#define BASEMESHEFFECT_T72759F31F9D204D7EFB6B45097873809D4524BA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___m_Graphic_4;

public:
	inline static int32_t get_offset_of_m_Graphic_4() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5, ___m_Graphic_4)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_m_Graphic_4() const { return ___m_Graphic_4; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_m_Graphic_4() { return &___m_Graphic_4; }
	inline void set_m_Graphic_4(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___m_Graphic_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T72759F31F9D204D7EFB6B45097873809D4524BA5_H
#ifndef LAYOUTGROUP_T9E072B95DA6476C487C0B07A815291249025C0E4_H
#define LAYOUTGROUP_T9E072B95DA6476C487C0B07A815291249025C0E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup
struct  LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::m_Padding
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * ___m_Padding_4;
	// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::m_ChildAlignment
	int32_t ___m_ChildAlignment_5;
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::m_Rect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_Rect_6;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.LayoutGroup::m_Tracker
	DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  ___m_Tracker_7;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalMinSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_TotalMinSize_8;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalPreferredSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_TotalPreferredSize_9;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalFlexibleSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_TotalFlexibleSize_10;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::m_RectChildren
	List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C * ___m_RectChildren_11;

public:
	inline static int32_t get_offset_of_m_Padding_4() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_Padding_4)); }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * get_m_Padding_4() const { return ___m_Padding_4; }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A ** get_address_of_m_Padding_4() { return &___m_Padding_4; }
	inline void set_m_Padding_4(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * value)
	{
		___m_Padding_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_4), value);
	}

	inline static int32_t get_offset_of_m_ChildAlignment_5() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_ChildAlignment_5)); }
	inline int32_t get_m_ChildAlignment_5() const { return ___m_ChildAlignment_5; }
	inline int32_t* get_address_of_m_ChildAlignment_5() { return &___m_ChildAlignment_5; }
	inline void set_m_ChildAlignment_5(int32_t value)
	{
		___m_ChildAlignment_5 = value;
	}

	inline static int32_t get_offset_of_m_Rect_6() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_Rect_6)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_Rect_6() const { return ___m_Rect_6; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_Rect_6() { return &___m_Rect_6; }
	inline void set_m_Rect_6(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_Rect_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_6), value);
	}

	inline static int32_t get_offset_of_m_Tracker_7() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_Tracker_7)); }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  get_m_Tracker_7() const { return ___m_Tracker_7; }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03 * get_address_of_m_Tracker_7() { return &___m_Tracker_7; }
	inline void set_m_Tracker_7(DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  value)
	{
		___m_Tracker_7 = value;
	}

	inline static int32_t get_offset_of_m_TotalMinSize_8() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_TotalMinSize_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_TotalMinSize_8() const { return ___m_TotalMinSize_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_TotalMinSize_8() { return &___m_TotalMinSize_8; }
	inline void set_m_TotalMinSize_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_TotalMinSize_8 = value;
	}

	inline static int32_t get_offset_of_m_TotalPreferredSize_9() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_TotalPreferredSize_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_TotalPreferredSize_9() const { return ___m_TotalPreferredSize_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_TotalPreferredSize_9() { return &___m_TotalPreferredSize_9; }
	inline void set_m_TotalPreferredSize_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_TotalPreferredSize_9 = value;
	}

	inline static int32_t get_offset_of_m_TotalFlexibleSize_10() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_TotalFlexibleSize_10)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_TotalFlexibleSize_10() const { return ___m_TotalFlexibleSize_10; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_TotalFlexibleSize_10() { return &___m_TotalFlexibleSize_10; }
	inline void set_m_TotalFlexibleSize_10(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_TotalFlexibleSize_10 = value;
	}

	inline static int32_t get_offset_of_m_RectChildren_11() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_RectChildren_11)); }
	inline List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C * get_m_RectChildren_11() const { return ___m_RectChildren_11; }
	inline List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C ** get_address_of_m_RectChildren_11() { return &___m_RectChildren_11; }
	inline void set_m_RectChildren_11(List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C * value)
	{
		___m_RectChildren_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectChildren_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTGROUP_T9E072B95DA6476C487C0B07A815291249025C0E4_H
#ifndef HEROSTATECRASH_TA05D940D5513FD58DEF5C9174C48FA344FDDB873_H
#define HEROSTATECRASH_TA05D940D5513FD58DEF5C9174C48FA344FDDB873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroStateCrash
struct  HeroStateCrash_tA05D940D5513FD58DEF5C9174C48FA344FDDB873  : public HeroState_t4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROSTATECRASH_TA05D940D5513FD58DEF5C9174C48FA344FDDB873_H
#ifndef HEROSTATEINAIR_T6BF4B686590D20FFAD10ED5B125E03FB2BEA55B5_H
#define HEROSTATEINAIR_T6BF4B686590D20FFAD10ED5B125E03FB2BEA55B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroStateInAir
struct  HeroStateInAir_t6BF4B686590D20FFAD10ED5B125E03FB2BEA55B5  : public HeroState_t4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559
{
public:
	// System.Boolean HeroStateInAir::_forceDown
	bool ____forceDown_17;

public:
	inline static int32_t get_offset_of__forceDown_17() { return static_cast<int32_t>(offsetof(HeroStateInAir_t6BF4B686590D20FFAD10ED5B125E03FB2BEA55B5, ____forceDown_17)); }
	inline bool get__forceDown_17() const { return ____forceDown_17; }
	inline bool* get_address_of__forceDown_17() { return &____forceDown_17; }
	inline void set__forceDown_17(bool value)
	{
		____forceDown_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROSTATEINAIR_T6BF4B686590D20FFAD10ED5B125E03FB2BEA55B5_H
#ifndef HEROSTATEJUMP_T23602F3AF872DD888F6461617D9CFDB32CE7E189_H
#define HEROSTATEJUMP_T23602F3AF872DD888F6461617D9CFDB32CE7E189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroStateJump
struct  HeroStateJump_t23602F3AF872DD888F6461617D9CFDB32CE7E189  : public HeroState_t4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROSTATEJUMP_T23602F3AF872DD888F6461617D9CFDB32CE7E189_H
#ifndef HEROSTATERUN_T45494C32EB550F45009424A43F225FB62263656E_H
#define HEROSTATERUN_T45494C32EB550F45009424A43F225FB62263656E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroStateRun
struct  HeroStateRun_t45494C32EB550F45009424A43F225FB62263656E  : public HeroState_t4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROSTATERUN_T45494C32EB550F45009424A43F225FB62263656E_H
#ifndef HEROSTATEWALK_T132A07E16CABB7DC4B19437499F95EDDCC24E5F4_H
#define HEROSTATEWALK_T132A07E16CABB7DC4B19437499F95EDDCC24E5F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroStateWalk
struct  HeroStateWalk_t132A07E16CABB7DC4B19437499F95EDDCC24E5F4  : public HeroState_t4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROSTATEWALK_T132A07E16CABB7DC4B19437499F95EDDCC24E5F4_H
#ifndef HORIZONTALORVERTICALLAYOUTGROUP_TFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB_H
#define HORIZONTALORVERTICALLAYOUTGROUP_TFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
struct  HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB  : public LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4
{
public:
	// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_Spacing
	float ___m_Spacing_12;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandWidth
	bool ___m_ChildForceExpandWidth_13;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandHeight
	bool ___m_ChildForceExpandHeight_14;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlWidth
	bool ___m_ChildControlWidth_15;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlHeight
	bool ___m_ChildControlHeight_16;

public:
	inline static int32_t get_offset_of_m_Spacing_12() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_Spacing_12)); }
	inline float get_m_Spacing_12() const { return ___m_Spacing_12; }
	inline float* get_address_of_m_Spacing_12() { return &___m_Spacing_12; }
	inline void set_m_Spacing_12(float value)
	{
		___m_Spacing_12 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandWidth_13() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_ChildForceExpandWidth_13)); }
	inline bool get_m_ChildForceExpandWidth_13() const { return ___m_ChildForceExpandWidth_13; }
	inline bool* get_address_of_m_ChildForceExpandWidth_13() { return &___m_ChildForceExpandWidth_13; }
	inline void set_m_ChildForceExpandWidth_13(bool value)
	{
		___m_ChildForceExpandWidth_13 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandHeight_14() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_ChildForceExpandHeight_14)); }
	inline bool get_m_ChildForceExpandHeight_14() const { return ___m_ChildForceExpandHeight_14; }
	inline bool* get_address_of_m_ChildForceExpandHeight_14() { return &___m_ChildForceExpandHeight_14; }
	inline void set_m_ChildForceExpandHeight_14(bool value)
	{
		___m_ChildForceExpandHeight_14 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlWidth_15() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_ChildControlWidth_15)); }
	inline bool get_m_ChildControlWidth_15() const { return ___m_ChildControlWidth_15; }
	inline bool* get_address_of_m_ChildControlWidth_15() { return &___m_ChildControlWidth_15; }
	inline void set_m_ChildControlWidth_15(bool value)
	{
		___m_ChildControlWidth_15 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlHeight_16() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_ChildControlHeight_16)); }
	inline bool get_m_ChildControlHeight_16() const { return ___m_ChildControlHeight_16; }
	inline bool* get_address_of_m_ChildControlHeight_16() { return &___m_ChildControlHeight_16; }
	inline void set_m_ChildControlHeight_16(bool value)
	{
		___m_ChildControlHeight_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALORVERTICALLAYOUTGROUP_TFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB_H
#ifndef POSITIONASUV1_T26F06E879E7B8DD2F93B8B3643053534D82F684A_H
#define POSITIONASUV1_T26F06E879E7B8DD2F93B8B3643053534D82F684A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.PositionAsUV1
struct  PositionAsUV1_t26F06E879E7B8DD2F93B8B3643053534D82F684A  : public BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONASUV1_T26F06E879E7B8DD2F93B8B3643053534D82F684A_H
#ifndef SHADOW_TA03D2493843CDF8E64569F985AEB3FEEEEB412E1_H
#define SHADOW_TA03D2493843CDF8E64569F985AEB3FEEEEB412E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1  : public BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_EffectColor_5;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_EffectDistance_6;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_7;

public:
	inline static int32_t get_offset_of_m_EffectColor_5() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_EffectColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_EffectColor_5() const { return ___m_EffectColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_EffectColor_5() { return &___m_EffectColor_5; }
	inline void set_m_EffectColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_EffectColor_5 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_6() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_EffectDistance_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_EffectDistance_6() const { return ___m_EffectDistance_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_EffectDistance_6() { return &___m_EffectDistance_6; }
	inline void set_m_EffectDistance_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_EffectDistance_6 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_7() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_UseGraphicAlpha_7)); }
	inline bool get_m_UseGraphicAlpha_7() const { return ___m_UseGraphicAlpha_7; }
	inline bool* get_address_of_m_UseGraphicAlpha_7() { return &___m_UseGraphicAlpha_7; }
	inline void set_m_UseGraphicAlpha_7(bool value)
	{
		___m_UseGraphicAlpha_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_TA03D2493843CDF8E64569F985AEB3FEEEEB412E1_H
#ifndef OUTLINE_TB750E496976B072E79142D51C0A991AC20183095_H
#define OUTLINE_TB750E496976B072E79142D51C0A991AC20183095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Outline
struct  Outline_tB750E496976B072E79142D51C0A991AC20183095  : public Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE_TB750E496976B072E79142D51C0A991AC20183095_H
#ifndef VERTICALLAYOUTGROUP_TAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11_H
#define VERTICALLAYOUTGROUP_TAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VerticalLayoutGroup
struct  VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11  : public HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTICALLAYOUTGROUP_TAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2602[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2603[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2604[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A), -1, sizeof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2605[7] = 
{
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_getRayIntersectionAllNonAlloc_4(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_getRaycastNonAlloc_5(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields::get_offset_of_s_ReflectionMethodsCache_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { sizeof (VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F), -1, sizeof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2612[12] = 
{
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Positions_0(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Colors_1(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv0S_2(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv1S_3(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv2S_4(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv3S_5(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Normals_6(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Tangents_7(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Indices_8(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields::get_offset_of_s_DefaultNormal_10(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_ListsInitalized_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { sizeof (BaseVertexEffect_t1EF95AB1FC33A027710E7DC86D19F700156C4F6A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2614[1] = 
{
	BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5::get_offset_of_m_Graphic_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (Outline_tB750E496976B072E79142D51C0A991AC20183095), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { sizeof (PositionAsUV1_t26F06E879E7B8DD2F93B8B3643053534D82F684A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { sizeof (Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2619[4] = 
{
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_EffectColor_5(),
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_EffectDistance_6(),
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_UseGraphicAlpha_7(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { sizeof (U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537), -1, sizeof(U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2620[1] = 
{
	U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { sizeof (U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { sizeof (U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { sizeof (BackgroundSprite_tDF2701B45FEB226EEA8435126B95867AD931DF9D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (Colorful_tD51FF6AEC7B62A96900FAD18EC6CD8D78882617A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2624[6] = 
{
	Colorful_tD51FF6AEC7B62A96900FAD18EC6CD8D78882617A::get_offset_of_m_HueModifyMin_4(),
	Colorful_tD51FF6AEC7B62A96900FAD18EC6CD8D78882617A::get_offset_of_m_HueModifyMax_5(),
	Colorful_tD51FF6AEC7B62A96900FAD18EC6CD8D78882617A::get_offset_of_m_HueOffset_6(),
	Colorful_tD51FF6AEC7B62A96900FAD18EC6CD8D78882617A::get_offset_of_m_SaturationOffset_7(),
	Colorful_tD51FF6AEC7B62A96900FAD18EC6CD8D78882617A::get_offset_of_m_ValueOffset_8(),
	Colorful_tD51FF6AEC7B62A96900FAD18EC6CD8D78882617A::get_offset_of_m_Rdrs_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (Electricity_t1F7DFD13FABFEFC103A769012CDF55C743549E4C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2625[4] = 
{
	Electricity_t1F7DFD13FABFEFC103A769012CDF55C743549E4C::get_offset_of_m_TransMode_4(),
	Electricity_t1F7DFD13FABFEFC103A769012CDF55C743549E4C::get_offset_of_m_Color_5(),
	Electricity_t1F7DFD13FABFEFC103A769012CDF55C743549E4C::get_offset_of_m_Noise_6(),
	Electricity_t1F7DFD13FABFEFC103A769012CDF55C743549E4C::get_offset_of_m_Rd_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (ETransparentMode_t371F435EE5FDB18BBCC5DACA1C309E541954CCDA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2626[3] = 
{
	ETransparentMode_t371F435EE5FDB18BBCC5DACA1C309E541954CCDA::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { sizeof (Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2627[18] = 
{
	Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449::get_offset_of_m_Enable_4(),
	Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449::get_offset_of_m_EnableQuantize_5(),
	Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449::get_offset_of_m_RBits_6(),
	Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449::get_offset_of_m_GBits_7(),
	Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449::get_offset_of_m_BBits_8(),
	Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449::get_offset_of_m_EnableTvCurvature_9(),
	Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449::get_offset_of_m_TvCurvature_10(),
	Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449::get_offset_of_m_EnablePixelMask_11(),
	Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449::get_offset_of_m_PixelMask_12(),
	Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449::get_offset_of_m_PixelMaskParams_13(),
	Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449::get_offset_of_m_EnableRollingFlicker_14(),
	Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449::get_offset_of_m_RollingFlickerAmount_15(),
	Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449::get_offset_of_m_RollingFlickerOffset_16(),
	Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449::get_offset_of_m_RollingFlickerVsynctime_17(),
	Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449::get_offset_of_m_EnableGameboy_18(),
	Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449::get_offset_of_m_EnablePixelate_19(),
	Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449::get_offset_of_m_PixelateSize_20(),
	Filters_t6332699A87E0C307D1D29E72E4EFB591D66FF449::get_offset_of_m_Mat_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { sizeof (Fire_t04C1C4B27F6275E3994BBB5F9117B7CB13A5054F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2628[2] = 
{
	Fire_t04C1C4B27F6275E3994BBB5F9117B7CB13A5054F::get_offset_of_m_LayerSpeed_4(),
	Fire_t04C1C4B27F6275E3994BBB5F9117B7CB13A5054F::get_offset_of_m_DistortionStrength_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { sizeof (Glitch_t6594C2956BB4993FF0D4641FCC32C65A565F5908), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2629[6] = 
{
	Glitch_t6594C2956BB4993FF0D4641FCC32C65A565F5908::get_offset_of_m_IntervalSeconds_4(),
	Glitch_t6594C2956BB4993FF0D4641FCC32C65A565F5908::get_offset_of_m_DispProbability_5(),
	Glitch_t6594C2956BB4993FF0D4641FCC32C65A565F5908::get_offset_of_m_DispIntensity_6(),
	Glitch_t6594C2956BB4993FF0D4641FCC32C65A565F5908::get_offset_of_m_ColorProbability_7(),
	Glitch_t6594C2956BB4993FF0D4641FCC32C65A565F5908::get_offset_of_m_ColorIntensity_8(),
	Glitch_t6594C2956BB4993FF0D4641FCC32C65A565F5908::get_offset_of_m_Mat_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { sizeof (Noise3D_t288ADB7A6ECA2CA06F240738A7A7FDF20E7D5B20), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2630[2] = 
{
	Noise3D_t288ADB7A6ECA2CA06F240738A7A7FDF20E7D5B20::get_offset_of_m_3DTex_0(),
	Noise3D_t288ADB7A6ECA2CA06F240738A7A7FDF20E7D5B20::get_offset_of_m_Size_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { sizeof (Perlin_t9FD22982AE95029048B11554B511F2A308111C49), -1, sizeof(Perlin_t9FD22982AE95029048B11554B511F2A308111C49_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2631[1] = 
{
	Perlin_t9FD22982AE95029048B11554B511F2A308111C49_StaticFields::get_offset_of_perm_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { sizeof (Sky_t49010BFAA5667EB627116AB4805D37D4A721C2EB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2632[10] = 
{
	Sky_t49010BFAA5667EB627116AB4805D37D4A721C2EB::get_offset_of_m_TimeOfDay_4(),
	Sky_t49010BFAA5667EB627116AB4805D37D4A721C2EB::get_offset_of_m_BottomSunLevel_5(),
	Sky_t49010BFAA5667EB627116AB4805D37D4A721C2EB::get_offset_of_m_SunSize_6(),
	Sky_t49010BFAA5667EB627116AB4805D37D4A721C2EB::get_offset_of_m_CloudSpeed_7(),
	Sky_t49010BFAA5667EB627116AB4805D37D4A721C2EB::get_offset_of_m_CloudDensity_8(),
	Sky_t49010BFAA5667EB627116AB4805D37D4A721C2EB::get_offset_of_m_CloudSharpness_9(),
	Sky_t49010BFAA5667EB627116AB4805D37D4A721C2EB::get_offset_of_m_CloudColor_10(),
	Sky_t49010BFAA5667EB627116AB4805D37D4A721C2EB::get_offset_of_m_UseThickCloud_11(),
	Sky_t49010BFAA5667EB627116AB4805D37D4A721C2EB::get_offset_of_m_StarDensity_12(),
	Sky_t49010BFAA5667EB627116AB4805D37D4A721C2EB::get_offset_of_m_Spr_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { sizeof (Between_t8AF05A448BE9EEF66260593E8CC15F84E6F3DA37), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { sizeof (FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2634[7] = 
{
	FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6::get_offset_of_fadeCurve_4(),
	FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6::get_offset_of_onEndCallback_5(),
	FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6::get_offset_of_startAlpha_6(),
	FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6::get_offset_of__alpha_7(),
	FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6::get_offset_of__texture_8(),
	FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6::get_offset_of__done_9(),
	FadeEffect_tED731F6EC346B384F24D34AD119A2ADB69AFDBE6::get_offset_of__time_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { sizeof (TouchTypes_tCEE6B2D4570D8BA114C071D5680AF688C3CCFC2F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2635[6] = 
{
	TouchTypes_tCEE6B2D4570D8BA114C071D5680AF688C3CCFC2F::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { sizeof (TouchControlsManager_t073F81430C963A33027AB789335B536141D833E5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2636[7] = 
{
	TouchControlsManager_t073F81430C963A33027AB789335B536141D833E5::get_offset_of__leftTouchBeganCallbacks_4(),
	TouchControlsManager_t073F81430C963A33027AB789335B536141D833E5::get_offset_of__leftTouchEndedCallbacks_5(),
	TouchControlsManager_t073F81430C963A33027AB789335B536141D833E5::get_offset_of__rightTouchBeganCallbacks_6(),
	TouchControlsManager_t073F81430C963A33027AB789335B536141D833E5::get_offset_of__rightTouchEndedCallbacks_7(),
	TouchControlsManager_t073F81430C963A33027AB789335B536141D833E5::get_offset_of__touchMovedCallbacks_8(),
	TouchControlsManager_t073F81430C963A33027AB789335B536141D833E5::get_offset_of_isLeftTouch_9(),
	TouchControlsManager_t073F81430C963A33027AB789335B536141D833E5::get_offset_of_isRightTouch_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (Pulse_tD3C350CD2F7D32DD3CD582813B2571DA6C0F387F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2637[5] = 
{
	Pulse_tD3C350CD2F7D32DD3CD582813B2571DA6C0F387F::get_offset_of_scaleFrom_4(),
	Pulse_tD3C350CD2F7D32DD3CD582813B2571DA6C0F387F::get_offset_of_scaleTo_5(),
	Pulse_tD3C350CD2F7D32DD3CD582813B2571DA6C0F387F::get_offset_of_velocityExpand_6(),
	Pulse_tD3C350CD2F7D32DD3CD582813B2571DA6C0F387F::get_offset_of_velocityBack_7(),
	Pulse_tD3C350CD2F7D32DD3CD582813B2571DA6C0F387F::get_offset_of_isExpanding_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (RandomScale_t6E4823A4B88513C67AC1568E5B5E746545871150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2638[4] = 
{
	RandomScale_t6E4823A4B88513C67AC1568E5B5E746545871150::get_offset_of_invokeRate_4(),
	RandomScale_t6E4823A4B88513C67AC1568E5B5E746545871150::get_offset_of_maxDtScaleX_5(),
	RandomScale_t6E4823A4B88513C67AC1568E5B5E746545871150::get_offset_of_maxDtScaleY_6(),
	RandomScale_t6E4823A4B88513C67AC1568E5B5E746545871150::get_offset_of_startScale_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (RotateObject_tBED621143EDF174654659FF578290495BBF4CFA9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2639[1] = 
{
	RotateObject_tBED621143EDF174654659FF578290495BBF4CFA9::get_offset_of_rotationVelocity_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (SpriteAlphaBlink_tE51DC575219556D6B2D78290C68C3F23160E4449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2640[3] = 
{
	SpriteAlphaBlink_tE51DC575219556D6B2D78290C68C3F23160E4449::get_offset_of_alphaValues_4(),
	SpriteAlphaBlink_tE51DC575219556D6B2D78290C68C3F23160E4449::get_offset_of_invokeRate_5(),
	SpriteAlphaBlink_tE51DC575219556D6B2D78290C68C3F23160E4449::get_offset_of_blinkSpriteRender_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { sizeof (Tremble_tCB9E4DDD06C2C00AA46B62F34E1236025FC79C91), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2641[5] = 
{
	Tremble_tCB9E4DDD06C2C00AA46B62F34E1236025FC79C91::get_offset_of_trembleDeltaMax_4(),
	Tremble_tCB9E4DDD06C2C00AA46B62F34E1236025FC79C91::get_offset_of_invokeRate_5(),
	Tremble_tCB9E4DDD06C2C00AA46B62F34E1236025FC79C91::get_offset_of_moveBack_6(),
	Tremble_tCB9E4DDD06C2C00AA46B62F34E1236025FC79C91::get_offset_of_trembleDeltaX_7(),
	Tremble_tCB9E4DDD06C2C00AA46B62F34E1236025FC79C91::get_offset_of_trembleDeltaY_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (GameStateFade_t31A9B8F0A111B6789D1379DDEC4488DED53464DC), -1, sizeof(GameStateFade_t31A9B8F0A111B6789D1379DDEC4488DED53464DC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2642[2] = 
{
	GameStateFade_t31A9B8F0A111B6789D1379DDEC4488DED53464DC::get_offset_of_faderIn_9(),
	GameStateFade_t31A9B8F0A111B6789D1379DDEC4488DED53464DC_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2643[16] = 
{
	GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476::get_offset_of_target_9(),
	GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476::get_offset_of_follower_10(),
	GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476::get_offset_of_trembleDeltaMin_11(),
	GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476::get_offset_of_trembleDeltaMax_12(),
	GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476::get_offset_of_bgTransform_13(),
	GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476::get_offset_of_faderOut_14(),
	GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476::get_offset_of_trembleDeltaX_15(),
	GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476::get_offset_of_trembleDeltaY_16(),
	GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476::get_offset_of_deltaX_17(),
	GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476::get_offset_of_sky_18(),
	GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476::get_offset_of_sky1_19(),
	GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476::get_offset_of_sunTransform_20(),
	GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476::get_offset_of_previousCameraPositionX_21(),
	GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476::get_offset_of_previousCameraPositionY_22(),
	GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476::get_offset_of_bord_23(),
	GameStateLevel_t84AF52399373535B9561E8FA60043B91459DA476::get_offset_of_heroStateMachine_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { sizeof (State_tC5BA2DC909C73846EB01A3E0806E5D6347274619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2644[5] = 
{
	State_tC5BA2DC909C73846EB01A3E0806E5D6347274619::get_offset_of_onEnterAction_4(),
	State_tC5BA2DC909C73846EB01A3E0806E5D6347274619::get_offset_of_onExitAction_5(),
	State_tC5BA2DC909C73846EB01A3E0806E5D6347274619::get_offset_of_previousState_6(),
	State_tC5BA2DC909C73846EB01A3E0806E5D6347274619::get_offset_of_stateMachine_7(),
	State_tC5BA2DC909C73846EB01A3E0806E5D6347274619::get_offset_of_sharedData_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { sizeof (StateMachine_tA4A39BBD7B24E4B1C7B690D3C6BE73E7023EFD01), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2645[1] = 
{
	StateMachine_tA4A39BBD7B24E4B1C7B690D3C6BE73E7023EFD01::get_offset_of_currentStateName_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { sizeof (CircleTriggerCollector_t712D0845A66149785F544AB14CC7A56A77CFC970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2646[2] = 
{
	CircleTriggerCollector_t712D0845A66149785F544AB14CC7A56A77CFC970::get_offset_of_radius_5(),
	CircleTriggerCollector_t712D0845A66149785F544AB14CC7A56A77CFC970::get_offset_of_offset_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { sizeof (Trigger_tB7343D9B8651521A7BC56AFD7F1BD6B05F65B7F3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2647[2] = 
{
	Trigger_tB7343D9B8651521A7BC56AFD7F1BD6B05F65B7F3::get_offset_of_onEnter_4(),
	Trigger_tB7343D9B8651521A7BC56AFD7F1BD6B05F65B7F3::get_offset_of_onExit_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { sizeof (TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2648[3] = 
{
	TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9::get_offset_of_enterActions_4(),
	TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9::get_offset_of_exitActions_5(),
	TriggerChecker_t70CB84C98DC6B14E23B37C8C4D74DF81161A3DD9::get_offset_of_collidersIn_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { sizeof (TriggerCollector_t9CCA0DBEFFA8AA280F464C98117DD43F72E8B23F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2649[1] = 
{
	TriggerCollector_t9CCA0DBEFFA8AA280F464C98117DD43F72E8B23F::get_offset_of_collidersIn_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { sizeof (TimeUtils_t75BA8DA980BA5920FF9F9B62D23E95A15972239F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (U3CDelayU3Ec__Iterator0_t6351117B347A09EF481280CB9742B31C2EAA2828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2651[5] = 
{
	U3CDelayU3Ec__Iterator0_t6351117B347A09EF481280CB9742B31C2EAA2828::get_offset_of_time_0(),
	U3CDelayU3Ec__Iterator0_t6351117B347A09EF481280CB9742B31C2EAA2828::get_offset_of_func_1(),
	U3CDelayU3Ec__Iterator0_t6351117B347A09EF481280CB9742B31C2EAA2828::get_offset_of_U24current_2(),
	U3CDelayU3Ec__Iterator0_t6351117B347A09EF481280CB9742B31C2EAA2828::get_offset_of_U24disposing_3(),
	U3CDelayU3Ec__Iterator0_t6351117B347A09EF481280CB9742B31C2EAA2828::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (U3CDelayFramesU3Ec__Iterator1_t862B5A2F6BD951EFD1E7A38ADCFA8BF4934E0DE9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2652[5] = 
{
	U3CDelayFramesU3Ec__Iterator1_t862B5A2F6BD951EFD1E7A38ADCFA8BF4934E0DE9::get_offset_of_frames_0(),
	U3CDelayFramesU3Ec__Iterator1_t862B5A2F6BD951EFD1E7A38ADCFA8BF4934E0DE9::get_offset_of_func_1(),
	U3CDelayFramesU3Ec__Iterator1_t862B5A2F6BD951EFD1E7A38ADCFA8BF4934E0DE9::get_offset_of_U24current_2(),
	U3CDelayFramesU3Ec__Iterator1_t862B5A2F6BD951EFD1E7A38ADCFA8BF4934E0DE9::get_offset_of_U24disposing_3(),
	U3CDelayFramesU3Ec__Iterator1_t862B5A2F6BD951EFD1E7A38ADCFA8BF4934E0DE9::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (LevelLoader_t528DC9F40D2C5040043E386207C89B1400AE0343), -1, sizeof(LevelLoader_t528DC9F40D2C5040043E386207C89B1400AE0343_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2653[2] = 
{
	LevelLoader_t528DC9F40D2C5040043E386207C89B1400AE0343::get_offset_of_faderIn_4(),
	LevelLoader_t528DC9F40D2C5040043E386207C89B1400AE0343_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { sizeof (Hero_t39258A796E56A51462429C62F9A05CB012E66B5D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2654[5] = 
{
	Hero_t39258A796E56A51462429C62F9A05CB012E66B5D::get_offset_of_walkVelocity_4(),
	Hero_t39258A796E56A51462429C62F9A05CB012E66B5D::get_offset_of_runVelocity_5(),
	Hero_t39258A796E56A51462429C62F9A05CB012E66B5D::get_offset_of_stateMachine_6(),
	Hero_t39258A796E56A51462429C62F9A05CB012E66B5D::get_offset_of_body_7(),
	Hero_t39258A796E56A51462429C62F9A05CB012E66B5D::get_offset_of_groundTrigger_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (HeroState_t4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2655[8] = 
{
	HeroState_t4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559::get_offset_of__hero_9(),
	HeroState_t4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559::get_offset_of__touchControls_10(),
	HeroState_t4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559::get_offset_of__crushTriggerCheckerLeft_11(),
	HeroState_t4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559::get_offset_of__crushTriggerHead_12(),
	HeroState_t4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559::get_offset_of__crushColliderLeft_13(),
	HeroState_t4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559::get_offset_of__body_14(),
	HeroState_t4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559::get_offset_of__bordTriggerChecker_15(),
	HeroState_t4CD0ECC837E324FABD6B7BA13BC7F8C0E2654559::get_offset_of__leaveGroundTriggerChecker_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { sizeof (HeroStateCrash_tA05D940D5513FD58DEF5C9174C48FA344FDDB873), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { sizeof (HeroStateInAir_t6BF4B686590D20FFAD10ED5B125E03FB2BEA55B5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2657[1] = 
{
	HeroStateInAir_t6BF4B686590D20FFAD10ED5B125E03FB2BEA55B5::get_offset_of__forceDown_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (HeroStateJump_t23602F3AF872DD888F6461617D9CFDB32CE7E189), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { sizeof (HeroStateLanded_t684EA939292822247AC6C7565F6D115BB396183A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { sizeof (HeroStateRun_t45494C32EB550F45009424A43F225FB62263656E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { sizeof (HeroStateWalk_t132A07E16CABB7DC4B19437499F95EDDCC24E5F4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { sizeof (Statics_t95A18A72EC5A85C490E5129598C5FFCD300070DC), -1, sizeof(Statics_t95A18A72EC5A85C490E5129598C5FFCD300070DC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2662[2] = 
{
	Statics_t95A18A72EC5A85C490E5129598C5FFCD300070DC_StaticFields::get_offset_of_currentLevel_4(),
	Statics_t95A18A72EC5A85C490E5129598C5FFCD300070DC_StaticFields::get_offset_of_sceneToSwitch_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (ColliderHelper_t04617AD58EF978484019FB3298FBD37B17159BEF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2663[3] = 
{
	ColliderHelper_t04617AD58EF978484019FB3298FBD37B17159BEF::get_offset_of_collisionShapeData_4(),
	ColliderHelper_t04617AD58EF978484019FB3298FBD37B17159BEF::get_offset_of_autoUpdateOnAwake_5(),
	ColliderHelper_t04617AD58EF978484019FB3298FBD37B17159BEF::get_offset_of_polygonCollider_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (CameraAndCarMovement_t75FA6CE2E014F8A189589F575D6EB7C038D2739B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2664[10] = 
{
	CameraAndCarMovement_t75FA6CE2E014F8A189589F575D6EB7C038D2739B::get_offset_of_cameraTransform_4(),
	CameraAndCarMovement_t75FA6CE2E014F8A189589F575D6EB7C038D2739B::get_offset_of_carTransform_5(),
	CameraAndCarMovement_t75FA6CE2E014F8A189589F575D6EB7C038D2739B::get_offset_of_frontWheel_6(),
	CameraAndCarMovement_t75FA6CE2E014F8A189589F575D6EB7C038D2739B::get_offset_of_backWheel_7(),
	CameraAndCarMovement_t75FA6CE2E014F8A189589F575D6EB7C038D2739B::get_offset_of_carCamera_8(),
	CameraAndCarMovement_t75FA6CE2E014F8A189589F575D6EB7C038D2739B::get_offset_of_maxZoom_9(),
	CameraAndCarMovement_t75FA6CE2E014F8A189589F575D6EB7C038D2739B::get_offset_of_minZoom_10(),
	CameraAndCarMovement_t75FA6CE2E014F8A189589F575D6EB7C038D2739B::get_offset_of_zoomSpeed_11(),
	CameraAndCarMovement_t75FA6CE2E014F8A189589F575D6EB7C038D2739B::get_offset_of_minZoomCameraY_12(),
	CameraAndCarMovement_t75FA6CE2E014F8A189589F575D6EB7C038D2739B::get_offset_of_maxZoomCameraY_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (RuntimeImport_t7CD3697F47C08BD16904ECDE7B6F6543803D19D8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2665[2] = 
{
	RuntimeImport_t7CD3697F47C08BD16904ECDE7B6F6543803D19D8::get_offset_of_svgData_4(),
	RuntimeImport_t7CD3697F47C08BD16904ECDE7B6F6543803D19D8::get_offset_of_meshFilter_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (CollisionShapeData_t09D2AF9F5D9E3B0C513049F97C117CF4EB9B2C6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2666[1] = 
{
	CollisionShapeData_t09D2AF9F5D9E3B0C513049F97C117CF4EB9B2C6B::get_offset_of_collisionPolygons_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (Polygon_t12BBDCB595FE6F844811B0E4FE7399BDDF4F77FC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2667[1] = 
{
	Polygon_t12BBDCB595FE6F844811B0E4FE7399BDDF4F77FC::get_offset_of_points_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (ConvexHullUtility_t9E7A2B2B8A2FE0D3AD40BD279D90816210158D85), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (CascadeContext_tD6A26BF562AC92D19625EDC2D5D625387A04384C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2669[3] = 
{
	CascadeContext_tD6A26BF562AC92D19625EDC2D5D625387A04384C::get_offset_of_graphicalAttributes_0(),
	CascadeContext_tD6A26BF562AC92D19625EDC2D5D625387A04384C::get_offset_of_transformAttributes_1(),
	CascadeContext_tD6A26BF562AC92D19625EDC2D5D625387A04384C::get_offset_of_clipStencil_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { sizeof (CircleElement_t6728507302B4FCFC11507336EACCB8C925E59C17), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2670[3] = 
{
	CircleElement_t6728507302B4FCFC11507336EACCB8C925E59C17::get_offset_of_cx_12(),
	CircleElement_t6728507302B4FCFC11507336EACCB8C925E59C17::get_offset_of_cy_13(),
	CircleElement_t6728507302B4FCFC11507336EACCB8C925E59C17::get_offset_of_r_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (ClipPathElement_t33F2F0C15FFA86FCE2BB3857AE271A01D7CA561E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2671[3] = 
{
	ClipPathElement_t33F2F0C15FFA86FCE2BB3857AE271A01D7CA561E::get_offset_of_localGraphicalAttributes_4(),
	ClipPathElement_t33F2F0C15FFA86FCE2BB3857AE271A01D7CA561E::get_offset_of_localTransformAttributes_5(),
	ClipPathElement_t33F2F0C15FFA86FCE2BB3857AE271A01D7CA561E::get_offset_of_stencil_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { sizeof (RecurseSVGElements_t7025179A40653E0F0B00540527D136A2C1D5A1F3), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (U3CGetStencilU3Ec__AnonStorey0_tC0F2C4FF08AE04C08BA27CD042D0043EAFF2E13B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2673[3] = 
{
	U3CGetStencilU3Ec__AnonStorey0_tC0F2C4FF08AE04C08BA27CD042D0043EAFF2E13B::get_offset_of_stencilClipper_0(),
	U3CGetStencilU3Ec__AnonStorey0_tC0F2C4FF08AE04C08BA27CD042D0043EAFF2E13B::get_offset_of_recurse_1(),
	U3CGetStencilU3Ec__AnonStorey0_tC0F2C4FF08AE04C08BA27CD042D0043EAFF2E13B::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (DefsElement_t89F1B8B79F84DBAEDDBF170EE2B324AF2730AD9C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { sizeof (EllipseElement_tBA913A69B5B46788D424D865C36E80416847159D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2675[4] = 
{
	EllipseElement_tBA913A69B5B46788D424D865C36E80416847159D::get_offset_of_cx_12(),
	EllipseElement_tBA913A69B5B46788D424D865C36E80416847159D::get_offset_of_cy_13(),
	EllipseElement_tBA913A69B5B46788D424D865C36E80416847159D::get_offset_of_rx_14(),
	EllipseElement_tBA913A69B5B46788D424D865C36E80416847159D::get_offset_of_ry_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (U3CMakeEllipsoidContourPointsU3Ec__AnonStorey0_t17222FE283B193808F145DC9CFA7BADA0346B784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2676[5] = 
{
	U3CMakeEllipsoidContourPointsU3Ec__AnonStorey0_t17222FE283B193808F145DC9CFA7BADA0346B784::get_offset_of_angle_0(),
	U3CMakeEllipsoidContourPointsU3Ec__AnonStorey0_t17222FE283B193808F145DC9CFA7BADA0346B784::get_offset_of_rx_1(),
	U3CMakeEllipsoidContourPointsU3Ec__AnonStorey0_t17222FE283B193808F145DC9CFA7BADA0346B784::get_offset_of_cx_2(),
	U3CMakeEllipsoidContourPointsU3Ec__AnonStorey0_t17222FE283B193808F145DC9CFA7BADA0346B784::get_offset_of_ry_3(),
	U3CMakeEllipsoidContourPointsU3Ec__AnonStorey0_t17222FE283B193808F145DC9CFA7BADA0346B784::get_offset_of_cy_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2677[12] = 
{
	GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D::get_offset_of_opacity_0(),
	GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D::get_offset_of_useFill_1(),
	GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D::get_offset_of_fillColor_2(),
	GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D::get_offset_of_fillOpacity_3(),
	GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D::get_offset_of_useStroke_4(),
	GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D::get_offset_of_strokeWidth_5(),
	GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D::get_offset_of_strokeColor_6(),
	GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D::get_offset_of_strokeOpacity_7(),
	GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D::get_offset_of_strokeMiterLimit_8(),
	GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D::get_offset_of_fillRule_9(),
	GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D::get_offset_of_clipPath_10(),
	GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D::get_offset_of_clipRule_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D), -1, sizeof(GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2678[8] = 
{
	GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D_StaticFields::get_offset_of_clipperCoordinateScale_4(),
	GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D::get_offset_of_localGraphicalAttributes_5(),
	GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D::get_offset_of_localTransformAttributes_6(),
	GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D::get_offset_of_contourPaths_7(),
	GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D::get_offset_of_stencilTree_8(),
	GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D::get_offset_of_fillTree_9(),
	GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D::get_offset_of_openStrokeTree_10(),
	GraphicalElement_tDCF600D6023CE5078883348F9B0A76C990D3131D::get_offset_of_closedStrokeTree_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (PolyTreeRecurse_t1392592EADEA5FBAC613340FE5FDE2518273715A), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (ContourPath_t9DAB1CD8D360AE05DEB804F7BB49378FC5449BA6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2680[3] = 
{
	ContourPath_t9DAB1CD8D360AE05DEB804F7BB49378FC5449BA6::get_offset_of_closed_0(),
	ContourPath_t9DAB1CD8D360AE05DEB804F7BB49378FC5449BA6::get_offset_of_path_1(),
	ContourPath_t9DAB1CD8D360AE05DEB804F7BB49378FC5449BA6::get_offset_of_clipperPath_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (U3CApplyTransformationU3Ec__AnonStorey0_t7D29C559666E3163C110AB9F4F5BCF4C8E66E9C3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2681[2] = 
{
	U3CApplyTransformationU3Ec__AnonStorey0_t7D29C559666E3163C110AB9F4F5BCF4C8E66E9C3::get_offset_of_transformation_0(),
	U3CApplyTransformationU3Ec__AnonStorey0_t7D29C559666E3163C110AB9F4F5BCF4C8E66E9C3::get_offset_of_recurse_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (GroupElement_t8F6CB728CEC1D10D2E14197B062FF41F02E1CC72), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2682[2] = 
{
	GroupElement_t8F6CB728CEC1D10D2E14197B062FF41F02E1CC72::get_offset_of_localGraphicalAttributes_4(),
	GroupElement_t8F6CB728CEC1D10D2E14197B062FF41F02E1CC72::get_offset_of_localTransformAttributes_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2683[3] = 
{
	SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7::get_offset_of_rootElement_0(),
	SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7::get_offset_of_importSettings_1(),
	SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7::get_offset_of_elementsById_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2684[4] = 
{
	SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888::get_offset_of_id_0(),
	SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888::get_offset_of_ownerDocument_1(),
	SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888::get_offset_of_parent_2(),
	SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888::get_offset_of_children_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (LineElement_tA17BDE9B13B5BBD50226B780E30B61E53AF93F73), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2688[4] = 
{
	LineElement_tA17BDE9B13B5BBD50226B780E30B61E53AF93F73::get_offset_of_x1_12(),
	LineElement_tA17BDE9B13B5BBD50226B780E30B61E53AF93F73::get_offset_of_y1_13(),
	LineElement_tA17BDE9B13B5BBD50226B780E30B61E53AF93F73::get_offset_of_x2_14(),
	LineElement_tA17BDE9B13B5BBD50226B780E30B61E53AF93F73::get_offset_of_y2_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (PathElement_tDAFCC03770733653E4E344DAD73CC3E7AF9D7025), -1, sizeof(PathElement_tDAFCC03770733653E4E344DAD73CC3E7AF9D7025_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2689[2] = 
{
	PathElement_tDAFCC03770733653E4E344DAD73CC3E7AF9D7025::get_offset_of_subPaths_12(),
	PathElement_tDAFCC03770733653E4E344DAD73CC3E7AF9D7025_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (MakePathPointDelegate_t9450E94A77BBF3351AE2573993C7FB4A1F10ECD0), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (SubPath_t96958462F98C903C3B5FE837612402A70921E627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2691[2] = 
{
	SubPath_t96958462F98C903C3B5FE837612402A70921E627::get_offset_of_closed_0(),
	SubPath_t96958462F98C903C3B5FE837612402A70921E627::get_offset_of_path_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (PathComponent_t34B34D47FB4CDCAA01D53BECAC8F852ADC801020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2692[10] = 
{
	PathComponent_t34B34D47FB4CDCAA01D53BECAC8F852ADC801020::get_offset_of_segmentType_0(),
	PathComponent_t34B34D47FB4CDCAA01D53BECAC8F852ADC801020::get_offset_of_pos_1(),
	PathComponent_t34B34D47FB4CDCAA01D53BECAC8F852ADC801020::get_offset_of_startCurvePos_2(),
	PathComponent_t34B34D47FB4CDCAA01D53BECAC8F852ADC801020::get_offset_of_endCurvePos_3(),
	PathComponent_t34B34D47FB4CDCAA01D53BECAC8F852ADC801020::get_offset_of_useStartCurvePos_4(),
	PathComponent_t34B34D47FB4CDCAA01D53BECAC8F852ADC801020::get_offset_of_useEndCurvePos_5(),
	PathComponent_t34B34D47FB4CDCAA01D53BECAC8F852ADC801020::get_offset_of_arcRadius_6(),
	PathComponent_t34B34D47FB4CDCAA01D53BECAC8F852ADC801020::get_offset_of_arcRotation_7(),
	PathComponent_t34B34D47FB4CDCAA01D53BECAC8F852ADC801020::get_offset_of_arcLarge_8(),
	PathComponent_t34B34D47FB4CDCAA01D53BECAC8F852ADC801020::get_offset_of_arcSweep_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (SegmentType_t50BD7FD0D2405225267ADAFB03820A0824E04926)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2693[5] = 
{
	SegmentType_t50BD7FD0D2405225267ADAFB03820A0824E04926::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (U3CBuildShapeU3Ec__AnonStorey0_tC188B4701BAECA5BEAADBC952D3F269E39C2CB30), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2694[7] = 
{
	U3CBuildShapeU3Ec__AnonStorey0_tC188B4701BAECA5BEAADBC952D3F269E39C2CB30::get_offset_of_theta1_0(),
	U3CBuildShapeU3Ec__AnonStorey0_tC188B4701BAECA5BEAADBC952D3F269E39C2CB30::get_offset_of_dtheta_1(),
	U3CBuildShapeU3Ec__AnonStorey0_tC188B4701BAECA5BEAADBC952D3F269E39C2CB30::get_offset_of_rho_2(),
	U3CBuildShapeU3Ec__AnonStorey0_tC188B4701BAECA5BEAADBC952D3F269E39C2CB30::get_offset_of_rx_3(),
	U3CBuildShapeU3Ec__AnonStorey0_tC188B4701BAECA5BEAADBC952D3F269E39C2CB30::get_offset_of_ry_4(),
	U3CBuildShapeU3Ec__AnonStorey0_tC188B4701BAECA5BEAADBC952D3F269E39C2CB30::get_offset_of_cx_5(),
	U3CBuildShapeU3Ec__AnonStorey0_tC188B4701BAECA5BEAADBC952D3F269E39C2CB30::get_offset_of_cy_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (U3CBuildShapeU3Ec__AnonStorey1_t578BE985F5CE829848E84EB8798614B40AA7B534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2695[4] = 
{
	U3CBuildShapeU3Ec__AnonStorey1_t578BE985F5CE829848E84EB8798614B40AA7B534::get_offset_of_p_0_0(),
	U3CBuildShapeU3Ec__AnonStorey1_t578BE985F5CE829848E84EB8798614B40AA7B534::get_offset_of_p_1_1(),
	U3CBuildShapeU3Ec__AnonStorey1_t578BE985F5CE829848E84EB8798614B40AA7B534::get_offset_of_p_2_2(),
	U3CBuildShapeU3Ec__AnonStorey1_t578BE985F5CE829848E84EB8798614B40AA7B534::get_offset_of_p_3_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { sizeof (U3CBuildShapeU3Ec__AnonStorey2_t432E5413D51C5401FAA8E6CB5A2E4E11022F605B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2696[3] = 
{
	U3CBuildShapeU3Ec__AnonStorey2_t432E5413D51C5401FAA8E6CB5A2E4E11022F605B::get_offset_of_p_0_0(),
	U3CBuildShapeU3Ec__AnonStorey2_t432E5413D51C5401FAA8E6CB5A2E4E11022F605B::get_offset_of_p_1_1(),
	U3CBuildShapeU3Ec__AnonStorey2_t432E5413D51C5401FAA8E6CB5A2E4E11022F605B::get_offset_of_p_2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { sizeof (PolygonElement_t8AFB041E1A75C7DB015FF9F5CE41C80DABB7098D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2697[1] = 
{
	PolygonElement_t8AFB041E1A75C7DB015FF9F5CE41C80DABB7098D::get_offset_of_points_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (PolylineElement_t5AE0317375BCE446D91905B2CCBFF8386324F5B1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2698[1] = 
{
	PolylineElement_t5AE0317375BCE446D91905B2CCBFF8386324F5B1::get_offset_of_points_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (RectElement_t9B126A04ED3550785335C61B6F2524E442CA1DD0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2699[4] = 
{
	RectElement_t9B126A04ED3550785335C61B6F2524E442CA1DD0::get_offset_of_x_12(),
	RectElement_t9B126A04ED3550785335C61B6F2524E442CA1DD0::get_offset_of_y_13(),
	RectElement_t9B126A04ED3550785335C61B6F2524E442CA1DD0::get_offset_of_width_14(),
	RectElement_t9B126A04ED3550785335C61B6F2524E442CA1DD0::get_offset_of_height_15(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
