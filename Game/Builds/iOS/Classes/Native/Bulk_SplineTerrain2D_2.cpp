﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// Kolibri2d.BezierSpline
struct BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE;
// Kolibri2d.BezierSpline/OnRefreshCallback
struct OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300;
// Kolibri2d.BezierSplineQuality
struct BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF;
// Kolibri2d.BezierSpline[]
struct BezierSplineU5BU5D_tD9962D7525021265D09A9C68F754780C16917F1C;
// Kolibri2d.MaterialTextureInfo[]
struct MaterialTextureInfoU5BU5D_tAEBAFCAFCD888B2D9CBD6B9739EE67FD30B3F264;
// Kolibri2d.PhysicsSettings
struct PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190;
// Kolibri2d.SplineColliderCreator
struct SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A;
// Kolibri2d.SplineColliderSettings
struct SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4;
// Kolibri2d.SplineData
struct SplineData_t97926CA36C3A0FF54D20743F14165941B0C11747;
// Kolibri2d.SplineModifierWithCallbacks
struct SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232;
// Kolibri2d.SplineSegmentInfo[]
struct SplineSegmentInfoU5BU5D_t624E5D3182FB6F8E0866F27EFE82E878F58B40B6;
// Kolibri2d.SplineTerrain2DCreator
struct SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1;
// Kolibri2d.SplineWater
struct SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8;
// Kolibri2d.SplineWater/WaterEvent
struct WaterEvent_t414CD81AE9F28B656088F21ECC14F030B1FDA33B;
// Kolibri2d.Terrain2D
struct Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B;
// Kolibri2d.Terrain2D/Terrain2DMaterialHolder
struct Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F;
// Kolibri2d.Terrain2D/Terrain2DMaterialHolder[]
struct Terrain2DMaterialHolderU5BU5D_t16F721941294EA9C048940BD138D26F87D871464;
// Kolibri2d.Terrain2DArchitecture
struct Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A;
// Kolibri2d.Terrain2DArchitectureAngles
struct Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208;
// Kolibri2d.Terrain2DMaterial
struct Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D;
// Kolibri2d.Terrain2DMaterial/CornerSprites2
struct CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703;
// Kolibri2d.Terrain2DMaterial/SortingLayerOptions2
struct SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE;
// Kolibri2d.Terrain2DMaterial/SortingZ_3D_Options
struct SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F;
// Kolibri2d.TerrainType[]
struct TerrainTypeU5BU5D_t3DA5B2A1F9D3FF051682ECFF02704F8EDE40122F;
// Kolibri2d.Utils
struct Utils_t6B1F49395E2728FC7087CA82F420C8183CECBD71;
// Kolibri2d.WaterTestPanel
struct WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80;
// System.Char[]
struct CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744;
// System.Collections.Generic.Dictionary`2/Transform`1<Kolibri2d.TerrainType,Kolibri2d.MaterialTextureInfo,System.Collections.DictionaryEntry>
struct Transform_1_t4C073460CE5CB06CC2AE739773A9BC6FE183BA46;
// System.Collections.Generic.Dictionary`2/Transform`1<System.UInt32,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t7EA7B7A2427C9EEE9B079C56FC2060C7C7B9C707;
// System.Collections.Generic.Dictionary`2<Kolibri2d.TerrainType,Kolibri2d.MaterialTextureInfo>
struct Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A;
// System.Collections.Generic.Dictionary`2<Kolibri2d.TerrainType,Kolibri2d.PhysicsSettings>
struct Dictionary_2_t99ADF3EF31BE384D1A13236F275C436B11310513;
// System.Collections.Generic.Dictionary`2<System.UInt32,System.Int32>
struct Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F;
// System.Collections.Generic.Dictionary`2<UnityEngine.Collider2D,System.Collections.Generic.List`1<System.Int32>>
struct Dictionary_2_t97FE799FB5630EA306A1F53E2CA192562EF2E217;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Color>
struct IEnumerable_1_t0B3CE9E59930065B8B846BAF454DC8FAC9B2F7C9;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>
struct IEnumerable_1_t022DEB6115EB42DC022B7E7F92C994BC074724E5;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>
struct IEnumerable_1_t85CDFDE61E9ED5EC59179D4466DF210DD727EEEC;
// System.Collections.Generic.IEqualityComparer`1<Kolibri2d.TerrainType>
struct IEqualityComparer_1_t0D49C0F691E53F3CE825C90C3B07F6151BA398F5;
// System.Collections.Generic.IEqualityComparer`1<System.UInt32>
struct IEqualityComparer_1_tE6F7A7BC5BBBE6BE5A8707118223CA6819AAC3BF;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_tA58A56CD6B5AD6B2DF5F4934A46CEC9F681B71AD;
// System.Collections.Generic.List`1<Kolibri2d.BezierSpline/BezierPoint>
struct List_1_tD5F01382C85482BD3A1133E16C27709D77F65384;
// System.Collections.Generic.List`1<Kolibri2d.BezierSpline/PointData>
struct List_1_t96B06A7F290D61DC5641A89066DC34043C6F5286;
// System.Collections.Generic.List`1<Kolibri2d.SplineSegmentInfo>
struct List_1_t517344AED92C69B6645F0ABFA470063D220BACCD;
// System.Collections.Generic.List`1<Kolibri2d.Terrain2D/Terrain2DMaterialHolder>
struct List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t4A961510635950236678F1B3B2436ECCAF28713A;
// System.Collections.Generic.List`1<System.Object>
struct List_1_tE72A517BD14F52539FF78EA90F58D1387FEED660;
// System.Collections.Generic.List`1<System.Single>
struct List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E;
// System.Collections.Generic.List`1<UnityEngine.Material>
struct List_1_t56B616621385F653B408094FBDFA5DEC6C4B47EA;
// System.Collections.Generic.List`1<UnityEngine.Mesh>
struct List_1_tBE402A53CD5331E0CB483C591CFDC8C6827F35B8;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Int32[]
struct Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074;
// System.Object[]
struct ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t778922F6A5AACC38C8F326D3338A91D6D72B11E2;
// System.Single[]
struct SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5;
// System.String
struct String_t;
// System.UInt32[]
struct UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5;
// System.Void
struct Void_tDB81A15FA2AB53E2401A76B745D215397B29F783;
// UnityEngine.Behaviour
struct Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.Events.UnityEvent`1<System.Single>
struct UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067;
// UnityEngine.LineRenderer
struct LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MeshFilter
struct MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0;
// UnityEngine.MeshRenderer
struct MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.PolygonCollider2D
struct PolygonCollider2D_t360CC66FD7BAF5F4FADE54EB23E58E95D0B8CD81;
// UnityEngine.PolygonCollider2D[]
struct PolygonCollider2DU5BU5D_t740B9DF43CFB32BA191190CEA2770604FF8C0968;
// UnityEngine.RenderTexture
struct RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Renderer[]
struct RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF;
// UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;

extern RuntimeClass* CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F_il2cpp_TypeInfo_var;
extern RuntimeClass* GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var;
extern RuntimeClass* Graphics_t6FB7A5D4561F3AB3C34BF334BB0BD8061BE763B1_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t4A961510635950236678F1B3B2436ECCAF28713A_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t517344AED92C69B6645F0ABFA470063D220BACCD_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_il2cpp_TypeInfo_var;
extern RuntimeClass* Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var;
extern RuntimeClass* MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
extern RuntimeClass* Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var;
extern RuntimeClass* RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6_il2cpp_TypeInfo_var;
extern RuntimeClass* SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE_il2cpp_TypeInfo_var;
extern RuntimeClass* SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F_il2cpp_TypeInfo_var;
extern RuntimeClass* SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A_il2cpp_TypeInfo_var;
extern RuntimeClass* SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1_il2cpp_TypeInfo_var;
extern RuntimeClass* Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_il2cpp_TypeInfo_var;
extern RuntimeClass* Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F_il2cpp_TypeInfo_var;
extern RuntimeClass* Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D_il2cpp_TypeInfo_var;
extern RuntimeClass* Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisBezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_m997621D49F9FF6305B153B07112BDDE09A165DC1_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisSplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A_m434AAC7C9A971A8416BB64D702DA25F199DFB2C0_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisSplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8_m2CF9090BA6AB33D29C9FC86E7FE19E20187241F8_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_mDCE3CAF8CAFEBD13FB38A51C069F27896353ECB5_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m230D107B4D3C342ECBC961100D82015C14534FCC_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m7E5325C12ECF67A2A855BBDC04968EE0CC66ACF1_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m9266E02BD88994992C4A72CDE2733990EEE6C9B8_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Count_mB1BF016039CF52477957DBB0568653081D14612E_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_mCECAC44EF2E9AE18023E7FDAD8CC268588065CE3_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_mF1FB9229F3B369C779CEF8B43902E24D6C3FCD3D_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_set_Item_m85881E2C19054C55C36474E5876AC2AB23552E0B_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m640FFEF2E9A18C15D423069973168AD40E10FBFB_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_mCB51DC072567AD26ACE9160F23683B7D32B7190B_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_mCDA3AD77115D33F3528CD546C85CA20239DD4294_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m168734AAA4869F1578DF4D4538B74BFF12522F68_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m3D433FA2C3588249702F14524E89E464EE14EE13_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_mFB1EBCAC4E921F22042B0BDB6B954B7A09589454_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m063115BDDF8FC2B26C79A4AD2BCB44A7661BBA28_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m495068DBBE452DB06B67916B5D28B026BD336204_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_mAB079301955D01523AB6CD62D400632D218B265D_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD202DDB933765AC3989986C7FA3C14E6E9A191EC_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponentsInChildren_TisBezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_m968410DEC000954AFE3CA0F676596D8F14B8A570_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_m8BA044694988F70856600001893D4CB591BD5B29_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m00EB6B0859FF6BCA9F89EF1FE572855EAA143F8F_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m649A438448972F32C1D67F10BBFB80D0CF81EF94_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_mF10FF505AE13DE269187B75AC591D0D47188FF0B_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m9536E635B50A2D8549D7615CC042AF1D64AEF454_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_mA6AD60947F39CE54DA7D237D278C8948D5B2C331_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_mA9C0CCE2B4A25A7A18800BE1E1E3DD78AE5E9515_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveAt_mDDB8509DB8FA2CC2FB3F8A9A13C8028CD8185109_RuntimeMethod_var;
extern const RuntimeMethod* List_1_ToArray_m062BF9DC0570B6EEC207AAEDDEC602FB2EB3F9BA_RuntimeMethod_var;
extern const RuntimeMethod* List_1_ToArray_mB9E07F625DF343AED13593FB79319A4ADFC02AB7_RuntimeMethod_var;
extern const RuntimeMethod* List_1_ToArray_mE3701008FD26BBC8184384CA22962898157F7BAF_RuntimeMethod_var;
extern const RuntimeMethod* List_1_ToArray_mE9F8DDC7219DCC0198F4A265DBD77BC4307FA6F4_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m0E7CD0EBE7EA0CCD1DDA59B1F34C56CDB1F79827_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m6A5DF174C63F24D00FC3CCA142FF30E9FE3B0B84_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m6DB64BAFEC79776AB013124B548E074CAA2FAD95_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m80FC80854B1AD97640F2B64DE9B250A3D8DAB145_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m8E52D55D4C56236ED57F6275B7E7EDD692F55961_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m9E8734104EA5EA636BBA9DD4E5E56DC6C7C1B063_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_mCAB49C9A39B98BC7B6D7CEDCA6AE260556381C09_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m1603A7C79B587156F949A0561C5CE4635B4AD255_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m9E6BB40B113CEAFD4EA6A2BD7FCEB7259C20561D_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_mA31EF7A05D93A92566AE15D88053820A2E85F1AF_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_mB725E23AF0513F5A32B32BBC9C2471097DE235C4_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B_RuntimeMethod_var;
extern const RuntimeMethod* List_1_set_Item_mCAC53DD791D8E571F6A22EE17D615DD70B9C6619_RuntimeMethod_var;
extern const RuntimeMethod* ScriptableObject_CreateInstance_TisTerrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_m07B85156417883C4AB46AA7039D2CF9CEC71B80F_RuntimeMethod_var;
extern const RuntimeMethod* UnityEvent_1__ctor_m0664E6A8259E48D52CF88851228B1DC47F751C48_RuntimeMethod_var;
extern const uint32_t CornerSprites2_GetCornerOffset_mE32AE7EB9004D2EAEC9BB00049F278F24CEF6320_MetadataUsageId;
extern const uint32_t MeshHelper2_CleanUp_mBC2EA65D0BD5F6AA5D87F7C6259B63330A5B62B9_MetadataUsageId;
extern const uint32_t MeshHelper2_GetNewVertex4_m1B46BFC32053E23F3C9F90347AAF3EECD46B715F_MetadataUsageId;
extern const uint32_t MeshHelper2_GetNewVertex9_m83649A9E4437ABA844B158098FB1B65272FB3D8C_MetadataUsageId;
extern const uint32_t MeshHelper2_InitArrays_mDBEB2310C61A2DB7A2EDC56A961F92F7CDCF1681_MetadataUsageId;
extern const uint32_t MeshHelper2_Subdivide4_m222D393186AD66CD72DF6F297E2C38AF69EC8E90_MetadataUsageId;
extern const uint32_t MeshHelper2_Subdivide9_m4FB982D43A206A43AE8BF55805987803940FBE13_MetadataUsageId;
extern const uint32_t Terrain2DArchitectureAngles_SaveCurrentAsDefaultAngle_m078AEFEDAA28049E4A2EFC41C1B83B263418B22E_MetadataUsageId;
extern const uint32_t Terrain2DArchitectureAngles_get_DefaultAngle_m11AAFDCC6F2D57CED18469D56B6C678B9D75979F_MetadataUsageId;
extern const uint32_t Terrain2DArchitecture_ApplySegmentGuessOnLine_m3B0094A0015B70D9CB31C6AEE77CE6F6C3350B9E_MetadataUsageId;
extern const uint32_t Terrain2DArchitecture_CalculateSegmentsInfo_m7E9A4EA786517CBF0C45CDCEA214D0767751F460_MetadataUsageId;
extern const uint32_t Terrain2DArchitecture_GetDirectionFromLength01_m16B0CF93290E47DEBFFE522DBF4C05C057B471C4_MetadataUsageId;
extern const uint32_t Terrain2DArchitecture_GetSegmentsInfoAndMergeEnds_mA4D4C7532720F4EFCA6C9C1B9885A02FADB51522_MetadataUsageId;
extern const uint32_t Terrain2DArchitecture_GetTerrainTypeFromSplineLength01_m21F1F20D19B6D1A2D9F55CCFB147671FF099731D_MetadataUsageId;
extern const uint32_t Terrain2DArchitecture_RefreshIfMissingData_m7D08ADA747580A063BA739D33732CFB36F90A5F7_MetadataUsageId;
extern const uint32_t Terrain2DArchitecture_Refresh_m46793DC53A31B24651E22531A2D201889CD13565_MetadataUsageId;
extern const uint32_t Terrain2DArchitecture__ctor_m4151CE5C2A2172A142614E734327EEDB1247CD3F_MetadataUsageId;
extern const uint32_t Terrain2DMaterial_GetSpriteFromTerrainType_mBD2DE70D633EC4156E2A6D845EC2CEA03AFEC661_MetadataUsageId;
extern const uint32_t Terrain2DMaterial_RefreshIfMissingData_m7CACBD5F7AD5D5BBBA21AD4B2CD65E51A9DC77E8_MetadataUsageId;
extern const uint32_t Terrain2DMaterial_Refresh_mD6A2F2D44B21F6CC68F65957C50A71065DE64AFC_MetadataUsageId;
extern const uint32_t Terrain2DMaterial__ctor_mDAEBAA185FC1090745CB30C7A3857C5E64C66AA8_MetadataUsageId;
extern const uint32_t Terrain2D_ClearAll_m40A08E96E6E9ED3C435653E59D15393811EA1B29_MetadataUsageId;
extern const uint32_t Terrain2D_ContainsEnabledTerrainMaterial_m8A1FD92C1A1D4B25A8B82D4CDEDA48CB95DD8B6C_MetadataUsageId;
extern const uint32_t Terrain2D_ContainsTerrainMaterial_m159B1951A499694BB61D9B2C791676D99B2D4B80_MetadataUsageId;
extern const uint32_t Terrain2D_RefreshColliders_m2CD6434AC10F2F0369129AE06C3936F1A6BA15E4_MetadataUsageId;
extern const uint32_t Terrain2D_Reset_mCE35946462E22190DD164BDF64E03C8A263DA43F_MetadataUsageId;
extern const uint32_t Terrain2D_SetCreator_m5DF018BBE08473E58C9FC42A45076CB16A6DAA4D_MetadataUsageId;
extern const uint32_t Terrain2D__ctor_mCF2695B04E60C4BA2D809A07720B7DDA69B1B55B_MetadataUsageId;
extern const uint32_t Utils_ArePointsClockwise_m377B93EB2228C4F2B0E5D84147DE0F032A49C42D_MetadataUsageId;
extern const uint32_t Utils_CreateOrFindChildNode_m7F0060AEB5544F12EAF507A003F170E6A6FA3BF2_MetadataUsageId;
extern const uint32_t Utils_CropTexture2D_m9A361D3C4C7CF74DBF59F6BB5050268B452ADE94_MetadataUsageId;
extern const uint32_t Utils_GetAngleABC_mE43F92353A13E1D3011FDE80954F05B69EC60BDC_MetadataUsageId;
extern const uint32_t Utils_GetAngleBetweenSplineHelpers_mED4FE688F3A8BAA88AFA9A807978D6F44291C73B_MetadataUsageId;
extern const uint32_t Utils_GetAngleFromDir_m62A41F885D74A5155D0E4684783672982D7CB8B3_MetadataUsageId;
extern const uint32_t Utils_GetBoundsFromPoints_m336ABC40128A0F623E3AB290DA0A425B707E2796_MetadataUsageId;
extern const uint32_t Utils_GetBounds_mF3CC88CAFD5D87D5B845757575247F0324B0D38B_MetadataUsageId;
extern const uint32_t Utils_GetDirFromPoints_mE9D1FC9BF146E38D7A53FD21E2A8C34F4B08F16B_MetadataUsageId;
extern const uint32_t Utils_GetNormalsFromPositions_m2D293F791AEB966D5D2530D1C05CD9416E668041_MetadataUsageId;
extern const uint32_t Utils_GetPoints2DFromVector_m90A81A570C3D07DB970F0342572FFFCF5C53A51F_MetadataUsageId;
extern const uint32_t Utils_GetPoints2DFromVector_mA4EA2CFA4980E6B1B24739C2B26DC00A9B2F0AAB_MetadataUsageId;
extern const uint32_t Utils_GetUVsFromPointsAndBounds_mA2F9A9B0BF41EE9854F6E9C53B8F58CAC6BD31E7_MetadataUsageId;
extern const uint32_t Utils_Intersect_m47DF229EA539C283BF8A7B9F29680696F466951C_MetadataUsageId;
extern const uint32_t Utils_IsZero_m4B8A254B0F87897812D1DA9263A8CE9BFD9A8FD9_MetadataUsageId;
extern const uint32_t Utils_LineSegementsIntersect_m201115D61811BDC929D3B3B5168662F6CBCF4FBA_MetadataUsageId;
extern const uint32_t WaterEvent__ctor_mEA2A570BF2B1212229659F6F9844F4EE32429FB0_MetadataUsageId;
extern const uint32_t WaterTestPanel_Awake_mF1CF8CB0258A414CFBD28BBB13E0BBF92691A1D8_MetadataUsageId;
extern const uint32_t WaterTestPanel_OnEnable_mD5A7C3D276F35B616C81AC47EC94F40F874570F2_MetadataUsageId;

struct BezierSplineU5BU5D_tD9962D7525021265D09A9C68F754780C16917F1C;
struct Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074;
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
struct RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF;
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef SPLINECOLLIDERSETTINGS_T0665313F90EDA2B7B57385855A06973EA15213A4_H
#define SPLINECOLLIDERSETTINGS_T0665313F90EDA2B7B57385855A06973EA15213A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineColliderSettings
struct  SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4  : public RuntimeObject
{
public:
	// Kolibri2d.PhysicsSettings Kolibri2d.SplineColliderSettings::polygonCollider
	PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * ___polygonCollider_1;
	// Kolibri2d.PhysicsSettings Kolibri2d.SplineColliderSettings::edgeCollider
	PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * ___edgeCollider_2;
	// Kolibri2d.PhysicsSettings Kolibri2d.SplineColliderSettings::groundCollider
	PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * ___groundCollider_3;
	// Kolibri2d.PhysicsSettings Kolibri2d.SplineColliderSettings::wallCollider
	PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * ___wallCollider_4;
	// Kolibri2d.PhysicsSettings Kolibri2d.SplineColliderSettings::ceilingCollider
	PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * ___ceilingCollider_5;
	// System.Collections.Generic.Dictionary`2<Kolibri2d.TerrainType,Kolibri2d.PhysicsSettings> Kolibri2d.SplineColliderSettings::physicsSettingsByTerrainType
	Dictionary_2_t99ADF3EF31BE384D1A13236F275C436B11310513 * ___physicsSettingsByTerrainType_6;

public:
	inline static int32_t get_offset_of_polygonCollider_1() { return static_cast<int32_t>(offsetof(SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4, ___polygonCollider_1)); }
	inline PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * get_polygonCollider_1() const { return ___polygonCollider_1; }
	inline PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 ** get_address_of_polygonCollider_1() { return &___polygonCollider_1; }
	inline void set_polygonCollider_1(PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * value)
	{
		___polygonCollider_1 = value;
		Il2CppCodeGenWriteBarrier((&___polygonCollider_1), value);
	}

	inline static int32_t get_offset_of_edgeCollider_2() { return static_cast<int32_t>(offsetof(SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4, ___edgeCollider_2)); }
	inline PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * get_edgeCollider_2() const { return ___edgeCollider_2; }
	inline PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 ** get_address_of_edgeCollider_2() { return &___edgeCollider_2; }
	inline void set_edgeCollider_2(PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * value)
	{
		___edgeCollider_2 = value;
		Il2CppCodeGenWriteBarrier((&___edgeCollider_2), value);
	}

	inline static int32_t get_offset_of_groundCollider_3() { return static_cast<int32_t>(offsetof(SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4, ___groundCollider_3)); }
	inline PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * get_groundCollider_3() const { return ___groundCollider_3; }
	inline PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 ** get_address_of_groundCollider_3() { return &___groundCollider_3; }
	inline void set_groundCollider_3(PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * value)
	{
		___groundCollider_3 = value;
		Il2CppCodeGenWriteBarrier((&___groundCollider_3), value);
	}

	inline static int32_t get_offset_of_wallCollider_4() { return static_cast<int32_t>(offsetof(SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4, ___wallCollider_4)); }
	inline PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * get_wallCollider_4() const { return ___wallCollider_4; }
	inline PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 ** get_address_of_wallCollider_4() { return &___wallCollider_4; }
	inline void set_wallCollider_4(PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * value)
	{
		___wallCollider_4 = value;
		Il2CppCodeGenWriteBarrier((&___wallCollider_4), value);
	}

	inline static int32_t get_offset_of_ceilingCollider_5() { return static_cast<int32_t>(offsetof(SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4, ___ceilingCollider_5)); }
	inline PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * get_ceilingCollider_5() const { return ___ceilingCollider_5; }
	inline PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 ** get_address_of_ceilingCollider_5() { return &___ceilingCollider_5; }
	inline void set_ceilingCollider_5(PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * value)
	{
		___ceilingCollider_5 = value;
		Il2CppCodeGenWriteBarrier((&___ceilingCollider_5), value);
	}

	inline static int32_t get_offset_of_physicsSettingsByTerrainType_6() { return static_cast<int32_t>(offsetof(SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4, ___physicsSettingsByTerrainType_6)); }
	inline Dictionary_2_t99ADF3EF31BE384D1A13236F275C436B11310513 * get_physicsSettingsByTerrainType_6() const { return ___physicsSettingsByTerrainType_6; }
	inline Dictionary_2_t99ADF3EF31BE384D1A13236F275C436B11310513 ** get_address_of_physicsSettingsByTerrainType_6() { return &___physicsSettingsByTerrainType_6; }
	inline void set_physicsSettingsByTerrainType_6(Dictionary_2_t99ADF3EF31BE384D1A13236F275C436B11310513 * value)
	{
		___physicsSettingsByTerrainType_6 = value;
		Il2CppCodeGenWriteBarrier((&___physicsSettingsByTerrainType_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINECOLLIDERSETTINGS_T0665313F90EDA2B7B57385855A06973EA15213A4_H
#ifndef SPLINETERRAIN2DCREATOR_TB376515DA574676086ACA2E8C0914DDB27FDFBD1_H
#define SPLINETERRAIN2DCREATOR_TB376515DA574676086ACA2E8C0914DDB27FDFBD1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineTerrain2DCreator
struct  SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1  : public RuntimeObject
{
public:
	// Kolibri2d.BezierSpline Kolibri2d.SplineTerrain2DCreator::spline
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * ___spline_0;
	// Kolibri2d.Terrain2DArchitecture Kolibri2d.SplineTerrain2DCreator::arch
	Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * ___arch_1;
	// System.Collections.Generic.List`1<Kolibri2d.Terrain2D/Terrain2DMaterialHolder> Kolibri2d.SplineTerrain2DCreator::materialHolders
	List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 * ___materialHolders_2;
	// Kolibri2d.Terrain2D Kolibri2d.SplineTerrain2DCreator::terrain
	Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B * ___terrain_3;
	// System.Collections.Generic.List`1<UnityEngine.Mesh> Kolibri2d.SplineTerrain2DCreator::createdMeshes
	List_1_tBE402A53CD5331E0CB483C591CFDC8C6827F35B8 * ___createdMeshes_5;
	// System.Collections.Generic.List`1<UnityEngine.Material> Kolibri2d.SplineTerrain2DCreator::createdMaterials
	List_1_t56B616621385F653B408094FBDFA5DEC6C4B47EA * ___createdMaterials_6;
	// System.Single[] Kolibri2d.SplineTerrain2DCreator::distancePerPoint
	SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* ___distancePerPoint_7;

public:
	inline static int32_t get_offset_of_spline_0() { return static_cast<int32_t>(offsetof(SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1, ___spline_0)); }
	inline BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * get_spline_0() const { return ___spline_0; }
	inline BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE ** get_address_of_spline_0() { return &___spline_0; }
	inline void set_spline_0(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * value)
	{
		___spline_0 = value;
		Il2CppCodeGenWriteBarrier((&___spline_0), value);
	}

	inline static int32_t get_offset_of_arch_1() { return static_cast<int32_t>(offsetof(SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1, ___arch_1)); }
	inline Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * get_arch_1() const { return ___arch_1; }
	inline Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A ** get_address_of_arch_1() { return &___arch_1; }
	inline void set_arch_1(Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * value)
	{
		___arch_1 = value;
		Il2CppCodeGenWriteBarrier((&___arch_1), value);
	}

	inline static int32_t get_offset_of_materialHolders_2() { return static_cast<int32_t>(offsetof(SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1, ___materialHolders_2)); }
	inline List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 * get_materialHolders_2() const { return ___materialHolders_2; }
	inline List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 ** get_address_of_materialHolders_2() { return &___materialHolders_2; }
	inline void set_materialHolders_2(List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 * value)
	{
		___materialHolders_2 = value;
		Il2CppCodeGenWriteBarrier((&___materialHolders_2), value);
	}

	inline static int32_t get_offset_of_terrain_3() { return static_cast<int32_t>(offsetof(SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1, ___terrain_3)); }
	inline Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B * get_terrain_3() const { return ___terrain_3; }
	inline Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B ** get_address_of_terrain_3() { return &___terrain_3; }
	inline void set_terrain_3(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B * value)
	{
		___terrain_3 = value;
		Il2CppCodeGenWriteBarrier((&___terrain_3), value);
	}

	inline static int32_t get_offset_of_createdMeshes_5() { return static_cast<int32_t>(offsetof(SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1, ___createdMeshes_5)); }
	inline List_1_tBE402A53CD5331E0CB483C591CFDC8C6827F35B8 * get_createdMeshes_5() const { return ___createdMeshes_5; }
	inline List_1_tBE402A53CD5331E0CB483C591CFDC8C6827F35B8 ** get_address_of_createdMeshes_5() { return &___createdMeshes_5; }
	inline void set_createdMeshes_5(List_1_tBE402A53CD5331E0CB483C591CFDC8C6827F35B8 * value)
	{
		___createdMeshes_5 = value;
		Il2CppCodeGenWriteBarrier((&___createdMeshes_5), value);
	}

	inline static int32_t get_offset_of_createdMaterials_6() { return static_cast<int32_t>(offsetof(SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1, ___createdMaterials_6)); }
	inline List_1_t56B616621385F653B408094FBDFA5DEC6C4B47EA * get_createdMaterials_6() const { return ___createdMaterials_6; }
	inline List_1_t56B616621385F653B408094FBDFA5DEC6C4B47EA ** get_address_of_createdMaterials_6() { return &___createdMaterials_6; }
	inline void set_createdMaterials_6(List_1_t56B616621385F653B408094FBDFA5DEC6C4B47EA * value)
	{
		___createdMaterials_6 = value;
		Il2CppCodeGenWriteBarrier((&___createdMaterials_6), value);
	}

	inline static int32_t get_offset_of_distancePerPoint_7() { return static_cast<int32_t>(offsetof(SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1, ___distancePerPoint_7)); }
	inline SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* get_distancePerPoint_7() const { return ___distancePerPoint_7; }
	inline SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5** get_address_of_distancePerPoint_7() { return &___distancePerPoint_7; }
	inline void set_distancePerPoint_7(SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* value)
	{
		___distancePerPoint_7 = value;
		Il2CppCodeGenWriteBarrier((&___distancePerPoint_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINETERRAIN2DCREATOR_TB376515DA574676086ACA2E8C0914DDB27FDFBD1_H
#ifndef SORTINGLAYEROPTIONS2_TBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE_H
#define SORTINGLAYEROPTIONS2_TBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2DMaterial/SortingLayerOptions2
struct  SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE  : public RuntimeObject
{
public:
	// System.Int32 Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::groundLayer
	int32_t ___groundLayer_0;
	// System.Int32 Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::groundOrder
	int32_t ___groundOrder_1;
	// System.Int32 Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::wallLayer
	int32_t ___wallLayer_2;
	// System.Int32 Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::wallOrder
	int32_t ___wallOrder_3;
	// System.Int32 Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::ceilingLayer
	int32_t ___ceilingLayer_4;
	// System.Int32 Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::ceilingOrder
	int32_t ___ceilingOrder_5;
	// System.Int32 Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::fillLayer
	int32_t ___fillLayer_6;
	// System.Int32 Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::fillOrder
	int32_t ___fillOrder_7;
	// System.Int32 Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::cornersLayer
	int32_t ___cornersLayer_8;
	// System.Int32 Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::cornerOrder
	int32_t ___cornerOrder_9;

public:
	inline static int32_t get_offset_of_groundLayer_0() { return static_cast<int32_t>(offsetof(SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE, ___groundLayer_0)); }
	inline int32_t get_groundLayer_0() const { return ___groundLayer_0; }
	inline int32_t* get_address_of_groundLayer_0() { return &___groundLayer_0; }
	inline void set_groundLayer_0(int32_t value)
	{
		___groundLayer_0 = value;
	}

	inline static int32_t get_offset_of_groundOrder_1() { return static_cast<int32_t>(offsetof(SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE, ___groundOrder_1)); }
	inline int32_t get_groundOrder_1() const { return ___groundOrder_1; }
	inline int32_t* get_address_of_groundOrder_1() { return &___groundOrder_1; }
	inline void set_groundOrder_1(int32_t value)
	{
		___groundOrder_1 = value;
	}

	inline static int32_t get_offset_of_wallLayer_2() { return static_cast<int32_t>(offsetof(SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE, ___wallLayer_2)); }
	inline int32_t get_wallLayer_2() const { return ___wallLayer_2; }
	inline int32_t* get_address_of_wallLayer_2() { return &___wallLayer_2; }
	inline void set_wallLayer_2(int32_t value)
	{
		___wallLayer_2 = value;
	}

	inline static int32_t get_offset_of_wallOrder_3() { return static_cast<int32_t>(offsetof(SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE, ___wallOrder_3)); }
	inline int32_t get_wallOrder_3() const { return ___wallOrder_3; }
	inline int32_t* get_address_of_wallOrder_3() { return &___wallOrder_3; }
	inline void set_wallOrder_3(int32_t value)
	{
		___wallOrder_3 = value;
	}

	inline static int32_t get_offset_of_ceilingLayer_4() { return static_cast<int32_t>(offsetof(SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE, ___ceilingLayer_4)); }
	inline int32_t get_ceilingLayer_4() const { return ___ceilingLayer_4; }
	inline int32_t* get_address_of_ceilingLayer_4() { return &___ceilingLayer_4; }
	inline void set_ceilingLayer_4(int32_t value)
	{
		___ceilingLayer_4 = value;
	}

	inline static int32_t get_offset_of_ceilingOrder_5() { return static_cast<int32_t>(offsetof(SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE, ___ceilingOrder_5)); }
	inline int32_t get_ceilingOrder_5() const { return ___ceilingOrder_5; }
	inline int32_t* get_address_of_ceilingOrder_5() { return &___ceilingOrder_5; }
	inline void set_ceilingOrder_5(int32_t value)
	{
		___ceilingOrder_5 = value;
	}

	inline static int32_t get_offset_of_fillLayer_6() { return static_cast<int32_t>(offsetof(SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE, ___fillLayer_6)); }
	inline int32_t get_fillLayer_6() const { return ___fillLayer_6; }
	inline int32_t* get_address_of_fillLayer_6() { return &___fillLayer_6; }
	inline void set_fillLayer_6(int32_t value)
	{
		___fillLayer_6 = value;
	}

	inline static int32_t get_offset_of_fillOrder_7() { return static_cast<int32_t>(offsetof(SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE, ___fillOrder_7)); }
	inline int32_t get_fillOrder_7() const { return ___fillOrder_7; }
	inline int32_t* get_address_of_fillOrder_7() { return &___fillOrder_7; }
	inline void set_fillOrder_7(int32_t value)
	{
		___fillOrder_7 = value;
	}

	inline static int32_t get_offset_of_cornersLayer_8() { return static_cast<int32_t>(offsetof(SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE, ___cornersLayer_8)); }
	inline int32_t get_cornersLayer_8() const { return ___cornersLayer_8; }
	inline int32_t* get_address_of_cornersLayer_8() { return &___cornersLayer_8; }
	inline void set_cornersLayer_8(int32_t value)
	{
		___cornersLayer_8 = value;
	}

	inline static int32_t get_offset_of_cornerOrder_9() { return static_cast<int32_t>(offsetof(SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE, ___cornerOrder_9)); }
	inline int32_t get_cornerOrder_9() const { return ___cornerOrder_9; }
	inline int32_t* get_address_of_cornerOrder_9() { return &___cornerOrder_9; }
	inline void set_cornerOrder_9(int32_t value)
	{
		___cornerOrder_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTINGLAYEROPTIONS2_TBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE_H
#ifndef SORTINGZ_3D_OPTIONS_TFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F_H
#define SORTINGZ_3D_OPTIONS_TFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2DMaterial/SortingZ_3D_Options
struct  SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F  : public RuntimeObject
{
public:
	// System.Single Kolibri2d.Terrain2DMaterial/SortingZ_3D_Options::groundZ
	float ___groundZ_0;
	// System.Single Kolibri2d.Terrain2DMaterial/SortingZ_3D_Options::wallZ
	float ___wallZ_1;
	// System.Single Kolibri2d.Terrain2DMaterial/SortingZ_3D_Options::ceilingZ
	float ___ceilingZ_2;
	// System.Single Kolibri2d.Terrain2DMaterial/SortingZ_3D_Options::fillZ
	float ___fillZ_3;
	// System.Single Kolibri2d.Terrain2DMaterial/SortingZ_3D_Options::cornerZ
	float ___cornerZ_4;

public:
	inline static int32_t get_offset_of_groundZ_0() { return static_cast<int32_t>(offsetof(SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F, ___groundZ_0)); }
	inline float get_groundZ_0() const { return ___groundZ_0; }
	inline float* get_address_of_groundZ_0() { return &___groundZ_0; }
	inline void set_groundZ_0(float value)
	{
		___groundZ_0 = value;
	}

	inline static int32_t get_offset_of_wallZ_1() { return static_cast<int32_t>(offsetof(SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F, ___wallZ_1)); }
	inline float get_wallZ_1() const { return ___wallZ_1; }
	inline float* get_address_of_wallZ_1() { return &___wallZ_1; }
	inline void set_wallZ_1(float value)
	{
		___wallZ_1 = value;
	}

	inline static int32_t get_offset_of_ceilingZ_2() { return static_cast<int32_t>(offsetof(SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F, ___ceilingZ_2)); }
	inline float get_ceilingZ_2() const { return ___ceilingZ_2; }
	inline float* get_address_of_ceilingZ_2() { return &___ceilingZ_2; }
	inline void set_ceilingZ_2(float value)
	{
		___ceilingZ_2 = value;
	}

	inline static int32_t get_offset_of_fillZ_3() { return static_cast<int32_t>(offsetof(SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F, ___fillZ_3)); }
	inline float get_fillZ_3() const { return ___fillZ_3; }
	inline float* get_address_of_fillZ_3() { return &___fillZ_3; }
	inline void set_fillZ_3(float value)
	{
		___fillZ_3 = value;
	}

	inline static int32_t get_offset_of_cornerZ_4() { return static_cast<int32_t>(offsetof(SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F, ___cornerZ_4)); }
	inline float get_cornerZ_4() const { return ___cornerZ_4; }
	inline float* get_address_of_cornerZ_4() { return &___cornerZ_4; }
	inline void set_cornerZ_4(float value)
	{
		___cornerZ_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTINGZ_3D_OPTIONS_TFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F_H
#ifndef MESHHELPER2_T51BC2BE07E689574784F85C458B76F5361726DC2_H
#define MESHHELPER2_T51BC2BE07E689574784F85C458B76F5361726DC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Utils/MeshHelper2
struct  MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2  : public RuntimeObject
{
public:

public:
};

struct MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Kolibri2d.Utils/MeshHelper2::vertices
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___vertices_0;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Kolibri2d.Utils/MeshHelper2::normals
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___normals_1;
	// System.Collections.Generic.List`1<UnityEngine.Color> Kolibri2d.Utils/MeshHelper2::colors
	List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * ___colors_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Kolibri2d.Utils/MeshHelper2::uv
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___uv_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Kolibri2d.Utils/MeshHelper2::uv1
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___uv1_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Kolibri2d.Utils/MeshHelper2::uv2
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___uv2_5;
	// System.Collections.Generic.List`1<System.Int32> Kolibri2d.Utils/MeshHelper2::indices
	List_1_t4A961510635950236678F1B3B2436ECCAF28713A * ___indices_6;
	// System.Collections.Generic.Dictionary`2<System.UInt32,System.Int32> Kolibri2d.Utils/MeshHelper2::newVectices
	Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F * ___newVectices_7;

public:
	inline static int32_t get_offset_of_vertices_0() { return static_cast<int32_t>(offsetof(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields, ___vertices_0)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_vertices_0() const { return ___vertices_0; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_vertices_0() { return &___vertices_0; }
	inline void set_vertices_0(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___vertices_0 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_0), value);
	}

	inline static int32_t get_offset_of_normals_1() { return static_cast<int32_t>(offsetof(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields, ___normals_1)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_normals_1() const { return ___normals_1; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_normals_1() { return &___normals_1; }
	inline void set_normals_1(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___normals_1 = value;
		Il2CppCodeGenWriteBarrier((&___normals_1), value);
	}

	inline static int32_t get_offset_of_colors_2() { return static_cast<int32_t>(offsetof(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields, ___colors_2)); }
	inline List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * get_colors_2() const { return ___colors_2; }
	inline List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E ** get_address_of_colors_2() { return &___colors_2; }
	inline void set_colors_2(List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * value)
	{
		___colors_2 = value;
		Il2CppCodeGenWriteBarrier((&___colors_2), value);
	}

	inline static int32_t get_offset_of_uv_3() { return static_cast<int32_t>(offsetof(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields, ___uv_3)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_uv_3() const { return ___uv_3; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_uv_3() { return &___uv_3; }
	inline void set_uv_3(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___uv_3 = value;
		Il2CppCodeGenWriteBarrier((&___uv_3), value);
	}

	inline static int32_t get_offset_of_uv1_4() { return static_cast<int32_t>(offsetof(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields, ___uv1_4)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_uv1_4() const { return ___uv1_4; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_uv1_4() { return &___uv1_4; }
	inline void set_uv1_4(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___uv1_4 = value;
		Il2CppCodeGenWriteBarrier((&___uv1_4), value);
	}

	inline static int32_t get_offset_of_uv2_5() { return static_cast<int32_t>(offsetof(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields, ___uv2_5)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_uv2_5() const { return ___uv2_5; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_uv2_5() { return &___uv2_5; }
	inline void set_uv2_5(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___uv2_5 = value;
		Il2CppCodeGenWriteBarrier((&___uv2_5), value);
	}

	inline static int32_t get_offset_of_indices_6() { return static_cast<int32_t>(offsetof(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields, ___indices_6)); }
	inline List_1_t4A961510635950236678F1B3B2436ECCAF28713A * get_indices_6() const { return ___indices_6; }
	inline List_1_t4A961510635950236678F1B3B2436ECCAF28713A ** get_address_of_indices_6() { return &___indices_6; }
	inline void set_indices_6(List_1_t4A961510635950236678F1B3B2436ECCAF28713A * value)
	{
		___indices_6 = value;
		Il2CppCodeGenWriteBarrier((&___indices_6), value);
	}

	inline static int32_t get_offset_of_newVectices_7() { return static_cast<int32_t>(offsetof(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields, ___newVectices_7)); }
	inline Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F * get_newVectices_7() const { return ___newVectices_7; }
	inline Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F ** get_address_of_newVectices_7() { return &___newVectices_7; }
	inline void set_newVectices_7(Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F * value)
	{
		___newVectices_7 = value;
		Il2CppCodeGenWriteBarrier((&___newVectices_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHHELPER2_T51BC2BE07E689574784F85C458B76F5361726DC2_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef DICTIONARY_2_T57122ABBA5C27104755B10FD4CDC2D5583CE469A_H
#define DICTIONARY_2_T57122ABBA5C27104755B10FD4CDC2D5583CE469A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<Kolibri2d.TerrainType,Kolibri2d.MaterialTextureInfo>
struct  Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_tA58A56CD6B5AD6B2DF5F4934A46CEC9F681B71AD* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	TerrainTypeU5BU5D_t3DA5B2A1F9D3FF051682ECFF02704F8EDE40122F* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	MaterialTextureInfoU5BU5D_tAEBAFCAFCD888B2D9CBD6B9739EE67FD30B3F264* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t778922F6A5AACC38C8F326D3338A91D6D72B11E2 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A, ___table_4)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A, ___linkSlots_5)); }
	inline LinkU5BU5D_tA58A56CD6B5AD6B2DF5F4934A46CEC9F681B71AD* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_tA58A56CD6B5AD6B2DF5F4934A46CEC9F681B71AD** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_tA58A56CD6B5AD6B2DF5F4934A46CEC9F681B71AD* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A, ___keySlots_6)); }
	inline TerrainTypeU5BU5D_t3DA5B2A1F9D3FF051682ECFF02704F8EDE40122F* get_keySlots_6() const { return ___keySlots_6; }
	inline TerrainTypeU5BU5D_t3DA5B2A1F9D3FF051682ECFF02704F8EDE40122F** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(TerrainTypeU5BU5D_t3DA5B2A1F9D3FF051682ECFF02704F8EDE40122F* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A, ___valueSlots_7)); }
	inline MaterialTextureInfoU5BU5D_tAEBAFCAFCD888B2D9CBD6B9739EE67FD30B3F264* get_valueSlots_7() const { return ___valueSlots_7; }
	inline MaterialTextureInfoU5BU5D_tAEBAFCAFCD888B2D9CBD6B9739EE67FD30B3F264** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(MaterialTextureInfoU5BU5D_tAEBAFCAFCD888B2D9CBD6B9739EE67FD30B3F264* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A, ___serialization_info_13)); }
	inline SerializationInfo_t778922F6A5AACC38C8F326D3338A91D6D72B11E2 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t778922F6A5AACC38C8F326D3338A91D6D72B11E2 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t778922F6A5AACC38C8F326D3338A91D6D72B11E2 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t4C073460CE5CB06CC2AE739773A9BC6FE183BA46 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t4C073460CE5CB06CC2AE739773A9BC6FE183BA46 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t4C073460CE5CB06CC2AE739773A9BC6FE183BA46 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t4C073460CE5CB06CC2AE739773A9BC6FE183BA46 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T57122ABBA5C27104755B10FD4CDC2D5583CE469A_H
#ifndef DICTIONARY_2_T6F999E1DBEE168573A7FC8688595C749C148353F_H
#define DICTIONARY_2_T6F999E1DBEE168573A7FC8688595C749C148353F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.UInt32,System.Int32>
struct  Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_tA58A56CD6B5AD6B2DF5F4934A46CEC9F681B71AD* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t778922F6A5AACC38C8F326D3338A91D6D72B11E2 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F, ___table_4)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F, ___linkSlots_5)); }
	inline LinkU5BU5D_tA58A56CD6B5AD6B2DF5F4934A46CEC9F681B71AD* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_tA58A56CD6B5AD6B2DF5F4934A46CEC9F681B71AD** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_tA58A56CD6B5AD6B2DF5F4934A46CEC9F681B71AD* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F, ___keySlots_6)); }
	inline UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5* get_keySlots_6() const { return ___keySlots_6; }
	inline UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F, ___valueSlots_7)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_valueSlots_7() const { return ___valueSlots_7; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F, ___serialization_info_13)); }
	inline SerializationInfo_t778922F6A5AACC38C8F326D3338A91D6D72B11E2 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t778922F6A5AACC38C8F326D3338A91D6D72B11E2 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t778922F6A5AACC38C8F326D3338A91D6D72B11E2 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t7EA7B7A2427C9EEE9B079C56FC2060C7C7B9C707 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t7EA7B7A2427C9EEE9B079C56FC2060C7C7B9C707 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t7EA7B7A2427C9EEE9B079C56FC2060C7C7B9C707 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t7EA7B7A2427C9EEE9B079C56FC2060C7C7B9C707 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T6F999E1DBEE168573A7FC8688595C749C148353F_H
#ifndef LIST_1_T517344AED92C69B6645F0ABFA470063D220BACCD_H
#define LIST_1_T517344AED92C69B6645F0ABFA470063D220BACCD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Kolibri2d.SplineSegmentInfo>
struct  List_1_t517344AED92C69B6645F0ABFA470063D220BACCD  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	SplineSegmentInfoU5BU5D_t624E5D3182FB6F8E0866F27EFE82E878F58B40B6* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t517344AED92C69B6645F0ABFA470063D220BACCD, ____items_1)); }
	inline SplineSegmentInfoU5BU5D_t624E5D3182FB6F8E0866F27EFE82E878F58B40B6* get__items_1() const { return ____items_1; }
	inline SplineSegmentInfoU5BU5D_t624E5D3182FB6F8E0866F27EFE82E878F58B40B6** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(SplineSegmentInfoU5BU5D_t624E5D3182FB6F8E0866F27EFE82E878F58B40B6* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t517344AED92C69B6645F0ABFA470063D220BACCD, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t517344AED92C69B6645F0ABFA470063D220BACCD, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t517344AED92C69B6645F0ABFA470063D220BACCD_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	SplineSegmentInfoU5BU5D_t624E5D3182FB6F8E0866F27EFE82E878F58B40B6* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t517344AED92C69B6645F0ABFA470063D220BACCD_StaticFields, ___EmptyArray_4)); }
	inline SplineSegmentInfoU5BU5D_t624E5D3182FB6F8E0866F27EFE82E878F58B40B6* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline SplineSegmentInfoU5BU5D_t624E5D3182FB6F8E0866F27EFE82E878F58B40B6** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(SplineSegmentInfoU5BU5D_t624E5D3182FB6F8E0866F27EFE82E878F58B40B6* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T517344AED92C69B6645F0ABFA470063D220BACCD_H
#ifndef LIST_1_T99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4_H
#define LIST_1_T99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Kolibri2d.Terrain2D/Terrain2DMaterialHolder>
struct  List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Terrain2DMaterialHolderU5BU5D_t16F721941294EA9C048940BD138D26F87D871464* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4, ____items_1)); }
	inline Terrain2DMaterialHolderU5BU5D_t16F721941294EA9C048940BD138D26F87D871464* get__items_1() const { return ____items_1; }
	inline Terrain2DMaterialHolderU5BU5D_t16F721941294EA9C048940BD138D26F87D871464** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Terrain2DMaterialHolderU5BU5D_t16F721941294EA9C048940BD138D26F87D871464* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Terrain2DMaterialHolderU5BU5D_t16F721941294EA9C048940BD138D26F87D871464* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4_StaticFields, ___EmptyArray_4)); }
	inline Terrain2DMaterialHolderU5BU5D_t16F721941294EA9C048940BD138D26F87D871464* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Terrain2DMaterialHolderU5BU5D_t16F721941294EA9C048940BD138D26F87D871464** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Terrain2DMaterialHolderU5BU5D_t16F721941294EA9C048940BD138D26F87D871464* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4_H
#ifndef LIST_1_T4A961510635950236678F1B3B2436ECCAF28713A_H
#define LIST_1_T4A961510635950236678F1B3B2436ECCAF28713A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Int32>
struct  List_1_t4A961510635950236678F1B3B2436ECCAF28713A  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4A961510635950236678F1B3B2436ECCAF28713A, ____items_1)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4A961510635950236678F1B3B2436ECCAF28713A, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4A961510635950236678F1B3B2436ECCAF28713A, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t4A961510635950236678F1B3B2436ECCAF28713A_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t4A961510635950236678F1B3B2436ECCAF28713A_StaticFields, ___EmptyArray_4)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T4A961510635950236678F1B3B2436ECCAF28713A_H
#ifndef LIST_1_T09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E_H
#define LIST_1_T09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Color>
struct  List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E, ____items_1)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get__items_1() const { return ____items_1; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E_StaticFields, ___EmptyArray_4)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E_H
#ifndef LIST_1_T0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_H
#define LIST_1_T0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct  List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____items_1)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get__items_1() const { return ____items_1; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_StaticFields, ___EmptyArray_4)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_H
#ifndef LIST_1_TFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_H
#define LIST_1_TFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct  List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____items_1)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_StaticFields, ___EmptyArray_4)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_TFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#define VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_com
{
};
#endif // VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef BOOLEAN_T92E4792324DA9B716F339A3B965A14965E99A4EF_H
#define BOOLEAN_T92E4792324DA9B716F339A3B965A14965E99A4EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t92E4792324DA9B716F339A3B965A14965E99A4EF 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t92E4792324DA9B716F339A3B965A14965E99A4EF, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t92E4792324DA9B716F339A3B965A14965E99A4EF_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t92E4792324DA9B716F339A3B965A14965E99A4EF_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t92E4792324DA9B716F339A3B965A14965E99A4EF_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T92E4792324DA9B716F339A3B965A14965E99A4EF_H
#ifndef ENUMERATOR_T0B3353B842EAAEDC0D496EC51375E2783162A5B7_H
#define ENUMERATOR_T0B3353B842EAAEDC0D496EC51375E2783162A5B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<Kolibri2d.Terrain2D/Terrain2DMaterialHolder>
struct  Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7, ___l_0)); }
	inline List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 * get_l_0() const { return ___l_0; }
	inline List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7, ___current_3)); }
	inline Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * get_current_3() const { return ___current_3; }
	inline Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T0B3353B842EAAEDC0D496EC51375E2783162A5B7_H
#ifndef ENUMERATOR_T7069929711A56B20B09076697B86D7BEE647C7DC_H
#define ENUMERATOR_T7069929711A56B20B09076697B86D7BEE647C7DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t7069929711A56B20B09076697B86D7BEE647C7DC 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_tE72A517BD14F52539FF78EA90F58D1387FEED660 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t7069929711A56B20B09076697B86D7BEE647C7DC, ___l_0)); }
	inline List_1_tE72A517BD14F52539FF78EA90F58D1387FEED660 * get_l_0() const { return ___l_0; }
	inline List_1_tE72A517BD14F52539FF78EA90F58D1387FEED660 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_tE72A517BD14F52539FF78EA90F58D1387FEED660 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t7069929711A56B20B09076697B86D7BEE647C7DC, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t7069929711A56B20B09076697B86D7BEE647C7DC, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t7069929711A56B20B09076697B86D7BEE647C7DC, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T7069929711A56B20B09076697B86D7BEE647C7DC_H
#ifndef DOUBLE_T2011D65DAF7D1FCBE71DD4CBDFA69A8F24643159_H
#define DOUBLE_T2011D65DAF7D1FCBE71DD4CBDFA69A8F24643159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t2011D65DAF7D1FCBE71DD4CBDFA69A8F24643159 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t2011D65DAF7D1FCBE71DD4CBDFA69A8F24643159, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T2011D65DAF7D1FCBE71DD4CBDFA69A8F24643159_H
#ifndef ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#define ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF  : public ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C
{
public:

public:
};

struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_com
{
};
#endif // ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifndef INT32_TC16F64335CE8B56D99229DE94BB3A876ED55FE87_H
#define INT32_TC16F64335CE8B56D99229DE94BB3A876ED55FE87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_tC16F64335CE8B56D99229DE94BB3A876ED55FE87 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_tC16F64335CE8B56D99229DE94BB3A876ED55FE87, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_TC16F64335CE8B56D99229DE94BB3A876ED55FE87_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TF2613CF3F3AF0E8BEBC3CE8AD41479C44E6075DB_H
#define SINGLE_TF2613CF3F3AF0E8BEBC3CE8AD41479C44E6075DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tF2613CF3F3AF0E8BEBC3CE8AD41479C44E6075DB 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_tF2613CF3F3AF0E8BEBC3CE8AD41479C44E6075DB, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TF2613CF3F3AF0E8BEBC3CE8AD41479C44E6075DB_H
#ifndef UINT32_T69F92C53356907895162C7F31D87C59F9141D3B8_H
#define UINT32_T69F92C53356907895162C7F31D87C59F9141D3B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t69F92C53356907895162C7F31D87C59F9141D3B8 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t69F92C53356907895162C7F31D87C59F9141D3B8, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T69F92C53356907895162C7F31D87C59F9141D3B8_H
#ifndef VOID_TDB81A15FA2AB53E2401A76B745D215397B29F783_H
#define VOID_TDB81A15FA2AB53E2401A76B745D215397B29F783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_tDB81A15FA2AB53E2401A76B745D215397B29F783 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_TDB81A15FA2AB53E2401A76B745D215397B29F783_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef UNITYEVENT_1_T7839A0014FFD3A212A87547A44A7719D6549ED87_H
#define UNITYEVENT_1_T7839A0014FFD3A212A87547A44A7719D6549ED87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T7839A0014FFD3A212A87547A44A7719D6549ED87_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef ROTATIONMODE_T931B059AC9C559A27F74EAD84F6702CCEA711106_H
#define ROTATIONMODE_T931B059AC9C559A27F74EAD84F6702CCEA711106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.MaterialTextureInfo/RotationMode
struct  RotationMode_t931B059AC9C559A27F74EAD84F6702CCEA711106 
{
public:
	// System.Int32 Kolibri2d.MaterialTextureInfo/RotationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RotationMode_t931B059AC9C559A27F74EAD84F6702CCEA711106, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONMODE_T931B059AC9C559A27F74EAD84F6702CCEA711106_H
#ifndef SPRITESTRETCHTYPE_TE3931A80B38F8AE955979B13406A9B99BE0F570D_H
#define SPRITESTRETCHTYPE_TE3931A80B38F8AE955979B13406A9B99BE0F570D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.MaterialTextureInfo/SpriteStretchType
struct  SpriteStretchType_tE3931A80B38F8AE955979B13406A9B99BE0F570D 
{
public:
	// System.Int32 Kolibri2d.MaterialTextureInfo/SpriteStretchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SpriteStretchType_tE3931A80B38F8AE955979B13406A9B99BE0F570D, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITESTRETCHTYPE_TE3931A80B38F8AE955979B13406A9B99BE0F570D_H
#ifndef SHAPETANGENTMODE_TD59E4B5749266A0BCF5D57B67CD30F5832591CAE_H
#define SHAPETANGENTMODE_TD59E4B5749266A0BCF5D57B67CD30F5832591CAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.ShapeTangentMode
struct  ShapeTangentMode_tD59E4B5749266A0BCF5D57B67CD30F5832591CAE 
{
public:
	// System.Int32 Kolibri2d.ShapeTangentMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ShapeTangentMode_tD59E4B5749266A0BCF5D57B67CD30F5832591CAE, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAPETANGENTMODE_TD59E4B5749266A0BCF5D57B67CD30F5832591CAE_H
#ifndef FORCEDIRECTION_T92D92333E954731283BD44FF051EF85F9FB8413D_H
#define FORCEDIRECTION_T92D92333E954731283BD44FF051EF85F9FB8413D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineWater/ForceDirection
struct  ForceDirection_t92D92333E954731283BD44FF051EF85F9FB8413D 
{
public:
	// System.Int32 Kolibri2d.SplineWater/ForceDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ForceDirection_t92D92333E954731283BD44FF051EF85F9FB8413D, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCEDIRECTION_T92D92333E954731283BD44FF051EF85F9FB8413D_H
#ifndef MESHSEGMENTTYPE_TB60582871C7DE1B80274E63C8FCA2A0E90054B5D_H
#define MESHSEGMENTTYPE_TB60582871C7DE1B80274E63C8FCA2A0E90054B5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineWater/MeshSegmentType
struct  MeshSegmentType_tB60582871C7DE1B80274E63C8FCA2A0E90054B5D 
{
public:
	// System.Int32 Kolibri2d.SplineWater/MeshSegmentType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MeshSegmentType_tB60582871C7DE1B80274E63C8FCA2A0E90054B5D, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHSEGMENTTYPE_TB60582871C7DE1B80274E63C8FCA2A0E90054B5D_H
#ifndef WATEREVENT_T414CD81AE9F28B656088F21ECC14F030B1FDA33B_H
#define WATEREVENT_T414CD81AE9F28B656088F21ECC14F030B1FDA33B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineWater/WaterEvent
struct  WaterEvent_t414CD81AE9F28B656088F21ECC14F030B1FDA33B  : public UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATEREVENT_T414CD81AE9F28B656088F21ECC14F030B1FDA33B_H
#ifndef SKIRTTYPE_TF27DB6D7DFF61A49FB473B5B26C52F1EBED74A99_H
#define SKIRTTYPE_TF27DB6D7DFF61A49FB473B5B26C52F1EBED74A99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2D/SkirtType
struct  SkirtType_tF27DB6D7DFF61A49FB473B5B26C52F1EBED74A99 
{
public:
	// System.Int32 Kolibri2d.Terrain2D/SkirtType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SkirtType_tF27DB6D7DFF61A49FB473B5B26C52F1EBED74A99, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKIRTTYPE_TF27DB6D7DFF61A49FB473B5B26C52F1EBED74A99_H
#ifndef TERRAIN2DMATERIALHOLDER_TBF26B45C9650004249F5363B2D8AB054CB1EE60F_H
#define TERRAIN2DMATERIALHOLDER_TBF26B45C9650004249F5363B2D8AB054CB1EE60F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2D/Terrain2DMaterialHolder
struct  Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F  : public RuntimeObject
{
public:
	// System.Boolean Kolibri2d.Terrain2D/Terrain2DMaterialHolder::enabled
	bool ___enabled_0;
	// Kolibri2d.Terrain2DMaterial Kolibri2d.Terrain2D/Terrain2DMaterialHolder::terrainMaterial
	Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * ___terrainMaterial_1;
	// UnityEngine.Material Kolibri2d.Terrain2D/Terrain2DMaterialHolder::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_2;
	// UnityEngine.Color Kolibri2d.Terrain2D/Terrain2DMaterialHolder::tint
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___tint_3;
	// System.Boolean Kolibri2d.Terrain2D/Terrain2DMaterialHolder::useTint
	bool ___useTint_4;
	// System.Boolean Kolibri2d.Terrain2D/Terrain2DMaterialHolder::createColliders
	bool ___createColliders_5;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_terrainMaterial_1() { return static_cast<int32_t>(offsetof(Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F, ___terrainMaterial_1)); }
	inline Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * get_terrainMaterial_1() const { return ___terrainMaterial_1; }
	inline Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D ** get_address_of_terrainMaterial_1() { return &___terrainMaterial_1; }
	inline void set_terrainMaterial_1(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * value)
	{
		___terrainMaterial_1 = value;
		Il2CppCodeGenWriteBarrier((&___terrainMaterial_1), value);
	}

	inline static int32_t get_offset_of_material_2() { return static_cast<int32_t>(offsetof(Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F, ___material_2)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_2() const { return ___material_2; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_2() { return &___material_2; }
	inline void set_material_2(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_2 = value;
		Il2CppCodeGenWriteBarrier((&___material_2), value);
	}

	inline static int32_t get_offset_of_tint_3() { return static_cast<int32_t>(offsetof(Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F, ___tint_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_tint_3() const { return ___tint_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_tint_3() { return &___tint_3; }
	inline void set_tint_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___tint_3 = value;
	}

	inline static int32_t get_offset_of_useTint_4() { return static_cast<int32_t>(offsetof(Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F, ___useTint_4)); }
	inline bool get_useTint_4() const { return ___useTint_4; }
	inline bool* get_address_of_useTint_4() { return &___useTint_4; }
	inline void set_useTint_4(bool value)
	{
		___useTint_4 = value;
	}

	inline static int32_t get_offset_of_createColliders_5() { return static_cast<int32_t>(offsetof(Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F, ___createColliders_5)); }
	inline bool get_createColliders_5() const { return ___createColliders_5; }
	inline bool* get_address_of_createColliders_5() { return &___createColliders_5; }
	inline void set_createColliders_5(bool value)
	{
		___createColliders_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERRAIN2DMATERIALHOLDER_TBF26B45C9650004249F5363B2D8AB054CB1EE60F_H
#ifndef ASPECT_T4C69BE83340C4AF890ACE7BB85B0B09491E8C5AC_H
#define ASPECT_T4C69BE83340C4AF890ACE7BB85B0B09491E8C5AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2DArchitecture/Aspect
struct  Aspect_t4C69BE83340C4AF890ACE7BB85B0B09491E8C5AC 
{
public:
	// System.Int32 Kolibri2d.Terrain2DArchitecture/Aspect::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Aspect_t4C69BE83340C4AF890ACE7BB85B0B09491E8C5AC, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECT_T4C69BE83340C4AF890ACE7BB85B0B09491E8C5AC_H
#ifndef CORNERSIDE_T8D5E50DDEDB6E82A23FCCFA8EFAD995D237D95FC_H
#define CORNERSIDE_T8D5E50DDEDB6E82A23FCCFA8EFAD995D237D95FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2DMaterial/CornerSide
struct  CornerSide_t8D5E50DDEDB6E82A23FCCFA8EFAD995D237D95FC 
{
public:
	// System.Int32 Kolibri2d.Terrain2DMaterial/CornerSide::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CornerSide_t8D5E50DDEDB6E82A23FCCFA8EFAD995D237D95FC, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORNERSIDE_T8D5E50DDEDB6E82A23FCCFA8EFAD995D237D95FC_H
#ifndef CORNERDISPLAYTYPE_T371ADA93330AF421D7EC2FBEB92BC9EA01C9815D_H
#define CORNERDISPLAYTYPE_T371ADA93330AF421D7EC2FBEB92BC9EA01C9815D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2DMaterial/CornerSprites2/CornerDisplayType
struct  CornerDisplayType_t371ADA93330AF421D7EC2FBEB92BC9EA01C9815D 
{
public:
	// System.Int32 Kolibri2d.Terrain2DMaterial/CornerSprites2/CornerDisplayType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CornerDisplayType_t371ADA93330AF421D7EC2FBEB92BC9EA01C9815D, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORNERDISPLAYTYPE_T371ADA93330AF421D7EC2FBEB92BC9EA01C9815D_H
#ifndef CORNERTYPE_TB72616AAD3B5283135F2E892B87F36ECEB748AE7_H
#define CORNERTYPE_TB72616AAD3B5283135F2E892B87F36ECEB748AE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2DMaterial/CornerType
struct  CornerType_tB72616AAD3B5283135F2E892B87F36ECEB748AE7 
{
public:
	// System.Int32 Kolibri2d.Terrain2DMaterial/CornerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CornerType_tB72616AAD3B5283135F2E892B87F36ECEB748AE7, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORNERTYPE_TB72616AAD3B5283135F2E892B87F36ECEB748AE7_H
#ifndef FILLTYPE_TBDFB72417EBD74C72808826851F60D35622EFD6E_H
#define FILLTYPE_TBDFB72417EBD74C72808826851F60D35622EFD6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2DMaterial/FillType
struct  FillType_tBDFB72417EBD74C72808826851F60D35622EFD6E 
{
public:
	// System.Int32 Kolibri2d.Terrain2DMaterial/FillType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillType_tBDFB72417EBD74C72808826851F60D35622EFD6E, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLTYPE_TBDFB72417EBD74C72808826851F60D35622EFD6E_H
#ifndef RENDERORDERTYPE_T080E9E94A764E427B39096E25C800721F2FDE98A_H
#define RENDERORDERTYPE_T080E9E94A764E427B39096E25C800721F2FDE98A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2DMaterial/RenderOrderType
struct  RenderOrderType_t080E9E94A764E427B39096E25C800721F2FDE98A 
{
public:
	// System.Int32 Kolibri2d.Terrain2DMaterial/RenderOrderType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderOrderType_t080E9E94A764E427B39096E25C800721F2FDE98A, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERORDERTYPE_T080E9E94A764E427B39096E25C800721F2FDE98A_H
#ifndef TERRAINTYPE_TCDCA8C4F29F1D3F57A51D0DD84B025709F7BFB19_H
#define TERRAINTYPE_TCDCA8C4F29F1D3F57A51D0DD84B025709F7BFB19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.TerrainType
struct  TerrainType_tCDCA8C4F29F1D3F57A51D0DD84B025709F7BFB19 
{
public:
	// System.Int32 Kolibri2d.TerrainType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TerrainType_tCDCA8C4F29F1D3F57A51D0DD84B025709F7BFB19, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERRAINTYPE_TCDCA8C4F29F1D3F57A51D0DD84B025709F7BFB19_H
#ifndef ENUMERATOR_TB77EBE584C61354E0222C9BCA686AB5B98BA7F0F_H
#define ENUMERATOR_TB77EBE584C61354E0222C9BCA686AB5B98BA7F0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>
struct  Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F, ___l_0)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_l_0() const { return ___l_0; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F, ___current_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_current_3() const { return ___current_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_TB77EBE584C61354E0222C9BCA686AB5B98BA7F0F_H
#ifndef BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#define BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Center_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Extents_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifndef FILTERMODE_T6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF_H
#define FILTERMODE_T6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FilterMode
struct  FilterMode_t6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF 
{
public:
	// System.Int32 UnityEngine.FilterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FilterMode_t6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERMODE_T6590B4B0BAE2BBBCABA8E1E93FA07A052B3261AF_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef TEXTUREFORMAT_T7C6B5101554065C47682E592D1E26079D4EC2DCE_H
#define TEXTUREFORMAT_T7C6B5101554065C47682E592D1E26079D4EC2DCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t7C6B5101554065C47682E592D1E26079D4EC2DCE 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureFormat_t7C6B5101554065C47682E592D1E26079D4EC2DCE, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T7C6B5101554065C47682E592D1E26079D4EC2DCE_H
#ifndef TEXTUREWRAPMODE_T8AC763BD80806A9175C6AA8D33D6BABAD83E950F_H
#define TEXTUREWRAPMODE_T8AC763BD80806A9175C6AA8D33D6BABAD83E950F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureWrapMode
struct  TextureWrapMode_t8AC763BD80806A9175C6AA8D33D6BABAD83E950F 
{
public:
	// System.Int32 UnityEngine.TextureWrapMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureWrapMode_t8AC763BD80806A9175C6AA8D33D6BABAD83E950F, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREWRAPMODE_T8AC763BD80806A9175C6AA8D33D6BABAD83E950F_H
#ifndef MATERIALTEXTUREINFO_T0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F_H
#define MATERIALTEXTUREINFO_T0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.MaterialTextureInfo
struct  MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F 
{
public:
	// UnityEngine.Sprite Kolibri2d.MaterialTextureInfo::sprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___sprite_0;
	// Kolibri2d.MaterialTextureInfo/RotationMode Kolibri2d.MaterialTextureInfo::rotation
	int32_t ___rotation_1;
	// Kolibri2d.MaterialTextureInfo/SpriteStretchType Kolibri2d.MaterialTextureInfo::stretchType
	int32_t ___stretchType_2;
	// System.Single Kolibri2d.MaterialTextureInfo::stretchValue
	float ___stretchValue_3;
	// UnityEngine.Vector2 Kolibri2d.MaterialTextureInfo::scale
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___scale_4;
	// UnityEngine.Vector2 Kolibri2d.MaterialTextureInfo::offset
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___offset_5;

public:
	inline static int32_t get_offset_of_sprite_0() { return static_cast<int32_t>(offsetof(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F, ___sprite_0)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_sprite_0() const { return ___sprite_0; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_sprite_0() { return &___sprite_0; }
	inline void set_sprite_0(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___sprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___sprite_0), value);
	}

	inline static int32_t get_offset_of_rotation_1() { return static_cast<int32_t>(offsetof(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F, ___rotation_1)); }
	inline int32_t get_rotation_1() const { return ___rotation_1; }
	inline int32_t* get_address_of_rotation_1() { return &___rotation_1; }
	inline void set_rotation_1(int32_t value)
	{
		___rotation_1 = value;
	}

	inline static int32_t get_offset_of_stretchType_2() { return static_cast<int32_t>(offsetof(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F, ___stretchType_2)); }
	inline int32_t get_stretchType_2() const { return ___stretchType_2; }
	inline int32_t* get_address_of_stretchType_2() { return &___stretchType_2; }
	inline void set_stretchType_2(int32_t value)
	{
		___stretchType_2 = value;
	}

	inline static int32_t get_offset_of_stretchValue_3() { return static_cast<int32_t>(offsetof(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F, ___stretchValue_3)); }
	inline float get_stretchValue_3() const { return ___stretchValue_3; }
	inline float* get_address_of_stretchValue_3() { return &___stretchValue_3; }
	inline void set_stretchValue_3(float value)
	{
		___stretchValue_3 = value;
	}

	inline static int32_t get_offset_of_scale_4() { return static_cast<int32_t>(offsetof(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F, ___scale_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_scale_4() const { return ___scale_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_scale_4() { return &___scale_4; }
	inline void set_scale_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___scale_4 = value;
	}

	inline static int32_t get_offset_of_offset_5() { return static_cast<int32_t>(offsetof(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F, ___offset_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_offset_5() const { return ___offset_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_offset_5() { return &___offset_5; }
	inline void set_offset_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___offset_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Kolibri2d.MaterialTextureInfo
struct MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F_marshaled_pinvoke
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___sprite_0;
	int32_t ___rotation_1;
	int32_t ___stretchType_2;
	float ___stretchValue_3;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___scale_4;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___offset_5;
};
// Native definition for COM marshalling of Kolibri2d.MaterialTextureInfo
struct MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F_marshaled_com
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___sprite_0;
	int32_t ___rotation_1;
	int32_t ___stretchType_2;
	float ___stretchValue_3;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___scale_4;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___offset_5;
};
#endif // MATERIALTEXTUREINFO_T0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F_H
#ifndef SPLINESEGMENTINFO_TBB34F651D1ED6826933D71C125491ADE7056F066_H
#define SPLINESEGMENTINFO_TBB34F651D1ED6826933D71C125491ADE7056F066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineSegmentInfo
struct  SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066 
{
public:
	// System.Int32 Kolibri2d.SplineSegmentInfo::begin
	int32_t ___begin_0;
	// System.Int32 Kolibri2d.SplineSegmentInfo::end
	int32_t ___end_1;
	// System.Single Kolibri2d.SplineSegmentInfo::angleBegin
	float ___angleBegin_2;
	// System.Single Kolibri2d.SplineSegmentInfo::angleCorner
	float ___angleCorner_3;
	// UnityEngine.Vector3 Kolibri2d.SplineSegmentInfo::dirBegin
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___dirBegin_4;
	// Kolibri2d.TerrainType Kolibri2d.SplineSegmentInfo::type
	int32_t ___type_5;

public:
	inline static int32_t get_offset_of_begin_0() { return static_cast<int32_t>(offsetof(SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066, ___begin_0)); }
	inline int32_t get_begin_0() const { return ___begin_0; }
	inline int32_t* get_address_of_begin_0() { return &___begin_0; }
	inline void set_begin_0(int32_t value)
	{
		___begin_0 = value;
	}

	inline static int32_t get_offset_of_end_1() { return static_cast<int32_t>(offsetof(SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066, ___end_1)); }
	inline int32_t get_end_1() const { return ___end_1; }
	inline int32_t* get_address_of_end_1() { return &___end_1; }
	inline void set_end_1(int32_t value)
	{
		___end_1 = value;
	}

	inline static int32_t get_offset_of_angleBegin_2() { return static_cast<int32_t>(offsetof(SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066, ___angleBegin_2)); }
	inline float get_angleBegin_2() const { return ___angleBegin_2; }
	inline float* get_address_of_angleBegin_2() { return &___angleBegin_2; }
	inline void set_angleBegin_2(float value)
	{
		___angleBegin_2 = value;
	}

	inline static int32_t get_offset_of_angleCorner_3() { return static_cast<int32_t>(offsetof(SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066, ___angleCorner_3)); }
	inline float get_angleCorner_3() const { return ___angleCorner_3; }
	inline float* get_address_of_angleCorner_3() { return &___angleCorner_3; }
	inline void set_angleCorner_3(float value)
	{
		___angleCorner_3 = value;
	}

	inline static int32_t get_offset_of_dirBegin_4() { return static_cast<int32_t>(offsetof(SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066, ___dirBegin_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_dirBegin_4() const { return ___dirBegin_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_dirBegin_4() { return &___dirBegin_4; }
	inline void set_dirBegin_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___dirBegin_4 = value;
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066, ___type_5)); }
	inline int32_t get_type_5() const { return ___type_5; }
	inline int32_t* get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(int32_t value)
	{
		___type_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINESEGMENTINFO_TBB34F651D1ED6826933D71C125491ADE7056F066_H
#ifndef CORNERSPRITES2_T56F48A9FF2507A11F647BF6F8B9F6B163B5A1703_H
#define CORNERSPRITES2_T56F48A9FF2507A11F647BF6F8B9F6B163B5A1703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2DMaterial/CornerSprites2
struct  CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703  : public RuntimeObject
{
public:
	// Kolibri2d.Terrain2DMaterial/CornerSprites2/CornerDisplayType Kolibri2d.Terrain2DMaterial/CornerSprites2::cornerDisplayType
	int32_t ___cornerDisplayType_0;
	// UnityEngine.Sprite Kolibri2d.Terrain2DMaterial/CornerSprites2::interiorTopLeft
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___interiorTopLeft_1;
	// UnityEngine.Vector2 Kolibri2d.Terrain2DMaterial/CornerSprites2::offsetITL
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___offsetITL_2;
	// System.Int32 Kolibri2d.Terrain2DMaterial/CornerSprites2::rotationITL
	int32_t ___rotationITL_3;
	// UnityEngine.Sprite Kolibri2d.Terrain2DMaterial/CornerSprites2::interiorTopRight
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___interiorTopRight_4;
	// UnityEngine.Vector2 Kolibri2d.Terrain2DMaterial/CornerSprites2::offsetITR
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___offsetITR_5;
	// System.Int32 Kolibri2d.Terrain2DMaterial/CornerSprites2::rotationITR
	int32_t ___rotationITR_6;
	// UnityEngine.Sprite Kolibri2d.Terrain2DMaterial/CornerSprites2::interiorBotLeft
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___interiorBotLeft_7;
	// UnityEngine.Vector2 Kolibri2d.Terrain2DMaterial/CornerSprites2::offsetIBL
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___offsetIBL_8;
	// System.Int32 Kolibri2d.Terrain2DMaterial/CornerSprites2::rotationIBL
	int32_t ___rotationIBL_9;
	// UnityEngine.Sprite Kolibri2d.Terrain2DMaterial/CornerSprites2::interiorBotRight
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___interiorBotRight_10;
	// UnityEngine.Vector2 Kolibri2d.Terrain2DMaterial/CornerSprites2::offsetIBR
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___offsetIBR_11;
	// System.Int32 Kolibri2d.Terrain2DMaterial/CornerSprites2::rotationIBR
	int32_t ___rotationIBR_12;
	// UnityEngine.Sprite Kolibri2d.Terrain2DMaterial/CornerSprites2::exteriorTopLeft
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___exteriorTopLeft_13;
	// UnityEngine.Vector2 Kolibri2d.Terrain2DMaterial/CornerSprites2::offsetETL
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___offsetETL_14;
	// System.Int32 Kolibri2d.Terrain2DMaterial/CornerSprites2::rotationETL
	int32_t ___rotationETL_15;
	// UnityEngine.Sprite Kolibri2d.Terrain2DMaterial/CornerSprites2::exteriorTopRight
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___exteriorTopRight_16;
	// UnityEngine.Vector2 Kolibri2d.Terrain2DMaterial/CornerSprites2::offsetETR
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___offsetETR_17;
	// System.Int32 Kolibri2d.Terrain2DMaterial/CornerSprites2::rotationETR
	int32_t ___rotationETR_18;
	// UnityEngine.Sprite Kolibri2d.Terrain2DMaterial/CornerSprites2::exteriorBotLeft
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___exteriorBotLeft_19;
	// UnityEngine.Vector2 Kolibri2d.Terrain2DMaterial/CornerSprites2::offsetEBL
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___offsetEBL_20;
	// UnityEngine.Sprite Kolibri2d.Terrain2DMaterial/CornerSprites2::exteriorBotRight
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___exteriorBotRight_21;
	// System.Int32 Kolibri2d.Terrain2DMaterial/CornerSprites2::rotationEBL
	int32_t ___rotationEBL_22;
	// UnityEngine.Vector2 Kolibri2d.Terrain2DMaterial/CornerSprites2::offsetEBR
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___offsetEBR_23;
	// System.Int32 Kolibri2d.Terrain2DMaterial/CornerSprites2::rotationEBR
	int32_t ___rotationEBR_24;

public:
	inline static int32_t get_offset_of_cornerDisplayType_0() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___cornerDisplayType_0)); }
	inline int32_t get_cornerDisplayType_0() const { return ___cornerDisplayType_0; }
	inline int32_t* get_address_of_cornerDisplayType_0() { return &___cornerDisplayType_0; }
	inline void set_cornerDisplayType_0(int32_t value)
	{
		___cornerDisplayType_0 = value;
	}

	inline static int32_t get_offset_of_interiorTopLeft_1() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___interiorTopLeft_1)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_interiorTopLeft_1() const { return ___interiorTopLeft_1; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_interiorTopLeft_1() { return &___interiorTopLeft_1; }
	inline void set_interiorTopLeft_1(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___interiorTopLeft_1 = value;
		Il2CppCodeGenWriteBarrier((&___interiorTopLeft_1), value);
	}

	inline static int32_t get_offset_of_offsetITL_2() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___offsetITL_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_offsetITL_2() const { return ___offsetITL_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_offsetITL_2() { return &___offsetITL_2; }
	inline void set_offsetITL_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___offsetITL_2 = value;
	}

	inline static int32_t get_offset_of_rotationITL_3() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___rotationITL_3)); }
	inline int32_t get_rotationITL_3() const { return ___rotationITL_3; }
	inline int32_t* get_address_of_rotationITL_3() { return &___rotationITL_3; }
	inline void set_rotationITL_3(int32_t value)
	{
		___rotationITL_3 = value;
	}

	inline static int32_t get_offset_of_interiorTopRight_4() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___interiorTopRight_4)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_interiorTopRight_4() const { return ___interiorTopRight_4; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_interiorTopRight_4() { return &___interiorTopRight_4; }
	inline void set_interiorTopRight_4(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___interiorTopRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___interiorTopRight_4), value);
	}

	inline static int32_t get_offset_of_offsetITR_5() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___offsetITR_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_offsetITR_5() const { return ___offsetITR_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_offsetITR_5() { return &___offsetITR_5; }
	inline void set_offsetITR_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___offsetITR_5 = value;
	}

	inline static int32_t get_offset_of_rotationITR_6() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___rotationITR_6)); }
	inline int32_t get_rotationITR_6() const { return ___rotationITR_6; }
	inline int32_t* get_address_of_rotationITR_6() { return &___rotationITR_6; }
	inline void set_rotationITR_6(int32_t value)
	{
		___rotationITR_6 = value;
	}

	inline static int32_t get_offset_of_interiorBotLeft_7() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___interiorBotLeft_7)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_interiorBotLeft_7() const { return ___interiorBotLeft_7; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_interiorBotLeft_7() { return &___interiorBotLeft_7; }
	inline void set_interiorBotLeft_7(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___interiorBotLeft_7 = value;
		Il2CppCodeGenWriteBarrier((&___interiorBotLeft_7), value);
	}

	inline static int32_t get_offset_of_offsetIBL_8() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___offsetIBL_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_offsetIBL_8() const { return ___offsetIBL_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_offsetIBL_8() { return &___offsetIBL_8; }
	inline void set_offsetIBL_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___offsetIBL_8 = value;
	}

	inline static int32_t get_offset_of_rotationIBL_9() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___rotationIBL_9)); }
	inline int32_t get_rotationIBL_9() const { return ___rotationIBL_9; }
	inline int32_t* get_address_of_rotationIBL_9() { return &___rotationIBL_9; }
	inline void set_rotationIBL_9(int32_t value)
	{
		___rotationIBL_9 = value;
	}

	inline static int32_t get_offset_of_interiorBotRight_10() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___interiorBotRight_10)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_interiorBotRight_10() const { return ___interiorBotRight_10; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_interiorBotRight_10() { return &___interiorBotRight_10; }
	inline void set_interiorBotRight_10(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___interiorBotRight_10 = value;
		Il2CppCodeGenWriteBarrier((&___interiorBotRight_10), value);
	}

	inline static int32_t get_offset_of_offsetIBR_11() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___offsetIBR_11)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_offsetIBR_11() const { return ___offsetIBR_11; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_offsetIBR_11() { return &___offsetIBR_11; }
	inline void set_offsetIBR_11(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___offsetIBR_11 = value;
	}

	inline static int32_t get_offset_of_rotationIBR_12() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___rotationIBR_12)); }
	inline int32_t get_rotationIBR_12() const { return ___rotationIBR_12; }
	inline int32_t* get_address_of_rotationIBR_12() { return &___rotationIBR_12; }
	inline void set_rotationIBR_12(int32_t value)
	{
		___rotationIBR_12 = value;
	}

	inline static int32_t get_offset_of_exteriorTopLeft_13() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___exteriorTopLeft_13)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_exteriorTopLeft_13() const { return ___exteriorTopLeft_13; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_exteriorTopLeft_13() { return &___exteriorTopLeft_13; }
	inline void set_exteriorTopLeft_13(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___exteriorTopLeft_13 = value;
		Il2CppCodeGenWriteBarrier((&___exteriorTopLeft_13), value);
	}

	inline static int32_t get_offset_of_offsetETL_14() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___offsetETL_14)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_offsetETL_14() const { return ___offsetETL_14; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_offsetETL_14() { return &___offsetETL_14; }
	inline void set_offsetETL_14(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___offsetETL_14 = value;
	}

	inline static int32_t get_offset_of_rotationETL_15() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___rotationETL_15)); }
	inline int32_t get_rotationETL_15() const { return ___rotationETL_15; }
	inline int32_t* get_address_of_rotationETL_15() { return &___rotationETL_15; }
	inline void set_rotationETL_15(int32_t value)
	{
		___rotationETL_15 = value;
	}

	inline static int32_t get_offset_of_exteriorTopRight_16() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___exteriorTopRight_16)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_exteriorTopRight_16() const { return ___exteriorTopRight_16; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_exteriorTopRight_16() { return &___exteriorTopRight_16; }
	inline void set_exteriorTopRight_16(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___exteriorTopRight_16 = value;
		Il2CppCodeGenWriteBarrier((&___exteriorTopRight_16), value);
	}

	inline static int32_t get_offset_of_offsetETR_17() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___offsetETR_17)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_offsetETR_17() const { return ___offsetETR_17; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_offsetETR_17() { return &___offsetETR_17; }
	inline void set_offsetETR_17(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___offsetETR_17 = value;
	}

	inline static int32_t get_offset_of_rotationETR_18() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___rotationETR_18)); }
	inline int32_t get_rotationETR_18() const { return ___rotationETR_18; }
	inline int32_t* get_address_of_rotationETR_18() { return &___rotationETR_18; }
	inline void set_rotationETR_18(int32_t value)
	{
		___rotationETR_18 = value;
	}

	inline static int32_t get_offset_of_exteriorBotLeft_19() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___exteriorBotLeft_19)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_exteriorBotLeft_19() const { return ___exteriorBotLeft_19; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_exteriorBotLeft_19() { return &___exteriorBotLeft_19; }
	inline void set_exteriorBotLeft_19(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___exteriorBotLeft_19 = value;
		Il2CppCodeGenWriteBarrier((&___exteriorBotLeft_19), value);
	}

	inline static int32_t get_offset_of_offsetEBL_20() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___offsetEBL_20)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_offsetEBL_20() const { return ___offsetEBL_20; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_offsetEBL_20() { return &___offsetEBL_20; }
	inline void set_offsetEBL_20(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___offsetEBL_20 = value;
	}

	inline static int32_t get_offset_of_exteriorBotRight_21() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___exteriorBotRight_21)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_exteriorBotRight_21() const { return ___exteriorBotRight_21; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_exteriorBotRight_21() { return &___exteriorBotRight_21; }
	inline void set_exteriorBotRight_21(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___exteriorBotRight_21 = value;
		Il2CppCodeGenWriteBarrier((&___exteriorBotRight_21), value);
	}

	inline static int32_t get_offset_of_rotationEBL_22() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___rotationEBL_22)); }
	inline int32_t get_rotationEBL_22() const { return ___rotationEBL_22; }
	inline int32_t* get_address_of_rotationEBL_22() { return &___rotationEBL_22; }
	inline void set_rotationEBL_22(int32_t value)
	{
		___rotationEBL_22 = value;
	}

	inline static int32_t get_offset_of_offsetEBR_23() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___offsetEBR_23)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_offsetEBR_23() const { return ___offsetEBR_23; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_offsetEBR_23() { return &___offsetEBR_23; }
	inline void set_offsetEBR_23(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___offsetEBR_23 = value;
	}

	inline static int32_t get_offset_of_rotationEBR_24() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___rotationEBR_24)); }
	inline int32_t get_rotationEBR_24() const { return ___rotationEBR_24; }
	inline int32_t* get_address_of_rotationEBR_24() { return &___rotationEBR_24; }
	inline void set_rotationEBR_24(int32_t value)
	{
		___rotationEBR_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORNERSPRITES2_T56F48A9FF2507A11F647BF6F8B9F6B163B5A1703_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#define GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifndef MESH_T6106B8D8E4C691321581AB0445552EC78B947B8C_H
#define MESH_T6106B8D8E4C691321581AB0445552EC78B947B8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Mesh
struct  Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_T6106B8D8E4C691321581AB0445552EC78B947B8C_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef SPRITE_TCA09498D612D08DE668653AF1E9C12BF53434198_H
#define SPRITE_TCA09498D612D08DE668653AF1E9C12BF53434198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprite
struct  Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_TCA09498D612D08DE668653AF1E9C12BF53434198_H
#ifndef TEXTURE_T387FE83BB848001FD06B14707AEA6D5A0F6A95F4_H
#define TEXTURE_T387FE83BB848001FD06B14707AEA6D5A0F6A95F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T387FE83BB848001FD06B14707AEA6D5A0F6A95F4_H
#ifndef TERRAIN2DARCHITECTUREANGLES_T804E8203AA786D1A464DF24D7EAFC66109AC4208_H
#define TERRAIN2DARCHITECTUREANGLES_T804E8203AA786D1A464DF24D7EAFC66109AC4208_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2DArchitectureAngles
struct  Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Single Kolibri2d.Terrain2DArchitectureAngles::maxGroundAngle
	float ___maxGroundAngle_5;
	// System.Single Kolibri2d.Terrain2DArchitectureAngles::maxCeilingAngle
	float ___maxCeilingAngle_6;
	// System.Boolean Kolibri2d.Terrain2DArchitectureAngles::replaceSegments
	bool ___replaceSegments_7;
	// Kolibri2d.TerrainType Kolibri2d.Terrain2DArchitectureAngles::replaceGroundWith
	int32_t ___replaceGroundWith_8;
	// Kolibri2d.TerrainType Kolibri2d.Terrain2DArchitectureAngles::replaceWallsWith
	int32_t ___replaceWallsWith_9;
	// Kolibri2d.TerrainType Kolibri2d.Terrain2DArchitectureAngles::replaceCeilingWith
	int32_t ___replaceCeilingWith_10;

public:
	inline static int32_t get_offset_of_maxGroundAngle_5() { return static_cast<int32_t>(offsetof(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208, ___maxGroundAngle_5)); }
	inline float get_maxGroundAngle_5() const { return ___maxGroundAngle_5; }
	inline float* get_address_of_maxGroundAngle_5() { return &___maxGroundAngle_5; }
	inline void set_maxGroundAngle_5(float value)
	{
		___maxGroundAngle_5 = value;
	}

	inline static int32_t get_offset_of_maxCeilingAngle_6() { return static_cast<int32_t>(offsetof(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208, ___maxCeilingAngle_6)); }
	inline float get_maxCeilingAngle_6() const { return ___maxCeilingAngle_6; }
	inline float* get_address_of_maxCeilingAngle_6() { return &___maxCeilingAngle_6; }
	inline void set_maxCeilingAngle_6(float value)
	{
		___maxCeilingAngle_6 = value;
	}

	inline static int32_t get_offset_of_replaceSegments_7() { return static_cast<int32_t>(offsetof(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208, ___replaceSegments_7)); }
	inline bool get_replaceSegments_7() const { return ___replaceSegments_7; }
	inline bool* get_address_of_replaceSegments_7() { return &___replaceSegments_7; }
	inline void set_replaceSegments_7(bool value)
	{
		___replaceSegments_7 = value;
	}

	inline static int32_t get_offset_of_replaceGroundWith_8() { return static_cast<int32_t>(offsetof(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208, ___replaceGroundWith_8)); }
	inline int32_t get_replaceGroundWith_8() const { return ___replaceGroundWith_8; }
	inline int32_t* get_address_of_replaceGroundWith_8() { return &___replaceGroundWith_8; }
	inline void set_replaceGroundWith_8(int32_t value)
	{
		___replaceGroundWith_8 = value;
	}

	inline static int32_t get_offset_of_replaceWallsWith_9() { return static_cast<int32_t>(offsetof(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208, ___replaceWallsWith_9)); }
	inline int32_t get_replaceWallsWith_9() const { return ___replaceWallsWith_9; }
	inline int32_t* get_address_of_replaceWallsWith_9() { return &___replaceWallsWith_9; }
	inline void set_replaceWallsWith_9(int32_t value)
	{
		___replaceWallsWith_9 = value;
	}

	inline static int32_t get_offset_of_replaceCeilingWith_10() { return static_cast<int32_t>(offsetof(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208, ___replaceCeilingWith_10)); }
	inline int32_t get_replaceCeilingWith_10() const { return ___replaceCeilingWith_10; }
	inline int32_t* get_address_of_replaceCeilingWith_10() { return &___replaceCeilingWith_10; }
	inline void set_replaceCeilingWith_10(int32_t value)
	{
		___replaceCeilingWith_10 = value;
	}
};

struct Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_StaticFields
{
public:
	// Kolibri2d.Terrain2DArchitectureAngles Kolibri2d.Terrain2DArchitectureAngles::_instance
	Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * ____instance_4;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_StaticFields, ____instance_4)); }
	inline Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * get__instance_4() const { return ____instance_4; }
	inline Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((&____instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERRAIN2DARCHITECTUREANGLES_T804E8203AA786D1A464DF24D7EAFC66109AC4208_H
#ifndef TERRAIN2DMATERIAL_TEEB7D9BF129459584A2806CF311886501F0EE44D_H
#define TERRAIN2DMATERIAL_TEEB7D9BF129459584A2806CF311886501F0EE44D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2DMaterial
struct  Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Single Kolibri2d.Terrain2DMaterial::scale
	float ___scale_4;
	// System.Single Kolibri2d.Terrain2DMaterial::offset
	float ___offset_5;
	// System.Collections.Generic.Dictionary`2<Kolibri2d.TerrainType,Kolibri2d.MaterialTextureInfo> Kolibri2d.Terrain2DMaterial::textureInfoByType
	Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A * ___textureInfoByType_6;
	// Kolibri2d.MaterialTextureInfo Kolibri2d.Terrain2DMaterial::infoGround
	MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  ___infoGround_7;
	// Kolibri2d.MaterialTextureInfo Kolibri2d.Terrain2DMaterial::infoWall
	MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  ___infoWall_8;
	// Kolibri2d.MaterialTextureInfo Kolibri2d.Terrain2DMaterial::infoCeiling
	MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  ___infoCeiling_9;
	// Kolibri2d.MaterialTextureInfo Kolibri2d.Terrain2DMaterial::infoFill
	MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  ___infoFill_10;
	// System.Boolean Kolibri2d.Terrain2DMaterial::useCorners
	bool ___useCorners_12;
	// System.Boolean Kolibri2d.Terrain2DMaterial::cornersFollowsPath
	bool ___cornersFollowsPath_13;
	// Kolibri2d.Terrain2DMaterial/CornerSprites2 Kolibri2d.Terrain2DMaterial::cornerSprites
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703 * ___cornerSprites_14;
	// Kolibri2d.Terrain2DMaterial/SortingLayerOptions2 Kolibri2d.Terrain2DMaterial::layersAndSorting
	SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE * ___layersAndSorting_15;
	// System.Boolean Kolibri2d.Terrain2DMaterial::splitPaths
	bool ___splitPaths_16;
	// System.Boolean Kolibri2d.Terrain2DMaterial::rotateTextures
	bool ___rotateTextures_17;
	// UnityEngine.Material Kolibri2d.Terrain2DMaterial::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_18;
	// Kolibri2d.SplineColliderSettings Kolibri2d.Terrain2DMaterial::settings
	SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4 * ___settings_19;
	// System.Int32 Kolibri2d.Terrain2DMaterial::objectLayer
	int32_t ___objectLayer_20;
	// Kolibri2d.Terrain2DMaterial/RenderOrderType Kolibri2d.Terrain2DMaterial::renderOrderType
	int32_t ___renderOrderType_21;
	// Kolibri2d.Terrain2DMaterial/SortingZ_3D_Options Kolibri2d.Terrain2DMaterial::z_depthSorting
	SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F * ___z_depthSorting_22;
	// UnityEngine.Color Kolibri2d.Terrain2DMaterial::ColorFill
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___ColorFill_23;
	// Kolibri2d.Terrain2DMaterial/FillType Kolibri2d.Terrain2DMaterial::fillType
	int32_t ___fillType_24;

public:
	inline static int32_t get_offset_of_scale_4() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___scale_4)); }
	inline float get_scale_4() const { return ___scale_4; }
	inline float* get_address_of_scale_4() { return &___scale_4; }
	inline void set_scale_4(float value)
	{
		___scale_4 = value;
	}

	inline static int32_t get_offset_of_offset_5() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___offset_5)); }
	inline float get_offset_5() const { return ___offset_5; }
	inline float* get_address_of_offset_5() { return &___offset_5; }
	inline void set_offset_5(float value)
	{
		___offset_5 = value;
	}

	inline static int32_t get_offset_of_textureInfoByType_6() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___textureInfoByType_6)); }
	inline Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A * get_textureInfoByType_6() const { return ___textureInfoByType_6; }
	inline Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A ** get_address_of_textureInfoByType_6() { return &___textureInfoByType_6; }
	inline void set_textureInfoByType_6(Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A * value)
	{
		___textureInfoByType_6 = value;
		Il2CppCodeGenWriteBarrier((&___textureInfoByType_6), value);
	}

	inline static int32_t get_offset_of_infoGround_7() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___infoGround_7)); }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  get_infoGround_7() const { return ___infoGround_7; }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F * get_address_of_infoGround_7() { return &___infoGround_7; }
	inline void set_infoGround_7(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  value)
	{
		___infoGround_7 = value;
	}

	inline static int32_t get_offset_of_infoWall_8() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___infoWall_8)); }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  get_infoWall_8() const { return ___infoWall_8; }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F * get_address_of_infoWall_8() { return &___infoWall_8; }
	inline void set_infoWall_8(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  value)
	{
		___infoWall_8 = value;
	}

	inline static int32_t get_offset_of_infoCeiling_9() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___infoCeiling_9)); }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  get_infoCeiling_9() const { return ___infoCeiling_9; }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F * get_address_of_infoCeiling_9() { return &___infoCeiling_9; }
	inline void set_infoCeiling_9(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  value)
	{
		___infoCeiling_9 = value;
	}

	inline static int32_t get_offset_of_infoFill_10() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___infoFill_10)); }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  get_infoFill_10() const { return ___infoFill_10; }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F * get_address_of_infoFill_10() { return &___infoFill_10; }
	inline void set_infoFill_10(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  value)
	{
		___infoFill_10 = value;
	}

	inline static int32_t get_offset_of_useCorners_12() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___useCorners_12)); }
	inline bool get_useCorners_12() const { return ___useCorners_12; }
	inline bool* get_address_of_useCorners_12() { return &___useCorners_12; }
	inline void set_useCorners_12(bool value)
	{
		___useCorners_12 = value;
	}

	inline static int32_t get_offset_of_cornersFollowsPath_13() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___cornersFollowsPath_13)); }
	inline bool get_cornersFollowsPath_13() const { return ___cornersFollowsPath_13; }
	inline bool* get_address_of_cornersFollowsPath_13() { return &___cornersFollowsPath_13; }
	inline void set_cornersFollowsPath_13(bool value)
	{
		___cornersFollowsPath_13 = value;
	}

	inline static int32_t get_offset_of_cornerSprites_14() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___cornerSprites_14)); }
	inline CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703 * get_cornerSprites_14() const { return ___cornerSprites_14; }
	inline CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703 ** get_address_of_cornerSprites_14() { return &___cornerSprites_14; }
	inline void set_cornerSprites_14(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703 * value)
	{
		___cornerSprites_14 = value;
		Il2CppCodeGenWriteBarrier((&___cornerSprites_14), value);
	}

	inline static int32_t get_offset_of_layersAndSorting_15() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___layersAndSorting_15)); }
	inline SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE * get_layersAndSorting_15() const { return ___layersAndSorting_15; }
	inline SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE ** get_address_of_layersAndSorting_15() { return &___layersAndSorting_15; }
	inline void set_layersAndSorting_15(SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE * value)
	{
		___layersAndSorting_15 = value;
		Il2CppCodeGenWriteBarrier((&___layersAndSorting_15), value);
	}

	inline static int32_t get_offset_of_splitPaths_16() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___splitPaths_16)); }
	inline bool get_splitPaths_16() const { return ___splitPaths_16; }
	inline bool* get_address_of_splitPaths_16() { return &___splitPaths_16; }
	inline void set_splitPaths_16(bool value)
	{
		___splitPaths_16 = value;
	}

	inline static int32_t get_offset_of_rotateTextures_17() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___rotateTextures_17)); }
	inline bool get_rotateTextures_17() const { return ___rotateTextures_17; }
	inline bool* get_address_of_rotateTextures_17() { return &___rotateTextures_17; }
	inline void set_rotateTextures_17(bool value)
	{
		___rotateTextures_17 = value;
	}

	inline static int32_t get_offset_of_material_18() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___material_18)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_18() const { return ___material_18; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_18() { return &___material_18; }
	inline void set_material_18(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_18 = value;
		Il2CppCodeGenWriteBarrier((&___material_18), value);
	}

	inline static int32_t get_offset_of_settings_19() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___settings_19)); }
	inline SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4 * get_settings_19() const { return ___settings_19; }
	inline SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4 ** get_address_of_settings_19() { return &___settings_19; }
	inline void set_settings_19(SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4 * value)
	{
		___settings_19 = value;
		Il2CppCodeGenWriteBarrier((&___settings_19), value);
	}

	inline static int32_t get_offset_of_objectLayer_20() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___objectLayer_20)); }
	inline int32_t get_objectLayer_20() const { return ___objectLayer_20; }
	inline int32_t* get_address_of_objectLayer_20() { return &___objectLayer_20; }
	inline void set_objectLayer_20(int32_t value)
	{
		___objectLayer_20 = value;
	}

	inline static int32_t get_offset_of_renderOrderType_21() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___renderOrderType_21)); }
	inline int32_t get_renderOrderType_21() const { return ___renderOrderType_21; }
	inline int32_t* get_address_of_renderOrderType_21() { return &___renderOrderType_21; }
	inline void set_renderOrderType_21(int32_t value)
	{
		___renderOrderType_21 = value;
	}

	inline static int32_t get_offset_of_z_depthSorting_22() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___z_depthSorting_22)); }
	inline SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F * get_z_depthSorting_22() const { return ___z_depthSorting_22; }
	inline SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F ** get_address_of_z_depthSorting_22() { return &___z_depthSorting_22; }
	inline void set_z_depthSorting_22(SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F * value)
	{
		___z_depthSorting_22 = value;
		Il2CppCodeGenWriteBarrier((&___z_depthSorting_22), value);
	}

	inline static int32_t get_offset_of_ColorFill_23() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___ColorFill_23)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_ColorFill_23() const { return ___ColorFill_23; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_ColorFill_23() { return &___ColorFill_23; }
	inline void set_ColorFill_23(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___ColorFill_23 = value;
	}

	inline static int32_t get_offset_of_fillType_24() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___fillType_24)); }
	inline int32_t get_fillType_24() const { return ___fillType_24; }
	inline int32_t* get_address_of_fillType_24() { return &___fillType_24; }
	inline void set_fillType_24(int32_t value)
	{
		___fillType_24 = value;
	}
};

struct Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D_StaticFields
{
public:
	// Kolibri2d.MaterialTextureInfo Kolibri2d.Terrain2DMaterial::infoEmpty
	MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  ___infoEmpty_11;

public:
	inline static int32_t get_offset_of_infoEmpty_11() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D_StaticFields, ___infoEmpty_11)); }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  get_infoEmpty_11() const { return ___infoEmpty_11; }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F * get_address_of_infoEmpty_11() { return &___infoEmpty_11; }
	inline void set_infoEmpty_11(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  value)
	{
		___infoEmpty_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERRAIN2DMATERIAL_TEEB7D9BF129459584A2806CF311886501F0EE44D_H
#ifndef ENUMERATOR_TDCC546968169F89B6BDD256ADFD2F97686EFF4FB_H
#define ENUMERATOR_TDCC546968169F89B6BDD256ADFD2F97686EFF4FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<Kolibri2d.SplineSegmentInfo>
struct  Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB, ___l_0)); }
	inline List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * get_l_0() const { return ___l_0; }
	inline List_1_t517344AED92C69B6645F0ABFA470063D220BACCD ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB, ___current_3)); }
	inline SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  get_current_3() const { return ___current_3; }
	inline SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_TDCC546968169F89B6BDD256ADFD2F97686EFF4FB_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef RENDERTEXTURE_TBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6_H
#define RENDERTEXTURE_TBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTexture
struct  RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6  : public Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTURE_TBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6_H
#ifndef RENDERER_T0556D67DD582620D1F495627EDE30D03284151F4_H
#define RENDERER_T0556D67DD582620D1F495627EDE30D03284151F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t0556D67DD582620D1F495627EDE30D03284151F4  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T0556D67DD582620D1F495627EDE30D03284151F4_H
#ifndef TEXTURE2D_TBBF96AC337723E2EF156DF17E09D4379FD05DE1C_H
#define TEXTURE2D_TBBF96AC337723E2EF156DF17E09D4379FD05DE1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C  : public Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_TBBF96AC337723E2EF156DF17E09D4379FD05DE1C_H
#ifndef TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#define TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef BEZIERSPLINE_T869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_H
#define BEZIERSPLINE_T869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.BezierSpline
struct  BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<Kolibri2d.BezierSpline/BezierPoint> Kolibri2d.BezierSpline::points
	List_1_tD5F01382C85482BD3A1133E16C27709D77F65384 * ___points_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Kolibri2d.BezierSpline::allStepPoints
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___allStepPoints_5;
	// System.Collections.Generic.List`1<System.Single> Kolibri2d.BezierSpline::allStepLengths01
	List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * ___allStepLengths01_6;
	// System.Collections.Generic.List`1<System.Single> Kolibri2d.BezierSpline::allStepLengths
	List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * ___allStepLengths_7;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Kolibri2d.BezierSpline::allStepDirections
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___allStepDirections_8;
	// UnityEngine.Vector2[] Kolibri2d.BezierSpline::allStepNormals
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___allStepNormals_9;
	// System.Boolean Kolibri2d.BezierSpline::showDirection
	bool ___showDirection_10;
	// System.Boolean Kolibri2d.BezierSpline::isClockwise
	bool ___isClockwise_11;
	// System.Single Kolibri2d.BezierSpline::smoothness
	float ___smoothness_12;
	// Kolibri2d.BezierSpline/OnRefreshCallback Kolibri2d.BezierSpline::onRefreshCallback
	OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300 * ___onRefreshCallback_13;
	// Kolibri2d.SplineData Kolibri2d.BezierSpline::spline
	SplineData_t97926CA36C3A0FF54D20743F14165941B0C11747 * ___spline_14;
	// System.Collections.Generic.List`1<Kolibri2d.BezierSpline/PointData> Kolibri2d.BezierSpline::perPointData
	List_1_t96B06A7F290D61DC5641A89066DC34043C6F5286 * ___perPointData_16;
	// System.Boolean Kolibri2d.BezierSpline::adjustTangentsOnMoving
	bool ___adjustTangentsOnMoving_24;
	// Kolibri2d.BezierSplineQuality Kolibri2d.BezierSpline::quality
	BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF * ___quality_25;

public:
	inline static int32_t get_offset_of_points_4() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___points_4)); }
	inline List_1_tD5F01382C85482BD3A1133E16C27709D77F65384 * get_points_4() const { return ___points_4; }
	inline List_1_tD5F01382C85482BD3A1133E16C27709D77F65384 ** get_address_of_points_4() { return &___points_4; }
	inline void set_points_4(List_1_tD5F01382C85482BD3A1133E16C27709D77F65384 * value)
	{
		___points_4 = value;
		Il2CppCodeGenWriteBarrier((&___points_4), value);
	}

	inline static int32_t get_offset_of_allStepPoints_5() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___allStepPoints_5)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_allStepPoints_5() const { return ___allStepPoints_5; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_allStepPoints_5() { return &___allStepPoints_5; }
	inline void set_allStepPoints_5(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___allStepPoints_5 = value;
		Il2CppCodeGenWriteBarrier((&___allStepPoints_5), value);
	}

	inline static int32_t get_offset_of_allStepLengths01_6() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___allStepLengths01_6)); }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * get_allStepLengths01_6() const { return ___allStepLengths01_6; }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E ** get_address_of_allStepLengths01_6() { return &___allStepLengths01_6; }
	inline void set_allStepLengths01_6(List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * value)
	{
		___allStepLengths01_6 = value;
		Il2CppCodeGenWriteBarrier((&___allStepLengths01_6), value);
	}

	inline static int32_t get_offset_of_allStepLengths_7() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___allStepLengths_7)); }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * get_allStepLengths_7() const { return ___allStepLengths_7; }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E ** get_address_of_allStepLengths_7() { return &___allStepLengths_7; }
	inline void set_allStepLengths_7(List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * value)
	{
		___allStepLengths_7 = value;
		Il2CppCodeGenWriteBarrier((&___allStepLengths_7), value);
	}

	inline static int32_t get_offset_of_allStepDirections_8() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___allStepDirections_8)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_allStepDirections_8() const { return ___allStepDirections_8; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_allStepDirections_8() { return &___allStepDirections_8; }
	inline void set_allStepDirections_8(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___allStepDirections_8 = value;
		Il2CppCodeGenWriteBarrier((&___allStepDirections_8), value);
	}

	inline static int32_t get_offset_of_allStepNormals_9() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___allStepNormals_9)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_allStepNormals_9() const { return ___allStepNormals_9; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_allStepNormals_9() { return &___allStepNormals_9; }
	inline void set_allStepNormals_9(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___allStepNormals_9 = value;
		Il2CppCodeGenWriteBarrier((&___allStepNormals_9), value);
	}

	inline static int32_t get_offset_of_showDirection_10() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___showDirection_10)); }
	inline bool get_showDirection_10() const { return ___showDirection_10; }
	inline bool* get_address_of_showDirection_10() { return &___showDirection_10; }
	inline void set_showDirection_10(bool value)
	{
		___showDirection_10 = value;
	}

	inline static int32_t get_offset_of_isClockwise_11() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___isClockwise_11)); }
	inline bool get_isClockwise_11() const { return ___isClockwise_11; }
	inline bool* get_address_of_isClockwise_11() { return &___isClockwise_11; }
	inline void set_isClockwise_11(bool value)
	{
		___isClockwise_11 = value;
	}

	inline static int32_t get_offset_of_smoothness_12() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___smoothness_12)); }
	inline float get_smoothness_12() const { return ___smoothness_12; }
	inline float* get_address_of_smoothness_12() { return &___smoothness_12; }
	inline void set_smoothness_12(float value)
	{
		___smoothness_12 = value;
	}

	inline static int32_t get_offset_of_onRefreshCallback_13() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___onRefreshCallback_13)); }
	inline OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300 * get_onRefreshCallback_13() const { return ___onRefreshCallback_13; }
	inline OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300 ** get_address_of_onRefreshCallback_13() { return &___onRefreshCallback_13; }
	inline void set_onRefreshCallback_13(OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300 * value)
	{
		___onRefreshCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___onRefreshCallback_13), value);
	}

	inline static int32_t get_offset_of_spline_14() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___spline_14)); }
	inline SplineData_t97926CA36C3A0FF54D20743F14165941B0C11747 * get_spline_14() const { return ___spline_14; }
	inline SplineData_t97926CA36C3A0FF54D20743F14165941B0C11747 ** get_address_of_spline_14() { return &___spline_14; }
	inline void set_spline_14(SplineData_t97926CA36C3A0FF54D20743F14165941B0C11747 * value)
	{
		___spline_14 = value;
		Il2CppCodeGenWriteBarrier((&___spline_14), value);
	}

	inline static int32_t get_offset_of_perPointData_16() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___perPointData_16)); }
	inline List_1_t96B06A7F290D61DC5641A89066DC34043C6F5286 * get_perPointData_16() const { return ___perPointData_16; }
	inline List_1_t96B06A7F290D61DC5641A89066DC34043C6F5286 ** get_address_of_perPointData_16() { return &___perPointData_16; }
	inline void set_perPointData_16(List_1_t96B06A7F290D61DC5641A89066DC34043C6F5286 * value)
	{
		___perPointData_16 = value;
		Il2CppCodeGenWriteBarrier((&___perPointData_16), value);
	}

	inline static int32_t get_offset_of_adjustTangentsOnMoving_24() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___adjustTangentsOnMoving_24)); }
	inline bool get_adjustTangentsOnMoving_24() const { return ___adjustTangentsOnMoving_24; }
	inline bool* get_address_of_adjustTangentsOnMoving_24() { return &___adjustTangentsOnMoving_24; }
	inline void set_adjustTangentsOnMoving_24(bool value)
	{
		___adjustTangentsOnMoving_24 = value;
	}

	inline static int32_t get_offset_of_quality_25() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___quality_25)); }
	inline BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF * get_quality_25() const { return ___quality_25; }
	inline BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF ** get_address_of_quality_25() { return &___quality_25; }
	inline void set_quality_25(BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF * value)
	{
		___quality_25 = value;
		Il2CppCodeGenWriteBarrier((&___quality_25), value);
	}
};

struct BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields
{
public:
	// System.Single Kolibri2d.BezierSpline::KEpsilon
	float ___KEpsilon_15;
	// Kolibri2d.ShapeTangentMode Kolibri2d.BezierSpline::settings_shapeTangentMode
	int32_t ___settings_shapeTangentMode_17;
	// System.Single Kolibri2d.BezierSpline::settings_smoothness
	float ___settings_smoothness_18;
	// System.Boolean Kolibri2d.BezierSpline::settings_squareLinearTangents
	bool ___settings_squareLinearTangents_19;
	// System.Boolean Kolibri2d.BezierSpline::settings_autoSelectPoints
	bool ___settings_autoSelectPoints_20;
	// System.Boolean Kolibri2d.BezierSpline::settings_pointsAlwaysVisible
	bool ___settings_pointsAlwaysVisible_21;
	// System.Single Kolibri2d.BezierSpline::settings_sceneHandleSize
	float ___settings_sceneHandleSize_22;
	// Kolibri2d.BezierSplineQuality Kolibri2d.BezierSpline::settings_quality
	BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF * ___settings_quality_23;

public:
	inline static int32_t get_offset_of_KEpsilon_15() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields, ___KEpsilon_15)); }
	inline float get_KEpsilon_15() const { return ___KEpsilon_15; }
	inline float* get_address_of_KEpsilon_15() { return &___KEpsilon_15; }
	inline void set_KEpsilon_15(float value)
	{
		___KEpsilon_15 = value;
	}

	inline static int32_t get_offset_of_settings_shapeTangentMode_17() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields, ___settings_shapeTangentMode_17)); }
	inline int32_t get_settings_shapeTangentMode_17() const { return ___settings_shapeTangentMode_17; }
	inline int32_t* get_address_of_settings_shapeTangentMode_17() { return &___settings_shapeTangentMode_17; }
	inline void set_settings_shapeTangentMode_17(int32_t value)
	{
		___settings_shapeTangentMode_17 = value;
	}

	inline static int32_t get_offset_of_settings_smoothness_18() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields, ___settings_smoothness_18)); }
	inline float get_settings_smoothness_18() const { return ___settings_smoothness_18; }
	inline float* get_address_of_settings_smoothness_18() { return &___settings_smoothness_18; }
	inline void set_settings_smoothness_18(float value)
	{
		___settings_smoothness_18 = value;
	}

	inline static int32_t get_offset_of_settings_squareLinearTangents_19() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields, ___settings_squareLinearTangents_19)); }
	inline bool get_settings_squareLinearTangents_19() const { return ___settings_squareLinearTangents_19; }
	inline bool* get_address_of_settings_squareLinearTangents_19() { return &___settings_squareLinearTangents_19; }
	inline void set_settings_squareLinearTangents_19(bool value)
	{
		___settings_squareLinearTangents_19 = value;
	}

	inline static int32_t get_offset_of_settings_autoSelectPoints_20() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields, ___settings_autoSelectPoints_20)); }
	inline bool get_settings_autoSelectPoints_20() const { return ___settings_autoSelectPoints_20; }
	inline bool* get_address_of_settings_autoSelectPoints_20() { return &___settings_autoSelectPoints_20; }
	inline void set_settings_autoSelectPoints_20(bool value)
	{
		___settings_autoSelectPoints_20 = value;
	}

	inline static int32_t get_offset_of_settings_pointsAlwaysVisible_21() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields, ___settings_pointsAlwaysVisible_21)); }
	inline bool get_settings_pointsAlwaysVisible_21() const { return ___settings_pointsAlwaysVisible_21; }
	inline bool* get_address_of_settings_pointsAlwaysVisible_21() { return &___settings_pointsAlwaysVisible_21; }
	inline void set_settings_pointsAlwaysVisible_21(bool value)
	{
		___settings_pointsAlwaysVisible_21 = value;
	}

	inline static int32_t get_offset_of_settings_sceneHandleSize_22() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields, ___settings_sceneHandleSize_22)); }
	inline float get_settings_sceneHandleSize_22() const { return ___settings_sceneHandleSize_22; }
	inline float* get_address_of_settings_sceneHandleSize_22() { return &___settings_sceneHandleSize_22; }
	inline void set_settings_sceneHandleSize_22(float value)
	{
		___settings_sceneHandleSize_22 = value;
	}

	inline static int32_t get_offset_of_settings_quality_23() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields, ___settings_quality_23)); }
	inline BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF * get_settings_quality_23() const { return ___settings_quality_23; }
	inline BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF ** get_address_of_settings_quality_23() { return &___settings_quality_23; }
	inline void set_settings_quality_23(BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF * value)
	{
		___settings_quality_23 = value;
		Il2CppCodeGenWriteBarrier((&___settings_quality_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEZIERSPLINE_T869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_H
#ifndef SPLINECOLLIDERCREATOR_TF1A0739E71C3626B1789AA6E68D8BD5244C83B1A_H
#define SPLINECOLLIDERCREATOR_TF1A0739E71C3626B1789AA6E68D8BD5244C83B1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineColliderCreator
struct  SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Kolibri2d.BezierSpline Kolibri2d.SplineColliderCreator::spline
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * ___spline_5;
	// Kolibri2d.Terrain2DArchitecture Kolibri2d.SplineColliderCreator::arch
	Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * ___arch_6;
	// System.Boolean Kolibri2d.SplineColliderCreator::useGameobjectLayer
	bool ___useGameobjectLayer_7;
	// Kolibri2d.SplineColliderSettings Kolibri2d.SplineColliderCreator::physicsSettings
	SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4 * ___physicsSettings_8;
	// System.Boolean Kolibri2d.SplineColliderCreator::refreshOnNextUpdate
	bool ___refreshOnNextUpdate_9;
	// Kolibri2d.BezierSpline/OnRefreshCallback Kolibri2d.SplineColliderCreator::refreshCallback
	OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300 * ___refreshCallback_10;

public:
	inline static int32_t get_offset_of_spline_5() { return static_cast<int32_t>(offsetof(SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A, ___spline_5)); }
	inline BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * get_spline_5() const { return ___spline_5; }
	inline BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE ** get_address_of_spline_5() { return &___spline_5; }
	inline void set_spline_5(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * value)
	{
		___spline_5 = value;
		Il2CppCodeGenWriteBarrier((&___spline_5), value);
	}

	inline static int32_t get_offset_of_arch_6() { return static_cast<int32_t>(offsetof(SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A, ___arch_6)); }
	inline Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * get_arch_6() const { return ___arch_6; }
	inline Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A ** get_address_of_arch_6() { return &___arch_6; }
	inline void set_arch_6(Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * value)
	{
		___arch_6 = value;
		Il2CppCodeGenWriteBarrier((&___arch_6), value);
	}

	inline static int32_t get_offset_of_useGameobjectLayer_7() { return static_cast<int32_t>(offsetof(SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A, ___useGameobjectLayer_7)); }
	inline bool get_useGameobjectLayer_7() const { return ___useGameobjectLayer_7; }
	inline bool* get_address_of_useGameobjectLayer_7() { return &___useGameobjectLayer_7; }
	inline void set_useGameobjectLayer_7(bool value)
	{
		___useGameobjectLayer_7 = value;
	}

	inline static int32_t get_offset_of_physicsSettings_8() { return static_cast<int32_t>(offsetof(SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A, ___physicsSettings_8)); }
	inline SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4 * get_physicsSettings_8() const { return ___physicsSettings_8; }
	inline SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4 ** get_address_of_physicsSettings_8() { return &___physicsSettings_8; }
	inline void set_physicsSettings_8(SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4 * value)
	{
		___physicsSettings_8 = value;
		Il2CppCodeGenWriteBarrier((&___physicsSettings_8), value);
	}

	inline static int32_t get_offset_of_refreshOnNextUpdate_9() { return static_cast<int32_t>(offsetof(SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A, ___refreshOnNextUpdate_9)); }
	inline bool get_refreshOnNextUpdate_9() const { return ___refreshOnNextUpdate_9; }
	inline bool* get_address_of_refreshOnNextUpdate_9() { return &___refreshOnNextUpdate_9; }
	inline void set_refreshOnNextUpdate_9(bool value)
	{
		___refreshOnNextUpdate_9 = value;
	}

	inline static int32_t get_offset_of_refreshCallback_10() { return static_cast<int32_t>(offsetof(SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A, ___refreshCallback_10)); }
	inline OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300 * get_refreshCallback_10() const { return ___refreshCallback_10; }
	inline OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300 ** get_address_of_refreshCallback_10() { return &___refreshCallback_10; }
	inline void set_refreshCallback_10(OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300 * value)
	{
		___refreshCallback_10 = value;
		Il2CppCodeGenWriteBarrier((&___refreshCallback_10), value);
	}
};

struct SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A_StaticFields
{
public:
	// System.String Kolibri2d.SplineColliderCreator::decorationNodeName
	String_t* ___decorationNodeName_4;

public:
	inline static int32_t get_offset_of_decorationNodeName_4() { return static_cast<int32_t>(offsetof(SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A_StaticFields, ___decorationNodeName_4)); }
	inline String_t* get_decorationNodeName_4() const { return ___decorationNodeName_4; }
	inline String_t** get_address_of_decorationNodeName_4() { return &___decorationNodeName_4; }
	inline void set_decorationNodeName_4(String_t* value)
	{
		___decorationNodeName_4 = value;
		Il2CppCodeGenWriteBarrier((&___decorationNodeName_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINECOLLIDERCREATOR_TF1A0739E71C3626B1789AA6E68D8BD5244C83B1A_H
#ifndef SPLINEMODIFIERWITHCALLBACKS_T6F0DF8A674D295BBD21F986A7733B3E12CAE1232_H
#define SPLINEMODIFIERWITHCALLBACKS_T6F0DF8A674D295BBD21F986A7733B3E12CAE1232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineModifierWithCallbacks
struct  SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Kolibri2d.BezierSpline Kolibri2d.SplineModifierWithCallbacks::spline
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * ___spline_4;
	// Kolibri2d.Terrain2DArchitecture Kolibri2d.SplineModifierWithCallbacks::arch
	Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * ___arch_5;
	// System.Boolean Kolibri2d.SplineModifierWithCallbacks::refreshOnNextUpdate
	bool ___refreshOnNextUpdate_6;
	// Kolibri2d.BezierSpline/OnRefreshCallback Kolibri2d.SplineModifierWithCallbacks::refreshCallback
	OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300 * ___refreshCallback_7;

public:
	inline static int32_t get_offset_of_spline_4() { return static_cast<int32_t>(offsetof(SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232, ___spline_4)); }
	inline BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * get_spline_4() const { return ___spline_4; }
	inline BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE ** get_address_of_spline_4() { return &___spline_4; }
	inline void set_spline_4(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * value)
	{
		___spline_4 = value;
		Il2CppCodeGenWriteBarrier((&___spline_4), value);
	}

	inline static int32_t get_offset_of_arch_5() { return static_cast<int32_t>(offsetof(SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232, ___arch_5)); }
	inline Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * get_arch_5() const { return ___arch_5; }
	inline Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A ** get_address_of_arch_5() { return &___arch_5; }
	inline void set_arch_5(Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * value)
	{
		___arch_5 = value;
		Il2CppCodeGenWriteBarrier((&___arch_5), value);
	}

	inline static int32_t get_offset_of_refreshOnNextUpdate_6() { return static_cast<int32_t>(offsetof(SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232, ___refreshOnNextUpdate_6)); }
	inline bool get_refreshOnNextUpdate_6() const { return ___refreshOnNextUpdate_6; }
	inline bool* get_address_of_refreshOnNextUpdate_6() { return &___refreshOnNextUpdate_6; }
	inline void set_refreshOnNextUpdate_6(bool value)
	{
		___refreshOnNextUpdate_6 = value;
	}

	inline static int32_t get_offset_of_refreshCallback_7() { return static_cast<int32_t>(offsetof(SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232, ___refreshCallback_7)); }
	inline OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300 * get_refreshCallback_7() const { return ___refreshCallback_7; }
	inline OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300 ** get_address_of_refreshCallback_7() { return &___refreshCallback_7; }
	inline void set_refreshCallback_7(OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300 * value)
	{
		___refreshCallback_7 = value;
		Il2CppCodeGenWriteBarrier((&___refreshCallback_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINEMODIFIERWITHCALLBACKS_T6F0DF8A674D295BBD21F986A7733B3E12CAE1232_H
#ifndef TERRAIN2DARCHITECTURE_T01B2C8A0361E9A7595B6F9267322714F6672DB3A_H
#define TERRAIN2DARCHITECTURE_T01B2C8A0361E9A7595B6F9267322714F6672DB3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2DArchitecture
struct  Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Kolibri2d.BezierSpline Kolibri2d.Terrain2DArchitecture::spline
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * ___spline_4;
	// Kolibri2d.Terrain2DArchitecture/Aspect Kolibri2d.Terrain2DArchitecture::aspect
	int32_t ___aspect_5;
	// System.Single Kolibri2d.Terrain2DArchitecture::exteriorMargin
	float ___exteriorMargin_6;
	// System.Boolean Kolibri2d.Terrain2DArchitecture::manualFlip
	bool ___manualFlip_7;
	// Kolibri2d.Terrain2DArchitectureAngles Kolibri2d.Terrain2DArchitecture::overrideAngle
	Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * ___overrideAngle_8;
	// Kolibri2d.Terrain2DArchitectureAngles Kolibri2d.Terrain2DArchitecture::currArchAngle
	Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * ___currArchAngle_9;
	// UnityEngine.Vector2[] Kolibri2d.Terrain2DArchitecture::stepPointNormals
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___stepPointNormals_10;
	// UnityEngine.Vector2[] Kolibri2d.Terrain2DArchitecture::stepPointNormalsSmooth
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___stepPointNormalsSmooth_11;
	// System.Collections.Generic.List`1<Kolibri2d.SplineSegmentInfo> Kolibri2d.Terrain2DArchitecture::segmentsInfo
	List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * ___segmentsInfo_12;

public:
	inline static int32_t get_offset_of_spline_4() { return static_cast<int32_t>(offsetof(Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A, ___spline_4)); }
	inline BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * get_spline_4() const { return ___spline_4; }
	inline BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE ** get_address_of_spline_4() { return &___spline_4; }
	inline void set_spline_4(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * value)
	{
		___spline_4 = value;
		Il2CppCodeGenWriteBarrier((&___spline_4), value);
	}

	inline static int32_t get_offset_of_aspect_5() { return static_cast<int32_t>(offsetof(Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A, ___aspect_5)); }
	inline int32_t get_aspect_5() const { return ___aspect_5; }
	inline int32_t* get_address_of_aspect_5() { return &___aspect_5; }
	inline void set_aspect_5(int32_t value)
	{
		___aspect_5 = value;
	}

	inline static int32_t get_offset_of_exteriorMargin_6() { return static_cast<int32_t>(offsetof(Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A, ___exteriorMargin_6)); }
	inline float get_exteriorMargin_6() const { return ___exteriorMargin_6; }
	inline float* get_address_of_exteriorMargin_6() { return &___exteriorMargin_6; }
	inline void set_exteriorMargin_6(float value)
	{
		___exteriorMargin_6 = value;
	}

	inline static int32_t get_offset_of_manualFlip_7() { return static_cast<int32_t>(offsetof(Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A, ___manualFlip_7)); }
	inline bool get_manualFlip_7() const { return ___manualFlip_7; }
	inline bool* get_address_of_manualFlip_7() { return &___manualFlip_7; }
	inline void set_manualFlip_7(bool value)
	{
		___manualFlip_7 = value;
	}

	inline static int32_t get_offset_of_overrideAngle_8() { return static_cast<int32_t>(offsetof(Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A, ___overrideAngle_8)); }
	inline Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * get_overrideAngle_8() const { return ___overrideAngle_8; }
	inline Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 ** get_address_of_overrideAngle_8() { return &___overrideAngle_8; }
	inline void set_overrideAngle_8(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * value)
	{
		___overrideAngle_8 = value;
		Il2CppCodeGenWriteBarrier((&___overrideAngle_8), value);
	}

	inline static int32_t get_offset_of_currArchAngle_9() { return static_cast<int32_t>(offsetof(Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A, ___currArchAngle_9)); }
	inline Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * get_currArchAngle_9() const { return ___currArchAngle_9; }
	inline Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 ** get_address_of_currArchAngle_9() { return &___currArchAngle_9; }
	inline void set_currArchAngle_9(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * value)
	{
		___currArchAngle_9 = value;
		Il2CppCodeGenWriteBarrier((&___currArchAngle_9), value);
	}

	inline static int32_t get_offset_of_stepPointNormals_10() { return static_cast<int32_t>(offsetof(Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A, ___stepPointNormals_10)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_stepPointNormals_10() const { return ___stepPointNormals_10; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_stepPointNormals_10() { return &___stepPointNormals_10; }
	inline void set_stepPointNormals_10(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___stepPointNormals_10 = value;
		Il2CppCodeGenWriteBarrier((&___stepPointNormals_10), value);
	}

	inline static int32_t get_offset_of_stepPointNormalsSmooth_11() { return static_cast<int32_t>(offsetof(Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A, ___stepPointNormalsSmooth_11)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_stepPointNormalsSmooth_11() const { return ___stepPointNormalsSmooth_11; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_stepPointNormalsSmooth_11() { return &___stepPointNormalsSmooth_11; }
	inline void set_stepPointNormalsSmooth_11(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___stepPointNormalsSmooth_11 = value;
		Il2CppCodeGenWriteBarrier((&___stepPointNormalsSmooth_11), value);
	}

	inline static int32_t get_offset_of_segmentsInfo_12() { return static_cast<int32_t>(offsetof(Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A, ___segmentsInfo_12)); }
	inline List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * get_segmentsInfo_12() const { return ___segmentsInfo_12; }
	inline List_1_t517344AED92C69B6645F0ABFA470063D220BACCD ** get_address_of_segmentsInfo_12() { return &___segmentsInfo_12; }
	inline void set_segmentsInfo_12(List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * value)
	{
		___segmentsInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___segmentsInfo_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERRAIN2DARCHITECTURE_T01B2C8A0361E9A7595B6F9267322714F6672DB3A_H
#ifndef UTILS_T6B1F49395E2728FC7087CA82F420C8183CECBD71_H
#define UTILS_T6B1F49395E2728FC7087CA82F420C8183CECBD71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Utils
struct  Utils_t6B1F49395E2728FC7087CA82F420C8183CECBD71  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILS_T6B1F49395E2728FC7087CA82F420C8183CECBD71_H
#ifndef WATERTESTPANEL_TE3F44AF75B167F1003B7110B16C9ACE0D09EDC80_H
#define WATERTESTPANEL_TE3F44AF75B167F1003B7110B16C9ACE0D09EDC80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.WaterTestPanel
struct  WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Kolibri2d.SplineWater Kolibri2d.WaterTestPanel::water
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8 * ___water_4;
	// System.Single Kolibri2d.WaterTestPanel::lastForceRaw
	float ___lastForceRaw_5;
	// System.Single Kolibri2d.WaterTestPanel::lastForceAfterMultiplier
	float ___lastForceAfterMultiplier_6;
	// System.Boolean Kolibri2d.WaterTestPanel::lastForceIgnored
	bool ___lastForceIgnored_7;
	// System.Boolean Kolibri2d.WaterTestPanel::lastForceClamped
	bool ___lastForceClamped_8;
	// System.Single Kolibri2d.WaterTestPanel::lastForceAfterClamp
	float ___lastForceAfterClamp_9;

public:
	inline static int32_t get_offset_of_water_4() { return static_cast<int32_t>(offsetof(WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80, ___water_4)); }
	inline SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8 * get_water_4() const { return ___water_4; }
	inline SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8 ** get_address_of_water_4() { return &___water_4; }
	inline void set_water_4(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8 * value)
	{
		___water_4 = value;
		Il2CppCodeGenWriteBarrier((&___water_4), value);
	}

	inline static int32_t get_offset_of_lastForceRaw_5() { return static_cast<int32_t>(offsetof(WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80, ___lastForceRaw_5)); }
	inline float get_lastForceRaw_5() const { return ___lastForceRaw_5; }
	inline float* get_address_of_lastForceRaw_5() { return &___lastForceRaw_5; }
	inline void set_lastForceRaw_5(float value)
	{
		___lastForceRaw_5 = value;
	}

	inline static int32_t get_offset_of_lastForceAfterMultiplier_6() { return static_cast<int32_t>(offsetof(WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80, ___lastForceAfterMultiplier_6)); }
	inline float get_lastForceAfterMultiplier_6() const { return ___lastForceAfterMultiplier_6; }
	inline float* get_address_of_lastForceAfterMultiplier_6() { return &___lastForceAfterMultiplier_6; }
	inline void set_lastForceAfterMultiplier_6(float value)
	{
		___lastForceAfterMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_lastForceIgnored_7() { return static_cast<int32_t>(offsetof(WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80, ___lastForceIgnored_7)); }
	inline bool get_lastForceIgnored_7() const { return ___lastForceIgnored_7; }
	inline bool* get_address_of_lastForceIgnored_7() { return &___lastForceIgnored_7; }
	inline void set_lastForceIgnored_7(bool value)
	{
		___lastForceIgnored_7 = value;
	}

	inline static int32_t get_offset_of_lastForceClamped_8() { return static_cast<int32_t>(offsetof(WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80, ___lastForceClamped_8)); }
	inline bool get_lastForceClamped_8() const { return ___lastForceClamped_8; }
	inline bool* get_address_of_lastForceClamped_8() { return &___lastForceClamped_8; }
	inline void set_lastForceClamped_8(bool value)
	{
		___lastForceClamped_8 = value;
	}

	inline static int32_t get_offset_of_lastForceAfterClamp_9() { return static_cast<int32_t>(offsetof(WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80, ___lastForceAfterClamp_9)); }
	inline float get_lastForceAfterClamp_9() const { return ___lastForceAfterClamp_9; }
	inline float* get_address_of_lastForceAfterClamp_9() { return &___lastForceAfterClamp_9; }
	inline void set_lastForceAfterClamp_9(float value)
	{
		___lastForceAfterClamp_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERTESTPANEL_TE3F44AF75B167F1003B7110B16C9ACE0D09EDC80_H
#ifndef SPLINEWATER_T28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8_H
#define SPLINEWATER_T28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineWater
struct  SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8  : public SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232
{
public:
	// Kolibri2d.SplineWater/WaterEvent Kolibri2d.SplineWater::onEnterEvent
	WaterEvent_t414CD81AE9F28B656088F21ECC14F030B1FDA33B * ___onEnterEvent_8;
	// Kolibri2d.SplineWater/WaterEvent Kolibri2d.SplineWater::onExitEvent
	WaterEvent_t414CD81AE9F28B656088F21ECC14F030B1FDA33B * ___onExitEvent_9;
	// System.Single Kolibri2d.SplineWater::depth
	float ___depth_10;
	// System.Int32 Kolibri2d.SplineWater::edgecount
	int32_t ___edgecount_11;
	// System.Int32 Kolibri2d.SplineWater::nodecount
	int32_t ___nodecount_12;
	// System.Single[] Kolibri2d.SplineWater::x
	SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* ___x_13;
	// System.Single[] Kolibri2d.SplineWater::y
	SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* ___y_14;
	// System.Single[] Kolibri2d.SplineWater::velocities
	SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* ___velocities_15;
	// System.Single[] Kolibri2d.SplineWater::accelerations
	SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* ___accelerations_16;
	// UnityEngine.Vector3[] Kolibri2d.SplineWater::baseVertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___baseVertices_17;
	// UnityEngine.Vector3[] Kolibri2d.SplineWater::meshVertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___meshVertices_18;
	// UnityEngine.Vector2[] Kolibri2d.SplineWater::normals
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___normals_19;
	// UnityEngine.Mesh Kolibri2d.SplineWater::mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh_20;
	// UnityEngine.MeshFilter Kolibri2d.SplineWater::filter
	MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * ___filter_21;
	// UnityEngine.GameObject Kolibri2d.SplineWater::meshObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___meshObject_22;
	// UnityEngine.MeshRenderer Kolibri2d.SplineWater::meshRenderer
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___meshRenderer_23;
	// UnityEngine.PolygonCollider2D Kolibri2d.SplineWater::polygonCollider
	PolygonCollider2D_t360CC66FD7BAF5F4FADE54EB23E58E95D0B8CD81 * ___polygonCollider_24;
	// UnityEngine.Material Kolibri2d.SplineWater::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_25;
	// UnityEngine.Material Kolibri2d.SplineWater::copyMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___copyMaterial_26;
	// Kolibri2d.MaterialTextureInfo Kolibri2d.SplineWater::textureInfo
	MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  ___textureInfo_27;
	// System.Int32 Kolibri2d.SplineWater::orderInLayer
	int32_t ___orderInLayer_28;
	// System.Int32 Kolibri2d.SplineWater::sortingLayer
	int32_t ___sortingLayer_29;
	// System.Boolean Kolibri2d.SplineWater::slowdownOnEnterOrLeave
	bool ___slowdownOnEnterOrLeave_30;
	// UnityEngine.Material Kolibri2d.SplineWater::edgeMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___edgeMaterial_31;
	// UnityEngine.LineRenderer Kolibri2d.SplineWater::line
	LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * ___line_32;
	// System.Boolean Kolibri2d.SplineWater::onEnter
	bool ___onEnter_33;
	// System.Boolean Kolibri2d.SplineWater::onExit
	bool ___onExit_34;
	// System.Single Kolibri2d.SplineWater::mass
	float ___mass_35;
	// System.Single Kolibri2d.SplineWater::spread
	float ___spread_36;
	// System.Single Kolibri2d.SplineWater::springconstant
	float ___springconstant_37;
	// System.Single Kolibri2d.SplineWater::damping
	float ___damping_38;
	// System.Single Kolibri2d.SplineWater::forceMultiplier
	float ___forceMultiplier_39;
	// System.Single Kolibri2d.SplineWater::forceThreshold
	float ___forceThreshold_40;
	// System.Single Kolibri2d.SplineWater::forceClampLow
	float ___forceClampLow_41;
	// System.Single Kolibri2d.SplineWater::forceClampHigh
	float ___forceClampHigh_42;
	// System.Boolean Kolibri2d.SplineWater::waveLimitTop
	bool ___waveLimitTop_43;
	// System.Single Kolibri2d.SplineWater::waveLimitDistance
	float ___waveLimitDistance_44;
	// System.Single Kolibri2d.SplineWater::forceOnEdgesTop
	float ___forceOnEdgesTop_45;
	// System.Single Kolibri2d.SplineWater::forceOnEdgesBottom
	float ___forceOnEdgesBottom_46;
	// System.Int32 Kolibri2d.SplineWater::edgeSize
	int32_t ___edgeSize_47;
	// System.Boolean Kolibri2d.SplineWater::physicsEnabled
	bool ___physicsEnabled_49;
	// System.Boolean Kolibri2d.SplineWater::stayOnEdge
	bool ___stayOnEdge_50;
	// Kolibri2d.SplineWater/ForceDirection Kolibri2d.SplineWater::forceDirection
	int32_t ___forceDirection_51;
	// System.Single Kolibri2d.SplineWater::forceMagnitude
	float ___forceMagnitude_52;
	// System.Single Kolibri2d.SplineWater::floatThreshold
	float ___floatThreshold_53;
	// System.Single Kolibri2d.SplineWater::waterDensity
	float ___waterDensity_54;
	// System.Boolean Kolibri2d.SplineWater::forceDependsOnDistance
	bool ___forceDependsOnDistance_55;
	// System.Boolean Kolibri2d.SplineWater::forceProportionalToMass
	bool ___forceProportionalToMass_56;
	// System.Int32 Kolibri2d.SplineWater::touchingObjects
	int32_t ___touchingObjects_57;
	// UnityEngine.GameObject Kolibri2d.SplineWater::collidersObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___collidersObject_58;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Collider2D,System.Collections.Generic.List`1<System.Int32>> Kolibri2d.SplineWater::touchingColliders
	Dictionary_2_t97FE799FB5630EA306A1F53E2CA192562EF2E217 * ___touchingColliders_59;
	// UnityEngine.PolygonCollider2D[] Kolibri2d.SplineWater::innerColliders
	PolygonCollider2DU5BU5D_t740B9DF43CFB32BA191190CEA2770604FF8C0968* ___innerColliders_60;
	// UnityEngine.GameObject[] Kolibri2d.SplineWater::innerGOs
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___innerGOs_61;
	// System.Int32 Kolibri2d.SplineWater::pointsPerUnit
	int32_t ___pointsPerUnit_62;
	// System.Single Kolibri2d.SplineWater::delta
	float ___delta_63;
	// System.Collections.Generic.List`1<System.Single> Kolibri2d.SplineWater::lengths
	List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * ___lengths_64;
	// System.Collections.Generic.List`1<System.Single> Kolibri2d.SplineWater::lengths01
	List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * ___lengths01_65;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Kolibri2d.SplineWater::points
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___points_66;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Kolibri2d.SplineWater::tangentDirs
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___tangentDirs_67;
	// Kolibri2d.WaterTestPanel Kolibri2d.SplineWater::testPanel
	WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80 * ___testPanel_68;
	// Kolibri2d.SplineWater/MeshSegmentType Kolibri2d.SplineWater::meshSegmentType
	int32_t ___meshSegmentType_70;

public:
	inline static int32_t get_offset_of_onEnterEvent_8() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___onEnterEvent_8)); }
	inline WaterEvent_t414CD81AE9F28B656088F21ECC14F030B1FDA33B * get_onEnterEvent_8() const { return ___onEnterEvent_8; }
	inline WaterEvent_t414CD81AE9F28B656088F21ECC14F030B1FDA33B ** get_address_of_onEnterEvent_8() { return &___onEnterEvent_8; }
	inline void set_onEnterEvent_8(WaterEvent_t414CD81AE9F28B656088F21ECC14F030B1FDA33B * value)
	{
		___onEnterEvent_8 = value;
		Il2CppCodeGenWriteBarrier((&___onEnterEvent_8), value);
	}

	inline static int32_t get_offset_of_onExitEvent_9() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___onExitEvent_9)); }
	inline WaterEvent_t414CD81AE9F28B656088F21ECC14F030B1FDA33B * get_onExitEvent_9() const { return ___onExitEvent_9; }
	inline WaterEvent_t414CD81AE9F28B656088F21ECC14F030B1FDA33B ** get_address_of_onExitEvent_9() { return &___onExitEvent_9; }
	inline void set_onExitEvent_9(WaterEvent_t414CD81AE9F28B656088F21ECC14F030B1FDA33B * value)
	{
		___onExitEvent_9 = value;
		Il2CppCodeGenWriteBarrier((&___onExitEvent_9), value);
	}

	inline static int32_t get_offset_of_depth_10() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___depth_10)); }
	inline float get_depth_10() const { return ___depth_10; }
	inline float* get_address_of_depth_10() { return &___depth_10; }
	inline void set_depth_10(float value)
	{
		___depth_10 = value;
	}

	inline static int32_t get_offset_of_edgecount_11() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___edgecount_11)); }
	inline int32_t get_edgecount_11() const { return ___edgecount_11; }
	inline int32_t* get_address_of_edgecount_11() { return &___edgecount_11; }
	inline void set_edgecount_11(int32_t value)
	{
		___edgecount_11 = value;
	}

	inline static int32_t get_offset_of_nodecount_12() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___nodecount_12)); }
	inline int32_t get_nodecount_12() const { return ___nodecount_12; }
	inline int32_t* get_address_of_nodecount_12() { return &___nodecount_12; }
	inline void set_nodecount_12(int32_t value)
	{
		___nodecount_12 = value;
	}

	inline static int32_t get_offset_of_x_13() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___x_13)); }
	inline SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* get_x_13() const { return ___x_13; }
	inline SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5** get_address_of_x_13() { return &___x_13; }
	inline void set_x_13(SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* value)
	{
		___x_13 = value;
		Il2CppCodeGenWriteBarrier((&___x_13), value);
	}

	inline static int32_t get_offset_of_y_14() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___y_14)); }
	inline SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* get_y_14() const { return ___y_14; }
	inline SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5** get_address_of_y_14() { return &___y_14; }
	inline void set_y_14(SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* value)
	{
		___y_14 = value;
		Il2CppCodeGenWriteBarrier((&___y_14), value);
	}

	inline static int32_t get_offset_of_velocities_15() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___velocities_15)); }
	inline SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* get_velocities_15() const { return ___velocities_15; }
	inline SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5** get_address_of_velocities_15() { return &___velocities_15; }
	inline void set_velocities_15(SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* value)
	{
		___velocities_15 = value;
		Il2CppCodeGenWriteBarrier((&___velocities_15), value);
	}

	inline static int32_t get_offset_of_accelerations_16() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___accelerations_16)); }
	inline SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* get_accelerations_16() const { return ___accelerations_16; }
	inline SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5** get_address_of_accelerations_16() { return &___accelerations_16; }
	inline void set_accelerations_16(SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* value)
	{
		___accelerations_16 = value;
		Il2CppCodeGenWriteBarrier((&___accelerations_16), value);
	}

	inline static int32_t get_offset_of_baseVertices_17() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___baseVertices_17)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_baseVertices_17() const { return ___baseVertices_17; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_baseVertices_17() { return &___baseVertices_17; }
	inline void set_baseVertices_17(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___baseVertices_17 = value;
		Il2CppCodeGenWriteBarrier((&___baseVertices_17), value);
	}

	inline static int32_t get_offset_of_meshVertices_18() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___meshVertices_18)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_meshVertices_18() const { return ___meshVertices_18; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_meshVertices_18() { return &___meshVertices_18; }
	inline void set_meshVertices_18(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___meshVertices_18 = value;
		Il2CppCodeGenWriteBarrier((&___meshVertices_18), value);
	}

	inline static int32_t get_offset_of_normals_19() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___normals_19)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_normals_19() const { return ___normals_19; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_normals_19() { return &___normals_19; }
	inline void set_normals_19(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___normals_19 = value;
		Il2CppCodeGenWriteBarrier((&___normals_19), value);
	}

	inline static int32_t get_offset_of_mesh_20() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___mesh_20)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_mesh_20() const { return ___mesh_20; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_mesh_20() { return &___mesh_20; }
	inline void set_mesh_20(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___mesh_20 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_20), value);
	}

	inline static int32_t get_offset_of_filter_21() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___filter_21)); }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * get_filter_21() const { return ___filter_21; }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 ** get_address_of_filter_21() { return &___filter_21; }
	inline void set_filter_21(MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * value)
	{
		___filter_21 = value;
		Il2CppCodeGenWriteBarrier((&___filter_21), value);
	}

	inline static int32_t get_offset_of_meshObject_22() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___meshObject_22)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_meshObject_22() const { return ___meshObject_22; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_meshObject_22() { return &___meshObject_22; }
	inline void set_meshObject_22(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___meshObject_22 = value;
		Il2CppCodeGenWriteBarrier((&___meshObject_22), value);
	}

	inline static int32_t get_offset_of_meshRenderer_23() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___meshRenderer_23)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_meshRenderer_23() const { return ___meshRenderer_23; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_meshRenderer_23() { return &___meshRenderer_23; }
	inline void set_meshRenderer_23(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___meshRenderer_23 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_23), value);
	}

	inline static int32_t get_offset_of_polygonCollider_24() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___polygonCollider_24)); }
	inline PolygonCollider2D_t360CC66FD7BAF5F4FADE54EB23E58E95D0B8CD81 * get_polygonCollider_24() const { return ___polygonCollider_24; }
	inline PolygonCollider2D_t360CC66FD7BAF5F4FADE54EB23E58E95D0B8CD81 ** get_address_of_polygonCollider_24() { return &___polygonCollider_24; }
	inline void set_polygonCollider_24(PolygonCollider2D_t360CC66FD7BAF5F4FADE54EB23E58E95D0B8CD81 * value)
	{
		___polygonCollider_24 = value;
		Il2CppCodeGenWriteBarrier((&___polygonCollider_24), value);
	}

	inline static int32_t get_offset_of_material_25() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___material_25)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_25() const { return ___material_25; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_25() { return &___material_25; }
	inline void set_material_25(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_25 = value;
		Il2CppCodeGenWriteBarrier((&___material_25), value);
	}

	inline static int32_t get_offset_of_copyMaterial_26() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___copyMaterial_26)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_copyMaterial_26() const { return ___copyMaterial_26; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_copyMaterial_26() { return &___copyMaterial_26; }
	inline void set_copyMaterial_26(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___copyMaterial_26 = value;
		Il2CppCodeGenWriteBarrier((&___copyMaterial_26), value);
	}

	inline static int32_t get_offset_of_textureInfo_27() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___textureInfo_27)); }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  get_textureInfo_27() const { return ___textureInfo_27; }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F * get_address_of_textureInfo_27() { return &___textureInfo_27; }
	inline void set_textureInfo_27(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  value)
	{
		___textureInfo_27 = value;
	}

	inline static int32_t get_offset_of_orderInLayer_28() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___orderInLayer_28)); }
	inline int32_t get_orderInLayer_28() const { return ___orderInLayer_28; }
	inline int32_t* get_address_of_orderInLayer_28() { return &___orderInLayer_28; }
	inline void set_orderInLayer_28(int32_t value)
	{
		___orderInLayer_28 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_29() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___sortingLayer_29)); }
	inline int32_t get_sortingLayer_29() const { return ___sortingLayer_29; }
	inline int32_t* get_address_of_sortingLayer_29() { return &___sortingLayer_29; }
	inline void set_sortingLayer_29(int32_t value)
	{
		___sortingLayer_29 = value;
	}

	inline static int32_t get_offset_of_slowdownOnEnterOrLeave_30() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___slowdownOnEnterOrLeave_30)); }
	inline bool get_slowdownOnEnterOrLeave_30() const { return ___slowdownOnEnterOrLeave_30; }
	inline bool* get_address_of_slowdownOnEnterOrLeave_30() { return &___slowdownOnEnterOrLeave_30; }
	inline void set_slowdownOnEnterOrLeave_30(bool value)
	{
		___slowdownOnEnterOrLeave_30 = value;
	}

	inline static int32_t get_offset_of_edgeMaterial_31() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___edgeMaterial_31)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_edgeMaterial_31() const { return ___edgeMaterial_31; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_edgeMaterial_31() { return &___edgeMaterial_31; }
	inline void set_edgeMaterial_31(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___edgeMaterial_31 = value;
		Il2CppCodeGenWriteBarrier((&___edgeMaterial_31), value);
	}

	inline static int32_t get_offset_of_line_32() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___line_32)); }
	inline LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * get_line_32() const { return ___line_32; }
	inline LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 ** get_address_of_line_32() { return &___line_32; }
	inline void set_line_32(LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * value)
	{
		___line_32 = value;
		Il2CppCodeGenWriteBarrier((&___line_32), value);
	}

	inline static int32_t get_offset_of_onEnter_33() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___onEnter_33)); }
	inline bool get_onEnter_33() const { return ___onEnter_33; }
	inline bool* get_address_of_onEnter_33() { return &___onEnter_33; }
	inline void set_onEnter_33(bool value)
	{
		___onEnter_33 = value;
	}

	inline static int32_t get_offset_of_onExit_34() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___onExit_34)); }
	inline bool get_onExit_34() const { return ___onExit_34; }
	inline bool* get_address_of_onExit_34() { return &___onExit_34; }
	inline void set_onExit_34(bool value)
	{
		___onExit_34 = value;
	}

	inline static int32_t get_offset_of_mass_35() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___mass_35)); }
	inline float get_mass_35() const { return ___mass_35; }
	inline float* get_address_of_mass_35() { return &___mass_35; }
	inline void set_mass_35(float value)
	{
		___mass_35 = value;
	}

	inline static int32_t get_offset_of_spread_36() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___spread_36)); }
	inline float get_spread_36() const { return ___spread_36; }
	inline float* get_address_of_spread_36() { return &___spread_36; }
	inline void set_spread_36(float value)
	{
		___spread_36 = value;
	}

	inline static int32_t get_offset_of_springconstant_37() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___springconstant_37)); }
	inline float get_springconstant_37() const { return ___springconstant_37; }
	inline float* get_address_of_springconstant_37() { return &___springconstant_37; }
	inline void set_springconstant_37(float value)
	{
		___springconstant_37 = value;
	}

	inline static int32_t get_offset_of_damping_38() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___damping_38)); }
	inline float get_damping_38() const { return ___damping_38; }
	inline float* get_address_of_damping_38() { return &___damping_38; }
	inline void set_damping_38(float value)
	{
		___damping_38 = value;
	}

	inline static int32_t get_offset_of_forceMultiplier_39() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___forceMultiplier_39)); }
	inline float get_forceMultiplier_39() const { return ___forceMultiplier_39; }
	inline float* get_address_of_forceMultiplier_39() { return &___forceMultiplier_39; }
	inline void set_forceMultiplier_39(float value)
	{
		___forceMultiplier_39 = value;
	}

	inline static int32_t get_offset_of_forceThreshold_40() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___forceThreshold_40)); }
	inline float get_forceThreshold_40() const { return ___forceThreshold_40; }
	inline float* get_address_of_forceThreshold_40() { return &___forceThreshold_40; }
	inline void set_forceThreshold_40(float value)
	{
		___forceThreshold_40 = value;
	}

	inline static int32_t get_offset_of_forceClampLow_41() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___forceClampLow_41)); }
	inline float get_forceClampLow_41() const { return ___forceClampLow_41; }
	inline float* get_address_of_forceClampLow_41() { return &___forceClampLow_41; }
	inline void set_forceClampLow_41(float value)
	{
		___forceClampLow_41 = value;
	}

	inline static int32_t get_offset_of_forceClampHigh_42() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___forceClampHigh_42)); }
	inline float get_forceClampHigh_42() const { return ___forceClampHigh_42; }
	inline float* get_address_of_forceClampHigh_42() { return &___forceClampHigh_42; }
	inline void set_forceClampHigh_42(float value)
	{
		___forceClampHigh_42 = value;
	}

	inline static int32_t get_offset_of_waveLimitTop_43() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___waveLimitTop_43)); }
	inline bool get_waveLimitTop_43() const { return ___waveLimitTop_43; }
	inline bool* get_address_of_waveLimitTop_43() { return &___waveLimitTop_43; }
	inline void set_waveLimitTop_43(bool value)
	{
		___waveLimitTop_43 = value;
	}

	inline static int32_t get_offset_of_waveLimitDistance_44() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___waveLimitDistance_44)); }
	inline float get_waveLimitDistance_44() const { return ___waveLimitDistance_44; }
	inline float* get_address_of_waveLimitDistance_44() { return &___waveLimitDistance_44; }
	inline void set_waveLimitDistance_44(float value)
	{
		___waveLimitDistance_44 = value;
	}

	inline static int32_t get_offset_of_forceOnEdgesTop_45() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___forceOnEdgesTop_45)); }
	inline float get_forceOnEdgesTop_45() const { return ___forceOnEdgesTop_45; }
	inline float* get_address_of_forceOnEdgesTop_45() { return &___forceOnEdgesTop_45; }
	inline void set_forceOnEdgesTop_45(float value)
	{
		___forceOnEdgesTop_45 = value;
	}

	inline static int32_t get_offset_of_forceOnEdgesBottom_46() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___forceOnEdgesBottom_46)); }
	inline float get_forceOnEdgesBottom_46() const { return ___forceOnEdgesBottom_46; }
	inline float* get_address_of_forceOnEdgesBottom_46() { return &___forceOnEdgesBottom_46; }
	inline void set_forceOnEdgesBottom_46(float value)
	{
		___forceOnEdgesBottom_46 = value;
	}

	inline static int32_t get_offset_of_edgeSize_47() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___edgeSize_47)); }
	inline int32_t get_edgeSize_47() const { return ___edgeSize_47; }
	inline int32_t* get_address_of_edgeSize_47() { return &___edgeSize_47; }
	inline void set_edgeSize_47(int32_t value)
	{
		___edgeSize_47 = value;
	}

	inline static int32_t get_offset_of_physicsEnabled_49() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___physicsEnabled_49)); }
	inline bool get_physicsEnabled_49() const { return ___physicsEnabled_49; }
	inline bool* get_address_of_physicsEnabled_49() { return &___physicsEnabled_49; }
	inline void set_physicsEnabled_49(bool value)
	{
		___physicsEnabled_49 = value;
	}

	inline static int32_t get_offset_of_stayOnEdge_50() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___stayOnEdge_50)); }
	inline bool get_stayOnEdge_50() const { return ___stayOnEdge_50; }
	inline bool* get_address_of_stayOnEdge_50() { return &___stayOnEdge_50; }
	inline void set_stayOnEdge_50(bool value)
	{
		___stayOnEdge_50 = value;
	}

	inline static int32_t get_offset_of_forceDirection_51() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___forceDirection_51)); }
	inline int32_t get_forceDirection_51() const { return ___forceDirection_51; }
	inline int32_t* get_address_of_forceDirection_51() { return &___forceDirection_51; }
	inline void set_forceDirection_51(int32_t value)
	{
		___forceDirection_51 = value;
	}

	inline static int32_t get_offset_of_forceMagnitude_52() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___forceMagnitude_52)); }
	inline float get_forceMagnitude_52() const { return ___forceMagnitude_52; }
	inline float* get_address_of_forceMagnitude_52() { return &___forceMagnitude_52; }
	inline void set_forceMagnitude_52(float value)
	{
		___forceMagnitude_52 = value;
	}

	inline static int32_t get_offset_of_floatThreshold_53() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___floatThreshold_53)); }
	inline float get_floatThreshold_53() const { return ___floatThreshold_53; }
	inline float* get_address_of_floatThreshold_53() { return &___floatThreshold_53; }
	inline void set_floatThreshold_53(float value)
	{
		___floatThreshold_53 = value;
	}

	inline static int32_t get_offset_of_waterDensity_54() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___waterDensity_54)); }
	inline float get_waterDensity_54() const { return ___waterDensity_54; }
	inline float* get_address_of_waterDensity_54() { return &___waterDensity_54; }
	inline void set_waterDensity_54(float value)
	{
		___waterDensity_54 = value;
	}

	inline static int32_t get_offset_of_forceDependsOnDistance_55() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___forceDependsOnDistance_55)); }
	inline bool get_forceDependsOnDistance_55() const { return ___forceDependsOnDistance_55; }
	inline bool* get_address_of_forceDependsOnDistance_55() { return &___forceDependsOnDistance_55; }
	inline void set_forceDependsOnDistance_55(bool value)
	{
		___forceDependsOnDistance_55 = value;
	}

	inline static int32_t get_offset_of_forceProportionalToMass_56() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___forceProportionalToMass_56)); }
	inline bool get_forceProportionalToMass_56() const { return ___forceProportionalToMass_56; }
	inline bool* get_address_of_forceProportionalToMass_56() { return &___forceProportionalToMass_56; }
	inline void set_forceProportionalToMass_56(bool value)
	{
		___forceProportionalToMass_56 = value;
	}

	inline static int32_t get_offset_of_touchingObjects_57() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___touchingObjects_57)); }
	inline int32_t get_touchingObjects_57() const { return ___touchingObjects_57; }
	inline int32_t* get_address_of_touchingObjects_57() { return &___touchingObjects_57; }
	inline void set_touchingObjects_57(int32_t value)
	{
		___touchingObjects_57 = value;
	}

	inline static int32_t get_offset_of_collidersObject_58() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___collidersObject_58)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_collidersObject_58() const { return ___collidersObject_58; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_collidersObject_58() { return &___collidersObject_58; }
	inline void set_collidersObject_58(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___collidersObject_58 = value;
		Il2CppCodeGenWriteBarrier((&___collidersObject_58), value);
	}

	inline static int32_t get_offset_of_touchingColliders_59() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___touchingColliders_59)); }
	inline Dictionary_2_t97FE799FB5630EA306A1F53E2CA192562EF2E217 * get_touchingColliders_59() const { return ___touchingColliders_59; }
	inline Dictionary_2_t97FE799FB5630EA306A1F53E2CA192562EF2E217 ** get_address_of_touchingColliders_59() { return &___touchingColliders_59; }
	inline void set_touchingColliders_59(Dictionary_2_t97FE799FB5630EA306A1F53E2CA192562EF2E217 * value)
	{
		___touchingColliders_59 = value;
		Il2CppCodeGenWriteBarrier((&___touchingColliders_59), value);
	}

	inline static int32_t get_offset_of_innerColliders_60() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___innerColliders_60)); }
	inline PolygonCollider2DU5BU5D_t740B9DF43CFB32BA191190CEA2770604FF8C0968* get_innerColliders_60() const { return ___innerColliders_60; }
	inline PolygonCollider2DU5BU5D_t740B9DF43CFB32BA191190CEA2770604FF8C0968** get_address_of_innerColliders_60() { return &___innerColliders_60; }
	inline void set_innerColliders_60(PolygonCollider2DU5BU5D_t740B9DF43CFB32BA191190CEA2770604FF8C0968* value)
	{
		___innerColliders_60 = value;
		Il2CppCodeGenWriteBarrier((&___innerColliders_60), value);
	}

	inline static int32_t get_offset_of_innerGOs_61() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___innerGOs_61)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_innerGOs_61() const { return ___innerGOs_61; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_innerGOs_61() { return &___innerGOs_61; }
	inline void set_innerGOs_61(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___innerGOs_61 = value;
		Il2CppCodeGenWriteBarrier((&___innerGOs_61), value);
	}

	inline static int32_t get_offset_of_pointsPerUnit_62() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___pointsPerUnit_62)); }
	inline int32_t get_pointsPerUnit_62() const { return ___pointsPerUnit_62; }
	inline int32_t* get_address_of_pointsPerUnit_62() { return &___pointsPerUnit_62; }
	inline void set_pointsPerUnit_62(int32_t value)
	{
		___pointsPerUnit_62 = value;
	}

	inline static int32_t get_offset_of_delta_63() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___delta_63)); }
	inline float get_delta_63() const { return ___delta_63; }
	inline float* get_address_of_delta_63() { return &___delta_63; }
	inline void set_delta_63(float value)
	{
		___delta_63 = value;
	}

	inline static int32_t get_offset_of_lengths_64() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___lengths_64)); }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * get_lengths_64() const { return ___lengths_64; }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E ** get_address_of_lengths_64() { return &___lengths_64; }
	inline void set_lengths_64(List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * value)
	{
		___lengths_64 = value;
		Il2CppCodeGenWriteBarrier((&___lengths_64), value);
	}

	inline static int32_t get_offset_of_lengths01_65() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___lengths01_65)); }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * get_lengths01_65() const { return ___lengths01_65; }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E ** get_address_of_lengths01_65() { return &___lengths01_65; }
	inline void set_lengths01_65(List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * value)
	{
		___lengths01_65 = value;
		Il2CppCodeGenWriteBarrier((&___lengths01_65), value);
	}

	inline static int32_t get_offset_of_points_66() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___points_66)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_points_66() const { return ___points_66; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_points_66() { return &___points_66; }
	inline void set_points_66(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___points_66 = value;
		Il2CppCodeGenWriteBarrier((&___points_66), value);
	}

	inline static int32_t get_offset_of_tangentDirs_67() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___tangentDirs_67)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_tangentDirs_67() const { return ___tangentDirs_67; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_tangentDirs_67() { return &___tangentDirs_67; }
	inline void set_tangentDirs_67(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___tangentDirs_67 = value;
		Il2CppCodeGenWriteBarrier((&___tangentDirs_67), value);
	}

	inline static int32_t get_offset_of_testPanel_68() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___testPanel_68)); }
	inline WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80 * get_testPanel_68() const { return ___testPanel_68; }
	inline WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80 ** get_address_of_testPanel_68() { return &___testPanel_68; }
	inline void set_testPanel_68(WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80 * value)
	{
		___testPanel_68 = value;
		Il2CppCodeGenWriteBarrier((&___testPanel_68), value);
	}

	inline static int32_t get_offset_of_meshSegmentType_70() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___meshSegmentType_70)); }
	inline int32_t get_meshSegmentType_70() const { return ___meshSegmentType_70; }
	inline int32_t* get_address_of_meshSegmentType_70() { return &___meshSegmentType_70; }
	inline void set_meshSegmentType_70(int32_t value)
	{
		___meshSegmentType_70 = value;
	}
};

struct SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8_StaticFields
{
public:
	// UnityEngine.Vector2 Kolibri2d.SplineWater::flatNormal
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___flatNormal_69;

public:
	inline static int32_t get_offset_of_flatNormal_69() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8_StaticFields, ___flatNormal_69)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_flatNormal_69() const { return ___flatNormal_69; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_flatNormal_69() { return &___flatNormal_69; }
	inline void set_flatNormal_69(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___flatNormal_69 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINEWATER_T28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8_H
#ifndef TERRAIN2D_T0C58931CB54475940DC223D25AB20A9389D0BA4B_H
#define TERRAIN2D_T0C58931CB54475940DC223D25AB20A9389D0BA4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2D
struct  Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B  : public SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232
{
public:
	// System.Int32 Kolibri2d.Terrain2D::additiveSortInLayer
	int32_t ___additiveSortInLayer_8;
	// System.Collections.Generic.List`1<Kolibri2d.Terrain2D/Terrain2DMaterialHolder> Kolibri2d.Terrain2D::materialHolders
	List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 * ___materialHolders_9;
	// Kolibri2d.SplineTerrain2DCreator Kolibri2d.Terrain2D::creator
	SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1 * ___creator_10;
	// System.Single Kolibri2d.Terrain2D::skirtOffsetMagnitude
	float ___skirtOffsetMagnitude_11;
	// System.Single Kolibri2d.Terrain2D::skirtOffsetAngle
	float ___skirtOffsetAngle_12;
	// System.Single Kolibri2d.Terrain2D::skirtPosY
	float ___skirtPosY_13;
	// System.Single Kolibri2d.Terrain2D::skirtPosX
	float ___skirtPosX_14;
	// System.Single Kolibri2d.Terrain2D::skirtTopOffsetMagnitude
	float ___skirtTopOffsetMagnitude_15;
	// System.Single Kolibri2d.Terrain2D::skirtBottomOffsetMagnitude
	float ___skirtBottomOffsetMagnitude_16;
	// System.Single Kolibri2d.Terrain2D::skirtLeftOffsetMagnitude
	float ___skirtLeftOffsetMagnitude_17;
	// System.Single Kolibri2d.Terrain2D::skirtRightOffsetMagnitude
	float ___skirtRightOffsetMagnitude_18;
	// Kolibri2d.Terrain2D/SkirtType Kolibri2d.Terrain2D::skirtType
	int32_t ___skirtType_19;
	// System.Single Kolibri2d.Terrain2D::distortion
	float ___distortion_20;
	// System.Int32 Kolibri2d.Terrain2D::meshSubdivision
	int32_t ___meshSubdivision_21;

public:
	inline static int32_t get_offset_of_additiveSortInLayer_8() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___additiveSortInLayer_8)); }
	inline int32_t get_additiveSortInLayer_8() const { return ___additiveSortInLayer_8; }
	inline int32_t* get_address_of_additiveSortInLayer_8() { return &___additiveSortInLayer_8; }
	inline void set_additiveSortInLayer_8(int32_t value)
	{
		___additiveSortInLayer_8 = value;
	}

	inline static int32_t get_offset_of_materialHolders_9() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___materialHolders_9)); }
	inline List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 * get_materialHolders_9() const { return ___materialHolders_9; }
	inline List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 ** get_address_of_materialHolders_9() { return &___materialHolders_9; }
	inline void set_materialHolders_9(List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 * value)
	{
		___materialHolders_9 = value;
		Il2CppCodeGenWriteBarrier((&___materialHolders_9), value);
	}

	inline static int32_t get_offset_of_creator_10() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___creator_10)); }
	inline SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1 * get_creator_10() const { return ___creator_10; }
	inline SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1 ** get_address_of_creator_10() { return &___creator_10; }
	inline void set_creator_10(SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1 * value)
	{
		___creator_10 = value;
		Il2CppCodeGenWriteBarrier((&___creator_10), value);
	}

	inline static int32_t get_offset_of_skirtOffsetMagnitude_11() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___skirtOffsetMagnitude_11)); }
	inline float get_skirtOffsetMagnitude_11() const { return ___skirtOffsetMagnitude_11; }
	inline float* get_address_of_skirtOffsetMagnitude_11() { return &___skirtOffsetMagnitude_11; }
	inline void set_skirtOffsetMagnitude_11(float value)
	{
		___skirtOffsetMagnitude_11 = value;
	}

	inline static int32_t get_offset_of_skirtOffsetAngle_12() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___skirtOffsetAngle_12)); }
	inline float get_skirtOffsetAngle_12() const { return ___skirtOffsetAngle_12; }
	inline float* get_address_of_skirtOffsetAngle_12() { return &___skirtOffsetAngle_12; }
	inline void set_skirtOffsetAngle_12(float value)
	{
		___skirtOffsetAngle_12 = value;
	}

	inline static int32_t get_offset_of_skirtPosY_13() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___skirtPosY_13)); }
	inline float get_skirtPosY_13() const { return ___skirtPosY_13; }
	inline float* get_address_of_skirtPosY_13() { return &___skirtPosY_13; }
	inline void set_skirtPosY_13(float value)
	{
		___skirtPosY_13 = value;
	}

	inline static int32_t get_offset_of_skirtPosX_14() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___skirtPosX_14)); }
	inline float get_skirtPosX_14() const { return ___skirtPosX_14; }
	inline float* get_address_of_skirtPosX_14() { return &___skirtPosX_14; }
	inline void set_skirtPosX_14(float value)
	{
		___skirtPosX_14 = value;
	}

	inline static int32_t get_offset_of_skirtTopOffsetMagnitude_15() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___skirtTopOffsetMagnitude_15)); }
	inline float get_skirtTopOffsetMagnitude_15() const { return ___skirtTopOffsetMagnitude_15; }
	inline float* get_address_of_skirtTopOffsetMagnitude_15() { return &___skirtTopOffsetMagnitude_15; }
	inline void set_skirtTopOffsetMagnitude_15(float value)
	{
		___skirtTopOffsetMagnitude_15 = value;
	}

	inline static int32_t get_offset_of_skirtBottomOffsetMagnitude_16() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___skirtBottomOffsetMagnitude_16)); }
	inline float get_skirtBottomOffsetMagnitude_16() const { return ___skirtBottomOffsetMagnitude_16; }
	inline float* get_address_of_skirtBottomOffsetMagnitude_16() { return &___skirtBottomOffsetMagnitude_16; }
	inline void set_skirtBottomOffsetMagnitude_16(float value)
	{
		___skirtBottomOffsetMagnitude_16 = value;
	}

	inline static int32_t get_offset_of_skirtLeftOffsetMagnitude_17() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___skirtLeftOffsetMagnitude_17)); }
	inline float get_skirtLeftOffsetMagnitude_17() const { return ___skirtLeftOffsetMagnitude_17; }
	inline float* get_address_of_skirtLeftOffsetMagnitude_17() { return &___skirtLeftOffsetMagnitude_17; }
	inline void set_skirtLeftOffsetMagnitude_17(float value)
	{
		___skirtLeftOffsetMagnitude_17 = value;
	}

	inline static int32_t get_offset_of_skirtRightOffsetMagnitude_18() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___skirtRightOffsetMagnitude_18)); }
	inline float get_skirtRightOffsetMagnitude_18() const { return ___skirtRightOffsetMagnitude_18; }
	inline float* get_address_of_skirtRightOffsetMagnitude_18() { return &___skirtRightOffsetMagnitude_18; }
	inline void set_skirtRightOffsetMagnitude_18(float value)
	{
		___skirtRightOffsetMagnitude_18 = value;
	}

	inline static int32_t get_offset_of_skirtType_19() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___skirtType_19)); }
	inline int32_t get_skirtType_19() const { return ___skirtType_19; }
	inline int32_t* get_address_of_skirtType_19() { return &___skirtType_19; }
	inline void set_skirtType_19(int32_t value)
	{
		___skirtType_19 = value;
	}

	inline static int32_t get_offset_of_distortion_20() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___distortion_20)); }
	inline float get_distortion_20() const { return ___distortion_20; }
	inline float* get_address_of_distortion_20() { return &___distortion_20; }
	inline void set_distortion_20(float value)
	{
		___distortion_20 = value;
	}

	inline static int32_t get_offset_of_meshSubdivision_21() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___meshSubdivision_21)); }
	inline int32_t get_meshSubdivision_21() const { return ___meshSubdivision_21; }
	inline int32_t* get_address_of_meshSubdivision_21() { return &___meshSubdivision_21; }
	inline void set_meshSubdivision_21(int32_t value)
	{
		___meshSubdivision_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERRAIN2D_T0C58931CB54475940DC223D25AB20A9389D0BA4B_H
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  m_Items[1];

public:
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  m_Items[1];

public:
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Renderer[]
struct RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * m_Items[1];

public:
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Kolibri2d.BezierSpline[]
struct BezierSplineU5BU5D_tD9962D7525021265D09A9C68F754780C16917F1C  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * m_Items[1];

public:
	inline BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  m_Items[1];

public:
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};


// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void UnityEvent_1__ctor_m0664E6A8259E48D52CF88851228B1DC47F751C48_gshared (UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m3EE2218014A615CBBF70592A7E92CC83BF59AEF9_gshared (List_1_tE72A517BD14F52539FF78EA90F58D1387FEED660 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m66ACE80424582788B769E061E2FEB145E4035121_gshared (List_1_tE72A517BD14F52539FF78EA90F58D1387FEED660 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR Enumerator_t7069929711A56B20B09076697B86D7BEE647C7DC  List_1_GetEnumerator_m00315869290B2B3EA4FCADE6FC323577EED0813D_gshared (List_1_tE72A517BD14F52539FF78EA90F58D1387FEED660 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m0EB38CF3F39639D66B501AE149795A3D14591CF0_gshared (Enumerator_t7069929711A56B20B09076697B86D7BEE647C7DC * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m0E742AAB7856E7A01F363EF296E59EFE93A47CAE_gshared (Enumerator_t7069929711A56B20B09076697B86D7BEE647C7DC * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void Enumerator_Dispose_m70AEC38F82BAB3295F58CCD00DFD4683760B7BC2_gshared (Enumerator_t7069929711A56B20B09076697B86D7BEE647C7DC * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<Kolibri2d.SplineSegmentInfo>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m1603A7C79B587156F949A0561C5CE4635B4AD255_gshared (List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Kolibri2d.SplineSegmentInfo>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m8E52D55D4C56236ED57F6275B7E7EDD692F55961_gshared (List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_gshared (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_gshared (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Kolibri2d.SplineSegmentInfo>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m649A438448972F32C1D67F10BBFB80D0CF81EF94_gshared (List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * __this, SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  p0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<Kolibri2d.SplineSegmentInfo>::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB  List_1_GetEnumerator_m9536E635B50A2D8549D7615CC042AF1D64AEF454_gshared (List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<Kolibri2d.SplineSegmentInfo>::get_Current()
extern "C" IL2CPP_METHOD_ATTR SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  Enumerator_get_Current_m495068DBBE452DB06B67916B5D28B026BD336204_gshared (Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<Kolibri2d.SplineSegmentInfo>::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m3D433FA2C3588249702F14524E89E464EE14EE13_gshared (Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<Kolibri2d.SplineSegmentInfo>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void Enumerator_Dispose_mCB51DC072567AD26ACE9160F23683B7D32B7190B_gshared (Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<Kolibri2d.SplineSegmentInfo>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  List_1_get_Item_mA31EF7A05D93A92566AE15D88053820A2E85F1AF_gshared (List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Kolibri2d.SplineSegmentInfo>::set_Item(System.Int32,!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_set_Item_mCAC53DD791D8E571F6A22EE17D615DD70B9C6619_gshared (List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * __this, int32_t p0, SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Kolibri2d.SplineSegmentInfo>::RemoveAt(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void List_1_RemoveAt_mDDB8509DB8FA2CC2FB3F8A9A13C8028CD8185109_gshared (List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * __this, int32_t p0, const RuntimeMethod* method);
// !!0 UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * ScriptableObject_CreateInstance_TisRuntimeObject_m7A8F75139352BA04C2EEC1D72D430FAC94C753DE_gshared (const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<Kolibri2d.TerrainType,Kolibri2d.MaterialTextureInfo>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m9266E02BD88994992C4A72CDE2733990EEE6C9B8_gshared (Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Dictionary`2<Kolibri2d.TerrainType,Kolibri2d.MaterialTextureInfo>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t Dictionary_2_get_Count_mB1BF016039CF52477957DBB0568653081D14612E_gshared (Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<Kolibri2d.TerrainType,Kolibri2d.MaterialTextureInfo>::set_Item(!0,!1)
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2_set_Item_m85881E2C19054C55C36474E5876AC2AB23552E0B_gshared (Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A * __this, int32_t p0, MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  p1, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<Kolibri2d.TerrainType,Kolibri2d.MaterialTextureInfo>::get_Item(!0)
extern "C" IL2CPP_METHOD_ATTR MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  Dictionary_2_get_Item_mF1FB9229F3B369C779CEF8B43902E24D6C3FCD3D_gshared (Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A * __this, int32_t p0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.Vector3>::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F  List_1_GetEnumerator_mA6AD60947F39CE54DA7D237D278C8948D5B2C331_gshared (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::get_Current()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Enumerator_get_Current_mAB079301955D01523AB6CD62D400632D218B265D_gshared (Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mFB1EBCAC4E921F22042B0BDB6B954B7A09589454_gshared (Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void Enumerator_Dispose_m640FFEF2E9A18C15D423069973168AD40E10FBFB_gshared (Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m41E09C4CA476451FE275573062956CED105CB79A_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C" IL2CPP_METHOD_ATTR ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D* GameObject_GetComponentsInChildren_TisRuntimeObject_m615406921CD720805AC5F99C672532A31C81D1C7_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m6DB64BAFEC79776AB013124B548E074CAA2FAD95_gshared (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857_gshared (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m0E7CD0EBE7EA0CCD1DDA59B1F34C56CDB1F79827_gshared (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m6A5DF174C63F24D00FC3CCA142FF30E9FE3B0B84_gshared (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_mCAB49C9A39B98BC7B6D7CEDCA6AE260556381C09_gshared (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m80FC80854B1AD97640F2B64DE9B250A3D8DAB145_gshared (List_1_t4A961510635950236678F1B3B2436ECCAF28713A * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Int32>::ContainsKey(!0)
extern "C" IL2CPP_METHOD_ATTR bool Dictionary_2_ContainsKey_m230D107B4D3C342ECBC961100D82015C14534FCC_gshared (Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F * __this, uint32_t p0, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.UInt32,System.Int32>::get_Item(!0)
extern "C" IL2CPP_METHOD_ATTR int32_t Dictionary_2_get_Item_mCECAC44EF2E9AE18023E7FDAD8CC268588065CE3_gshared (Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F * __this, uint32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Int32>::Add(!0,!1)
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2_Add_mDCE3CAF8CAFEBD13FB38A51C069F27896353ECB5_gshared (Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F * __this, uint32_t p0, int32_t p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_gshared (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m9E6BB40B113CEAFD4EA6A2BD7FCEB7259C20561D_gshared (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Color>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  List_1_get_Item_mB725E23AF0513F5A32B32BBC9C2471097DE235C4_gshared (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m00EB6B0859FF6BCA9F89EF1FE572855EAA143F8F_gshared (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * __this, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7_gshared (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B_gshared (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Int32>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m7E5325C12ECF67A2A855BBDC04968EE0CC66ACF1_gshared (Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_gshared (List_1_t4A961510635950236678F1B3B2436ECCAF28713A * __this, int32_t p0, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<UnityEngine.Vector3>::ToArray()
extern "C" IL2CPP_METHOD_ATTR Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* List_1_ToArray_m062BF9DC0570B6EEC207AAEDDEC602FB2EB3F9BA_gshared (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<UnityEngine.Color>::ToArray()
extern "C" IL2CPP_METHOD_ATTR ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* List_1_ToArray_mE9F8DDC7219DCC0198F4A265DBD77BC4307FA6F4_gshared (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * __this, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<UnityEngine.Vector2>::ToArray()
extern "C" IL2CPP_METHOD_ATTR Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* List_1_ToArray_mB9E07F625DF343AED13593FB79319A4ADFC02AB7_gshared (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<System.Int32>::ToArray()
extern "C" IL2CPP_METHOD_ATTR Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* List_1_ToArray_mE3701008FD26BBC8184384CA22962898157F7BAF_gshared (List_1_t4A961510635950236678F1B3B2436ECCAF28713A * __this, const RuntimeMethod* method);

// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::.ctor()
inline void UnityEvent_1__ctor_m0664E6A8259E48D52CF88851228B1DC47F751C48 (UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87 * __this, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87 *, const RuntimeMethod*))UnityEvent_1__ctor_m0664E6A8259E48D52CF88851228B1DC47F751C48_gshared)(__this, method);
}
// System.Void Kolibri2d.SplineTerrain2DCreator::.ctor(Kolibri2d.Terrain2D)
extern "C" IL2CPP_METHOD_ATTR void SplineTerrain2DCreator__ctor_m580BBD9F2E83001DF2B1AD5B68C9F5630E27657F (SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1 * __this, Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B * ____terrain0, const RuntimeMethod* method);
// System.Void Kolibri2d.SplineTerrain2DCreator::SetTerrain(Kolibri2d.Terrain2D)
extern "C" IL2CPP_METHOD_ATTR void SplineTerrain2DCreator_SetTerrain_m2B191F050A07085B2B7835DA44EB68EADD6D681C (SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1 * __this, Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B * ____terrain0, const RuntimeMethod* method);
// System.Void Kolibri2d.SplineModifierWithCallbacks::Reset()
extern "C" IL2CPP_METHOD_ATTR void SplineModifierWithCallbacks_Reset_m6071B127F32D6ECA8C3963C11A9C1DA3E74E0E8F (SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Kolibri2d.Terrain2D/Terrain2DMaterialHolder>::.ctor()
inline void List_1__ctor_m9E8734104EA5EA636BBA9DD4E5E56DC6C7C1B063 (List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 *, const RuntimeMethod*))List_1__ctor_m3EE2218014A615CBBF70592A7E92CC83BF59AEF9_gshared)(__this, method);
}
// System.Void Kolibri2d.Terrain2D/Terrain2DMaterialHolder::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Terrain2DMaterialHolder__ctor_mD9ADF2A9EA239E77BCBD828F337A70412D6BB96B (Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Kolibri2d.Terrain2D/Terrain2DMaterialHolder>::Add(!0)
inline void List_1_Add_mF10FF505AE13DE269187B75AC591D0D47188FF0B (List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 * __this, Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 *, Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F *, const RuntimeMethod*))List_1_Add_m66ACE80424582788B769E061E2FEB145E4035121_gshared)(__this, p0, method);
}
// System.Void Kolibri2d.Terrain2D::SetCreator()
extern "C" IL2CPP_METHOD_ATTR void Terrain2D_SetCreator_m5DF018BBE08473E58C9FC42A45076CB16A6DAA4D (Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B * __this, const RuntimeMethod* method);
// System.Void Kolibri2d.Terrain2D::ClearAll()
extern "C" IL2CPP_METHOD_ATTR void Terrain2D_ClearAll_m40A08E96E6E9ED3C435653E59D15393811EA1B29 (Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B * __this, const RuntimeMethod* method);
// System.Void Kolibri2d.SplineModifierWithCallbacks::RefreshComponents()
extern "C" IL2CPP_METHOD_ATTR void SplineModifierWithCallbacks_RefreshComponents_m715A574992FA4002704981C5792BD4BE05A5A2E8 (SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232 * __this, const RuntimeMethod* method);
// System.Void Kolibri2d.Terrain2D::RefreshColliders()
extern "C" IL2CPP_METHOD_ATTR void Terrain2D_RefreshColliders_m2CD6434AC10F2F0369129AE06C3936F1A6BA15E4 (Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B * __this, const RuntimeMethod* method);
// System.Void Kolibri2d.SplineTerrain2DCreator::Refresh()
extern "C" IL2CPP_METHOD_ATTR void SplineTerrain2DCreator_Refresh_mEC5BD746E18701BACF47CB916EED1EB406A15D5C (SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Void Kolibri2d.SplineColliderCreator::Clear(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR void SplineColliderCreator_Clear_m0BACF4B997CF3D8991A3503901CCEBB1DA9A0A43 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj0, const RuntimeMethod* method);
// System.Void Kolibri2d.SplineTerrain2DCreator::ClearAll()
extern "C" IL2CPP_METHOD_ATTR void SplineTerrain2DCreator_ClearAll_mF7790FD9F6595DA333A63133844DA1816D0C2862 (SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<Kolibri2d.SplineColliderCreator>()
inline SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A * Component_GetComponent_TisSplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A_m434AAC7C9A971A8416BB64D702DA25F199DFB2C0 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared)(__this, method);
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
extern "C" IL2CPP_METHOD_ATTR bool Behaviour_get_isActiveAndEnabled_mC42DFCC1ECC2C94D52928FFE446CE7E266CA8B61 (Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<Kolibri2d.Terrain2D/Terrain2DMaterialHolder>::GetEnumerator()
inline Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7  List_1_GetEnumerator_mA9C0CCE2B4A25A7A18800BE1E1E3DD78AE5E9515 (List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7  (*) (List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 *, const RuntimeMethod*))List_1_GetEnumerator_m00315869290B2B3EA4FCADE6FC323577EED0813D_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<Kolibri2d.Terrain2D/Terrain2DMaterialHolder>::get_Current()
inline Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * Enumerator_get_Current_m063115BDDF8FC2B26C79A4AD2BCB44A7661BBA28 (Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7 * __this, const RuntimeMethod* method)
{
	return ((  Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * (*) (Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7 *, const RuntimeMethod*))Enumerator_get_Current_m0EB38CF3F39639D66B501AE149795A3D14591CF0_gshared)(__this, method);
}
// System.Void Kolibri2d.SplineColliderSettings::Refresh()
extern "C" IL2CPP_METHOD_ATTR void SplineColliderSettings_Refresh_mED92DD849162B0E6CB5D25F42853409E7FFFAF1A (SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4 * __this, const RuntimeMethod* method);
// System.Void Kolibri2d.SplineColliderCreator::CreateColliders(Kolibri2d.BezierSpline,Kolibri2d.SplineColliderSettings,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void SplineColliderCreator_CreateColliders_m6ECFBD78F2CAF8EA0C68DA612D50E9862ACB1BC8 (BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * ___spline0, SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4 * ___settings1, int32_t ___objectLayer2, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<Kolibri2d.Terrain2D/Terrain2DMaterialHolder>::MoveNext()
inline bool Enumerator_MoveNext_m168734AAA4869F1578DF4D4538B74BFF12522F68 (Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7 *, const RuntimeMethod*))Enumerator_MoveNext_m0E742AAB7856E7A01F363EF296E59EFE93A47CAE_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Kolibri2d.Terrain2D/Terrain2DMaterialHolder>::Dispose()
inline void Enumerator_Dispose_mCDA3AD77115D33F3528CD546C85CA20239DD4294 (Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7 *, const RuntimeMethod*))Enumerator_Dispose_m70AEC38F82BAB3295F58CCD00DFD4683760B7BC2_gshared)(__this, method);
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p1, const RuntimeMethod* method);
// System.Void Kolibri2d.SplineTerrain2DCreator::RandomizeDistortions()
extern "C" IL2CPP_METHOD_ATTR void SplineTerrain2DCreator_RandomizeDistortions_mA01B093693F10666A6AF040F50D8144565F51BC6 (SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1 * __this, const RuntimeMethod* method);
// System.Void Kolibri2d.SplineModifierWithCallbacks::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SplineModifierWithCallbacks__ctor_m9C2181407A87C589515FDA4335D3FB71238C7827 (SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232 * __this, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C" IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_get_white_mE7F3AC4FF0D6F35E48049C73116A222CBE96D905 (const RuntimeMethod* method);
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m8BA07445967EE2CC15961AD3C16F25DB74506EA0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void Kolibri2d.Terrain2DArchitecture::Refresh()
extern "C" IL2CPP_METHOD_ATTR void Terrain2DArchitecture_Refresh_m46793DC53A31B24651E22531A2D201889CD13565 (Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<Kolibri2d.SplineSegmentInfo>::get_Count()
inline int32_t List_1_get_Count_m1603A7C79B587156F949A0561C5CE4635B4AD255 (List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t517344AED92C69B6645F0ABFA470063D220BACCD *, const RuntimeMethod*))List_1_get_Count_m1603A7C79B587156F949A0561C5CE4635B4AD255_gshared)(__this, method);
}
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, const RuntimeMethod* method);
// Kolibri2d.Terrain2DArchitectureAngles Kolibri2d.Terrain2DArchitectureAngles::get_DefaultAngle()
extern "C" IL2CPP_METHOD_ATTR Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * Terrain2DArchitectureAngles_get_DefaultAngle_m11AAFDCC6F2D57CED18469D56B6C678B9D75979F (const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<Kolibri2d.BezierSpline>()
inline BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * Component_GetComponent_TisBezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_m997621D49F9FF6305B153B07112BDDE09A165DC1 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared)(__this, method);
}
// System.Collections.Generic.List`1<UnityEngine.Vector3> Kolibri2d.BezierSpline::GetAllStepPoints()
extern "C" IL2CPP_METHOD_ATTR List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * BezierSpline_GetAllStepPoints_m5F33CE8AFBA4C9472E308BBCCCF6B1AFA2DEB495 (BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * __this, const RuntimeMethod* method);
// UnityEngine.Vector2[] Kolibri2d.BezierSpline::GetAllStepNormals()
extern "C" IL2CPP_METHOD_ATTR Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* BezierSpline_GetAllStepNormals_m3AEA137445942FDF65D21AA08CA28C48F61A43E2 (BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * __this, const RuntimeMethod* method);
// System.Boolean Kolibri2d.BezierSpline::get_Loop()
extern "C" IL2CPP_METHOD_ATTR bool BezierSpline_get_Loop_m231A6FE13B839CEE5A0F98B8DAE26554FE981D03 (BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * __this, const RuntimeMethod* method);
// UnityEngine.Vector2[] Kolibri2d.Utils::GetNormalsFromPositions(System.Collections.Generic.List`1<UnityEngine.Vector3>&,System.Boolean,System.Int32)
extern "C" IL2CPP_METHOD_ATTR Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* Utils_GetNormalsFromPositions_m2D293F791AEB966D5D2530D1C05CD9416E668041 (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** ___points0, bool ___loop1, int32_t ___smoothLevel2, const RuntimeMethod* method);
// System.Collections.Generic.List`1<Kolibri2d.SplineSegmentInfo> Kolibri2d.Terrain2DArchitecture::CalculateSegmentsInfo(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * Terrain2DArchitecture_CalculateSegmentsInfo_m7E9A4EA786517CBF0C45CDCEA214D0767751F460 (Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * __this, bool ___setManualFlip0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Kolibri2d.SplineSegmentInfo>::.ctor()
inline void List_1__ctor_m8E52D55D4C56236ED57F6275B7E7EDD692F55961 (List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t517344AED92C69B6645F0ABFA470063D220BACCD *, const RuntimeMethod*))List_1__ctor_m8E52D55D4C56236ED57F6275B7E7EDD692F55961_gshared)(__this, method);
}
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
inline int32_t List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3 (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *, const RuntimeMethod*))List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  (*) (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *, int32_t, const RuntimeMethod*))List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_gshared)(__this, p0, method);
}
// UnityEngine.Vector3 Kolibri2d.Utils::GetDirFromPoints(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Utils_GetDirFromPoints_mE9D1FC9BF146E38D7A53FD21E2A8C34F4B08F16B (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___p10, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___p21, const RuntimeMethod* method);
// System.Single Kolibri2d.Utils::GetAngleFromDir(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR float Utils_GetAngleFromDir_m62A41F885D74A5155D0E4684783672982D7CB8B3 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___dir0, const RuntimeMethod* method);
// Kolibri2d.TerrainType Kolibri2d.Terrain2DArchitectureAngles::GetTypeFromAngle(System.Single,System.Boolean,System.Boolean,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR int32_t Terrain2DArchitectureAngles_GetTypeFromAngle_m4A53062F36D10357353B71776B6421475C32B19C (Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * __this, float ___degree0, bool ___isClockwise1, bool ___inverted2, bool ___manualFlip3, const RuntimeMethod* method);
// System.Void Kolibri2d.SplineSegmentInfo::.ctor(System.Int32,System.Int32,System.Single,System.Single,UnityEngine.Vector3,Kolibri2d.TerrainType)
extern "C" IL2CPP_METHOD_ATTR void SplineSegmentInfo__ctor_m73B0DFDDE6E8AC741DA121FBA64BF1E513E3C8C5 (SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066 * __this, int32_t ___begin0, int32_t ___end1, float ___angleBegin2, float ___angleCorner3, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___dirBegin4, int32_t ___type5, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Kolibri2d.SplineSegmentInfo>::Add(!0)
inline void List_1_Add_m649A438448972F32C1D67F10BBFB80D0CF81EF94 (List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * __this, SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t517344AED92C69B6645F0ABFA470063D220BACCD *, SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066 , const RuntimeMethod*))List_1_Add_m649A438448972F32C1D67F10BBFB80D0CF81EF94_gshared)(__this, p0, method);
}
// System.Single Kolibri2d.Utils::GetAngleABC(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR float Utils_GetAngleABC_mE43F92353A13E1D3011FDE80954F05B69EC60BDC (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___c2, const RuntimeMethod* method);
// System.Void Kolibri2d.Terrain2DArchitecture::ApplySegmentGuessOnLine()
extern "C" IL2CPP_METHOD_ATTR void Terrain2DArchitecture_ApplySegmentGuessOnLine_m3B0094A0015B70D9CB31C6AEE77CE6F6C3350B9E (Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<Kolibri2d.SplineSegmentInfo>::GetEnumerator()
inline Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB  List_1_GetEnumerator_m9536E635B50A2D8549D7615CC042AF1D64AEF454 (List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB  (*) (List_1_t517344AED92C69B6645F0ABFA470063D220BACCD *, const RuntimeMethod*))List_1_GetEnumerator_m9536E635B50A2D8549D7615CC042AF1D64AEF454_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<Kolibri2d.SplineSegmentInfo>::get_Current()
inline SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  Enumerator_get_Current_m495068DBBE452DB06B67916B5D28B026BD336204 (Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB * __this, const RuntimeMethod* method)
{
	return ((  SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  (*) (Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB *, const RuntimeMethod*))Enumerator_get_Current_m495068DBBE452DB06B67916B5D28B026BD336204_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Kolibri2d.SplineSegmentInfo>::MoveNext()
inline bool Enumerator_MoveNext_m3D433FA2C3588249702F14524E89E464EE14EE13 (Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB *, const RuntimeMethod*))Enumerator_MoveNext_m3D433FA2C3588249702F14524E89E464EE14EE13_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Kolibri2d.SplineSegmentInfo>::Dispose()
inline void Enumerator_Dispose_mCB51DC072567AD26ACE9160F23683B7D32B7190B (Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB *, const RuntimeMethod*))Enumerator_Dispose_mCB51DC072567AD26ACE9160F23683B7D32B7190B_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1<Kolibri2d.SplineSegmentInfo>::get_Item(System.Int32)
inline SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  List_1_get_Item_mA31EF7A05D93A92566AE15D88053820A2E85F1AF (List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  (*) (List_1_t517344AED92C69B6645F0ABFA470063D220BACCD *, int32_t, const RuntimeMethod*))List_1_get_Item_mA31EF7A05D93A92566AE15D88053820A2E85F1AF_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<Kolibri2d.SplineSegmentInfo>::set_Item(System.Int32,!0)
inline void List_1_set_Item_mCAC53DD791D8E571F6A22EE17D615DD70B9C6619 (List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * __this, int32_t p0, SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  p1, const RuntimeMethod* method)
{
	((  void (*) (List_1_t517344AED92C69B6645F0ABFA470063D220BACCD *, int32_t, SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066 , const RuntimeMethod*))List_1_set_Item_mCAC53DD791D8E571F6A22EE17D615DD70B9C6619_gshared)(__this, p0, p1, method);
}
// System.Void System.Collections.Generic.List`1<Kolibri2d.SplineSegmentInfo>::RemoveAt(System.Int32)
inline void List_1_RemoveAt_mDDB8509DB8FA2CC2FB3F8A9A13C8028CD8185109 (List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * __this, int32_t p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t517344AED92C69B6645F0ABFA470063D220BACCD *, int32_t, const RuntimeMethod*))List_1_RemoveAt_mDDB8509DB8FA2CC2FB3F8A9A13C8028CD8185109_gshared)(__this, p0, method);
}
// System.Void Kolibri2d.Terrain2DArchitecture::RefreshIfMissingData()
extern "C" IL2CPP_METHOD_ATTR void Terrain2DArchitecture_RefreshIfMissingData_m7D08ADA747580A063BA739D33732CFB36F90A5F7 (Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_Clamp_m033DD894F89E6DCCDAFC580091053059C86A4507 (float p0, float p1, float p2, const RuntimeMethod* method);
// UnityEngine.Vector3 Kolibri2d.BezierSpline::GetDirectionFromLength01(System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  BezierSpline_GetDirectionFromLength01_m2AD48C7B60722B8A4A49117EE84B7A6264F4DC28 (BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * __this, float ___len010, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_UnaryNegation(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_UnaryNegation_m2AFBBF22801F9BCA5A4EBE642A29F433FE1339C2 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.ScriptableObject::CreateInstance<Kolibri2d.Terrain2DArchitectureAngles>()
inline Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * ScriptableObject_CreateInstance_TisTerrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_m07B85156417883C4AB46AA7039D2CF9CEC71B80F (const RuntimeMethod* method)
{
	return ((  Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * (*) (const RuntimeMethod*))ScriptableObject_CreateInstance_TisRuntimeObject_m7A8F75139352BA04C2EEC1D72D430FAC94C753DE_gshared)(method);
}
// System.Void Kolibri2d.Terrain2DArchitectureAngles::ResetValues()
extern "C" IL2CPP_METHOD_ATTR void Terrain2DArchitectureAngles_ResetValues_mAF413B8C39C39522A62BB55D7A9D5CD5EB9FAAE3 (Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ScriptableObject__ctor_m6E2B3821A4A361556FC12E9B1C71E1D5DC002C5B (ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734 * __this, const RuntimeMethod* method);
// Kolibri2d.TerrainType Kolibri2d.Terrain2DArchitectureAngles::GetReplacedType(Kolibri2d.TerrainType)
extern "C" IL2CPP_METHOD_ATTR int32_t Terrain2DArchitectureAngles_GetReplacedType_mB72A1062389E0C760E72DFED807ACE704DB54862 (Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * __this, int32_t ___t0, const RuntimeMethod* method);
// System.Void Kolibri2d.Terrain2DMaterial::Refresh()
extern "C" IL2CPP_METHOD_ATTR void Terrain2DMaterial_Refresh_mD6A2F2D44B21F6CC68F65957C50A71065DE64AFC (Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<Kolibri2d.TerrainType,Kolibri2d.MaterialTextureInfo>::.ctor()
inline void Dictionary_2__ctor_m9266E02BD88994992C4A72CDE2733990EEE6C9B8 (Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A *, const RuntimeMethod*))Dictionary_2__ctor_m9266E02BD88994992C4A72CDE2733990EEE6C9B8_gshared)(__this, method);
}
// System.Void Kolibri2d.MaterialTextureInfo::.ctor(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void MaterialTextureInfo__ctor_m5665381247ECA422AB1FAB1EDBC5A883DFA0D20C (MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F * __this, bool ___initialized0, const RuntimeMethod* method);
// System.Void Kolibri2d.Terrain2DMaterial/CornerSprites2::.ctor()
extern "C" IL2CPP_METHOD_ATTR void CornerSprites2__ctor_m1AF5E76E4493B22EC500394DF591D669EBAA1458 (CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703 * __this, const RuntimeMethod* method);
// System.Void Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SortingLayerOptions2__ctor_m5CE73EC2FC70B971D15CE81AC4C4EDC3EA4C49DC (SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE * __this, const RuntimeMethod* method);
// System.Void Kolibri2d.Terrain2DMaterial/SortingZ_3D_Options::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SortingZ_3D_Options__ctor_m9944ABAD4B79DEC1A85324A1CA2F9FCE738CBA14 (SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F * __this, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_gray()
extern "C" IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_get_gray_m11EEED9092A8D949D3326611C7137334B746DF09 (const RuntimeMethod* method);
// System.Void Kolibri2d.MaterialTextureInfo::Reset()
extern "C" IL2CPP_METHOD_ATTR void MaterialTextureInfo_Reset_m10A220F3D8334B1734C642F93409B8EB9C375289 (MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Dictionary`2<Kolibri2d.TerrainType,Kolibri2d.MaterialTextureInfo>::get_Count()
inline int32_t Dictionary_2_get_Count_mB1BF016039CF52477957DBB0568653081D14612E (Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A *, const RuntimeMethod*))Dictionary_2_get_Count_mB1BF016039CF52477957DBB0568653081D14612E_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<Kolibri2d.TerrainType,Kolibri2d.MaterialTextureInfo>::set_Item(!0,!1)
inline void Dictionary_2_set_Item_m85881E2C19054C55C36474E5876AC2AB23552E0B (Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A * __this, int32_t p0, MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  p1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A *, int32_t, MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F , const RuntimeMethod*))Dictionary_2_set_Item_m85881E2C19054C55C36474E5876AC2AB23552E0B_gshared)(__this, p0, p1, method);
}
// System.Void Kolibri2d.Terrain2DMaterial::RefreshIfMissingData()
extern "C" IL2CPP_METHOD_ATTR void Terrain2DMaterial_RefreshIfMissingData_m7CACBD5F7AD5D5BBBA21AD4B2CD65E51A9DC77E8 (Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * __this, const RuntimeMethod* method);
// System.Int32 Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::GetSortingLayer(Kolibri2d.TerrainType)
extern "C" IL2CPP_METHOD_ATTR int32_t SortingLayerOptions2_GetSortingLayer_m259EE7D3F8D0343560E701ACF982B9683C092F15 (SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE * __this, int32_t ___e0, const RuntimeMethod* method);
// System.Int32 Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::GetOrderInLayer(Kolibri2d.TerrainType)
extern "C" IL2CPP_METHOD_ATTR int32_t SortingLayerOptions2_GetOrderInLayer_mC6BD69D8605F124803B63ACDA6E40112C8B2667B (SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE * __this, int32_t ___e0, const RuntimeMethod* method);
// System.Single Kolibri2d.Terrain2DMaterial/SortingZ_3D_Options::GetZDepth(Kolibri2d.TerrainType)
extern "C" IL2CPP_METHOD_ATTR float SortingZ_3D_Options_GetZDepth_mFB12B76278730CA1BECC46930191E63EFF0645FD (SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F * __this, int32_t ___e0, const RuntimeMethod* method);
// UnityEngine.Sprite Kolibri2d.Terrain2DMaterial/CornerSprites2::GetCornerSprite(Kolibri2d.Terrain2DMaterial/CornerType,Kolibri2d.Terrain2DMaterial/CornerSide)
extern "C" IL2CPP_METHOD_ATTR Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * CornerSprites2_GetCornerSprite_m6009BCA395A2ADDEB0CD134CF0949525A833F595 (CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703 * __this, int32_t ___c0, int32_t ___side1, const RuntimeMethod* method);
// UnityEngine.Vector2 Kolibri2d.Terrain2DMaterial/CornerSprites2::GetCornerOffset(Kolibri2d.Terrain2DMaterial/CornerType,Kolibri2d.Terrain2DMaterial/CornerSide)
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  CornerSprites2_GetCornerOffset_mE32AE7EB9004D2EAEC9BB00049F278F24CEF6320 (CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703 * __this, int32_t ___c0, int32_t ___side1, const RuntimeMethod* method);
// System.Int32 Kolibri2d.Terrain2DMaterial/CornerSprites2::GetCornerExtraRotation(Kolibri2d.Terrain2DMaterial/CornerType,Kolibri2d.Terrain2DMaterial/CornerSide)
extern "C" IL2CPP_METHOD_ATTR int32_t CornerSprites2_GetCornerExtraRotation_mA2E459C1D40E476E275910F13E595F4DEFB78285 (CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703 * __this, int32_t ___c0, int32_t ___side1, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<Kolibri2d.TerrainType,Kolibri2d.MaterialTextureInfo>::get_Item(!0)
inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  Dictionary_2_get_Item_mF1FB9229F3B369C779CEF8B43902E24D6C3FCD3D (Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  (*) (Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A *, int32_t, const RuntimeMethod*))Dictionary_2_get_Item_mF1FB9229F3B369C779CEF8B43902E24D6C3FCD3D_gshared)(__this, p0, method);
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8 (const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector3__ctor_m6AD8F21FFCC7723C6F507CCF2E4E2EFFC4871584 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, float p0, float p1, const RuntimeMethod* method);
// UnityEngine.Vector3 Kolibri2d.Bezier::GetFirstDerivative(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Bezier_GetFirstDerivative_mC9145E1496744EA98B64F8CE58B27DC5DCC21A30 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___p00, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___p11, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___p22, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___p33, float ___t4, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2 (const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR bool Vector3_op_Equality_mA9E2F96E98E71AE7ACCE74766D700D41F0404806 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
extern "C" IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Transform_Find_m673797B6329C2669A543904532ABA1680DA4EAD1 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, String_t* p0, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, String_t* p0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_parent_m65B8E4660B2C554069C57A957D9E55FECA7AA73E (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_one_mA11B83037CB269C6076CBCF754E24C8F3ACEC2AB (const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64 (const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.Vector3>::GetEnumerator()
inline Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F  List_1_GetEnumerator_mA6AD60947F39CE54DA7D237D278C8948D5B2C331 (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F  (*) (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *, const RuntimeMethod*))List_1_GetEnumerator_mA6AD60947F39CE54DA7D237D278C8948D5B2C331_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::get_Current()
inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Enumerator_get_Current_mAB079301955D01523AB6CD62D400632D218B265D (Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F * __this, const RuntimeMethod* method)
{
	return ((  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  (*) (Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F *, const RuntimeMethod*))Enumerator_get_Current_mAB079301955D01523AB6CD62D400632D218B265D_gshared)(__this, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Division_mDF34F1CC445981B4D1137765BC6277419E561624 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, float p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::MoveNext()
inline bool Enumerator_MoveNext_mFB1EBCAC4E921F22042B0BDB6B954B7A09589454 (Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F *, const RuntimeMethod*))Enumerator_MoveNext_mFB1EBCAC4E921F22042B0BDB6B954B7A09589454_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::Dispose()
inline void Enumerator_Dispose_m640FFEF2E9A18C15D423069973168AD40E10FBFB (Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F *, const RuntimeMethod*))Enumerator_Dispose_m640FFEF2E9A18C15D423069973168AD40E10FBFB_gshared)(__this, method);
}
// System.Void UnityEngine.Bounds::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Bounds__ctor_m294E77A20EC1A3E96985FE1A925CB271D1B5266D (Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD202DDB933765AC3989986C7FA3C14E6E9A191EC (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m41E09C4CA476451FE275573062956CED105CB79A_gshared)(__this, method);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Bounds UnityEngine.Renderer::get_bounds()
extern "C" IL2CPP_METHOD_ATTR Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  Renderer_get_bounds_mB29E41E26DD95939C09F3EC67F5B2793A438BDB5 (Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Bounds)
extern "C" IL2CPP_METHOD_ATTR void Bounds_Encapsulate_m394E7F823ADE56B97E6723B645C9458F1ADF0089 (Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * __this, Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  p0, const RuntimeMethod* method);
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Renderer>()
inline RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* GameObject_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_m8BA044694988F70856600001893D4CB591BD5B29 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponentsInChildren_TisRuntimeObject_m615406921CD720805AC5F99C672532A31C81D1C7_gshared)(__this, method);
}
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<Kolibri2d.BezierSpline>()
inline BezierSplineU5BU5D_tD9962D7525021265D09A9C68F754780C16917F1C* GameObject_GetComponentsInChildren_TisBezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_m968410DEC000954AFE3CA0F676596D8F14B8A570 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  BezierSplineU5BU5D_tD9962D7525021265D09A9C68F754780C16917F1C* (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponentsInChildren_TisRuntimeObject_m615406921CD720805AC5F99C672532A31C81D1C7_gshared)(__this, method);
}
// UnityEngine.Bounds Kolibri2d.BezierSpline::GetBounds()
extern "C" IL2CPP_METHOD_ATTR Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  BezierSpline_GetBounds_m0AA9FA3C3BC4BD110D1521173CC24A85095600D1 (BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Bounds_get_center_m4FB6E99F0533EE2D432988B08474D6DC9B8B744B (Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Bounds_get_size_m0739F2686AE2D3416A33AEF892653091347FD4A6 (Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor(System.Int32)
inline void List_1__ctor_m6DB64BAFEC79776AB013124B548E074CAA2FAD95 (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, int32_t p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *, int32_t, const RuntimeMethod*))List_1__ctor_m6DB64BAFEC79776AB013124B548E074CAA2FAD95_gshared)(__this, p0, method);
}
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * __this, float p0, float p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(!0)
inline void List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857 (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D , const RuntimeMethod*))List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857_gshared)(__this, p0, method);
}
// System.Void UnityEngine.Vector3::Normalize()
extern "C" IL2CPP_METHOD_ATTR void Vector3_Normalize_m174460238EC6322B9095A378AA8624B1DD9000F3 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_normalized()
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_get_normalized_m058E75C38C6FC66E178D7C8EF1B6298DE8F0E14B (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * __this, const RuntimeMethod* method);
// System.Single Kolibri2d.Utils::Cross(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR float Utils_Cross_m0B48E0950C9AF1B9708E346B5D1B37D728958BD9 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___v10, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___v21, const RuntimeMethod* method);
// System.Boolean Kolibri2d.Utils::IsZero(System.Double)
extern "C" IL2CPP_METHOD_ATTR bool Utils_IsZero_m4B8A254B0F87897812D1DA9263A8CE9BFD9A8FD9 (double ___d0, const RuntimeMethod* method);
// UnityEngine.Vector2 Kolibri2d.Utils::Mult(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Utils_Mult_mAF1AC9C9C45260976167E0D519D6CEFD1C875AEB (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___a0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector2::get_magnitude()
extern "C" IL2CPP_METHOD_ATTR float Vector2_get_magnitude_m66097AFDF9696BD3E88467D4398D4F82B8A4C7DF (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(System.Single,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Multiply_m2E30A54E315810911DFC2E25C700757A68AC1F38 (float p0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_width()
extern "C" IL2CPP_METHOD_ATTR float Rect_get_width_m54FF69FC2C086E2DC349ED091FD0D6576BFB1484 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_height()
extern "C" IL2CPP_METHOD_ATTR float Rect_get_height_m088C36990E0A255C5D7DCE36575DCE23ABB364B5 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Texture2D__ctor_m22561E039BC96019757E6B2427BE09734AE2C44A (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, int32_t p0, int32_t p1, int32_t p2, bool p3, const RuntimeMethod* method);
// UnityEngine.RenderTexture UnityEngine.RenderTexture::get_active()
extern "C" IL2CPP_METHOD_ATTR RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * RenderTexture_get_active_m670416A37BF4239DE5A55F6138CAA1FEEF184957 (const RuntimeMethod* method);
// System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void RenderTexture__ctor_mB54A3ABBD56D38AB762D0AB8B789E2771BC42A7D (RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::.ctor(UnityEngine.Rect)
extern "C" IL2CPP_METHOD_ATTR void Rect__ctor_m53B98CABA163EB899C81CB3694EB9E2AC19D8C90 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, Rect_t35B976DE901B5423C11705E156938EA27AB402CE  p0, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_y()
extern "C" IL2CPP_METHOD_ATTR float Rect_get_y_m53E3E4F62D9840FBEA751A66293038F1F5D1D45C (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::set_y(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Rect_set_y_mCFDB9BD77334EF9CD896F64BE63C755777D7CCD5 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, float p0, const RuntimeMethod* method);
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture)
extern "C" IL2CPP_METHOD_ATTR void Graphics_Blit_mB042EC04307A5617038DA4210DE7BA4B3E529113 (Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * p0, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * p1, const RuntimeMethod* method);
// System.Void UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)
extern "C" IL2CPP_METHOD_ATTR void RenderTexture_set_active_m992E25C701DEFC8042B31022EA45F02A787A84F1 (RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Texture2D_ReadPixels_m1ED1C11E41D0ACC8CFCABBD25946CF0BD16D4F61 (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, Rect_t35B976DE901B5423C11705E156938EA27AB402CE  p0, int32_t p1, int32_t p2, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::Apply()
extern "C" IL2CPP_METHOD_ATTR void Texture2D_Apply_m0F3B4A4B1B89E44E2AF60ABDEFAA18D93735B5CA (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, const RuntimeMethod* method);
// UnityEngine.FilterMode UnityEngine.Texture::get_filterMode()
extern "C" IL2CPP_METHOD_ATTR int32_t Texture_get_filterMode_m4A05E0414655866D27811376709B3A8C584A2579 (Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)
extern "C" IL2CPP_METHOD_ATTR void Texture_set_filterMode_mB9AC927A527EFE95771B9B438E2CFB9EDA84AF01 (Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)
extern "C" IL2CPP_METHOD_ATTR void Texture_set_wrapMode_m85E9A995D5947B59FE13A7311E891F3DEDEBBCEC (Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * __this, int32_t p0, const RuntimeMethod* method);
// UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
extern "C" IL2CPP_METHOD_ATTR Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* Mesh_get_vertices_m7D07DC0F071C142B87F675B148FC0F7A243238B9 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
inline void List_1__ctor_m0E7CD0EBE7EA0CCD1DDA59B1F34C56CDB1F79827 (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *, RuntimeObject*, const RuntimeMethod*))List_1__ctor_m0E7CD0EBE7EA0CCD1DDA59B1F34C56CDB1F79827_gshared)(__this, p0, method);
}
// UnityEngine.Vector3[] UnityEngine.Mesh::get_normals()
extern "C" IL2CPP_METHOD_ATTR Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* Mesh_get_normals_m3CE4668899836CBD17C3F85EB24261CBCEB3EABB (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// UnityEngine.Color[] UnityEngine.Mesh::get_colors()
extern "C" IL2CPP_METHOD_ATTR ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* Mesh_get_colors_m16A1999334A7013DD5A1FF619E51983B4BFF7848 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
inline void List_1__ctor_m6A5DF174C63F24D00FC3CCA142FF30E9FE3B0B84 (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E *, RuntimeObject*, const RuntimeMethod*))List_1__ctor_m6A5DF174C63F24D00FC3CCA142FF30E9FE3B0B84_gshared)(__this, p0, method);
}
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv()
extern "C" IL2CPP_METHOD_ATTR Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* Mesh_get_uv_m0EBA5CA4644C9D5F1B2125AF3FE3873EFC8A4616 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
inline void List_1__ctor_mCAB49C9A39B98BC7B6D7CEDCA6AE260556381C09 (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *, RuntimeObject*, const RuntimeMethod*))List_1__ctor_mCAB49C9A39B98BC7B6D7CEDCA6AE260556381C09_gshared)(__this, p0, method);
}
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv2()
extern "C" IL2CPP_METHOD_ATTR Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* Mesh_get_uv2_m3E70D5DD7A5C6910A074A78296269EBF2CBAE97F (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
inline void List_1__ctor_m80FC80854B1AD97640F2B64DE9B250A3D8DAB145 (List_1_t4A961510635950236678F1B3B2436ECCAF28713A * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4A961510635950236678F1B3B2436ECCAF28713A *, const RuntimeMethod*))List_1__ctor_m80FC80854B1AD97640F2B64DE9B250A3D8DAB145_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Int32>::ContainsKey(!0)
inline bool Dictionary_2_ContainsKey_m230D107B4D3C342ECBC961100D82015C14534FCC (Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F * __this, uint32_t p0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F *, uint32_t, const RuntimeMethod*))Dictionary_2_ContainsKey_m230D107B4D3C342ECBC961100D82015C14534FCC_gshared)(__this, p0, method);
}
// !1 System.Collections.Generic.Dictionary`2<System.UInt32,System.Int32>::get_Item(!0)
inline int32_t Dictionary_2_get_Item_mCECAC44EF2E9AE18023E7FDAD8CC268588065CE3 (Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F * __this, uint32_t p0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F *, uint32_t, const RuntimeMethod*))Dictionary_2_get_Item_mCECAC44EF2E9AE18023E7FDAD8CC268588065CE3_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Int32>::Add(!0,!1)
inline void Dictionary_2_Add_mDCE3CAF8CAFEBD13FB38A51C069F27896353ECB5 (Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F * __this, uint32_t p0, int32_t p1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F *, uint32_t, int32_t, const RuntimeMethod*))Dictionary_2_Add_mDCE3CAF8CAFEBD13FB38A51C069F27896353ECB5_gshared)(__this, p0, p1, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, float p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
inline void List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76 (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 , const RuntimeMethod*))List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_gshared)(__this, p0, method);
}
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color>::get_Count()
inline int32_t List_1_get_Count_m9E6BB40B113CEAFD4EA6A2BD7FCEB7259C20561D (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E *, const RuntimeMethod*))List_1_get_Count_m9E6BB40B113CEAFD4EA6A2BD7FCEB7259C20561D_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1<UnityEngine.Color>::get_Item(System.Int32)
inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  List_1_get_Item_mB725E23AF0513F5A32B32BBC9C2471097DE235C4 (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  (*) (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E *, int32_t, const RuntimeMethod*))List_1_get_Item_mB725E23AF0513F5A32B32BBC9C2471097DE235C4_gshared)(__this, p0, method);
}
// UnityEngine.Color UnityEngine.Color::op_Addition(UnityEngine.Color,UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_op_Addition_m26E4EEFAF21C0C0DC2FD93CE594E02F61DB1F7DF (Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  p0, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  p1, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::op_Multiply(UnityEngine.Color,System.Single)
extern "C" IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_op_Multiply_mF36917AD6235221537542FD079817CAB06CB1934 (Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  p0, float p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::Add(!0)
inline void List_1_Add_m00EB6B0859FF6BCA9F89EF1FE572855EAA143F8F (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * __this, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E *, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 , const RuntimeMethod*))List_1_Add_m00EB6B0859FF6BCA9F89EF1FE572855EAA143F8F_gshared)(__this, p0, method);
}
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Count()
inline int32_t List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7 (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *, const RuntimeMethod*))List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Item(System.Int32)
inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  (*) (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *, int32_t, const RuntimeMethod*))List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B_gshared)(__this, p0, method);
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Multiply_m8A843A37F2F3199EBE99DC7BDABC1DC2EE01AF56 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p0, float p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Int32>::.ctor()
inline void Dictionary_2__ctor_m7E5325C12ECF67A2A855BBDC04968EE0CC66ACF1 (Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F *, const RuntimeMethod*))Dictionary_2__ctor_m7E5325C12ECF67A2A855BBDC04968EE0CC66ACF1_gshared)(__this, method);
}
// System.Void Kolibri2d.Utils/MeshHelper2::InitArrays(UnityEngine.Mesh)
extern "C" IL2CPP_METHOD_ATTR void MeshHelper2_InitArrays_mDBEB2310C61A2DB7A2EDC56A961F92F7CDCF1681 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh0, const RuntimeMethod* method);
// System.Int32[] UnityEngine.Mesh::get_triangles()
extern "C" IL2CPP_METHOD_ATTR Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* Mesh_get_triangles_mAAAFC770B8EE3F56992D5764EF8C2EC06EF743AC (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, const RuntimeMethod* method);
// System.Int32 Kolibri2d.Utils/MeshHelper2::GetNewVertex4(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t MeshHelper2_GetNewVertex4_m1B46BFC32053E23F3C9F90347AAF3EECD46B715F (int32_t ___i10, int32_t ___i21, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
inline void List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733 (List_1_t4A961510635950236678F1B3B2436ECCAF28713A * __this, int32_t p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4A961510635950236678F1B3B2436ECCAF28713A *, int32_t, const RuntimeMethod*))List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_gshared)(__this, p0, method);
}
// System.Void UnityEngine.Mesh::SetVertices(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C" IL2CPP_METHOD_ATTR void Mesh_SetVertices_m5F487FC255C9CAF4005B75CFE67A88C8C0E7BB06 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * p0, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<UnityEngine.Vector3>::ToArray()
inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* List_1_ToArray_m062BF9DC0570B6EEC207AAEDDEC602FB2EB3F9BA (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * __this, const RuntimeMethod* method)
{
	return ((  Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* (*) (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *, const RuntimeMethod*))List_1_ToArray_m062BF9DC0570B6EEC207AAEDDEC602FB2EB3F9BA_gshared)(__this, method);
}
// System.Void UnityEngine.Mesh::set_normals(UnityEngine.Vector3[])
extern "C" IL2CPP_METHOD_ATTR void Mesh_set_normals_m4054D319A67DAAA25A794D67AD37278A84406589 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* p0, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<UnityEngine.Color>::ToArray()
inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* List_1_ToArray_mE9F8DDC7219DCC0198F4A265DBD77BC4307FA6F4 (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * __this, const RuntimeMethod* method)
{
	return ((  ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* (*) (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E *, const RuntimeMethod*))List_1_ToArray_mE9F8DDC7219DCC0198F4A265DBD77BC4307FA6F4_gshared)(__this, method);
}
// System.Void UnityEngine.Mesh::set_colors(UnityEngine.Color[])
extern "C" IL2CPP_METHOD_ATTR void Mesh_set_colors_m704D0EF58B7AED0D64AE4763EA375638FB08E026 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* p0, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<UnityEngine.Vector2>::ToArray()
inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* List_1_ToArray_mB9E07F625DF343AED13593FB79319A4ADFC02AB7 (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * __this, const RuntimeMethod* method)
{
	return ((  Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* (*) (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *, const RuntimeMethod*))List_1_ToArray_mB9E07F625DF343AED13593FB79319A4ADFC02AB7_gshared)(__this, method);
}
// System.Void UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])
extern "C" IL2CPP_METHOD_ATTR void Mesh_set_uv_m56E4B52315669FBDA89DC9C550AC89EEE8A4E7C8 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Mesh::set_uv2(UnityEngine.Vector2[])
extern "C" IL2CPP_METHOD_ATTR void Mesh_set_uv2_m8D37F9622B68D4CEA7FCF96BE4F8E442155DB038 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* p0, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<System.Int32>::ToArray()
inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* List_1_ToArray_mE3701008FD26BBC8184384CA22962898157F7BAF (List_1_t4A961510635950236678F1B3B2436ECCAF28713A * __this, const RuntimeMethod* method)
{
	return ((  Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* (*) (List_1_t4A961510635950236678F1B3B2436ECCAF28713A *, const RuntimeMethod*))List_1_ToArray_mE3701008FD26BBC8184384CA22962898157F7BAF_gshared)(__this, method);
}
// System.Void UnityEngine.Mesh::set_triangles(System.Int32[])
extern "C" IL2CPP_METHOD_ATTR void Mesh_set_triangles_m143A1C262BADCFACE43587EBA2CDC6EBEB5DFAED (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * __this, Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* p0, const RuntimeMethod* method);
// System.Void Kolibri2d.Utils/MeshHelper2::CleanUp()
extern "C" IL2CPP_METHOD_ATTR void MeshHelper2_CleanUp_mBC2EA65D0BD5F6AA5D87F7C6259B63330A5B62B9 (const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::op_Division(UnityEngine.Color,System.Single)
extern "C" IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color_op_Division_m52600EAF26D4F0F781929486BEB9273938807B82 (Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  p0, float p1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Division(UnityEngine.Vector2,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Division_m0961A935168EE6701E098E2B37013DFFF46A5077 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p0, float p1, const RuntimeMethod* method);
// System.Int32 Kolibri2d.Utils/MeshHelper2::GetNewVertex9(System.Int32,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t MeshHelper2_GetNewVertex9_m83649A9E4437ABA844B158098FB1B65272FB3D8C (int32_t ___i10, int32_t ___i21, int32_t ___i32, const RuntimeMethod* method);
// System.Void Kolibri2d.Utils/MeshHelper2::Subdivide9(UnityEngine.Mesh)
extern "C" IL2CPP_METHOD_ATTR void MeshHelper2_Subdivide9_m4FB982D43A206A43AE8BF55805987803940FBE13 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh0, const RuntimeMethod* method);
// System.Void Kolibri2d.Utils/MeshHelper2::Subdivide4(UnityEngine.Mesh)
extern "C" IL2CPP_METHOD_ATTR void MeshHelper2_Subdivide4_m222D393186AD66CD72DF6F297E2C38AF69EC8E90 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<Kolibri2d.SplineWater>()
inline SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8 * Component_GetComponent_TisSplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8_m2CF9090BA6AB33D29C9FC86E7FE19E20187241F8 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared)(__this, method);
}
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Kolibri2d.SplineWater/WaterEvent::.ctor()
extern "C" IL2CPP_METHOD_ATTR void WaterEvent__ctor_mEA2A570BF2B1212229659F6F9844F4EE32429FB0 (WaterEvent_t414CD81AE9F28B656088F21ECC14F030B1FDA33B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaterEvent__ctor_mEA2A570BF2B1212229659F6F9844F4EE32429FB0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m0664E6A8259E48D52CF88851228B1DC47F751C48(__this, /*hidden argument*/UnityEvent_1__ctor_m0664E6A8259E48D52CF88851228B1DC47F751C48_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Kolibri2d.Terrain2D::SetCreator()
extern "C" IL2CPP_METHOD_ATTR void Terrain2D_SetCreator_m5DF018BBE08473E58C9FC42A45076CB16A6DAA4D (Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Terrain2D_SetCreator_m5DF018BBE08473E58C9FC42A45076CB16A6DAA4D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1 * L_0 = __this->get_creator_10();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1 * L_1 = (SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1 *)il2cpp_codegen_object_new(SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1_il2cpp_TypeInfo_var);
		SplineTerrain2DCreator__ctor_m580BBD9F2E83001DF2B1AD5B68C9F5630E27657F(L_1, __this, /*hidden argument*/NULL);
		__this->set_creator_10(L_1);
		return;
	}

IL_0015:
	{
		SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1 * L_2 = __this->get_creator_10();
		NullCheck(L_2);
		SplineTerrain2DCreator_SetTerrain_m2B191F050A07085B2B7835DA44EB68EADD6D681C(L_2, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Kolibri2d.Terrain2D::Reset()
extern "C" IL2CPP_METHOD_ATTR void Terrain2D_Reset_mCE35946462E22190DD164BDF64E03C8A263DA43F (Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Terrain2D_Reset_mCE35946462E22190DD164BDF64E03C8A263DA43F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SplineModifierWithCallbacks_Reset_m6071B127F32D6ECA8C3963C11A9C1DA3E74E0E8F(__this, /*hidden argument*/NULL);
		List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 * L_0 = (List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 *)il2cpp_codegen_object_new(List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4_il2cpp_TypeInfo_var);
		List_1__ctor_m9E8734104EA5EA636BBA9DD4E5E56DC6C7C1B063(L_0, /*hidden argument*/List_1__ctor_m9E8734104EA5EA636BBA9DD4E5E56DC6C7C1B063_RuntimeMethod_var);
		__this->set_materialHolders_9(L_0);
		List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 * L_1 = __this->get_materialHolders_9();
		Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * L_2 = (Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F *)il2cpp_codegen_object_new(Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F_il2cpp_TypeInfo_var);
		Terrain2DMaterialHolder__ctor_mD9ADF2A9EA239E77BCBD828F337A70412D6BB96B(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		List_1_Add_mF10FF505AE13DE269187B75AC591D0D47188FF0B(L_1, L_2, /*hidden argument*/List_1_Add_mF10FF505AE13DE269187B75AC591D0D47188FF0B_RuntimeMethod_var);
		Terrain2D_SetCreator_m5DF018BBE08473E58C9FC42A45076CB16A6DAA4D(__this, /*hidden argument*/NULL);
		Terrain2D_ClearAll_m40A08E96E6E9ED3C435653E59D15393811EA1B29(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Kolibri2d.Terrain2D::OnUpdateCallback()
extern "C" IL2CPP_METHOD_ATTR void Terrain2D_OnUpdateCallback_mAD534125501BF9146F53CB8B2E15D9F104948402 (Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B * __this, const RuntimeMethod* method)
{
	{
		SplineModifierWithCallbacks_RefreshComponents_m715A574992FA4002704981C5792BD4BE05A5A2E8(__this, /*hidden argument*/NULL);
		Terrain2D_RefreshColliders_m2CD6434AC10F2F0369129AE06C3936F1A6BA15E4(__this, /*hidden argument*/NULL);
		Terrain2D_SetCreator_m5DF018BBE08473E58C9FC42A45076CB16A6DAA4D(__this, /*hidden argument*/NULL);
		SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1 * L_0 = __this->get_creator_10();
		NullCheck(L_0);
		SplineTerrain2DCreator_Refresh_mEC5BD746E18701BACF47CB916EED1EB406A15D5C(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Kolibri2d.Terrain2D::ClearAll()
extern "C" IL2CPP_METHOD_ATTR void Terrain2D_ClearAll_m40A08E96E6E9ED3C435653E59D15393811EA1B29 (Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Terrain2D_ClearAll_m40A08E96E6E9ED3C435653E59D15393811EA1B29_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Terrain2D_SetCreator_m5DF018BBE08473E58C9FC42A45076CB16A6DAA4D(__this, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A_il2cpp_TypeInfo_var);
		SplineColliderCreator_Clear_m0BACF4B997CF3D8991A3503901CCEBB1DA9A0A43(L_0, /*hidden argument*/NULL);
		SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1 * L_1 = __this->get_creator_10();
		NullCheck(L_1);
		SplineTerrain2DCreator_ClearAll_mF7790FD9F6595DA333A63133844DA1816D0C2862(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Kolibri2d.Terrain2D::RefreshColliders()
extern "C" IL2CPP_METHOD_ATTR void Terrain2D_RefreshColliders_m2CD6434AC10F2F0369129AE06C3936F1A6BA15E4 (Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Terrain2D_RefreshColliders_m2CD6434AC10F2F0369129AE06C3936F1A6BA15E4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A * V_0 = NULL;
	Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A * L_0 = Component_GetComponent_TisSplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A_m434AAC7C9A971A8416BB64D702DA25F199DFB2C0(__this, /*hidden argument*/Component_GetComponent_TisSplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A_m434AAC7C9A971A8416BB64D702DA25F199DFB2C0_RuntimeMethod_var);
		V_0 = L_0;
		SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_1, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = Behaviour_get_isActiveAndEnabled_mC42DFCC1ECC2C94D52928FFE446CE7E266CA8B61(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A_il2cpp_TypeInfo_var);
		SplineColliderCreator_Clear_m0BACF4B997CF3D8991A3503901CCEBB1DA9A0A43(L_5, /*hidden argument*/NULL);
		List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 * L_6 = __this->get_materialHolders_9();
		NullCheck(L_6);
		Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7  L_7 = List_1_GetEnumerator_mA9C0CCE2B4A25A7A18800BE1E1E3DD78AE5E9515(L_6, /*hidden argument*/List_1_GetEnumerator_mA9C0CCE2B4A25A7A18800BE1E1E3DD78AE5E9515_RuntimeMethod_var);
		V_1 = L_7;
	}

IL_0030:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0089;
		}

IL_0032:
		{
			Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * L_8 = Enumerator_get_Current_m063115BDDF8FC2B26C79A4AD2BCB44A7661BBA28((Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7 *)(&V_1), /*hidden argument*/Enumerator_get_Current_m063115BDDF8FC2B26C79A4AD2BCB44A7661BBA28_RuntimeMethod_var);
			V_2 = L_8;
			Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * L_9 = V_2;
			NullCheck(L_9);
			bool L_10 = L_9->get_enabled_0();
			if (!L_10)
			{
				goto IL_0089;
			}
		}

IL_0042:
		{
			Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * L_11 = V_2;
			NullCheck(L_11);
			Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * L_12 = L_11->get_terrainMaterial_1();
			IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
			bool L_13 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_12, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
			if (!L_13)
			{
				goto IL_0089;
			}
		}

IL_0050:
		{
			Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * L_14 = V_2;
			NullCheck(L_14);
			Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * L_15 = L_14->get_terrainMaterial_1();
			NullCheck(L_15);
			SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4 * L_16 = L_15->get_settings_19();
			NullCheck(L_16);
			SplineColliderSettings_Refresh_mED92DD849162B0E6CB5D25F42853409E7FFFAF1A(L_16, /*hidden argument*/NULL);
			Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * L_17 = V_2;
			NullCheck(L_17);
			bool L_18 = L_17->get_createColliders_5();
			if (!L_18)
			{
				goto IL_0089;
			}
		}

IL_0068:
		{
			BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * L_19 = ((SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232 *)__this)->get_spline_4();
			Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * L_20 = V_2;
			NullCheck(L_20);
			Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * L_21 = L_20->get_terrainMaterial_1();
			NullCheck(L_21);
			SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4 * L_22 = L_21->get_settings_19();
			Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * L_23 = V_2;
			NullCheck(L_23);
			Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * L_24 = L_23->get_terrainMaterial_1();
			NullCheck(L_24);
			int32_t L_25 = L_24->get_objectLayer_20();
			IL2CPP_RUNTIME_CLASS_INIT(SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A_il2cpp_TypeInfo_var);
			SplineColliderCreator_CreateColliders_m6ECFBD78F2CAF8EA0C68DA612D50E9862ACB1BC8(L_19, L_22, L_25, /*hidden argument*/NULL);
		}

IL_0089:
		{
			bool L_26 = Enumerator_MoveNext_m168734AAA4869F1578DF4D4538B74BFF12522F68((Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7 *)(&V_1), /*hidden argument*/Enumerator_MoveNext_m168734AAA4869F1578DF4D4538B74BFF12522F68_RuntimeMethod_var);
			if (L_26)
			{
				goto IL_0032;
			}
		}

IL_0092:
		{
			IL2CPP_LEAVE(0xA2, FINALLY_0094);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0094;
	}

FINALLY_0094:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_mCDA3AD77115D33F3528CD546C85CA20239DD4294((Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7 *)(&V_1), /*hidden argument*/Enumerator_Dispose_mCDA3AD77115D33F3528CD546C85CA20239DD4294_RuntimeMethod_var);
		IL2CPP_END_FINALLY(148)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(148)
	{
		IL2CPP_JUMP_TBL(0xA2, IL_00a2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00a2:
	{
		return;
	}
}
// System.Boolean Kolibri2d.Terrain2D::ContainsTerrainMaterial(Kolibri2d.Terrain2DMaterial)
extern "C" IL2CPP_METHOD_ATTR bool Terrain2D_ContainsTerrainMaterial_m159B1951A499694BB61D9B2C791676D99B2D4B80 (Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B * __this, Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * ___p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Terrain2D_ContainsTerrainMaterial_m159B1951A499694BB61D9B2C791676D99B2D4B80_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * V_1 = NULL;
	bool V_2 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * L_0 = ___p0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0058;
		}
	}
	{
		List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 * L_2 = __this->get_materialHolders_9();
		NullCheck(L_2);
		Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7  L_3 = List_1_GetEnumerator_mA9C0CCE2B4A25A7A18800BE1E1E3DD78AE5E9515(L_2, /*hidden argument*/List_1_GetEnumerator_mA9C0CCE2B4A25A7A18800BE1E1E3DD78AE5E9515_RuntimeMethod_var);
		V_0 = L_3;
	}

IL_0015:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003f;
		}

IL_0017:
		{
			Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * L_4 = Enumerator_get_Current_m063115BDDF8FC2B26C79A4AD2BCB44A7661BBA28((Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7 *)(&V_0), /*hidden argument*/Enumerator_get_Current_m063115BDDF8FC2B26C79A4AD2BCB44A7661BBA28_RuntimeMethod_var);
			V_1 = L_4;
			Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * L_5 = V_1;
			NullCheck(L_5);
			Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * L_6 = L_5->get_terrainMaterial_1();
			IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
			bool L_7 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_6, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * L_8 = ___p0;
			Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * L_9 = V_1;
			NullCheck(L_9);
			Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * L_10 = L_9->get_terrainMaterial_1();
			IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
			bool L_11 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_8, L_10, /*hidden argument*/NULL);
			if (!L_11)
			{
				goto IL_003f;
			}
		}

IL_003b:
		{
			V_2 = (bool)1;
			IL2CPP_LEAVE(0x5A, FINALLY_004a);
		}

IL_003f:
		{
			bool L_12 = Enumerator_MoveNext_m168734AAA4869F1578DF4D4538B74BFF12522F68((Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7 *)(&V_0), /*hidden argument*/Enumerator_MoveNext_m168734AAA4869F1578DF4D4538B74BFF12522F68_RuntimeMethod_var);
			if (L_12)
			{
				goto IL_0017;
			}
		}

IL_0048:
		{
			IL2CPP_LEAVE(0x58, FINALLY_004a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_004a;
	}

FINALLY_004a:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_mCDA3AD77115D33F3528CD546C85CA20239DD4294((Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7 *)(&V_0), /*hidden argument*/Enumerator_Dispose_mCDA3AD77115D33F3528CD546C85CA20239DD4294_RuntimeMethod_var);
		IL2CPP_END_FINALLY(74)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(74)
	{
		IL2CPP_JUMP_TBL(0x5A, IL_005a)
		IL2CPP_JUMP_TBL(0x58, IL_0058)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0058:
	{
		return (bool)0;
	}

IL_005a:
	{
		bool L_13 = V_2;
		return L_13;
	}
}
// System.Boolean Kolibri2d.Terrain2D::ContainsEnabledTerrainMaterial(Kolibri2d.Terrain2DMaterial)
extern "C" IL2CPP_METHOD_ATTR bool Terrain2D_ContainsEnabledTerrainMaterial_m8A1FD92C1A1D4B25A8B82D4CDEDA48CB95DD8B6C (Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B * __this, Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * ___p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Terrain2D_ContainsEnabledTerrainMaterial_m8A1FD92C1A1D4B25A8B82D4CDEDA48CB95DD8B6C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * V_1 = NULL;
	bool V_2 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * L_0 = ___p0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0052;
		}
	}
	{
		List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 * L_2 = __this->get_materialHolders_9();
		NullCheck(L_2);
		Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7  L_3 = List_1_GetEnumerator_mA9C0CCE2B4A25A7A18800BE1E1E3DD78AE5E9515(L_2, /*hidden argument*/List_1_GetEnumerator_mA9C0CCE2B4A25A7A18800BE1E1E3DD78AE5E9515_RuntimeMethod_var);
		V_0 = L_3;
	}

IL_0015:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0039;
		}

IL_0017:
		{
			Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * L_4 = Enumerator_get_Current_m063115BDDF8FC2B26C79A4AD2BCB44A7661BBA28((Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7 *)(&V_0), /*hidden argument*/Enumerator_get_Current_m063115BDDF8FC2B26C79A4AD2BCB44A7661BBA28_RuntimeMethod_var);
			V_1 = L_4;
			Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * L_5 = V_1;
			NullCheck(L_5);
			bool L_6 = L_5->get_enabled_0();
			if (!L_6)
			{
				goto IL_0039;
			}
		}

IL_0027:
		{
			Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * L_7 = ___p0;
			Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * L_8 = V_1;
			NullCheck(L_8);
			Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * L_9 = L_8->get_terrainMaterial_1();
			IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
			bool L_10 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_7, L_9, /*hidden argument*/NULL);
			if (!L_10)
			{
				goto IL_0039;
			}
		}

IL_0035:
		{
			V_2 = (bool)1;
			IL2CPP_LEAVE(0x54, FINALLY_0044);
		}

IL_0039:
		{
			bool L_11 = Enumerator_MoveNext_m168734AAA4869F1578DF4D4538B74BFF12522F68((Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7 *)(&V_0), /*hidden argument*/Enumerator_MoveNext_m168734AAA4869F1578DF4D4538B74BFF12522F68_RuntimeMethod_var);
			if (L_11)
			{
				goto IL_0017;
			}
		}

IL_0042:
		{
			IL2CPP_LEAVE(0x52, FINALLY_0044);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_mCDA3AD77115D33F3528CD546C85CA20239DD4294((Enumerator_t0B3353B842EAAEDC0D496EC51375E2783162A5B7 *)(&V_0), /*hidden argument*/Enumerator_Dispose_mCDA3AD77115D33F3528CD546C85CA20239DD4294_RuntimeMethod_var);
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x54, IL_0054)
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0052:
	{
		return (bool)0;
	}

IL_0054:
	{
		bool L_12 = V_2;
		return L_12;
	}
}
// System.Void Kolibri2d.Terrain2D::RandomizeDistortions()
extern "C" IL2CPP_METHOD_ATTR void Terrain2D_RandomizeDistortions_m4FD96A3931713907DF6D6D5B654DF90522D9322D (Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B * __this, const RuntimeMethod* method)
{
	{
		SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1 * L_0 = __this->get_creator_10();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1 * L_1 = __this->get_creator_10();
		NullCheck(L_1);
		SplineTerrain2DCreator_RandomizeDistortions_mA01B093693F10666A6AF040F50D8144565F51BC6(L_1, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.Void Kolibri2d.Terrain2D::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Terrain2D__ctor_mCF2695B04E60C4BA2D809A07720B7DDA69B1B55B (Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Terrain2D__ctor_mCF2695B04E60C4BA2D809A07720B7DDA69B1B55B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 * L_0 = (List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 *)il2cpp_codegen_object_new(List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4_il2cpp_TypeInfo_var);
		List_1__ctor_m9E8734104EA5EA636BBA9DD4E5E56DC6C7C1B063(L_0, /*hidden argument*/List_1__ctor_m9E8734104EA5EA636BBA9DD4E5E56DC6C7C1B063_RuntimeMethod_var);
		__this->set_materialHolders_9(L_0);
		__this->set_skirtOffsetMagnitude_11((5.0f));
		__this->set_skirtOffsetAngle_12((-90.0f));
		__this->set_skirtPosY_13((-10.0f));
		__this->set_skirtPosX_14((-10.0f));
		__this->set_skirtTopOffsetMagnitude_15((10.0f));
		__this->set_skirtBottomOffsetMagnitude_16((10.0f));
		__this->set_skirtLeftOffsetMagnitude_17((10.0f));
		__this->set_skirtRightOffsetMagnitude_18((10.0f));
		__this->set_meshSubdivision_21(1);
		SplineModifierWithCallbacks__ctor_m9C2181407A87C589515FDA4335D3FB71238C7827(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Kolibri2d.Terrain2D/Terrain2DMaterialHolder::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Terrain2DMaterialHolder__ctor_mD9ADF2A9EA239E77BCBD828F337A70412D6BB96B (Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F * __this, const RuntimeMethod* method)
{
	{
		__this->set_enabled_0((bool)1);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_0 = Color_get_white_mE7F3AC4FF0D6F35E48049C73116A222CBE96D905(/*hidden argument*/NULL);
		__this->set_tint_3(L_0);
		__this->set_createColliders_5((bool)1);
		Object__ctor_m8BA07445967EE2CC15961AD3C16F25DB74506EA0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Kolibri2d.Terrain2DArchitecture::Reset()
extern "C" IL2CPP_METHOD_ATTR void Terrain2DArchitecture_Reset_m9B8DFB988C031FDA3B267E832A1F38967045E90B (Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * __this, const RuntimeMethod* method)
{
	{
		Terrain2DArchitecture_Refresh_m46793DC53A31B24651E22531A2D201889CD13565(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Kolibri2d.Terrain2DArchitecture::RefreshIfMissingData()
extern "C" IL2CPP_METHOD_ATTR void Terrain2DArchitecture_RefreshIfMissingData_m7D08ADA747580A063BA739D33732CFB36F90A5F7 (Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Terrain2DArchitecture_RefreshIfMissingData_m7D08ADA747580A063BA739D33732CFB36F90A5F7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * L_0 = __this->get_spline_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0045;
		}
	}
	{
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_2 = __this->get_stepPointNormals_10();
		if (!L_2)
		{
			goto IL_0045;
		}
	}
	{
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_3 = __this->get_stepPointNormals_10();
		NullCheck(L_3);
		if (!(((RuntimeArray *)L_3)->max_length))
		{
			goto IL_0045;
		}
	}
	{
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_4 = __this->get_stepPointNormalsSmooth_11();
		if (!L_4)
		{
			goto IL_0045;
		}
	}
	{
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_5 = __this->get_stepPointNormalsSmooth_11();
		NullCheck(L_5);
		if (!(((RuntimeArray *)L_5)->max_length))
		{
			goto IL_0045;
		}
	}
	{
		List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * L_6 = __this->get_segmentsInfo_12();
		if (!L_6)
		{
			goto IL_0045;
		}
	}
	{
		List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * L_7 = __this->get_segmentsInfo_12();
		NullCheck(L_7);
		int32_t L_8 = List_1_get_Count_m1603A7C79B587156F949A0561C5CE4635B4AD255(L_7, /*hidden argument*/List_1_get_Count_m1603A7C79B587156F949A0561C5CE4635B4AD255_RuntimeMethod_var);
		if (L_8)
		{
			goto IL_004b;
		}
	}

IL_0045:
	{
		Terrain2DArchitecture_Refresh_m46793DC53A31B24651E22531A2D201889CD13565(__this, /*hidden argument*/NULL);
	}

IL_004b:
	{
		return;
	}
}
// System.Void Kolibri2d.Terrain2DArchitecture::Refresh()
extern "C" IL2CPP_METHOD_ATTR void Terrain2DArchitecture_Refresh_m46793DC53A31B24651E22531A2D201889CD13565 (Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Terrain2DArchitecture_Refresh_m46793DC53A31B24651E22531A2D201889CD13565_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * V_0 = NULL;
	Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * G_B2_0 = NULL;
	Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * G_B1_0 = NULL;
	Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * G_B3_0 = NULL;
	Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * G_B3_1 = NULL;
	{
		Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * L_0 = __this->get_overrideAngle_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_0, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (L_1)
		{
			G_B2_0 = __this;
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_il2cpp_TypeInfo_var);
		Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * L_2 = Terrain2DArchitectureAngles_get_DefaultAngle_m11AAFDCC6F2D57CED18469D56B6C678B9D75979F(/*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_001b;
	}

IL_0015:
	{
		Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * L_3 = __this->get_overrideAngle_8();
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_001b:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_currArchAngle_9(G_B3_0);
		BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * L_4 = Component_GetComponent_TisBezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_m997621D49F9FF6305B153B07112BDDE09A165DC1(__this, /*hidden argument*/Component_GetComponent_TisBezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_m997621D49F9FF6305B153B07112BDDE09A165DC1_RuntimeMethod_var);
		__this->set_spline_4(L_4);
		BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * L_5 = __this->get_spline_4();
		NullCheck(L_5);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_6 = BezierSpline_GetAllStepPoints_m5F33CE8AFBA4C9472E308BBCCCF6B1AFA2DEB495(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * L_7 = __this->get_spline_4();
		NullCheck(L_7);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_8 = BezierSpline_GetAllStepNormals_m3AEA137445942FDF65D21AA08CA28C48F61A43E2(L_7, /*hidden argument*/NULL);
		__this->set_stepPointNormals_10(L_8);
		BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * L_9 = __this->get_spline_4();
		NullCheck(L_9);
		bool L_10 = BezierSpline_get_Loop_m231A6FE13B839CEE5A0F98B8DAE26554FE981D03(L_9, /*hidden argument*/NULL);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_11 = Utils_GetNormalsFromPositions_m2D293F791AEB966D5D2530D1C05CD9416E668041((List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 **)(&V_0), L_10, 1, /*hidden argument*/NULL);
		__this->set_stepPointNormalsSmooth_11(L_11);
		Terrain2DArchitecture_CalculateSegmentsInfo_m7E9A4EA786517CBF0C45CDCEA214D0767751F460(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<Kolibri2d.SplineSegmentInfo> Kolibri2d.Terrain2DArchitecture::CalculateSegmentsInfo(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * Terrain2DArchitecture_CalculateSegmentsInfo_m7E9A4EA786517CBF0C45CDCEA214D0767751F460 (Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * __this, bool ___setManualFlip0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Terrain2DArchitecture_CalculateSegmentsInfo_m7E9A4EA786517CBF0C45CDCEA214D0767751F460_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * V_0 = NULL;
	int32_t V_1 = 0;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	bool V_5 = false;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	{
		bool L_0 = ___setManualFlip0;
		__this->set_manualFlip_7(L_0);
		List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * L_1 = (List_1_t517344AED92C69B6645F0ABFA470063D220BACCD *)il2cpp_codegen_object_new(List_1_t517344AED92C69B6645F0ABFA470063D220BACCD_il2cpp_TypeInfo_var);
		List_1__ctor_m8E52D55D4C56236ED57F6275B7E7EDD692F55961(L_1, /*hidden argument*/List_1__ctor_m8E52D55D4C56236ED57F6275B7E7EDD692F55961_RuntimeMethod_var);
		__this->set_segmentsInfo_12(L_1);
		BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * L_2 = __this->get_spline_4();
		NullCheck(L_2);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_3 = BezierSpline_GetAllStepPoints_m5F33CE8AFBA4C9472E308BBCCCF6B1AFA2DEB495(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3(L_4, /*hidden argument*/List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_RuntimeMethod_var);
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_6 = V_0;
		NullCheck(L_6);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_6, 0, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_8 = V_0;
		NullCheck(L_8);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_8, 1, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = Utils_GetDirFromPoints_mE9D1FC9BF146E38D7A53FD21E2A8C34F4B08F16B(L_7, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = V_2;
		float L_12 = Utils_GetAngleFromDir_m62A41F885D74A5155D0E4684783672982D7CB8B3(L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		V_4 = (0.0f);
		int32_t L_13 = __this->get_aspect_5();
		V_5 = (bool)((((int32_t)L_13) == ((int32_t)2))? 1 : 0);
		Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * L_14 = __this->get_currArchAngle_9();
		float L_15 = V_3;
		BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * L_16 = __this->get_spline_4();
		NullCheck(L_16);
		bool L_17 = L_16->get_isClockwise_11();
		bool L_18 = V_5;
		bool L_19 = __this->get_manualFlip_7();
		NullCheck(L_14);
		int32_t L_20 = Terrain2DArchitectureAngles_GetTypeFromAngle_m4A53062F36D10357353B71776B6421475C32B19C(L_14, L_15, L_17, L_18, L_19, /*hidden argument*/NULL);
		V_6 = L_20;
		V_7 = 0;
		V_8 = 1;
		goto IL_013e;
	}

IL_0080:
	{
		int32_t L_21 = V_8;
		int32_t L_22 = V_1;
		if ((!(((uint32_t)L_21) == ((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_22, (int32_t)2))))))
		{
			goto IL_00a6;
		}
	}
	{
		List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * L_23 = __this->get_segmentsInfo_12();
		int32_t L_24 = V_7;
		int32_t L_25 = V_8;
		float L_26 = V_3;
		float L_27 = V_4;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_28 = V_2;
		int32_t L_29 = V_6;
		SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_30;
		memset(&L_30, 0, sizeof(L_30));
		SplineSegmentInfo__ctor_m73B0DFDDE6E8AC741DA121FBA64BF1E513E3C8C5((&L_30), L_24, L_25, L_26, L_27, L_28, L_29, /*hidden argument*/NULL);
		NullCheck(L_23);
		List_1_Add_m649A438448972F32C1D67F10BBFB80D0CF81EF94(L_23, L_30, /*hidden argument*/List_1_Add_m649A438448972F32C1D67F10BBFB80D0CF81EF94_RuntimeMethod_var);
		goto IL_0148;
	}

IL_00a6:
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_31 = V_0;
		int32_t L_32 = V_8;
		NullCheck(L_31);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_33 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_31, L_32, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_34 = V_0;
		int32_t L_35 = V_8;
		NullCheck(L_34);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_36 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_34, ((int32_t)il2cpp_codegen_add((int32_t)L_35, (int32_t)1)), /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_37 = Utils_GetDirFromPoints_mE9D1FC9BF146E38D7A53FD21E2A8C34F4B08F16B(L_33, L_36, /*hidden argument*/NULL);
		V_2 = L_37;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_38 = V_2;
		float L_39 = Utils_GetAngleFromDir_m62A41F885D74A5155D0E4684783672982D7CB8B3(L_38, /*hidden argument*/NULL);
		V_3 = L_39;
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_40 = V_0;
		int32_t L_41 = V_8;
		NullCheck(L_40);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_42 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_40, ((int32_t)il2cpp_codegen_subtract((int32_t)L_41, (int32_t)1)), /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_43 = V_0;
		int32_t L_44 = V_8;
		NullCheck(L_43);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_45 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_43, L_44, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_46 = V_0;
		int32_t L_47 = V_8;
		NullCheck(L_46);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_48 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_46, ((int32_t)il2cpp_codegen_add((int32_t)L_47, (int32_t)1)), /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		float L_49 = Utils_GetAngleABC_mE43F92353A13E1D3011FDE80954F05B69EC60BDC(L_42, L_45, L_48, /*hidden argument*/NULL);
		V_4 = L_49;
		Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * L_50 = __this->get_currArchAngle_9();
		float L_51 = V_3;
		BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * L_52 = __this->get_spline_4();
		NullCheck(L_52);
		bool L_53 = L_52->get_isClockwise_11();
		int32_t L_54 = __this->get_aspect_5();
		bool L_55 = __this->get_manualFlip_7();
		NullCheck(L_50);
		int32_t L_56 = Terrain2DArchitectureAngles_GetTypeFromAngle_m4A53062F36D10357353B71776B6421475C32B19C(L_50, L_51, L_53, (bool)((((int32_t)L_54) == ((int32_t)2))? 1 : 0), L_55, /*hidden argument*/NULL);
		V_9 = L_56;
		int32_t L_57 = V_6;
		int32_t L_58 = V_9;
		if ((((int32_t)L_57) == ((int32_t)L_58)))
		{
			goto IL_0138;
		}
	}
	{
		List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * L_59 = __this->get_segmentsInfo_12();
		int32_t L_60 = V_7;
		int32_t L_61 = V_8;
		float L_62 = V_3;
		float L_63 = V_4;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_64 = V_2;
		int32_t L_65 = V_6;
		SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_66;
		memset(&L_66, 0, sizeof(L_66));
		SplineSegmentInfo__ctor_m73B0DFDDE6E8AC741DA121FBA64BF1E513E3C8C5((&L_66), L_60, L_61, L_62, L_63, L_64, L_65, /*hidden argument*/NULL);
		NullCheck(L_59);
		List_1_Add_m649A438448972F32C1D67F10BBFB80D0CF81EF94(L_59, L_66, /*hidden argument*/List_1_Add_m649A438448972F32C1D67F10BBFB80D0CF81EF94_RuntimeMethod_var);
		int32_t L_67 = V_9;
		V_6 = L_67;
		int32_t L_68 = V_8;
		V_7 = L_68;
	}

IL_0138:
	{
		int32_t L_69 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_69, (int32_t)1));
	}

IL_013e:
	{
		int32_t L_70 = V_8;
		int32_t L_71 = V_1;
		if ((((int32_t)L_70) < ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_71, (int32_t)1)))))
		{
			goto IL_0080;
		}
	}

IL_0148:
	{
		bool L_72 = __this->get_manualFlip_7();
		if (L_72)
		{
			goto IL_015e;
		}
	}
	{
		int32_t L_73 = __this->get_aspect_5();
		if (L_73)
		{
			goto IL_015e;
		}
	}
	{
		Terrain2DArchitecture_ApplySegmentGuessOnLine_m3B0094A0015B70D9CB31C6AEE77CE6F6C3350B9E(__this, /*hidden argument*/NULL);
	}

IL_015e:
	{
		List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * L_74 = __this->get_segmentsInfo_12();
		return L_74;
	}
}
// System.Collections.Generic.List`1<Kolibri2d.SplineSegmentInfo> Kolibri2d.Terrain2DArchitecture::GetSegmentsInfoAndMergeEnds()
extern "C" IL2CPP_METHOD_ATTR List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * Terrain2DArchitecture_GetSegmentsInfoAndMergeEnds_mA4D4C7532720F4EFCA6C9C1B9885A02FADB51522 (Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Terrain2DArchitecture_GetSegmentsInfoAndMergeEnds_mA4D4C7532720F4EFCA6C9C1B9885A02FADB51522_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * V_0 = NULL;
	SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  V_1;
	memset(&V_1, 0, sizeof(V_1));
	SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB  V_3;
	memset(&V_3, 0, sizeof(V_3));
	SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * L_0 = (List_1_t517344AED92C69B6645F0ABFA470063D220BACCD *)il2cpp_codegen_object_new(List_1_t517344AED92C69B6645F0ABFA470063D220BACCD_il2cpp_TypeInfo_var);
		List_1__ctor_m8E52D55D4C56236ED57F6275B7E7EDD692F55961(L_0, /*hidden argument*/List_1__ctor_m8E52D55D4C56236ED57F6275B7E7EDD692F55961_RuntimeMethod_var);
		V_0 = L_0;
		List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * L_1 = __this->get_segmentsInfo_12();
		NullCheck(L_1);
		Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB  L_2 = List_1_GetEnumerator_m9536E635B50A2D8549D7615CC042AF1D64AEF454(L_1, /*hidden argument*/List_1_GetEnumerator_m9536E635B50A2D8549D7615CC042AF1D64AEF454_RuntimeMethod_var);
		V_3 = L_2;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0052;
		}

IL_0014:
		{
			SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_3 = Enumerator_get_Current_m495068DBBE452DB06B67916B5D28B026BD336204((Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB *)(&V_3), /*hidden argument*/Enumerator_get_Current_m495068DBBE452DB06B67916B5D28B026BD336204_RuntimeMethod_var);
			V_4 = L_3;
			List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * L_4 = V_0;
			SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_5 = V_4;
			int32_t L_6 = L_5.get_begin_0();
			SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_7 = V_4;
			int32_t L_8 = L_7.get_end_1();
			SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_9 = V_4;
			float L_10 = L_9.get_angleBegin_2();
			SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_11 = V_4;
			float L_12 = L_11.get_angleCorner_3();
			SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_13 = V_4;
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_14 = L_13.get_dirBegin_4();
			SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_15 = V_4;
			int32_t L_16 = L_15.get_type_5();
			SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_17;
			memset(&L_17, 0, sizeof(L_17));
			SplineSegmentInfo__ctor_m73B0DFDDE6E8AC741DA121FBA64BF1E513E3C8C5((&L_17), L_6, L_8, L_10, L_12, L_14, L_16, /*hidden argument*/NULL);
			NullCheck(L_4);
			List_1_Add_m649A438448972F32C1D67F10BBFB80D0CF81EF94(L_4, L_17, /*hidden argument*/List_1_Add_m649A438448972F32C1D67F10BBFB80D0CF81EF94_RuntimeMethod_var);
		}

IL_0052:
		{
			bool L_18 = Enumerator_MoveNext_m3D433FA2C3588249702F14524E89E464EE14EE13((Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB *)(&V_3), /*hidden argument*/Enumerator_MoveNext_m3D433FA2C3588249702F14524E89E464EE14EE13_RuntimeMethod_var);
			if (L_18)
			{
				goto IL_0014;
			}
		}

IL_005b:
		{
			IL2CPP_LEAVE(0x6B, FINALLY_005d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_005d;
	}

FINALLY_005d:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_mCB51DC072567AD26ACE9160F23683B7D32B7190B((Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB *)(&V_3), /*hidden argument*/Enumerator_Dispose_mCB51DC072567AD26ACE9160F23683B7D32B7190B_RuntimeMethod_var);
		IL2CPP_END_FINALLY(93)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(93)
	{
		IL2CPP_JUMP_TBL(0x6B, IL_006b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_006b:
	{
		List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * L_19 = __this->get_segmentsInfo_12();
		NullCheck(L_19);
		SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_20 = List_1_get_Item_mA31EF7A05D93A92566AE15D88053820A2E85F1AF(L_19, 0, /*hidden argument*/List_1_get_Item_mA31EF7A05D93A92566AE15D88053820A2E85F1AF_RuntimeMethod_var);
		V_1 = L_20;
		List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * L_21 = __this->get_segmentsInfo_12();
		List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * L_22 = __this->get_segmentsInfo_12();
		NullCheck(L_22);
		int32_t L_23 = List_1_get_Count_m1603A7C79B587156F949A0561C5CE4635B4AD255(L_22, /*hidden argument*/List_1_get_Count_m1603A7C79B587156F949A0561C5CE4635B4AD255_RuntimeMethod_var);
		NullCheck(L_21);
		SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_24 = List_1_get_Item_mA31EF7A05D93A92566AE15D88053820A2E85F1AF(L_21, ((int32_t)il2cpp_codegen_subtract((int32_t)L_23, (int32_t)1)), /*hidden argument*/List_1_get_Item_mA31EF7A05D93A92566AE15D88053820A2E85F1AF_RuntimeMethod_var);
		V_2 = L_24;
		BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * L_25 = __this->get_spline_4();
		NullCheck(L_25);
		bool L_26 = BezierSpline_get_Loop_m231A6FE13B839CEE5A0F98B8DAE26554FE981D03(L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_0129;
		}
	}
	{
		List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * L_27 = __this->get_segmentsInfo_12();
		NullCheck(L_27);
		int32_t L_28 = List_1_get_Count_m1603A7C79B587156F949A0561C5CE4635B4AD255(L_27, /*hidden argument*/List_1_get_Count_m1603A7C79B587156F949A0561C5CE4635B4AD255_RuntimeMethod_var);
		if ((((int32_t)L_28) < ((int32_t)2)))
		{
			goto IL_0129;
		}
	}
	{
		SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_29 = V_1;
		int32_t L_30 = L_29.get_begin_0();
		if (L_30)
		{
			goto IL_0129;
		}
	}
	{
		SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_31 = V_2;
		int32_t L_32 = L_31.get_end_1();
		BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * L_33 = __this->get_spline_4();
		NullCheck(L_33);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_34 = BezierSpline_GetAllStepPoints_m5F33CE8AFBA4C9472E308BBCCCF6B1AFA2DEB495(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		int32_t L_35 = List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3(L_34, /*hidden argument*/List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_RuntimeMethod_var);
		if ((!(((uint32_t)L_32) == ((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_35, (int32_t)1))))))
		{
			goto IL_0129;
		}
	}
	{
		SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_36 = V_1;
		int32_t L_37 = L_36.get_type_5();
		SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_38 = V_2;
		int32_t L_39 = L_38.get_type_5();
		if ((!(((uint32_t)L_37) == ((uint32_t)L_39))))
		{
			goto IL_0129;
		}
	}
	{
		List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * L_40 = V_0;
		List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * L_41 = __this->get_segmentsInfo_12();
		NullCheck(L_41);
		int32_t L_42 = List_1_get_Count_m1603A7C79B587156F949A0561C5CE4635B4AD255(L_41, /*hidden argument*/List_1_get_Count_m1603A7C79B587156F949A0561C5CE4635B4AD255_RuntimeMethod_var);
		SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_43 = V_2;
		int32_t L_44 = L_43.get_begin_0();
		SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_45 = V_2;
		int32_t L_46 = L_45.get_end_1();
		SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_47 = V_1;
		int32_t L_48 = L_47.get_end_1();
		SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_49 = V_2;
		float L_50 = L_49.get_angleBegin_2();
		SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_51 = V_2;
		float L_52 = L_51.get_angleCorner_3();
		SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_53 = V_2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_54 = L_53.get_dirBegin_4();
		SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_55 = V_2;
		int32_t L_56 = L_55.get_type_5();
		SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_57;
		memset(&L_57, 0, sizeof(L_57));
		SplineSegmentInfo__ctor_m73B0DFDDE6E8AC741DA121FBA64BF1E513E3C8C5((&L_57), L_44, ((int32_t)il2cpp_codegen_add((int32_t)L_46, (int32_t)L_48)), L_50, L_52, L_54, L_56, /*hidden argument*/NULL);
		NullCheck(L_40);
		List_1_set_Item_mCAC53DD791D8E571F6A22EE17D615DD70B9C6619(L_40, ((int32_t)il2cpp_codegen_subtract((int32_t)L_42, (int32_t)1)), L_57, /*hidden argument*/List_1_set_Item_mCAC53DD791D8E571F6A22EE17D615DD70B9C6619_RuntimeMethod_var);
		List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * L_58 = V_0;
		NullCheck(L_58);
		List_1_RemoveAt_mDDB8509DB8FA2CC2FB3F8A9A13C8028CD8185109(L_58, 0, /*hidden argument*/List_1_RemoveAt_mDDB8509DB8FA2CC2FB3F8A9A13C8028CD8185109_RuntimeMethod_var);
	}

IL_0129:
	{
		List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * L_59 = V_0;
		return L_59;
	}
}
// Kolibri2d.TerrainType Kolibri2d.Terrain2DArchitecture::GetTerrainTypeFromAngle(System.Single)
extern "C" IL2CPP_METHOD_ATTR int32_t Terrain2DArchitecture_GetTerrainTypeFromAngle_mFA154972E6CF8007BBA51B5EEA2BA7D89AA37F2B (Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * __this, float ___degree0, const RuntimeMethod* method)
{
	{
		Terrain2DArchitecture_RefreshIfMissingData_m7D08ADA747580A063BA739D33732CFB36F90A5F7(__this, /*hidden argument*/NULL);
		Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * L_0 = __this->get_currArchAngle_9();
		float L_1 = ___degree0;
		BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * L_2 = __this->get_spline_4();
		NullCheck(L_2);
		bool L_3 = L_2->get_isClockwise_11();
		int32_t L_4 = __this->get_aspect_5();
		bool L_5 = __this->get_manualFlip_7();
		NullCheck(L_0);
		int32_t L_6 = Terrain2DArchitectureAngles_GetTypeFromAngle_m4A53062F36D10357353B71776B6421475C32B19C(L_0, L_1, L_3, (bool)((((int32_t)L_4) == ((int32_t)2))? 1 : 0), L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// Kolibri2d.TerrainType Kolibri2d.Terrain2DArchitecture::GetTerrainTypeFromSplineLength01(System.Single)
extern "C" IL2CPP_METHOD_ATTR int32_t Terrain2DArchitecture_GetTerrainTypeFromSplineLength01_m21F1F20D19B6D1A2D9F55CCFB147671FF099731D (Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * __this, float ___length0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Terrain2DArchitecture_GetTerrainTypeFromSplineLength01_m21F1F20D19B6D1A2D9F55CCFB147671FF099731D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Terrain2DArchitecture_RefreshIfMissingData_m7D08ADA747580A063BA739D33732CFB36F90A5F7(__this, /*hidden argument*/NULL);
		float L_0 = ___length0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp_m033DD894F89E6DCCDAFC580091053059C86A4507(L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		___length0 = L_1;
		BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * L_2 = __this->get_spline_4();
		float L_3 = ___length0;
		NullCheck(L_2);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = BezierSpline_GetDirectionFromLength01_m2AD48C7B60722B8A4A49117EE84B7A6264F4DC28(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * L_5 = __this->get_currArchAngle_9();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = V_0;
		float L_7 = L_6.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = V_0;
		float L_9 = L_8.get_y_3();
		float L_10 = atan2f(L_7, L_9);
		BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * L_11 = __this->get_spline_4();
		NullCheck(L_11);
		bool L_12 = L_11->get_isClockwise_11();
		int32_t L_13 = __this->get_aspect_5();
		bool L_14 = __this->get_manualFlip_7();
		NullCheck(L_5);
		int32_t L_15 = Terrain2DArchitectureAngles_GetTypeFromAngle_m4A53062F36D10357353B71776B6421475C32B19C(L_5, ((float)il2cpp_codegen_multiply((float)L_10, (float)(57.29578f))), L_12, (bool)((((int32_t)L_13) == ((int32_t)2))? 1 : 0), L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
// UnityEngine.Vector3 Kolibri2d.Terrain2DArchitecture::GetDirectionFromLength01(System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Terrain2DArchitecture_GetDirectionFromLength01_m16B0CF93290E47DEBFFE522DBF4C05C057B471C4 (Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * __this, float ___len010, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Terrain2DArchitecture_GetDirectionFromLength01_m16B0CF93290E47DEBFFE522DBF4C05C057B471C4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * L_0 = __this->get_spline_4();
		float L_1 = ___len010;
		NullCheck(L_0);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = BezierSpline_GetDirectionFromLength01_m2AD48C7B60722B8A4A49117EE84B7A6264F4DC28(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * L_3 = __this->get_spline_4();
		NullCheck(L_3);
		bool L_4 = BezierSpline_get_Loop_m231A6FE13B839CEE5A0F98B8DAE26554FE981D03(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0048;
		}
	}
	{
		BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * L_5 = __this->get_spline_4();
		NullCheck(L_5);
		bool L_6 = L_5->get_isClockwise_11();
		int32_t L_7 = __this->get_aspect_5();
		if ((!(((uint32_t)L_6) == ((uint32_t)((((int32_t)L_7) == ((int32_t)2))? 1 : 0)))))
		{
			goto IL_0037;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = Vector3_op_UnaryNegation_m2AFBBF22801F9BCA5A4EBE642A29F433FE1339C2(L_8, /*hidden argument*/NULL);
		V_0 = L_9;
	}

IL_0037:
	{
		bool L_10 = __this->get_manualFlip_7();
		if (!L_10)
		{
			goto IL_0074;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = Vector3_op_UnaryNegation_m2AFBBF22801F9BCA5A4EBE642A29F433FE1339C2(L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0074;
	}

IL_0048:
	{
		BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * L_13 = __this->get_spline_4();
		NullCheck(L_13);
		bool L_14 = L_13->get_isClockwise_11();
		int32_t L_15 = __this->get_aspect_5();
		if ((!(((uint32_t)L_14) == ((uint32_t)((((int32_t)L_15) == ((int32_t)2))? 1 : 0)))))
		{
			goto IL_0065;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_17 = Vector3_op_UnaryNegation_m2AFBBF22801F9BCA5A4EBE642A29F433FE1339C2(L_16, /*hidden argument*/NULL);
		V_0 = L_17;
	}

IL_0065:
	{
		bool L_18 = __this->get_manualFlip_7();
		if (!L_18)
		{
			goto IL_0074;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_19 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_20 = Vector3_op_UnaryNegation_m2AFBBF22801F9BCA5A4EBE642A29F433FE1339C2(L_19, /*hidden argument*/NULL);
		V_0 = L_20;
	}

IL_0074:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_21 = V_0;
		return L_21;
	}
}
// System.Void Kolibri2d.Terrain2DArchitecture::ApplySegmentGuessOnLine()
extern "C" IL2CPP_METHOD_ATTR void Terrain2DArchitecture_ApplySegmentGuessOnLine_m3B0094A0015B70D9CB31C6AEE77CE6F6C3350B9E (Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Terrain2DArchitecture_ApplySegmentGuessOnLine_m3B0094A0015B70D9CB31C6AEE77CE6F6C3350B9E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB  V_2;
	memset(&V_2, 0, sizeof(V_2));
	SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * L_0 = __this->get_spline_4();
		NullCheck(L_0);
		bool L_1 = BezierSpline_get_Loop_m231A6FE13B839CEE5A0F98B8DAE26554FE981D03(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0067;
		}
	}
	{
		V_0 = 0;
		V_1 = 0;
		List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * L_2 = __this->get_segmentsInfo_12();
		NullCheck(L_2);
		Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB  L_3 = List_1_GetEnumerator_m9536E635B50A2D8549D7615CC042AF1D64AEF454(L_2, /*hidden argument*/List_1_GetEnumerator_m9536E635B50A2D8549D7615CC042AF1D64AEF454_RuntimeMethod_var);
		V_2 = L_3;
	}

IL_001d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0042;
		}

IL_001f:
		{
			SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_4 = Enumerator_get_Current_m495068DBBE452DB06B67916B5D28B026BD336204((Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB *)(&V_2), /*hidden argument*/Enumerator_get_Current_m495068DBBE452DB06B67916B5D28B026BD336204_RuntimeMethod_var);
			V_3 = L_4;
			SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_5 = V_3;
			int32_t L_6 = L_5.get_type_5();
			if (L_6)
			{
				goto IL_0035;
			}
		}

IL_002f:
		{
			int32_t L_7 = V_0;
			V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
			goto IL_0042;
		}

IL_0035:
		{
			SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066  L_8 = V_3;
			int32_t L_9 = L_8.get_type_5();
			if ((!(((uint32_t)L_9) == ((uint32_t)2))))
			{
				goto IL_0042;
			}
		}

IL_003e:
		{
			int32_t L_10 = V_1;
			V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
		}

IL_0042:
		{
			bool L_11 = Enumerator_MoveNext_m3D433FA2C3588249702F14524E89E464EE14EE13((Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB *)(&V_2), /*hidden argument*/Enumerator_MoveNext_m3D433FA2C3588249702F14524E89E464EE14EE13_RuntimeMethod_var);
			if (L_11)
			{
				goto IL_001f;
			}
		}

IL_004b:
		{
			IL2CPP_LEAVE(0x5B, FINALLY_004d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_004d;
	}

FINALLY_004d:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_mCB51DC072567AD26ACE9160F23683B7D32B7190B((Enumerator_tDCC546968169F89B6BDD256ADFD2F97686EFF4FB *)(&V_2), /*hidden argument*/Enumerator_Dispose_mCB51DC072567AD26ACE9160F23683B7D32B7190B_RuntimeMethod_var);
		IL2CPP_END_FINALLY(77)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(77)
	{
		IL2CPP_JUMP_TBL(0x5B, IL_005b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_005b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) <= ((int32_t)L_13)))
		{
			goto IL_0067;
		}
	}
	{
		Terrain2DArchitecture_CalculateSegmentsInfo_m7E9A4EA786517CBF0C45CDCEA214D0767751F460(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_0067:
	{
		return;
	}
}
// System.Void Kolibri2d.Terrain2DArchitecture::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Terrain2DArchitecture__ctor_m4151CE5C2A2172A142614E734327EEDB1247CD3F (Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Terrain2DArchitecture__ctor_m4151CE5C2A2172A142614E734327EEDB1247CD3F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_exteriorMargin_6((10.0f));
		List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * L_0 = (List_1_t517344AED92C69B6645F0ABFA470063D220BACCD *)il2cpp_codegen_object_new(List_1_t517344AED92C69B6645F0ABFA470063D220BACCD_il2cpp_TypeInfo_var);
		List_1__ctor_m8E52D55D4C56236ED57F6275B7E7EDD692F55961(L_0, /*hidden argument*/List_1__ctor_m8E52D55D4C56236ED57F6275B7E7EDD692F55961_RuntimeMethod_var);
		__this->set_segmentsInfo_12(L_0);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Kolibri2d.Terrain2DArchitectureAngles Kolibri2d.Terrain2DArchitectureAngles::get_DefaultAngle()
extern "C" IL2CPP_METHOD_ATTR Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * Terrain2DArchitectureAngles_get_DefaultAngle_m11AAFDCC6F2D57CED18469D56B6C678B9D75979F (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Terrain2DArchitectureAngles_get_DefaultAngle_m11AAFDCC6F2D57CED18469D56B6C678B9D75979F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_il2cpp_TypeInfo_var);
		Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * L_0 = ((Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_StaticFields*)il2cpp_codegen_static_fields_for(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_il2cpp_TypeInfo_var))->get__instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * L_2 = ScriptableObject_CreateInstance_TisTerrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_m07B85156417883C4AB46AA7039D2CF9CEC71B80F(/*hidden argument*/ScriptableObject_CreateInstance_TisTerrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_m07B85156417883C4AB46AA7039D2CF9CEC71B80F_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_il2cpp_TypeInfo_var);
		((Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_StaticFields*)il2cpp_codegen_static_fields_for(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_il2cpp_TypeInfo_var))->set__instance_4(L_2);
		Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * L_3 = ((Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_StaticFields*)il2cpp_codegen_static_fields_for(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_il2cpp_TypeInfo_var))->get__instance_4();
		NullCheck(L_3);
		Terrain2DArchitectureAngles_ResetValues_mAF413B8C39C39522A62BB55D7A9D5CD5EB9FAAE3(L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_il2cpp_TypeInfo_var);
		Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * L_4 = ((Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_StaticFields*)il2cpp_codegen_static_fields_for(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_il2cpp_TypeInfo_var))->get__instance_4();
		return L_4;
	}
}
// Kolibri2d.TerrainType Kolibri2d.Terrain2DArchitectureAngles::GetReplacedType(Kolibri2d.TerrainType)
extern "C" IL2CPP_METHOD_ATTR int32_t Terrain2DArchitectureAngles_GetReplacedType_mB72A1062389E0C760E72DFED807ACE704DB54862 (Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * __this, int32_t ___t0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___t0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0014;
			}
			case 1:
			{
				goto IL_001b;
			}
			case 2:
			{
				goto IL_0022;
			}
		}
	}
	{
		goto IL_0029;
	}

IL_0014:
	{
		int32_t L_1 = __this->get_replaceGroundWith_8();
		return L_1;
	}

IL_001b:
	{
		int32_t L_2 = __this->get_replaceWallsWith_9();
		return L_2;
	}

IL_0022:
	{
		int32_t L_3 = __this->get_replaceCeilingWith_10();
		return L_3;
	}

IL_0029:
	{
		return (int32_t)(4);
	}
}
// System.Void Kolibri2d.Terrain2DArchitectureAngles::ResetValues()
extern "C" IL2CPP_METHOD_ATTR void Terrain2DArchitectureAngles_ResetValues_mAF413B8C39C39522A62BB55D7A9D5CD5EB9FAAE3 (Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * __this, const RuntimeMethod* method)
{
	{
		__this->set_maxGroundAngle_5((65.0f));
		__this->set_maxCeilingAngle_6((46.0f));
		__this->set_replaceGroundWith_8(0);
		__this->set_replaceWallsWith_9(1);
		__this->set_replaceCeilingWith_10(2);
		return;
	}
}
// System.Void Kolibri2d.Terrain2DArchitectureAngles::.ctor(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Terrain2DArchitectureAngles__ctor_mA743674F17D83BA6274198FCC425ED5DF7779E03 (Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * __this, float ___maxGroundAngle0, float ___maxCeilingAngle1, const RuntimeMethod* method)
{
	{
		__this->set_maxGroundAngle_5((65.0f));
		__this->set_maxCeilingAngle_6((46.0f));
		__this->set_replaceWallsWith_9(1);
		__this->set_replaceCeilingWith_10(2);
		ScriptableObject__ctor_m6E2B3821A4A361556FC12E9B1C71E1D5DC002C5B(__this, /*hidden argument*/NULL);
		float L_0 = ___maxGroundAngle0;
		__this->set_maxCeilingAngle_6(L_0);
		float L_1 = ___maxCeilingAngle1;
		__this->set_maxCeilingAngle_6(L_1);
		return;
	}
}
// Kolibri2d.TerrainType Kolibri2d.Terrain2DArchitectureAngles::GetTypeFromAngle(System.Single,System.Boolean,System.Boolean,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR int32_t Terrain2DArchitectureAngles_GetTypeFromAngle_m4A53062F36D10357353B71776B6421475C32B19C (Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * __this, float ___degree0, bool ___isClockwise1, bool ___inverted2, bool ___manualFlip3, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		bool L_0 = ___isClockwise1;
		bool L_1 = ___inverted2;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0008;
		}
	}
	{
		float L_2 = ___degree0;
		___degree0 = ((-L_2));
	}

IL_0008:
	{
		bool L_3 = ___manualFlip3;
		if (!L_3)
		{
			goto IL_0010;
		}
	}
	{
		float L_4 = ___degree0;
		___degree0 = ((-L_4));
	}

IL_0010:
	{
		float L_5 = ___degree0;
		if ((!(((float)L_5) > ((float)(0.0f)))))
		{
			goto IL_003b;
		}
	}
	{
		float L_6 = __this->get_maxGroundAngle_5();
		V_1 = ((float)il2cpp_codegen_subtract((float)(90.0f), (float)L_6));
		float L_7 = ___degree0;
		float L_8 = V_1;
		if ((((float)L_7) < ((float)L_8)))
		{
			goto IL_0033;
		}
	}
	{
		float L_9 = ___degree0;
		float L_10 = V_1;
		if ((!(((float)L_9) > ((float)((float)il2cpp_codegen_subtract((float)(180.0f), (float)L_10))))))
		{
			goto IL_0037;
		}
	}

IL_0033:
	{
		V_0 = 1;
		goto IL_005e;
	}

IL_0037:
	{
		V_0 = 0;
		goto IL_005e;
	}

IL_003b:
	{
		float L_11 = __this->get_maxCeilingAngle_6();
		V_2 = ((float)il2cpp_codegen_subtract((float)(90.0f), (float)L_11));
		float L_12 = ___degree0;
		float L_13 = V_2;
		if ((((float)L_12) > ((float)((-L_13)))))
		{
			goto IL_0058;
		}
	}
	{
		float L_14 = ___degree0;
		float L_15 = V_2;
		if ((!(((float)L_14) < ((float)((-((float)il2cpp_codegen_subtract((float)(180.0f), (float)L_15))))))))
		{
			goto IL_005c;
		}
	}

IL_0058:
	{
		V_0 = 1;
		goto IL_005e;
	}

IL_005c:
	{
		V_0 = 2;
	}

IL_005e:
	{
		bool L_16 = __this->get_replaceSegments_7();
		if (L_16)
		{
			goto IL_0068;
		}
	}
	{
		int32_t L_17 = V_0;
		return L_17;
	}

IL_0068:
	{
		int32_t L_18 = V_0;
		int32_t L_19 = Terrain2DArchitectureAngles_GetReplacedType_mB72A1062389E0C760E72DFED807ACE704DB54862(__this, L_18, /*hidden argument*/NULL);
		return L_19;
	}
}
// System.Void Kolibri2d.Terrain2DArchitectureAngles::SaveCurrentAsDefaultAngle()
extern "C" IL2CPP_METHOD_ATTR void Terrain2DArchitectureAngles_SaveCurrentAsDefaultAngle_m078AEFEDAA28049E4A2EFC41C1B83B263418B22E (Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Terrain2DArchitectureAngles_SaveCurrentAsDefaultAngle_m078AEFEDAA28049E4A2EFC41C1B83B263418B22E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_il2cpp_TypeInfo_var);
		((Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_StaticFields*)il2cpp_codegen_static_fields_for(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_il2cpp_TypeInfo_var))->set__instance_4(__this);
		return;
	}
}
// System.Void Kolibri2d.Terrain2DArchitectureAngles::.cctor()
extern "C" IL2CPP_METHOD_ATTR void Terrain2DArchitectureAngles__cctor_mD2E0FF8C0D9C997E4F4344146C095A5D905B7FE2 (const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Kolibri2d.Terrain2DMaterial::Reset()
extern "C" IL2CPP_METHOD_ATTR void Terrain2DMaterial_Reset_mF7BF480A2BA84A453C98479BA9DA5BDDC26D1458 (Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * __this, const RuntimeMethod* method)
{
	{
		Terrain2DMaterial_Refresh_mD6A2F2D44B21F6CC68F65957C50A71065DE64AFC(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Kolibri2d.Terrain2DMaterial::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Terrain2DMaterial__ctor_mDAEBAA185FC1090745CB30C7A3857C5E64C66AA8 (Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Terrain2DMaterial__ctor_mDAEBAA185FC1090745CB30C7A3857C5E64C66AA8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_scale_4((1.0f));
		Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A * L_0 = (Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A *)il2cpp_codegen_object_new(Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m9266E02BD88994992C4A72CDE2733990EEE6C9B8(L_0, /*hidden argument*/Dictionary_2__ctor_m9266E02BD88994992C4A72CDE2733990EEE6C9B8_RuntimeMethod_var);
		__this->set_textureInfoByType_6(L_0);
		MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  L_1;
		memset(&L_1, 0, sizeof(L_1));
		MaterialTextureInfo__ctor_m5665381247ECA422AB1FAB1EDBC5A883DFA0D20C((&L_1), (bool)1, /*hidden argument*/NULL);
		__this->set_infoGround_7(L_1);
		MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  L_2;
		memset(&L_2, 0, sizeof(L_2));
		MaterialTextureInfo__ctor_m5665381247ECA422AB1FAB1EDBC5A883DFA0D20C((&L_2), (bool)1, /*hidden argument*/NULL);
		__this->set_infoWall_8(L_2);
		MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  L_3;
		memset(&L_3, 0, sizeof(L_3));
		MaterialTextureInfo__ctor_m5665381247ECA422AB1FAB1EDBC5A883DFA0D20C((&L_3), (bool)1, /*hidden argument*/NULL);
		__this->set_infoCeiling_9(L_3);
		MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  L_4;
		memset(&L_4, 0, sizeof(L_4));
		MaterialTextureInfo__ctor_m5665381247ECA422AB1FAB1EDBC5A883DFA0D20C((&L_4), (bool)1, /*hidden argument*/NULL);
		__this->set_infoFill_10(L_4);
		__this->set_useCorners_12((bool)1);
		CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703 * L_5 = (CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703 *)il2cpp_codegen_object_new(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703_il2cpp_TypeInfo_var);
		CornerSprites2__ctor_m1AF5E76E4493B22EC500394DF591D669EBAA1458(L_5, /*hidden argument*/NULL);
		__this->set_cornerSprites_14(L_5);
		SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE * L_6 = (SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE *)il2cpp_codegen_object_new(SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE_il2cpp_TypeInfo_var);
		SortingLayerOptions2__ctor_m5CE73EC2FC70B971D15CE81AC4C4EDC3EA4C49DC(L_6, /*hidden argument*/NULL);
		__this->set_layersAndSorting_15(L_6);
		__this->set_splitPaths_16((bool)1);
		SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F * L_7 = (SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F *)il2cpp_codegen_object_new(SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F_il2cpp_TypeInfo_var);
		SortingZ_3D_Options__ctor_m9944ABAD4B79DEC1A85324A1CA2F9FCE738CBA14(L_7, /*hidden argument*/NULL);
		__this->set_z_depthSorting_22(L_7);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_8 = Color_get_gray_m11EEED9092A8D949D3326611C7137334B746DF09(/*hidden argument*/NULL);
		__this->set_ColorFill_23(L_8);
		__this->set_fillType_24(2);
		ScriptableObject__ctor_m6E2B3821A4A361556FC12E9B1C71E1D5DC002C5B(__this, /*hidden argument*/NULL);
		MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F * L_9 = __this->get_address_of_infoGround_7();
		MaterialTextureInfo_Reset_m10A220F3D8334B1734C642F93409B8EB9C375289((MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F *)L_9, /*hidden argument*/NULL);
		MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F * L_10 = __this->get_address_of_infoWall_8();
		MaterialTextureInfo_Reset_m10A220F3D8334B1734C642F93409B8EB9C375289((MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F *)L_10, /*hidden argument*/NULL);
		MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F * L_11 = __this->get_address_of_infoCeiling_9();
		MaterialTextureInfo_Reset_m10A220F3D8334B1734C642F93409B8EB9C375289((MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F *)L_11, /*hidden argument*/NULL);
		MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F * L_12 = __this->get_address_of_infoFill_10();
		MaterialTextureInfo_Reset_m10A220F3D8334B1734C642F93409B8EB9C375289((MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F *)L_12, /*hidden argument*/NULL);
		MaterialTextureInfo_Reset_m10A220F3D8334B1734C642F93409B8EB9C375289((MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F *)(((Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D_StaticFields*)il2cpp_codegen_static_fields_for(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D_il2cpp_TypeInfo_var))->get_address_of_infoEmpty_11()), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Kolibri2d.Terrain2DMaterial::RefreshIfMissingData()
extern "C" IL2CPP_METHOD_ATTR void Terrain2DMaterial_RefreshIfMissingData_m7CACBD5F7AD5D5BBBA21AD4B2CD65E51A9DC77E8 (Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Terrain2DMaterial_RefreshIfMissingData_m7CACBD5F7AD5D5BBBA21AD4B2CD65E51A9DC77E8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A * L_0 = __this->get_textureInfoByType_6();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A * L_1 = __this->get_textureInfoByType_6();
		NullCheck(L_1);
		int32_t L_2 = Dictionary_2_get_Count_mB1BF016039CF52477957DBB0568653081D14612E(L_1, /*hidden argument*/Dictionary_2_get_Count_mB1BF016039CF52477957DBB0568653081D14612E_RuntimeMethod_var);
		if ((((int32_t)L_2) >= ((int32_t)5)))
		{
			goto IL_001c;
		}
	}

IL_0016:
	{
		Terrain2DMaterial_Refresh_mD6A2F2D44B21F6CC68F65957C50A71065DE64AFC(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void Kolibri2d.Terrain2DMaterial::Refresh()
extern "C" IL2CPP_METHOD_ATTR void Terrain2DMaterial_Refresh_mD6A2F2D44B21F6CC68F65957C50A71065DE64AFC (Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Terrain2DMaterial_Refresh_mD6A2F2D44B21F6CC68F65957C50A71065DE64AFC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A * L_0 = __this->get_textureInfoByType_6();
		MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  L_1 = __this->get_infoGround_7();
		NullCheck(L_0);
		Dictionary_2_set_Item_m85881E2C19054C55C36474E5876AC2AB23552E0B(L_0, 0, L_1, /*hidden argument*/Dictionary_2_set_Item_m85881E2C19054C55C36474E5876AC2AB23552E0B_RuntimeMethod_var);
		Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A * L_2 = __this->get_textureInfoByType_6();
		MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  L_3 = __this->get_infoWall_8();
		NullCheck(L_2);
		Dictionary_2_set_Item_m85881E2C19054C55C36474E5876AC2AB23552E0B(L_2, 1, L_3, /*hidden argument*/Dictionary_2_set_Item_m85881E2C19054C55C36474E5876AC2AB23552E0B_RuntimeMethod_var);
		Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A * L_4 = __this->get_textureInfoByType_6();
		MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  L_5 = __this->get_infoCeiling_9();
		NullCheck(L_4);
		Dictionary_2_set_Item_m85881E2C19054C55C36474E5876AC2AB23552E0B(L_4, 2, L_5, /*hidden argument*/Dictionary_2_set_Item_m85881E2C19054C55C36474E5876AC2AB23552E0B_RuntimeMethod_var);
		Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A * L_6 = __this->get_textureInfoByType_6();
		MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  L_7 = __this->get_infoFill_10();
		NullCheck(L_6);
		Dictionary_2_set_Item_m85881E2C19054C55C36474E5876AC2AB23552E0B(L_6, 3, L_7, /*hidden argument*/Dictionary_2_set_Item_m85881E2C19054C55C36474E5876AC2AB23552E0B_RuntimeMethod_var);
		Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A * L_8 = __this->get_textureInfoByType_6();
		MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  L_9 = ((Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D_StaticFields*)il2cpp_codegen_static_fields_for(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D_il2cpp_TypeInfo_var))->get_infoEmpty_11();
		NullCheck(L_8);
		Dictionary_2_set_Item_m85881E2C19054C55C36474E5876AC2AB23552E0B(L_8, 4, L_9, /*hidden argument*/Dictionary_2_set_Item_m85881E2C19054C55C36474E5876AC2AB23552E0B_RuntimeMethod_var);
		CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703 * L_10 = __this->get_cornerSprites_14();
		NullCheck(L_10);
		int32_t L_11 = L_10->get_cornerDisplayType_0();
		if ((((int32_t)L_11) == ((int32_t)1)))
		{
			goto IL_0073;
		}
	}
	{
		CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703 * L_12 = __this->get_cornerSprites_14();
		NullCheck(L_12);
		L_12->set_cornerDisplayType_0(1);
	}

IL_0073:
	{
		return;
	}
}
// System.Int32 Kolibri2d.Terrain2DMaterial::GetSortingLayer(Kolibri2d.TerrainType)
extern "C" IL2CPP_METHOD_ATTR int32_t Terrain2DMaterial_GetSortingLayer_m48717D940A3396AC6FA09ACEFCB72852DEC4D6D0 (Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * __this, int32_t ___e0, const RuntimeMethod* method)
{
	{
		Terrain2DMaterial_RefreshIfMissingData_m7CACBD5F7AD5D5BBBA21AD4B2CD65E51A9DC77E8(__this, /*hidden argument*/NULL);
		SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE * L_0 = __this->get_layersAndSorting_15();
		int32_t L_1 = ___e0;
		NullCheck(L_0);
		int32_t L_2 = SortingLayerOptions2_GetSortingLayer_m259EE7D3F8D0343560E701ACF982B9683C092F15(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 Kolibri2d.Terrain2DMaterial::GetOrderInLayer(Kolibri2d.TerrainType)
extern "C" IL2CPP_METHOD_ATTR int32_t Terrain2DMaterial_GetOrderInLayer_m60F485444FD21B5E12A24BF2DEE5A70A3DDD66D2 (Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * __this, int32_t ___e0, const RuntimeMethod* method)
{
	{
		Terrain2DMaterial_RefreshIfMissingData_m7CACBD5F7AD5D5BBBA21AD4B2CD65E51A9DC77E8(__this, /*hidden argument*/NULL);
		SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE * L_0 = __this->get_layersAndSorting_15();
		int32_t L_1 = ___e0;
		NullCheck(L_0);
		int32_t L_2 = SortingLayerOptions2_GetOrderInLayer_mC6BD69D8605F124803B63ACDA6E40112C8B2667B(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single Kolibri2d.Terrain2DMaterial::GetZDepth(Kolibri2d.TerrainType)
extern "C" IL2CPP_METHOD_ATTR float Terrain2DMaterial_GetZDepth_m01D619285CC2F4EE71E2354480E53F7A50939AF1 (Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * __this, int32_t ___e0, const RuntimeMethod* method)
{
	{
		Terrain2DMaterial_RefreshIfMissingData_m7CACBD5F7AD5D5BBBA21AD4B2CD65E51A9DC77E8(__this, /*hidden argument*/NULL);
		SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F * L_0 = __this->get_z_depthSorting_22();
		int32_t L_1 = ___e0;
		NullCheck(L_0);
		float L_2 = SortingZ_3D_Options_GetZDepth_mFB12B76278730CA1BECC46930191E63EFF0645FD(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Sprite Kolibri2d.Terrain2DMaterial::GetCornerSprite(Kolibri2d.Terrain2DMaterial/CornerType,Kolibri2d.Terrain2DMaterial/CornerSide)
extern "C" IL2CPP_METHOD_ATTR Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * Terrain2DMaterial_GetCornerSprite_mE2DCCE96E3B627EA35C6408978DDA65662C76E7A (Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * __this, int32_t ___c0, int32_t ___side1, const RuntimeMethod* method)
{
	{
		Terrain2DMaterial_RefreshIfMissingData_m7CACBD5F7AD5D5BBBA21AD4B2CD65E51A9DC77E8(__this, /*hidden argument*/NULL);
		CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703 * L_0 = __this->get_cornerSprites_14();
		int32_t L_1 = ___c0;
		int32_t L_2 = ___side1;
		NullCheck(L_0);
		Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * L_3 = CornerSprites2_GetCornerSprite_m6009BCA395A2ADDEB0CD134CF0949525A833F595(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Vector2 Kolibri2d.Terrain2DMaterial::GetCornerOffset(Kolibri2d.Terrain2DMaterial/CornerType,Kolibri2d.Terrain2DMaterial/CornerSide)
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Terrain2DMaterial_GetCornerOffset_mD784B3F1B5D9C7C6DBEACC1F206BC3CFD655BE02 (Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * __this, int32_t ___c0, int32_t ___side1, const RuntimeMethod* method)
{
	{
		Terrain2DMaterial_RefreshIfMissingData_m7CACBD5F7AD5D5BBBA21AD4B2CD65E51A9DC77E8(__this, /*hidden argument*/NULL);
		CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703 * L_0 = __this->get_cornerSprites_14();
		int32_t L_1 = ___c0;
		int32_t L_2 = ___side1;
		NullCheck(L_0);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_3 = CornerSprites2_GetCornerOffset_mE32AE7EB9004D2EAEC9BB00049F278F24CEF6320(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 Kolibri2d.Terrain2DMaterial::GetCornerExtraRotation(Kolibri2d.Terrain2DMaterial/CornerType,Kolibri2d.Terrain2DMaterial/CornerSide)
extern "C" IL2CPP_METHOD_ATTR int32_t Terrain2DMaterial_GetCornerExtraRotation_mC70D35F6DE26F67824F73EBA74A5ED8BB091E76C (Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * __this, int32_t ___c0, int32_t ___side1, const RuntimeMethod* method)
{
	{
		Terrain2DMaterial_RefreshIfMissingData_m7CACBD5F7AD5D5BBBA21AD4B2CD65E51A9DC77E8(__this, /*hidden argument*/NULL);
		CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703 * L_0 = __this->get_cornerSprites_14();
		int32_t L_1 = ___c0;
		int32_t L_2 = ___side1;
		NullCheck(L_0);
		int32_t L_3 = CornerSprites2_GetCornerExtraRotation_mA2E459C1D40E476E275910F13E595F4DEFB78285(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Sprite Kolibri2d.Terrain2DMaterial::GetSpriteFromTerrainType(Kolibri2d.TerrainType)
extern "C" IL2CPP_METHOD_ATTR Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * Terrain2DMaterial_GetSpriteFromTerrainType_mBD2DE70D633EC4156E2A6D845EC2CEA03AFEC661 (Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * __this, int32_t ___terrainType0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Terrain2DMaterial_GetSpriteFromTerrainType_mBD2DE70D633EC4156E2A6D845EC2CEA03AFEC661_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Terrain2DMaterial_RefreshIfMissingData_m7CACBD5F7AD5D5BBBA21AD4B2CD65E51A9DC77E8(__this, /*hidden argument*/NULL);
		Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A * L_0 = __this->get_textureInfoByType_6();
		int32_t L_1 = ___terrainType0;
		NullCheck(L_0);
		MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  L_2 = Dictionary_2_get_Item_mF1FB9229F3B369C779CEF8B43902E24D6C3FCD3D(L_0, L_1, /*hidden argument*/Dictionary_2_get_Item_mF1FB9229F3B369C779CEF8B43902E24D6C3FCD3D_RuntimeMethod_var);
		Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * L_3 = L_2.get_sprite_0();
		return L_3;
	}
}
// Kolibri2d.Terrain2DMaterial/CornerType Kolibri2d.Terrain2DMaterial::GetCornerType(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,Kolibri2d.TerrainType,System.Boolean,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR int32_t Terrain2DMaterial_GetCornerType_m188801980D16E5824EB9BEF103BD4CCBCEC0D4EB (Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___p10, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___p21, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___p32, int32_t ___currType3, bool ___isClockwise4, bool ___manualFlip5, const RuntimeMethod* method)
{
	{
		Terrain2DMaterial_RefreshIfMissingData_m7CACBD5F7AD5D5BBBA21AD4B2CD65E51A9DC77E8(__this, /*hidden argument*/NULL);
		bool L_0 = ___manualFlip5;
		if (!L_0)
		{
			goto IL_0086;
		}
	}
	{
		int32_t L_1 = ___currType3;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0022;
			}
			case 1:
			{
				goto IL_0054;
			}
			case 2:
			{
				goto IL_0022;
			}
		}
	}
	{
		goto IL_00ff;
	}

IL_0022:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ___p21;
		float L_3 = L_2.get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = ___p32;
		float L_5 = L_4.get_y_3();
		if ((!(((float)L_3) < ((float)L_5))))
		{
			goto IL_0042;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = ___p21;
		float L_7 = L_6.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = ___p10;
		float L_9 = L_8.get_x_2();
		if ((((float)L_7) < ((float)L_9)))
		{
			goto IL_0040;
		}
	}
	{
		return (int32_t)(3);
	}

IL_0040:
	{
		return (int32_t)(2);
	}

IL_0042:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = ___p21;
		float L_11 = L_10.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = ___p10;
		float L_13 = L_12.get_x_2();
		if ((((float)L_11) < ((float)L_13)))
		{
			goto IL_0052;
		}
	}
	{
		return (int32_t)(1);
	}

IL_0052:
	{
		return (int32_t)(0);
	}

IL_0054:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_14 = ___p21;
		float L_15 = L_14.get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16 = ___p10;
		float L_17 = L_16.get_y_3();
		if ((!(((float)L_15) < ((float)L_17))))
		{
			goto IL_0074;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_18 = ___p21;
		float L_19 = L_18.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_20 = ___p32;
		float L_21 = L_20.get_x_2();
		if ((((float)L_19) < ((float)L_21)))
		{
			goto IL_0072;
		}
	}
	{
		return (int32_t)(3);
	}

IL_0072:
	{
		return (int32_t)(2);
	}

IL_0074:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22 = ___p21;
		float L_23 = L_22.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_24 = ___p32;
		float L_25 = L_24.get_x_2();
		if ((((float)L_23) < ((float)L_25)))
		{
			goto IL_0084;
		}
	}
	{
		return (int32_t)(1);
	}

IL_0084:
	{
		return (int32_t)(0);
	}

IL_0086:
	{
		int32_t L_26 = ___currType3;
		switch (L_26)
		{
			case 0:
			{
				goto IL_009b;
			}
			case 1:
			{
				goto IL_00cd;
			}
			case 2:
			{
				goto IL_009b;
			}
		}
	}
	{
		goto IL_00ff;
	}

IL_009b:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_27 = ___p21;
		float L_28 = L_27.get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_29 = ___p32;
		float L_30 = L_29.get_y_3();
		if ((!(((float)L_28) < ((float)L_30))))
		{
			goto IL_00bb;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_31 = ___p21;
		float L_32 = L_31.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_33 = ___p10;
		float L_34 = L_33.get_x_2();
		if ((((float)L_32) < ((float)L_34)))
		{
			goto IL_00b9;
		}
	}
	{
		return (int32_t)(3);
	}

IL_00b9:
	{
		return (int32_t)(2);
	}

IL_00bb:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_35 = ___p21;
		float L_36 = L_35.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_37 = ___p10;
		float L_38 = L_37.get_x_2();
		if ((((float)L_36) < ((float)L_38)))
		{
			goto IL_00cb;
		}
	}
	{
		return (int32_t)(1);
	}

IL_00cb:
	{
		return (int32_t)(0);
	}

IL_00cd:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_39 = ___p21;
		float L_40 = L_39.get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_41 = ___p10;
		float L_42 = L_41.get_y_3();
		if ((!(((float)L_40) < ((float)L_42))))
		{
			goto IL_00ed;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_43 = ___p21;
		float L_44 = L_43.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_45 = ___p32;
		float L_46 = L_45.get_x_2();
		if ((((float)L_44) < ((float)L_46)))
		{
			goto IL_00eb;
		}
	}
	{
		return (int32_t)(3);
	}

IL_00eb:
	{
		return (int32_t)(2);
	}

IL_00ed:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_47 = ___p21;
		float L_48 = L_47.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_49 = ___p32;
		float L_50 = L_49.get_x_2();
		if ((((float)L_48) < ((float)L_50)))
		{
			goto IL_00fd;
		}
	}
	{
		return (int32_t)(1);
	}

IL_00fd:
	{
		return (int32_t)(0);
	}

IL_00ff:
	{
		return (int32_t)(4);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Sprite Kolibri2d.Terrain2DMaterial/CornerSprites2::GetCornerSprite(Kolibri2d.Terrain2DMaterial/CornerType,Kolibri2d.Terrain2DMaterial/CornerSide)
extern "C" IL2CPP_METHOD_ATTR Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * CornerSprites2_GetCornerSprite_m6009BCA395A2ADDEB0CD134CF0949525A833F595 (CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703 * __this, int32_t ___c0, int32_t ___side1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___side1;
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		int32_t L_1 = ___side1;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_003d;
		}
	}
	{
		goto IL_0071;
	}

IL_0009:
	{
		int32_t L_2 = ___c0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0028;
			}
			case 2:
			{
				goto IL_002f;
			}
			case 3:
			{
				goto IL_0036;
			}
		}
	}
	{
		goto IL_0071;
	}

IL_0021:
	{
		Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * L_3 = __this->get_interiorTopLeft_1();
		return L_3;
	}

IL_0028:
	{
		Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * L_4 = __this->get_interiorTopRight_4();
		return L_4;
	}

IL_002f:
	{
		Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * L_5 = __this->get_interiorBotLeft_7();
		return L_5;
	}

IL_0036:
	{
		Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * L_6 = __this->get_interiorBotRight_10();
		return L_6;
	}

IL_003d:
	{
		int32_t L_7 = ___c0;
		switch (L_7)
		{
			case 0:
			{
				goto IL_0055;
			}
			case 1:
			{
				goto IL_005c;
			}
			case 2:
			{
				goto IL_0063;
			}
			case 3:
			{
				goto IL_006a;
			}
		}
	}
	{
		goto IL_0071;
	}

IL_0055:
	{
		Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * L_8 = __this->get_exteriorTopLeft_13();
		return L_8;
	}

IL_005c:
	{
		Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * L_9 = __this->get_exteriorTopRight_16();
		return L_9;
	}

IL_0063:
	{
		Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * L_10 = __this->get_exteriorBotLeft_19();
		return L_10;
	}

IL_006a:
	{
		Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * L_11 = __this->get_exteriorBotRight_21();
		return L_11;
	}

IL_0071:
	{
		return (Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 *)NULL;
	}
}
// UnityEngine.Vector2 Kolibri2d.Terrain2DMaterial/CornerSprites2::GetCornerOffset(Kolibri2d.Terrain2DMaterial/CornerType,Kolibri2d.Terrain2DMaterial/CornerSide)
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  CornerSprites2_GetCornerOffset_mE32AE7EB9004D2EAEC9BB00049F278F24CEF6320 (CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703 * __this, int32_t ___c0, int32_t ___side1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CornerSprites2_GetCornerOffset_mE32AE7EB9004D2EAEC9BB00049F278F24CEF6320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___side1;
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		int32_t L_1 = ___side1;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_003d;
		}
	}
	{
		goto IL_0071;
	}

IL_0009:
	{
		int32_t L_2 = ___c0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0028;
			}
			case 2:
			{
				goto IL_002f;
			}
			case 3:
			{
				goto IL_0036;
			}
		}
	}
	{
		goto IL_0071;
	}

IL_0021:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_3 = __this->get_offsetITL_2();
		return L_3;
	}

IL_0028:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_4 = __this->get_offsetITR_5();
		return L_4;
	}

IL_002f:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_5 = __this->get_offsetIBL_8();
		return L_5;
	}

IL_0036:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_6 = __this->get_offsetIBR_11();
		return L_6;
	}

IL_003d:
	{
		int32_t L_7 = ___c0;
		switch (L_7)
		{
			case 0:
			{
				goto IL_0055;
			}
			case 1:
			{
				goto IL_005c;
			}
			case 2:
			{
				goto IL_0063;
			}
			case 3:
			{
				goto IL_006a;
			}
		}
	}
	{
		goto IL_0071;
	}

IL_0055:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_8 = __this->get_offsetETL_14();
		return L_8;
	}

IL_005c:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_9 = __this->get_offsetETR_17();
		return L_9;
	}

IL_0063:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_10 = __this->get_offsetEBL_20();
		return L_10;
	}

IL_006a:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_11 = __this->get_offsetEBR_23();
		return L_11;
	}

IL_0071:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_12 = Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8(/*hidden argument*/NULL);
		return L_12;
	}
}
// System.Int32 Kolibri2d.Terrain2DMaterial/CornerSprites2::GetCornerExtraRotation(Kolibri2d.Terrain2DMaterial/CornerType,Kolibri2d.Terrain2DMaterial/CornerSide)
extern "C" IL2CPP_METHOD_ATTR int32_t CornerSprites2_GetCornerExtraRotation_mA2E459C1D40E476E275910F13E595F4DEFB78285 (CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703 * __this, int32_t ___c0, int32_t ___side1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___side1;
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		int32_t L_1 = ___side1;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_003d;
		}
	}
	{
		goto IL_0071;
	}

IL_0009:
	{
		int32_t L_2 = ___c0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0028;
			}
			case 2:
			{
				goto IL_002f;
			}
			case 3:
			{
				goto IL_0036;
			}
		}
	}
	{
		goto IL_0071;
	}

IL_0021:
	{
		int32_t L_3 = __this->get_rotationITL_3();
		return L_3;
	}

IL_0028:
	{
		int32_t L_4 = __this->get_rotationITR_6();
		return L_4;
	}

IL_002f:
	{
		int32_t L_5 = __this->get_rotationIBL_9();
		return L_5;
	}

IL_0036:
	{
		int32_t L_6 = __this->get_rotationIBR_12();
		return L_6;
	}

IL_003d:
	{
		int32_t L_7 = ___c0;
		switch (L_7)
		{
			case 0:
			{
				goto IL_0055;
			}
			case 1:
			{
				goto IL_005c;
			}
			case 2:
			{
				goto IL_0063;
			}
			case 3:
			{
				goto IL_006a;
			}
		}
	}
	{
		goto IL_0071;
	}

IL_0055:
	{
		int32_t L_8 = __this->get_rotationETL_15();
		return L_8;
	}

IL_005c:
	{
		int32_t L_9 = __this->get_rotationETR_18();
		return L_9;
	}

IL_0063:
	{
		int32_t L_10 = __this->get_rotationEBL_22();
		return L_10;
	}

IL_006a:
	{
		int32_t L_11 = __this->get_rotationEBR_24();
		return L_11;
	}

IL_0071:
	{
		return 0;
	}
}
// System.Void Kolibri2d.Terrain2DMaterial/CornerSprites2::.ctor()
extern "C" IL2CPP_METHOD_ATTR void CornerSprites2__ctor_m1AF5E76E4493B22EC500394DF591D669EBAA1458 (CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703 * __this, const RuntimeMethod* method)
{
	{
		__this->set_cornerDisplayType_0(1);
		Object__ctor_m8BA07445967EE2CC15961AD3C16F25DB74506EA0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::GetSortingLayer(Kolibri2d.TerrainType)
extern "C" IL2CPP_METHOD_ATTR int32_t SortingLayerOptions2_GetSortingLayer_m259EE7D3F8D0343560E701ACF982B9683C092F15 (SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE * __this, int32_t ___e0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___e0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0018;
			}
			case 1:
			{
				goto IL_001f;
			}
			case 2:
			{
				goto IL_0026;
			}
			case 3:
			{
				goto IL_002d;
			}
		}
	}
	{
		goto IL_0034;
	}

IL_0018:
	{
		int32_t L_1 = __this->get_groundLayer_0();
		return L_1;
	}

IL_001f:
	{
		int32_t L_2 = __this->get_wallLayer_2();
		return L_2;
	}

IL_0026:
	{
		int32_t L_3 = __this->get_ceilingLayer_4();
		return L_3;
	}

IL_002d:
	{
		int32_t L_4 = __this->get_fillLayer_6();
		return L_4;
	}

IL_0034:
	{
		return (-1);
	}
}
// System.Int32 Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::GetOrderInLayer(Kolibri2d.TerrainType)
extern "C" IL2CPP_METHOD_ATTR int32_t SortingLayerOptions2_GetOrderInLayer_mC6BD69D8605F124803B63ACDA6E40112C8B2667B (SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE * __this, int32_t ___e0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___e0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0018;
			}
			case 1:
			{
				goto IL_001f;
			}
			case 2:
			{
				goto IL_0026;
			}
			case 3:
			{
				goto IL_002d;
			}
		}
	}
	{
		goto IL_0034;
	}

IL_0018:
	{
		int32_t L_1 = __this->get_groundOrder_1();
		return L_1;
	}

IL_001f:
	{
		int32_t L_2 = __this->get_wallOrder_3();
		return L_2;
	}

IL_0026:
	{
		int32_t L_3 = __this->get_ceilingOrder_5();
		return L_3;
	}

IL_002d:
	{
		int32_t L_4 = __this->get_fillOrder_7();
		return L_4;
	}

IL_0034:
	{
		return 0;
	}
}
// System.Void Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SortingLayerOptions2__ctor_m5CE73EC2FC70B971D15CE81AC4C4EDC3EA4C49DC (SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE * __this, const RuntimeMethod* method)
{
	{
		__this->set_groundOrder_1(3);
		__this->set_wallOrder_3(2);
		__this->set_ceilingOrder_5(1);
		__this->set_cornerOrder_9(4);
		Object__ctor_m8BA07445967EE2CC15961AD3C16F25DB74506EA0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single Kolibri2d.Terrain2DMaterial/SortingZ_3D_Options::GetZDepth(Kolibri2d.TerrainType)
extern "C" IL2CPP_METHOD_ATTR float SortingZ_3D_Options_GetZDepth_mFB12B76278730CA1BECC46930191E63EFF0645FD (SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F * __this, int32_t ___e0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___e0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0018;
			}
			case 1:
			{
				goto IL_001f;
			}
			case 2:
			{
				goto IL_0026;
			}
			case 3:
			{
				goto IL_002d;
			}
		}
	}
	{
		goto IL_0034;
	}

IL_0018:
	{
		float L_1 = __this->get_groundZ_0();
		return L_1;
	}

IL_001f:
	{
		float L_2 = __this->get_wallZ_1();
		return L_2;
	}

IL_0026:
	{
		float L_3 = __this->get_ceilingZ_2();
		return L_3;
	}

IL_002d:
	{
		float L_4 = __this->get_fillZ_3();
		return L_4;
	}

IL_0034:
	{
		return (0.0f);
	}
}
// System.Void Kolibri2d.Terrain2DMaterial/SortingZ_3D_Options::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SortingZ_3D_Options__ctor_m9944ABAD4B79DEC1A85324A1CA2F9FCE738CBA14 (SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F * __this, const RuntimeMethod* method)
{
	{
		__this->set_wallZ_1((-0.1f));
		__this->set_fillZ_3((0.1f));
		__this->set_cornerZ_4((-0.2f));
		Object__ctor_m8BA07445967EE2CC15961AD3C16F25DB74506EA0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single Kolibri2d.Utils::GetAngleABC(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR float Utils_GetAngleABC_mE43F92353A13E1D3011FDE80954F05B69EC60BDC (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___c2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_GetAngleABC_mE43F92353A13E1D3011FDE80954F05B69EC60BDC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___b1;
		float L_1 = L_0.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ___a0;
		float L_3 = L_2.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = ___b1;
		float L_5 = L_4.get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = ___a0;
		float L_7 = L_6.get_y_3();
		Vector3__ctor_m6AD8F21FFCC7723C6F507CCF2E4E2EFFC4871584((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = ___b1;
		float L_9 = L_8.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = ___c2;
		float L_11 = L_10.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = ___b1;
		float L_13 = L_12.get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_14 = ___c2;
		float L_15 = L_14.get_y_3();
		Vector3__ctor_m6AD8F21FFCC7723C6F507CCF2E4E2EFFC4871584((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_1), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), ((float)il2cpp_codegen_subtract((float)L_13, (float)L_15)), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16 = V_0;
		float L_17 = L_16.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_18 = V_1;
		float L_19 = L_18.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_20 = V_0;
		float L_21 = L_20.get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22 = V_1;
		float L_23 = L_22.get_y_3();
		V_2 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_17, (float)L_19)), (float)((float)il2cpp_codegen_multiply((float)L_21, (float)L_23))));
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_24 = V_0;
		float L_25 = L_24.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_26 = V_1;
		float L_27 = L_26.get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_28 = V_0;
		float L_29 = L_28.get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_30 = V_1;
		float L_31 = L_30.get_x_2();
		float L_32 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_33 = atan2f(((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_25, (float)L_27)), (float)((float)il2cpp_codegen_multiply((float)L_29, (float)L_31)))), L_32);
		float L_34 = floorf(((float)il2cpp_codegen_add((float)((float)((float)((float)il2cpp_codegen_multiply((float)L_33, (float)(180.0f)))/(float)(3.14159274f))), (float)(0.5f))));
		return L_34;
	}
}
// System.Single Kolibri2d.Utils::GetAngleBetweenSplineHelpers(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR float Utils_GetAngleBetweenSplineHelpers_mED4FE688F3A8BAA88AFA9A807978D6F44291C73B (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___p10, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___helper11, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___helper22, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___p23, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_GetAngleBetweenSplineHelpers_mED4FE688F3A8BAA88AFA9A807978D6F44291C73B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___p10;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___helper11;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ___helper22;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = ___p23;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = Bezier_GetFirstDerivative_mC9145E1496744EA98B64F8CE58B27DC5DCC21A30(L_0, L_1, L_2, L_3, (0.0f), /*hidden argument*/NULL);
		V_0 = L_4;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = ___p10;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = ___helper11;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = ___helper22;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = ___p23;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = Bezier_GetFirstDerivative_mC9145E1496744EA98B64F8CE58B27DC5DCC21A30(L_5, L_6, L_7, L_8, (1.0f), /*hidden argument*/NULL);
		V_1 = L_9;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		bool L_12 = Vector3_op_Equality_mA9E2F96E98E71AE7ACCE74766D700D41F0404806(L_10, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0038;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_14 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		bool L_15 = Vector3_op_Equality_mA9E2F96E98E71AE7ACCE74766D700D41F0404806(L_13, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_003e;
		}
	}

IL_0038:
	{
		return (0.0f);
	}

IL_003e:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_17 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_18 = V_1;
		float L_19 = Utils_GetAngleABC_mE43F92353A13E1D3011FDE80954F05B69EC60BDC(L_16, L_17, L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_20 = fabsf(L_19);
		return L_20;
	}
}
// System.Boolean Kolibri2d.Utils::Intersect(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&)
extern "C" IL2CPP_METHOD_ATTR bool Utils_Intersect_m47DF229EA539C283BF8A7B9F29680696F466951C (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___line1V10, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___line1V21, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___line2V12, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___line2V23, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___result4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_Intersect_m47DF229EA539C283BF8A7B9F29680696F466951C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___line1V21;
		float L_1 = L_0.get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ___line1V10;
		float L_3 = L_2.get_y_3();
		V_0 = ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3));
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = ___line1V21;
		float L_5 = L_4.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = ___line1V10;
		float L_7 = L_6.get_x_2();
		V_1 = ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7));
		float L_8 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = ___line1V10;
		float L_10 = L_9.get_x_2();
		float L_11 = V_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = ___line1V10;
		float L_13 = L_12.get_y_3();
		V_2 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_8, (float)L_10)), (float)((float)il2cpp_codegen_multiply((float)L_11, (float)L_13))));
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_14 = ___line2V23;
		float L_15 = L_14.get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16 = ___line2V12;
		float L_17 = L_16.get_y_3();
		V_3 = ((float)il2cpp_codegen_subtract((float)L_15, (float)L_17));
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_18 = ___line2V23;
		float L_19 = L_18.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_20 = ___line2V12;
		float L_21 = L_20.get_x_2();
		V_4 = ((float)il2cpp_codegen_subtract((float)L_19, (float)L_21));
		float L_22 = V_3;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_23 = ___line2V12;
		float L_24 = L_23.get_x_2();
		float L_25 = V_4;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_26 = ___line2V12;
		float L_27 = L_26.get_y_3();
		V_5 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_22, (float)L_24)), (float)((float)il2cpp_codegen_multiply((float)L_25, (float)L_27))));
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_28 = ___result4;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_29 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		*(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)L_28 = L_29;
		float L_30 = V_0;
		float L_31 = V_4;
		float L_32 = V_3;
		float L_33 = V_1;
		V_6 = ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_30, (float)L_31)), (float)((float)il2cpp_codegen_multiply((float)L_32, (float)L_33))));
		float L_34 = V_6;
		if ((!(((float)L_34) == ((float)(0.0f)))))
		{
			goto IL_0080;
		}
	}
	{
		return (bool)0;
	}

IL_0080:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_35 = ___result4;
		float L_36 = V_4;
		float L_37 = V_2;
		float L_38 = V_1;
		float L_39 = V_5;
		float L_40 = V_6;
		L_35->set_x_2(((float)((float)((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_36, (float)L_37)), (float)((float)il2cpp_codegen_multiply((float)L_38, (float)L_39))))/(float)L_40)));
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_41 = ___result4;
		float L_42 = V_0;
		float L_43 = V_5;
		float L_44 = V_3;
		float L_45 = V_2;
		float L_46 = V_6;
		L_41->set_y_3(((float)((float)((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_42, (float)L_43)), (float)((float)il2cpp_codegen_multiply((float)L_44, (float)L_45))))/(float)L_46)));
		return (bool)1;
	}
}
// UnityEngine.Vector2[] Kolibri2d.Utils::GetPoints2DFromVector(UnityEngine.Vector3[],System.Boolean)
extern "C" IL2CPP_METHOD_ATTR Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* Utils_GetPoints2DFromVector_mA4EA2CFA4980E6B1B24739C2B26DC00A9B2F0AAB (Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___points0, bool ___removeLast1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_GetPoints2DFromVector_mA4EA2CFA4980E6B1B24739C2B26DC00A9B2F0AAB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = ___removeLast1;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_1 = ___points0;
		NullCheck(L_1);
		G_B3_0 = (((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))));
		goto IL_000d;
	}

IL_0008:
	{
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_2 = ___points0;
		NullCheck(L_2);
		G_B3_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length)))), (int32_t)1));
	}

IL_000d:
	{
		V_0 = G_B3_0;
		int32_t L_3 = V_0;
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_4 = (Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6*)SZArrayNew(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6_il2cpp_TypeInfo_var, (uint32_t)L_3);
		V_1 = L_4;
		V_2 = 0;
		goto IL_004d;
	}

IL_0019:
	{
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_7 = ___points0;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		float L_9 = ((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)))->get_x_2();
		((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->set_x_0(L_9);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_10 = V_1;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_12 = ___points0;
		int32_t L_13 = V_2;
		NullCheck(L_12);
		float L_14 = ((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_y_3();
		((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->set_y_1(L_14);
		int32_t L_15 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_004d:
	{
		int32_t L_16 = V_2;
		int32_t L_17 = V_0;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0019;
		}
	}
	{
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_18 = V_1;
		return L_18;
	}
}
// UnityEngine.Vector2[] Kolibri2d.Utils::GetPoints2DFromVector(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* Utils_GetPoints2DFromVector_m90A81A570C3D07DB970F0342572FFFCF5C53A51F (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___points0, bool ___removeLast1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_GetPoints2DFromVector_m90A81A570C3D07DB970F0342572FFFCF5C53A51F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = ___removeLast1;
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_1 = ___points0;
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3(L_1, /*hidden argument*/List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_RuntimeMethod_var);
		G_B3_0 = L_2;
		goto IL_0013;
	}

IL_000b:
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_3 = ___points0;
		NullCheck(L_3);
		int32_t L_4 = List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3(L_3, /*hidden argument*/List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_RuntimeMethod_var);
		G_B3_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)1));
	}

IL_0013:
	{
		V_0 = G_B3_0;
		int32_t L_5 = V_0;
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_6 = (Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6*)SZArrayNew(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6_il2cpp_TypeInfo_var, (uint32_t)L_5);
		V_1 = L_6;
		V_2 = 0;
		goto IL_0053;
	}

IL_001f:
	{
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_7 = V_1;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_9 = ___points0;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_9, L_10, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		float L_12 = L_11.get_x_2();
		((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)))->set_x_0(L_12);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_13 = V_1;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_15 = ___points0;
		int32_t L_16 = V_2;
		NullCheck(L_15);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_17 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_15, L_16, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		float L_18 = L_17.get_y_3();
		((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_14)))->set_y_1(L_18);
		int32_t L_19 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
	}

IL_0053:
	{
		int32_t L_20 = V_2;
		int32_t L_21 = V_0;
		if ((((int32_t)L_20) < ((int32_t)L_21)))
		{
			goto IL_001f;
		}
	}
	{
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_22 = V_1;
		return L_22;
	}
}
// UnityEngine.Transform Kolibri2d.Utils::CreateOrFindChildNode(UnityEngine.Transform,System.String)
extern "C" IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Utils_CreateOrFindChildNode_m7F0060AEB5544F12EAF507A003F170E6A6FA3BF2 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___t0, String_t* ___name1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_CreateOrFindChildNode_m7F0060AEB5544F12EAF507A003F170E6A6FA3BF2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * V_0 = NULL;
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = ___t0;
		String_t* L_1 = ___name1;
		NullCheck(L_0);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_2 = Transform_Find_m673797B6329C2669A543904532ABA1680DA4EAD1(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_3, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004a;
		}
	}
	{
		String_t* L_5 = ___name1;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)il2cpp_codegen_object_new(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var);
		GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686(L_6, L_5, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = L_6;
		NullCheck(L_7);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_7, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = ___t0;
		NullCheck(L_8);
		Transform_set_parent_m65B8E4660B2C554069C57A957D9E55FECA7AA73E(L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_7, /*hidden argument*/NULL);
		V_0 = L_10;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = Vector3_get_one_mA11B83037CB269C6076CBCF754E24C8F3ACEC2AB(/*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_11, L_12, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_13 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_14 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_13, L_14, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_16 = Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64(/*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A(L_15, L_16, /*hidden argument*/NULL);
	}

IL_004a:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_17 = V_0;
		return L_17;
	}
}
// UnityEngine.Bounds Kolibri2d.Utils::GetBoundsFromPoints(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C" IL2CPP_METHOD_ATTR Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  Utils_GetBoundsFromPoints_m336ABC40128A0F623E3AB290DA0A425B707E2796 (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___points0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_GetBoundsFromPoints_m336ABC40128A0F623E3AB290DA0A425B707E2796_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_0 = ___points0;
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3(L_0, /*hidden argument*/List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_RuntimeMethod_var);
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		il2cpp_codegen_initobj((&V_3), sizeof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 ));
		Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  L_2 = V_3;
		return L_2;
	}

IL_0013:
	{
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), (std::numeric_limits<float>::max()), (std::numeric_limits<float>::max()), (std::numeric_limits<float>::max()), /*hidden argument*/NULL);
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_1), (-std::numeric_limits<float>::max()), (-std::numeric_limits<float>::max()), (-std::numeric_limits<float>::max()), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		V_2 = L_3;
		Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_4 = ___points0;
		NullCheck(L_4);
		Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F  L_5 = List_1_GetEnumerator_mA6AD60947F39CE54DA7D237D278C8948D5B2C331(L_4, /*hidden argument*/List_1_GetEnumerator_mA6AD60947F39CE54DA7D237D278C8948D5B2C331_RuntimeMethod_var);
		V_4 = L_5;
	}

IL_0053:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0129;
		}

IL_0058:
		{
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Enumerator_get_Current_mAB079301955D01523AB6CD62D400632D218B265D((Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F *)(&V_4), /*hidden argument*/Enumerator_get_Current_mAB079301955D01523AB6CD62D400632D218B265D_RuntimeMethod_var);
			V_5 = L_6;
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = V_5;
			float L_8 = L_7.get_x_2();
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = V_0;
			float L_10 = L_9.get_x_2();
			if ((!(((float)L_8) < ((float)L_10))))
			{
				goto IL_007e;
			}
		}

IL_0070:
		{
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = V_5;
			float L_12 = L_11.get_x_2();
			(&V_0)->set_x_2(L_12);
		}

IL_007e:
		{
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = V_5;
			float L_14 = L_13.get_y_3();
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_15 = V_0;
			float L_16 = L_15.get_y_3();
			if ((!(((float)L_14) < ((float)L_16))))
			{
				goto IL_009b;
			}
		}

IL_008d:
		{
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_17 = V_5;
			float L_18 = L_17.get_y_3();
			(&V_0)->set_y_3(L_18);
		}

IL_009b:
		{
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_19 = V_5;
			float L_20 = L_19.get_z_4();
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_21 = V_0;
			float L_22 = L_21.get_z_4();
			if ((!(((float)L_20) < ((float)L_22))))
			{
				goto IL_00b8;
			}
		}

IL_00aa:
		{
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_23 = V_5;
			float L_24 = L_23.get_z_4();
			(&V_0)->set_z_4(L_24);
		}

IL_00b8:
		{
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_25 = V_5;
			float L_26 = L_25.get_x_2();
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_27 = V_1;
			float L_28 = L_27.get_x_2();
			if ((!(((float)L_26) > ((float)L_28))))
			{
				goto IL_00d5;
			}
		}

IL_00c7:
		{
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_29 = V_5;
			float L_30 = L_29.get_x_2();
			(&V_1)->set_x_2(L_30);
		}

IL_00d5:
		{
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_31 = V_5;
			float L_32 = L_31.get_y_3();
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_33 = V_1;
			float L_34 = L_33.get_y_3();
			if ((!(((float)L_32) > ((float)L_34))))
			{
				goto IL_00f2;
			}
		}

IL_00e4:
		{
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_35 = V_5;
			float L_36 = L_35.get_y_3();
			(&V_1)->set_y_3(L_36);
		}

IL_00f2:
		{
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_37 = V_5;
			float L_38 = L_37.get_z_4();
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_39 = V_1;
			float L_40 = L_39.get_z_4();
			if ((!(((float)L_38) > ((float)L_40))))
			{
				goto IL_010f;
			}
		}

IL_0101:
		{
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_41 = V_5;
			float L_42 = L_41.get_z_4();
			(&V_1)->set_z_4(L_42);
		}

IL_010f:
		{
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_43 = V_1;
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_44 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_45 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_43, L_44, /*hidden argument*/NULL);
			V_2 = L_45;
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_46 = V_0;
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_47 = V_2;
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_48 = Vector3_op_Division_mDF34F1CC445981B4D1137765BC6277419E561624(L_47, (2.0f), /*hidden argument*/NULL);
			Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_46, L_48, /*hidden argument*/NULL);
		}

IL_0129:
		{
			bool L_49 = Enumerator_MoveNext_mFB1EBCAC4E921F22042B0BDB6B954B7A09589454((Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F *)(&V_4), /*hidden argument*/Enumerator_MoveNext_mFB1EBCAC4E921F22042B0BDB6B954B7A09589454_RuntimeMethod_var);
			if (L_49)
			{
				goto IL_0058;
			}
		}

IL_0135:
		{
			IL2CPP_LEAVE(0x145, FINALLY_0137);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0137;
	}

FINALLY_0137:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m640FFEF2E9A18C15D423069973168AD40E10FBFB((Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F *)(&V_4), /*hidden argument*/Enumerator_Dispose_m640FFEF2E9A18C15D423069973168AD40E10FBFB_RuntimeMethod_var);
		IL2CPP_END_FINALLY(311)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(311)
	{
		IL2CPP_JUMP_TBL(0x145, IL_0145)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0145:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_50 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_51 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_52 = Vector3_op_Division_mDF34F1CC445981B4D1137765BC6277419E561624(L_51, (2.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_53 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_50, L_52, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_54 = V_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_55 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_56 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_54, L_55, /*hidden argument*/NULL);
		Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  L_57;
		memset(&L_57, 0, sizeof(L_57));
		Bounds__ctor_m294E77A20EC1A3E96985FE1A925CB271D1B5266D((&L_57), L_53, L_56, /*hidden argument*/NULL);
		return L_57;
	}
}
// UnityEngine.Bounds Kolibri2d.Utils::GetBounds(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  Utils_GetBounds_mF3CC88CAFD5D87D5B845757575247F0324B0D38B (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_GetBounds_mF3CC88CAFD5D87D5B845757575247F0324B0D38B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * V_0 = NULL;
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  V_1;
	memset(&V_1, 0, sizeof(V_1));
	RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* V_2 = NULL;
	int32_t V_3 = 0;
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * V_4 = NULL;
	BezierSplineU5BU5D_tD9962D7525021265D09A9C68F754780C16917F1C* V_5 = NULL;
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  V_6;
	memset(&V_6, 0, sizeof(V_6));
	int32_t V_7 = 0;
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * V_8 = NULL;
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  G_B8_0;
	memset(&G_B8_0, 0, sizeof(G_B8_0));
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = ___obj0;
		NullCheck(L_0);
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_1 = GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD202DDB933765AC3989986C7FA3C14E6E9A191EC(L_0, /*hidden argument*/GameObject_GetComponent_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_mD202DDB933765AC3989986C7FA3C14E6E9A191EC_RuntimeMethod_var);
		V_0 = L_1;
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_005d;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = ___obj0;
		NullCheck(L_4);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_5 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		Bounds__ctor_m294E77A20EC1A3E96985FE1A925CB271D1B5266D((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&V_1), L_6, L_7, /*hidden argument*/NULL);
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_8 = V_0;
		NullCheck(L_8);
		Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  L_9 = Renderer_get_bounds_mB29E41E26DD95939C09F3EC67F5B2793A438BDB5(L_8, /*hidden argument*/NULL);
		Bounds_Encapsulate_m394E7F823ADE56B97E6723B645C9458F1ADF0089((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&V_1), L_9, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_10 = ___obj0;
		NullCheck(L_10);
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_11 = GameObject_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_m8BA044694988F70856600001893D4CB591BD5B29(L_10, /*hidden argument*/GameObject_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_m8BA044694988F70856600001893D4CB591BD5B29_RuntimeMethod_var);
		V_2 = L_11;
		V_3 = 0;
		goto IL_0055;
	}

IL_003e:
	{
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_12 = V_2;
		int32_t L_13 = V_3;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_4 = L_15;
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_16 = V_4;
		NullCheck(L_16);
		Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  L_17 = Renderer_get_bounds_mB29E41E26DD95939C09F3EC67F5B2793A438BDB5(L_16, /*hidden argument*/NULL);
		Bounds_Encapsulate_m394E7F823ADE56B97E6723B645C9458F1ADF0089((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&V_1), L_17, /*hidden argument*/NULL);
		int32_t L_18 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_0055:
	{
		int32_t L_19 = V_3;
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_20 = V_2;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_20)->max_length)))))))
		{
			goto IL_003e;
		}
	}
	{
		Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  L_21 = V_1;
		return L_21;
	}

IL_005d:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_22 = ___obj0;
		NullCheck(L_22);
		BezierSplineU5BU5D_tD9962D7525021265D09A9C68F754780C16917F1C* L_23 = GameObject_GetComponentsInChildren_TisBezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_m968410DEC000954AFE3CA0F676596D8F14B8A570(L_22, /*hidden argument*/GameObject_GetComponentsInChildren_TisBezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_m968410DEC000954AFE3CA0F676596D8F14B8A570_RuntimeMethod_var);
		V_5 = L_23;
		BezierSplineU5BU5D_tD9962D7525021265D09A9C68F754780C16917F1C* L_24 = V_5;
		NullCheck(L_24);
		if ((((RuntimeArray *)L_24)->max_length))
		{
			goto IL_0081;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_25 = ___obj0;
		NullCheck(L_25);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_26 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_27 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_26, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_28 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  L_29;
		memset(&L_29, 0, sizeof(L_29));
		Bounds__ctor_m294E77A20EC1A3E96985FE1A925CB271D1B5266D((&L_29), L_27, L_28, /*hidden argument*/NULL);
		G_B8_0 = L_29;
		goto IL_008a;
	}

IL_0081:
	{
		BezierSplineU5BU5D_tD9962D7525021265D09A9C68F754780C16917F1C* L_30 = V_5;
		NullCheck(L_30);
		int32_t L_31 = 0;
		BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * L_32 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		NullCheck(L_32);
		Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  L_33 = BezierSpline_GetBounds_m0AA9FA3C3BC4BD110D1521173CC24A85095600D1(L_32, /*hidden argument*/NULL);
		G_B8_0 = L_33;
	}

IL_008a:
	{
		V_6 = G_B8_0;
		BezierSplineU5BU5D_tD9962D7525021265D09A9C68F754780C16917F1C* L_34 = V_5;
		if (!L_34)
		{
			goto IL_00bb;
		}
	}
	{
		BezierSplineU5BU5D_tD9962D7525021265D09A9C68F754780C16917F1C* L_35 = V_5;
		NullCheck(L_35);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_35)->max_length))))) <= ((int32_t)1)))
		{
			goto IL_00bb;
		}
	}
	{
		V_7 = 1;
		goto IL_00b3;
	}

IL_009c:
	{
		BezierSplineU5BU5D_tD9962D7525021265D09A9C68F754780C16917F1C* L_36 = V_5;
		int32_t L_37 = V_7;
		NullCheck(L_36);
		int32_t L_38 = L_37;
		BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * L_39 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		NullCheck(L_39);
		Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  L_40 = BezierSpline_GetBounds_m0AA9FA3C3BC4BD110D1521173CC24A85095600D1(L_39, /*hidden argument*/NULL);
		Bounds_Encapsulate_m394E7F823ADE56B97E6723B645C9458F1ADF0089((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&V_6), L_40, /*hidden argument*/NULL);
		int32_t L_41 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_41, (int32_t)1));
	}

IL_00b3:
	{
		int32_t L_42 = V_7;
		BezierSplineU5BU5D_tD9962D7525021265D09A9C68F754780C16917F1C* L_43 = V_5;
		NullCheck(L_43);
		if ((((int32_t)L_42) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_43)->max_length)))))))
		{
			goto IL_009c;
		}
	}

IL_00bb:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_44 = ___obj0;
		NullCheck(L_44);
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_45 = GameObject_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_m8BA044694988F70856600001893D4CB591BD5B29(L_44, /*hidden argument*/GameObject_GetComponentsInChildren_TisRenderer_t0556D67DD582620D1F495627EDE30D03284151F4_m8BA044694988F70856600001893D4CB591BD5B29_RuntimeMethod_var);
		V_2 = L_45;
		V_3 = 0;
		goto IL_00dd;
	}

IL_00c6:
	{
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_46 = V_2;
		int32_t L_47 = V_3;
		NullCheck(L_46);
		int32_t L_48 = L_47;
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		V_8 = L_49;
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_50 = V_8;
		NullCheck(L_50);
		Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  L_51 = Renderer_get_bounds_mB29E41E26DD95939C09F3EC67F5B2793A438BDB5(L_50, /*hidden argument*/NULL);
		Bounds_Encapsulate_m394E7F823ADE56B97E6723B645C9458F1ADF0089((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&V_6), L_51, /*hidden argument*/NULL);
		int32_t L_52 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_52, (int32_t)1));
	}

IL_00dd:
	{
		int32_t L_53 = V_3;
		RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* L_54 = V_2;
		NullCheck(L_54);
		if ((((int32_t)L_53) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_54)->max_length)))))))
		{
			goto IL_00c6;
		}
	}
	{
		Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  L_55 = V_6;
		return L_55;
	}
}
// System.Collections.Generic.List`1<UnityEngine.Vector2> Kolibri2d.Utils::GetUVsFromPointsAndBounds(System.Collections.Generic.List`1<UnityEngine.Vector3>,UnityEngine.Bounds)
extern "C" IL2CPP_METHOD_ATTR List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * Utils_GetUVsFromPointsAndBounds_mA2F9A9B0BF41EE9854F6E9C53B8F58CAC6BD31E7 (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___points0, Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  ___bounds1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_GetUVsFromPointsAndBounds_mA2F9A9B0BF41EE9854F6E9C53B8F58CAC6BD31E7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_1;
	memset(&V_1, 0, sizeof(V_1));
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * V_2 = NULL;
	Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = Bounds_get_center_m4FB6E99F0533EE2D432988B08474D6DC9B8B744B((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds1), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Bounds_get_size_m0739F2686AE2D3416A33AEF892653091347FD4A6((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Vector3_op_Division_mDF34F1CC445981B4D1137765BC6277419E561624(L_1, (2.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_0, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_4 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = Bounds_get_center_m4FB6E99F0533EE2D432988B08474D6DC9B8B744B((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds1), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Bounds_get_size_m0739F2686AE2D3416A33AEF892653091347FD4A6((Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&___bounds1), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Vector3_op_Division_mDF34F1CC445981B4D1137765BC6277419E561624(L_6, (2.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_5, L_7, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_9 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_8, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_10 = V_0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_11 = Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0(L_9, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_12 = ___points0;
		NullCheck(L_12);
		int32_t L_13 = List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3(L_12, /*hidden argument*/List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_RuntimeMethod_var);
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_14 = (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *)il2cpp_codegen_object_new(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_il2cpp_TypeInfo_var);
		List_1__ctor_m6DB64BAFEC79776AB013124B548E074CAA2FAD95(L_14, L_13, /*hidden argument*/List_1__ctor_m6DB64BAFEC79776AB013124B548E074CAA2FAD95_RuntimeMethod_var);
		V_2 = L_14;
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_15 = ___points0;
		NullCheck(L_15);
		Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F  L_16 = List_1_GetEnumerator_mA6AD60947F39CE54DA7D237D278C8948D5B2C331(L_15, /*hidden argument*/List_1_GetEnumerator_mA6AD60947F39CE54DA7D237D278C8948D5B2C331_RuntimeMethod_var);
		V_3 = L_16;
	}

IL_005f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_009f;
		}

IL_0061:
		{
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_17 = Enumerator_get_Current_mAB079301955D01523AB6CD62D400632D218B265D((Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F *)(&V_3), /*hidden argument*/Enumerator_get_Current_mAB079301955D01523AB6CD62D400632D218B265D_RuntimeMethod_var);
			V_4 = L_17;
			List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_18 = V_2;
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_19 = V_4;
			float L_20 = L_19.get_x_2();
			Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_21 = V_0;
			float L_22 = L_21.get_x_0();
			Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_23 = V_1;
			float L_24 = L_23.get_x_0();
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_25 = V_4;
			float L_26 = L_25.get_y_3();
			Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_27 = V_0;
			float L_28 = L_27.get_y_1();
			Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_29 = V_1;
			float L_30 = L_29.get_y_1();
			Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_31;
			memset(&L_31, 0, sizeof(L_31));
			Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_31), ((float)((float)((float)il2cpp_codegen_subtract((float)L_20, (float)L_22))/(float)L_24)), ((float)((float)((float)il2cpp_codegen_subtract((float)L_26, (float)L_28))/(float)L_30)), /*hidden argument*/NULL);
			NullCheck(L_18);
			List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857(L_18, L_31, /*hidden argument*/List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857_RuntimeMethod_var);
		}

IL_009f:
		{
			bool L_32 = Enumerator_MoveNext_mFB1EBCAC4E921F22042B0BDB6B954B7A09589454((Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F *)(&V_3), /*hidden argument*/Enumerator_MoveNext_mFB1EBCAC4E921F22042B0BDB6B954B7A09589454_RuntimeMethod_var);
			if (L_32)
			{
				goto IL_0061;
			}
		}

IL_00a8:
		{
			IL2CPP_LEAVE(0xB8, FINALLY_00aa);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00aa;
	}

FINALLY_00aa:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m640FFEF2E9A18C15D423069973168AD40E10FBFB((Enumerator_tB77EBE584C61354E0222C9BCA686AB5B98BA7F0F *)(&V_3), /*hidden argument*/Enumerator_Dispose_m640FFEF2E9A18C15D423069973168AD40E10FBFB_RuntimeMethod_var);
		IL2CPP_END_FINALLY(170)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(170)
	{
		IL2CPP_JUMP_TBL(0xB8, IL_00b8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00b8:
	{
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_33 = V_2;
		return L_33;
	}
}
// UnityEngine.Vector2[] Kolibri2d.Utils::GetNormalsFromPositions(System.Collections.Generic.List`1<UnityEngine.Vector3>&,System.Boolean,System.Int32)
extern "C" IL2CPP_METHOD_ATTR Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* Utils_GetNormalsFromPositions_m2D293F791AEB966D5D2530D1C05CD9416E668041 (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** ___points0, bool ___loop1, int32_t ___smoothLevel2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_GetNormalsFromPositions_m2D293F791AEB966D5D2530D1C05CD9416E668041_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* V_1 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_6;
	memset(&V_6, 0, sizeof(V_6));
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** L_0 = ___points0;
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_1 = *((List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 **)L_0);
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3(L_1, /*hidden argument*/List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_RuntimeMethod_var);
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1));
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** L_3 = ___points0;
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_4 = *((List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 **)L_3);
		NullCheck(L_4);
		int32_t L_5 = List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3(L_4, /*hidden argument*/List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_RuntimeMethod_var);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_6 = (Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6*)SZArrayNew(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6_il2cpp_TypeInfo_var, (uint32_t)L_5);
		V_1 = L_6;
		V_4 = 0;
		goto IL_0067;
	}

IL_001c:
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** L_7 = ___points0;
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_8 = *((List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 **)L_7);
		int32_t L_9 = V_4;
		NullCheck(L_8);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_8, ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1)), /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** L_11 = ___points0;
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_12 = *((List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 **)L_11);
		int32_t L_13 = V_4;
		NullCheck(L_12);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_14 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_12, L_13, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_15 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_10, L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		Vector3_Normalize_m174460238EC6322B9095A378AA8624B1DD9000F3((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_2), /*hidden argument*/NULL);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_16 = V_1;
		int32_t L_17 = V_4;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_18 = V_2;
		float L_19 = L_18.get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_20 = V_2;
		float L_21 = L_20.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_22), ((-L_19)), L_21, (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_23 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_22, /*hidden argument*/NULL);
		NullCheck(L_16);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D )L_23);
		int32_t L_24 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_24, (int32_t)1));
	}

IL_0067:
	{
		int32_t L_25 = V_4;
		int32_t L_26 = V_0;
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_001c;
		}
	}
	{
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_27 = V_1;
		int32_t L_28 = V_0;
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_29 = V_1;
		int32_t L_30 = V_0;
		NullCheck(L_29);
		int32_t L_31 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_30, (int32_t)1));
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_32 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		NullCheck(L_27);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(L_28), (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D )L_32);
		int32_t L_33 = ___smoothLevel2;
		if ((((int32_t)L_33) > ((int32_t)0)))
		{
			goto IL_0082;
		}
	}
	{
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_34 = V_1;
		return L_34;
	}

IL_0082:
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** L_35 = ___points0;
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_36 = *((List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 **)L_35);
		NullCheck(L_36);
		int32_t L_37 = List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3(L_36, /*hidden argument*/List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_RuntimeMethod_var);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_38 = (Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6*)SZArrayNew(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6_il2cpp_TypeInfo_var, (uint32_t)L_37);
		V_3 = L_38;
		V_5 = 1;
		goto IL_00c2;
	}

IL_0094:
	{
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_39 = V_3;
		int32_t L_40 = V_5;
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_41 = V_1;
		int32_t L_42 = V_5;
		NullCheck(L_41);
		int32_t L_43 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_42, (int32_t)1));
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_45 = V_1;
		int32_t L_46 = V_5;
		NullCheck(L_45);
		int32_t L_47 = L_46;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_49 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_44, L_48, /*hidden argument*/NULL);
		V_6 = L_49;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_50 = Vector2_get_normalized_m058E75C38C6FC66E178D7C8EF1B6298DE8F0E14B((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)(&V_6), /*hidden argument*/NULL);
		NullCheck(L_39);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(L_40), (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D )L_50);
		int32_t L_51 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_51, (int32_t)1));
	}

IL_00c2:
	{
		int32_t L_52 = V_5;
		int32_t L_53 = V_0;
		if ((((int32_t)L_52) < ((int32_t)L_53)))
		{
			goto IL_0094;
		}
	}
	{
		bool L_54 = ___loop1;
		if (!L_54)
		{
			goto IL_00ff;
		}
	}
	{
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_55 = V_3;
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_56 = V_1;
		NullCheck(L_56);
		int32_t L_57 = 0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_58 = (L_56)->GetAt(static_cast<il2cpp_array_size_t>(L_57));
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_59 = V_1;
		int32_t L_60 = V_0;
		NullCheck(L_59);
		int32_t L_61 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_60, (int32_t)1));
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_62 = (L_59)->GetAt(static_cast<il2cpp_array_size_t>(L_61));
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_63 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_58, L_62, /*hidden argument*/NULL);
		V_6 = L_63;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_64 = Vector2_get_normalized_m058E75C38C6FC66E178D7C8EF1B6298DE8F0E14B((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)(&V_6), /*hidden argument*/NULL);
		NullCheck(L_55);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(0), (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D )L_64);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_65 = V_3;
		int32_t L_66 = V_0;
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_67 = V_3;
		NullCheck(L_67);
		int32_t L_68 = 0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_69 = (L_67)->GetAt(static_cast<il2cpp_array_size_t>(L_68));
		NullCheck(L_65);
		(L_65)->SetAt(static_cast<il2cpp_array_size_t>(L_66), (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D )L_69);
		goto IL_011b;
	}

IL_00ff:
	{
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_70 = V_3;
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_71 = V_1;
		NullCheck(L_71);
		int32_t L_72 = 0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_73 = (L_71)->GetAt(static_cast<il2cpp_array_size_t>(L_72));
		NullCheck(L_70);
		(L_70)->SetAt(static_cast<il2cpp_array_size_t>(0), (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D )L_73);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_74 = V_3;
		int32_t L_75 = V_0;
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_76 = V_1;
		int32_t L_77 = V_0;
		NullCheck(L_76);
		int32_t L_78 = L_77;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_79 = (L_76)->GetAt(static_cast<il2cpp_array_size_t>(L_78));
		NullCheck(L_74);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(L_75), (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D )L_79);
	}

IL_011b:
	{
		V_7 = 1;
		goto IL_01fb;
	}

IL_0123:
	{
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_80 = V_3;
		V_1 = L_80;
		V_8 = 1;
		goto IL_015a;
	}

IL_012a:
	{
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_81 = V_3;
		int32_t L_82 = V_8;
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_83 = V_1;
		int32_t L_84 = V_8;
		NullCheck(L_83);
		int32_t L_85 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_84, (int32_t)1));
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_86 = (L_83)->GetAt(static_cast<il2cpp_array_size_t>(L_85));
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_87 = V_1;
		int32_t L_88 = V_8;
		NullCheck(L_87);
		int32_t L_89 = ((int32_t)il2cpp_codegen_add((int32_t)L_88, (int32_t)1));
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_90 = (L_87)->GetAt(static_cast<il2cpp_array_size_t>(L_89));
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_91 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_86, L_90, /*hidden argument*/NULL);
		V_6 = L_91;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_92 = Vector2_get_normalized_m058E75C38C6FC66E178D7C8EF1B6298DE8F0E14B((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)(&V_6), /*hidden argument*/NULL);
		NullCheck(L_81);
		(L_81)->SetAt(static_cast<il2cpp_array_size_t>(L_82), (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D )L_92);
		int32_t L_93 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_93, (int32_t)1));
	}

IL_015a:
	{
		int32_t L_94 = V_8;
		int32_t L_95 = V_0;
		if ((((int32_t)L_94) < ((int32_t)L_95)))
		{
			goto IL_012a;
		}
	}
	{
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_96 = V_3;
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_97 = V_1;
		NullCheck(L_97);
		int32_t L_98 = 0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_99 = (L_97)->GetAt(static_cast<il2cpp_array_size_t>(L_98));
		NullCheck(L_96);
		(L_96)->SetAt(static_cast<il2cpp_array_size_t>(0), (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D )L_99);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_100 = V_3;
		int32_t L_101 = V_0;
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_102 = V_1;
		int32_t L_103 = V_0;
		NullCheck(L_102);
		int32_t L_104 = L_103;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_105 = (L_102)->GetAt(static_cast<il2cpp_array_size_t>(L_104));
		NullCheck(L_100);
		(L_100)->SetAt(static_cast<il2cpp_array_size_t>(L_101), (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D )L_105);
		bool L_106 = ___loop1;
		if (!L_106)
		{
			goto IL_01f5;
		}
	}
	{
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_107 = V_3;
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_108 = V_1;
		int32_t L_109 = V_0;
		NullCheck(L_108);
		int32_t L_110 = L_109;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_111 = (L_108)->GetAt(static_cast<il2cpp_array_size_t>(L_110));
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_112 = V_1;
		NullCheck(L_112);
		int32_t L_113 = 1;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_114 = (L_112)->GetAt(static_cast<il2cpp_array_size_t>(L_113));
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_115 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_111, L_114, /*hidden argument*/NULL);
		V_6 = L_115;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_116 = Vector2_get_normalized_m058E75C38C6FC66E178D7C8EF1B6298DE8F0E14B((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)(&V_6), /*hidden argument*/NULL);
		NullCheck(L_107);
		(L_107)->SetAt(static_cast<il2cpp_array_size_t>(0), (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D )L_116);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_117 = V_3;
		int32_t L_118 = V_0;
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_119 = V_1;
		int32_t L_120 = V_0;
		NullCheck(L_119);
		int32_t L_121 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_120, (int32_t)1));
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_122 = (L_119)->GetAt(static_cast<il2cpp_array_size_t>(L_121));
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_123 = V_1;
		NullCheck(L_123);
		int32_t L_124 = 0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_125 = (L_123)->GetAt(static_cast<il2cpp_array_size_t>(L_124));
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_126 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_122, L_125, /*hidden argument*/NULL);
		V_6 = L_126;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_127 = Vector2_get_normalized_m058E75C38C6FC66E178D7C8EF1B6298DE8F0E14B((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)(&V_6), /*hidden argument*/NULL);
		NullCheck(L_117);
		(L_117)->SetAt(static_cast<il2cpp_array_size_t>(L_118), (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D )L_127);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_128 = V_3;
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_129 = V_3;
		int32_t L_130 = V_0;
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_131 = V_3;
		NullCheck(L_131);
		int32_t L_132 = 0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_133 = (L_131)->GetAt(static_cast<il2cpp_array_size_t>(L_132));
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_134 = V_3;
		int32_t L_135 = V_0;
		NullCheck(L_134);
		int32_t L_136 = L_135;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_137 = (L_134)->GetAt(static_cast<il2cpp_array_size_t>(L_136));
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_138 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_133, L_137, /*hidden argument*/NULL);
		V_6 = L_138;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_139 = Vector2_get_normalized_m058E75C38C6FC66E178D7C8EF1B6298DE8F0E14B((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)(&V_6), /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_140 = L_139;
		V_6 = L_140;
		NullCheck(L_129);
		(L_129)->SetAt(static_cast<il2cpp_array_size_t>(L_130), (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D )L_140);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_141 = V_6;
		NullCheck(L_128);
		(L_128)->SetAt(static_cast<il2cpp_array_size_t>(0), (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D )L_141);
	}

IL_01f5:
	{
		int32_t L_142 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_142, (int32_t)1));
	}

IL_01fb:
	{
		int32_t L_143 = V_7;
		int32_t L_144 = ___smoothLevel2;
		if ((((int32_t)L_143) < ((int32_t)L_144)))
		{
			goto IL_0123;
		}
	}
	{
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_145 = V_3;
		return L_145;
	}
}
// System.Single Kolibri2d.Utils::Cross(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR float Utils_Cross_m0B48E0950C9AF1B9708E346B5D1B37D728958BD9 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___v10, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___v21, const RuntimeMethod* method)
{
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___v10;
		float L_1 = L_0.get_x_0();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_2 = ___v21;
		float L_3 = L_2.get_y_1();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_4 = ___v10;
		float L_5 = L_4.get_y_1();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_6 = ___v21;
		float L_7 = L_6.get_x_0();
		return ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), (float)((float)il2cpp_codegen_multiply((float)L_5, (float)L_7))));
	}
}
// UnityEngine.Vector2 Kolibri2d.Utils::Cross(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Utils_Cross_m652596EE95F4A01033A3DD383EE40C39CB435A45 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___v0, const RuntimeMethod* method)
{
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___v0;
		float L_1 = L_0.get_y_1();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_2 = ___v0;
		float L_3 = L_2.get_x_0();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_4), L_1, ((-L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 Kolibri2d.Utils::Mult(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Utils_Mult_mAF1AC9C9C45260976167E0D519D6CEFD1C875AEB (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___a0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___b1, const RuntimeMethod* method)
{
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___a0;
		float L_1 = L_0.get_x_0();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_2 = ___b1;
		float L_3 = L_2.get_x_0();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_4 = ___a0;
		float L_5 = L_4.get_y_1();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_6 = ___b1;
		float L_7 = L_6.get_y_1();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_8), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), ((float)il2cpp_codegen_multiply((float)L_5, (float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean Kolibri2d.Utils::IsZero(System.Double)
extern "C" IL2CPP_METHOD_ATTR bool Utils_IsZero_m4B8A254B0F87897812D1DA9263A8CE9BFD9A8FD9 (double ___d0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_IsZero_m4B8A254B0F87897812D1DA9263A8CE9BFD9A8FD9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		double L_0 = ___d0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_1 = fabsf((((float)((float)L_0))));
		return (bool)((((double)(((double)((double)L_1)))) < ((double)(1.0E-10)))? 1 : 0);
	}
}
// System.Boolean Kolibri2d.Utils::LineSegementsIntersect(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2&,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR bool Utils_LineSegementsIntersect_m201115D61811BDC929D3B3B5168662F6CBCF4FBA (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___p0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___p21, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___q2, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___q23, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * ___intersection4, bool ___considerCollinearOverlapAsIntersect5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_LineSegementsIntersect_m201115D61811BDC929D3B3B5168662F6CBCF4FBA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_10;
	memset(&V_10, 0, sizeof(V_10));
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_0 = ___intersection4;
		il2cpp_codegen_initobj(L_0, sizeof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D ));
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_1 = ___p21;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_2 = ___p0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_3 = Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_4 = ___q23;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_5 = ___q2;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_6 = Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0(L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_7 = V_0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_8 = V_1;
		float L_9 = Utils_Cross_m0B48E0950C9AF1B9708E346B5D1B37D728958BD9(L_7, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_10 = ___q2;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_11 = ___p0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_12 = Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0(L_10, L_11, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_13 = V_0;
		float L_14 = Utils_Cross_m0B48E0950C9AF1B9708E346B5D1B37D728958BD9(L_12, L_13, /*hidden argument*/NULL);
		V_3 = L_14;
		float L_15 = V_2;
		bool L_16 = Utils_IsZero_m4B8A254B0F87897812D1DA9263A8CE9BFD9A8FD9((((double)((double)L_15))), /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00bd;
		}
	}
	{
		float L_17 = V_3;
		bool L_18 = Utils_IsZero_m4B8A254B0F87897812D1DA9263A8CE9BFD9A8FD9((((double)((double)L_17))), /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00bd;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_19 = ___p0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_20 = ___q2;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_21 = Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0(L_19, L_20, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_22 = ___q2;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_23 = ___p0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_24 = Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0(L_22, L_23, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_25 = V_0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_26 = V_0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_27 = Utils_Mult_mAF1AC9C9C45260976167E0D519D6CEFD1C875AEB(L_25, L_26, /*hidden argument*/NULL);
		V_10 = L_27;
		float L_28 = Vector2_get_magnitude_m66097AFDF9696BD3E88467D4398D4F82B8A4C7DF((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)(&V_10), /*hidden argument*/NULL);
		V_6 = L_28;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_29 = V_1;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_30 = V_1;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_31 = Utils_Mult_mAF1AC9C9C45260976167E0D519D6CEFD1C875AEB(L_29, L_30, /*hidden argument*/NULL);
		V_10 = L_31;
		float L_32 = Vector2_get_magnitude_m66097AFDF9696BD3E88467D4398D4F82B8A4C7DF((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)(&V_10), /*hidden argument*/NULL);
		V_7 = L_32;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_33 = V_0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_34 = Utils_Mult_mAF1AC9C9C45260976167E0D519D6CEFD1C875AEB(L_24, L_33, /*hidden argument*/NULL);
		V_10 = L_34;
		float L_35 = Vector2_get_magnitude_m66097AFDF9696BD3E88467D4398D4F82B8A4C7DF((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)(&V_10), /*hidden argument*/NULL);
		V_8 = L_35;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_36 = V_1;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_37 = Utils_Mult_mAF1AC9C9C45260976167E0D519D6CEFD1C875AEB(L_21, L_36, /*hidden argument*/NULL);
		V_10 = L_37;
		float L_38 = Vector2_get_magnitude_m66097AFDF9696BD3E88467D4398D4F82B8A4C7DF((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)(&V_10), /*hidden argument*/NULL);
		V_9 = L_38;
		bool L_39 = ___considerCollinearOverlapAsIntersect5;
		if (!L_39)
		{
			goto IL_00bb;
		}
	}
	{
		float L_40 = V_8;
		if ((!(((float)(0.0f)) <= ((float)L_40))))
		{
			goto IL_00aa;
		}
	}
	{
		float L_41 = V_8;
		float L_42 = V_6;
		if ((((float)L_41) <= ((float)L_42)))
		{
			goto IL_00b9;
		}
	}

IL_00aa:
	{
		float L_43 = V_9;
		if ((!(((float)(0.0f)) <= ((float)L_43))))
		{
			goto IL_00bb;
		}
	}
	{
		float L_44 = V_9;
		float L_45 = V_7;
		if ((!(((float)L_44) <= ((float)L_45))))
		{
			goto IL_00bb;
		}
	}

IL_00b9:
	{
		return (bool)1;
	}

IL_00bb:
	{
		return (bool)0;
	}

IL_00bd:
	{
		float L_46 = V_2;
		bool L_47 = Utils_IsZero_m4B8A254B0F87897812D1DA9263A8CE9BFD9A8FD9((((double)((double)L_46))), /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_00d1;
		}
	}
	{
		float L_48 = V_3;
		bool L_49 = Utils_IsZero_m4B8A254B0F87897812D1DA9263A8CE9BFD9A8FD9((((double)((double)L_48))), /*hidden argument*/NULL);
		if (L_49)
		{
			goto IL_00d1;
		}
	}
	{
		return (bool)0;
	}

IL_00d1:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_50 = ___q2;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_51 = ___p0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_52 = Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0(L_50, L_51, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_53 = V_1;
		float L_54 = Utils_Cross_m0B48E0950C9AF1B9708E346B5D1B37D728958BD9(L_52, L_53, /*hidden argument*/NULL);
		float L_55 = V_2;
		V_4 = ((float)((float)L_54/(float)L_55));
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_56 = ___q2;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_57 = ___p0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_58 = Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0(L_56, L_57, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_59 = V_0;
		float L_60 = Utils_Cross_m0B48E0950C9AF1B9708E346B5D1B37D728958BD9(L_58, L_59, /*hidden argument*/NULL);
		float L_61 = V_2;
		V_5 = ((float)((float)L_60/(float)L_61));
		float L_62 = V_2;
		bool L_63 = Utils_IsZero_m4B8A254B0F87897812D1DA9263A8CE9BFD9A8FD9((((double)((double)L_62))), /*hidden argument*/NULL);
		if (L_63)
		{
			goto IL_0137;
		}
	}
	{
		float L_64 = V_4;
		if ((!(((float)(0.0f)) <= ((float)L_64))))
		{
			goto IL_0137;
		}
	}
	{
		float L_65 = V_4;
		if ((!(((float)L_65) <= ((float)(1.0f)))))
		{
			goto IL_0137;
		}
	}
	{
		float L_66 = V_5;
		if ((!(((float)(0.0f)) <= ((float)L_66))))
		{
			goto IL_0137;
		}
	}
	{
		float L_67 = V_5;
		if ((!(((float)L_67) <= ((float)(1.0f)))))
		{
			goto IL_0137;
		}
	}
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_68 = ___intersection4;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_69 = ___p0;
		float L_70 = V_4;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_71 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_72 = Vector2_op_Multiply_m2E30A54E315810911DFC2E25C700757A68AC1F38(L_70, L_71, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_73 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_69, L_72, /*hidden argument*/NULL);
		*(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)L_68 = L_73;
		return (bool)1;
	}

IL_0137:
	{
		return (bool)0;
	}
}
// System.Boolean Kolibri2d.Utils::ArePointsClockwise(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C" IL2CPP_METHOD_ATTR bool Utils_ArePointsClockwise_m377B93EB2228C4F2B0E5D84147DE0F032A49C42D (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___points0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_ArePointsClockwise_m377B93EB2228C4F2B0E5D84147DE0F032A49C42D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	double V_0 = 0.0;
	double V_1 = 0.0;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	{
		V_0 = (0.0);
		V_1 = (0.0);
		V_4 = 0;
		goto IL_0054;
	}

IL_0019:
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_0 = ___points0;
		int32_t L_1 = V_4;
		NullCheck(L_0);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_0, L_1, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		V_2 = L_2;
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_3 = ___points0;
		int32_t L_4 = V_4;
		NullCheck(L_3);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_3, ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1)), /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		V_3 = L_5;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = V_3;
		float L_7 = L_6.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = V_2;
		float L_9 = L_8.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = V_3;
		float L_11 = L_10.get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = V_2;
		float L_13 = L_12.get_y_3();
		V_1 = (((double)((double)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_7, (float)L_9)), (float)((float)il2cpp_codegen_add((float)L_11, (float)L_13)))))));
		double L_14 = V_0;
		double L_15 = V_1;
		V_0 = ((double)il2cpp_codegen_add((double)L_14, (double)L_15));
		int32_t L_16 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)1));
	}

IL_0054:
	{
		int32_t L_17 = V_4;
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_18 = ___points0;
		NullCheck(L_18);
		int32_t L_19 = List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3(L_18, /*hidden argument*/List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_RuntimeMethod_var);
		if ((((int32_t)L_17) < ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_19, (int32_t)1)))))
		{
			goto IL_0019;
		}
	}
	{
		double L_20 = V_0;
		return (bool)((((double)L_20) > ((double)(0.0)))? 1 : 0);
	}
}
// System.Single Kolibri2d.Utils::GetAngleFromDir(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR float Utils_GetAngleFromDir_m62A41F885D74A5155D0E4684783672982D7CB8B3 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___dir0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_GetAngleFromDir_m62A41F885D74A5155D0E4684783672982D7CB8B3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___dir0;
		float L_1 = L_0.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ___dir0;
		float L_3 = L_2.get_y_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_4 = atan2f(L_1, L_3);
		return ((float)il2cpp_codegen_multiply((float)L_4, (float)(57.29578f)));
	}
}
// UnityEngine.Vector3 Kolibri2d.Utils::GetDirFromPoints(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Utils_GetDirFromPoints_mE9D1FC9BF146E38D7A53FD21E2A8C34F4B08F16B (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___p10, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___p21, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_GetDirFromPoints_mE9D1FC9BF146E38D7A53FD21E2A8C34F4B08F16B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___p21;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___p10;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Texture2D Kolibri2d.Utils::CropTexture2D(UnityEngine.Texture,UnityEngine.Rect)
extern "C" IL2CPP_METHOD_ATTR Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * Utils_CropTexture2D_m9A361D3C4C7CF74DBF59F6BB5050268B452ADE94 (Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___tex0, Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___rect1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_CropTexture2D_m9A361D3C4C7CF74DBF59F6BB5050268B452ADE94_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * V_1 = NULL;
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * V_2 = NULL;
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		float L_0 = Rect_get_width_m54FF69FC2C086E2DC349ED091FD0D6576BFB1484((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&___rect1), /*hidden argument*/NULL);
		float L_1 = Rect_get_height_m088C36990E0A255C5D7DCE36575DCE23ABB364B5((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&___rect1), /*hidden argument*/NULL);
		V_0 = (((int32_t)((int32_t)L_1)));
		int32_t L_2 = V_0;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_3 = (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)il2cpp_codegen_object_new(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var);
		Texture2D__ctor_m22561E039BC96019757E6B2427BE09734AE2C44A(L_3, (((int32_t)((int32_t)L_0))), L_2, 4, (bool)0, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_4 = RenderTexture_get_active_m670416A37BF4239DE5A55F6138CAA1FEEF184957(/*hidden argument*/NULL);
		V_1 = L_4;
		Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * L_5 = ___tex0;
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_5);
		Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * L_7 = ___tex0;
		NullCheck(L_7);
		int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_7);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_9 = (RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 *)il2cpp_codegen_object_new(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6_il2cpp_TypeInfo_var);
		RenderTexture__ctor_mB54A3ABBD56D38AB762D0AB8B789E2771BC42A7D(L_9, L_6, L_8, ((int32_t)32), /*hidden argument*/NULL);
		V_2 = L_9;
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_10 = ___rect1;
		Rect__ctor_m53B98CABA163EB899C81CB3694EB9E2AC19D8C90((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_3), L_10, /*hidden argument*/NULL);
		Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * L_11 = ___tex0;
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_11);
		int32_t L_13 = V_0;
		float L_14 = Rect_get_y_m53E3E4F62D9840FBEA751A66293038F1F5D1D45C((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&___rect1), /*hidden argument*/NULL);
		Rect_set_y_mCFDB9BD77334EF9CD896F64BE63C755777D7CCD5((Rect_t35B976DE901B5423C11705E156938EA27AB402CE *)(&V_3), ((float)il2cpp_codegen_subtract((float)(((float)((float)L_12))), (float)((float)il2cpp_codegen_add((float)(((float)((float)L_13))), (float)L_14)))), /*hidden argument*/NULL);
		Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * L_15 = ___tex0;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_16 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t6FB7A5D4561F3AB3C34BF334BB0BD8061BE763B1_il2cpp_TypeInfo_var);
		Graphics_Blit_mB042EC04307A5617038DA4210DE7BA4B3E529113(L_15, L_16, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_17 = V_2;
		RenderTexture_set_active_m992E25C701DEFC8042B31022EA45F02A787A84F1(L_17, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_18 = L_3;
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_19 = V_3;
		NullCheck(L_18);
		Texture2D_ReadPixels_m1ED1C11E41D0ACC8CFCABBD25946CF0BD16D4F61(L_18, L_19, 0, 0, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_20 = L_18;
		NullCheck(L_20);
		Texture2D_Apply_m0F3B4A4B1B89E44E2AF60ABDEFAA18D93735B5CA(L_20, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_21 = V_1;
		RenderTexture_set_active_m992E25C701DEFC8042B31022EA45F02A787A84F1(L_21, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_22 = L_20;
		Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * L_23 = ___tex0;
		NullCheck(L_23);
		int32_t L_24 = Texture_get_filterMode_m4A05E0414655866D27811376709B3A8C584A2579(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		Texture_set_filterMode_mB9AC927A527EFE95771B9B438E2CFB9EDA84AF01(L_22, L_24, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_25 = L_22;
		NullCheck(L_25);
		Texture_set_wrapMode_m85E9A995D5947B59FE13A7311E891F3DEDEBBCEC(L_25, 0, /*hidden argument*/NULL);
		return L_25;
	}
}
// System.Void Kolibri2d.Utils::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Utils__ctor_mEEB8D4A699A80375E0FA6DFDA857030C30914260 (Utils_t6B1F49395E2728FC7087CA82F420C8183CECBD71 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Kolibri2d.Utils/MeshHelper2::InitArrays(UnityEngine.Mesh)
extern "C" IL2CPP_METHOD_ATTR void MeshHelper2_InitArrays_mDBEB2310C61A2DB7A2EDC56A961F92F7CDCF1681 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MeshHelper2_InitArrays_mDBEB2310C61A2DB7A2EDC56A961F92F7CDCF1681_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_0 = ___mesh0;
		NullCheck(L_0);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_1 = Mesh_get_vertices_m7D07DC0F071C142B87F675B148FC0F7A243238B9(L_0, /*hidden argument*/NULL);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_2 = (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *)il2cpp_codegen_object_new(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_il2cpp_TypeInfo_var);
		List_1__ctor_m0E7CD0EBE7EA0CCD1DDA59B1F34C56CDB1F79827(L_2, (RuntimeObject*)(RuntimeObject*)L_1, /*hidden argument*/List_1__ctor_m0E7CD0EBE7EA0CCD1DDA59B1F34C56CDB1F79827_RuntimeMethod_var);
		((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->set_vertices_0(L_2);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_3 = ___mesh0;
		NullCheck(L_3);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_4 = Mesh_get_normals_m3CE4668899836CBD17C3F85EB24261CBCEB3EABB(L_3, /*hidden argument*/NULL);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_5 = (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *)il2cpp_codegen_object_new(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_il2cpp_TypeInfo_var);
		List_1__ctor_m0E7CD0EBE7EA0CCD1DDA59B1F34C56CDB1F79827(L_5, (RuntimeObject*)(RuntimeObject*)L_4, /*hidden argument*/List_1__ctor_m0E7CD0EBE7EA0CCD1DDA59B1F34C56CDB1F79827_RuntimeMethod_var);
		((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->set_normals_1(L_5);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_6 = ___mesh0;
		NullCheck(L_6);
		ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* L_7 = Mesh_get_colors_m16A1999334A7013DD5A1FF619E51983B4BFF7848(L_6, /*hidden argument*/NULL);
		List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * L_8 = (List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E *)il2cpp_codegen_object_new(List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E_il2cpp_TypeInfo_var);
		List_1__ctor_m6A5DF174C63F24D00FC3CCA142FF30E9FE3B0B84(L_8, (RuntimeObject*)(RuntimeObject*)L_7, /*hidden argument*/List_1__ctor_m6A5DF174C63F24D00FC3CCA142FF30E9FE3B0B84_RuntimeMethod_var);
		((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->set_colors_2(L_8);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_9 = ___mesh0;
		NullCheck(L_9);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_10 = Mesh_get_uv_m0EBA5CA4644C9D5F1B2125AF3FE3873EFC8A4616(L_9, /*hidden argument*/NULL);
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_11 = (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *)il2cpp_codegen_object_new(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_il2cpp_TypeInfo_var);
		List_1__ctor_mCAB49C9A39B98BC7B6D7CEDCA6AE260556381C09(L_11, (RuntimeObject*)(RuntimeObject*)L_10, /*hidden argument*/List_1__ctor_mCAB49C9A39B98BC7B6D7CEDCA6AE260556381C09_RuntimeMethod_var);
		((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->set_uv_3(L_11);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_12 = ___mesh0;
		NullCheck(L_12);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_13 = Mesh_get_uv2_m3E70D5DD7A5C6910A074A78296269EBF2CBAE97F(L_12, /*hidden argument*/NULL);
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_14 = (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *)il2cpp_codegen_object_new(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_il2cpp_TypeInfo_var);
		List_1__ctor_mCAB49C9A39B98BC7B6D7CEDCA6AE260556381C09(L_14, (RuntimeObject*)(RuntimeObject*)L_13, /*hidden argument*/List_1__ctor_mCAB49C9A39B98BC7B6D7CEDCA6AE260556381C09_RuntimeMethod_var);
		((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->set_uv1_4(L_14);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_15 = ___mesh0;
		NullCheck(L_15);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_16 = Mesh_get_uv2_m3E70D5DD7A5C6910A074A78296269EBF2CBAE97F(L_15, /*hidden argument*/NULL);
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_17 = (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *)il2cpp_codegen_object_new(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_il2cpp_TypeInfo_var);
		List_1__ctor_mCAB49C9A39B98BC7B6D7CEDCA6AE260556381C09(L_17, (RuntimeObject*)(RuntimeObject*)L_16, /*hidden argument*/List_1__ctor_mCAB49C9A39B98BC7B6D7CEDCA6AE260556381C09_RuntimeMethod_var);
		((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->set_uv2_5(L_17);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_18 = (List_1_t4A961510635950236678F1B3B2436ECCAF28713A *)il2cpp_codegen_object_new(List_1_t4A961510635950236678F1B3B2436ECCAF28713A_il2cpp_TypeInfo_var);
		List_1__ctor_m80FC80854B1AD97640F2B64DE9B250A3D8DAB145(L_18, /*hidden argument*/List_1__ctor_m80FC80854B1AD97640F2B64DE9B250A3D8DAB145_RuntimeMethod_var);
		((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->set_indices_6(L_18);
		return;
	}
}
// System.Void Kolibri2d.Utils/MeshHelper2::CleanUp()
extern "C" IL2CPP_METHOD_ATTR void MeshHelper2_CleanUp_mBC2EA65D0BD5F6AA5D87F7C6259B63330A5B62B9 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MeshHelper2_CleanUp_mBC2EA65D0BD5F6AA5D87F7C6259B63330A5B62B9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->set_vertices_0((List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *)NULL);
		((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->set_normals_1((List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *)NULL);
		((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->set_colors_2((List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E *)NULL);
		((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->set_uv_3((List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *)NULL);
		((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->set_uv1_4((List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *)NULL);
		((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->set_uv2_5((List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *)NULL);
		((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->set_indices_6((List_1_t4A961510635950236678F1B3B2436ECCAF28713A *)NULL);
		return;
	}
}
// System.Int32 Kolibri2d.Utils/MeshHelper2::GetNewVertex4(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t MeshHelper2_GetNewVertex4_m1B46BFC32053E23F3C9F90347AAF3EECD46B715F (int32_t ___i10, int32_t ___i21, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MeshHelper2_GetNewVertex4_m1B46BFC32053E23F3C9F90347AAF3EECD46B715F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	uint32_t V_1 = 0;
	uint32_t V_2 = 0;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_0 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_vertices_0();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3(L_0, /*hidden argument*/List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_RuntimeMethod_var);
		V_0 = L_1;
		int32_t L_2 = ___i10;
		int32_t L_3 = ___i21;
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_2<<(int32_t)((int32_t)16)))|(int32_t)L_3));
		int32_t L_4 = ___i21;
		int32_t L_5 = ___i10;
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)L_4<<(int32_t)((int32_t)16)))|(int32_t)L_5));
		Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F * L_6 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_newVectices_7();
		uint32_t L_7 = V_2;
		NullCheck(L_6);
		bool L_8 = Dictionary_2_ContainsKey_m230D107B4D3C342ECBC961100D82015C14534FCC(L_6, L_7, /*hidden argument*/Dictionary_2_ContainsKey_m230D107B4D3C342ECBC961100D82015C14534FCC_RuntimeMethod_var);
		if (!L_8)
		{
			goto IL_0032;
		}
	}
	{
		Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F * L_9 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_newVectices_7();
		uint32_t L_10 = V_2;
		NullCheck(L_9);
		int32_t L_11 = Dictionary_2_get_Item_mCECAC44EF2E9AE18023E7FDAD8CC268588065CE3(L_9, L_10, /*hidden argument*/Dictionary_2_get_Item_mCECAC44EF2E9AE18023E7FDAD8CC268588065CE3_RuntimeMethod_var);
		return L_11;
	}

IL_0032:
	{
		Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F * L_12 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_newVectices_7();
		uint32_t L_13 = V_1;
		NullCheck(L_12);
		bool L_14 = Dictionary_2_ContainsKey_m230D107B4D3C342ECBC961100D82015C14534FCC(L_12, L_13, /*hidden argument*/Dictionary_2_ContainsKey_m230D107B4D3C342ECBC961100D82015C14534FCC_RuntimeMethod_var);
		if (!L_14)
		{
			goto IL_004b;
		}
	}
	{
		Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F * L_15 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_newVectices_7();
		uint32_t L_16 = V_1;
		NullCheck(L_15);
		int32_t L_17 = Dictionary_2_get_Item_mCECAC44EF2E9AE18023E7FDAD8CC268588065CE3(L_15, L_16, /*hidden argument*/Dictionary_2_get_Item_mCECAC44EF2E9AE18023E7FDAD8CC268588065CE3_RuntimeMethod_var);
		return L_17;
	}

IL_004b:
	{
		Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F * L_18 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_newVectices_7();
		uint32_t L_19 = V_1;
		int32_t L_20 = V_0;
		NullCheck(L_18);
		Dictionary_2_Add_mDCE3CAF8CAFEBD13FB38A51C069F27896353ECB5(L_18, L_19, L_20, /*hidden argument*/Dictionary_2_Add_mDCE3CAF8CAFEBD13FB38A51C069F27896353ECB5_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_21 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_vertices_0();
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_22 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_vertices_0();
		int32_t L_23 = ___i10;
		NullCheck(L_22);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_24 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_22, L_23, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_25 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_vertices_0();
		int32_t L_26 = ___i21;
		NullCheck(L_25);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_27 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_25, L_26, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_28 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_24, L_27, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_29 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_28, (0.5f), /*hidden argument*/NULL);
		NullCheck(L_21);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_21, L_29, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_30 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_normals_1();
		NullCheck(L_30);
		int32_t L_31 = List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3(L_30, /*hidden argument*/List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_RuntimeMethod_var);
		if ((((int32_t)L_31) <= ((int32_t)0)))
		{
			goto IL_00c0;
		}
	}
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_32 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_normals_1();
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_33 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_normals_1();
		int32_t L_34 = ___i10;
		NullCheck(L_33);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_35 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_33, L_34, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_36 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_normals_1();
		int32_t L_37 = ___i21;
		NullCheck(L_36);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_38 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_36, L_37, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_39 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_35, L_38, /*hidden argument*/NULL);
		V_3 = L_39;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_40 = Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_3), /*hidden argument*/NULL);
		NullCheck(L_32);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_32, L_40, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
	}

IL_00c0:
	{
		List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * L_41 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_colors_2();
		NullCheck(L_41);
		int32_t L_42 = List_1_get_Count_m9E6BB40B113CEAFD4EA6A2BD7FCEB7259C20561D(L_41, /*hidden argument*/List_1_get_Count_m9E6BB40B113CEAFD4EA6A2BD7FCEB7259C20561D_RuntimeMethod_var);
		if ((((int32_t)L_42) <= ((int32_t)0)))
		{
			goto IL_00fc;
		}
	}
	{
		List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * L_43 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_colors_2();
		List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * L_44 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_colors_2();
		int32_t L_45 = ___i10;
		NullCheck(L_44);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_46 = List_1_get_Item_mB725E23AF0513F5A32B32BBC9C2471097DE235C4(L_44, L_45, /*hidden argument*/List_1_get_Item_mB725E23AF0513F5A32B32BBC9C2471097DE235C4_RuntimeMethod_var);
		List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * L_47 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_colors_2();
		int32_t L_48 = ___i21;
		NullCheck(L_47);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_49 = List_1_get_Item_mB725E23AF0513F5A32B32BBC9C2471097DE235C4(L_47, L_48, /*hidden argument*/List_1_get_Item_mB725E23AF0513F5A32B32BBC9C2471097DE235C4_RuntimeMethod_var);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_50 = Color_op_Addition_m26E4EEFAF21C0C0DC2FD93CE594E02F61DB1F7DF(L_46, L_49, /*hidden argument*/NULL);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_51 = Color_op_Multiply_mF36917AD6235221537542FD079817CAB06CB1934(L_50, (0.5f), /*hidden argument*/NULL);
		NullCheck(L_43);
		List_1_Add_m00EB6B0859FF6BCA9F89EF1FE572855EAA143F8F(L_43, L_51, /*hidden argument*/List_1_Add_m00EB6B0859FF6BCA9F89EF1FE572855EAA143F8F_RuntimeMethod_var);
	}

IL_00fc:
	{
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_52 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv_3();
		NullCheck(L_52);
		int32_t L_53 = List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7(L_52, /*hidden argument*/List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7_RuntimeMethod_var);
		if ((((int32_t)L_53) <= ((int32_t)0)))
		{
			goto IL_0138;
		}
	}
	{
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_54 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv_3();
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_55 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv_3();
		int32_t L_56 = ___i10;
		NullCheck(L_55);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_57 = List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B(L_55, L_56, /*hidden argument*/List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B_RuntimeMethod_var);
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_58 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv_3();
		int32_t L_59 = ___i21;
		NullCheck(L_58);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_60 = List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B(L_58, L_59, /*hidden argument*/List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_61 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_57, L_60, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_62 = Vector2_op_Multiply_m8A843A37F2F3199EBE99DC7BDABC1DC2EE01AF56(L_61, (0.5f), /*hidden argument*/NULL);
		NullCheck(L_54);
		List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857(L_54, L_62, /*hidden argument*/List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857_RuntimeMethod_var);
	}

IL_0138:
	{
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_63 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv1_4();
		NullCheck(L_63);
		int32_t L_64 = List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7(L_63, /*hidden argument*/List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7_RuntimeMethod_var);
		if ((((int32_t)L_64) <= ((int32_t)0)))
		{
			goto IL_0174;
		}
	}
	{
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_65 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv1_4();
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_66 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv1_4();
		int32_t L_67 = ___i10;
		NullCheck(L_66);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_68 = List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B(L_66, L_67, /*hidden argument*/List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B_RuntimeMethod_var);
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_69 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv1_4();
		int32_t L_70 = ___i21;
		NullCheck(L_69);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_71 = List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B(L_69, L_70, /*hidden argument*/List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_72 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_68, L_71, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_73 = Vector2_op_Multiply_m8A843A37F2F3199EBE99DC7BDABC1DC2EE01AF56(L_72, (0.5f), /*hidden argument*/NULL);
		NullCheck(L_65);
		List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857(L_65, L_73, /*hidden argument*/List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857_RuntimeMethod_var);
	}

IL_0174:
	{
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_74 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv2_5();
		NullCheck(L_74);
		int32_t L_75 = List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7(L_74, /*hidden argument*/List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7_RuntimeMethod_var);
		if ((((int32_t)L_75) <= ((int32_t)0)))
		{
			goto IL_01b0;
		}
	}
	{
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_76 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv2_5();
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_77 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv2_5();
		int32_t L_78 = ___i10;
		NullCheck(L_77);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_79 = List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B(L_77, L_78, /*hidden argument*/List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B_RuntimeMethod_var);
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_80 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv2_5();
		int32_t L_81 = ___i21;
		NullCheck(L_80);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_82 = List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B(L_80, L_81, /*hidden argument*/List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_83 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_79, L_82, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_84 = Vector2_op_Multiply_m8A843A37F2F3199EBE99DC7BDABC1DC2EE01AF56(L_83, (0.5f), /*hidden argument*/NULL);
		NullCheck(L_76);
		List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857(L_76, L_84, /*hidden argument*/List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857_RuntimeMethod_var);
	}

IL_01b0:
	{
		int32_t L_85 = V_0;
		return L_85;
	}
}
// System.Void Kolibri2d.Utils/MeshHelper2::Subdivide4(UnityEngine.Mesh)
extern "C" IL2CPP_METHOD_ATTR void MeshHelper2_Subdivide4_m222D393186AD66CD72DF6F297E2C38AF69EC8E90 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MeshHelper2_Subdivide4_m222D393186AD66CD72DF6F297E2C38AF69EC8E90_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	{
		Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F * L_0 = (Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F *)il2cpp_codegen_object_new(Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m7E5325C12ECF67A2A855BBDC04968EE0CC66ACF1(L_0, /*hidden argument*/Dictionary_2__ctor_m7E5325C12ECF67A2A855BBDC04968EE0CC66ACF1_RuntimeMethod_var);
		((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->set_newVectices_7(L_0);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_1 = ___mesh0;
		MeshHelper2_InitArrays_mDBEB2310C61A2DB7A2EDC56A961F92F7CDCF1681(L_1, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_2 = ___mesh0;
		NullCheck(L_2);
		Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* L_3 = Mesh_get_triangles_mAAAFC770B8EE3F56992D5764EF8C2EC06EF743AC(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_00de;
	}

IL_001e:
	{
		Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* L_4 = V_0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		int32_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_2 = L_7;
		Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* L_8 = V_0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		int32_t L_10 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
		int32_t L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		V_3 = L_11;
		Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* L_12 = V_0;
		int32_t L_13 = V_1;
		NullCheck(L_12);
		int32_t L_14 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)2));
		int32_t L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_4 = L_15;
		int32_t L_16 = V_2;
		int32_t L_17 = V_3;
		int32_t L_18 = MeshHelper2_GetNewVertex4_m1B46BFC32053E23F3C9F90347AAF3EECD46B715F(L_16, L_17, /*hidden argument*/NULL);
		V_5 = L_18;
		int32_t L_19 = V_3;
		int32_t L_20 = V_4;
		int32_t L_21 = MeshHelper2_GetNewVertex4_m1B46BFC32053E23F3C9F90347AAF3EECD46B715F(L_19, L_20, /*hidden argument*/NULL);
		V_6 = L_21;
		int32_t L_22 = V_4;
		int32_t L_23 = V_2;
		int32_t L_24 = MeshHelper2_GetNewVertex4_m1B46BFC32053E23F3C9F90347AAF3EECD46B715F(L_22, L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_25 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_26 = V_2;
		NullCheck(L_25);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_25, L_26, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_27 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_28 = V_5;
		NullCheck(L_27);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_27, L_28, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_29 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_30 = V_7;
		NullCheck(L_29);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_29, L_30, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_31 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_32 = V_3;
		NullCheck(L_31);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_31, L_32, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_33 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_34 = V_6;
		NullCheck(L_33);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_33, L_34, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_35 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_36 = V_5;
		NullCheck(L_35);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_35, L_36, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_37 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_38 = V_4;
		NullCheck(L_37);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_37, L_38, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_39 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_40 = V_7;
		NullCheck(L_39);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_39, L_40, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_41 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_42 = V_6;
		NullCheck(L_41);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_41, L_42, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_43 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_44 = V_5;
		NullCheck(L_43);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_43, L_44, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_45 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_46 = V_6;
		NullCheck(L_45);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_45, L_46, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_47 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_48 = V_7;
		NullCheck(L_47);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_47, L_48, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		int32_t L_49 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_49, (int32_t)3));
	}

IL_00de:
	{
		int32_t L_50 = V_1;
		Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* L_51 = V_0;
		NullCheck(L_51);
		if ((((int32_t)L_50) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_51)->max_length)))))))
		{
			goto IL_001e;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_52 = ___mesh0;
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_53 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_vertices_0();
		NullCheck(L_52);
		Mesh_SetVertices_m5F487FC255C9CAF4005B75CFE67A88C8C0E7BB06(L_52, L_53, /*hidden argument*/NULL);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_54 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_normals_1();
		NullCheck(L_54);
		int32_t L_55 = List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3(L_54, /*hidden argument*/List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_RuntimeMethod_var);
		if ((((int32_t)L_55) <= ((int32_t)0)))
		{
			goto IL_010f;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_56 = ___mesh0;
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_57 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_normals_1();
		NullCheck(L_57);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_58 = List_1_ToArray_m062BF9DC0570B6EEC207AAEDDEC602FB2EB3F9BA(L_57, /*hidden argument*/List_1_ToArray_m062BF9DC0570B6EEC207AAEDDEC602FB2EB3F9BA_RuntimeMethod_var);
		NullCheck(L_56);
		Mesh_set_normals_m4054D319A67DAAA25A794D67AD37278A84406589(L_56, L_58, /*hidden argument*/NULL);
	}

IL_010f:
	{
		List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * L_59 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_colors_2();
		NullCheck(L_59);
		int32_t L_60 = List_1_get_Count_m9E6BB40B113CEAFD4EA6A2BD7FCEB7259C20561D(L_59, /*hidden argument*/List_1_get_Count_m9E6BB40B113CEAFD4EA6A2BD7FCEB7259C20561D_RuntimeMethod_var);
		if ((((int32_t)L_60) <= ((int32_t)0)))
		{
			goto IL_012c;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_61 = ___mesh0;
		List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * L_62 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_colors_2();
		NullCheck(L_62);
		ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* L_63 = List_1_ToArray_mE9F8DDC7219DCC0198F4A265DBD77BC4307FA6F4(L_62, /*hidden argument*/List_1_ToArray_mE9F8DDC7219DCC0198F4A265DBD77BC4307FA6F4_RuntimeMethod_var);
		NullCheck(L_61);
		Mesh_set_colors_m704D0EF58B7AED0D64AE4763EA375638FB08E026(L_61, L_63, /*hidden argument*/NULL);
	}

IL_012c:
	{
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_64 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv_3();
		NullCheck(L_64);
		int32_t L_65 = List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7(L_64, /*hidden argument*/List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7_RuntimeMethod_var);
		if ((((int32_t)L_65) <= ((int32_t)0)))
		{
			goto IL_0149;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_66 = ___mesh0;
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_67 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv_3();
		NullCheck(L_67);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_68 = List_1_ToArray_mB9E07F625DF343AED13593FB79319A4ADFC02AB7(L_67, /*hidden argument*/List_1_ToArray_mB9E07F625DF343AED13593FB79319A4ADFC02AB7_RuntimeMethod_var);
		NullCheck(L_66);
		Mesh_set_uv_m56E4B52315669FBDA89DC9C550AC89EEE8A4E7C8(L_66, L_68, /*hidden argument*/NULL);
	}

IL_0149:
	{
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_69 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv1_4();
		NullCheck(L_69);
		int32_t L_70 = List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7(L_69, /*hidden argument*/List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7_RuntimeMethod_var);
		if ((((int32_t)L_70) <= ((int32_t)0)))
		{
			goto IL_0166;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_71 = ___mesh0;
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_72 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv1_4();
		NullCheck(L_72);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_73 = List_1_ToArray_mB9E07F625DF343AED13593FB79319A4ADFC02AB7(L_72, /*hidden argument*/List_1_ToArray_mB9E07F625DF343AED13593FB79319A4ADFC02AB7_RuntimeMethod_var);
		NullCheck(L_71);
		Mesh_set_uv2_m8D37F9622B68D4CEA7FCF96BE4F8E442155DB038(L_71, L_73, /*hidden argument*/NULL);
	}

IL_0166:
	{
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_74 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv2_5();
		NullCheck(L_74);
		int32_t L_75 = List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7(L_74, /*hidden argument*/List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7_RuntimeMethod_var);
		if ((((int32_t)L_75) <= ((int32_t)0)))
		{
			goto IL_0183;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_76 = ___mesh0;
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_77 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv2_5();
		NullCheck(L_77);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_78 = List_1_ToArray_mB9E07F625DF343AED13593FB79319A4ADFC02AB7(L_77, /*hidden argument*/List_1_ToArray_mB9E07F625DF343AED13593FB79319A4ADFC02AB7_RuntimeMethod_var);
		NullCheck(L_76);
		Mesh_set_uv2_m8D37F9622B68D4CEA7FCF96BE4F8E442155DB038(L_76, L_78, /*hidden argument*/NULL);
	}

IL_0183:
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_79 = ___mesh0;
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_80 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		NullCheck(L_80);
		Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* L_81 = List_1_ToArray_mE3701008FD26BBC8184384CA22962898157F7BAF(L_80, /*hidden argument*/List_1_ToArray_mE3701008FD26BBC8184384CA22962898157F7BAF_RuntimeMethod_var);
		NullCheck(L_79);
		Mesh_set_triangles_m143A1C262BADCFACE43587EBA2CDC6EBEB5DFAED(L_79, L_81, /*hidden argument*/NULL);
		MeshHelper2_CleanUp_mBC2EA65D0BD5F6AA5D87F7C6259B63330A5B62B9(/*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Kolibri2d.Utils/MeshHelper2::GetNewVertex9(System.Int32,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t MeshHelper2_GetNewVertex9_m83649A9E4437ABA844B158098FB1B65272FB3D8C (int32_t ___i10, int32_t ___i21, int32_t ___i32, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MeshHelper2_GetNewVertex9_m83649A9E4437ABA844B158098FB1B65272FB3D8C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	uint32_t V_1 = 0;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_0 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_vertices_0();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3(L_0, /*hidden argument*/List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_RuntimeMethod_var);
		V_0 = L_1;
		int32_t L_2 = ___i32;
		int32_t L_3 = ___i10;
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_4 = ___i32;
		int32_t L_5 = ___i21;
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_003f;
		}
	}

IL_0013:
	{
		int32_t L_6 = ___i10;
		int32_t L_7 = ___i21;
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6<<(int32_t)((int32_t)16)))|(int32_t)L_7));
		Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F * L_8 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_newVectices_7();
		uint32_t L_9 = V_1;
		NullCheck(L_8);
		bool L_10 = Dictionary_2_ContainsKey_m230D107B4D3C342ECBC961100D82015C14534FCC(L_8, L_9, /*hidden argument*/Dictionary_2_ContainsKey_m230D107B4D3C342ECBC961100D82015C14534FCC_RuntimeMethod_var);
		if (!L_10)
		{
			goto IL_0033;
		}
	}
	{
		Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F * L_11 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_newVectices_7();
		uint32_t L_12 = V_1;
		NullCheck(L_11);
		int32_t L_13 = Dictionary_2_get_Item_mCECAC44EF2E9AE18023E7FDAD8CC268588065CE3(L_11, L_12, /*hidden argument*/Dictionary_2_get_Item_mCECAC44EF2E9AE18023E7FDAD8CC268588065CE3_RuntimeMethod_var);
		return L_13;
	}

IL_0033:
	{
		Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F * L_14 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_newVectices_7();
		uint32_t L_15 = V_1;
		int32_t L_16 = V_0;
		NullCheck(L_14);
		Dictionary_2_Add_mDCE3CAF8CAFEBD13FB38A51C069F27896353ECB5(L_14, L_15, L_16, /*hidden argument*/Dictionary_2_Add_mDCE3CAF8CAFEBD13FB38A51C069F27896353ECB5_RuntimeMethod_var);
	}

IL_003f:
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_17 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_vertices_0();
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_18 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_vertices_0();
		int32_t L_19 = ___i10;
		NullCheck(L_18);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_20 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_18, L_19, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_21 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_vertices_0();
		int32_t L_22 = ___i21;
		NullCheck(L_21);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_23 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_21, L_22, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_24 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_20, L_23, /*hidden argument*/NULL);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_25 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_vertices_0();
		int32_t L_26 = ___i32;
		NullCheck(L_25);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_27 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_25, L_26, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_28 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_24, L_27, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_29 = Vector3_op_Division_mDF34F1CC445981B4D1137765BC6277419E561624(L_28, (3.0f), /*hidden argument*/NULL);
		NullCheck(L_17);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_17, L_29, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_30 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_normals_1();
		NullCheck(L_30);
		int32_t L_31 = List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3(L_30, /*hidden argument*/List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_RuntimeMethod_var);
		if ((((int32_t)L_31) <= ((int32_t)0)))
		{
			goto IL_00c8;
		}
	}
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_32 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_normals_1();
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_33 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_normals_1();
		int32_t L_34 = ___i10;
		NullCheck(L_33);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_35 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_33, L_34, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_36 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_normals_1();
		int32_t L_37 = ___i21;
		NullCheck(L_36);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_38 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_36, L_37, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_39 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_35, L_38, /*hidden argument*/NULL);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_40 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_normals_1();
		int32_t L_41 = ___i32;
		NullCheck(L_40);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_42 = List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D(L_40, L_41, /*hidden argument*/List_1_get_Item_m8BA0B92A220CF9DE7230C71B12A020BD983EF43D_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_43 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_39, L_42, /*hidden argument*/NULL);
		V_2 = L_43;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_44 = Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_2), /*hidden argument*/NULL);
		NullCheck(L_32);
		List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76(L_32, L_44, /*hidden argument*/List_1_Add_mF254441BAE00BD522FC9EB34808181B3A3949C76_RuntimeMethod_var);
	}

IL_00c8:
	{
		List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * L_45 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_colors_2();
		NullCheck(L_45);
		int32_t L_46 = List_1_get_Count_m9E6BB40B113CEAFD4EA6A2BD7FCEB7259C20561D(L_45, /*hidden argument*/List_1_get_Count_m9E6BB40B113CEAFD4EA6A2BD7FCEB7259C20561D_RuntimeMethod_var);
		if ((((int32_t)L_46) <= ((int32_t)0)))
		{
			goto IL_0114;
		}
	}
	{
		List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * L_47 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_colors_2();
		List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * L_48 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_colors_2();
		int32_t L_49 = ___i10;
		NullCheck(L_48);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_50 = List_1_get_Item_mB725E23AF0513F5A32B32BBC9C2471097DE235C4(L_48, L_49, /*hidden argument*/List_1_get_Item_mB725E23AF0513F5A32B32BBC9C2471097DE235C4_RuntimeMethod_var);
		List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * L_51 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_colors_2();
		int32_t L_52 = ___i21;
		NullCheck(L_51);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_53 = List_1_get_Item_mB725E23AF0513F5A32B32BBC9C2471097DE235C4(L_51, L_52, /*hidden argument*/List_1_get_Item_mB725E23AF0513F5A32B32BBC9C2471097DE235C4_RuntimeMethod_var);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_54 = Color_op_Addition_m26E4EEFAF21C0C0DC2FD93CE594E02F61DB1F7DF(L_50, L_53, /*hidden argument*/NULL);
		List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * L_55 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_colors_2();
		int32_t L_56 = ___i32;
		NullCheck(L_55);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_57 = List_1_get_Item_mB725E23AF0513F5A32B32BBC9C2471097DE235C4(L_55, L_56, /*hidden argument*/List_1_get_Item_mB725E23AF0513F5A32B32BBC9C2471097DE235C4_RuntimeMethod_var);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_58 = Color_op_Addition_m26E4EEFAF21C0C0DC2FD93CE594E02F61DB1F7DF(L_54, L_57, /*hidden argument*/NULL);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_59 = Color_op_Division_m52600EAF26D4F0F781929486BEB9273938807B82(L_58, (3.0f), /*hidden argument*/NULL);
		NullCheck(L_47);
		List_1_Add_m00EB6B0859FF6BCA9F89EF1FE572855EAA143F8F(L_47, L_59, /*hidden argument*/List_1_Add_m00EB6B0859FF6BCA9F89EF1FE572855EAA143F8F_RuntimeMethod_var);
	}

IL_0114:
	{
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_60 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv_3();
		NullCheck(L_60);
		int32_t L_61 = List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7(L_60, /*hidden argument*/List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7_RuntimeMethod_var);
		if ((((int32_t)L_61) <= ((int32_t)0)))
		{
			goto IL_0160;
		}
	}
	{
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_62 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv_3();
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_63 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv_3();
		int32_t L_64 = ___i10;
		NullCheck(L_63);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_65 = List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B(L_63, L_64, /*hidden argument*/List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B_RuntimeMethod_var);
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_66 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv_3();
		int32_t L_67 = ___i21;
		NullCheck(L_66);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_68 = List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B(L_66, L_67, /*hidden argument*/List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_69 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_65, L_68, /*hidden argument*/NULL);
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_70 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv_3();
		int32_t L_71 = ___i32;
		NullCheck(L_70);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_72 = List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B(L_70, L_71, /*hidden argument*/List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B_RuntimeMethod_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_73 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_69, L_72, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_74 = Vector2_op_Division_m0961A935168EE6701E098E2B37013DFFF46A5077(L_73, (3.0f), /*hidden argument*/NULL);
		NullCheck(L_62);
		List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857(L_62, L_74, /*hidden argument*/List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857_RuntimeMethod_var);
	}

IL_0160:
	{
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_75 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv1_4();
		NullCheck(L_75);
		int32_t L_76 = List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7(L_75, /*hidden argument*/List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7_RuntimeMethod_var);
		if ((((int32_t)L_76) <= ((int32_t)0)))
		{
			goto IL_01ac;
		}
	}
	{
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_77 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv1_4();
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_78 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv1_4();
		int32_t L_79 = ___i10;
		NullCheck(L_78);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_80 = List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B(L_78, L_79, /*hidden argument*/List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B_RuntimeMethod_var);
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_81 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv1_4();
		int32_t L_82 = ___i21;
		NullCheck(L_81);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_83 = List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B(L_81, L_82, /*hidden argument*/List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_84 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_80, L_83, /*hidden argument*/NULL);
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_85 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv1_4();
		int32_t L_86 = ___i32;
		NullCheck(L_85);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_87 = List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B(L_85, L_86, /*hidden argument*/List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B_RuntimeMethod_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_88 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_84, L_87, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_89 = Vector2_op_Division_m0961A935168EE6701E098E2B37013DFFF46A5077(L_88, (3.0f), /*hidden argument*/NULL);
		NullCheck(L_77);
		List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857(L_77, L_89, /*hidden argument*/List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857_RuntimeMethod_var);
	}

IL_01ac:
	{
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_90 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv2_5();
		NullCheck(L_90);
		int32_t L_91 = List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7(L_90, /*hidden argument*/List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7_RuntimeMethod_var);
		if ((((int32_t)L_91) <= ((int32_t)0)))
		{
			goto IL_01f8;
		}
	}
	{
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_92 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv2_5();
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_93 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv2_5();
		int32_t L_94 = ___i10;
		NullCheck(L_93);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_95 = List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B(L_93, L_94, /*hidden argument*/List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B_RuntimeMethod_var);
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_96 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv2_5();
		int32_t L_97 = ___i21;
		NullCheck(L_96);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_98 = List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B(L_96, L_97, /*hidden argument*/List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_99 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_95, L_98, /*hidden argument*/NULL);
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_100 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv2_5();
		int32_t L_101 = ___i32;
		NullCheck(L_100);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_102 = List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B(L_100, L_101, /*hidden argument*/List_1_get_Item_mEBB57A0D130B365BDAED6DD55068D4F19082600B_RuntimeMethod_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_103 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_99, L_102, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_104 = Vector2_op_Division_m0961A935168EE6701E098E2B37013DFFF46A5077(L_103, (3.0f), /*hidden argument*/NULL);
		NullCheck(L_92);
		List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857(L_92, L_104, /*hidden argument*/List_1_Add_m58534191D5DE803D5187A4653908969E7E63C857_RuntimeMethod_var);
	}

IL_01f8:
	{
		int32_t L_105 = V_0;
		return L_105;
	}
}
// System.Void Kolibri2d.Utils/MeshHelper2::Subdivide9(UnityEngine.Mesh)
extern "C" IL2CPP_METHOD_ATTR void MeshHelper2_Subdivide9_m4FB982D43A206A43AE8BF55805987803940FBE13 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MeshHelper2_Subdivide9_m4FB982D43A206A43AE8BF55805987803940FBE13_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	{
		Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F * L_0 = (Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F *)il2cpp_codegen_object_new(Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m7E5325C12ECF67A2A855BBDC04968EE0CC66ACF1(L_0, /*hidden argument*/Dictionary_2__ctor_m7E5325C12ECF67A2A855BBDC04968EE0CC66ACF1_RuntimeMethod_var);
		((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->set_newVectices_7(L_0);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_1 = ___mesh0;
		MeshHelper2_InitArrays_mDBEB2310C61A2DB7A2EDC56A961F92F7CDCF1681(L_1, /*hidden argument*/NULL);
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_2 = ___mesh0;
		NullCheck(L_2);
		Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* L_3 = Mesh_get_triangles_mAAAFC770B8EE3F56992D5764EF8C2EC06EF743AC(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_01c2;
	}

IL_001e:
	{
		Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* L_4 = V_0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		int32_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_2 = L_7;
		Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* L_8 = V_0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		int32_t L_10 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
		int32_t L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		V_3 = L_11;
		Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* L_12 = V_0;
		int32_t L_13 = V_1;
		NullCheck(L_12);
		int32_t L_14 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)2));
		int32_t L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_4 = L_15;
		int32_t L_16 = V_2;
		int32_t L_17 = V_3;
		int32_t L_18 = V_2;
		int32_t L_19 = MeshHelper2_GetNewVertex9_m83649A9E4437ABA844B158098FB1B65272FB3D8C(L_16, L_17, L_18, /*hidden argument*/NULL);
		V_5 = L_19;
		int32_t L_20 = V_3;
		int32_t L_21 = V_2;
		int32_t L_22 = V_3;
		int32_t L_23 = MeshHelper2_GetNewVertex9_m83649A9E4437ABA844B158098FB1B65272FB3D8C(L_20, L_21, L_22, /*hidden argument*/NULL);
		V_6 = L_23;
		int32_t L_24 = V_3;
		int32_t L_25 = V_4;
		int32_t L_26 = V_3;
		int32_t L_27 = MeshHelper2_GetNewVertex9_m83649A9E4437ABA844B158098FB1B65272FB3D8C(L_24, L_25, L_26, /*hidden argument*/NULL);
		V_7 = L_27;
		int32_t L_28 = V_4;
		int32_t L_29 = V_3;
		int32_t L_30 = V_4;
		int32_t L_31 = MeshHelper2_GetNewVertex9_m83649A9E4437ABA844B158098FB1B65272FB3D8C(L_28, L_29, L_30, /*hidden argument*/NULL);
		V_8 = L_31;
		int32_t L_32 = V_4;
		int32_t L_33 = V_2;
		int32_t L_34 = V_4;
		int32_t L_35 = MeshHelper2_GetNewVertex9_m83649A9E4437ABA844B158098FB1B65272FB3D8C(L_32, L_33, L_34, /*hidden argument*/NULL);
		V_9 = L_35;
		int32_t L_36 = V_2;
		int32_t L_37 = V_4;
		int32_t L_38 = V_2;
		int32_t L_39 = MeshHelper2_GetNewVertex9_m83649A9E4437ABA844B158098FB1B65272FB3D8C(L_36, L_37, L_38, /*hidden argument*/NULL);
		V_10 = L_39;
		int32_t L_40 = V_2;
		int32_t L_41 = V_3;
		int32_t L_42 = V_4;
		int32_t L_43 = MeshHelper2_GetNewVertex9_m83649A9E4437ABA844B158098FB1B65272FB3D8C(L_40, L_41, L_42, /*hidden argument*/NULL);
		V_11 = L_43;
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_44 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_45 = V_2;
		NullCheck(L_44);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_44, L_45, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_46 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_47 = V_5;
		NullCheck(L_46);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_46, L_47, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_48 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_49 = V_10;
		NullCheck(L_48);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_48, L_49, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_50 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_51 = V_3;
		NullCheck(L_50);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_50, L_51, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_52 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_53 = V_7;
		NullCheck(L_52);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_52, L_53, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_54 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_55 = V_6;
		NullCheck(L_54);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_54, L_55, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_56 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_57 = V_4;
		NullCheck(L_56);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_56, L_57, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_58 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_59 = V_9;
		NullCheck(L_58);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_58, L_59, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_60 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_61 = V_8;
		NullCheck(L_60);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_60, L_61, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_62 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_63 = V_11;
		NullCheck(L_62);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_62, L_63, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_64 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_65 = V_5;
		NullCheck(L_64);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_64, L_65, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_66 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_67 = V_6;
		NullCheck(L_66);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_66, L_67, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_68 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_69 = V_11;
		NullCheck(L_68);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_68, L_69, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_70 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_71 = V_7;
		NullCheck(L_70);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_70, L_71, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_72 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_73 = V_8;
		NullCheck(L_72);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_72, L_73, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_74 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_75 = V_11;
		NullCheck(L_74);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_74, L_75, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_76 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_77 = V_9;
		NullCheck(L_76);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_76, L_77, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_78 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_79 = V_10;
		NullCheck(L_78);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_78, L_79, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_80 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_81 = V_11;
		NullCheck(L_80);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_80, L_81, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_82 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_83 = V_10;
		NullCheck(L_82);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_82, L_83, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_84 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_85 = V_5;
		NullCheck(L_84);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_84, L_85, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_86 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_87 = V_11;
		NullCheck(L_86);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_86, L_87, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_88 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_89 = V_6;
		NullCheck(L_88);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_88, L_89, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_90 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_91 = V_7;
		NullCheck(L_90);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_90, L_91, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_92 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_93 = V_11;
		NullCheck(L_92);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_92, L_93, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_94 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_95 = V_8;
		NullCheck(L_94);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_94, L_95, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_96 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		int32_t L_97 = V_9;
		NullCheck(L_96);
		List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733(L_96, L_97, /*hidden argument*/List_1_Add_mF8F83E07217FCBD9DDAADAEF4AF51E0823735733_RuntimeMethod_var);
		int32_t L_98 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_98, (int32_t)3));
	}

IL_01c2:
	{
		int32_t L_99 = V_1;
		Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* L_100 = V_0;
		NullCheck(L_100);
		if ((((int32_t)L_99) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_100)->max_length)))))))
		{
			goto IL_001e;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_101 = ___mesh0;
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_102 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_vertices_0();
		NullCheck(L_101);
		Mesh_SetVertices_m5F487FC255C9CAF4005B75CFE67A88C8C0E7BB06(L_101, L_102, /*hidden argument*/NULL);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_103 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_normals_1();
		NullCheck(L_103);
		int32_t L_104 = List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3(L_103, /*hidden argument*/List_1_get_Count_m69F7903337857AF491827CF48D18C3CD7DF49CA3_RuntimeMethod_var);
		if ((((int32_t)L_104) <= ((int32_t)0)))
		{
			goto IL_01f3;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_105 = ___mesh0;
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_106 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_normals_1();
		NullCheck(L_106);
		Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* L_107 = List_1_ToArray_m062BF9DC0570B6EEC207AAEDDEC602FB2EB3F9BA(L_106, /*hidden argument*/List_1_ToArray_m062BF9DC0570B6EEC207AAEDDEC602FB2EB3F9BA_RuntimeMethod_var);
		NullCheck(L_105);
		Mesh_set_normals_m4054D319A67DAAA25A794D67AD37278A84406589(L_105, L_107, /*hidden argument*/NULL);
	}

IL_01f3:
	{
		List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * L_108 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_colors_2();
		NullCheck(L_108);
		int32_t L_109 = List_1_get_Count_m9E6BB40B113CEAFD4EA6A2BD7FCEB7259C20561D(L_108, /*hidden argument*/List_1_get_Count_m9E6BB40B113CEAFD4EA6A2BD7FCEB7259C20561D_RuntimeMethod_var);
		if ((((int32_t)L_109) <= ((int32_t)0)))
		{
			goto IL_0210;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_110 = ___mesh0;
		List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * L_111 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_colors_2();
		NullCheck(L_111);
		ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* L_112 = List_1_ToArray_mE9F8DDC7219DCC0198F4A265DBD77BC4307FA6F4(L_111, /*hidden argument*/List_1_ToArray_mE9F8DDC7219DCC0198F4A265DBD77BC4307FA6F4_RuntimeMethod_var);
		NullCheck(L_110);
		Mesh_set_colors_m704D0EF58B7AED0D64AE4763EA375638FB08E026(L_110, L_112, /*hidden argument*/NULL);
	}

IL_0210:
	{
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_113 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv_3();
		NullCheck(L_113);
		int32_t L_114 = List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7(L_113, /*hidden argument*/List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7_RuntimeMethod_var);
		if ((((int32_t)L_114) <= ((int32_t)0)))
		{
			goto IL_022d;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_115 = ___mesh0;
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_116 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv_3();
		NullCheck(L_116);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_117 = List_1_ToArray_mB9E07F625DF343AED13593FB79319A4ADFC02AB7(L_116, /*hidden argument*/List_1_ToArray_mB9E07F625DF343AED13593FB79319A4ADFC02AB7_RuntimeMethod_var);
		NullCheck(L_115);
		Mesh_set_uv_m56E4B52315669FBDA89DC9C550AC89EEE8A4E7C8(L_115, L_117, /*hidden argument*/NULL);
	}

IL_022d:
	{
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_118 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv1_4();
		NullCheck(L_118);
		int32_t L_119 = List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7(L_118, /*hidden argument*/List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7_RuntimeMethod_var);
		if ((((int32_t)L_119) <= ((int32_t)0)))
		{
			goto IL_024a;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_120 = ___mesh0;
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_121 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv1_4();
		NullCheck(L_121);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_122 = List_1_ToArray_mB9E07F625DF343AED13593FB79319A4ADFC02AB7(L_121, /*hidden argument*/List_1_ToArray_mB9E07F625DF343AED13593FB79319A4ADFC02AB7_RuntimeMethod_var);
		NullCheck(L_120);
		Mesh_set_uv2_m8D37F9622B68D4CEA7FCF96BE4F8E442155DB038(L_120, L_122, /*hidden argument*/NULL);
	}

IL_024a:
	{
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_123 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv2_5();
		NullCheck(L_123);
		int32_t L_124 = List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7(L_123, /*hidden argument*/List_1_get_Count_mA5AEE229A6D0F3CC92E8BD66C3FC7759FB0101A7_RuntimeMethod_var);
		if ((((int32_t)L_124) <= ((int32_t)0)))
		{
			goto IL_0267;
		}
	}
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_125 = ___mesh0;
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_126 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_uv2_5();
		NullCheck(L_126);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_127 = List_1_ToArray_mB9E07F625DF343AED13593FB79319A4ADFC02AB7(L_126, /*hidden argument*/List_1_ToArray_mB9E07F625DF343AED13593FB79319A4ADFC02AB7_RuntimeMethod_var);
		NullCheck(L_125);
		Mesh_set_uv2_m8D37F9622B68D4CEA7FCF96BE4F8E442155DB038(L_125, L_127, /*hidden argument*/NULL);
	}

IL_0267:
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_128 = ___mesh0;
		List_1_t4A961510635950236678F1B3B2436ECCAF28713A * L_129 = ((MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields*)il2cpp_codegen_static_fields_for(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_il2cpp_TypeInfo_var))->get_indices_6();
		NullCheck(L_129);
		Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* L_130 = List_1_ToArray_mE3701008FD26BBC8184384CA22962898157F7BAF(L_129, /*hidden argument*/List_1_ToArray_mE3701008FD26BBC8184384CA22962898157F7BAF_RuntimeMethod_var);
		NullCheck(L_128);
		Mesh_set_triangles_m143A1C262BADCFACE43587EBA2CDC6EBEB5DFAED(L_128, L_130, /*hidden argument*/NULL);
		MeshHelper2_CleanUp_mBC2EA65D0BD5F6AA5D87F7C6259B63330A5B62B9(/*hidden argument*/NULL);
		return;
	}
}
// System.Void Kolibri2d.Utils/MeshHelper2::Subdivide(UnityEngine.Mesh,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void MeshHelper2_Subdivide_mF84D68B23BAA6DE863EBA78812805794AB622F31 (Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh0, int32_t ___level1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___level1;
		if ((((int32_t)L_0) >= ((int32_t)2)))
		{
			goto IL_0030;
		}
	}
	{
		return;
	}

IL_0005:
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_1 = ___mesh0;
		MeshHelper2_Subdivide9_m4FB982D43A206A43AE8BF55805987803940FBE13(L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___level1;
		___level1 = ((int32_t)((int32_t)L_2/(int32_t)3));
	}

IL_0010:
	{
		int32_t L_3 = ___level1;
		if (!((int32_t)((int32_t)L_3%(int32_t)3)))
		{
			goto IL_0005;
		}
	}
	{
		goto IL_0022;
	}

IL_0017:
	{
		Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * L_4 = ___mesh0;
		MeshHelper2_Subdivide4_m222D393186AD66CD72DF6F297E2C38AF69EC8E90(L_4, /*hidden argument*/NULL);
		int32_t L_5 = ___level1;
		___level1 = ((int32_t)((int32_t)L_5/(int32_t)2));
	}

IL_0022:
	{
		int32_t L_6 = ___level1;
		if (!((int32_t)((int32_t)L_6%(int32_t)2)))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_7 = ___level1;
		if ((((int32_t)L_7) <= ((int32_t)3)))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_8 = ___level1;
		___level1 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0030:
	{
		int32_t L_9 = ___level1;
		if ((((int32_t)L_9) > ((int32_t)1)))
		{
			goto IL_0010;
		}
	}
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Kolibri2d.WaterTestPanel::Awake()
extern "C" IL2CPP_METHOD_ATTR void WaterTestPanel_Awake_mF1CF8CB0258A414CFBD28BBB13E0BBF92691A1D8 (WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaterTestPanel_Awake_mF1CF8CB0258A414CFBD28BBB13E0BBF92691A1D8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8 * L_0 = Component_GetComponent_TisSplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8_m2CF9090BA6AB33D29C9FC86E7FE19E20187241F8(__this, /*hidden argument*/Component_GetComponent_TisSplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8_m2CF9090BA6AB33D29C9FC86E7FE19E20187241F8_RuntimeMethod_var);
		__this->set_water_4(L_0);
		return;
	}
}
// System.Void Kolibri2d.WaterTestPanel::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void WaterTestPanel_OnEnable_mD5A7C3D276F35B616C81AC47EC94F40F874570F2 (WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaterTestPanel_OnEnable_mD5A7C3D276F35B616C81AC47EC94F40F874570F2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8 * L_0 = Component_GetComponent_TisSplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8_m2CF9090BA6AB33D29C9FC86E7FE19E20187241F8(__this, /*hidden argument*/Component_GetComponent_TisSplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8_m2CF9090BA6AB33D29C9FC86E7FE19E20187241F8_RuntimeMethod_var);
		__this->set_water_4(L_0);
		return;
	}
}
// System.Void Kolibri2d.WaterTestPanel::.ctor()
extern "C" IL2CPP_METHOD_ATTR void WaterTestPanel__ctor_mD40CE96AA61F39F3508364139BFFA5708BAA99B4 (WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80 * __this, const RuntimeMethod* method)
{
	{
		__this->set_lastForceRaw_5((-1.0f));
		__this->set_lastForceAfterMultiplier_6((-1.0f));
		__this->set_lastForceIgnored_7((bool)1);
		__this->set_lastForceClamped_8((bool)1);
		__this->set_lastForceAfterClamp_9((-1.0f));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
