﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Kolibri2d.BezierSpline
struct BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE;
// Kolibri2d.BezierSpline/OnRefreshCallback
struct OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300;
// Kolibri2d.BezierSplineQuality
struct BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF;
// Kolibri2d.Decoration
struct Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159;
// Kolibri2d.DecorationGroup
struct DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0;
// Kolibri2d.DecorationGroup/SortingLayerOptions
struct SortingLayerOptions_t241DB2E522009CEB9D89742D1213F4B22EE99E9E;
// Kolibri2d.DecorationGroupRandomizer
struct DecorationGroupRandomizer_t723A277DE2132DC6F09140A53D0393805D412605;
// Kolibri2d.DecorationMaterial
struct DecorationMaterial_t0D0DD06B7E2AC4FF1E10BA9FAEAB4FEBDD125A14;
// Kolibri2d.PhysicsSettings
struct PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190;
// Kolibri2d.Poly2Tri.AdvancingFront
struct AdvancingFront_tBCCD1124A9FAD23592008C9B975AA05853B39C44;
// Kolibri2d.Poly2Tri.AdvancingFrontNode
struct AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357;
// Kolibri2d.Poly2Tri.DTSweepBasin
struct DTSweepBasin_t47769C4C8A6508E384E73DCD2ED888AEA2A782CD;
// Kolibri2d.Poly2Tri.DTSweepConstraint
struct DTSweepConstraint_t4773D59AAEBA0883F8DA0C42FBD8C92350DBE594;
// Kolibri2d.Poly2Tri.DTSweepEdgeEvent
struct DTSweepEdgeEvent_t2BF104D3D9F78FA8F6BB2F1B63E5FFAAA9D446BC;
// Kolibri2d.Poly2Tri.DTSweepPointComparator
struct DTSweepPointComparator_t030E4A9D87B78D989BCE53C4756FBC3AA4D7EA1B;
// Kolibri2d.Poly2Tri.DelaunayTriangle
struct DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278;
// Kolibri2d.Poly2Tri.ITriangulatable
struct ITriangulatable_t1F37AD256C77B0E1CFDA7C785C2950EFE0D74AE7;
// Kolibri2d.Poly2Tri.Point2D
struct Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D;
// Kolibri2d.Poly2Tri.PolygonPoint
struct PolygonPoint_t94267F9D8AC5851730C1D804609D51ABC86911E4;
// Kolibri2d.Poly2Tri.Rect2D
struct Rect2D_t337FF4CBB07E16CB6E029205BF01F1984DDE5353;
// Kolibri2d.Poly2Tri.TriangulationContext
struct TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B;
// Kolibri2d.Poly2Tri.TriangulationDebugContext
struct TriangulationDebugContext_t23D041A57AAB2224970A201625386E8BEB37D9A4;
// Kolibri2d.Poly2Tri.TriangulationPoint
struct TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E;
// Kolibri2d.SplineColliderSettings
struct SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4;
// Kolibri2d.SplineData
struct SplineData_t97926CA36C3A0FF54D20743F14165941B0C11747;
// Kolibri2d.SplineTerrain2DCreator
struct SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1;
// Kolibri2d.SplineWater
struct SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8;
// Kolibri2d.SplineWater/WaterEvent
struct WaterEvent_t414CD81AE9F28B656088F21ECC14F030B1FDA33B;
// Kolibri2d.Terrain2D
struct Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B;
// Kolibri2d.Terrain2DArchitecture
struct Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A;
// Kolibri2d.Terrain2DArchitectureAngles
struct Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208;
// Kolibri2d.Terrain2DMaterial
struct Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D;
// Kolibri2d.Terrain2DMaterial/CornerSprites2
struct CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703;
// Kolibri2d.Terrain2DMaterial/SortingLayerOptions2
struct SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE;
// Kolibri2d.Terrain2DMaterial/SortingZ_3D_Options
struct SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F;
// Kolibri2d.WaterTestPanel
struct WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80;
// System.Action
struct Action_tC9F24EBA5075EA0322C6E0DC6BE2C7BDF962FB21;
// System.Action`1<System.Boolean>
struct Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD;
// System.Action`3<System.Boolean,System.Boolean,System.Int32>
struct Action_3_tEE1FB0623176AFA5109FAA9BA7E82293445CAE1E;
// System.AsyncCallback
struct AsyncCallback_t74ABD1277F711E7FBDCB00E169A63DEFD39E86A2;
// System.Char[]
struct CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744;
// System.Collections.Generic.Dictionary`2<Kolibri2d.DecorationGroup,Kolibri2d.DecorationGroupRandomizer>
struct Dictionary_2_tEC082D6EA1509396C080AABEBFC6445FF7640C3C;
// System.Collections.Generic.Dictionary`2<Kolibri2d.TerrainType,Kolibri2d.MaterialTextureInfo>
struct Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A;
// System.Collections.Generic.Dictionary`2<Kolibri2d.TerrainType,Kolibri2d.PhysicsSettings>
struct Dictionary_2_t99ADF3EF31BE384D1A13236F275C436B11310513;
// System.Collections.Generic.Dictionary`2<Kolibri2d.TerrainType,Kolibri2d.SeedRandomizer>
struct Dictionary_2_t323DE15258681F41547A3501288B5D1722FEF150;
// System.Collections.Generic.Dictionary`2<Kolibri2d.TerrainType,System.Int32>
struct Dictionary_2_tB362795ED1A6C15ECE1381757FD2C6FA491211AB;
// System.Collections.Generic.Dictionary`2<System.UInt32,Kolibri2d.Poly2Tri.TriangulationPoint>
struct Dictionary_2_tEF8705C50BC680A7436EB757C3290BE780393000;
// System.Collections.Generic.Dictionary`2<System.UInt32,System.Int32>
struct Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F;
// System.Collections.Generic.Dictionary`2<UnityEngine.Collider2D,System.Collections.Generic.List`1<System.Int32>>
struct Dictionary_2_t97FE799FB5630EA306A1F53E2CA192562EF2E217;
// System.Collections.Generic.List`1<Kolibri2d.BezierSpline/BezierPoint>
struct List_1_tD5F01382C85482BD3A1133E16C27709D77F65384;
// System.Collections.Generic.List`1<Kolibri2d.BezierSpline/PointData>
struct List_1_t96B06A7F290D61DC5641A89066DC34043C6F5286;
// System.Collections.Generic.List`1<Kolibri2d.Decoration>
struct List_1_tA358CC6533316031BC20A834B3C5627D3B034883;
// System.Collections.Generic.List`1<Kolibri2d.DecorationMaterial/GroupHolder>
struct List_1_t1BC6EE4FE66E61BC50A73C8FB747B1A186D9D6C6;
// System.Collections.Generic.List`1<Kolibri2d.Poly2Tri.DelaunayTriangle>
struct List_1_tA161C1D023FB6D4167CAE9ED2A7AB1554978B9D9;
// System.Collections.Generic.List`1<Kolibri2d.Poly2Tri.Point2D>
struct List_1_tFD3B51F91BAFEAD7A39B0EF917EC5B58423E32AE;
// System.Collections.Generic.List`1<Kolibri2d.Poly2Tri.Polygon>
struct List_1_t458521EC9E0F7DCD4972FFAF1DF76E029E984E67;
// System.Collections.Generic.List`1<Kolibri2d.Poly2Tri.TriangulationPoint>
struct List_1_tAAF0D3178D082F2377FDDDC38ED8BC5C83A1FF81;
// System.Collections.Generic.List`1<Kolibri2d.SplineControlPoint>
struct List_1_t4022365DF596613EEE444DE140D4966C2BD9EA5B;
// System.Collections.Generic.List`1<Kolibri2d.SplineSegmentInfo>
struct List_1_t517344AED92C69B6645F0ABFA470063D220BACCD;
// System.Collections.Generic.List`1<Kolibri2d.Terrain2D/Terrain2DMaterialHolder>
struct List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct List_1_tF10DA541010813ED923C2FC487CDFC2FEE36A92E;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct List_1_t6C5629FA82F6D17869213B9E74068E1F03FE8B53;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t4A961510635950236678F1B3B2436ECCAF28713A;
// System.Collections.Generic.List`1<System.Single>
struct List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650;
// System.Collections.Generic.List`1<UnityEngine.Material>
struct List_1_t56B616621385F653B408094FBDFA5DEC6C4B47EA;
// System.Collections.Generic.List`1<UnityEngine.Mesh>
struct List_1_tBE402A53CD5331E0CB483C591CFDC8C6827F35B8;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.IDictionary
struct IDictionary_tD35B9437F08BE98D1E0B295CC73C48E168CAB316;
// System.DelegateData
struct DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9;
// System.IAsyncResult
struct IAsyncResult_tDA33C24465239FB383C4C2CDAAC43B9AD3CB7F05;
// System.IntPtr[]
struct IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659;
// System.Object[]
struct ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Single[]
struct SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5;
// System.String
struct String_t;
// System.Text.RegularExpressions.Regex
struct Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B;
// System.Void
struct Void_tDB81A15FA2AB53E2401A76B745D215397B29F783;
// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged
struct SessionStateChanged_t9084549A636BD45086D66CC6765DA8C3DD31066F;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067;
// UnityEngine.LineRenderer
struct LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MeshFilter
struct MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0;
// UnityEngine.MeshRenderer
struct MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED;
// UnityEngine.Object[]
struct ObjectU5BU5D_t0989B392BF4AF7D427A68A3A60BD412E54A9573C;
// UnityEngine.PhysicsMaterial2D
struct PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937;
// UnityEngine.PolygonCollider2D
struct PolygonCollider2D_t360CC66FD7BAF5F4FADE54EB23E58E95D0B8CD81;
// UnityEngine.PolygonCollider2D[]
struct PolygonCollider2DU5BU5D_t740B9DF43CFB32BA191190CEA2770604FF8C0968;
// UnityEngine.RemoteSettings/UpdatedEventHandler
struct UpdatedEventHandler_tB0230BC83686D7126AB4D3800A66351028CA514F;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;




#ifndef U3CMODULEU3E_TCD4309F8DDA0F37A98DBCDFE49F6C8F300C242B0_H
#define U3CMODULEU3E_TCD4309F8DDA0F37A98DBCDFE49F6C8F300C242B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tCD4309F8DDA0F37A98DBCDFE49F6C8F300C242B0 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TCD4309F8DDA0F37A98DBCDFE49F6C8F300C242B0_H
#ifndef U3CMODULEU3E_T46A9D89D51A2AC061548E4AC6E72FAFE9714FED8_H
#define U3CMODULEU3E_T46A9D89D51A2AC061548E4AC6E72FAFE9714FED8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t46A9D89D51A2AC061548E4AC6E72FAFE9714FED8 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T46A9D89D51A2AC061548E4AC6E72FAFE9714FED8_H
#ifndef U3CMODULEU3E_TB308A2384DEB86F8845A4E61970976B8944B5DC4_H
#define U3CMODULEU3E_TB308A2384DEB86F8845A4E61970976B8944B5DC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tB308A2384DEB86F8845A4E61970976B8944B5DC4 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TB308A2384DEB86F8845A4E61970976B8944B5DC4_H
#ifndef U3CMODULEU3E_T2FBFFC67F8D6B1FA13284515F9BBD8C9333B5C86_H
#define U3CMODULEU3E_T2FBFFC67F8D6B1FA13284515F9BBD8C9333B5C86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t2FBFFC67F8D6B1FA13284515F9BBD8C9333B5C86 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T2FBFFC67F8D6B1FA13284515F9BBD8C9333B5C86_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef BEZIER_TC49C92841ACE82C81E44CB3A8B49CD6A8911FE1F_H
#define BEZIER_TC49C92841ACE82C81E44CB3A8B49CD6A8911FE1F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Bezier
struct  Bezier_tC49C92841ACE82C81E44CB3A8B49CD6A8911FE1F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEZIER_TC49C92841ACE82C81E44CB3A8B49CD6A8911FE1F_H
#ifndef POINTDATA_T8282E0CA58CA95B34C07C6A651E1A63EC74662F9_H
#define POINTDATA_T8282E0CA58CA95B34C07C6A651E1A63EC74662F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.BezierSpline/PointData
struct  PointData_t8282E0CA58CA95B34C07C6A651E1A63EC74662F9  : public RuntimeObject
{
public:
	// System.Int32 Kolibri2d.BezierSpline/PointData::subdivisions
	int32_t ___subdivisions_0;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Kolibri2d.BezierSpline/PointData::positions
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___positions_1;
	// System.Collections.Generic.List`1<System.Single> Kolibri2d.BezierSpline/PointData::lengths
	List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * ___lengths_2;
	// System.Single Kolibri2d.BezierSpline/PointData::smoothness
	float ___smoothness_3;
	// System.Boolean Kolibri2d.BezierSpline/PointData::manualTangent
	bool ___manualTangent_4;

public:
	inline static int32_t get_offset_of_subdivisions_0() { return static_cast<int32_t>(offsetof(PointData_t8282E0CA58CA95B34C07C6A651E1A63EC74662F9, ___subdivisions_0)); }
	inline int32_t get_subdivisions_0() const { return ___subdivisions_0; }
	inline int32_t* get_address_of_subdivisions_0() { return &___subdivisions_0; }
	inline void set_subdivisions_0(int32_t value)
	{
		___subdivisions_0 = value;
	}

	inline static int32_t get_offset_of_positions_1() { return static_cast<int32_t>(offsetof(PointData_t8282E0CA58CA95B34C07C6A651E1A63EC74662F9, ___positions_1)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_positions_1() const { return ___positions_1; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_positions_1() { return &___positions_1; }
	inline void set_positions_1(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___positions_1 = value;
		Il2CppCodeGenWriteBarrier((&___positions_1), value);
	}

	inline static int32_t get_offset_of_lengths_2() { return static_cast<int32_t>(offsetof(PointData_t8282E0CA58CA95B34C07C6A651E1A63EC74662F9, ___lengths_2)); }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * get_lengths_2() const { return ___lengths_2; }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E ** get_address_of_lengths_2() { return &___lengths_2; }
	inline void set_lengths_2(List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * value)
	{
		___lengths_2 = value;
		Il2CppCodeGenWriteBarrier((&___lengths_2), value);
	}

	inline static int32_t get_offset_of_smoothness_3() { return static_cast<int32_t>(offsetof(PointData_t8282E0CA58CA95B34C07C6A651E1A63EC74662F9, ___smoothness_3)); }
	inline float get_smoothness_3() const { return ___smoothness_3; }
	inline float* get_address_of_smoothness_3() { return &___smoothness_3; }
	inline void set_smoothness_3(float value)
	{
		___smoothness_3 = value;
	}

	inline static int32_t get_offset_of_manualTangent_4() { return static_cast<int32_t>(offsetof(PointData_t8282E0CA58CA95B34C07C6A651E1A63EC74662F9, ___manualTangent_4)); }
	inline bool get_manualTangent_4() const { return ___manualTangent_4; }
	inline bool* get_address_of_manualTangent_4() { return &___manualTangent_4; }
	inline void set_manualTangent_4(bool value)
	{
		___manualTangent_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTDATA_T8282E0CA58CA95B34C07C6A651E1A63EC74662F9_H
#ifndef SORTINGLAYEROPTIONS_T241DB2E522009CEB9D89742D1213F4B22EE99E9E_H
#define SORTINGLAYEROPTIONS_T241DB2E522009CEB9D89742D1213F4B22EE99E9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.DecorationGroup/SortingLayerOptions
struct  SortingLayerOptions_t241DB2E522009CEB9D89742D1213F4B22EE99E9E  : public RuntimeObject
{
public:
	// System.Int32 Kolibri2d.DecorationGroup/SortingLayerOptions::sortingLayerId
	int32_t ___sortingLayerId_0;
	// System.Int32 Kolibri2d.DecorationGroup/SortingLayerOptions::orderInLayerMin
	int32_t ___orderInLayerMin_1;
	// System.Int32 Kolibri2d.DecorationGroup/SortingLayerOptions::orderInLayerMax
	int32_t ___orderInLayerMax_2;

public:
	inline static int32_t get_offset_of_sortingLayerId_0() { return static_cast<int32_t>(offsetof(SortingLayerOptions_t241DB2E522009CEB9D89742D1213F4B22EE99E9E, ___sortingLayerId_0)); }
	inline int32_t get_sortingLayerId_0() const { return ___sortingLayerId_0; }
	inline int32_t* get_address_of_sortingLayerId_0() { return &___sortingLayerId_0; }
	inline void set_sortingLayerId_0(int32_t value)
	{
		___sortingLayerId_0 = value;
	}

	inline static int32_t get_offset_of_orderInLayerMin_1() { return static_cast<int32_t>(offsetof(SortingLayerOptions_t241DB2E522009CEB9D89742D1213F4B22EE99E9E, ___orderInLayerMin_1)); }
	inline int32_t get_orderInLayerMin_1() const { return ___orderInLayerMin_1; }
	inline int32_t* get_address_of_orderInLayerMin_1() { return &___orderInLayerMin_1; }
	inline void set_orderInLayerMin_1(int32_t value)
	{
		___orderInLayerMin_1 = value;
	}

	inline static int32_t get_offset_of_orderInLayerMax_2() { return static_cast<int32_t>(offsetof(SortingLayerOptions_t241DB2E522009CEB9D89742D1213F4B22EE99E9E, ___orderInLayerMax_2)); }
	inline int32_t get_orderInLayerMax_2() const { return ___orderInLayerMax_2; }
	inline int32_t* get_address_of_orderInLayerMax_2() { return &___orderInLayerMax_2; }
	inline void set_orderInLayerMax_2(int32_t value)
	{
		___orderInLayerMax_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTINGLAYEROPTIONS_T241DB2E522009CEB9D89742D1213F4B22EE99E9E_H
#ifndef DECORATIONGROUPRANDOMIZER_T723A277DE2132DC6F09140A53D0393805D412605_H
#define DECORATIONGROUPRANDOMIZER_T723A277DE2132DC6F09140A53D0393805D412605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.DecorationGroupRandomizer
struct  DecorationGroupRandomizer_t723A277DE2132DC6F09140A53D0393805D412605  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<Kolibri2d.TerrainType,Kolibri2d.SeedRandomizer> Kolibri2d.DecorationGroupRandomizer::seeds
	Dictionary_2_t323DE15258681F41547A3501288B5D1722FEF150 * ___seeds_0;

public:
	inline static int32_t get_offset_of_seeds_0() { return static_cast<int32_t>(offsetof(DecorationGroupRandomizer_t723A277DE2132DC6F09140A53D0393805D412605, ___seeds_0)); }
	inline Dictionary_2_t323DE15258681F41547A3501288B5D1722FEF150 * get_seeds_0() const { return ___seeds_0; }
	inline Dictionary_2_t323DE15258681F41547A3501288B5D1722FEF150 ** get_address_of_seeds_0() { return &___seeds_0; }
	inline void set_seeds_0(Dictionary_2_t323DE15258681F41547A3501288B5D1722FEF150 * value)
	{
		___seeds_0 = value;
		Il2CppCodeGenWriteBarrier((&___seeds_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECORATIONGROUPRANDOMIZER_T723A277DE2132DC6F09140A53D0393805D412605_H
#ifndef GROUPHOLDER_T6D44EAE647CB3F6269BCB0EF2BF51C40C3E14286_H
#define GROUPHOLDER_T6D44EAE647CB3F6269BCB0EF2BF51C40C3E14286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.DecorationMaterial/GroupHolder
struct  GroupHolder_t6D44EAE647CB3F6269BCB0EF2BF51C40C3E14286  : public RuntimeObject
{
public:
	// System.Boolean Kolibri2d.DecorationMaterial/GroupHolder::unfolded
	bool ___unfolded_0;
	// System.Boolean Kolibri2d.DecorationMaterial/GroupHolder::enabled
	bool ___enabled_1;
	// Kolibri2d.DecorationGroup Kolibri2d.DecorationMaterial/GroupHolder::group
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0 * ___group_2;

public:
	inline static int32_t get_offset_of_unfolded_0() { return static_cast<int32_t>(offsetof(GroupHolder_t6D44EAE647CB3F6269BCB0EF2BF51C40C3E14286, ___unfolded_0)); }
	inline bool get_unfolded_0() const { return ___unfolded_0; }
	inline bool* get_address_of_unfolded_0() { return &___unfolded_0; }
	inline void set_unfolded_0(bool value)
	{
		___unfolded_0 = value;
	}

	inline static int32_t get_offset_of_enabled_1() { return static_cast<int32_t>(offsetof(GroupHolder_t6D44EAE647CB3F6269BCB0EF2BF51C40C3E14286, ___enabled_1)); }
	inline bool get_enabled_1() const { return ___enabled_1; }
	inline bool* get_address_of_enabled_1() { return &___enabled_1; }
	inline void set_enabled_1(bool value)
	{
		___enabled_1 = value;
	}

	inline static int32_t get_offset_of_group_2() { return static_cast<int32_t>(offsetof(GroupHolder_t6D44EAE647CB3F6269BCB0EF2BF51C40C3E14286, ___group_2)); }
	inline DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0 * get_group_2() const { return ___group_2; }
	inline DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0 ** get_address_of_group_2() { return &___group_2; }
	inline void set_group_2(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0 * value)
	{
		___group_2 = value;
		Il2CppCodeGenWriteBarrier((&___group_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPHOLDER_T6D44EAE647CB3F6269BCB0EF2BF51C40C3E14286_H
#ifndef PHYSICSSETTINGS_T78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190_H
#define PHYSICSSETTINGS_T78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.PhysicsSettings
struct  PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190  : public RuntimeObject
{
public:
	// System.Boolean Kolibri2d.PhysicsSettings::enabled
	bool ___enabled_0;
	// UnityEngine.PhysicsMaterial2D Kolibri2d.PhysicsSettings::material
	PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937 * ___material_1;
	// System.Boolean Kolibri2d.PhysicsSettings::isTrigger
	bool ___isTrigger_2;
	// System.Boolean Kolibri2d.PhysicsSettings::usedByEffector
	bool ___usedByEffector_3;
	// System.Single Kolibri2d.PhysicsSettings::radius
	float ___radius_4;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_material_1() { return static_cast<int32_t>(offsetof(PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190, ___material_1)); }
	inline PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937 * get_material_1() const { return ___material_1; }
	inline PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937 ** get_address_of_material_1() { return &___material_1; }
	inline void set_material_1(PhysicsMaterial2D_t3FF87BB9D39D6B4E3C081016547FBF7EA6C46937 * value)
	{
		___material_1 = value;
		Il2CppCodeGenWriteBarrier((&___material_1), value);
	}

	inline static int32_t get_offset_of_isTrigger_2() { return static_cast<int32_t>(offsetof(PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190, ___isTrigger_2)); }
	inline bool get_isTrigger_2() const { return ___isTrigger_2; }
	inline bool* get_address_of_isTrigger_2() { return &___isTrigger_2; }
	inline void set_isTrigger_2(bool value)
	{
		___isTrigger_2 = value;
	}

	inline static int32_t get_offset_of_usedByEffector_3() { return static_cast<int32_t>(offsetof(PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190, ___usedByEffector_3)); }
	inline bool get_usedByEffector_3() const { return ___usedByEffector_3; }
	inline bool* get_address_of_usedByEffector_3() { return &___usedByEffector_3; }
	inline void set_usedByEffector_3(bool value)
	{
		___usedByEffector_3 = value;
	}

	inline static int32_t get_offset_of_radius_4() { return static_cast<int32_t>(offsetof(PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190, ___radius_4)); }
	inline float get_radius_4() const { return ___radius_4; }
	inline float* get_address_of_radius_4() { return &___radius_4; }
	inline void set_radius_4(float value)
	{
		___radius_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHYSICSSETTINGS_T78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190_H
#ifndef POLY2MESH_TF3734BC685ACFDF1B9EF78ED4A5549AE34589AE3_H
#define POLY2MESH_TF3734BC685ACFDF1B9EF78ED4A5549AE34589AE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Mesh
struct  Poly2Mesh_tF3734BC685ACFDF1B9EF78ED4A5549AE34589AE3  : public RuntimeObject
{
public:

public:
};

struct Poly2Mesh_tF3734BC685ACFDF1B9EF78ED4A5549AE34589AE3_StaticFields
{
public:
	// System.Boolean Kolibri2d.Poly2Mesh::fullDebug
	bool ___fullDebug_0;

public:
	inline static int32_t get_offset_of_fullDebug_0() { return static_cast<int32_t>(offsetof(Poly2Mesh_tF3734BC685ACFDF1B9EF78ED4A5549AE34589AE3_StaticFields, ___fullDebug_0)); }
	inline bool get_fullDebug_0() const { return ___fullDebug_0; }
	inline bool* get_address_of_fullDebug_0() { return &___fullDebug_0; }
	inline void set_fullDebug_0(bool value)
	{
		___fullDebug_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLY2MESH_TF3734BC685ACFDF1B9EF78ED4A5549AE34589AE3_H
#ifndef ADVANCINGFRONT_TBCCD1124A9FAD23592008C9B975AA05853B39C44_H
#define ADVANCINGFRONT_TBCCD1124A9FAD23592008C9B975AA05853B39C44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.AdvancingFront
struct  AdvancingFront_tBCCD1124A9FAD23592008C9B975AA05853B39C44  : public RuntimeObject
{
public:
	// Kolibri2d.Poly2Tri.AdvancingFrontNode Kolibri2d.Poly2Tri.AdvancingFront::Head
	AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * ___Head_0;
	// Kolibri2d.Poly2Tri.AdvancingFrontNode Kolibri2d.Poly2Tri.AdvancingFront::Tail
	AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * ___Tail_1;
	// Kolibri2d.Poly2Tri.AdvancingFrontNode Kolibri2d.Poly2Tri.AdvancingFront::Search
	AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * ___Search_2;

public:
	inline static int32_t get_offset_of_Head_0() { return static_cast<int32_t>(offsetof(AdvancingFront_tBCCD1124A9FAD23592008C9B975AA05853B39C44, ___Head_0)); }
	inline AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * get_Head_0() const { return ___Head_0; }
	inline AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 ** get_address_of_Head_0() { return &___Head_0; }
	inline void set_Head_0(AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * value)
	{
		___Head_0 = value;
		Il2CppCodeGenWriteBarrier((&___Head_0), value);
	}

	inline static int32_t get_offset_of_Tail_1() { return static_cast<int32_t>(offsetof(AdvancingFront_tBCCD1124A9FAD23592008C9B975AA05853B39C44, ___Tail_1)); }
	inline AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * get_Tail_1() const { return ___Tail_1; }
	inline AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 ** get_address_of_Tail_1() { return &___Tail_1; }
	inline void set_Tail_1(AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * value)
	{
		___Tail_1 = value;
		Il2CppCodeGenWriteBarrier((&___Tail_1), value);
	}

	inline static int32_t get_offset_of_Search_2() { return static_cast<int32_t>(offsetof(AdvancingFront_tBCCD1124A9FAD23592008C9B975AA05853B39C44, ___Search_2)); }
	inline AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * get_Search_2() const { return ___Search_2; }
	inline AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 ** get_address_of_Search_2() { return &___Search_2; }
	inline void set_Search_2(AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * value)
	{
		___Search_2 = value;
		Il2CppCodeGenWriteBarrier((&___Search_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVANCINGFRONT_TBCCD1124A9FAD23592008C9B975AA05853B39C44_H
#ifndef ADVANCINGFRONTNODE_T8AE38AAE0D87C80EC9F408D43A39F0F4125BF357_H
#define ADVANCINGFRONTNODE_T8AE38AAE0D87C80EC9F408D43A39F0F4125BF357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.AdvancingFrontNode
struct  AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357  : public RuntimeObject
{
public:
	// Kolibri2d.Poly2Tri.AdvancingFrontNode Kolibri2d.Poly2Tri.AdvancingFrontNode::Next
	AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * ___Next_0;
	// Kolibri2d.Poly2Tri.AdvancingFrontNode Kolibri2d.Poly2Tri.AdvancingFrontNode::Prev
	AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * ___Prev_1;
	// System.Double Kolibri2d.Poly2Tri.AdvancingFrontNode::Value
	double ___Value_2;
	// Kolibri2d.Poly2Tri.TriangulationPoint Kolibri2d.Poly2Tri.AdvancingFrontNode::Point
	TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * ___Point_3;
	// Kolibri2d.Poly2Tri.DelaunayTriangle Kolibri2d.Poly2Tri.AdvancingFrontNode::Triangle
	DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278 * ___Triangle_4;

public:
	inline static int32_t get_offset_of_Next_0() { return static_cast<int32_t>(offsetof(AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357, ___Next_0)); }
	inline AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * get_Next_0() const { return ___Next_0; }
	inline AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 ** get_address_of_Next_0() { return &___Next_0; }
	inline void set_Next_0(AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * value)
	{
		___Next_0 = value;
		Il2CppCodeGenWriteBarrier((&___Next_0), value);
	}

	inline static int32_t get_offset_of_Prev_1() { return static_cast<int32_t>(offsetof(AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357, ___Prev_1)); }
	inline AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * get_Prev_1() const { return ___Prev_1; }
	inline AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 ** get_address_of_Prev_1() { return &___Prev_1; }
	inline void set_Prev_1(AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * value)
	{
		___Prev_1 = value;
		Il2CppCodeGenWriteBarrier((&___Prev_1), value);
	}

	inline static int32_t get_offset_of_Value_2() { return static_cast<int32_t>(offsetof(AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357, ___Value_2)); }
	inline double get_Value_2() const { return ___Value_2; }
	inline double* get_address_of_Value_2() { return &___Value_2; }
	inline void set_Value_2(double value)
	{
		___Value_2 = value;
	}

	inline static int32_t get_offset_of_Point_3() { return static_cast<int32_t>(offsetof(AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357, ___Point_3)); }
	inline TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * get_Point_3() const { return ___Point_3; }
	inline TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E ** get_address_of_Point_3() { return &___Point_3; }
	inline void set_Point_3(TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * value)
	{
		___Point_3 = value;
		Il2CppCodeGenWriteBarrier((&___Point_3), value);
	}

	inline static int32_t get_offset_of_Triangle_4() { return static_cast<int32_t>(offsetof(AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357, ___Triangle_4)); }
	inline DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278 * get_Triangle_4() const { return ___Triangle_4; }
	inline DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278 ** get_address_of_Triangle_4() { return &___Triangle_4; }
	inline void set_Triangle_4(DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278 * value)
	{
		___Triangle_4 = value;
		Il2CppCodeGenWriteBarrier((&___Triangle_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVANCINGFRONTNODE_T8AE38AAE0D87C80EC9F408D43A39F0F4125BF357_H
#ifndef DTSWEEP_T34A4AE5A6C924CB276FD4C28198A27B3A251BCED_H
#define DTSWEEP_T34A4AE5A6C924CB276FD4C28198A27B3A251BCED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.DTSweep
struct  DTSweep_t34A4AE5A6C924CB276FD4C28198A27B3A251BCED  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTSWEEP_T34A4AE5A6C924CB276FD4C28198A27B3A251BCED_H
#ifndef DTSWEEPBASIN_T47769C4C8A6508E384E73DCD2ED888AEA2A782CD_H
#define DTSWEEPBASIN_T47769C4C8A6508E384E73DCD2ED888AEA2A782CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.DTSweepBasin
struct  DTSweepBasin_t47769C4C8A6508E384E73DCD2ED888AEA2A782CD  : public RuntimeObject
{
public:
	// Kolibri2d.Poly2Tri.AdvancingFrontNode Kolibri2d.Poly2Tri.DTSweepBasin::leftNode
	AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * ___leftNode_0;
	// Kolibri2d.Poly2Tri.AdvancingFrontNode Kolibri2d.Poly2Tri.DTSweepBasin::bottomNode
	AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * ___bottomNode_1;
	// Kolibri2d.Poly2Tri.AdvancingFrontNode Kolibri2d.Poly2Tri.DTSweepBasin::rightNode
	AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * ___rightNode_2;
	// System.Double Kolibri2d.Poly2Tri.DTSweepBasin::width
	double ___width_3;
	// System.Boolean Kolibri2d.Poly2Tri.DTSweepBasin::leftHighest
	bool ___leftHighest_4;

public:
	inline static int32_t get_offset_of_leftNode_0() { return static_cast<int32_t>(offsetof(DTSweepBasin_t47769C4C8A6508E384E73DCD2ED888AEA2A782CD, ___leftNode_0)); }
	inline AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * get_leftNode_0() const { return ___leftNode_0; }
	inline AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 ** get_address_of_leftNode_0() { return &___leftNode_0; }
	inline void set_leftNode_0(AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * value)
	{
		___leftNode_0 = value;
		Il2CppCodeGenWriteBarrier((&___leftNode_0), value);
	}

	inline static int32_t get_offset_of_bottomNode_1() { return static_cast<int32_t>(offsetof(DTSweepBasin_t47769C4C8A6508E384E73DCD2ED888AEA2A782CD, ___bottomNode_1)); }
	inline AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * get_bottomNode_1() const { return ___bottomNode_1; }
	inline AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 ** get_address_of_bottomNode_1() { return &___bottomNode_1; }
	inline void set_bottomNode_1(AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * value)
	{
		___bottomNode_1 = value;
		Il2CppCodeGenWriteBarrier((&___bottomNode_1), value);
	}

	inline static int32_t get_offset_of_rightNode_2() { return static_cast<int32_t>(offsetof(DTSweepBasin_t47769C4C8A6508E384E73DCD2ED888AEA2A782CD, ___rightNode_2)); }
	inline AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * get_rightNode_2() const { return ___rightNode_2; }
	inline AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 ** get_address_of_rightNode_2() { return &___rightNode_2; }
	inline void set_rightNode_2(AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * value)
	{
		___rightNode_2 = value;
		Il2CppCodeGenWriteBarrier((&___rightNode_2), value);
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(DTSweepBasin_t47769C4C8A6508E384E73DCD2ED888AEA2A782CD, ___width_3)); }
	inline double get_width_3() const { return ___width_3; }
	inline double* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(double value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_leftHighest_4() { return static_cast<int32_t>(offsetof(DTSweepBasin_t47769C4C8A6508E384E73DCD2ED888AEA2A782CD, ___leftHighest_4)); }
	inline bool get_leftHighest_4() const { return ___leftHighest_4; }
	inline bool* get_address_of_leftHighest_4() { return &___leftHighest_4; }
	inline void set_leftHighest_4(bool value)
	{
		___leftHighest_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTSWEEPBASIN_T47769C4C8A6508E384E73DCD2ED888AEA2A782CD_H
#ifndef DTSWEEPEDGEEVENT_T2BF104D3D9F78FA8F6BB2F1B63E5FFAAA9D446BC_H
#define DTSWEEPEDGEEVENT_T2BF104D3D9F78FA8F6BB2F1B63E5FFAAA9D446BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.DTSweepEdgeEvent
struct  DTSweepEdgeEvent_t2BF104D3D9F78FA8F6BB2F1B63E5FFAAA9D446BC  : public RuntimeObject
{
public:
	// Kolibri2d.Poly2Tri.DTSweepConstraint Kolibri2d.Poly2Tri.DTSweepEdgeEvent::ConstrainedEdge
	DTSweepConstraint_t4773D59AAEBA0883F8DA0C42FBD8C92350DBE594 * ___ConstrainedEdge_0;
	// System.Boolean Kolibri2d.Poly2Tri.DTSweepEdgeEvent::Right
	bool ___Right_1;

public:
	inline static int32_t get_offset_of_ConstrainedEdge_0() { return static_cast<int32_t>(offsetof(DTSweepEdgeEvent_t2BF104D3D9F78FA8F6BB2F1B63E5FFAAA9D446BC, ___ConstrainedEdge_0)); }
	inline DTSweepConstraint_t4773D59AAEBA0883F8DA0C42FBD8C92350DBE594 * get_ConstrainedEdge_0() const { return ___ConstrainedEdge_0; }
	inline DTSweepConstraint_t4773D59AAEBA0883F8DA0C42FBD8C92350DBE594 ** get_address_of_ConstrainedEdge_0() { return &___ConstrainedEdge_0; }
	inline void set_ConstrainedEdge_0(DTSweepConstraint_t4773D59AAEBA0883F8DA0C42FBD8C92350DBE594 * value)
	{
		___ConstrainedEdge_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConstrainedEdge_0), value);
	}

	inline static int32_t get_offset_of_Right_1() { return static_cast<int32_t>(offsetof(DTSweepEdgeEvent_t2BF104D3D9F78FA8F6BB2F1B63E5FFAAA9D446BC, ___Right_1)); }
	inline bool get_Right_1() const { return ___Right_1; }
	inline bool* get_address_of_Right_1() { return &___Right_1; }
	inline void set_Right_1(bool value)
	{
		___Right_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTSWEEPEDGEEVENT_T2BF104D3D9F78FA8F6BB2F1B63E5FFAAA9D446BC_H
#ifndef DTSWEEPPOINTCOMPARATOR_T030E4A9D87B78D989BCE53C4756FBC3AA4D7EA1B_H
#define DTSWEEPPOINTCOMPARATOR_T030E4A9D87B78D989BCE53C4756FBC3AA4D7EA1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.DTSweepPointComparator
struct  DTSweepPointComparator_t030E4A9D87B78D989BCE53C4756FBC3AA4D7EA1B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTSWEEPPOINTCOMPARATOR_T030E4A9D87B78D989BCE53C4756FBC3AA4D7EA1B_H
#ifndef EDGE_TFB6F8F74D888B19F8D03A97A9CA65E60889DC672_H
#define EDGE_TFB6F8F74D888B19F8D03A97A9CA65E60889DC672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.Edge
struct  Edge_tFB6F8F74D888B19F8D03A97A9CA65E60889DC672  : public RuntimeObject
{
public:
	// Kolibri2d.Poly2Tri.Point2D Kolibri2d.Poly2Tri.Edge::mP
	Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D * ___mP_0;
	// Kolibri2d.Poly2Tri.Point2D Kolibri2d.Poly2Tri.Edge::mQ
	Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D * ___mQ_1;

public:
	inline static int32_t get_offset_of_mP_0() { return static_cast<int32_t>(offsetof(Edge_tFB6F8F74D888B19F8D03A97A9CA65E60889DC672, ___mP_0)); }
	inline Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D * get_mP_0() const { return ___mP_0; }
	inline Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D ** get_address_of_mP_0() { return &___mP_0; }
	inline void set_mP_0(Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D * value)
	{
		___mP_0 = value;
		Il2CppCodeGenWriteBarrier((&___mP_0), value);
	}

	inline static int32_t get_offset_of_mQ_1() { return static_cast<int32_t>(offsetof(Edge_tFB6F8F74D888B19F8D03A97A9CA65E60889DC672, ___mQ_1)); }
	inline Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D * get_mQ_1() const { return ___mQ_1; }
	inline Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D ** get_address_of_mQ_1() { return &___mQ_1; }
	inline void set_mQ_1(Point2D_t6F40CD1103366A28DD6D6A3B2B31AC7314B32F1D * value)
	{
		___mQ_1 = value;
		Il2CppCodeGenWriteBarrier((&___mQ_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDGE_TFB6F8F74D888B19F8D03A97A9CA65E60889DC672_H
#ifndef TRIANGULATIONDEBUGCONTEXT_T23D041A57AAB2224970A201625386E8BEB37D9A4_H
#define TRIANGULATIONDEBUGCONTEXT_T23D041A57AAB2224970A201625386E8BEB37D9A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.TriangulationDebugContext
struct  TriangulationDebugContext_t23D041A57AAB2224970A201625386E8BEB37D9A4  : public RuntimeObject
{
public:
	// Kolibri2d.Poly2Tri.TriangulationContext Kolibri2d.Poly2Tri.TriangulationDebugContext::_tcx
	TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B * ____tcx_0;

public:
	inline static int32_t get_offset_of__tcx_0() { return static_cast<int32_t>(offsetof(TriangulationDebugContext_t23D041A57AAB2224970A201625386E8BEB37D9A4, ____tcx_0)); }
	inline TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B * get__tcx_0() const { return ____tcx_0; }
	inline TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B ** get_address_of__tcx_0() { return &____tcx_0; }
	inline void set__tcx_0(TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B * value)
	{
		____tcx_0 = value;
		Il2CppCodeGenWriteBarrier((&____tcx_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATIONDEBUGCONTEXT_T23D041A57AAB2224970A201625386E8BEB37D9A4_H
#ifndef SPLINECOLLIDERSETTINGS_T0665313F90EDA2B7B57385855A06973EA15213A4_H
#define SPLINECOLLIDERSETTINGS_T0665313F90EDA2B7B57385855A06973EA15213A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineColliderSettings
struct  SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4  : public RuntimeObject
{
public:
	// Kolibri2d.PhysicsSettings Kolibri2d.SplineColliderSettings::polygonCollider
	PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * ___polygonCollider_1;
	// Kolibri2d.PhysicsSettings Kolibri2d.SplineColliderSettings::edgeCollider
	PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * ___edgeCollider_2;
	// Kolibri2d.PhysicsSettings Kolibri2d.SplineColliderSettings::groundCollider
	PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * ___groundCollider_3;
	// Kolibri2d.PhysicsSettings Kolibri2d.SplineColliderSettings::wallCollider
	PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * ___wallCollider_4;
	// Kolibri2d.PhysicsSettings Kolibri2d.SplineColliderSettings::ceilingCollider
	PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * ___ceilingCollider_5;
	// System.Collections.Generic.Dictionary`2<Kolibri2d.TerrainType,Kolibri2d.PhysicsSettings> Kolibri2d.SplineColliderSettings::physicsSettingsByTerrainType
	Dictionary_2_t99ADF3EF31BE384D1A13236F275C436B11310513 * ___physicsSettingsByTerrainType_6;

public:
	inline static int32_t get_offset_of_polygonCollider_1() { return static_cast<int32_t>(offsetof(SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4, ___polygonCollider_1)); }
	inline PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * get_polygonCollider_1() const { return ___polygonCollider_1; }
	inline PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 ** get_address_of_polygonCollider_1() { return &___polygonCollider_1; }
	inline void set_polygonCollider_1(PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * value)
	{
		___polygonCollider_1 = value;
		Il2CppCodeGenWriteBarrier((&___polygonCollider_1), value);
	}

	inline static int32_t get_offset_of_edgeCollider_2() { return static_cast<int32_t>(offsetof(SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4, ___edgeCollider_2)); }
	inline PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * get_edgeCollider_2() const { return ___edgeCollider_2; }
	inline PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 ** get_address_of_edgeCollider_2() { return &___edgeCollider_2; }
	inline void set_edgeCollider_2(PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * value)
	{
		___edgeCollider_2 = value;
		Il2CppCodeGenWriteBarrier((&___edgeCollider_2), value);
	}

	inline static int32_t get_offset_of_groundCollider_3() { return static_cast<int32_t>(offsetof(SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4, ___groundCollider_3)); }
	inline PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * get_groundCollider_3() const { return ___groundCollider_3; }
	inline PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 ** get_address_of_groundCollider_3() { return &___groundCollider_3; }
	inline void set_groundCollider_3(PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * value)
	{
		___groundCollider_3 = value;
		Il2CppCodeGenWriteBarrier((&___groundCollider_3), value);
	}

	inline static int32_t get_offset_of_wallCollider_4() { return static_cast<int32_t>(offsetof(SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4, ___wallCollider_4)); }
	inline PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * get_wallCollider_4() const { return ___wallCollider_4; }
	inline PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 ** get_address_of_wallCollider_4() { return &___wallCollider_4; }
	inline void set_wallCollider_4(PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * value)
	{
		___wallCollider_4 = value;
		Il2CppCodeGenWriteBarrier((&___wallCollider_4), value);
	}

	inline static int32_t get_offset_of_ceilingCollider_5() { return static_cast<int32_t>(offsetof(SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4, ___ceilingCollider_5)); }
	inline PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * get_ceilingCollider_5() const { return ___ceilingCollider_5; }
	inline PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 ** get_address_of_ceilingCollider_5() { return &___ceilingCollider_5; }
	inline void set_ceilingCollider_5(PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190 * value)
	{
		___ceilingCollider_5 = value;
		Il2CppCodeGenWriteBarrier((&___ceilingCollider_5), value);
	}

	inline static int32_t get_offset_of_physicsSettingsByTerrainType_6() { return static_cast<int32_t>(offsetof(SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4, ___physicsSettingsByTerrainType_6)); }
	inline Dictionary_2_t99ADF3EF31BE384D1A13236F275C436B11310513 * get_physicsSettingsByTerrainType_6() const { return ___physicsSettingsByTerrainType_6; }
	inline Dictionary_2_t99ADF3EF31BE384D1A13236F275C436B11310513 ** get_address_of_physicsSettingsByTerrainType_6() { return &___physicsSettingsByTerrainType_6; }
	inline void set_physicsSettingsByTerrainType_6(Dictionary_2_t99ADF3EF31BE384D1A13236F275C436B11310513 * value)
	{
		___physicsSettingsByTerrainType_6 = value;
		Il2CppCodeGenWriteBarrier((&___physicsSettingsByTerrainType_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINECOLLIDERSETTINGS_T0665313F90EDA2B7B57385855A06973EA15213A4_H
#ifndef SPLINEDATA_T97926CA36C3A0FF54D20743F14165941B0C11747_H
#define SPLINEDATA_T97926CA36C3A0FF54D20743F14165941B0C11747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineData
struct  SplineData_t97926CA36C3A0FF54D20743F14165941B0C11747  : public RuntimeObject
{
public:
	// System.Boolean Kolibri2d.SplineData::m_IsOpenEnded
	bool ___m_IsOpenEnded_0;
	// System.Collections.Generic.List`1<Kolibri2d.SplineControlPoint> Kolibri2d.SplineData::m_ControlPoints
	List_1_t4022365DF596613EEE444DE140D4966C2BD9EA5B * ___m_ControlPoints_1;

public:
	inline static int32_t get_offset_of_m_IsOpenEnded_0() { return static_cast<int32_t>(offsetof(SplineData_t97926CA36C3A0FF54D20743F14165941B0C11747, ___m_IsOpenEnded_0)); }
	inline bool get_m_IsOpenEnded_0() const { return ___m_IsOpenEnded_0; }
	inline bool* get_address_of_m_IsOpenEnded_0() { return &___m_IsOpenEnded_0; }
	inline void set_m_IsOpenEnded_0(bool value)
	{
		___m_IsOpenEnded_0 = value;
	}

	inline static int32_t get_offset_of_m_ControlPoints_1() { return static_cast<int32_t>(offsetof(SplineData_t97926CA36C3A0FF54D20743F14165941B0C11747, ___m_ControlPoints_1)); }
	inline List_1_t4022365DF596613EEE444DE140D4966C2BD9EA5B * get_m_ControlPoints_1() const { return ___m_ControlPoints_1; }
	inline List_1_t4022365DF596613EEE444DE140D4966C2BD9EA5B ** get_address_of_m_ControlPoints_1() { return &___m_ControlPoints_1; }
	inline void set_m_ControlPoints_1(List_1_t4022365DF596613EEE444DE140D4966C2BD9EA5B * value)
	{
		___m_ControlPoints_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ControlPoints_1), value);
	}
};

struct SplineData_t97926CA36C3A0FF54D20743F14165941B0C11747_StaticFields
{
public:
	// System.Single Kolibri2d.SplineData::KEpsilon
	float ___KEpsilon_2;

public:
	inline static int32_t get_offset_of_KEpsilon_2() { return static_cast<int32_t>(offsetof(SplineData_t97926CA36C3A0FF54D20743F14165941B0C11747_StaticFields, ___KEpsilon_2)); }
	inline float get_KEpsilon_2() const { return ___KEpsilon_2; }
	inline float* get_address_of_KEpsilon_2() { return &___KEpsilon_2; }
	inline void set_KEpsilon_2(float value)
	{
		___KEpsilon_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINEDATA_T97926CA36C3A0FF54D20743F14165941B0C11747_H
#ifndef SPLINETERRAIN2DCREATOR_TB376515DA574676086ACA2E8C0914DDB27FDFBD1_H
#define SPLINETERRAIN2DCREATOR_TB376515DA574676086ACA2E8C0914DDB27FDFBD1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineTerrain2DCreator
struct  SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1  : public RuntimeObject
{
public:
	// Kolibri2d.BezierSpline Kolibri2d.SplineTerrain2DCreator::spline
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * ___spline_0;
	// Kolibri2d.Terrain2DArchitecture Kolibri2d.SplineTerrain2DCreator::arch
	Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * ___arch_1;
	// System.Collections.Generic.List`1<Kolibri2d.Terrain2D/Terrain2DMaterialHolder> Kolibri2d.SplineTerrain2DCreator::materialHolders
	List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 * ___materialHolders_2;
	// Kolibri2d.Terrain2D Kolibri2d.SplineTerrain2DCreator::terrain
	Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B * ___terrain_3;
	// System.Collections.Generic.List`1<UnityEngine.Mesh> Kolibri2d.SplineTerrain2DCreator::createdMeshes
	List_1_tBE402A53CD5331E0CB483C591CFDC8C6827F35B8 * ___createdMeshes_5;
	// System.Collections.Generic.List`1<UnityEngine.Material> Kolibri2d.SplineTerrain2DCreator::createdMaterials
	List_1_t56B616621385F653B408094FBDFA5DEC6C4B47EA * ___createdMaterials_6;
	// System.Single[] Kolibri2d.SplineTerrain2DCreator::distancePerPoint
	SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* ___distancePerPoint_7;

public:
	inline static int32_t get_offset_of_spline_0() { return static_cast<int32_t>(offsetof(SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1, ___spline_0)); }
	inline BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * get_spline_0() const { return ___spline_0; }
	inline BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE ** get_address_of_spline_0() { return &___spline_0; }
	inline void set_spline_0(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * value)
	{
		___spline_0 = value;
		Il2CppCodeGenWriteBarrier((&___spline_0), value);
	}

	inline static int32_t get_offset_of_arch_1() { return static_cast<int32_t>(offsetof(SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1, ___arch_1)); }
	inline Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * get_arch_1() const { return ___arch_1; }
	inline Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A ** get_address_of_arch_1() { return &___arch_1; }
	inline void set_arch_1(Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * value)
	{
		___arch_1 = value;
		Il2CppCodeGenWriteBarrier((&___arch_1), value);
	}

	inline static int32_t get_offset_of_materialHolders_2() { return static_cast<int32_t>(offsetof(SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1, ___materialHolders_2)); }
	inline List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 * get_materialHolders_2() const { return ___materialHolders_2; }
	inline List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 ** get_address_of_materialHolders_2() { return &___materialHolders_2; }
	inline void set_materialHolders_2(List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 * value)
	{
		___materialHolders_2 = value;
		Il2CppCodeGenWriteBarrier((&___materialHolders_2), value);
	}

	inline static int32_t get_offset_of_terrain_3() { return static_cast<int32_t>(offsetof(SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1, ___terrain_3)); }
	inline Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B * get_terrain_3() const { return ___terrain_3; }
	inline Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B ** get_address_of_terrain_3() { return &___terrain_3; }
	inline void set_terrain_3(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B * value)
	{
		___terrain_3 = value;
		Il2CppCodeGenWriteBarrier((&___terrain_3), value);
	}

	inline static int32_t get_offset_of_createdMeshes_5() { return static_cast<int32_t>(offsetof(SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1, ___createdMeshes_5)); }
	inline List_1_tBE402A53CD5331E0CB483C591CFDC8C6827F35B8 * get_createdMeshes_5() const { return ___createdMeshes_5; }
	inline List_1_tBE402A53CD5331E0CB483C591CFDC8C6827F35B8 ** get_address_of_createdMeshes_5() { return &___createdMeshes_5; }
	inline void set_createdMeshes_5(List_1_tBE402A53CD5331E0CB483C591CFDC8C6827F35B8 * value)
	{
		___createdMeshes_5 = value;
		Il2CppCodeGenWriteBarrier((&___createdMeshes_5), value);
	}

	inline static int32_t get_offset_of_createdMaterials_6() { return static_cast<int32_t>(offsetof(SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1, ___createdMaterials_6)); }
	inline List_1_t56B616621385F653B408094FBDFA5DEC6C4B47EA * get_createdMaterials_6() const { return ___createdMaterials_6; }
	inline List_1_t56B616621385F653B408094FBDFA5DEC6C4B47EA ** get_address_of_createdMaterials_6() { return &___createdMaterials_6; }
	inline void set_createdMaterials_6(List_1_t56B616621385F653B408094FBDFA5DEC6C4B47EA * value)
	{
		___createdMaterials_6 = value;
		Il2CppCodeGenWriteBarrier((&___createdMaterials_6), value);
	}

	inline static int32_t get_offset_of_distancePerPoint_7() { return static_cast<int32_t>(offsetof(SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1, ___distancePerPoint_7)); }
	inline SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* get_distancePerPoint_7() const { return ___distancePerPoint_7; }
	inline SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5** get_address_of_distancePerPoint_7() { return &___distancePerPoint_7; }
	inline void set_distancePerPoint_7(SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* value)
	{
		___distancePerPoint_7 = value;
		Il2CppCodeGenWriteBarrier((&___distancePerPoint_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINETERRAIN2DCREATOR_TB376515DA574676086ACA2E8C0914DDB27FDFBD1_H
#ifndef SORTINGLAYEROPTIONS2_TBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE_H
#define SORTINGLAYEROPTIONS2_TBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2DMaterial/SortingLayerOptions2
struct  SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE  : public RuntimeObject
{
public:
	// System.Int32 Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::groundLayer
	int32_t ___groundLayer_0;
	// System.Int32 Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::groundOrder
	int32_t ___groundOrder_1;
	// System.Int32 Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::wallLayer
	int32_t ___wallLayer_2;
	// System.Int32 Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::wallOrder
	int32_t ___wallOrder_3;
	// System.Int32 Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::ceilingLayer
	int32_t ___ceilingLayer_4;
	// System.Int32 Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::ceilingOrder
	int32_t ___ceilingOrder_5;
	// System.Int32 Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::fillLayer
	int32_t ___fillLayer_6;
	// System.Int32 Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::fillOrder
	int32_t ___fillOrder_7;
	// System.Int32 Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::cornersLayer
	int32_t ___cornersLayer_8;
	// System.Int32 Kolibri2d.Terrain2DMaterial/SortingLayerOptions2::cornerOrder
	int32_t ___cornerOrder_9;

public:
	inline static int32_t get_offset_of_groundLayer_0() { return static_cast<int32_t>(offsetof(SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE, ___groundLayer_0)); }
	inline int32_t get_groundLayer_0() const { return ___groundLayer_0; }
	inline int32_t* get_address_of_groundLayer_0() { return &___groundLayer_0; }
	inline void set_groundLayer_0(int32_t value)
	{
		___groundLayer_0 = value;
	}

	inline static int32_t get_offset_of_groundOrder_1() { return static_cast<int32_t>(offsetof(SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE, ___groundOrder_1)); }
	inline int32_t get_groundOrder_1() const { return ___groundOrder_1; }
	inline int32_t* get_address_of_groundOrder_1() { return &___groundOrder_1; }
	inline void set_groundOrder_1(int32_t value)
	{
		___groundOrder_1 = value;
	}

	inline static int32_t get_offset_of_wallLayer_2() { return static_cast<int32_t>(offsetof(SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE, ___wallLayer_2)); }
	inline int32_t get_wallLayer_2() const { return ___wallLayer_2; }
	inline int32_t* get_address_of_wallLayer_2() { return &___wallLayer_2; }
	inline void set_wallLayer_2(int32_t value)
	{
		___wallLayer_2 = value;
	}

	inline static int32_t get_offset_of_wallOrder_3() { return static_cast<int32_t>(offsetof(SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE, ___wallOrder_3)); }
	inline int32_t get_wallOrder_3() const { return ___wallOrder_3; }
	inline int32_t* get_address_of_wallOrder_3() { return &___wallOrder_3; }
	inline void set_wallOrder_3(int32_t value)
	{
		___wallOrder_3 = value;
	}

	inline static int32_t get_offset_of_ceilingLayer_4() { return static_cast<int32_t>(offsetof(SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE, ___ceilingLayer_4)); }
	inline int32_t get_ceilingLayer_4() const { return ___ceilingLayer_4; }
	inline int32_t* get_address_of_ceilingLayer_4() { return &___ceilingLayer_4; }
	inline void set_ceilingLayer_4(int32_t value)
	{
		___ceilingLayer_4 = value;
	}

	inline static int32_t get_offset_of_ceilingOrder_5() { return static_cast<int32_t>(offsetof(SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE, ___ceilingOrder_5)); }
	inline int32_t get_ceilingOrder_5() const { return ___ceilingOrder_5; }
	inline int32_t* get_address_of_ceilingOrder_5() { return &___ceilingOrder_5; }
	inline void set_ceilingOrder_5(int32_t value)
	{
		___ceilingOrder_5 = value;
	}

	inline static int32_t get_offset_of_fillLayer_6() { return static_cast<int32_t>(offsetof(SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE, ___fillLayer_6)); }
	inline int32_t get_fillLayer_6() const { return ___fillLayer_6; }
	inline int32_t* get_address_of_fillLayer_6() { return &___fillLayer_6; }
	inline void set_fillLayer_6(int32_t value)
	{
		___fillLayer_6 = value;
	}

	inline static int32_t get_offset_of_fillOrder_7() { return static_cast<int32_t>(offsetof(SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE, ___fillOrder_7)); }
	inline int32_t get_fillOrder_7() const { return ___fillOrder_7; }
	inline int32_t* get_address_of_fillOrder_7() { return &___fillOrder_7; }
	inline void set_fillOrder_7(int32_t value)
	{
		___fillOrder_7 = value;
	}

	inline static int32_t get_offset_of_cornersLayer_8() { return static_cast<int32_t>(offsetof(SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE, ___cornersLayer_8)); }
	inline int32_t get_cornersLayer_8() const { return ___cornersLayer_8; }
	inline int32_t* get_address_of_cornersLayer_8() { return &___cornersLayer_8; }
	inline void set_cornersLayer_8(int32_t value)
	{
		___cornersLayer_8 = value;
	}

	inline static int32_t get_offset_of_cornerOrder_9() { return static_cast<int32_t>(offsetof(SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE, ___cornerOrder_9)); }
	inline int32_t get_cornerOrder_9() const { return ___cornerOrder_9; }
	inline int32_t* get_address_of_cornerOrder_9() { return &___cornerOrder_9; }
	inline void set_cornerOrder_9(int32_t value)
	{
		___cornerOrder_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTINGLAYEROPTIONS2_TBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE_H
#ifndef SORTINGZ_3D_OPTIONS_TFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F_H
#define SORTINGZ_3D_OPTIONS_TFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2DMaterial/SortingZ_3D_Options
struct  SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F  : public RuntimeObject
{
public:
	// System.Single Kolibri2d.Terrain2DMaterial/SortingZ_3D_Options::groundZ
	float ___groundZ_0;
	// System.Single Kolibri2d.Terrain2DMaterial/SortingZ_3D_Options::wallZ
	float ___wallZ_1;
	// System.Single Kolibri2d.Terrain2DMaterial/SortingZ_3D_Options::ceilingZ
	float ___ceilingZ_2;
	// System.Single Kolibri2d.Terrain2DMaterial/SortingZ_3D_Options::fillZ
	float ___fillZ_3;
	// System.Single Kolibri2d.Terrain2DMaterial/SortingZ_3D_Options::cornerZ
	float ___cornerZ_4;

public:
	inline static int32_t get_offset_of_groundZ_0() { return static_cast<int32_t>(offsetof(SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F, ___groundZ_0)); }
	inline float get_groundZ_0() const { return ___groundZ_0; }
	inline float* get_address_of_groundZ_0() { return &___groundZ_0; }
	inline void set_groundZ_0(float value)
	{
		___groundZ_0 = value;
	}

	inline static int32_t get_offset_of_wallZ_1() { return static_cast<int32_t>(offsetof(SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F, ___wallZ_1)); }
	inline float get_wallZ_1() const { return ___wallZ_1; }
	inline float* get_address_of_wallZ_1() { return &___wallZ_1; }
	inline void set_wallZ_1(float value)
	{
		___wallZ_1 = value;
	}

	inline static int32_t get_offset_of_ceilingZ_2() { return static_cast<int32_t>(offsetof(SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F, ___ceilingZ_2)); }
	inline float get_ceilingZ_2() const { return ___ceilingZ_2; }
	inline float* get_address_of_ceilingZ_2() { return &___ceilingZ_2; }
	inline void set_ceilingZ_2(float value)
	{
		___ceilingZ_2 = value;
	}

	inline static int32_t get_offset_of_fillZ_3() { return static_cast<int32_t>(offsetof(SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F, ___fillZ_3)); }
	inline float get_fillZ_3() const { return ___fillZ_3; }
	inline float* get_address_of_fillZ_3() { return &___fillZ_3; }
	inline void set_fillZ_3(float value)
	{
		___fillZ_3 = value;
	}

	inline static int32_t get_offset_of_cornerZ_4() { return static_cast<int32_t>(offsetof(SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F, ___cornerZ_4)); }
	inline float get_cornerZ_4() const { return ___cornerZ_4; }
	inline float* get_address_of_cornerZ_4() { return &___cornerZ_4; }
	inline void set_cornerZ_4(float value)
	{
		___cornerZ_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTINGZ_3D_OPTIONS_TFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F_H
#ifndef MESHHELPER2_T51BC2BE07E689574784F85C458B76F5361726DC2_H
#define MESHHELPER2_T51BC2BE07E689574784F85C458B76F5361726DC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Utils/MeshHelper2
struct  MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2  : public RuntimeObject
{
public:

public:
};

struct MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Kolibri2d.Utils/MeshHelper2::vertices
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___vertices_0;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Kolibri2d.Utils/MeshHelper2::normals
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___normals_1;
	// System.Collections.Generic.List`1<UnityEngine.Color> Kolibri2d.Utils/MeshHelper2::colors
	List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * ___colors_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Kolibri2d.Utils/MeshHelper2::uv
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___uv_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Kolibri2d.Utils/MeshHelper2::uv1
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___uv1_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Kolibri2d.Utils/MeshHelper2::uv2
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___uv2_5;
	// System.Collections.Generic.List`1<System.Int32> Kolibri2d.Utils/MeshHelper2::indices
	List_1_t4A961510635950236678F1B3B2436ECCAF28713A * ___indices_6;
	// System.Collections.Generic.Dictionary`2<System.UInt32,System.Int32> Kolibri2d.Utils/MeshHelper2::newVectices
	Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F * ___newVectices_7;

public:
	inline static int32_t get_offset_of_vertices_0() { return static_cast<int32_t>(offsetof(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields, ___vertices_0)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_vertices_0() const { return ___vertices_0; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_vertices_0() { return &___vertices_0; }
	inline void set_vertices_0(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___vertices_0 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_0), value);
	}

	inline static int32_t get_offset_of_normals_1() { return static_cast<int32_t>(offsetof(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields, ___normals_1)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_normals_1() const { return ___normals_1; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_normals_1() { return &___normals_1; }
	inline void set_normals_1(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___normals_1 = value;
		Il2CppCodeGenWriteBarrier((&___normals_1), value);
	}

	inline static int32_t get_offset_of_colors_2() { return static_cast<int32_t>(offsetof(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields, ___colors_2)); }
	inline List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * get_colors_2() const { return ___colors_2; }
	inline List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E ** get_address_of_colors_2() { return &___colors_2; }
	inline void set_colors_2(List_1_t09EDD5CC4DC14E68FF0E1328525D8174E14A5B0E * value)
	{
		___colors_2 = value;
		Il2CppCodeGenWriteBarrier((&___colors_2), value);
	}

	inline static int32_t get_offset_of_uv_3() { return static_cast<int32_t>(offsetof(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields, ___uv_3)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_uv_3() const { return ___uv_3; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_uv_3() { return &___uv_3; }
	inline void set_uv_3(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___uv_3 = value;
		Il2CppCodeGenWriteBarrier((&___uv_3), value);
	}

	inline static int32_t get_offset_of_uv1_4() { return static_cast<int32_t>(offsetof(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields, ___uv1_4)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_uv1_4() const { return ___uv1_4; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_uv1_4() { return &___uv1_4; }
	inline void set_uv1_4(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___uv1_4 = value;
		Il2CppCodeGenWriteBarrier((&___uv1_4), value);
	}

	inline static int32_t get_offset_of_uv2_5() { return static_cast<int32_t>(offsetof(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields, ___uv2_5)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_uv2_5() const { return ___uv2_5; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_uv2_5() { return &___uv2_5; }
	inline void set_uv2_5(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___uv2_5 = value;
		Il2CppCodeGenWriteBarrier((&___uv2_5), value);
	}

	inline static int32_t get_offset_of_indices_6() { return static_cast<int32_t>(offsetof(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields, ___indices_6)); }
	inline List_1_t4A961510635950236678F1B3B2436ECCAF28713A * get_indices_6() const { return ___indices_6; }
	inline List_1_t4A961510635950236678F1B3B2436ECCAF28713A ** get_address_of_indices_6() { return &___indices_6; }
	inline void set_indices_6(List_1_t4A961510635950236678F1B3B2436ECCAF28713A * value)
	{
		___indices_6 = value;
		Il2CppCodeGenWriteBarrier((&___indices_6), value);
	}

	inline static int32_t get_offset_of_newVectices_7() { return static_cast<int32_t>(offsetof(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields, ___newVectices_7)); }
	inline Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F * get_newVectices_7() const { return ___newVectices_7; }
	inline Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F ** get_address_of_newVectices_7() { return &___newVectices_7; }
	inline void set_newVectices_7(Dictionary_2_t6F999E1DBEE168573A7FC8688595C749C148353F * value)
	{
		___newVectices_7 = value;
		Il2CppCodeGenWriteBarrier((&___newVectices_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHHELPER2_T51BC2BE07E689574784F85C458B76F5361726DC2_H
#ifndef ATTRIBUTE_T60F25EB48D5935E4C6C2BAF7F90F57A43528E469_H
#define ATTRIBUTE_T60F25EB48D5935E4C6C2BAF7F90F57A43528E469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t60F25EB48D5935E4C6C2BAF7F90F57A43528E469  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T60F25EB48D5935E4C6C2BAF7F90F57A43528E469_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#define VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_com
{
};
#endif // VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifndef ANALYTICSSESSIONINFO_TE075F764A74D2B095CFD57F3B179397F504B7D8C_H
#define ANALYTICSSESSIONINFO_TE075F764A74D2B095CFD57F3B179397F504B7D8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionInfo
struct  AnalyticsSessionInfo_tE075F764A74D2B095CFD57F3B179397F504B7D8C  : public RuntimeObject
{
public:

public:
};

struct AnalyticsSessionInfo_tE075F764A74D2B095CFD57F3B179397F504B7D8C_StaticFields
{
public:
	// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged UnityEngine.Analytics.AnalyticsSessionInfo::sessionStateChanged
	SessionStateChanged_t9084549A636BD45086D66CC6765DA8C3DD31066F * ___sessionStateChanged_0;

public:
	inline static int32_t get_offset_of_sessionStateChanged_0() { return static_cast<int32_t>(offsetof(AnalyticsSessionInfo_tE075F764A74D2B095CFD57F3B179397F504B7D8C_StaticFields, ___sessionStateChanged_0)); }
	inline SessionStateChanged_t9084549A636BD45086D66CC6765DA8C3DD31066F * get_sessionStateChanged_0() const { return ___sessionStateChanged_0; }
	inline SessionStateChanged_t9084549A636BD45086D66CC6765DA8C3DD31066F ** get_address_of_sessionStateChanged_0() { return &___sessionStateChanged_0; }
	inline void set_sessionStateChanged_0(SessionStateChanged_t9084549A636BD45086D66CC6765DA8C3DD31066F * value)
	{
		___sessionStateChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___sessionStateChanged_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSSESSIONINFO_TE075F764A74D2B095CFD57F3B179397F504B7D8C_H
#ifndef CONTINUOUSEVENT_TBAB6336255F3FC327CBA03CE368CD4D8D027107A_H
#define CONTINUOUSEVENT_TBAB6336255F3FC327CBA03CE368CD4D8D027107A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ContinuousEvent
struct  ContinuousEvent_tBAB6336255F3FC327CBA03CE368CD4D8D027107A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTINUOUSEVENT_TBAB6336255F3FC327CBA03CE368CD4D8D027107A_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef RECTTRANSFORMUTILITY_T9B90669A72B05A33DD88BEBB817BC9CDBB614BBA_H
#define RECTTRANSFORMUTILITY_T9B90669A72B05A33DD88BEBB817BC9CDBB614BBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransformUtility
struct  RectTransformUtility_t9B90669A72B05A33DD88BEBB817BC9CDBB614BBA  : public RuntimeObject
{
public:

public:
};

struct RectTransformUtility_t9B90669A72B05A33DD88BEBB817BC9CDBB614BBA_StaticFields
{
public:
	// UnityEngine.Vector3[] UnityEngine.RectTransformUtility::s_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___s_Corners_0;

public:
	inline static int32_t get_offset_of_s_Corners_0() { return static_cast<int32_t>(offsetof(RectTransformUtility_t9B90669A72B05A33DD88BEBB817BC9CDBB614BBA_StaticFields, ___s_Corners_0)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_s_Corners_0() const { return ___s_Corners_0; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_s_Corners_0() { return &___s_Corners_0; }
	inline void set_s_Corners_0(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___s_Corners_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Corners_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMUTILITY_T9B90669A72B05A33DD88BEBB817BC9CDBB614BBA_H
#ifndef REMOTESETTINGS_T3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_H
#define REMOTESETTINGS_T3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RemoteSettings
struct  RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED  : public RuntimeObject
{
public:

public:
};

struct RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_StaticFields
{
public:
	// UnityEngine.RemoteSettings/UpdatedEventHandler UnityEngine.RemoteSettings::Updated
	UpdatedEventHandler_tB0230BC83686D7126AB4D3800A66351028CA514F * ___Updated_0;
	// System.Action UnityEngine.RemoteSettings::BeforeFetchFromServer
	Action_tC9F24EBA5075EA0322C6E0DC6BE2C7BDF962FB21 * ___BeforeFetchFromServer_1;
	// System.Action`3<System.Boolean,System.Boolean,System.Int32> UnityEngine.RemoteSettings::Completed
	Action_3_tEE1FB0623176AFA5109FAA9BA7E82293445CAE1E * ___Completed_2;

public:
	inline static int32_t get_offset_of_Updated_0() { return static_cast<int32_t>(offsetof(RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_StaticFields, ___Updated_0)); }
	inline UpdatedEventHandler_tB0230BC83686D7126AB4D3800A66351028CA514F * get_Updated_0() const { return ___Updated_0; }
	inline UpdatedEventHandler_tB0230BC83686D7126AB4D3800A66351028CA514F ** get_address_of_Updated_0() { return &___Updated_0; }
	inline void set_Updated_0(UpdatedEventHandler_tB0230BC83686D7126AB4D3800A66351028CA514F * value)
	{
		___Updated_0 = value;
		Il2CppCodeGenWriteBarrier((&___Updated_0), value);
	}

	inline static int32_t get_offset_of_BeforeFetchFromServer_1() { return static_cast<int32_t>(offsetof(RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_StaticFields, ___BeforeFetchFromServer_1)); }
	inline Action_tC9F24EBA5075EA0322C6E0DC6BE2C7BDF962FB21 * get_BeforeFetchFromServer_1() const { return ___BeforeFetchFromServer_1; }
	inline Action_tC9F24EBA5075EA0322C6E0DC6BE2C7BDF962FB21 ** get_address_of_BeforeFetchFromServer_1() { return &___BeforeFetchFromServer_1; }
	inline void set_BeforeFetchFromServer_1(Action_tC9F24EBA5075EA0322C6E0DC6BE2C7BDF962FB21 * value)
	{
		___BeforeFetchFromServer_1 = value;
		Il2CppCodeGenWriteBarrier((&___BeforeFetchFromServer_1), value);
	}

	inline static int32_t get_offset_of_Completed_2() { return static_cast<int32_t>(offsetof(RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_StaticFields, ___Completed_2)); }
	inline Action_3_tEE1FB0623176AFA5109FAA9BA7E82293445CAE1E * get_Completed_2() const { return ___Completed_2; }
	inline Action_3_tEE1FB0623176AFA5109FAA9BA7E82293445CAE1E ** get_address_of_Completed_2() { return &___Completed_2; }
	inline void set_Completed_2(Action_3_tEE1FB0623176AFA5109FAA9BA7E82293445CAE1E * value)
	{
		___Completed_2 = value;
		Il2CppCodeGenWriteBarrier((&___Completed_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTESETTINGS_T3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_H
#ifndef UISYSTEMPROFILERAPI_TD33E558B2D0176096E5DB375956ACA9F03678F1B_H
#define UISYSTEMPROFILERAPI_TD33E558B2D0176096E5DB375956ACA9F03678F1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UISystemProfilerApi
struct  UISystemProfilerApi_tD33E558B2D0176096E5DB375956ACA9F03678F1B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISYSTEMPROFILERAPI_TD33E558B2D0176096E5DB375956ACA9F03678F1B_H
#ifndef WEBREQUESTUTILS_TBE8F8607E3A9633419968F6AF2F706A029AE1296_H
#define WEBREQUESTUTILS_TBE8F8607E3A9633419968F6AF2F706A029AE1296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineInternal.WebRequestUtils
struct  WebRequestUtils_tBE8F8607E3A9633419968F6AF2F706A029AE1296  : public RuntimeObject
{
public:

public:
};

struct WebRequestUtils_tBE8F8607E3A9633419968F6AF2F706A029AE1296_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex UnityEngineInternal.WebRequestUtils::domainRegex
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B * ___domainRegex_0;

public:
	inline static int32_t get_offset_of_domainRegex_0() { return static_cast<int32_t>(offsetof(WebRequestUtils_tBE8F8607E3A9633419968F6AF2F706A029AE1296_StaticFields, ___domainRegex_0)); }
	inline Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B * get_domainRegex_0() const { return ___domainRegex_0; }
	inline Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B ** get_address_of_domainRegex_0() { return &___domainRegex_0; }
	inline void set_domainRegex_0(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B * value)
	{
		___domainRegex_0 = value;
		Il2CppCodeGenWriteBarrier((&___domainRegex_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTUTILS_TBE8F8607E3A9633419968F6AF2F706A029AE1296_H
#ifndef DTSWEEPDEBUGCONTEXT_TB6641E7B9644367EFA38339F39BEFE4B8403746F_H
#define DTSWEEPDEBUGCONTEXT_TB6641E7B9644367EFA38339F39BEFE4B8403746F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.DTSweepDebugContext
struct  DTSweepDebugContext_tB6641E7B9644367EFA38339F39BEFE4B8403746F  : public TriangulationDebugContext_t23D041A57AAB2224970A201625386E8BEB37D9A4
{
public:
	// Kolibri2d.Poly2Tri.DelaunayTriangle Kolibri2d.Poly2Tri.DTSweepDebugContext::_primaryTriangle
	DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278 * ____primaryTriangle_1;
	// Kolibri2d.Poly2Tri.DelaunayTriangle Kolibri2d.Poly2Tri.DTSweepDebugContext::_secondaryTriangle
	DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278 * ____secondaryTriangle_2;
	// Kolibri2d.Poly2Tri.TriangulationPoint Kolibri2d.Poly2Tri.DTSweepDebugContext::_activePoint
	TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * ____activePoint_3;
	// Kolibri2d.Poly2Tri.AdvancingFrontNode Kolibri2d.Poly2Tri.DTSweepDebugContext::_activeNode
	AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * ____activeNode_4;
	// Kolibri2d.Poly2Tri.DTSweepConstraint Kolibri2d.Poly2Tri.DTSweepDebugContext::_activeConstraint
	DTSweepConstraint_t4773D59AAEBA0883F8DA0C42FBD8C92350DBE594 * ____activeConstraint_5;

public:
	inline static int32_t get_offset_of__primaryTriangle_1() { return static_cast<int32_t>(offsetof(DTSweepDebugContext_tB6641E7B9644367EFA38339F39BEFE4B8403746F, ____primaryTriangle_1)); }
	inline DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278 * get__primaryTriangle_1() const { return ____primaryTriangle_1; }
	inline DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278 ** get_address_of__primaryTriangle_1() { return &____primaryTriangle_1; }
	inline void set__primaryTriangle_1(DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278 * value)
	{
		____primaryTriangle_1 = value;
		Il2CppCodeGenWriteBarrier((&____primaryTriangle_1), value);
	}

	inline static int32_t get_offset_of__secondaryTriangle_2() { return static_cast<int32_t>(offsetof(DTSweepDebugContext_tB6641E7B9644367EFA38339F39BEFE4B8403746F, ____secondaryTriangle_2)); }
	inline DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278 * get__secondaryTriangle_2() const { return ____secondaryTriangle_2; }
	inline DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278 ** get_address_of__secondaryTriangle_2() { return &____secondaryTriangle_2; }
	inline void set__secondaryTriangle_2(DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278 * value)
	{
		____secondaryTriangle_2 = value;
		Il2CppCodeGenWriteBarrier((&____secondaryTriangle_2), value);
	}

	inline static int32_t get_offset_of__activePoint_3() { return static_cast<int32_t>(offsetof(DTSweepDebugContext_tB6641E7B9644367EFA38339F39BEFE4B8403746F, ____activePoint_3)); }
	inline TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * get__activePoint_3() const { return ____activePoint_3; }
	inline TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E ** get_address_of__activePoint_3() { return &____activePoint_3; }
	inline void set__activePoint_3(TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * value)
	{
		____activePoint_3 = value;
		Il2CppCodeGenWriteBarrier((&____activePoint_3), value);
	}

	inline static int32_t get_offset_of__activeNode_4() { return static_cast<int32_t>(offsetof(DTSweepDebugContext_tB6641E7B9644367EFA38339F39BEFE4B8403746F, ____activeNode_4)); }
	inline AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * get__activeNode_4() const { return ____activeNode_4; }
	inline AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 ** get_address_of__activeNode_4() { return &____activeNode_4; }
	inline void set__activeNode_4(AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357 * value)
	{
		____activeNode_4 = value;
		Il2CppCodeGenWriteBarrier((&____activeNode_4), value);
	}

	inline static int32_t get_offset_of__activeConstraint_5() { return static_cast<int32_t>(offsetof(DTSweepDebugContext_tB6641E7B9644367EFA38339F39BEFE4B8403746F, ____activeConstraint_5)); }
	inline DTSweepConstraint_t4773D59AAEBA0883F8DA0C42FBD8C92350DBE594 * get__activeConstraint_5() const { return ____activeConstraint_5; }
	inline DTSweepConstraint_t4773D59AAEBA0883F8DA0C42FBD8C92350DBE594 ** get_address_of__activeConstraint_5() { return &____activeConstraint_5; }
	inline void set__activeConstraint_5(DTSweepConstraint_t4773D59AAEBA0883F8DA0C42FBD8C92350DBE594 * value)
	{
		____activeConstraint_5 = value;
		Il2CppCodeGenWriteBarrier((&____activeConstraint_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTSWEEPDEBUGCONTEXT_TB6641E7B9644367EFA38339F39BEFE4B8403746F_H
#ifndef FIXEDARRAY3_1_TD8FD99C6FC0343C04B9710364EFD34F68CB159D9_H
#define FIXEDARRAY3_1_TD8FD99C6FC0343C04B9710364EFD34F68CB159D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.FixedArray3`1<Kolibri2d.Poly2Tri.DelaunayTriangle>
struct  FixedArray3_1_tD8FD99C6FC0343C04B9710364EFD34F68CB159D9 
{
public:
	// T Kolibri2d.Poly2Tri.FixedArray3`1::_0
	DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278 * ____0_0;
	// T Kolibri2d.Poly2Tri.FixedArray3`1::_1
	DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278 * ____1_1;
	// T Kolibri2d.Poly2Tri.FixedArray3`1::_2
	DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278 * ____2_2;

public:
	inline static int32_t get_offset_of__0_0() { return static_cast<int32_t>(offsetof(FixedArray3_1_tD8FD99C6FC0343C04B9710364EFD34F68CB159D9, ____0_0)); }
	inline DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278 * get__0_0() const { return ____0_0; }
	inline DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278 ** get_address_of__0_0() { return &____0_0; }
	inline void set__0_0(DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278 * value)
	{
		____0_0 = value;
		Il2CppCodeGenWriteBarrier((&____0_0), value);
	}

	inline static int32_t get_offset_of__1_1() { return static_cast<int32_t>(offsetof(FixedArray3_1_tD8FD99C6FC0343C04B9710364EFD34F68CB159D9, ____1_1)); }
	inline DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278 * get__1_1() const { return ____1_1; }
	inline DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278 ** get_address_of__1_1() { return &____1_1; }
	inline void set__1_1(DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278 * value)
	{
		____1_1 = value;
		Il2CppCodeGenWriteBarrier((&____1_1), value);
	}

	inline static int32_t get_offset_of__2_2() { return static_cast<int32_t>(offsetof(FixedArray3_1_tD8FD99C6FC0343C04B9710364EFD34F68CB159D9, ____2_2)); }
	inline DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278 * get__2_2() const { return ____2_2; }
	inline DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278 ** get_address_of__2_2() { return &____2_2; }
	inline void set__2_2(DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278 * value)
	{
		____2_2 = value;
		Il2CppCodeGenWriteBarrier((&____2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDARRAY3_1_TD8FD99C6FC0343C04B9710364EFD34F68CB159D9_H
#ifndef FIXEDARRAY3_1_T47C52BD9040451A725A1EE8F9930190136F1D7B7_H
#define FIXEDARRAY3_1_T47C52BD9040451A725A1EE8F9930190136F1D7B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.FixedArray3`1<Kolibri2d.Poly2Tri.TriangulationPoint>
struct  FixedArray3_1_t47C52BD9040451A725A1EE8F9930190136F1D7B7 
{
public:
	// T Kolibri2d.Poly2Tri.FixedArray3`1::_0
	TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * ____0_0;
	// T Kolibri2d.Poly2Tri.FixedArray3`1::_1
	TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * ____1_1;
	// T Kolibri2d.Poly2Tri.FixedArray3`1::_2
	TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * ____2_2;

public:
	inline static int32_t get_offset_of__0_0() { return static_cast<int32_t>(offsetof(FixedArray3_1_t47C52BD9040451A725A1EE8F9930190136F1D7B7, ____0_0)); }
	inline TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * get__0_0() const { return ____0_0; }
	inline TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E ** get_address_of__0_0() { return &____0_0; }
	inline void set__0_0(TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * value)
	{
		____0_0 = value;
		Il2CppCodeGenWriteBarrier((&____0_0), value);
	}

	inline static int32_t get_offset_of__1_1() { return static_cast<int32_t>(offsetof(FixedArray3_1_t47C52BD9040451A725A1EE8F9930190136F1D7B7, ____1_1)); }
	inline TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * get__1_1() const { return ____1_1; }
	inline TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E ** get_address_of__1_1() { return &____1_1; }
	inline void set__1_1(TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * value)
	{
		____1_1 = value;
		Il2CppCodeGenWriteBarrier((&____1_1), value);
	}

	inline static int32_t get_offset_of__2_2() { return static_cast<int32_t>(offsetof(FixedArray3_1_t47C52BD9040451A725A1EE8F9930190136F1D7B7, ____2_2)); }
	inline TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * get__2_2() const { return ____2_2; }
	inline TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E ** get_address_of__2_2() { return &____2_2; }
	inline void set__2_2(TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * value)
	{
		____2_2 = value;
		Il2CppCodeGenWriteBarrier((&____2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDARRAY3_1_T47C52BD9040451A725A1EE8F9930190136F1D7B7_H
#ifndef FIXEDBITARRAY3_T4B1347148FCA790BDC0EE6FD932E04291F68382D_H
#define FIXEDBITARRAY3_T4B1347148FCA790BDC0EE6FD932E04291F68382D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.FixedBitArray3
struct  FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D 
{
public:
	// System.Boolean Kolibri2d.Poly2Tri.FixedBitArray3::_0
	bool ____0_0;
	// System.Boolean Kolibri2d.Poly2Tri.FixedBitArray3::_1
	bool ____1_1;
	// System.Boolean Kolibri2d.Poly2Tri.FixedBitArray3::_2
	bool ____2_2;

public:
	inline static int32_t get_offset_of__0_0() { return static_cast<int32_t>(offsetof(FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D, ____0_0)); }
	inline bool get__0_0() const { return ____0_0; }
	inline bool* get_address_of__0_0() { return &____0_0; }
	inline void set__0_0(bool value)
	{
		____0_0 = value;
	}

	inline static int32_t get_offset_of__1_1() { return static_cast<int32_t>(offsetof(FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D, ____1_1)); }
	inline bool get__1_1() const { return ____1_1; }
	inline bool* get_address_of__1_1() { return &____1_1; }
	inline void set__1_1(bool value)
	{
		____1_1 = value;
	}

	inline static int32_t get_offset_of__2_2() { return static_cast<int32_t>(offsetof(FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D, ____2_2)); }
	inline bool get__2_2() const { return ____2_2; }
	inline bool* get_address_of__2_2() { return &____2_2; }
	inline void set__2_2(bool value)
	{
		____2_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Kolibri2d.Poly2Tri.FixedBitArray3
struct FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D_marshaled_pinvoke
{
	int32_t ____0_0;
	int32_t ____1_1;
	int32_t ____2_2;
};
// Native definition for COM marshalling of Kolibri2d.Poly2Tri.FixedBitArray3
struct FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D_marshaled_com
{
	int32_t ____0_0;
	int32_t ____1_1;
	int32_t ____2_2;
};
#endif // FIXEDBITARRAY3_T4B1347148FCA790BDC0EE6FD932E04291F68382D_H
#ifndef TRIANGULATIONCONSTRAINT_T1A7BE017E0DA33DF3619D2E27A1EA9E4A6A3C14C_H
#define TRIANGULATIONCONSTRAINT_T1A7BE017E0DA33DF3619D2E27A1EA9E4A6A3C14C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.TriangulationConstraint
struct  TriangulationConstraint_t1A7BE017E0DA33DF3619D2E27A1EA9E4A6A3C14C  : public Edge_tFB6F8F74D888B19F8D03A97A9CA65E60889DC672
{
public:
	// System.UInt32 Kolibri2d.Poly2Tri.TriangulationConstraint::mContraintCode
	uint32_t ___mContraintCode_2;

public:
	inline static int32_t get_offset_of_mContraintCode_2() { return static_cast<int32_t>(offsetof(TriangulationConstraint_t1A7BE017E0DA33DF3619D2E27A1EA9E4A6A3C14C, ___mContraintCode_2)); }
	inline uint32_t get_mContraintCode_2() const { return ___mContraintCode_2; }
	inline uint32_t* get_address_of_mContraintCode_2() { return &___mContraintCode_2; }
	inline void set_mContraintCode_2(uint32_t value)
	{
		___mContraintCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATIONCONSTRAINT_T1A7BE017E0DA33DF3619D2E27A1EA9E4A6A3C14C_H
#ifndef BOOLEAN_T92E4792324DA9B716F339A3B965A14965E99A4EF_H
#define BOOLEAN_T92E4792324DA9B716F339A3B965A14965E99A4EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t92E4792324DA9B716F339A3B965A14965E99A4EF 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t92E4792324DA9B716F339A3B965A14965E99A4EF, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t92E4792324DA9B716F339A3B965A14965E99A4EF_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t92E4792324DA9B716F339A3B965A14965E99A4EF_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t92E4792324DA9B716F339A3B965A14965E99A4EF_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T92E4792324DA9B716F339A3B965A14965E99A4EF_H
#ifndef ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#define ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF  : public ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C
{
public:

public:
};

struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_com
{
};
#endif // ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifndef INT64_TF61270729FC90F8A705A5FA6FE222C9644374ADF_H
#define INT64_TF61270729FC90F8A705A5FA6FE222C9644374ADF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_tF61270729FC90F8A705A5FA6FE222C9644374ADF 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_tF61270729FC90F8A705A5FA6FE222C9644374ADF, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_TF61270729FC90F8A705A5FA6FE222C9644374ADF_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SYSTEMEXCEPTION_T81A06ADD519539D252788D75E44AFFBE6580E301_H
#define SYSTEMEXCEPTION_T81A06ADD519539D252788D75E44AFFBE6580E301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t81A06ADD519539D252788D75E44AFFBE6580E301  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T81A06ADD519539D252788D75E44AFFBE6580E301_H
#ifndef VOID_TDB81A15FA2AB53E2401A76B745D215397B29F783_H
#define VOID_TDB81A15FA2AB53E2401A76B745D215397B29F783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_tDB81A15FA2AB53E2401A76B745D215397B29F783 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_TDB81A15FA2AB53E2401A76B745D215397B29F783_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef UNITYEVENT_1_T7839A0014FFD3A212A87547A44A7719D6549ED87_H
#define UNITYEVENT_1_T7839A0014FFD3A212A87547A44A7719D6549ED87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T7839A0014FFD3A212A87547A44A7719D6549ED87_H
#ifndef PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#define PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54  : public Attribute_t60F25EB48D5935E4C6C2BAF7F90F57A43528E469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef STATE_T7D2976DAE446125618B7DB82F6ED3048082E388D_H
#define STATE_T7D2976DAE446125618B7DB82F6ED3048082E388D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Random/State
struct  State_t7D2976DAE446125618B7DB82F6ED3048082E388D 
{
public:
	// System.Int32 UnityEngine.Random/State::s0
	int32_t ___s0_0;
	// System.Int32 UnityEngine.Random/State::s1
	int32_t ___s1_1;
	// System.Int32 UnityEngine.Random/State::s2
	int32_t ___s2_2;
	// System.Int32 UnityEngine.Random/State::s3
	int32_t ___s3_3;

public:
	inline static int32_t get_offset_of_s0_0() { return static_cast<int32_t>(offsetof(State_t7D2976DAE446125618B7DB82F6ED3048082E388D, ___s0_0)); }
	inline int32_t get_s0_0() const { return ___s0_0; }
	inline int32_t* get_address_of_s0_0() { return &___s0_0; }
	inline void set_s0_0(int32_t value)
	{
		___s0_0 = value;
	}

	inline static int32_t get_offset_of_s1_1() { return static_cast<int32_t>(offsetof(State_t7D2976DAE446125618B7DB82F6ED3048082E388D, ___s1_1)); }
	inline int32_t get_s1_1() const { return ___s1_1; }
	inline int32_t* get_address_of_s1_1() { return &___s1_1; }
	inline void set_s1_1(int32_t value)
	{
		___s1_1 = value;
	}

	inline static int32_t get_offset_of_s2_2() { return static_cast<int32_t>(offsetof(State_t7D2976DAE446125618B7DB82F6ED3048082E388D, ___s2_2)); }
	inline int32_t get_s2_2() const { return ___s2_2; }
	inline int32_t* get_address_of_s2_2() { return &___s2_2; }
	inline void set_s2_2(int32_t value)
	{
		___s2_2 = value;
	}

	inline static int32_t get_offset_of_s3_3() { return static_cast<int32_t>(offsetof(State_t7D2976DAE446125618B7DB82F6ED3048082E388D, ___s3_3)); }
	inline int32_t get_s3_3() const { return ___s3_3; }
	inline int32_t* get_address_of_s3_3() { return &___s3_3; }
	inline void set_s3_3(int32_t value)
	{
		___s3_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T7D2976DAE446125618B7DB82F6ED3048082E388D_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef BEZIERCONTROLPOINTMODE_T193A371F3CAA9EE612A584D1117BAFCA998D28C1_H
#define BEZIERCONTROLPOINTMODE_T193A371F3CAA9EE612A584D1117BAFCA998D28C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.BezierControlPointMode
struct  BezierControlPointMode_t193A371F3CAA9EE612A584D1117BAFCA998D28C1 
{
public:
	// System.Int32 Kolibri2d.BezierControlPointMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BezierControlPointMode_t193A371F3CAA9EE612A584D1117BAFCA998D28C1, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEZIERCONTROLPOINTMODE_T193A371F3CAA9EE612A584D1117BAFCA998D28C1_H
#ifndef POINTTYPE_TA15C051FEF502E0AB63B5CE0330A71DCE2189E34_H
#define POINTTYPE_TA15C051FEF502E0AB63B5CE0330A71DCE2189E34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.BezierSpline/PointType
struct  PointType_tA15C051FEF502E0AB63B5CE0330A71DCE2189E34 
{
public:
	// System.Int32 Kolibri2d.BezierSpline/PointType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PointType_tA15C051FEF502E0AB63B5CE0330A71DCE2189E34, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTTYPE_TA15C051FEF502E0AB63B5CE0330A71DCE2189E34_H
#ifndef QUALITYLEVEL_T134FE67DABCBA0D9D03CFC9C993315F0F55122B2_H
#define QUALITYLEVEL_T134FE67DABCBA0D9D03CFC9C993315F0F55122B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.BezierSplineQuality/QualityLevel
struct  QualityLevel_t134FE67DABCBA0D9D03CFC9C993315F0F55122B2 
{
public:
	// System.Int32 Kolibri2d.BezierSplineQuality/QualityLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(QualityLevel_t134FE67DABCBA0D9D03CFC9C993315F0F55122B2, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUALITYLEVEL_T134FE67DABCBA0D9D03CFC9C993315F0F55122B2_H
#ifndef SUBDIVISIONMODE_T1941AE7CBC2B923FCE3A36DA75996F5B81E4AC61_H
#define SUBDIVISIONMODE_T1941AE7CBC2B923FCE3A36DA75996F5B81E4AC61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.BezierSplineQuality/SubdivisionMode
struct  SubdivisionMode_t1941AE7CBC2B923FCE3A36DA75996F5B81E4AC61 
{
public:
	// System.Int32 Kolibri2d.BezierSplineQuality/SubdivisionMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SubdivisionMode_t1941AE7CBC2B923FCE3A36DA75996F5B81E4AC61, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBDIVISIONMODE_T1941AE7CBC2B923FCE3A36DA75996F5B81E4AC61_H
#ifndef DECORATION_T250E65EB1BF343D4A3EDACAABBC4608D99C2F159_H
#define DECORATION_T250E65EB1BF343D4A3EDACAABBC4608D99C2F159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Decoration
struct  Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159  : public RuntimeObject
{
public:
	// System.String Kolibri2d.Decoration::name
	String_t* ___name_4;
	// System.Boolean Kolibri2d.Decoration::allDefault
	bool ___allDefault_5;
	// UnityEngine.Sprite Kolibri2d.Decoration::spr
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___spr_6;
	// UnityEngine.GameObject Kolibri2d.Decoration::objToInstantiate
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___objToInstantiate_7;
	// UnityEngine.Color Kolibri2d.Decoration::tint
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___tint_8;
	// System.Boolean Kolibri2d.Decoration::useTint
	bool ___useTint_9;
	// System.Boolean Kolibri2d.Decoration::defaultRotation
	bool ___defaultRotation_10;
	// System.Single Kolibri2d.Decoration::maxRotation
	float ___maxRotation_11;
	// System.Boolean Kolibri2d.Decoration::defaultScale
	bool ___defaultScale_12;
	// System.Single Kolibri2d.Decoration::minScale
	float ___minScale_13;
	// System.Single Kolibri2d.Decoration::maxScale
	float ___maxScale_14;
	// System.Single Kolibri2d.Decoration::offsetY
	float ___offsetY_15;
	// System.Boolean Kolibri2d.Decoration::randomFlipX
	bool ___randomFlipX_16;

public:
	inline static int32_t get_offset_of_name_4() { return static_cast<int32_t>(offsetof(Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159, ___name_4)); }
	inline String_t* get_name_4() const { return ___name_4; }
	inline String_t** get_address_of_name_4() { return &___name_4; }
	inline void set_name_4(String_t* value)
	{
		___name_4 = value;
		Il2CppCodeGenWriteBarrier((&___name_4), value);
	}

	inline static int32_t get_offset_of_allDefault_5() { return static_cast<int32_t>(offsetof(Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159, ___allDefault_5)); }
	inline bool get_allDefault_5() const { return ___allDefault_5; }
	inline bool* get_address_of_allDefault_5() { return &___allDefault_5; }
	inline void set_allDefault_5(bool value)
	{
		___allDefault_5 = value;
	}

	inline static int32_t get_offset_of_spr_6() { return static_cast<int32_t>(offsetof(Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159, ___spr_6)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_spr_6() const { return ___spr_6; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_spr_6() { return &___spr_6; }
	inline void set_spr_6(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___spr_6 = value;
		Il2CppCodeGenWriteBarrier((&___spr_6), value);
	}

	inline static int32_t get_offset_of_objToInstantiate_7() { return static_cast<int32_t>(offsetof(Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159, ___objToInstantiate_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_objToInstantiate_7() const { return ___objToInstantiate_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_objToInstantiate_7() { return &___objToInstantiate_7; }
	inline void set_objToInstantiate_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___objToInstantiate_7 = value;
		Il2CppCodeGenWriteBarrier((&___objToInstantiate_7), value);
	}

	inline static int32_t get_offset_of_tint_8() { return static_cast<int32_t>(offsetof(Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159, ___tint_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_tint_8() const { return ___tint_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_tint_8() { return &___tint_8; }
	inline void set_tint_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___tint_8 = value;
	}

	inline static int32_t get_offset_of_useTint_9() { return static_cast<int32_t>(offsetof(Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159, ___useTint_9)); }
	inline bool get_useTint_9() const { return ___useTint_9; }
	inline bool* get_address_of_useTint_9() { return &___useTint_9; }
	inline void set_useTint_9(bool value)
	{
		___useTint_9 = value;
	}

	inline static int32_t get_offset_of_defaultRotation_10() { return static_cast<int32_t>(offsetof(Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159, ___defaultRotation_10)); }
	inline bool get_defaultRotation_10() const { return ___defaultRotation_10; }
	inline bool* get_address_of_defaultRotation_10() { return &___defaultRotation_10; }
	inline void set_defaultRotation_10(bool value)
	{
		___defaultRotation_10 = value;
	}

	inline static int32_t get_offset_of_maxRotation_11() { return static_cast<int32_t>(offsetof(Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159, ___maxRotation_11)); }
	inline float get_maxRotation_11() const { return ___maxRotation_11; }
	inline float* get_address_of_maxRotation_11() { return &___maxRotation_11; }
	inline void set_maxRotation_11(float value)
	{
		___maxRotation_11 = value;
	}

	inline static int32_t get_offset_of_defaultScale_12() { return static_cast<int32_t>(offsetof(Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159, ___defaultScale_12)); }
	inline bool get_defaultScale_12() const { return ___defaultScale_12; }
	inline bool* get_address_of_defaultScale_12() { return &___defaultScale_12; }
	inline void set_defaultScale_12(bool value)
	{
		___defaultScale_12 = value;
	}

	inline static int32_t get_offset_of_minScale_13() { return static_cast<int32_t>(offsetof(Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159, ___minScale_13)); }
	inline float get_minScale_13() const { return ___minScale_13; }
	inline float* get_address_of_minScale_13() { return &___minScale_13; }
	inline void set_minScale_13(float value)
	{
		___minScale_13 = value;
	}

	inline static int32_t get_offset_of_maxScale_14() { return static_cast<int32_t>(offsetof(Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159, ___maxScale_14)); }
	inline float get_maxScale_14() const { return ___maxScale_14; }
	inline float* get_address_of_maxScale_14() { return &___maxScale_14; }
	inline void set_maxScale_14(float value)
	{
		___maxScale_14 = value;
	}

	inline static int32_t get_offset_of_offsetY_15() { return static_cast<int32_t>(offsetof(Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159, ___offsetY_15)); }
	inline float get_offsetY_15() const { return ___offsetY_15; }
	inline float* get_address_of_offsetY_15() { return &___offsetY_15; }
	inline void set_offsetY_15(float value)
	{
		___offsetY_15 = value;
	}

	inline static int32_t get_offset_of_randomFlipX_16() { return static_cast<int32_t>(offsetof(Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159, ___randomFlipX_16)); }
	inline bool get_randomFlipX_16() const { return ___randomFlipX_16; }
	inline bool* get_address_of_randomFlipX_16() { return &___randomFlipX_16; }
	inline void set_randomFlipX_16(bool value)
	{
		___randomFlipX_16 = value;
	}
};

struct Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159_StaticFields
{
public:
	// System.Single Kolibri2d.Decoration::DEFAULT_MIN_SCALE
	float ___DEFAULT_MIN_SCALE_0;
	// System.Single Kolibri2d.Decoration::DEFAULT_MAX_SCALE
	float ___DEFAULT_MAX_SCALE_1;
	// System.Single Kolibri2d.Decoration::DEFAULT_MAX_ROTATION
	float ___DEFAULT_MAX_ROTATION_2;
	// System.Single Kolibri2d.Decoration::DEFAULT_OFFSET_Y
	float ___DEFAULT_OFFSET_Y_3;

public:
	inline static int32_t get_offset_of_DEFAULT_MIN_SCALE_0() { return static_cast<int32_t>(offsetof(Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159_StaticFields, ___DEFAULT_MIN_SCALE_0)); }
	inline float get_DEFAULT_MIN_SCALE_0() const { return ___DEFAULT_MIN_SCALE_0; }
	inline float* get_address_of_DEFAULT_MIN_SCALE_0() { return &___DEFAULT_MIN_SCALE_0; }
	inline void set_DEFAULT_MIN_SCALE_0(float value)
	{
		___DEFAULT_MIN_SCALE_0 = value;
	}

	inline static int32_t get_offset_of_DEFAULT_MAX_SCALE_1() { return static_cast<int32_t>(offsetof(Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159_StaticFields, ___DEFAULT_MAX_SCALE_1)); }
	inline float get_DEFAULT_MAX_SCALE_1() const { return ___DEFAULT_MAX_SCALE_1; }
	inline float* get_address_of_DEFAULT_MAX_SCALE_1() { return &___DEFAULT_MAX_SCALE_1; }
	inline void set_DEFAULT_MAX_SCALE_1(float value)
	{
		___DEFAULT_MAX_SCALE_1 = value;
	}

	inline static int32_t get_offset_of_DEFAULT_MAX_ROTATION_2() { return static_cast<int32_t>(offsetof(Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159_StaticFields, ___DEFAULT_MAX_ROTATION_2)); }
	inline float get_DEFAULT_MAX_ROTATION_2() const { return ___DEFAULT_MAX_ROTATION_2; }
	inline float* get_address_of_DEFAULT_MAX_ROTATION_2() { return &___DEFAULT_MAX_ROTATION_2; }
	inline void set_DEFAULT_MAX_ROTATION_2(float value)
	{
		___DEFAULT_MAX_ROTATION_2 = value;
	}

	inline static int32_t get_offset_of_DEFAULT_OFFSET_Y_3() { return static_cast<int32_t>(offsetof(Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159_StaticFields, ___DEFAULT_OFFSET_Y_3)); }
	inline float get_DEFAULT_OFFSET_Y_3() const { return ___DEFAULT_OFFSET_Y_3; }
	inline float* get_address_of_DEFAULT_OFFSET_Y_3() { return &___DEFAULT_OFFSET_Y_3; }
	inline void set_DEFAULT_OFFSET_Y_3(float value)
	{
		___DEFAULT_OFFSET_Y_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECORATION_T250E65EB1BF343D4A3EDACAABBC4608D99C2F159_H
#ifndef DISTRIBUTIONTYPE_TC7078E73ECB6855CFC58B052FB24A0B2B9B31602_H
#define DISTRIBUTIONTYPE_TC7078E73ECB6855CFC58B052FB24A0B2B9B31602_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.DecorationGroup/DistributionType
struct  DistributionType_tC7078E73ECB6855CFC58B052FB24A0B2B9B31602 
{
public:
	// System.Int32 Kolibri2d.DecorationGroup/DistributionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DistributionType_tC7078E73ECB6855CFC58B052FB24A0B2B9B31602, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTRIBUTIONTYPE_TC7078E73ECB6855CFC58B052FB24A0B2B9B31602_H
#ifndef ORDERTYPE_TBDCF237B9AC437D5BEDFD0744D52109576047CB0_H
#define ORDERTYPE_TBDCF237B9AC437D5BEDFD0744D52109576047CB0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.DecorationGroup/OrderType
struct  OrderType_tBDCF237B9AC437D5BEDFD0744D52109576047CB0 
{
public:
	// System.Int32 Kolibri2d.DecorationGroup/OrderType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OrderType_tBDCF237B9AC437D5BEDFD0744D52109576047CB0, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORDERTYPE_TBDCF237B9AC437D5BEDFD0744D52109576047CB0_H
#ifndef RENDERORDERTYPE_T4DD74BC3AC697D444EEF0D713C65FCB3218866B7_H
#define RENDERORDERTYPE_T4DD74BC3AC697D444EEF0D713C65FCB3218866B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.DecorationGroup/RenderOrderType
struct  RenderOrderType_t4DD74BC3AC697D444EEF0D713C65FCB3218866B7 
{
public:
	// System.Int32 Kolibri2d.DecorationGroup/RenderOrderType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderOrderType_t4DD74BC3AC697D444EEF0D713C65FCB3218866B7, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERORDERTYPE_T4DD74BC3AC697D444EEF0D713C65FCB3218866B7_H
#ifndef LAYERATTRIBUTE_T01EB463B71CDEFB2B871266893F7BB44157A1E3E_H
#define LAYERATTRIBUTE_T01EB463B71CDEFB2B871266893F7BB44157A1E3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.LayerAttribute
struct  LayerAttribute_t01EB463B71CDEFB2B871266893F7BB44157A1E3E  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERATTRIBUTE_T01EB463B71CDEFB2B871266893F7BB44157A1E3E_H
#ifndef MATERIALASSETINFO_TF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF_H
#define MATERIALASSETINFO_TF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.MaterialAssetInfo
struct  MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF 
{
public:
	// UnityEngine.Sprite Kolibri2d.MaterialAssetInfo::sprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___sprite_0;
	// UnityEngine.GameObject Kolibri2d.MaterialAssetInfo::gameObj
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___gameObj_1;
	// UnityEngine.Vector3 Kolibri2d.MaterialAssetInfo::scale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___scale_2;
	// UnityEngine.Vector3 Kolibri2d.MaterialAssetInfo::offset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___offset_3;
	// System.Single Kolibri2d.MaterialAssetInfo::rotationAngle
	float ___rotationAngle_4;
	// System.Int32 Kolibri2d.MaterialAssetInfo::orderInLayer
	int32_t ___orderInLayer_5;
	// System.Int32 Kolibri2d.MaterialAssetInfo::layer2D
	int32_t ___layer2D_6;

public:
	inline static int32_t get_offset_of_sprite_0() { return static_cast<int32_t>(offsetof(MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF, ___sprite_0)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_sprite_0() const { return ___sprite_0; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_sprite_0() { return &___sprite_0; }
	inline void set_sprite_0(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___sprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___sprite_0), value);
	}

	inline static int32_t get_offset_of_gameObj_1() { return static_cast<int32_t>(offsetof(MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF, ___gameObj_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_gameObj_1() const { return ___gameObj_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_gameObj_1() { return &___gameObj_1; }
	inline void set_gameObj_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___gameObj_1 = value;
		Il2CppCodeGenWriteBarrier((&___gameObj_1), value);
	}

	inline static int32_t get_offset_of_scale_2() { return static_cast<int32_t>(offsetof(MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF, ___scale_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_scale_2() const { return ___scale_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_scale_2() { return &___scale_2; }
	inline void set_scale_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___scale_2 = value;
	}

	inline static int32_t get_offset_of_offset_3() { return static_cast<int32_t>(offsetof(MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF, ___offset_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_offset_3() const { return ___offset_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_offset_3() { return &___offset_3; }
	inline void set_offset_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___offset_3 = value;
	}

	inline static int32_t get_offset_of_rotationAngle_4() { return static_cast<int32_t>(offsetof(MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF, ___rotationAngle_4)); }
	inline float get_rotationAngle_4() const { return ___rotationAngle_4; }
	inline float* get_address_of_rotationAngle_4() { return &___rotationAngle_4; }
	inline void set_rotationAngle_4(float value)
	{
		___rotationAngle_4 = value;
	}

	inline static int32_t get_offset_of_orderInLayer_5() { return static_cast<int32_t>(offsetof(MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF, ___orderInLayer_5)); }
	inline int32_t get_orderInLayer_5() const { return ___orderInLayer_5; }
	inline int32_t* get_address_of_orderInLayer_5() { return &___orderInLayer_5; }
	inline void set_orderInLayer_5(int32_t value)
	{
		___orderInLayer_5 = value;
	}

	inline static int32_t get_offset_of_layer2D_6() { return static_cast<int32_t>(offsetof(MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF, ___layer2D_6)); }
	inline int32_t get_layer2D_6() const { return ___layer2D_6; }
	inline int32_t* get_address_of_layer2D_6() { return &___layer2D_6; }
	inline void set_layer2D_6(int32_t value)
	{
		___layer2D_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Kolibri2d.MaterialAssetInfo
struct MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF_marshaled_pinvoke
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___sprite_0;
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___gameObj_1;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___scale_2;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___offset_3;
	float ___rotationAngle_4;
	int32_t ___orderInLayer_5;
	int32_t ___layer2D_6;
};
// Native definition for COM marshalling of Kolibri2d.MaterialAssetInfo
struct MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF_marshaled_com
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___sprite_0;
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___gameObj_1;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___scale_2;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___offset_3;
	float ___rotationAngle_4;
	int32_t ___orderInLayer_5;
	int32_t ___layer2D_6;
};
#endif // MATERIALASSETINFO_TF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF_H
#ifndef ROTATIONMODE_T931B059AC9C559A27F74EAD84F6702CCEA711106_H
#define ROTATIONMODE_T931B059AC9C559A27F74EAD84F6702CCEA711106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.MaterialTextureInfo/RotationMode
struct  RotationMode_t931B059AC9C559A27F74EAD84F6702CCEA711106 
{
public:
	// System.Int32 Kolibri2d.MaterialTextureInfo/RotationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RotationMode_t931B059AC9C559A27F74EAD84F6702CCEA711106, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONMODE_T931B059AC9C559A27F74EAD84F6702CCEA711106_H
#ifndef SPRITESTRETCHTYPE_TE3931A80B38F8AE955979B13406A9B99BE0F570D_H
#define SPRITESTRETCHTYPE_TE3931A80B38F8AE955979B13406A9B99BE0F570D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.MaterialTextureInfo/SpriteStretchType
struct  SpriteStretchType_tE3931A80B38F8AE955979B13406A9B99BE0F570D 
{
public:
	// System.Int32 Kolibri2d.MaterialTextureInfo/SpriteStretchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SpriteStretchType_tE3931A80B38F8AE955979B13406A9B99BE0F570D, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITESTRETCHTYPE_TE3931A80B38F8AE955979B13406A9B99BE0F570D_H
#ifndef POLYGON_T094AFEF6E70FF97C794634E7C64A409BC965C0DC_H
#define POLYGON_T094AFEF6E70FF97C794634E7C64A409BC965C0DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Mesh/Polygon
struct  Polygon_t094AFEF6E70FF97C794634E7C64A409BC965C0DC  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Kolibri2d.Poly2Mesh/Polygon::outside
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___outside_0;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector3>> Kolibri2d.Poly2Mesh/Polygon::holes
	List_1_t6C5629FA82F6D17869213B9E74068E1F03FE8B53 * ___holes_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Kolibri2d.Poly2Mesh/Polygon::outsideUVs
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___outsideUVs_2;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector2>> Kolibri2d.Poly2Mesh/Polygon::holesUVs
	List_1_tF10DA541010813ED923C2FC487CDFC2FEE36A92E * ___holesUVs_3;
	// UnityEngine.Vector3 Kolibri2d.Poly2Mesh/Polygon::planeNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___planeNormal_4;
	// UnityEngine.Quaternion Kolibri2d.Poly2Mesh/Polygon::rotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation_5;

public:
	inline static int32_t get_offset_of_outside_0() { return static_cast<int32_t>(offsetof(Polygon_t094AFEF6E70FF97C794634E7C64A409BC965C0DC, ___outside_0)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_outside_0() const { return ___outside_0; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_outside_0() { return &___outside_0; }
	inline void set_outside_0(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___outside_0 = value;
		Il2CppCodeGenWriteBarrier((&___outside_0), value);
	}

	inline static int32_t get_offset_of_holes_1() { return static_cast<int32_t>(offsetof(Polygon_t094AFEF6E70FF97C794634E7C64A409BC965C0DC, ___holes_1)); }
	inline List_1_t6C5629FA82F6D17869213B9E74068E1F03FE8B53 * get_holes_1() const { return ___holes_1; }
	inline List_1_t6C5629FA82F6D17869213B9E74068E1F03FE8B53 ** get_address_of_holes_1() { return &___holes_1; }
	inline void set_holes_1(List_1_t6C5629FA82F6D17869213B9E74068E1F03FE8B53 * value)
	{
		___holes_1 = value;
		Il2CppCodeGenWriteBarrier((&___holes_1), value);
	}

	inline static int32_t get_offset_of_outsideUVs_2() { return static_cast<int32_t>(offsetof(Polygon_t094AFEF6E70FF97C794634E7C64A409BC965C0DC, ___outsideUVs_2)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_outsideUVs_2() const { return ___outsideUVs_2; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_outsideUVs_2() { return &___outsideUVs_2; }
	inline void set_outsideUVs_2(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___outsideUVs_2 = value;
		Il2CppCodeGenWriteBarrier((&___outsideUVs_2), value);
	}

	inline static int32_t get_offset_of_holesUVs_3() { return static_cast<int32_t>(offsetof(Polygon_t094AFEF6E70FF97C794634E7C64A409BC965C0DC, ___holesUVs_3)); }
	inline List_1_tF10DA541010813ED923C2FC487CDFC2FEE36A92E * get_holesUVs_3() const { return ___holesUVs_3; }
	inline List_1_tF10DA541010813ED923C2FC487CDFC2FEE36A92E ** get_address_of_holesUVs_3() { return &___holesUVs_3; }
	inline void set_holesUVs_3(List_1_tF10DA541010813ED923C2FC487CDFC2FEE36A92E * value)
	{
		___holesUVs_3 = value;
		Il2CppCodeGenWriteBarrier((&___holesUVs_3), value);
	}

	inline static int32_t get_offset_of_planeNormal_4() { return static_cast<int32_t>(offsetof(Polygon_t094AFEF6E70FF97C794634E7C64A409BC965C0DC, ___planeNormal_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_planeNormal_4() const { return ___planeNormal_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_planeNormal_4() { return &___planeNormal_4; }
	inline void set_planeNormal_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___planeNormal_4 = value;
	}

	inline static int32_t get_offset_of_rotation_5() { return static_cast<int32_t>(offsetof(Polygon_t094AFEF6E70FF97C794634E7C64A409BC965C0DC, ___rotation_5)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_rotation_5() const { return ___rotation_5; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_rotation_5() { return &___rotation_5; }
	inline void set_rotation_5(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___rotation_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGON_T094AFEF6E70FF97C794634E7C64A409BC965C0DC_H
#ifndef DTSWEEPCONSTRAINT_T4773D59AAEBA0883F8DA0C42FBD8C92350DBE594_H
#define DTSWEEPCONSTRAINT_T4773D59AAEBA0883F8DA0C42FBD8C92350DBE594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.DTSweepConstraint
struct  DTSweepConstraint_t4773D59AAEBA0883F8DA0C42FBD8C92350DBE594  : public TriangulationConstraint_t1A7BE017E0DA33DF3619D2E27A1EA9E4A6A3C14C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTSWEEPCONSTRAINT_T4773D59AAEBA0883F8DA0C42FBD8C92350DBE594_H
#ifndef DELAUNAYTRIANGLE_TA052B6ED441F17213517B154298074815CA75278_H
#define DELAUNAYTRIANGLE_TA052B6ED441F17213517B154298074815CA75278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.DelaunayTriangle
struct  DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278  : public RuntimeObject
{
public:
	// Kolibri2d.Poly2Tri.FixedArray3`1<Kolibri2d.Poly2Tri.TriangulationPoint> Kolibri2d.Poly2Tri.DelaunayTriangle::Points
	FixedArray3_1_t47C52BD9040451A725A1EE8F9930190136F1D7B7  ___Points_0;
	// Kolibri2d.Poly2Tri.FixedArray3`1<Kolibri2d.Poly2Tri.DelaunayTriangle> Kolibri2d.Poly2Tri.DelaunayTriangle::Neighbors
	FixedArray3_1_tD8FD99C6FC0343C04B9710364EFD34F68CB159D9  ___Neighbors_1;
	// Kolibri2d.Poly2Tri.FixedBitArray3 Kolibri2d.Poly2Tri.DelaunayTriangle::mEdgeIsConstrained
	FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D  ___mEdgeIsConstrained_2;
	// Kolibri2d.Poly2Tri.FixedBitArray3 Kolibri2d.Poly2Tri.DelaunayTriangle::EdgeIsDelaunay
	FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D  ___EdgeIsDelaunay_3;
	// System.Boolean Kolibri2d.Poly2Tri.DelaunayTriangle::<IsInterior>k__BackingField
	bool ___U3CIsInteriorU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_Points_0() { return static_cast<int32_t>(offsetof(DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278, ___Points_0)); }
	inline FixedArray3_1_t47C52BD9040451A725A1EE8F9930190136F1D7B7  get_Points_0() const { return ___Points_0; }
	inline FixedArray3_1_t47C52BD9040451A725A1EE8F9930190136F1D7B7 * get_address_of_Points_0() { return &___Points_0; }
	inline void set_Points_0(FixedArray3_1_t47C52BD9040451A725A1EE8F9930190136F1D7B7  value)
	{
		___Points_0 = value;
	}

	inline static int32_t get_offset_of_Neighbors_1() { return static_cast<int32_t>(offsetof(DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278, ___Neighbors_1)); }
	inline FixedArray3_1_tD8FD99C6FC0343C04B9710364EFD34F68CB159D9  get_Neighbors_1() const { return ___Neighbors_1; }
	inline FixedArray3_1_tD8FD99C6FC0343C04B9710364EFD34F68CB159D9 * get_address_of_Neighbors_1() { return &___Neighbors_1; }
	inline void set_Neighbors_1(FixedArray3_1_tD8FD99C6FC0343C04B9710364EFD34F68CB159D9  value)
	{
		___Neighbors_1 = value;
	}

	inline static int32_t get_offset_of_mEdgeIsConstrained_2() { return static_cast<int32_t>(offsetof(DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278, ___mEdgeIsConstrained_2)); }
	inline FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D  get_mEdgeIsConstrained_2() const { return ___mEdgeIsConstrained_2; }
	inline FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D * get_address_of_mEdgeIsConstrained_2() { return &___mEdgeIsConstrained_2; }
	inline void set_mEdgeIsConstrained_2(FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D  value)
	{
		___mEdgeIsConstrained_2 = value;
	}

	inline static int32_t get_offset_of_EdgeIsDelaunay_3() { return static_cast<int32_t>(offsetof(DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278, ___EdgeIsDelaunay_3)); }
	inline FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D  get_EdgeIsDelaunay_3() const { return ___EdgeIsDelaunay_3; }
	inline FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D * get_address_of_EdgeIsDelaunay_3() { return &___EdgeIsDelaunay_3; }
	inline void set_EdgeIsDelaunay_3(FixedBitArray3_t4B1347148FCA790BDC0EE6FD932E04291F68382D  value)
	{
		___EdgeIsDelaunay_3 = value;
	}

	inline static int32_t get_offset_of_U3CIsInteriorU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278, ___U3CIsInteriorU3Ek__BackingField_4)); }
	inline bool get_U3CIsInteriorU3Ek__BackingField_4() const { return ___U3CIsInteriorU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsInteriorU3Ek__BackingField_4() { return &___U3CIsInteriorU3Ek__BackingField_4; }
	inline void set_U3CIsInteriorU3Ek__BackingField_4(bool value)
	{
		___U3CIsInteriorU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELAUNAYTRIANGLE_TA052B6ED441F17213517B154298074815CA75278_H
#ifndef ORIENTATION_T644FD42C9B5AA65D53079A04EBF0ACABF42D779D_H
#define ORIENTATION_T644FD42C9B5AA65D53079A04EBF0ACABF42D779D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.Orientation
struct  Orientation_t644FD42C9B5AA65D53079A04EBF0ACABF42D779D 
{
public:
	// System.Int32 Kolibri2d.Poly2Tri.Orientation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Orientation_t644FD42C9B5AA65D53079A04EBF0ACABF42D779D, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTATION_T644FD42C9B5AA65D53079A04EBF0ACABF42D779D_H
#ifndef WINDINGORDERTYPE_TF976AF91C062E008942E81B1895115A047A1CB0A_H
#define WINDINGORDERTYPE_TF976AF91C062E008942E81B1895115A047A1CB0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.Point2DList/WindingOrderType
struct  WindingOrderType_tF976AF91C062E008942E81B1895115A047A1CB0A 
{
public:
	// System.Int32 Kolibri2d.Poly2Tri.Point2DList/WindingOrderType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WindingOrderType_tF976AF91C062E008942E81B1895115A047A1CB0A, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDINGORDERTYPE_TF976AF91C062E008942E81B1895115A047A1CB0A_H
#ifndef TRIANGULATIONMODE_TF47A940AB02AFB9DC5163BB1B9AE1A85CD37764C_H
#define TRIANGULATIONMODE_TF47A940AB02AFB9DC5163BB1B9AE1A85CD37764C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.TriangulationMode
struct  TriangulationMode_tF47A940AB02AFB9DC5163BB1B9AE1A85CD37764C 
{
public:
	// System.Int32 Kolibri2d.Poly2Tri.TriangulationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriangulationMode_tF47A940AB02AFB9DC5163BB1B9AE1A85CD37764C, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATIONMODE_TF47A940AB02AFB9DC5163BB1B9AE1A85CD37764C_H
#ifndef SEEDRANDOMIZER_TBE46395F312BD0ECAB808CE4572B917DA733887E_H
#define SEEDRANDOMIZER_TBE46395F312BD0ECAB808CE4572B917DA733887E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SeedRandomizer
struct  SeedRandomizer_tBE46395F312BD0ECAB808CE4572B917DA733887E 
{
public:
	// System.Int32 Kolibri2d.SeedRandomizer::currentValue
	int32_t ___currentValue_0;
	// System.Int32 Kolibri2d.SeedRandomizer::seed
	int32_t ___seed_1;
	// UnityEngine.Random/State Kolibri2d.SeedRandomizer::seedState
	State_t7D2976DAE446125618B7DB82F6ED3048082E388D  ___seedState_2;

public:
	inline static int32_t get_offset_of_currentValue_0() { return static_cast<int32_t>(offsetof(SeedRandomizer_tBE46395F312BD0ECAB808CE4572B917DA733887E, ___currentValue_0)); }
	inline int32_t get_currentValue_0() const { return ___currentValue_0; }
	inline int32_t* get_address_of_currentValue_0() { return &___currentValue_0; }
	inline void set_currentValue_0(int32_t value)
	{
		___currentValue_0 = value;
	}

	inline static int32_t get_offset_of_seed_1() { return static_cast<int32_t>(offsetof(SeedRandomizer_tBE46395F312BD0ECAB808CE4572B917DA733887E, ___seed_1)); }
	inline int32_t get_seed_1() const { return ___seed_1; }
	inline int32_t* get_address_of_seed_1() { return &___seed_1; }
	inline void set_seed_1(int32_t value)
	{
		___seed_1 = value;
	}

	inline static int32_t get_offset_of_seedState_2() { return static_cast<int32_t>(offsetof(SeedRandomizer_tBE46395F312BD0ECAB808CE4572B917DA733887E, ___seedState_2)); }
	inline State_t7D2976DAE446125618B7DB82F6ED3048082E388D  get_seedState_2() const { return ___seedState_2; }
	inline State_t7D2976DAE446125618B7DB82F6ED3048082E388D * get_address_of_seedState_2() { return &___seedState_2; }
	inline void set_seedState_2(State_t7D2976DAE446125618B7DB82F6ED3048082E388D  value)
	{
		___seedState_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEEDRANDOMIZER_TBE46395F312BD0ECAB808CE4572B917DA733887E_H
#ifndef SHAPETANGENTMODE_TD59E4B5749266A0BCF5D57B67CD30F5832591CAE_H
#define SHAPETANGENTMODE_TD59E4B5749266A0BCF5D57B67CD30F5832591CAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.ShapeTangentMode
struct  ShapeTangentMode_tD59E4B5749266A0BCF5D57B67CD30F5832591CAE 
{
public:
	// System.Int32 Kolibri2d.ShapeTangentMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ShapeTangentMode_tD59E4B5749266A0BCF5D57B67CD30F5832591CAE, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAPETANGENTMODE_TD59E4B5749266A0BCF5D57B67CD30F5832591CAE_H
#ifndef CONTROLMODE_TE3E10F0DAE6B22C9CA156C8E559E83C5974927DE_H
#define CONTROLMODE_TE3E10F0DAE6B22C9CA156C8E559E83C5974927DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineTexture/ControlMode
struct  ControlMode_tE3E10F0DAE6B22C9CA156C8E559E83C5974927DE 
{
public:
	// System.Int32 Kolibri2d.SplineTexture/ControlMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ControlMode_tE3E10F0DAE6B22C9CA156C8E559E83C5974927DE, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLMODE_TE3E10F0DAE6B22C9CA156C8E559E83C5974927DE_H
#ifndef ORIENTATIONMODE_TAE99237272AD796110B94EF70D5129E96516C72B_H
#define ORIENTATIONMODE_TAE99237272AD796110B94EF70D5129E96516C72B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineTexture/OrientationMode
struct  OrientationMode_tAE99237272AD796110B94EF70D5129E96516C72B 
{
public:
	// System.Int32 Kolibri2d.SplineTexture/OrientationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OrientationMode_tAE99237272AD796110B94EF70D5129E96516C72B, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTATIONMODE_TAE99237272AD796110B94EF70D5129E96516C72B_H
#ifndef FORCEDIRECTION_T92D92333E954731283BD44FF051EF85F9FB8413D_H
#define FORCEDIRECTION_T92D92333E954731283BD44FF051EF85F9FB8413D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineWater/ForceDirection
struct  ForceDirection_t92D92333E954731283BD44FF051EF85F9FB8413D 
{
public:
	// System.Int32 Kolibri2d.SplineWater/ForceDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ForceDirection_t92D92333E954731283BD44FF051EF85F9FB8413D, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCEDIRECTION_T92D92333E954731283BD44FF051EF85F9FB8413D_H
#ifndef MESHSEGMENTTYPE_TB60582871C7DE1B80274E63C8FCA2A0E90054B5D_H
#define MESHSEGMENTTYPE_TB60582871C7DE1B80274E63C8FCA2A0E90054B5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineWater/MeshSegmentType
struct  MeshSegmentType_tB60582871C7DE1B80274E63C8FCA2A0E90054B5D 
{
public:
	// System.Int32 Kolibri2d.SplineWater/MeshSegmentType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MeshSegmentType_tB60582871C7DE1B80274E63C8FCA2A0E90054B5D, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHSEGMENTTYPE_TB60582871C7DE1B80274E63C8FCA2A0E90054B5D_H
#ifndef WATEREVENT_T414CD81AE9F28B656088F21ECC14F030B1FDA33B_H
#define WATEREVENT_T414CD81AE9F28B656088F21ECC14F030B1FDA33B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineWater/WaterEvent
struct  WaterEvent_t414CD81AE9F28B656088F21ECC14F030B1FDA33B  : public UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATEREVENT_T414CD81AE9F28B656088F21ECC14F030B1FDA33B_H
#ifndef SKIRTTYPE_TF27DB6D7DFF61A49FB473B5B26C52F1EBED74A99_H
#define SKIRTTYPE_TF27DB6D7DFF61A49FB473B5B26C52F1EBED74A99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2D/SkirtType
struct  SkirtType_tF27DB6D7DFF61A49FB473B5B26C52F1EBED74A99 
{
public:
	// System.Int32 Kolibri2d.Terrain2D/SkirtType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SkirtType_tF27DB6D7DFF61A49FB473B5B26C52F1EBED74A99, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKIRTTYPE_TF27DB6D7DFF61A49FB473B5B26C52F1EBED74A99_H
#ifndef TERRAIN2DMATERIALHOLDER_TBF26B45C9650004249F5363B2D8AB054CB1EE60F_H
#define TERRAIN2DMATERIALHOLDER_TBF26B45C9650004249F5363B2D8AB054CB1EE60F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2D/Terrain2DMaterialHolder
struct  Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F  : public RuntimeObject
{
public:
	// System.Boolean Kolibri2d.Terrain2D/Terrain2DMaterialHolder::enabled
	bool ___enabled_0;
	// Kolibri2d.Terrain2DMaterial Kolibri2d.Terrain2D/Terrain2DMaterialHolder::terrainMaterial
	Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * ___terrainMaterial_1;
	// UnityEngine.Material Kolibri2d.Terrain2D/Terrain2DMaterialHolder::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_2;
	// UnityEngine.Color Kolibri2d.Terrain2D/Terrain2DMaterialHolder::tint
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___tint_3;
	// System.Boolean Kolibri2d.Terrain2D/Terrain2DMaterialHolder::useTint
	bool ___useTint_4;
	// System.Boolean Kolibri2d.Terrain2D/Terrain2DMaterialHolder::createColliders
	bool ___createColliders_5;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_terrainMaterial_1() { return static_cast<int32_t>(offsetof(Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F, ___terrainMaterial_1)); }
	inline Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * get_terrainMaterial_1() const { return ___terrainMaterial_1; }
	inline Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D ** get_address_of_terrainMaterial_1() { return &___terrainMaterial_1; }
	inline void set_terrainMaterial_1(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D * value)
	{
		___terrainMaterial_1 = value;
		Il2CppCodeGenWriteBarrier((&___terrainMaterial_1), value);
	}

	inline static int32_t get_offset_of_material_2() { return static_cast<int32_t>(offsetof(Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F, ___material_2)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_2() const { return ___material_2; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_2() { return &___material_2; }
	inline void set_material_2(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_2 = value;
		Il2CppCodeGenWriteBarrier((&___material_2), value);
	}

	inline static int32_t get_offset_of_tint_3() { return static_cast<int32_t>(offsetof(Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F, ___tint_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_tint_3() const { return ___tint_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_tint_3() { return &___tint_3; }
	inline void set_tint_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___tint_3 = value;
	}

	inline static int32_t get_offset_of_useTint_4() { return static_cast<int32_t>(offsetof(Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F, ___useTint_4)); }
	inline bool get_useTint_4() const { return ___useTint_4; }
	inline bool* get_address_of_useTint_4() { return &___useTint_4; }
	inline void set_useTint_4(bool value)
	{
		___useTint_4 = value;
	}

	inline static int32_t get_offset_of_createColliders_5() { return static_cast<int32_t>(offsetof(Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F, ___createColliders_5)); }
	inline bool get_createColliders_5() const { return ___createColliders_5; }
	inline bool* get_address_of_createColliders_5() { return &___createColliders_5; }
	inline void set_createColliders_5(bool value)
	{
		___createColliders_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERRAIN2DMATERIALHOLDER_TBF26B45C9650004249F5363B2D8AB054CB1EE60F_H
#ifndef ASPECT_T4C69BE83340C4AF890ACE7BB85B0B09491E8C5AC_H
#define ASPECT_T4C69BE83340C4AF890ACE7BB85B0B09491E8C5AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2DArchitecture/Aspect
struct  Aspect_t4C69BE83340C4AF890ACE7BB85B0B09491E8C5AC 
{
public:
	// System.Int32 Kolibri2d.Terrain2DArchitecture/Aspect::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Aspect_t4C69BE83340C4AF890ACE7BB85B0B09491E8C5AC, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECT_T4C69BE83340C4AF890ACE7BB85B0B09491E8C5AC_H
#ifndef CORNERSIDE_T8D5E50DDEDB6E82A23FCCFA8EFAD995D237D95FC_H
#define CORNERSIDE_T8D5E50DDEDB6E82A23FCCFA8EFAD995D237D95FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2DMaterial/CornerSide
struct  CornerSide_t8D5E50DDEDB6E82A23FCCFA8EFAD995D237D95FC 
{
public:
	// System.Int32 Kolibri2d.Terrain2DMaterial/CornerSide::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CornerSide_t8D5E50DDEDB6E82A23FCCFA8EFAD995D237D95FC, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORNERSIDE_T8D5E50DDEDB6E82A23FCCFA8EFAD995D237D95FC_H
#ifndef CORNERDISPLAYTYPE_T371ADA93330AF421D7EC2FBEB92BC9EA01C9815D_H
#define CORNERDISPLAYTYPE_T371ADA93330AF421D7EC2FBEB92BC9EA01C9815D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2DMaterial/CornerSprites2/CornerDisplayType
struct  CornerDisplayType_t371ADA93330AF421D7EC2FBEB92BC9EA01C9815D 
{
public:
	// System.Int32 Kolibri2d.Terrain2DMaterial/CornerSprites2/CornerDisplayType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CornerDisplayType_t371ADA93330AF421D7EC2FBEB92BC9EA01C9815D, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORNERDISPLAYTYPE_T371ADA93330AF421D7EC2FBEB92BC9EA01C9815D_H
#ifndef CORNERTYPE_TB72616AAD3B5283135F2E892B87F36ECEB748AE7_H
#define CORNERTYPE_TB72616AAD3B5283135F2E892B87F36ECEB748AE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2DMaterial/CornerType
struct  CornerType_tB72616AAD3B5283135F2E892B87F36ECEB748AE7 
{
public:
	// System.Int32 Kolibri2d.Terrain2DMaterial/CornerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CornerType_tB72616AAD3B5283135F2E892B87F36ECEB748AE7, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORNERTYPE_TB72616AAD3B5283135F2E892B87F36ECEB748AE7_H
#ifndef FILLTYPE_TBDFB72417EBD74C72808826851F60D35622EFD6E_H
#define FILLTYPE_TBDFB72417EBD74C72808826851F60D35622EFD6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2DMaterial/FillType
struct  FillType_tBDFB72417EBD74C72808826851F60D35622EFD6E 
{
public:
	// System.Int32 Kolibri2d.Terrain2DMaterial/FillType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillType_tBDFB72417EBD74C72808826851F60D35622EFD6E, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLTYPE_TBDFB72417EBD74C72808826851F60D35622EFD6E_H
#ifndef RENDERORDERTYPE_T080E9E94A764E427B39096E25C800721F2FDE98A_H
#define RENDERORDERTYPE_T080E9E94A764E427B39096E25C800721F2FDE98A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2DMaterial/RenderOrderType
struct  RenderOrderType_t080E9E94A764E427B39096E25C800721F2FDE98A 
{
public:
	// System.Int32 Kolibri2d.Terrain2DMaterial/RenderOrderType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderOrderType_t080E9E94A764E427B39096E25C800721F2FDE98A, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERORDERTYPE_T080E9E94A764E427B39096E25C800721F2FDE98A_H
#ifndef TERRAINTYPE_TCDCA8C4F29F1D3F57A51D0DD84B025709F7BFB19_H
#define TERRAINTYPE_TCDCA8C4F29F1D3F57A51D0DD84B025709F7BFB19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.TerrainType
struct  TerrainType_tCDCA8C4F29F1D3F57A51D0DD84B025709F7BFB19 
{
public:
	// System.Int32 Kolibri2d.TerrainType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TerrainType_tCDCA8C4F29F1D3F57A51D0DD84B025709F7BFB19, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERRAINTYPE_TCDCA8C4F29F1D3F57A51D0DD84B025709F7BFB19_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_8)); }
	inline DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 * get_data_8() const { return ___data_8; }
	inline DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T_H
#ifndef NOTIMPLEMENTEDEXCEPTION_TAF5A82631683648991DEF6809680B0DF7A4A4767_H
#define NOTIMPLEMENTEDEXCEPTION_TAF5A82631683648991DEF6809680B0DF7A4A4767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotImplementedException
struct  NotImplementedException_tAF5A82631683648991DEF6809680B0DF7A4A4767  : public SystemException_t81A06ADD519539D252788D75E44AFFBE6580E301
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIMPLEMENTEDEXCEPTION_TAF5A82631683648991DEF6809680B0DF7A4A4767_H
#ifndef ANALYTICSSESSIONSTATE_T61CA873937E9A3B881B71B32F518A954A4C8F267_H
#define ANALYTICSSESSIONSTATE_T61CA873937E9A3B881B71B32F518A954A4C8F267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionState
struct  AnalyticsSessionState_t61CA873937E9A3B881B71B32F518A954A4C8F267 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsSessionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnalyticsSessionState_t61CA873937E9A3B881B71B32F518A954A4C8F267, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSSESSIONSTATE_T61CA873937E9A3B881B71B32F518A954A4C8F267_H
#ifndef CERTIFICATEHANDLER_TBD070BF4150A44AB482FD36EA3882C363117E8C0_H
#define CERTIFICATEHANDLER_TBD070BF4150A44AB482FD36EA3882C363117E8C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.CertificateHandler
struct  CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.CertificateHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.CertificateHandler
struct CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.CertificateHandler
struct CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // CERTIFICATEHANDLER_TBD070BF4150A44AB482FD36EA3882C363117E8C0_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef REMOTECONFIGSETTINGS_T97154F5546B47CE72257CC2F0B677BDF696AEC4A_H
#define REMOTECONFIGSETTINGS_T97154F5546B47CE72257CC2F0B677BDF696AEC4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RemoteConfigSettings
struct  RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.RemoteConfigSettings::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<System.Boolean> UnityEngine.RemoteConfigSettings::Updated
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___Updated_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_Updated_1() { return static_cast<int32_t>(offsetof(RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A, ___Updated_1)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_Updated_1() const { return ___Updated_1; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_Updated_1() { return &___Updated_1; }
	inline void set_Updated_1(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___Updated_1 = value;
		Il2CppCodeGenWriteBarrier((&___Updated_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RemoteConfigSettings
struct RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___Updated_1;
};
// Native definition for COM marshalling of UnityEngine.RemoteConfigSettings
struct RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___Updated_1;
};
#endif // REMOTECONFIGSETTINGS_T97154F5546B47CE72257CC2F0B677BDF696AEC4A_H
#ifndef SAMPLETYPE_T2144AEAF3447ACAFCE1C13AF669F63192F8E75EC_H
#define SAMPLETYPE_T2144AEAF3447ACAFCE1C13AF669F63192F8E75EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UISystemProfilerApi/SampleType
struct  SampleType_t2144AEAF3447ACAFCE1C13AF669F63192F8E75EC 
{
public:
	// System.Int32 UnityEngine.UISystemProfilerApi/SampleType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SampleType_t2144AEAF3447ACAFCE1C13AF669F63192F8E75EC, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLETYPE_T2144AEAF3447ACAFCE1C13AF669F63192F8E75EC_H
#ifndef BEZIERPOINT_T371F386D0EC739B5463360D9BB85CFBE271E9D93_H
#define BEZIERPOINT_T371F386D0EC739B5463360D9BB85CFBE271E9D93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.BezierSpline/BezierPoint
struct  BezierPoint_t371F386D0EC739B5463360D9BB85CFBE271E9D93  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Kolibri2d.BezierSpline/BezierPoint::p0
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___p0_0;
	// UnityEngine.Vector3 Kolibri2d.BezierSpline/BezierPoint::p1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___p1_1;
	// UnityEngine.Vector3 Kolibri2d.BezierSpline/BezierPoint::p2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___p2_2;
	// System.Single Kolibri2d.BezierSpline/BezierPoint::length
	float ___length_3;
	// Kolibri2d.BezierControlPointMode Kolibri2d.BezierSpline/BezierPoint::mode
	int32_t ___mode_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Kolibri2d.BezierSpline/BezierPoint::stepPoints
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___stepPoints_5;
	// System.Collections.Generic.List`1<System.Single> Kolibri2d.BezierSpline/BezierPoint::stepLengths
	List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * ___stepLengths_6;

public:
	inline static int32_t get_offset_of_p0_0() { return static_cast<int32_t>(offsetof(BezierPoint_t371F386D0EC739B5463360D9BB85CFBE271E9D93, ___p0_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_p0_0() const { return ___p0_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_p0_0() { return &___p0_0; }
	inline void set_p0_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___p0_0 = value;
	}

	inline static int32_t get_offset_of_p1_1() { return static_cast<int32_t>(offsetof(BezierPoint_t371F386D0EC739B5463360D9BB85CFBE271E9D93, ___p1_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_p1_1() const { return ___p1_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_p1_1() { return &___p1_1; }
	inline void set_p1_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___p1_1 = value;
	}

	inline static int32_t get_offset_of_p2_2() { return static_cast<int32_t>(offsetof(BezierPoint_t371F386D0EC739B5463360D9BB85CFBE271E9D93, ___p2_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_p2_2() const { return ___p2_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_p2_2() { return &___p2_2; }
	inline void set_p2_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___p2_2 = value;
	}

	inline static int32_t get_offset_of_length_3() { return static_cast<int32_t>(offsetof(BezierPoint_t371F386D0EC739B5463360D9BB85CFBE271E9D93, ___length_3)); }
	inline float get_length_3() const { return ___length_3; }
	inline float* get_address_of_length_3() { return &___length_3; }
	inline void set_length_3(float value)
	{
		___length_3 = value;
	}

	inline static int32_t get_offset_of_mode_4() { return static_cast<int32_t>(offsetof(BezierPoint_t371F386D0EC739B5463360D9BB85CFBE271E9D93, ___mode_4)); }
	inline int32_t get_mode_4() const { return ___mode_4; }
	inline int32_t* get_address_of_mode_4() { return &___mode_4; }
	inline void set_mode_4(int32_t value)
	{
		___mode_4 = value;
	}

	inline static int32_t get_offset_of_stepPoints_5() { return static_cast<int32_t>(offsetof(BezierPoint_t371F386D0EC739B5463360D9BB85CFBE271E9D93, ___stepPoints_5)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_stepPoints_5() const { return ___stepPoints_5; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_stepPoints_5() { return &___stepPoints_5; }
	inline void set_stepPoints_5(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___stepPoints_5 = value;
		Il2CppCodeGenWriteBarrier((&___stepPoints_5), value);
	}

	inline static int32_t get_offset_of_stepLengths_6() { return static_cast<int32_t>(offsetof(BezierPoint_t371F386D0EC739B5463360D9BB85CFBE271E9D93, ___stepLengths_6)); }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * get_stepLengths_6() const { return ___stepLengths_6; }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E ** get_address_of_stepLengths_6() { return &___stepLengths_6; }
	inline void set_stepLengths_6(List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * value)
	{
		___stepLengths_6 = value;
		Il2CppCodeGenWriteBarrier((&___stepLengths_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEZIERPOINT_T371F386D0EC739B5463360D9BB85CFBE271E9D93_H
#ifndef BEZIERSPLINEQUALITY_T3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF_H
#define BEZIERSPLINEQUALITY_T3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.BezierSplineQuality
struct  BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF  : public RuntimeObject
{
public:
	// Kolibri2d.BezierSplineQuality/SubdivisionMode Kolibri2d.BezierSplineQuality::mode
	int32_t ___mode_0;
	// Kolibri2d.BezierSplineQuality/QualityLevel Kolibri2d.BezierSplineQuality::level
	int32_t ___level_1;
	// System.Int32 Kolibri2d.BezierSplineQuality::userSubdivisionsPerSegment
	int32_t ___userSubdivisionsPerSegment_2;
	// System.Int32 Kolibri2d.BezierSplineQuality::fixedSubdivisionsPerSegment
	int32_t ___fixedSubdivisionsPerSegment_3;
	// System.Single Kolibri2d.BezierSplineQuality::straigthSegmentMultiplier
	float ___straigthSegmentMultiplier_4;
	// System.Single Kolibri2d.BezierSplineQuality::softCurveSegmentMultiplier
	float ___softCurveSegmentMultiplier_5;
	// System.Single Kolibri2d.BezierSplineQuality::hardCurveSegmentMultiplier
	float ___hardCurveSegmentMultiplier_6;
	// System.Single Kolibri2d.BezierSplineQuality::smallSegmentMultiplier
	float ___smallSegmentMultiplier_7;
	// System.Single Kolibri2d.BezierSplineQuality::bigSegmentMultiplier
	float ___bigSegmentMultiplier_8;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_level_1() { return static_cast<int32_t>(offsetof(BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF, ___level_1)); }
	inline int32_t get_level_1() const { return ___level_1; }
	inline int32_t* get_address_of_level_1() { return &___level_1; }
	inline void set_level_1(int32_t value)
	{
		___level_1 = value;
	}

	inline static int32_t get_offset_of_userSubdivisionsPerSegment_2() { return static_cast<int32_t>(offsetof(BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF, ___userSubdivisionsPerSegment_2)); }
	inline int32_t get_userSubdivisionsPerSegment_2() const { return ___userSubdivisionsPerSegment_2; }
	inline int32_t* get_address_of_userSubdivisionsPerSegment_2() { return &___userSubdivisionsPerSegment_2; }
	inline void set_userSubdivisionsPerSegment_2(int32_t value)
	{
		___userSubdivisionsPerSegment_2 = value;
	}

	inline static int32_t get_offset_of_fixedSubdivisionsPerSegment_3() { return static_cast<int32_t>(offsetof(BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF, ___fixedSubdivisionsPerSegment_3)); }
	inline int32_t get_fixedSubdivisionsPerSegment_3() const { return ___fixedSubdivisionsPerSegment_3; }
	inline int32_t* get_address_of_fixedSubdivisionsPerSegment_3() { return &___fixedSubdivisionsPerSegment_3; }
	inline void set_fixedSubdivisionsPerSegment_3(int32_t value)
	{
		___fixedSubdivisionsPerSegment_3 = value;
	}

	inline static int32_t get_offset_of_straigthSegmentMultiplier_4() { return static_cast<int32_t>(offsetof(BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF, ___straigthSegmentMultiplier_4)); }
	inline float get_straigthSegmentMultiplier_4() const { return ___straigthSegmentMultiplier_4; }
	inline float* get_address_of_straigthSegmentMultiplier_4() { return &___straigthSegmentMultiplier_4; }
	inline void set_straigthSegmentMultiplier_4(float value)
	{
		___straigthSegmentMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_softCurveSegmentMultiplier_5() { return static_cast<int32_t>(offsetof(BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF, ___softCurveSegmentMultiplier_5)); }
	inline float get_softCurveSegmentMultiplier_5() const { return ___softCurveSegmentMultiplier_5; }
	inline float* get_address_of_softCurveSegmentMultiplier_5() { return &___softCurveSegmentMultiplier_5; }
	inline void set_softCurveSegmentMultiplier_5(float value)
	{
		___softCurveSegmentMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_hardCurveSegmentMultiplier_6() { return static_cast<int32_t>(offsetof(BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF, ___hardCurveSegmentMultiplier_6)); }
	inline float get_hardCurveSegmentMultiplier_6() const { return ___hardCurveSegmentMultiplier_6; }
	inline float* get_address_of_hardCurveSegmentMultiplier_6() { return &___hardCurveSegmentMultiplier_6; }
	inline void set_hardCurveSegmentMultiplier_6(float value)
	{
		___hardCurveSegmentMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_smallSegmentMultiplier_7() { return static_cast<int32_t>(offsetof(BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF, ___smallSegmentMultiplier_7)); }
	inline float get_smallSegmentMultiplier_7() const { return ___smallSegmentMultiplier_7; }
	inline float* get_address_of_smallSegmentMultiplier_7() { return &___smallSegmentMultiplier_7; }
	inline void set_smallSegmentMultiplier_7(float value)
	{
		___smallSegmentMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_bigSegmentMultiplier_8() { return static_cast<int32_t>(offsetof(BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF, ___bigSegmentMultiplier_8)); }
	inline float get_bigSegmentMultiplier_8() const { return ___bigSegmentMultiplier_8; }
	inline float* get_address_of_bigSegmentMultiplier_8() { return &___bigSegmentMultiplier_8; }
	inline void set_bigSegmentMultiplier_8(float value)
	{
		___bigSegmentMultiplier_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEZIERSPLINEQUALITY_T3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF_H
#ifndef MATERIALTEXTUREINFO_T0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F_H
#define MATERIALTEXTUREINFO_T0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.MaterialTextureInfo
struct  MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F 
{
public:
	// UnityEngine.Sprite Kolibri2d.MaterialTextureInfo::sprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___sprite_0;
	// Kolibri2d.MaterialTextureInfo/RotationMode Kolibri2d.MaterialTextureInfo::rotation
	int32_t ___rotation_1;
	// Kolibri2d.MaterialTextureInfo/SpriteStretchType Kolibri2d.MaterialTextureInfo::stretchType
	int32_t ___stretchType_2;
	// System.Single Kolibri2d.MaterialTextureInfo::stretchValue
	float ___stretchValue_3;
	// UnityEngine.Vector2 Kolibri2d.MaterialTextureInfo::scale
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___scale_4;
	// UnityEngine.Vector2 Kolibri2d.MaterialTextureInfo::offset
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___offset_5;

public:
	inline static int32_t get_offset_of_sprite_0() { return static_cast<int32_t>(offsetof(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F, ___sprite_0)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_sprite_0() const { return ___sprite_0; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_sprite_0() { return &___sprite_0; }
	inline void set_sprite_0(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___sprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___sprite_0), value);
	}

	inline static int32_t get_offset_of_rotation_1() { return static_cast<int32_t>(offsetof(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F, ___rotation_1)); }
	inline int32_t get_rotation_1() const { return ___rotation_1; }
	inline int32_t* get_address_of_rotation_1() { return &___rotation_1; }
	inline void set_rotation_1(int32_t value)
	{
		___rotation_1 = value;
	}

	inline static int32_t get_offset_of_stretchType_2() { return static_cast<int32_t>(offsetof(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F, ___stretchType_2)); }
	inline int32_t get_stretchType_2() const { return ___stretchType_2; }
	inline int32_t* get_address_of_stretchType_2() { return &___stretchType_2; }
	inline void set_stretchType_2(int32_t value)
	{
		___stretchType_2 = value;
	}

	inline static int32_t get_offset_of_stretchValue_3() { return static_cast<int32_t>(offsetof(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F, ___stretchValue_3)); }
	inline float get_stretchValue_3() const { return ___stretchValue_3; }
	inline float* get_address_of_stretchValue_3() { return &___stretchValue_3; }
	inline void set_stretchValue_3(float value)
	{
		___stretchValue_3 = value;
	}

	inline static int32_t get_offset_of_scale_4() { return static_cast<int32_t>(offsetof(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F, ___scale_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_scale_4() const { return ___scale_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_scale_4() { return &___scale_4; }
	inline void set_scale_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___scale_4 = value;
	}

	inline static int32_t get_offset_of_offset_5() { return static_cast<int32_t>(offsetof(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F, ___offset_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_offset_5() const { return ___offset_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_offset_5() { return &___offset_5; }
	inline void set_offset_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___offset_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Kolibri2d.MaterialTextureInfo
struct MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F_marshaled_pinvoke
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___sprite_0;
	int32_t ___rotation_1;
	int32_t ___stretchType_2;
	float ___stretchValue_3;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___scale_4;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___offset_5;
};
// Native definition for COM marshalling of Kolibri2d.MaterialTextureInfo
struct MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F_marshaled_com
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___sprite_0;
	int32_t ___rotation_1;
	int32_t ___stretchType_2;
	float ___stretchValue_3;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___scale_4;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___offset_5;
};
#endif // MATERIALTEXTUREINFO_T0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F_H
#ifndef POINT2DLIST_T1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0_H
#define POINT2DLIST_T1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.Point2DList
struct  Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Kolibri2d.Poly2Tri.Point2D> Kolibri2d.Poly2Tri.Point2DList::mPoints
	List_1_tFD3B51F91BAFEAD7A39B0EF917EC5B58423E32AE * ___mPoints_3;
	// Kolibri2d.Poly2Tri.Rect2D Kolibri2d.Poly2Tri.Point2DList::mBoundingBox
	Rect2D_t337FF4CBB07E16CB6E029205BF01F1984DDE5353 * ___mBoundingBox_4;
	// Kolibri2d.Poly2Tri.Point2DList/WindingOrderType Kolibri2d.Poly2Tri.Point2DList::mWindingOrder
	int32_t ___mWindingOrder_5;
	// System.Double Kolibri2d.Poly2Tri.Point2DList::mEpsilon
	double ___mEpsilon_6;

public:
	inline static int32_t get_offset_of_mPoints_3() { return static_cast<int32_t>(offsetof(Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0, ___mPoints_3)); }
	inline List_1_tFD3B51F91BAFEAD7A39B0EF917EC5B58423E32AE * get_mPoints_3() const { return ___mPoints_3; }
	inline List_1_tFD3B51F91BAFEAD7A39B0EF917EC5B58423E32AE ** get_address_of_mPoints_3() { return &___mPoints_3; }
	inline void set_mPoints_3(List_1_tFD3B51F91BAFEAD7A39B0EF917EC5B58423E32AE * value)
	{
		___mPoints_3 = value;
		Il2CppCodeGenWriteBarrier((&___mPoints_3), value);
	}

	inline static int32_t get_offset_of_mBoundingBox_4() { return static_cast<int32_t>(offsetof(Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0, ___mBoundingBox_4)); }
	inline Rect2D_t337FF4CBB07E16CB6E029205BF01F1984DDE5353 * get_mBoundingBox_4() const { return ___mBoundingBox_4; }
	inline Rect2D_t337FF4CBB07E16CB6E029205BF01F1984DDE5353 ** get_address_of_mBoundingBox_4() { return &___mBoundingBox_4; }
	inline void set_mBoundingBox_4(Rect2D_t337FF4CBB07E16CB6E029205BF01F1984DDE5353 * value)
	{
		___mBoundingBox_4 = value;
		Il2CppCodeGenWriteBarrier((&___mBoundingBox_4), value);
	}

	inline static int32_t get_offset_of_mWindingOrder_5() { return static_cast<int32_t>(offsetof(Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0, ___mWindingOrder_5)); }
	inline int32_t get_mWindingOrder_5() const { return ___mWindingOrder_5; }
	inline int32_t* get_address_of_mWindingOrder_5() { return &___mWindingOrder_5; }
	inline void set_mWindingOrder_5(int32_t value)
	{
		___mWindingOrder_5 = value;
	}

	inline static int32_t get_offset_of_mEpsilon_6() { return static_cast<int32_t>(offsetof(Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0, ___mEpsilon_6)); }
	inline double get_mEpsilon_6() const { return ___mEpsilon_6; }
	inline double* get_address_of_mEpsilon_6() { return &___mEpsilon_6; }
	inline void set_mEpsilon_6(double value)
	{
		___mEpsilon_6 = value;
	}
};

struct Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0_StaticFields
{
public:
	// System.Int32 Kolibri2d.Poly2Tri.Point2DList::kMaxPolygonVertices
	int32_t ___kMaxPolygonVertices_0;
	// System.Double Kolibri2d.Poly2Tri.Point2DList::kLinearSlop
	double ___kLinearSlop_1;
	// System.Double Kolibri2d.Poly2Tri.Point2DList::kAngularSlop
	double ___kAngularSlop_2;

public:
	inline static int32_t get_offset_of_kMaxPolygonVertices_0() { return static_cast<int32_t>(offsetof(Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0_StaticFields, ___kMaxPolygonVertices_0)); }
	inline int32_t get_kMaxPolygonVertices_0() const { return ___kMaxPolygonVertices_0; }
	inline int32_t* get_address_of_kMaxPolygonVertices_0() { return &___kMaxPolygonVertices_0; }
	inline void set_kMaxPolygonVertices_0(int32_t value)
	{
		___kMaxPolygonVertices_0 = value;
	}

	inline static int32_t get_offset_of_kLinearSlop_1() { return static_cast<int32_t>(offsetof(Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0_StaticFields, ___kLinearSlop_1)); }
	inline double get_kLinearSlop_1() const { return ___kLinearSlop_1; }
	inline double* get_address_of_kLinearSlop_1() { return &___kLinearSlop_1; }
	inline void set_kLinearSlop_1(double value)
	{
		___kLinearSlop_1 = value;
	}

	inline static int32_t get_offset_of_kAngularSlop_2() { return static_cast<int32_t>(offsetof(Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0_StaticFields, ___kAngularSlop_2)); }
	inline double get_kAngularSlop_2() const { return ___kAngularSlop_2; }
	inline double* get_address_of_kAngularSlop_2() { return &___kAngularSlop_2; }
	inline void set_kAngularSlop_2(double value)
	{
		___kAngularSlop_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINT2DLIST_T1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0_H
#ifndef POINTONEDGEEXCEPTION_T410ABBD0AEDC8BEAD316BEC504857280BEC63802_H
#define POINTONEDGEEXCEPTION_T410ABBD0AEDC8BEAD316BEC504857280BEC63802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.PointOnEdgeException
struct  PointOnEdgeException_t410ABBD0AEDC8BEAD316BEC504857280BEC63802  : public NotImplementedException_tAF5A82631683648991DEF6809680B0DF7A4A4767
{
public:
	// Kolibri2d.Poly2Tri.TriangulationPoint Kolibri2d.Poly2Tri.PointOnEdgeException::A
	TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * ___A_11;
	// Kolibri2d.Poly2Tri.TriangulationPoint Kolibri2d.Poly2Tri.PointOnEdgeException::B
	TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * ___B_12;
	// Kolibri2d.Poly2Tri.TriangulationPoint Kolibri2d.Poly2Tri.PointOnEdgeException::C
	TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * ___C_13;

public:
	inline static int32_t get_offset_of_A_11() { return static_cast<int32_t>(offsetof(PointOnEdgeException_t410ABBD0AEDC8BEAD316BEC504857280BEC63802, ___A_11)); }
	inline TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * get_A_11() const { return ___A_11; }
	inline TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E ** get_address_of_A_11() { return &___A_11; }
	inline void set_A_11(TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * value)
	{
		___A_11 = value;
		Il2CppCodeGenWriteBarrier((&___A_11), value);
	}

	inline static int32_t get_offset_of_B_12() { return static_cast<int32_t>(offsetof(PointOnEdgeException_t410ABBD0AEDC8BEAD316BEC504857280BEC63802, ___B_12)); }
	inline TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * get_B_12() const { return ___B_12; }
	inline TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E ** get_address_of_B_12() { return &___B_12; }
	inline void set_B_12(TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * value)
	{
		___B_12 = value;
		Il2CppCodeGenWriteBarrier((&___B_12), value);
	}

	inline static int32_t get_offset_of_C_13() { return static_cast<int32_t>(offsetof(PointOnEdgeException_t410ABBD0AEDC8BEAD316BEC504857280BEC63802, ___C_13)); }
	inline TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * get_C_13() const { return ___C_13; }
	inline TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E ** get_address_of_C_13() { return &___C_13; }
	inline void set_C_13(TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * value)
	{
		___C_13 = value;
		Il2CppCodeGenWriteBarrier((&___C_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTONEDGEEXCEPTION_T410ABBD0AEDC8BEAD316BEC504857280BEC63802_H
#ifndef TRIANGULATIONCONTEXT_TC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B_H
#define TRIANGULATIONCONTEXT_TC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.TriangulationContext
struct  TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B  : public RuntimeObject
{
public:
	// Kolibri2d.Poly2Tri.TriangulationDebugContext Kolibri2d.Poly2Tri.TriangulationContext::<DebugContext>k__BackingField
	TriangulationDebugContext_t23D041A57AAB2224970A201625386E8BEB37D9A4 * ___U3CDebugContextU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<Kolibri2d.Poly2Tri.DelaunayTriangle> Kolibri2d.Poly2Tri.TriangulationContext::Triangles
	List_1_tA161C1D023FB6D4167CAE9ED2A7AB1554978B9D9 * ___Triangles_1;
	// System.Collections.Generic.List`1<Kolibri2d.Poly2Tri.TriangulationPoint> Kolibri2d.Poly2Tri.TriangulationContext::Points
	List_1_tAAF0D3178D082F2377FDDDC38ED8BC5C83A1FF81 * ___Points_2;
	// Kolibri2d.Poly2Tri.TriangulationMode Kolibri2d.Poly2Tri.TriangulationContext::<TriangulationMode>k__BackingField
	int32_t ___U3CTriangulationModeU3Ek__BackingField_3;
	// Kolibri2d.Poly2Tri.ITriangulatable Kolibri2d.Poly2Tri.TriangulationContext::<Triangulatable>k__BackingField
	RuntimeObject* ___U3CTriangulatableU3Ek__BackingField_4;
	// System.Int32 Kolibri2d.Poly2Tri.TriangulationContext::<StepCount>k__BackingField
	int32_t ___U3CStepCountU3Ek__BackingField_5;
	// System.Boolean Kolibri2d.Poly2Tri.TriangulationContext::<IsDebugEnabled>k__BackingField
	bool ___U3CIsDebugEnabledU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CDebugContextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B, ___U3CDebugContextU3Ek__BackingField_0)); }
	inline TriangulationDebugContext_t23D041A57AAB2224970A201625386E8BEB37D9A4 * get_U3CDebugContextU3Ek__BackingField_0() const { return ___U3CDebugContextU3Ek__BackingField_0; }
	inline TriangulationDebugContext_t23D041A57AAB2224970A201625386E8BEB37D9A4 ** get_address_of_U3CDebugContextU3Ek__BackingField_0() { return &___U3CDebugContextU3Ek__BackingField_0; }
	inline void set_U3CDebugContextU3Ek__BackingField_0(TriangulationDebugContext_t23D041A57AAB2224970A201625386E8BEB37D9A4 * value)
	{
		___U3CDebugContextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDebugContextU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_Triangles_1() { return static_cast<int32_t>(offsetof(TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B, ___Triangles_1)); }
	inline List_1_tA161C1D023FB6D4167CAE9ED2A7AB1554978B9D9 * get_Triangles_1() const { return ___Triangles_1; }
	inline List_1_tA161C1D023FB6D4167CAE9ED2A7AB1554978B9D9 ** get_address_of_Triangles_1() { return &___Triangles_1; }
	inline void set_Triangles_1(List_1_tA161C1D023FB6D4167CAE9ED2A7AB1554978B9D9 * value)
	{
		___Triangles_1 = value;
		Il2CppCodeGenWriteBarrier((&___Triangles_1), value);
	}

	inline static int32_t get_offset_of_Points_2() { return static_cast<int32_t>(offsetof(TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B, ___Points_2)); }
	inline List_1_tAAF0D3178D082F2377FDDDC38ED8BC5C83A1FF81 * get_Points_2() const { return ___Points_2; }
	inline List_1_tAAF0D3178D082F2377FDDDC38ED8BC5C83A1FF81 ** get_address_of_Points_2() { return &___Points_2; }
	inline void set_Points_2(List_1_tAAF0D3178D082F2377FDDDC38ED8BC5C83A1FF81 * value)
	{
		___Points_2 = value;
		Il2CppCodeGenWriteBarrier((&___Points_2), value);
	}

	inline static int32_t get_offset_of_U3CTriangulationModeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B, ___U3CTriangulationModeU3Ek__BackingField_3)); }
	inline int32_t get_U3CTriangulationModeU3Ek__BackingField_3() const { return ___U3CTriangulationModeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CTriangulationModeU3Ek__BackingField_3() { return &___U3CTriangulationModeU3Ek__BackingField_3; }
	inline void set_U3CTriangulationModeU3Ek__BackingField_3(int32_t value)
	{
		___U3CTriangulationModeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CTriangulatableU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B, ___U3CTriangulatableU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CTriangulatableU3Ek__BackingField_4() const { return ___U3CTriangulatableU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CTriangulatableU3Ek__BackingField_4() { return &___U3CTriangulatableU3Ek__BackingField_4; }
	inline void set_U3CTriangulatableU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CTriangulatableU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTriangulatableU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CStepCountU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B, ___U3CStepCountU3Ek__BackingField_5)); }
	inline int32_t get_U3CStepCountU3Ek__BackingField_5() const { return ___U3CStepCountU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CStepCountU3Ek__BackingField_5() { return &___U3CStepCountU3Ek__BackingField_5; }
	inline void set_U3CStepCountU3Ek__BackingField_5(int32_t value)
	{
		___U3CStepCountU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CIsDebugEnabledU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B, ___U3CIsDebugEnabledU3Ek__BackingField_6)); }
	inline bool get_U3CIsDebugEnabledU3Ek__BackingField_6() const { return ___U3CIsDebugEnabledU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CIsDebugEnabledU3Ek__BackingField_6() { return &___U3CIsDebugEnabledU3Ek__BackingField_6; }
	inline void set_U3CIsDebugEnabledU3Ek__BackingField_6(bool value)
	{
		___U3CIsDebugEnabledU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATIONCONTEXT_TC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B_H
#ifndef SPLINECONTROLPOINT_T5FB60D8FEB29C651E953002ECE34F9BA73654764_H
#define SPLINECONTROLPOINT_T5FB60D8FEB29C651E953002ECE34F9BA73654764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineControlPoint
struct  SplineControlPoint_t5FB60D8FEB29C651E953002ECE34F9BA73654764  : public RuntimeObject
{
public:
	// System.Single Kolibri2d.SplineControlPoint::height
	float ___height_0;
	// UnityEngine.Vector3 Kolibri2d.SplineControlPoint::leftTangent
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftTangent_1;
	// UnityEngine.Vector3 Kolibri2d.SplineControlPoint::rightTangent
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightTangent_2;
	// UnityEngine.Vector3 Kolibri2d.SplineControlPoint::position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_3;
	// Kolibri2d.ShapeTangentMode Kolibri2d.SplineControlPoint::mode
	int32_t ___mode_4;
	// System.Int32 Kolibri2d.SplineControlPoint::subdivisionsOverride
	int32_t ___subdivisionsOverride_5;

public:
	inline static int32_t get_offset_of_height_0() { return static_cast<int32_t>(offsetof(SplineControlPoint_t5FB60D8FEB29C651E953002ECE34F9BA73654764, ___height_0)); }
	inline float get_height_0() const { return ___height_0; }
	inline float* get_address_of_height_0() { return &___height_0; }
	inline void set_height_0(float value)
	{
		___height_0 = value;
	}

	inline static int32_t get_offset_of_leftTangent_1() { return static_cast<int32_t>(offsetof(SplineControlPoint_t5FB60D8FEB29C651E953002ECE34F9BA73654764, ___leftTangent_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftTangent_1() const { return ___leftTangent_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftTangent_1() { return &___leftTangent_1; }
	inline void set_leftTangent_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftTangent_1 = value;
	}

	inline static int32_t get_offset_of_rightTangent_2() { return static_cast<int32_t>(offsetof(SplineControlPoint_t5FB60D8FEB29C651E953002ECE34F9BA73654764, ___rightTangent_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightTangent_2() const { return ___rightTangent_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightTangent_2() { return &___rightTangent_2; }
	inline void set_rightTangent_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightTangent_2 = value;
	}

	inline static int32_t get_offset_of_position_3() { return static_cast<int32_t>(offsetof(SplineControlPoint_t5FB60D8FEB29C651E953002ECE34F9BA73654764, ___position_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_position_3() const { return ___position_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_position_3() { return &___position_3; }
	inline void set_position_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___position_3 = value;
	}

	inline static int32_t get_offset_of_mode_4() { return static_cast<int32_t>(offsetof(SplineControlPoint_t5FB60D8FEB29C651E953002ECE34F9BA73654764, ___mode_4)); }
	inline int32_t get_mode_4() const { return ___mode_4; }
	inline int32_t* get_address_of_mode_4() { return &___mode_4; }
	inline void set_mode_4(int32_t value)
	{
		___mode_4 = value;
	}

	inline static int32_t get_offset_of_subdivisionsOverride_5() { return static_cast<int32_t>(offsetof(SplineControlPoint_t5FB60D8FEB29C651E953002ECE34F9BA73654764, ___subdivisionsOverride_5)); }
	inline int32_t get_subdivisionsOverride_5() const { return ___subdivisionsOverride_5; }
	inline int32_t* get_address_of_subdivisionsOverride_5() { return &___subdivisionsOverride_5; }
	inline void set_subdivisionsOverride_5(int32_t value)
	{
		___subdivisionsOverride_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINECONTROLPOINT_T5FB60D8FEB29C651E953002ECE34F9BA73654764_H
#ifndef SPLINEDECORATIONDATA_T24E7CE18B8562961F4A230E019E1B7EFFFA6721E_H
#define SPLINEDECORATIONDATA_T24E7CE18B8562961F4A230E019E1B7EFFFA6721E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineDecorator/SplineDecorationData
struct  SplineDecorationData_t24E7CE18B8562961F4A230E019E1B7EFFFA6721E 
{
public:
	// Kolibri2d.Decoration Kolibri2d.SplineDecorator/SplineDecorationData::decoration
	Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159 * ___decoration_0;
	// System.Single Kolibri2d.SplineDecorator/SplineDecorationData::scale
	float ___scale_1;
	// System.Single Kolibri2d.SplineDecorator/SplineDecorationData::halfWidth
	float ___halfWidth_2;
	// System.Single Kolibri2d.SplineDecorator/SplineDecorationData::halfWidthPlusMargin
	float ___halfWidthPlusMargin_3;
	// UnityEngine.Vector3 Kolibri2d.SplineDecorator/SplineDecorationData::dir
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___dir_4;
	// UnityEngine.Vector3 Kolibri2d.SplineDecorator/SplineDecorationData::pos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pos_5;
	// System.Single Kolibri2d.SplineDecorator/SplineDecorationData::angle
	float ___angle_6;
	// Kolibri2d.TerrainType Kolibri2d.SplineDecorator/SplineDecorationData::middleType
	int32_t ___middleType_7;
	// Kolibri2d.TerrainType Kolibri2d.SplineDecorator/SplineDecorationData::beginType
	int32_t ___beginType_8;
	// System.Boolean Kolibri2d.SplineDecorator/SplineDecorationData::flipX
	bool ___flipX_9;

public:
	inline static int32_t get_offset_of_decoration_0() { return static_cast<int32_t>(offsetof(SplineDecorationData_t24E7CE18B8562961F4A230E019E1B7EFFFA6721E, ___decoration_0)); }
	inline Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159 * get_decoration_0() const { return ___decoration_0; }
	inline Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159 ** get_address_of_decoration_0() { return &___decoration_0; }
	inline void set_decoration_0(Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159 * value)
	{
		___decoration_0 = value;
		Il2CppCodeGenWriteBarrier((&___decoration_0), value);
	}

	inline static int32_t get_offset_of_scale_1() { return static_cast<int32_t>(offsetof(SplineDecorationData_t24E7CE18B8562961F4A230E019E1B7EFFFA6721E, ___scale_1)); }
	inline float get_scale_1() const { return ___scale_1; }
	inline float* get_address_of_scale_1() { return &___scale_1; }
	inline void set_scale_1(float value)
	{
		___scale_1 = value;
	}

	inline static int32_t get_offset_of_halfWidth_2() { return static_cast<int32_t>(offsetof(SplineDecorationData_t24E7CE18B8562961F4A230E019E1B7EFFFA6721E, ___halfWidth_2)); }
	inline float get_halfWidth_2() const { return ___halfWidth_2; }
	inline float* get_address_of_halfWidth_2() { return &___halfWidth_2; }
	inline void set_halfWidth_2(float value)
	{
		___halfWidth_2 = value;
	}

	inline static int32_t get_offset_of_halfWidthPlusMargin_3() { return static_cast<int32_t>(offsetof(SplineDecorationData_t24E7CE18B8562961F4A230E019E1B7EFFFA6721E, ___halfWidthPlusMargin_3)); }
	inline float get_halfWidthPlusMargin_3() const { return ___halfWidthPlusMargin_3; }
	inline float* get_address_of_halfWidthPlusMargin_3() { return &___halfWidthPlusMargin_3; }
	inline void set_halfWidthPlusMargin_3(float value)
	{
		___halfWidthPlusMargin_3 = value;
	}

	inline static int32_t get_offset_of_dir_4() { return static_cast<int32_t>(offsetof(SplineDecorationData_t24E7CE18B8562961F4A230E019E1B7EFFFA6721E, ___dir_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_dir_4() const { return ___dir_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_dir_4() { return &___dir_4; }
	inline void set_dir_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___dir_4 = value;
	}

	inline static int32_t get_offset_of_pos_5() { return static_cast<int32_t>(offsetof(SplineDecorationData_t24E7CE18B8562961F4A230E019E1B7EFFFA6721E, ___pos_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_pos_5() const { return ___pos_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_pos_5() { return &___pos_5; }
	inline void set_pos_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___pos_5 = value;
	}

	inline static int32_t get_offset_of_angle_6() { return static_cast<int32_t>(offsetof(SplineDecorationData_t24E7CE18B8562961F4A230E019E1B7EFFFA6721E, ___angle_6)); }
	inline float get_angle_6() const { return ___angle_6; }
	inline float* get_address_of_angle_6() { return &___angle_6; }
	inline void set_angle_6(float value)
	{
		___angle_6 = value;
	}

	inline static int32_t get_offset_of_middleType_7() { return static_cast<int32_t>(offsetof(SplineDecorationData_t24E7CE18B8562961F4A230E019E1B7EFFFA6721E, ___middleType_7)); }
	inline int32_t get_middleType_7() const { return ___middleType_7; }
	inline int32_t* get_address_of_middleType_7() { return &___middleType_7; }
	inline void set_middleType_7(int32_t value)
	{
		___middleType_7 = value;
	}

	inline static int32_t get_offset_of_beginType_8() { return static_cast<int32_t>(offsetof(SplineDecorationData_t24E7CE18B8562961F4A230E019E1B7EFFFA6721E, ___beginType_8)); }
	inline int32_t get_beginType_8() const { return ___beginType_8; }
	inline int32_t* get_address_of_beginType_8() { return &___beginType_8; }
	inline void set_beginType_8(int32_t value)
	{
		___beginType_8 = value;
	}

	inline static int32_t get_offset_of_flipX_9() { return static_cast<int32_t>(offsetof(SplineDecorationData_t24E7CE18B8562961F4A230E019E1B7EFFFA6721E, ___flipX_9)); }
	inline bool get_flipX_9() const { return ___flipX_9; }
	inline bool* get_address_of_flipX_9() { return &___flipX_9; }
	inline void set_flipX_9(bool value)
	{
		___flipX_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Kolibri2d.SplineDecorator/SplineDecorationData
struct SplineDecorationData_t24E7CE18B8562961F4A230E019E1B7EFFFA6721E_marshaled_pinvoke
{
	Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159 * ___decoration_0;
	float ___scale_1;
	float ___halfWidth_2;
	float ___halfWidthPlusMargin_3;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___dir_4;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pos_5;
	float ___angle_6;
	int32_t ___middleType_7;
	int32_t ___beginType_8;
	int32_t ___flipX_9;
};
// Native definition for COM marshalling of Kolibri2d.SplineDecorator/SplineDecorationData
struct SplineDecorationData_t24E7CE18B8562961F4A230E019E1B7EFFFA6721E_marshaled_com
{
	Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159 * ___decoration_0;
	float ___scale_1;
	float ___halfWidth_2;
	float ___halfWidthPlusMargin_3;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___dir_4;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pos_5;
	float ___angle_6;
	int32_t ___middleType_7;
	int32_t ___beginType_8;
	int32_t ___flipX_9;
};
#endif // SPLINEDECORATIONDATA_T24E7CE18B8562961F4A230E019E1B7EFFFA6721E_H
#ifndef SPLINESEGMENTINFO_TBB34F651D1ED6826933D71C125491ADE7056F066_H
#define SPLINESEGMENTINFO_TBB34F651D1ED6826933D71C125491ADE7056F066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineSegmentInfo
struct  SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066 
{
public:
	// System.Int32 Kolibri2d.SplineSegmentInfo::begin
	int32_t ___begin_0;
	// System.Int32 Kolibri2d.SplineSegmentInfo::end
	int32_t ___end_1;
	// System.Single Kolibri2d.SplineSegmentInfo::angleBegin
	float ___angleBegin_2;
	// System.Single Kolibri2d.SplineSegmentInfo::angleCorner
	float ___angleCorner_3;
	// UnityEngine.Vector3 Kolibri2d.SplineSegmentInfo::dirBegin
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___dirBegin_4;
	// Kolibri2d.TerrainType Kolibri2d.SplineSegmentInfo::type
	int32_t ___type_5;

public:
	inline static int32_t get_offset_of_begin_0() { return static_cast<int32_t>(offsetof(SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066, ___begin_0)); }
	inline int32_t get_begin_0() const { return ___begin_0; }
	inline int32_t* get_address_of_begin_0() { return &___begin_0; }
	inline void set_begin_0(int32_t value)
	{
		___begin_0 = value;
	}

	inline static int32_t get_offset_of_end_1() { return static_cast<int32_t>(offsetof(SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066, ___end_1)); }
	inline int32_t get_end_1() const { return ___end_1; }
	inline int32_t* get_address_of_end_1() { return &___end_1; }
	inline void set_end_1(int32_t value)
	{
		___end_1 = value;
	}

	inline static int32_t get_offset_of_angleBegin_2() { return static_cast<int32_t>(offsetof(SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066, ___angleBegin_2)); }
	inline float get_angleBegin_2() const { return ___angleBegin_2; }
	inline float* get_address_of_angleBegin_2() { return &___angleBegin_2; }
	inline void set_angleBegin_2(float value)
	{
		___angleBegin_2 = value;
	}

	inline static int32_t get_offset_of_angleCorner_3() { return static_cast<int32_t>(offsetof(SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066, ___angleCorner_3)); }
	inline float get_angleCorner_3() const { return ___angleCorner_3; }
	inline float* get_address_of_angleCorner_3() { return &___angleCorner_3; }
	inline void set_angleCorner_3(float value)
	{
		___angleCorner_3 = value;
	}

	inline static int32_t get_offset_of_dirBegin_4() { return static_cast<int32_t>(offsetof(SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066, ___dirBegin_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_dirBegin_4() const { return ___dirBegin_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_dirBegin_4() { return &___dirBegin_4; }
	inline void set_dirBegin_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___dirBegin_4 = value;
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066, ___type_5)); }
	inline int32_t get_type_5() const { return ___type_5; }
	inline int32_t* get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(int32_t value)
	{
		___type_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINESEGMENTINFO_TBB34F651D1ED6826933D71C125491ADE7056F066_H
#ifndef CORNERSPRITES2_T56F48A9FF2507A11F647BF6F8B9F6B163B5A1703_H
#define CORNERSPRITES2_T56F48A9FF2507A11F647BF6F8B9F6B163B5A1703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2DMaterial/CornerSprites2
struct  CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703  : public RuntimeObject
{
public:
	// Kolibri2d.Terrain2DMaterial/CornerSprites2/CornerDisplayType Kolibri2d.Terrain2DMaterial/CornerSprites2::cornerDisplayType
	int32_t ___cornerDisplayType_0;
	// UnityEngine.Sprite Kolibri2d.Terrain2DMaterial/CornerSprites2::interiorTopLeft
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___interiorTopLeft_1;
	// UnityEngine.Vector2 Kolibri2d.Terrain2DMaterial/CornerSprites2::offsetITL
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___offsetITL_2;
	// System.Int32 Kolibri2d.Terrain2DMaterial/CornerSprites2::rotationITL
	int32_t ___rotationITL_3;
	// UnityEngine.Sprite Kolibri2d.Terrain2DMaterial/CornerSprites2::interiorTopRight
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___interiorTopRight_4;
	// UnityEngine.Vector2 Kolibri2d.Terrain2DMaterial/CornerSprites2::offsetITR
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___offsetITR_5;
	// System.Int32 Kolibri2d.Terrain2DMaterial/CornerSprites2::rotationITR
	int32_t ___rotationITR_6;
	// UnityEngine.Sprite Kolibri2d.Terrain2DMaterial/CornerSprites2::interiorBotLeft
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___interiorBotLeft_7;
	// UnityEngine.Vector2 Kolibri2d.Terrain2DMaterial/CornerSprites2::offsetIBL
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___offsetIBL_8;
	// System.Int32 Kolibri2d.Terrain2DMaterial/CornerSprites2::rotationIBL
	int32_t ___rotationIBL_9;
	// UnityEngine.Sprite Kolibri2d.Terrain2DMaterial/CornerSprites2::interiorBotRight
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___interiorBotRight_10;
	// UnityEngine.Vector2 Kolibri2d.Terrain2DMaterial/CornerSprites2::offsetIBR
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___offsetIBR_11;
	// System.Int32 Kolibri2d.Terrain2DMaterial/CornerSprites2::rotationIBR
	int32_t ___rotationIBR_12;
	// UnityEngine.Sprite Kolibri2d.Terrain2DMaterial/CornerSprites2::exteriorTopLeft
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___exteriorTopLeft_13;
	// UnityEngine.Vector2 Kolibri2d.Terrain2DMaterial/CornerSprites2::offsetETL
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___offsetETL_14;
	// System.Int32 Kolibri2d.Terrain2DMaterial/CornerSprites2::rotationETL
	int32_t ___rotationETL_15;
	// UnityEngine.Sprite Kolibri2d.Terrain2DMaterial/CornerSprites2::exteriorTopRight
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___exteriorTopRight_16;
	// UnityEngine.Vector2 Kolibri2d.Terrain2DMaterial/CornerSprites2::offsetETR
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___offsetETR_17;
	// System.Int32 Kolibri2d.Terrain2DMaterial/CornerSprites2::rotationETR
	int32_t ___rotationETR_18;
	// UnityEngine.Sprite Kolibri2d.Terrain2DMaterial/CornerSprites2::exteriorBotLeft
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___exteriorBotLeft_19;
	// UnityEngine.Vector2 Kolibri2d.Terrain2DMaterial/CornerSprites2::offsetEBL
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___offsetEBL_20;
	// UnityEngine.Sprite Kolibri2d.Terrain2DMaterial/CornerSprites2::exteriorBotRight
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___exteriorBotRight_21;
	// System.Int32 Kolibri2d.Terrain2DMaterial/CornerSprites2::rotationEBL
	int32_t ___rotationEBL_22;
	// UnityEngine.Vector2 Kolibri2d.Terrain2DMaterial/CornerSprites2::offsetEBR
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___offsetEBR_23;
	// System.Int32 Kolibri2d.Terrain2DMaterial/CornerSprites2::rotationEBR
	int32_t ___rotationEBR_24;

public:
	inline static int32_t get_offset_of_cornerDisplayType_0() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___cornerDisplayType_0)); }
	inline int32_t get_cornerDisplayType_0() const { return ___cornerDisplayType_0; }
	inline int32_t* get_address_of_cornerDisplayType_0() { return &___cornerDisplayType_0; }
	inline void set_cornerDisplayType_0(int32_t value)
	{
		___cornerDisplayType_0 = value;
	}

	inline static int32_t get_offset_of_interiorTopLeft_1() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___interiorTopLeft_1)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_interiorTopLeft_1() const { return ___interiorTopLeft_1; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_interiorTopLeft_1() { return &___interiorTopLeft_1; }
	inline void set_interiorTopLeft_1(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___interiorTopLeft_1 = value;
		Il2CppCodeGenWriteBarrier((&___interiorTopLeft_1), value);
	}

	inline static int32_t get_offset_of_offsetITL_2() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___offsetITL_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_offsetITL_2() const { return ___offsetITL_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_offsetITL_2() { return &___offsetITL_2; }
	inline void set_offsetITL_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___offsetITL_2 = value;
	}

	inline static int32_t get_offset_of_rotationITL_3() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___rotationITL_3)); }
	inline int32_t get_rotationITL_3() const { return ___rotationITL_3; }
	inline int32_t* get_address_of_rotationITL_3() { return &___rotationITL_3; }
	inline void set_rotationITL_3(int32_t value)
	{
		___rotationITL_3 = value;
	}

	inline static int32_t get_offset_of_interiorTopRight_4() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___interiorTopRight_4)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_interiorTopRight_4() const { return ___interiorTopRight_4; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_interiorTopRight_4() { return &___interiorTopRight_4; }
	inline void set_interiorTopRight_4(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___interiorTopRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___interiorTopRight_4), value);
	}

	inline static int32_t get_offset_of_offsetITR_5() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___offsetITR_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_offsetITR_5() const { return ___offsetITR_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_offsetITR_5() { return &___offsetITR_5; }
	inline void set_offsetITR_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___offsetITR_5 = value;
	}

	inline static int32_t get_offset_of_rotationITR_6() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___rotationITR_6)); }
	inline int32_t get_rotationITR_6() const { return ___rotationITR_6; }
	inline int32_t* get_address_of_rotationITR_6() { return &___rotationITR_6; }
	inline void set_rotationITR_6(int32_t value)
	{
		___rotationITR_6 = value;
	}

	inline static int32_t get_offset_of_interiorBotLeft_7() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___interiorBotLeft_7)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_interiorBotLeft_7() const { return ___interiorBotLeft_7; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_interiorBotLeft_7() { return &___interiorBotLeft_7; }
	inline void set_interiorBotLeft_7(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___interiorBotLeft_7 = value;
		Il2CppCodeGenWriteBarrier((&___interiorBotLeft_7), value);
	}

	inline static int32_t get_offset_of_offsetIBL_8() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___offsetIBL_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_offsetIBL_8() const { return ___offsetIBL_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_offsetIBL_8() { return &___offsetIBL_8; }
	inline void set_offsetIBL_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___offsetIBL_8 = value;
	}

	inline static int32_t get_offset_of_rotationIBL_9() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___rotationIBL_9)); }
	inline int32_t get_rotationIBL_9() const { return ___rotationIBL_9; }
	inline int32_t* get_address_of_rotationIBL_9() { return &___rotationIBL_9; }
	inline void set_rotationIBL_9(int32_t value)
	{
		___rotationIBL_9 = value;
	}

	inline static int32_t get_offset_of_interiorBotRight_10() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___interiorBotRight_10)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_interiorBotRight_10() const { return ___interiorBotRight_10; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_interiorBotRight_10() { return &___interiorBotRight_10; }
	inline void set_interiorBotRight_10(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___interiorBotRight_10 = value;
		Il2CppCodeGenWriteBarrier((&___interiorBotRight_10), value);
	}

	inline static int32_t get_offset_of_offsetIBR_11() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___offsetIBR_11)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_offsetIBR_11() const { return ___offsetIBR_11; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_offsetIBR_11() { return &___offsetIBR_11; }
	inline void set_offsetIBR_11(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___offsetIBR_11 = value;
	}

	inline static int32_t get_offset_of_rotationIBR_12() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___rotationIBR_12)); }
	inline int32_t get_rotationIBR_12() const { return ___rotationIBR_12; }
	inline int32_t* get_address_of_rotationIBR_12() { return &___rotationIBR_12; }
	inline void set_rotationIBR_12(int32_t value)
	{
		___rotationIBR_12 = value;
	}

	inline static int32_t get_offset_of_exteriorTopLeft_13() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___exteriorTopLeft_13)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_exteriorTopLeft_13() const { return ___exteriorTopLeft_13; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_exteriorTopLeft_13() { return &___exteriorTopLeft_13; }
	inline void set_exteriorTopLeft_13(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___exteriorTopLeft_13 = value;
		Il2CppCodeGenWriteBarrier((&___exteriorTopLeft_13), value);
	}

	inline static int32_t get_offset_of_offsetETL_14() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___offsetETL_14)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_offsetETL_14() const { return ___offsetETL_14; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_offsetETL_14() { return &___offsetETL_14; }
	inline void set_offsetETL_14(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___offsetETL_14 = value;
	}

	inline static int32_t get_offset_of_rotationETL_15() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___rotationETL_15)); }
	inline int32_t get_rotationETL_15() const { return ___rotationETL_15; }
	inline int32_t* get_address_of_rotationETL_15() { return &___rotationETL_15; }
	inline void set_rotationETL_15(int32_t value)
	{
		___rotationETL_15 = value;
	}

	inline static int32_t get_offset_of_exteriorTopRight_16() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___exteriorTopRight_16)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_exteriorTopRight_16() const { return ___exteriorTopRight_16; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_exteriorTopRight_16() { return &___exteriorTopRight_16; }
	inline void set_exteriorTopRight_16(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___exteriorTopRight_16 = value;
		Il2CppCodeGenWriteBarrier((&___exteriorTopRight_16), value);
	}

	inline static int32_t get_offset_of_offsetETR_17() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___offsetETR_17)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_offsetETR_17() const { return ___offsetETR_17; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_offsetETR_17() { return &___offsetETR_17; }
	inline void set_offsetETR_17(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___offsetETR_17 = value;
	}

	inline static int32_t get_offset_of_rotationETR_18() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___rotationETR_18)); }
	inline int32_t get_rotationETR_18() const { return ___rotationETR_18; }
	inline int32_t* get_address_of_rotationETR_18() { return &___rotationETR_18; }
	inline void set_rotationETR_18(int32_t value)
	{
		___rotationETR_18 = value;
	}

	inline static int32_t get_offset_of_exteriorBotLeft_19() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___exteriorBotLeft_19)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_exteriorBotLeft_19() const { return ___exteriorBotLeft_19; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_exteriorBotLeft_19() { return &___exteriorBotLeft_19; }
	inline void set_exteriorBotLeft_19(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___exteriorBotLeft_19 = value;
		Il2CppCodeGenWriteBarrier((&___exteriorBotLeft_19), value);
	}

	inline static int32_t get_offset_of_offsetEBL_20() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___offsetEBL_20)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_offsetEBL_20() const { return ___offsetEBL_20; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_offsetEBL_20() { return &___offsetEBL_20; }
	inline void set_offsetEBL_20(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___offsetEBL_20 = value;
	}

	inline static int32_t get_offset_of_exteriorBotRight_21() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___exteriorBotRight_21)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_exteriorBotRight_21() const { return ___exteriorBotRight_21; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_exteriorBotRight_21() { return &___exteriorBotRight_21; }
	inline void set_exteriorBotRight_21(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___exteriorBotRight_21 = value;
		Il2CppCodeGenWriteBarrier((&___exteriorBotRight_21), value);
	}

	inline static int32_t get_offset_of_rotationEBL_22() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___rotationEBL_22)); }
	inline int32_t get_rotationEBL_22() const { return ___rotationEBL_22; }
	inline int32_t* get_address_of_rotationEBL_22() { return &___rotationEBL_22; }
	inline void set_rotationEBL_22(int32_t value)
	{
		___rotationEBL_22 = value;
	}

	inline static int32_t get_offset_of_offsetEBR_23() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___offsetEBR_23)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_offsetEBR_23() const { return ___offsetEBR_23; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_offsetEBR_23() { return &___offsetEBR_23; }
	inline void set_offsetEBR_23(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___offsetEBR_23 = value;
	}

	inline static int32_t get_offset_of_rotationEBR_24() { return static_cast<int32_t>(offsetof(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703, ___rotationEBR_24)); }
	inline int32_t get_rotationEBR_24() const { return ___rotationEBR_24; }
	inline int32_t* get_address_of_rotationEBR_24() { return &___rotationEBR_24; }
	inline void set_rotationEBR_24(int32_t value)
	{
		___rotationEBR_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORNERSPRITES2_T56F48A9FF2507A11F647BF6F8B9F6B163B5A1703_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef ONREFRESHCALLBACK_T30E4E0D8B612B00EAE2C396E25323C621CF16300_H
#define ONREFRESHCALLBACK_T30E4E0D8B612B00EAE2C396E25323C621CF16300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.BezierSpline/OnRefreshCallback
struct  OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONREFRESHCALLBACK_T30E4E0D8B612B00EAE2C396E25323C621CF16300_H
#ifndef DECORATIONGROUP_TA420E574206C36AC498513E58EE588CEC7FA50F0_H
#define DECORATIONGROUP_TA420E574206C36AC498513E58EE588CEC7FA50F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.DecorationGroup
struct  DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Boolean Kolibri2d.DecorationGroup::disabledForEveryone
	bool ___disabledForEveryone_4;
	// Kolibri2d.DecorationGroup/DistributionType Kolibri2d.DecorationGroup::distributionType
	int32_t ___distributionType_5;
	// Kolibri2d.DecorationGroup/OrderType Kolibri2d.DecorationGroup::orderType
	int32_t ___orderType_6;
	// System.Single Kolibri2d.DecorationGroup::margin
	float ___margin_7;
	// System.Single Kolibri2d.DecorationGroup::offsetMin
	float ___offsetMin_8;
	// System.Single Kolibri2d.DecorationGroup::offsetMax
	float ___offsetMax_9;
	// System.Int32 Kolibri2d.DecorationGroup::maxObjects
	int32_t ___maxObjects_10;
	// System.Single Kolibri2d.DecorationGroup::density
	float ___density_11;
	// System.Int32 Kolibri2d.DecorationGroup::distributedAmount
	int32_t ___distributedAmount_12;
	// System.Boolean Kolibri2d.DecorationGroup::distributionPerSegment
	bool ___distributionPerSegment_13;
	// System.Single Kolibri2d.DecorationGroup::constantScale
	float ___constantScale_14;
	// System.Boolean Kolibri2d.DecorationGroup::randomizeScales
	bool ___randomizeScales_15;
	// System.Single Kolibri2d.DecorationGroup::minScale
	float ___minScale_16;
	// System.Single Kolibri2d.DecorationGroup::maxScale
	float ___maxScale_17;
	// System.Boolean Kolibri2d.DecorationGroup::setGameObjectLayer
	bool ___setGameObjectLayer_18;
	// System.Int32 Kolibri2d.DecorationGroup::layer
	int32_t ___layer_19;
	// Kolibri2d.DecorationGroup/SortingLayerOptions Kolibri2d.DecorationGroup::sortingLayerOptions
	SortingLayerOptions_t241DB2E522009CEB9D89742D1213F4B22EE99E9E * ___sortingLayerOptions_20;
	// System.Collections.Generic.List`1<Kolibri2d.Decoration> Kolibri2d.DecorationGroup::grounds
	List_1_tA358CC6533316031BC20A834B3C5627D3B034883 * ___grounds_21;
	// System.Collections.Generic.List`1<Kolibri2d.Decoration> Kolibri2d.DecorationGroup::ceilings
	List_1_tA358CC6533316031BC20A834B3C5627D3B034883 * ___ceilings_22;
	// System.Collections.Generic.List`1<Kolibri2d.Decoration> Kolibri2d.DecorationGroup::walls
	List_1_tA358CC6533316031BC20A834B3C5627D3B034883 * ___walls_23;
	// UnityEngine.Object[] Kolibri2d.DecorationGroup::DraggedObjectsFromInspector
	ObjectU5BU5D_t0989B392BF4AF7D427A68A3A60BD412E54A9573C* ___DraggedObjectsFromInspector_24;
	// Kolibri2d.DecorationGroup/RenderOrderType Kolibri2d.DecorationGroup::renderOrderType
	int32_t ___renderOrderType_25;
	// System.Single Kolibri2d.DecorationGroup::renderZDepthDelta
	float ___renderZDepthDelta_26;
	// System.Single Kolibri2d.DecorationGroup::renderZDepthMin
	float ___renderZDepthMin_27;
	// System.Single Kolibri2d.DecorationGroup::renderZDepthMax
	float ___renderZDepthMax_28;

public:
	inline static int32_t get_offset_of_disabledForEveryone_4() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___disabledForEveryone_4)); }
	inline bool get_disabledForEveryone_4() const { return ___disabledForEveryone_4; }
	inline bool* get_address_of_disabledForEveryone_4() { return &___disabledForEveryone_4; }
	inline void set_disabledForEveryone_4(bool value)
	{
		___disabledForEveryone_4 = value;
	}

	inline static int32_t get_offset_of_distributionType_5() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___distributionType_5)); }
	inline int32_t get_distributionType_5() const { return ___distributionType_5; }
	inline int32_t* get_address_of_distributionType_5() { return &___distributionType_5; }
	inline void set_distributionType_5(int32_t value)
	{
		___distributionType_5 = value;
	}

	inline static int32_t get_offset_of_orderType_6() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___orderType_6)); }
	inline int32_t get_orderType_6() const { return ___orderType_6; }
	inline int32_t* get_address_of_orderType_6() { return &___orderType_6; }
	inline void set_orderType_6(int32_t value)
	{
		___orderType_6 = value;
	}

	inline static int32_t get_offset_of_margin_7() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___margin_7)); }
	inline float get_margin_7() const { return ___margin_7; }
	inline float* get_address_of_margin_7() { return &___margin_7; }
	inline void set_margin_7(float value)
	{
		___margin_7 = value;
	}

	inline static int32_t get_offset_of_offsetMin_8() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___offsetMin_8)); }
	inline float get_offsetMin_8() const { return ___offsetMin_8; }
	inline float* get_address_of_offsetMin_8() { return &___offsetMin_8; }
	inline void set_offsetMin_8(float value)
	{
		___offsetMin_8 = value;
	}

	inline static int32_t get_offset_of_offsetMax_9() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___offsetMax_9)); }
	inline float get_offsetMax_9() const { return ___offsetMax_9; }
	inline float* get_address_of_offsetMax_9() { return &___offsetMax_9; }
	inline void set_offsetMax_9(float value)
	{
		___offsetMax_9 = value;
	}

	inline static int32_t get_offset_of_maxObjects_10() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___maxObjects_10)); }
	inline int32_t get_maxObjects_10() const { return ___maxObjects_10; }
	inline int32_t* get_address_of_maxObjects_10() { return &___maxObjects_10; }
	inline void set_maxObjects_10(int32_t value)
	{
		___maxObjects_10 = value;
	}

	inline static int32_t get_offset_of_density_11() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___density_11)); }
	inline float get_density_11() const { return ___density_11; }
	inline float* get_address_of_density_11() { return &___density_11; }
	inline void set_density_11(float value)
	{
		___density_11 = value;
	}

	inline static int32_t get_offset_of_distributedAmount_12() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___distributedAmount_12)); }
	inline int32_t get_distributedAmount_12() const { return ___distributedAmount_12; }
	inline int32_t* get_address_of_distributedAmount_12() { return &___distributedAmount_12; }
	inline void set_distributedAmount_12(int32_t value)
	{
		___distributedAmount_12 = value;
	}

	inline static int32_t get_offset_of_distributionPerSegment_13() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___distributionPerSegment_13)); }
	inline bool get_distributionPerSegment_13() const { return ___distributionPerSegment_13; }
	inline bool* get_address_of_distributionPerSegment_13() { return &___distributionPerSegment_13; }
	inline void set_distributionPerSegment_13(bool value)
	{
		___distributionPerSegment_13 = value;
	}

	inline static int32_t get_offset_of_constantScale_14() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___constantScale_14)); }
	inline float get_constantScale_14() const { return ___constantScale_14; }
	inline float* get_address_of_constantScale_14() { return &___constantScale_14; }
	inline void set_constantScale_14(float value)
	{
		___constantScale_14 = value;
	}

	inline static int32_t get_offset_of_randomizeScales_15() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___randomizeScales_15)); }
	inline bool get_randomizeScales_15() const { return ___randomizeScales_15; }
	inline bool* get_address_of_randomizeScales_15() { return &___randomizeScales_15; }
	inline void set_randomizeScales_15(bool value)
	{
		___randomizeScales_15 = value;
	}

	inline static int32_t get_offset_of_minScale_16() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___minScale_16)); }
	inline float get_minScale_16() const { return ___minScale_16; }
	inline float* get_address_of_minScale_16() { return &___minScale_16; }
	inline void set_minScale_16(float value)
	{
		___minScale_16 = value;
	}

	inline static int32_t get_offset_of_maxScale_17() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___maxScale_17)); }
	inline float get_maxScale_17() const { return ___maxScale_17; }
	inline float* get_address_of_maxScale_17() { return &___maxScale_17; }
	inline void set_maxScale_17(float value)
	{
		___maxScale_17 = value;
	}

	inline static int32_t get_offset_of_setGameObjectLayer_18() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___setGameObjectLayer_18)); }
	inline bool get_setGameObjectLayer_18() const { return ___setGameObjectLayer_18; }
	inline bool* get_address_of_setGameObjectLayer_18() { return &___setGameObjectLayer_18; }
	inline void set_setGameObjectLayer_18(bool value)
	{
		___setGameObjectLayer_18 = value;
	}

	inline static int32_t get_offset_of_layer_19() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___layer_19)); }
	inline int32_t get_layer_19() const { return ___layer_19; }
	inline int32_t* get_address_of_layer_19() { return &___layer_19; }
	inline void set_layer_19(int32_t value)
	{
		___layer_19 = value;
	}

	inline static int32_t get_offset_of_sortingLayerOptions_20() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___sortingLayerOptions_20)); }
	inline SortingLayerOptions_t241DB2E522009CEB9D89742D1213F4B22EE99E9E * get_sortingLayerOptions_20() const { return ___sortingLayerOptions_20; }
	inline SortingLayerOptions_t241DB2E522009CEB9D89742D1213F4B22EE99E9E ** get_address_of_sortingLayerOptions_20() { return &___sortingLayerOptions_20; }
	inline void set_sortingLayerOptions_20(SortingLayerOptions_t241DB2E522009CEB9D89742D1213F4B22EE99E9E * value)
	{
		___sortingLayerOptions_20 = value;
		Il2CppCodeGenWriteBarrier((&___sortingLayerOptions_20), value);
	}

	inline static int32_t get_offset_of_grounds_21() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___grounds_21)); }
	inline List_1_tA358CC6533316031BC20A834B3C5627D3B034883 * get_grounds_21() const { return ___grounds_21; }
	inline List_1_tA358CC6533316031BC20A834B3C5627D3B034883 ** get_address_of_grounds_21() { return &___grounds_21; }
	inline void set_grounds_21(List_1_tA358CC6533316031BC20A834B3C5627D3B034883 * value)
	{
		___grounds_21 = value;
		Il2CppCodeGenWriteBarrier((&___grounds_21), value);
	}

	inline static int32_t get_offset_of_ceilings_22() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___ceilings_22)); }
	inline List_1_tA358CC6533316031BC20A834B3C5627D3B034883 * get_ceilings_22() const { return ___ceilings_22; }
	inline List_1_tA358CC6533316031BC20A834B3C5627D3B034883 ** get_address_of_ceilings_22() { return &___ceilings_22; }
	inline void set_ceilings_22(List_1_tA358CC6533316031BC20A834B3C5627D3B034883 * value)
	{
		___ceilings_22 = value;
		Il2CppCodeGenWriteBarrier((&___ceilings_22), value);
	}

	inline static int32_t get_offset_of_walls_23() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___walls_23)); }
	inline List_1_tA358CC6533316031BC20A834B3C5627D3B034883 * get_walls_23() const { return ___walls_23; }
	inline List_1_tA358CC6533316031BC20A834B3C5627D3B034883 ** get_address_of_walls_23() { return &___walls_23; }
	inline void set_walls_23(List_1_tA358CC6533316031BC20A834B3C5627D3B034883 * value)
	{
		___walls_23 = value;
		Il2CppCodeGenWriteBarrier((&___walls_23), value);
	}

	inline static int32_t get_offset_of_DraggedObjectsFromInspector_24() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___DraggedObjectsFromInspector_24)); }
	inline ObjectU5BU5D_t0989B392BF4AF7D427A68A3A60BD412E54A9573C* get_DraggedObjectsFromInspector_24() const { return ___DraggedObjectsFromInspector_24; }
	inline ObjectU5BU5D_t0989B392BF4AF7D427A68A3A60BD412E54A9573C** get_address_of_DraggedObjectsFromInspector_24() { return &___DraggedObjectsFromInspector_24; }
	inline void set_DraggedObjectsFromInspector_24(ObjectU5BU5D_t0989B392BF4AF7D427A68A3A60BD412E54A9573C* value)
	{
		___DraggedObjectsFromInspector_24 = value;
		Il2CppCodeGenWriteBarrier((&___DraggedObjectsFromInspector_24), value);
	}

	inline static int32_t get_offset_of_renderOrderType_25() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___renderOrderType_25)); }
	inline int32_t get_renderOrderType_25() const { return ___renderOrderType_25; }
	inline int32_t* get_address_of_renderOrderType_25() { return &___renderOrderType_25; }
	inline void set_renderOrderType_25(int32_t value)
	{
		___renderOrderType_25 = value;
	}

	inline static int32_t get_offset_of_renderZDepthDelta_26() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___renderZDepthDelta_26)); }
	inline float get_renderZDepthDelta_26() const { return ___renderZDepthDelta_26; }
	inline float* get_address_of_renderZDepthDelta_26() { return &___renderZDepthDelta_26; }
	inline void set_renderZDepthDelta_26(float value)
	{
		___renderZDepthDelta_26 = value;
	}

	inline static int32_t get_offset_of_renderZDepthMin_27() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___renderZDepthMin_27)); }
	inline float get_renderZDepthMin_27() const { return ___renderZDepthMin_27; }
	inline float* get_address_of_renderZDepthMin_27() { return &___renderZDepthMin_27; }
	inline void set_renderZDepthMin_27(float value)
	{
		___renderZDepthMin_27 = value;
	}

	inline static int32_t get_offset_of_renderZDepthMax_28() { return static_cast<int32_t>(offsetof(DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0, ___renderZDepthMax_28)); }
	inline float get_renderZDepthMax_28() const { return ___renderZDepthMax_28; }
	inline float* get_address_of_renderZDepthMax_28() { return &___renderZDepthMax_28; }
	inline void set_renderZDepthMax_28(float value)
	{
		___renderZDepthMax_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECORATIONGROUP_TA420E574206C36AC498513E58EE588CEC7FA50F0_H
#ifndef DECORATIONMATERIAL_T0D0DD06B7E2AC4FF1E10BA9FAEAB4FEBDD125A14_H
#define DECORATIONMATERIAL_T0D0DD06B7E2AC4FF1E10BA9FAEAB4FEBDD125A14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.DecorationMaterial
struct  DecorationMaterial_t0D0DD06B7E2AC4FF1E10BA9FAEAB4FEBDD125A14  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<Kolibri2d.DecorationMaterial/GroupHolder> Kolibri2d.DecorationMaterial::groupHolders
	List_1_t1BC6EE4FE66E61BC50A73C8FB747B1A186D9D6C6 * ___groupHolders_4;

public:
	inline static int32_t get_offset_of_groupHolders_4() { return static_cast<int32_t>(offsetof(DecorationMaterial_t0D0DD06B7E2AC4FF1E10BA9FAEAB4FEBDD125A14, ___groupHolders_4)); }
	inline List_1_t1BC6EE4FE66E61BC50A73C8FB747B1A186D9D6C6 * get_groupHolders_4() const { return ___groupHolders_4; }
	inline List_1_t1BC6EE4FE66E61BC50A73C8FB747B1A186D9D6C6 ** get_address_of_groupHolders_4() { return &___groupHolders_4; }
	inline void set_groupHolders_4(List_1_t1BC6EE4FE66E61BC50A73C8FB747B1A186D9D6C6 * value)
	{
		___groupHolders_4 = value;
		Il2CppCodeGenWriteBarrier((&___groupHolders_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECORATIONMATERIAL_T0D0DD06B7E2AC4FF1E10BA9FAEAB4FEBDD125A14_H
#ifndef DTSWEEPCONTEXT_T8FD4FC643B4645CD9771CF4E3631D75404EE2D06_H
#define DTSWEEPCONTEXT_T8FD4FC643B4645CD9771CF4E3631D75404EE2D06_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.DTSweepContext
struct  DTSweepContext_t8FD4FC643B4645CD9771CF4E3631D75404EE2D06  : public TriangulationContext_tC4D337B0A4FB732C4C9B0B56D09DFE47CE4C875B
{
public:
	// System.Single Kolibri2d.Poly2Tri.DTSweepContext::ALPHA
	float ___ALPHA_7;
	// Kolibri2d.Poly2Tri.AdvancingFront Kolibri2d.Poly2Tri.DTSweepContext::Front
	AdvancingFront_tBCCD1124A9FAD23592008C9B975AA05853B39C44 * ___Front_8;
	// Kolibri2d.Poly2Tri.TriangulationPoint Kolibri2d.Poly2Tri.DTSweepContext::<Head>k__BackingField
	TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * ___U3CHeadU3Ek__BackingField_9;
	// Kolibri2d.Poly2Tri.TriangulationPoint Kolibri2d.Poly2Tri.DTSweepContext::<Tail>k__BackingField
	TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * ___U3CTailU3Ek__BackingField_10;
	// Kolibri2d.Poly2Tri.DTSweepBasin Kolibri2d.Poly2Tri.DTSweepContext::Basin
	DTSweepBasin_t47769C4C8A6508E384E73DCD2ED888AEA2A782CD * ___Basin_11;
	// Kolibri2d.Poly2Tri.DTSweepEdgeEvent Kolibri2d.Poly2Tri.DTSweepContext::EdgeEvent
	DTSweepEdgeEvent_t2BF104D3D9F78FA8F6BB2F1B63E5FFAAA9D446BC * ___EdgeEvent_12;
	// Kolibri2d.Poly2Tri.DTSweepPointComparator Kolibri2d.Poly2Tri.DTSweepContext::_comparator
	DTSweepPointComparator_t030E4A9D87B78D989BCE53C4756FBC3AA4D7EA1B * ____comparator_13;

public:
	inline static int32_t get_offset_of_ALPHA_7() { return static_cast<int32_t>(offsetof(DTSweepContext_t8FD4FC643B4645CD9771CF4E3631D75404EE2D06, ___ALPHA_7)); }
	inline float get_ALPHA_7() const { return ___ALPHA_7; }
	inline float* get_address_of_ALPHA_7() { return &___ALPHA_7; }
	inline void set_ALPHA_7(float value)
	{
		___ALPHA_7 = value;
	}

	inline static int32_t get_offset_of_Front_8() { return static_cast<int32_t>(offsetof(DTSweepContext_t8FD4FC643B4645CD9771CF4E3631D75404EE2D06, ___Front_8)); }
	inline AdvancingFront_tBCCD1124A9FAD23592008C9B975AA05853B39C44 * get_Front_8() const { return ___Front_8; }
	inline AdvancingFront_tBCCD1124A9FAD23592008C9B975AA05853B39C44 ** get_address_of_Front_8() { return &___Front_8; }
	inline void set_Front_8(AdvancingFront_tBCCD1124A9FAD23592008C9B975AA05853B39C44 * value)
	{
		___Front_8 = value;
		Il2CppCodeGenWriteBarrier((&___Front_8), value);
	}

	inline static int32_t get_offset_of_U3CHeadU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(DTSweepContext_t8FD4FC643B4645CD9771CF4E3631D75404EE2D06, ___U3CHeadU3Ek__BackingField_9)); }
	inline TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * get_U3CHeadU3Ek__BackingField_9() const { return ___U3CHeadU3Ek__BackingField_9; }
	inline TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E ** get_address_of_U3CHeadU3Ek__BackingField_9() { return &___U3CHeadU3Ek__BackingField_9; }
	inline void set_U3CHeadU3Ek__BackingField_9(TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * value)
	{
		___U3CHeadU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CTailU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(DTSweepContext_t8FD4FC643B4645CD9771CF4E3631D75404EE2D06, ___U3CTailU3Ek__BackingField_10)); }
	inline TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * get_U3CTailU3Ek__BackingField_10() const { return ___U3CTailU3Ek__BackingField_10; }
	inline TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E ** get_address_of_U3CTailU3Ek__BackingField_10() { return &___U3CTailU3Ek__BackingField_10; }
	inline void set_U3CTailU3Ek__BackingField_10(TriangulationPoint_tBBB89E93E69AAF59B2A8B7662D60C619242E0F8E * value)
	{
		___U3CTailU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTailU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_Basin_11() { return static_cast<int32_t>(offsetof(DTSweepContext_t8FD4FC643B4645CD9771CF4E3631D75404EE2D06, ___Basin_11)); }
	inline DTSweepBasin_t47769C4C8A6508E384E73DCD2ED888AEA2A782CD * get_Basin_11() const { return ___Basin_11; }
	inline DTSweepBasin_t47769C4C8A6508E384E73DCD2ED888AEA2A782CD ** get_address_of_Basin_11() { return &___Basin_11; }
	inline void set_Basin_11(DTSweepBasin_t47769C4C8A6508E384E73DCD2ED888AEA2A782CD * value)
	{
		___Basin_11 = value;
		Il2CppCodeGenWriteBarrier((&___Basin_11), value);
	}

	inline static int32_t get_offset_of_EdgeEvent_12() { return static_cast<int32_t>(offsetof(DTSweepContext_t8FD4FC643B4645CD9771CF4E3631D75404EE2D06, ___EdgeEvent_12)); }
	inline DTSweepEdgeEvent_t2BF104D3D9F78FA8F6BB2F1B63E5FFAAA9D446BC * get_EdgeEvent_12() const { return ___EdgeEvent_12; }
	inline DTSweepEdgeEvent_t2BF104D3D9F78FA8F6BB2F1B63E5FFAAA9D446BC ** get_address_of_EdgeEvent_12() { return &___EdgeEvent_12; }
	inline void set_EdgeEvent_12(DTSweepEdgeEvent_t2BF104D3D9F78FA8F6BB2F1B63E5FFAAA9D446BC * value)
	{
		___EdgeEvent_12 = value;
		Il2CppCodeGenWriteBarrier((&___EdgeEvent_12), value);
	}

	inline static int32_t get_offset_of__comparator_13() { return static_cast<int32_t>(offsetof(DTSweepContext_t8FD4FC643B4645CD9771CF4E3631D75404EE2D06, ____comparator_13)); }
	inline DTSweepPointComparator_t030E4A9D87B78D989BCE53C4756FBC3AA4D7EA1B * get__comparator_13() const { return ____comparator_13; }
	inline DTSweepPointComparator_t030E4A9D87B78D989BCE53C4756FBC3AA4D7EA1B ** get_address_of__comparator_13() { return &____comparator_13; }
	inline void set__comparator_13(DTSweepPointComparator_t030E4A9D87B78D989BCE53C4756FBC3AA4D7EA1B * value)
	{
		____comparator_13 = value;
		Il2CppCodeGenWriteBarrier((&____comparator_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTSWEEPCONTEXT_T8FD4FC643B4645CD9771CF4E3631D75404EE2D06_H
#ifndef POLYGON_T3EF76BF4F85438E756235CCCE01F1DC3B5E2C99D_H
#define POLYGON_T3EF76BF4F85438E756235CCCE01F1DC3B5E2C99D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Poly2Tri.Polygon
struct  Polygon_t3EF76BF4F85438E756235CCCE01F1DC3B5E2C99D  : public Point2DList_t1DA5C4E77D36D22BDA48CBDA9553E497CF0E1AF0
{
public:
	// System.Collections.Generic.Dictionary`2<System.UInt32,Kolibri2d.Poly2Tri.TriangulationPoint> Kolibri2d.Poly2Tri.Polygon::mPointMap
	Dictionary_2_tEF8705C50BC680A7436EB757C3290BE780393000 * ___mPointMap_7;
	// System.Collections.Generic.List`1<Kolibri2d.Poly2Tri.DelaunayTriangle> Kolibri2d.Poly2Tri.Polygon::mTriangles
	List_1_tA161C1D023FB6D4167CAE9ED2A7AB1554978B9D9 * ___mTriangles_8;
	// System.Double Kolibri2d.Poly2Tri.Polygon::mPrecision
	double ___mPrecision_9;
	// System.Collections.Generic.List`1<Kolibri2d.Poly2Tri.Polygon> Kolibri2d.Poly2Tri.Polygon::mHoles
	List_1_t458521EC9E0F7DCD4972FFAF1DF76E029E984E67 * ___mHoles_10;
	// System.Collections.Generic.List`1<Kolibri2d.Poly2Tri.TriangulationPoint> Kolibri2d.Poly2Tri.Polygon::mSteinerPoints
	List_1_tAAF0D3178D082F2377FDDDC38ED8BC5C83A1FF81 * ___mSteinerPoints_11;
	// Kolibri2d.Poly2Tri.PolygonPoint Kolibri2d.Poly2Tri.Polygon::_last
	PolygonPoint_t94267F9D8AC5851730C1D804609D51ABC86911E4 * ____last_12;

public:
	inline static int32_t get_offset_of_mPointMap_7() { return static_cast<int32_t>(offsetof(Polygon_t3EF76BF4F85438E756235CCCE01F1DC3B5E2C99D, ___mPointMap_7)); }
	inline Dictionary_2_tEF8705C50BC680A7436EB757C3290BE780393000 * get_mPointMap_7() const { return ___mPointMap_7; }
	inline Dictionary_2_tEF8705C50BC680A7436EB757C3290BE780393000 ** get_address_of_mPointMap_7() { return &___mPointMap_7; }
	inline void set_mPointMap_7(Dictionary_2_tEF8705C50BC680A7436EB757C3290BE780393000 * value)
	{
		___mPointMap_7 = value;
		Il2CppCodeGenWriteBarrier((&___mPointMap_7), value);
	}

	inline static int32_t get_offset_of_mTriangles_8() { return static_cast<int32_t>(offsetof(Polygon_t3EF76BF4F85438E756235CCCE01F1DC3B5E2C99D, ___mTriangles_8)); }
	inline List_1_tA161C1D023FB6D4167CAE9ED2A7AB1554978B9D9 * get_mTriangles_8() const { return ___mTriangles_8; }
	inline List_1_tA161C1D023FB6D4167CAE9ED2A7AB1554978B9D9 ** get_address_of_mTriangles_8() { return &___mTriangles_8; }
	inline void set_mTriangles_8(List_1_tA161C1D023FB6D4167CAE9ED2A7AB1554978B9D9 * value)
	{
		___mTriangles_8 = value;
		Il2CppCodeGenWriteBarrier((&___mTriangles_8), value);
	}

	inline static int32_t get_offset_of_mPrecision_9() { return static_cast<int32_t>(offsetof(Polygon_t3EF76BF4F85438E756235CCCE01F1DC3B5E2C99D, ___mPrecision_9)); }
	inline double get_mPrecision_9() const { return ___mPrecision_9; }
	inline double* get_address_of_mPrecision_9() { return &___mPrecision_9; }
	inline void set_mPrecision_9(double value)
	{
		___mPrecision_9 = value;
	}

	inline static int32_t get_offset_of_mHoles_10() { return static_cast<int32_t>(offsetof(Polygon_t3EF76BF4F85438E756235CCCE01F1DC3B5E2C99D, ___mHoles_10)); }
	inline List_1_t458521EC9E0F7DCD4972FFAF1DF76E029E984E67 * get_mHoles_10() const { return ___mHoles_10; }
	inline List_1_t458521EC9E0F7DCD4972FFAF1DF76E029E984E67 ** get_address_of_mHoles_10() { return &___mHoles_10; }
	inline void set_mHoles_10(List_1_t458521EC9E0F7DCD4972FFAF1DF76E029E984E67 * value)
	{
		___mHoles_10 = value;
		Il2CppCodeGenWriteBarrier((&___mHoles_10), value);
	}

	inline static int32_t get_offset_of_mSteinerPoints_11() { return static_cast<int32_t>(offsetof(Polygon_t3EF76BF4F85438E756235CCCE01F1DC3B5E2C99D, ___mSteinerPoints_11)); }
	inline List_1_tAAF0D3178D082F2377FDDDC38ED8BC5C83A1FF81 * get_mSteinerPoints_11() const { return ___mSteinerPoints_11; }
	inline List_1_tAAF0D3178D082F2377FDDDC38ED8BC5C83A1FF81 ** get_address_of_mSteinerPoints_11() { return &___mSteinerPoints_11; }
	inline void set_mSteinerPoints_11(List_1_tAAF0D3178D082F2377FDDDC38ED8BC5C83A1FF81 * value)
	{
		___mSteinerPoints_11 = value;
		Il2CppCodeGenWriteBarrier((&___mSteinerPoints_11), value);
	}

	inline static int32_t get_offset_of__last_12() { return static_cast<int32_t>(offsetof(Polygon_t3EF76BF4F85438E756235CCCE01F1DC3B5E2C99D, ____last_12)); }
	inline PolygonPoint_t94267F9D8AC5851730C1D804609D51ABC86911E4 * get__last_12() const { return ____last_12; }
	inline PolygonPoint_t94267F9D8AC5851730C1D804609D51ABC86911E4 ** get_address_of__last_12() { return &____last_12; }
	inline void set__last_12(PolygonPoint_t94267F9D8AC5851730C1D804609D51ABC86911E4 * value)
	{
		____last_12 = value;
		Il2CppCodeGenWriteBarrier((&____last_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGON_T3EF76BF4F85438E756235CCCE01F1DC3B5E2C99D_H
#ifndef TERRAIN2DARCHITECTUREANGLES_T804E8203AA786D1A464DF24D7EAFC66109AC4208_H
#define TERRAIN2DARCHITECTUREANGLES_T804E8203AA786D1A464DF24D7EAFC66109AC4208_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2DArchitectureAngles
struct  Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Single Kolibri2d.Terrain2DArchitectureAngles::maxGroundAngle
	float ___maxGroundAngle_5;
	// System.Single Kolibri2d.Terrain2DArchitectureAngles::maxCeilingAngle
	float ___maxCeilingAngle_6;
	// System.Boolean Kolibri2d.Terrain2DArchitectureAngles::replaceSegments
	bool ___replaceSegments_7;
	// Kolibri2d.TerrainType Kolibri2d.Terrain2DArchitectureAngles::replaceGroundWith
	int32_t ___replaceGroundWith_8;
	// Kolibri2d.TerrainType Kolibri2d.Terrain2DArchitectureAngles::replaceWallsWith
	int32_t ___replaceWallsWith_9;
	// Kolibri2d.TerrainType Kolibri2d.Terrain2DArchitectureAngles::replaceCeilingWith
	int32_t ___replaceCeilingWith_10;

public:
	inline static int32_t get_offset_of_maxGroundAngle_5() { return static_cast<int32_t>(offsetof(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208, ___maxGroundAngle_5)); }
	inline float get_maxGroundAngle_5() const { return ___maxGroundAngle_5; }
	inline float* get_address_of_maxGroundAngle_5() { return &___maxGroundAngle_5; }
	inline void set_maxGroundAngle_5(float value)
	{
		___maxGroundAngle_5 = value;
	}

	inline static int32_t get_offset_of_maxCeilingAngle_6() { return static_cast<int32_t>(offsetof(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208, ___maxCeilingAngle_6)); }
	inline float get_maxCeilingAngle_6() const { return ___maxCeilingAngle_6; }
	inline float* get_address_of_maxCeilingAngle_6() { return &___maxCeilingAngle_6; }
	inline void set_maxCeilingAngle_6(float value)
	{
		___maxCeilingAngle_6 = value;
	}

	inline static int32_t get_offset_of_replaceSegments_7() { return static_cast<int32_t>(offsetof(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208, ___replaceSegments_7)); }
	inline bool get_replaceSegments_7() const { return ___replaceSegments_7; }
	inline bool* get_address_of_replaceSegments_7() { return &___replaceSegments_7; }
	inline void set_replaceSegments_7(bool value)
	{
		___replaceSegments_7 = value;
	}

	inline static int32_t get_offset_of_replaceGroundWith_8() { return static_cast<int32_t>(offsetof(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208, ___replaceGroundWith_8)); }
	inline int32_t get_replaceGroundWith_8() const { return ___replaceGroundWith_8; }
	inline int32_t* get_address_of_replaceGroundWith_8() { return &___replaceGroundWith_8; }
	inline void set_replaceGroundWith_8(int32_t value)
	{
		___replaceGroundWith_8 = value;
	}

	inline static int32_t get_offset_of_replaceWallsWith_9() { return static_cast<int32_t>(offsetof(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208, ___replaceWallsWith_9)); }
	inline int32_t get_replaceWallsWith_9() const { return ___replaceWallsWith_9; }
	inline int32_t* get_address_of_replaceWallsWith_9() { return &___replaceWallsWith_9; }
	inline void set_replaceWallsWith_9(int32_t value)
	{
		___replaceWallsWith_9 = value;
	}

	inline static int32_t get_offset_of_replaceCeilingWith_10() { return static_cast<int32_t>(offsetof(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208, ___replaceCeilingWith_10)); }
	inline int32_t get_replaceCeilingWith_10() const { return ___replaceCeilingWith_10; }
	inline int32_t* get_address_of_replaceCeilingWith_10() { return &___replaceCeilingWith_10; }
	inline void set_replaceCeilingWith_10(int32_t value)
	{
		___replaceCeilingWith_10 = value;
	}
};

struct Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_StaticFields
{
public:
	// Kolibri2d.Terrain2DArchitectureAngles Kolibri2d.Terrain2DArchitectureAngles::_instance
	Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * ____instance_4;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_StaticFields, ____instance_4)); }
	inline Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * get__instance_4() const { return ____instance_4; }
	inline Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((&____instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERRAIN2DARCHITECTUREANGLES_T804E8203AA786D1A464DF24D7EAFC66109AC4208_H
#ifndef TERRAIN2DMATERIAL_TEEB7D9BF129459584A2806CF311886501F0EE44D_H
#define TERRAIN2DMATERIAL_TEEB7D9BF129459584A2806CF311886501F0EE44D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2DMaterial
struct  Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Single Kolibri2d.Terrain2DMaterial::scale
	float ___scale_4;
	// System.Single Kolibri2d.Terrain2DMaterial::offset
	float ___offset_5;
	// System.Collections.Generic.Dictionary`2<Kolibri2d.TerrainType,Kolibri2d.MaterialTextureInfo> Kolibri2d.Terrain2DMaterial::textureInfoByType
	Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A * ___textureInfoByType_6;
	// Kolibri2d.MaterialTextureInfo Kolibri2d.Terrain2DMaterial::infoGround
	MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  ___infoGround_7;
	// Kolibri2d.MaterialTextureInfo Kolibri2d.Terrain2DMaterial::infoWall
	MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  ___infoWall_8;
	// Kolibri2d.MaterialTextureInfo Kolibri2d.Terrain2DMaterial::infoCeiling
	MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  ___infoCeiling_9;
	// Kolibri2d.MaterialTextureInfo Kolibri2d.Terrain2DMaterial::infoFill
	MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  ___infoFill_10;
	// System.Boolean Kolibri2d.Terrain2DMaterial::useCorners
	bool ___useCorners_12;
	// System.Boolean Kolibri2d.Terrain2DMaterial::cornersFollowsPath
	bool ___cornersFollowsPath_13;
	// Kolibri2d.Terrain2DMaterial/CornerSprites2 Kolibri2d.Terrain2DMaterial::cornerSprites
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703 * ___cornerSprites_14;
	// Kolibri2d.Terrain2DMaterial/SortingLayerOptions2 Kolibri2d.Terrain2DMaterial::layersAndSorting
	SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE * ___layersAndSorting_15;
	// System.Boolean Kolibri2d.Terrain2DMaterial::splitPaths
	bool ___splitPaths_16;
	// System.Boolean Kolibri2d.Terrain2DMaterial::rotateTextures
	bool ___rotateTextures_17;
	// UnityEngine.Material Kolibri2d.Terrain2DMaterial::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_18;
	// Kolibri2d.SplineColliderSettings Kolibri2d.Terrain2DMaterial::settings
	SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4 * ___settings_19;
	// System.Int32 Kolibri2d.Terrain2DMaterial::objectLayer
	int32_t ___objectLayer_20;
	// Kolibri2d.Terrain2DMaterial/RenderOrderType Kolibri2d.Terrain2DMaterial::renderOrderType
	int32_t ___renderOrderType_21;
	// Kolibri2d.Terrain2DMaterial/SortingZ_3D_Options Kolibri2d.Terrain2DMaterial::z_depthSorting
	SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F * ___z_depthSorting_22;
	// UnityEngine.Color Kolibri2d.Terrain2DMaterial::ColorFill
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___ColorFill_23;
	// Kolibri2d.Terrain2DMaterial/FillType Kolibri2d.Terrain2DMaterial::fillType
	int32_t ___fillType_24;

public:
	inline static int32_t get_offset_of_scale_4() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___scale_4)); }
	inline float get_scale_4() const { return ___scale_4; }
	inline float* get_address_of_scale_4() { return &___scale_4; }
	inline void set_scale_4(float value)
	{
		___scale_4 = value;
	}

	inline static int32_t get_offset_of_offset_5() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___offset_5)); }
	inline float get_offset_5() const { return ___offset_5; }
	inline float* get_address_of_offset_5() { return &___offset_5; }
	inline void set_offset_5(float value)
	{
		___offset_5 = value;
	}

	inline static int32_t get_offset_of_textureInfoByType_6() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___textureInfoByType_6)); }
	inline Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A * get_textureInfoByType_6() const { return ___textureInfoByType_6; }
	inline Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A ** get_address_of_textureInfoByType_6() { return &___textureInfoByType_6; }
	inline void set_textureInfoByType_6(Dictionary_2_t57122ABBA5C27104755B10FD4CDC2D5583CE469A * value)
	{
		___textureInfoByType_6 = value;
		Il2CppCodeGenWriteBarrier((&___textureInfoByType_6), value);
	}

	inline static int32_t get_offset_of_infoGround_7() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___infoGround_7)); }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  get_infoGround_7() const { return ___infoGround_7; }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F * get_address_of_infoGround_7() { return &___infoGround_7; }
	inline void set_infoGround_7(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  value)
	{
		___infoGround_7 = value;
	}

	inline static int32_t get_offset_of_infoWall_8() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___infoWall_8)); }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  get_infoWall_8() const { return ___infoWall_8; }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F * get_address_of_infoWall_8() { return &___infoWall_8; }
	inline void set_infoWall_8(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  value)
	{
		___infoWall_8 = value;
	}

	inline static int32_t get_offset_of_infoCeiling_9() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___infoCeiling_9)); }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  get_infoCeiling_9() const { return ___infoCeiling_9; }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F * get_address_of_infoCeiling_9() { return &___infoCeiling_9; }
	inline void set_infoCeiling_9(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  value)
	{
		___infoCeiling_9 = value;
	}

	inline static int32_t get_offset_of_infoFill_10() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___infoFill_10)); }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  get_infoFill_10() const { return ___infoFill_10; }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F * get_address_of_infoFill_10() { return &___infoFill_10; }
	inline void set_infoFill_10(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  value)
	{
		___infoFill_10 = value;
	}

	inline static int32_t get_offset_of_useCorners_12() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___useCorners_12)); }
	inline bool get_useCorners_12() const { return ___useCorners_12; }
	inline bool* get_address_of_useCorners_12() { return &___useCorners_12; }
	inline void set_useCorners_12(bool value)
	{
		___useCorners_12 = value;
	}

	inline static int32_t get_offset_of_cornersFollowsPath_13() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___cornersFollowsPath_13)); }
	inline bool get_cornersFollowsPath_13() const { return ___cornersFollowsPath_13; }
	inline bool* get_address_of_cornersFollowsPath_13() { return &___cornersFollowsPath_13; }
	inline void set_cornersFollowsPath_13(bool value)
	{
		___cornersFollowsPath_13 = value;
	}

	inline static int32_t get_offset_of_cornerSprites_14() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___cornerSprites_14)); }
	inline CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703 * get_cornerSprites_14() const { return ___cornerSprites_14; }
	inline CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703 ** get_address_of_cornerSprites_14() { return &___cornerSprites_14; }
	inline void set_cornerSprites_14(CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703 * value)
	{
		___cornerSprites_14 = value;
		Il2CppCodeGenWriteBarrier((&___cornerSprites_14), value);
	}

	inline static int32_t get_offset_of_layersAndSorting_15() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___layersAndSorting_15)); }
	inline SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE * get_layersAndSorting_15() const { return ___layersAndSorting_15; }
	inline SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE ** get_address_of_layersAndSorting_15() { return &___layersAndSorting_15; }
	inline void set_layersAndSorting_15(SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE * value)
	{
		___layersAndSorting_15 = value;
		Il2CppCodeGenWriteBarrier((&___layersAndSorting_15), value);
	}

	inline static int32_t get_offset_of_splitPaths_16() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___splitPaths_16)); }
	inline bool get_splitPaths_16() const { return ___splitPaths_16; }
	inline bool* get_address_of_splitPaths_16() { return &___splitPaths_16; }
	inline void set_splitPaths_16(bool value)
	{
		___splitPaths_16 = value;
	}

	inline static int32_t get_offset_of_rotateTextures_17() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___rotateTextures_17)); }
	inline bool get_rotateTextures_17() const { return ___rotateTextures_17; }
	inline bool* get_address_of_rotateTextures_17() { return &___rotateTextures_17; }
	inline void set_rotateTextures_17(bool value)
	{
		___rotateTextures_17 = value;
	}

	inline static int32_t get_offset_of_material_18() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___material_18)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_18() const { return ___material_18; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_18() { return &___material_18; }
	inline void set_material_18(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_18 = value;
		Il2CppCodeGenWriteBarrier((&___material_18), value);
	}

	inline static int32_t get_offset_of_settings_19() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___settings_19)); }
	inline SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4 * get_settings_19() const { return ___settings_19; }
	inline SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4 ** get_address_of_settings_19() { return &___settings_19; }
	inline void set_settings_19(SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4 * value)
	{
		___settings_19 = value;
		Il2CppCodeGenWriteBarrier((&___settings_19), value);
	}

	inline static int32_t get_offset_of_objectLayer_20() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___objectLayer_20)); }
	inline int32_t get_objectLayer_20() const { return ___objectLayer_20; }
	inline int32_t* get_address_of_objectLayer_20() { return &___objectLayer_20; }
	inline void set_objectLayer_20(int32_t value)
	{
		___objectLayer_20 = value;
	}

	inline static int32_t get_offset_of_renderOrderType_21() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___renderOrderType_21)); }
	inline int32_t get_renderOrderType_21() const { return ___renderOrderType_21; }
	inline int32_t* get_address_of_renderOrderType_21() { return &___renderOrderType_21; }
	inline void set_renderOrderType_21(int32_t value)
	{
		___renderOrderType_21 = value;
	}

	inline static int32_t get_offset_of_z_depthSorting_22() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___z_depthSorting_22)); }
	inline SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F * get_z_depthSorting_22() const { return ___z_depthSorting_22; }
	inline SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F ** get_address_of_z_depthSorting_22() { return &___z_depthSorting_22; }
	inline void set_z_depthSorting_22(SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F * value)
	{
		___z_depthSorting_22 = value;
		Il2CppCodeGenWriteBarrier((&___z_depthSorting_22), value);
	}

	inline static int32_t get_offset_of_ColorFill_23() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___ColorFill_23)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_ColorFill_23() const { return ___ColorFill_23; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_ColorFill_23() { return &___ColorFill_23; }
	inline void set_ColorFill_23(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___ColorFill_23 = value;
	}

	inline static int32_t get_offset_of_fillType_24() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D, ___fillType_24)); }
	inline int32_t get_fillType_24() const { return ___fillType_24; }
	inline int32_t* get_address_of_fillType_24() { return &___fillType_24; }
	inline void set_fillType_24(int32_t value)
	{
		___fillType_24 = value;
	}
};

struct Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D_StaticFields
{
public:
	// Kolibri2d.MaterialTextureInfo Kolibri2d.Terrain2DMaterial::infoEmpty
	MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  ___infoEmpty_11;

public:
	inline static int32_t get_offset_of_infoEmpty_11() { return static_cast<int32_t>(offsetof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D_StaticFields, ___infoEmpty_11)); }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  get_infoEmpty_11() const { return ___infoEmpty_11; }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F * get_address_of_infoEmpty_11() { return &___infoEmpty_11; }
	inline void set_infoEmpty_11(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  value)
	{
		___infoEmpty_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERRAIN2DMATERIAL_TEEB7D9BF129459584A2806CF311886501F0EE44D_H
#ifndef SESSIONSTATECHANGED_T9084549A636BD45086D66CC6765DA8C3DD31066F_H
#define SESSIONSTATECHANGED_T9084549A636BD45086D66CC6765DA8C3DD31066F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged
struct  SessionStateChanged_t9084549A636BD45086D66CC6765DA8C3DD31066F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONSTATECHANGED_T9084549A636BD45086D66CC6765DA8C3DD31066F_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef WILLRENDERCANVASES_TBD5AD090B5938021DEAA679A5AEEA790F60A8BEE_H
#define WILLRENDERCANVASES_TBD5AD090B5938021DEAA679A5AEEA790F60A8BEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Canvas/WillRenderCanvases
struct  WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WILLRENDERCANVASES_TBD5AD090B5938021DEAA679A5AEEA790F60A8BEE_H
#ifndef CANVASRENDERER_TB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72_H
#define CANVASRENDERER_TB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CanvasRenderer
struct  CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:
	// System.Boolean UnityEngine.CanvasRenderer::<isMask>k__BackingField
	bool ___U3CisMaskU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CisMaskU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72, ___U3CisMaskU3Ek__BackingField_4)); }
	inline bool get_U3CisMaskU3Ek__BackingField_4() const { return ___U3CisMaskU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CisMaskU3Ek__BackingField_4() { return &___U3CisMaskU3Ek__BackingField_4; }
	inline void set_U3CisMaskU3Ek__BackingField_4(bool value)
	{
		___U3CisMaskU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASRENDERER_TB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72_H
#ifndef UPDATEDEVENTHANDLER_TB0230BC83686D7126AB4D3800A66351028CA514F_H
#define UPDATEDEVENTHANDLER_TB0230BC83686D7126AB4D3800A66351028CA514F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RemoteSettings/UpdatedEventHandler
struct  UpdatedEventHandler_tB0230BC83686D7126AB4D3800A66351028CA514F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEDEVENTHANDLER_TB0230BC83686D7126AB4D3800A66351028CA514F_H
#ifndef CANVAS_TBC28BF1DD8D8499A89B5781505833D3728CF8591_H
#define CANVAS_TBC28BF1DD8D8499A89B5781505833D3728CF8591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Canvas
struct  Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_StaticFields
{
public:
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE * ___willRenderCanvases_4;

public:
	inline static int32_t get_offset_of_willRenderCanvases_4() { return static_cast<int32_t>(offsetof(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_StaticFields, ___willRenderCanvases_4)); }
	inline WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE * get_willRenderCanvases_4() const { return ___willRenderCanvases_4; }
	inline WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE ** get_address_of_willRenderCanvases_4() { return &___willRenderCanvases_4; }
	inline void set_willRenderCanvases_4(WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE * value)
	{
		___willRenderCanvases_4 = value;
		Il2CppCodeGenWriteBarrier((&___willRenderCanvases_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVAS_TBC28BF1DD8D8499A89B5781505833D3728CF8591_H
#ifndef CANVASGROUP_TE2C664C60990D1DCCEE0CC6545CC3E80369C7F90_H
#define CANVASGROUP_TE2C664C60990D1DCCEE0CC6545CC3E80369C7F90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CanvasGroup
struct  CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASGROUP_TE2C664C60990D1DCCEE0CC6545CC3E80369C7F90_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef BEZIERSPLINE_T869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_H
#define BEZIERSPLINE_T869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.BezierSpline
struct  BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<Kolibri2d.BezierSpline/BezierPoint> Kolibri2d.BezierSpline::points
	List_1_tD5F01382C85482BD3A1133E16C27709D77F65384 * ___points_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Kolibri2d.BezierSpline::allStepPoints
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___allStepPoints_5;
	// System.Collections.Generic.List`1<System.Single> Kolibri2d.BezierSpline::allStepLengths01
	List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * ___allStepLengths01_6;
	// System.Collections.Generic.List`1<System.Single> Kolibri2d.BezierSpline::allStepLengths
	List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * ___allStepLengths_7;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Kolibri2d.BezierSpline::allStepDirections
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___allStepDirections_8;
	// UnityEngine.Vector2[] Kolibri2d.BezierSpline::allStepNormals
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___allStepNormals_9;
	// System.Boolean Kolibri2d.BezierSpline::showDirection
	bool ___showDirection_10;
	// System.Boolean Kolibri2d.BezierSpline::isClockwise
	bool ___isClockwise_11;
	// System.Single Kolibri2d.BezierSpline::smoothness
	float ___smoothness_12;
	// Kolibri2d.BezierSpline/OnRefreshCallback Kolibri2d.BezierSpline::onRefreshCallback
	OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300 * ___onRefreshCallback_13;
	// Kolibri2d.SplineData Kolibri2d.BezierSpline::spline
	SplineData_t97926CA36C3A0FF54D20743F14165941B0C11747 * ___spline_14;
	// System.Collections.Generic.List`1<Kolibri2d.BezierSpline/PointData> Kolibri2d.BezierSpline::perPointData
	List_1_t96B06A7F290D61DC5641A89066DC34043C6F5286 * ___perPointData_16;
	// System.Boolean Kolibri2d.BezierSpline::adjustTangentsOnMoving
	bool ___adjustTangentsOnMoving_24;
	// Kolibri2d.BezierSplineQuality Kolibri2d.BezierSpline::quality
	BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF * ___quality_25;

public:
	inline static int32_t get_offset_of_points_4() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___points_4)); }
	inline List_1_tD5F01382C85482BD3A1133E16C27709D77F65384 * get_points_4() const { return ___points_4; }
	inline List_1_tD5F01382C85482BD3A1133E16C27709D77F65384 ** get_address_of_points_4() { return &___points_4; }
	inline void set_points_4(List_1_tD5F01382C85482BD3A1133E16C27709D77F65384 * value)
	{
		___points_4 = value;
		Il2CppCodeGenWriteBarrier((&___points_4), value);
	}

	inline static int32_t get_offset_of_allStepPoints_5() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___allStepPoints_5)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_allStepPoints_5() const { return ___allStepPoints_5; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_allStepPoints_5() { return &___allStepPoints_5; }
	inline void set_allStepPoints_5(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___allStepPoints_5 = value;
		Il2CppCodeGenWriteBarrier((&___allStepPoints_5), value);
	}

	inline static int32_t get_offset_of_allStepLengths01_6() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___allStepLengths01_6)); }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * get_allStepLengths01_6() const { return ___allStepLengths01_6; }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E ** get_address_of_allStepLengths01_6() { return &___allStepLengths01_6; }
	inline void set_allStepLengths01_6(List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * value)
	{
		___allStepLengths01_6 = value;
		Il2CppCodeGenWriteBarrier((&___allStepLengths01_6), value);
	}

	inline static int32_t get_offset_of_allStepLengths_7() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___allStepLengths_7)); }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * get_allStepLengths_7() const { return ___allStepLengths_7; }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E ** get_address_of_allStepLengths_7() { return &___allStepLengths_7; }
	inline void set_allStepLengths_7(List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * value)
	{
		___allStepLengths_7 = value;
		Il2CppCodeGenWriteBarrier((&___allStepLengths_7), value);
	}

	inline static int32_t get_offset_of_allStepDirections_8() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___allStepDirections_8)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_allStepDirections_8() const { return ___allStepDirections_8; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_allStepDirections_8() { return &___allStepDirections_8; }
	inline void set_allStepDirections_8(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___allStepDirections_8 = value;
		Il2CppCodeGenWriteBarrier((&___allStepDirections_8), value);
	}

	inline static int32_t get_offset_of_allStepNormals_9() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___allStepNormals_9)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_allStepNormals_9() const { return ___allStepNormals_9; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_allStepNormals_9() { return &___allStepNormals_9; }
	inline void set_allStepNormals_9(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___allStepNormals_9 = value;
		Il2CppCodeGenWriteBarrier((&___allStepNormals_9), value);
	}

	inline static int32_t get_offset_of_showDirection_10() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___showDirection_10)); }
	inline bool get_showDirection_10() const { return ___showDirection_10; }
	inline bool* get_address_of_showDirection_10() { return &___showDirection_10; }
	inline void set_showDirection_10(bool value)
	{
		___showDirection_10 = value;
	}

	inline static int32_t get_offset_of_isClockwise_11() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___isClockwise_11)); }
	inline bool get_isClockwise_11() const { return ___isClockwise_11; }
	inline bool* get_address_of_isClockwise_11() { return &___isClockwise_11; }
	inline void set_isClockwise_11(bool value)
	{
		___isClockwise_11 = value;
	}

	inline static int32_t get_offset_of_smoothness_12() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___smoothness_12)); }
	inline float get_smoothness_12() const { return ___smoothness_12; }
	inline float* get_address_of_smoothness_12() { return &___smoothness_12; }
	inline void set_smoothness_12(float value)
	{
		___smoothness_12 = value;
	}

	inline static int32_t get_offset_of_onRefreshCallback_13() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___onRefreshCallback_13)); }
	inline OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300 * get_onRefreshCallback_13() const { return ___onRefreshCallback_13; }
	inline OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300 ** get_address_of_onRefreshCallback_13() { return &___onRefreshCallback_13; }
	inline void set_onRefreshCallback_13(OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300 * value)
	{
		___onRefreshCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___onRefreshCallback_13), value);
	}

	inline static int32_t get_offset_of_spline_14() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___spline_14)); }
	inline SplineData_t97926CA36C3A0FF54D20743F14165941B0C11747 * get_spline_14() const { return ___spline_14; }
	inline SplineData_t97926CA36C3A0FF54D20743F14165941B0C11747 ** get_address_of_spline_14() { return &___spline_14; }
	inline void set_spline_14(SplineData_t97926CA36C3A0FF54D20743F14165941B0C11747 * value)
	{
		___spline_14 = value;
		Il2CppCodeGenWriteBarrier((&___spline_14), value);
	}

	inline static int32_t get_offset_of_perPointData_16() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___perPointData_16)); }
	inline List_1_t96B06A7F290D61DC5641A89066DC34043C6F5286 * get_perPointData_16() const { return ___perPointData_16; }
	inline List_1_t96B06A7F290D61DC5641A89066DC34043C6F5286 ** get_address_of_perPointData_16() { return &___perPointData_16; }
	inline void set_perPointData_16(List_1_t96B06A7F290D61DC5641A89066DC34043C6F5286 * value)
	{
		___perPointData_16 = value;
		Il2CppCodeGenWriteBarrier((&___perPointData_16), value);
	}

	inline static int32_t get_offset_of_adjustTangentsOnMoving_24() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___adjustTangentsOnMoving_24)); }
	inline bool get_adjustTangentsOnMoving_24() const { return ___adjustTangentsOnMoving_24; }
	inline bool* get_address_of_adjustTangentsOnMoving_24() { return &___adjustTangentsOnMoving_24; }
	inline void set_adjustTangentsOnMoving_24(bool value)
	{
		___adjustTangentsOnMoving_24 = value;
	}

	inline static int32_t get_offset_of_quality_25() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE, ___quality_25)); }
	inline BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF * get_quality_25() const { return ___quality_25; }
	inline BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF ** get_address_of_quality_25() { return &___quality_25; }
	inline void set_quality_25(BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF * value)
	{
		___quality_25 = value;
		Il2CppCodeGenWriteBarrier((&___quality_25), value);
	}
};

struct BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields
{
public:
	// System.Single Kolibri2d.BezierSpline::KEpsilon
	float ___KEpsilon_15;
	// Kolibri2d.ShapeTangentMode Kolibri2d.BezierSpline::settings_shapeTangentMode
	int32_t ___settings_shapeTangentMode_17;
	// System.Single Kolibri2d.BezierSpline::settings_smoothness
	float ___settings_smoothness_18;
	// System.Boolean Kolibri2d.BezierSpline::settings_squareLinearTangents
	bool ___settings_squareLinearTangents_19;
	// System.Boolean Kolibri2d.BezierSpline::settings_autoSelectPoints
	bool ___settings_autoSelectPoints_20;
	// System.Boolean Kolibri2d.BezierSpline::settings_pointsAlwaysVisible
	bool ___settings_pointsAlwaysVisible_21;
	// System.Single Kolibri2d.BezierSpline::settings_sceneHandleSize
	float ___settings_sceneHandleSize_22;
	// Kolibri2d.BezierSplineQuality Kolibri2d.BezierSpline::settings_quality
	BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF * ___settings_quality_23;

public:
	inline static int32_t get_offset_of_KEpsilon_15() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields, ___KEpsilon_15)); }
	inline float get_KEpsilon_15() const { return ___KEpsilon_15; }
	inline float* get_address_of_KEpsilon_15() { return &___KEpsilon_15; }
	inline void set_KEpsilon_15(float value)
	{
		___KEpsilon_15 = value;
	}

	inline static int32_t get_offset_of_settings_shapeTangentMode_17() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields, ___settings_shapeTangentMode_17)); }
	inline int32_t get_settings_shapeTangentMode_17() const { return ___settings_shapeTangentMode_17; }
	inline int32_t* get_address_of_settings_shapeTangentMode_17() { return &___settings_shapeTangentMode_17; }
	inline void set_settings_shapeTangentMode_17(int32_t value)
	{
		___settings_shapeTangentMode_17 = value;
	}

	inline static int32_t get_offset_of_settings_smoothness_18() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields, ___settings_smoothness_18)); }
	inline float get_settings_smoothness_18() const { return ___settings_smoothness_18; }
	inline float* get_address_of_settings_smoothness_18() { return &___settings_smoothness_18; }
	inline void set_settings_smoothness_18(float value)
	{
		___settings_smoothness_18 = value;
	}

	inline static int32_t get_offset_of_settings_squareLinearTangents_19() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields, ___settings_squareLinearTangents_19)); }
	inline bool get_settings_squareLinearTangents_19() const { return ___settings_squareLinearTangents_19; }
	inline bool* get_address_of_settings_squareLinearTangents_19() { return &___settings_squareLinearTangents_19; }
	inline void set_settings_squareLinearTangents_19(bool value)
	{
		___settings_squareLinearTangents_19 = value;
	}

	inline static int32_t get_offset_of_settings_autoSelectPoints_20() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields, ___settings_autoSelectPoints_20)); }
	inline bool get_settings_autoSelectPoints_20() const { return ___settings_autoSelectPoints_20; }
	inline bool* get_address_of_settings_autoSelectPoints_20() { return &___settings_autoSelectPoints_20; }
	inline void set_settings_autoSelectPoints_20(bool value)
	{
		___settings_autoSelectPoints_20 = value;
	}

	inline static int32_t get_offset_of_settings_pointsAlwaysVisible_21() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields, ___settings_pointsAlwaysVisible_21)); }
	inline bool get_settings_pointsAlwaysVisible_21() const { return ___settings_pointsAlwaysVisible_21; }
	inline bool* get_address_of_settings_pointsAlwaysVisible_21() { return &___settings_pointsAlwaysVisible_21; }
	inline void set_settings_pointsAlwaysVisible_21(bool value)
	{
		___settings_pointsAlwaysVisible_21 = value;
	}

	inline static int32_t get_offset_of_settings_sceneHandleSize_22() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields, ___settings_sceneHandleSize_22)); }
	inline float get_settings_sceneHandleSize_22() const { return ___settings_sceneHandleSize_22; }
	inline float* get_address_of_settings_sceneHandleSize_22() { return &___settings_sceneHandleSize_22; }
	inline void set_settings_sceneHandleSize_22(float value)
	{
		___settings_sceneHandleSize_22 = value;
	}

	inline static int32_t get_offset_of_settings_quality_23() { return static_cast<int32_t>(offsetof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields, ___settings_quality_23)); }
	inline BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF * get_settings_quality_23() const { return ___settings_quality_23; }
	inline BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF ** get_address_of_settings_quality_23() { return &___settings_quality_23; }
	inline void set_settings_quality_23(BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF * value)
	{
		___settings_quality_23 = value;
		Il2CppCodeGenWriteBarrier((&___settings_quality_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEZIERSPLINE_T869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_H
#ifndef SPLINECOLLIDERCREATOR_TF1A0739E71C3626B1789AA6E68D8BD5244C83B1A_H
#define SPLINECOLLIDERCREATOR_TF1A0739E71C3626B1789AA6E68D8BD5244C83B1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineColliderCreator
struct  SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Kolibri2d.BezierSpline Kolibri2d.SplineColliderCreator::spline
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * ___spline_5;
	// Kolibri2d.Terrain2DArchitecture Kolibri2d.SplineColliderCreator::arch
	Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * ___arch_6;
	// System.Boolean Kolibri2d.SplineColliderCreator::useGameobjectLayer
	bool ___useGameobjectLayer_7;
	// Kolibri2d.SplineColliderSettings Kolibri2d.SplineColliderCreator::physicsSettings
	SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4 * ___physicsSettings_8;
	// System.Boolean Kolibri2d.SplineColliderCreator::refreshOnNextUpdate
	bool ___refreshOnNextUpdate_9;
	// Kolibri2d.BezierSpline/OnRefreshCallback Kolibri2d.SplineColliderCreator::refreshCallback
	OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300 * ___refreshCallback_10;

public:
	inline static int32_t get_offset_of_spline_5() { return static_cast<int32_t>(offsetof(SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A, ___spline_5)); }
	inline BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * get_spline_5() const { return ___spline_5; }
	inline BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE ** get_address_of_spline_5() { return &___spline_5; }
	inline void set_spline_5(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * value)
	{
		___spline_5 = value;
		Il2CppCodeGenWriteBarrier((&___spline_5), value);
	}

	inline static int32_t get_offset_of_arch_6() { return static_cast<int32_t>(offsetof(SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A, ___arch_6)); }
	inline Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * get_arch_6() const { return ___arch_6; }
	inline Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A ** get_address_of_arch_6() { return &___arch_6; }
	inline void set_arch_6(Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * value)
	{
		___arch_6 = value;
		Il2CppCodeGenWriteBarrier((&___arch_6), value);
	}

	inline static int32_t get_offset_of_useGameobjectLayer_7() { return static_cast<int32_t>(offsetof(SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A, ___useGameobjectLayer_7)); }
	inline bool get_useGameobjectLayer_7() const { return ___useGameobjectLayer_7; }
	inline bool* get_address_of_useGameobjectLayer_7() { return &___useGameobjectLayer_7; }
	inline void set_useGameobjectLayer_7(bool value)
	{
		___useGameobjectLayer_7 = value;
	}

	inline static int32_t get_offset_of_physicsSettings_8() { return static_cast<int32_t>(offsetof(SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A, ___physicsSettings_8)); }
	inline SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4 * get_physicsSettings_8() const { return ___physicsSettings_8; }
	inline SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4 ** get_address_of_physicsSettings_8() { return &___physicsSettings_8; }
	inline void set_physicsSettings_8(SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4 * value)
	{
		___physicsSettings_8 = value;
		Il2CppCodeGenWriteBarrier((&___physicsSettings_8), value);
	}

	inline static int32_t get_offset_of_refreshOnNextUpdate_9() { return static_cast<int32_t>(offsetof(SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A, ___refreshOnNextUpdate_9)); }
	inline bool get_refreshOnNextUpdate_9() const { return ___refreshOnNextUpdate_9; }
	inline bool* get_address_of_refreshOnNextUpdate_9() { return &___refreshOnNextUpdate_9; }
	inline void set_refreshOnNextUpdate_9(bool value)
	{
		___refreshOnNextUpdate_9 = value;
	}

	inline static int32_t get_offset_of_refreshCallback_10() { return static_cast<int32_t>(offsetof(SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A, ___refreshCallback_10)); }
	inline OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300 * get_refreshCallback_10() const { return ___refreshCallback_10; }
	inline OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300 ** get_address_of_refreshCallback_10() { return &___refreshCallback_10; }
	inline void set_refreshCallback_10(OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300 * value)
	{
		___refreshCallback_10 = value;
		Il2CppCodeGenWriteBarrier((&___refreshCallback_10), value);
	}
};

struct SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A_StaticFields
{
public:
	// System.String Kolibri2d.SplineColliderCreator::decorationNodeName
	String_t* ___decorationNodeName_4;

public:
	inline static int32_t get_offset_of_decorationNodeName_4() { return static_cast<int32_t>(offsetof(SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A_StaticFields, ___decorationNodeName_4)); }
	inline String_t* get_decorationNodeName_4() const { return ___decorationNodeName_4; }
	inline String_t** get_address_of_decorationNodeName_4() { return &___decorationNodeName_4; }
	inline void set_decorationNodeName_4(String_t* value)
	{
		___decorationNodeName_4 = value;
		Il2CppCodeGenWriteBarrier((&___decorationNodeName_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINECOLLIDERCREATOR_TF1A0739E71C3626B1789AA6E68D8BD5244C83B1A_H
#ifndef SPLINEMODIFIERWITHCALLBACKS_T6F0DF8A674D295BBD21F986A7733B3E12CAE1232_H
#define SPLINEMODIFIERWITHCALLBACKS_T6F0DF8A674D295BBD21F986A7733B3E12CAE1232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineModifierWithCallbacks
struct  SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Kolibri2d.BezierSpline Kolibri2d.SplineModifierWithCallbacks::spline
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * ___spline_4;
	// Kolibri2d.Terrain2DArchitecture Kolibri2d.SplineModifierWithCallbacks::arch
	Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * ___arch_5;
	// System.Boolean Kolibri2d.SplineModifierWithCallbacks::refreshOnNextUpdate
	bool ___refreshOnNextUpdate_6;
	// Kolibri2d.BezierSpline/OnRefreshCallback Kolibri2d.SplineModifierWithCallbacks::refreshCallback
	OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300 * ___refreshCallback_7;

public:
	inline static int32_t get_offset_of_spline_4() { return static_cast<int32_t>(offsetof(SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232, ___spline_4)); }
	inline BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * get_spline_4() const { return ___spline_4; }
	inline BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE ** get_address_of_spline_4() { return &___spline_4; }
	inline void set_spline_4(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * value)
	{
		___spline_4 = value;
		Il2CppCodeGenWriteBarrier((&___spline_4), value);
	}

	inline static int32_t get_offset_of_arch_5() { return static_cast<int32_t>(offsetof(SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232, ___arch_5)); }
	inline Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * get_arch_5() const { return ___arch_5; }
	inline Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A ** get_address_of_arch_5() { return &___arch_5; }
	inline void set_arch_5(Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A * value)
	{
		___arch_5 = value;
		Il2CppCodeGenWriteBarrier((&___arch_5), value);
	}

	inline static int32_t get_offset_of_refreshOnNextUpdate_6() { return static_cast<int32_t>(offsetof(SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232, ___refreshOnNextUpdate_6)); }
	inline bool get_refreshOnNextUpdate_6() const { return ___refreshOnNextUpdate_6; }
	inline bool* get_address_of_refreshOnNextUpdate_6() { return &___refreshOnNextUpdate_6; }
	inline void set_refreshOnNextUpdate_6(bool value)
	{
		___refreshOnNextUpdate_6 = value;
	}

	inline static int32_t get_offset_of_refreshCallback_7() { return static_cast<int32_t>(offsetof(SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232, ___refreshCallback_7)); }
	inline OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300 * get_refreshCallback_7() const { return ___refreshCallback_7; }
	inline OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300 ** get_address_of_refreshCallback_7() { return &___refreshCallback_7; }
	inline void set_refreshCallback_7(OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300 * value)
	{
		___refreshCallback_7 = value;
		Il2CppCodeGenWriteBarrier((&___refreshCallback_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINEMODIFIERWITHCALLBACKS_T6F0DF8A674D295BBD21F986A7733B3E12CAE1232_H
#ifndef TERRAIN2DARCHITECTURE_T01B2C8A0361E9A7595B6F9267322714F6672DB3A_H
#define TERRAIN2DARCHITECTURE_T01B2C8A0361E9A7595B6F9267322714F6672DB3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2DArchitecture
struct  Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Kolibri2d.BezierSpline Kolibri2d.Terrain2DArchitecture::spline
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * ___spline_4;
	// Kolibri2d.Terrain2DArchitecture/Aspect Kolibri2d.Terrain2DArchitecture::aspect
	int32_t ___aspect_5;
	// System.Single Kolibri2d.Terrain2DArchitecture::exteriorMargin
	float ___exteriorMargin_6;
	// System.Boolean Kolibri2d.Terrain2DArchitecture::manualFlip
	bool ___manualFlip_7;
	// Kolibri2d.Terrain2DArchitectureAngles Kolibri2d.Terrain2DArchitecture::overrideAngle
	Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * ___overrideAngle_8;
	// Kolibri2d.Terrain2DArchitectureAngles Kolibri2d.Terrain2DArchitecture::currArchAngle
	Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * ___currArchAngle_9;
	// UnityEngine.Vector2[] Kolibri2d.Terrain2DArchitecture::stepPointNormals
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___stepPointNormals_10;
	// UnityEngine.Vector2[] Kolibri2d.Terrain2DArchitecture::stepPointNormalsSmooth
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___stepPointNormalsSmooth_11;
	// System.Collections.Generic.List`1<Kolibri2d.SplineSegmentInfo> Kolibri2d.Terrain2DArchitecture::segmentsInfo
	List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * ___segmentsInfo_12;

public:
	inline static int32_t get_offset_of_spline_4() { return static_cast<int32_t>(offsetof(Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A, ___spline_4)); }
	inline BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * get_spline_4() const { return ___spline_4; }
	inline BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE ** get_address_of_spline_4() { return &___spline_4; }
	inline void set_spline_4(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * value)
	{
		___spline_4 = value;
		Il2CppCodeGenWriteBarrier((&___spline_4), value);
	}

	inline static int32_t get_offset_of_aspect_5() { return static_cast<int32_t>(offsetof(Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A, ___aspect_5)); }
	inline int32_t get_aspect_5() const { return ___aspect_5; }
	inline int32_t* get_address_of_aspect_5() { return &___aspect_5; }
	inline void set_aspect_5(int32_t value)
	{
		___aspect_5 = value;
	}

	inline static int32_t get_offset_of_exteriorMargin_6() { return static_cast<int32_t>(offsetof(Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A, ___exteriorMargin_6)); }
	inline float get_exteriorMargin_6() const { return ___exteriorMargin_6; }
	inline float* get_address_of_exteriorMargin_6() { return &___exteriorMargin_6; }
	inline void set_exteriorMargin_6(float value)
	{
		___exteriorMargin_6 = value;
	}

	inline static int32_t get_offset_of_manualFlip_7() { return static_cast<int32_t>(offsetof(Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A, ___manualFlip_7)); }
	inline bool get_manualFlip_7() const { return ___manualFlip_7; }
	inline bool* get_address_of_manualFlip_7() { return &___manualFlip_7; }
	inline void set_manualFlip_7(bool value)
	{
		___manualFlip_7 = value;
	}

	inline static int32_t get_offset_of_overrideAngle_8() { return static_cast<int32_t>(offsetof(Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A, ___overrideAngle_8)); }
	inline Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * get_overrideAngle_8() const { return ___overrideAngle_8; }
	inline Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 ** get_address_of_overrideAngle_8() { return &___overrideAngle_8; }
	inline void set_overrideAngle_8(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * value)
	{
		___overrideAngle_8 = value;
		Il2CppCodeGenWriteBarrier((&___overrideAngle_8), value);
	}

	inline static int32_t get_offset_of_currArchAngle_9() { return static_cast<int32_t>(offsetof(Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A, ___currArchAngle_9)); }
	inline Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * get_currArchAngle_9() const { return ___currArchAngle_9; }
	inline Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 ** get_address_of_currArchAngle_9() { return &___currArchAngle_9; }
	inline void set_currArchAngle_9(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208 * value)
	{
		___currArchAngle_9 = value;
		Il2CppCodeGenWriteBarrier((&___currArchAngle_9), value);
	}

	inline static int32_t get_offset_of_stepPointNormals_10() { return static_cast<int32_t>(offsetof(Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A, ___stepPointNormals_10)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_stepPointNormals_10() const { return ___stepPointNormals_10; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_stepPointNormals_10() { return &___stepPointNormals_10; }
	inline void set_stepPointNormals_10(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___stepPointNormals_10 = value;
		Il2CppCodeGenWriteBarrier((&___stepPointNormals_10), value);
	}

	inline static int32_t get_offset_of_stepPointNormalsSmooth_11() { return static_cast<int32_t>(offsetof(Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A, ___stepPointNormalsSmooth_11)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_stepPointNormalsSmooth_11() const { return ___stepPointNormalsSmooth_11; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_stepPointNormalsSmooth_11() { return &___stepPointNormalsSmooth_11; }
	inline void set_stepPointNormalsSmooth_11(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___stepPointNormalsSmooth_11 = value;
		Il2CppCodeGenWriteBarrier((&___stepPointNormalsSmooth_11), value);
	}

	inline static int32_t get_offset_of_segmentsInfo_12() { return static_cast<int32_t>(offsetof(Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A, ___segmentsInfo_12)); }
	inline List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * get_segmentsInfo_12() const { return ___segmentsInfo_12; }
	inline List_1_t517344AED92C69B6645F0ABFA470063D220BACCD ** get_address_of_segmentsInfo_12() { return &___segmentsInfo_12; }
	inline void set_segmentsInfo_12(List_1_t517344AED92C69B6645F0ABFA470063D220BACCD * value)
	{
		___segmentsInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___segmentsInfo_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERRAIN2DARCHITECTURE_T01B2C8A0361E9A7595B6F9267322714F6672DB3A_H
#ifndef UTILS_T6B1F49395E2728FC7087CA82F420C8183CECBD71_H
#define UTILS_T6B1F49395E2728FC7087CA82F420C8183CECBD71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Utils
struct  Utils_t6B1F49395E2728FC7087CA82F420C8183CECBD71  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILS_T6B1F49395E2728FC7087CA82F420C8183CECBD71_H
#ifndef WATERTESTPANEL_TE3F44AF75B167F1003B7110B16C9ACE0D09EDC80_H
#define WATERTESTPANEL_TE3F44AF75B167F1003B7110B16C9ACE0D09EDC80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.WaterTestPanel
struct  WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Kolibri2d.SplineWater Kolibri2d.WaterTestPanel::water
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8 * ___water_4;
	// System.Single Kolibri2d.WaterTestPanel::lastForceRaw
	float ___lastForceRaw_5;
	// System.Single Kolibri2d.WaterTestPanel::lastForceAfterMultiplier
	float ___lastForceAfterMultiplier_6;
	// System.Boolean Kolibri2d.WaterTestPanel::lastForceIgnored
	bool ___lastForceIgnored_7;
	// System.Boolean Kolibri2d.WaterTestPanel::lastForceClamped
	bool ___lastForceClamped_8;
	// System.Single Kolibri2d.WaterTestPanel::lastForceAfterClamp
	float ___lastForceAfterClamp_9;

public:
	inline static int32_t get_offset_of_water_4() { return static_cast<int32_t>(offsetof(WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80, ___water_4)); }
	inline SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8 * get_water_4() const { return ___water_4; }
	inline SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8 ** get_address_of_water_4() { return &___water_4; }
	inline void set_water_4(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8 * value)
	{
		___water_4 = value;
		Il2CppCodeGenWriteBarrier((&___water_4), value);
	}

	inline static int32_t get_offset_of_lastForceRaw_5() { return static_cast<int32_t>(offsetof(WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80, ___lastForceRaw_5)); }
	inline float get_lastForceRaw_5() const { return ___lastForceRaw_5; }
	inline float* get_address_of_lastForceRaw_5() { return &___lastForceRaw_5; }
	inline void set_lastForceRaw_5(float value)
	{
		___lastForceRaw_5 = value;
	}

	inline static int32_t get_offset_of_lastForceAfterMultiplier_6() { return static_cast<int32_t>(offsetof(WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80, ___lastForceAfterMultiplier_6)); }
	inline float get_lastForceAfterMultiplier_6() const { return ___lastForceAfterMultiplier_6; }
	inline float* get_address_of_lastForceAfterMultiplier_6() { return &___lastForceAfterMultiplier_6; }
	inline void set_lastForceAfterMultiplier_6(float value)
	{
		___lastForceAfterMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_lastForceIgnored_7() { return static_cast<int32_t>(offsetof(WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80, ___lastForceIgnored_7)); }
	inline bool get_lastForceIgnored_7() const { return ___lastForceIgnored_7; }
	inline bool* get_address_of_lastForceIgnored_7() { return &___lastForceIgnored_7; }
	inline void set_lastForceIgnored_7(bool value)
	{
		___lastForceIgnored_7 = value;
	}

	inline static int32_t get_offset_of_lastForceClamped_8() { return static_cast<int32_t>(offsetof(WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80, ___lastForceClamped_8)); }
	inline bool get_lastForceClamped_8() const { return ___lastForceClamped_8; }
	inline bool* get_address_of_lastForceClamped_8() { return &___lastForceClamped_8; }
	inline void set_lastForceClamped_8(bool value)
	{
		___lastForceClamped_8 = value;
	}

	inline static int32_t get_offset_of_lastForceAfterClamp_9() { return static_cast<int32_t>(offsetof(WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80, ___lastForceAfterClamp_9)); }
	inline float get_lastForceAfterClamp_9() const { return ___lastForceAfterClamp_9; }
	inline float* get_address_of_lastForceAfterClamp_9() { return &___lastForceAfterClamp_9; }
	inline void set_lastForceAfterClamp_9(float value)
	{
		___lastForceAfterClamp_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERTESTPANEL_TE3F44AF75B167F1003B7110B16C9ACE0D09EDC80_H
#ifndef QUICKDECORATOR_T070F88253DAE60952901A2E8AF53B26EE77163F7_H
#define QUICKDECORATOR_T070F88253DAE60952901A2E8AF53B26EE77163F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.QuickDecorator
struct  QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7  : public SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232
{
public:
	// System.Single Kolibri2d.QuickDecorator::scale
	float ___scale_8;
	// Kolibri2d.DecorationGroup/SortingLayerOptions Kolibri2d.QuickDecorator::sortingLayerOptions
	SortingLayerOptions_t241DB2E522009CEB9D89742D1213F4B22EE99E9E * ___sortingLayerOptions_9;
	// UnityEngine.Material Kolibri2d.QuickDecorator::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_10;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Kolibri2d.QuickDecorator::objects
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___objects_11;
	// System.Collections.Generic.List`1<Kolibri2d.Decoration> Kolibri2d.QuickDecorator::decorations
	List_1_tA358CC6533316031BC20A834B3C5627D3B034883 * ___decorations_12;
	// System.Single Kolibri2d.QuickDecorator::scaleMin
	float ___scaleMin_13;
	// System.Single Kolibri2d.QuickDecorator::scaleMax
	float ___scaleMax_14;
	// System.Single Kolibri2d.QuickDecorator::offsetMin
	float ___offsetMin_15;
	// System.Single Kolibri2d.QuickDecorator::offsetMax
	float ___offsetMax_16;
	// Kolibri2d.DecorationGroup/DistributionType Kolibri2d.QuickDecorator::distribution
	int32_t ___distribution_17;
	// Kolibri2d.DecorationGroup/OrderType Kolibri2d.QuickDecorator::order
	int32_t ___order_18;
	// System.Single Kolibri2d.QuickDecorator::margin
	float ___margin_19;
	// System.Single Kolibri2d.QuickDecorator::density
	float ___density_20;
	// System.Int32 Kolibri2d.QuickDecorator::distributedAmount
	int32_t ___distributedAmount_21;
	// System.Boolean Kolibri2d.QuickDecorator::distributionPerSegment
	bool ___distributionPerSegment_22;
	// Kolibri2d.DecorationGroup/RenderOrderType Kolibri2d.QuickDecorator::renderOrderType
	int32_t ___renderOrderType_23;
	// System.Single Kolibri2d.QuickDecorator::renderZDepthDelta
	float ___renderZDepthDelta_24;
	// System.Single Kolibri2d.QuickDecorator::renderZDepthMin
	float ___renderZDepthMin_25;
	// System.Single Kolibri2d.QuickDecorator::renderZDepthMax
	float ___renderZDepthMax_26;
	// System.Boolean Kolibri2d.QuickDecorator::setGameObjectLayer
	bool ___setGameObjectLayer_27;
	// UnityEngine.Color Kolibri2d.QuickDecorator::tint
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___tint_28;
	// System.Boolean Kolibri2d.QuickDecorator::useTint
	bool ___useTint_29;
	// UnityEngine.Object[] Kolibri2d.QuickDecorator::draggedObjectsFromInspector
	ObjectU5BU5D_t0989B392BF4AF7D427A68A3A60BD412E54A9573C* ___draggedObjectsFromInspector_30;
	// Kolibri2d.DecorationGroupRandomizer Kolibri2d.QuickDecorator::groupRandomizer
	DecorationGroupRandomizer_t723A277DE2132DC6F09140A53D0393805D412605 * ___groupRandomizer_32;
	// Kolibri2d.SeedRandomizer Kolibri2d.QuickDecorator::randomizer
	SeedRandomizer_tBE46395F312BD0ECAB808CE4572B917DA733887E  ___randomizer_33;
	// System.Int32 Kolibri2d.QuickDecorator::orderedIndex
	int32_t ___orderedIndex_34;
	// System.Collections.Generic.List`1<UnityEngine.Material> Kolibri2d.QuickDecorator::createdMaterials
	List_1_t56B616621385F653B408094FBDFA5DEC6C4B47EA * ___createdMaterials_35;
	// System.Collections.Generic.List`1<System.Int32> Kolibri2d.QuickDecorator::indexesDepthZ
	List_1_t4A961510635950236678F1B3B2436ECCAF28713A * ___indexesDepthZ_36;
	// System.Int32 Kolibri2d.QuickDecorator::maxCreatedObjects
	int32_t ___maxCreatedObjects_37;
	// System.Boolean Kolibri2d.QuickDecorator::facingTowardsSpline
	bool ___facingTowardsSpline_38;

public:
	inline static int32_t get_offset_of_scale_8() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___scale_8)); }
	inline float get_scale_8() const { return ___scale_8; }
	inline float* get_address_of_scale_8() { return &___scale_8; }
	inline void set_scale_8(float value)
	{
		___scale_8 = value;
	}

	inline static int32_t get_offset_of_sortingLayerOptions_9() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___sortingLayerOptions_9)); }
	inline SortingLayerOptions_t241DB2E522009CEB9D89742D1213F4B22EE99E9E * get_sortingLayerOptions_9() const { return ___sortingLayerOptions_9; }
	inline SortingLayerOptions_t241DB2E522009CEB9D89742D1213F4B22EE99E9E ** get_address_of_sortingLayerOptions_9() { return &___sortingLayerOptions_9; }
	inline void set_sortingLayerOptions_9(SortingLayerOptions_t241DB2E522009CEB9D89742D1213F4B22EE99E9E * value)
	{
		___sortingLayerOptions_9 = value;
		Il2CppCodeGenWriteBarrier((&___sortingLayerOptions_9), value);
	}

	inline static int32_t get_offset_of_material_10() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___material_10)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_10() const { return ___material_10; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_10() { return &___material_10; }
	inline void set_material_10(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_10 = value;
		Il2CppCodeGenWriteBarrier((&___material_10), value);
	}

	inline static int32_t get_offset_of_objects_11() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___objects_11)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_objects_11() const { return ___objects_11; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_objects_11() { return &___objects_11; }
	inline void set_objects_11(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___objects_11 = value;
		Il2CppCodeGenWriteBarrier((&___objects_11), value);
	}

	inline static int32_t get_offset_of_decorations_12() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___decorations_12)); }
	inline List_1_tA358CC6533316031BC20A834B3C5627D3B034883 * get_decorations_12() const { return ___decorations_12; }
	inline List_1_tA358CC6533316031BC20A834B3C5627D3B034883 ** get_address_of_decorations_12() { return &___decorations_12; }
	inline void set_decorations_12(List_1_tA358CC6533316031BC20A834B3C5627D3B034883 * value)
	{
		___decorations_12 = value;
		Il2CppCodeGenWriteBarrier((&___decorations_12), value);
	}

	inline static int32_t get_offset_of_scaleMin_13() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___scaleMin_13)); }
	inline float get_scaleMin_13() const { return ___scaleMin_13; }
	inline float* get_address_of_scaleMin_13() { return &___scaleMin_13; }
	inline void set_scaleMin_13(float value)
	{
		___scaleMin_13 = value;
	}

	inline static int32_t get_offset_of_scaleMax_14() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___scaleMax_14)); }
	inline float get_scaleMax_14() const { return ___scaleMax_14; }
	inline float* get_address_of_scaleMax_14() { return &___scaleMax_14; }
	inline void set_scaleMax_14(float value)
	{
		___scaleMax_14 = value;
	}

	inline static int32_t get_offset_of_offsetMin_15() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___offsetMin_15)); }
	inline float get_offsetMin_15() const { return ___offsetMin_15; }
	inline float* get_address_of_offsetMin_15() { return &___offsetMin_15; }
	inline void set_offsetMin_15(float value)
	{
		___offsetMin_15 = value;
	}

	inline static int32_t get_offset_of_offsetMax_16() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___offsetMax_16)); }
	inline float get_offsetMax_16() const { return ___offsetMax_16; }
	inline float* get_address_of_offsetMax_16() { return &___offsetMax_16; }
	inline void set_offsetMax_16(float value)
	{
		___offsetMax_16 = value;
	}

	inline static int32_t get_offset_of_distribution_17() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___distribution_17)); }
	inline int32_t get_distribution_17() const { return ___distribution_17; }
	inline int32_t* get_address_of_distribution_17() { return &___distribution_17; }
	inline void set_distribution_17(int32_t value)
	{
		___distribution_17 = value;
	}

	inline static int32_t get_offset_of_order_18() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___order_18)); }
	inline int32_t get_order_18() const { return ___order_18; }
	inline int32_t* get_address_of_order_18() { return &___order_18; }
	inline void set_order_18(int32_t value)
	{
		___order_18 = value;
	}

	inline static int32_t get_offset_of_margin_19() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___margin_19)); }
	inline float get_margin_19() const { return ___margin_19; }
	inline float* get_address_of_margin_19() { return &___margin_19; }
	inline void set_margin_19(float value)
	{
		___margin_19 = value;
	}

	inline static int32_t get_offset_of_density_20() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___density_20)); }
	inline float get_density_20() const { return ___density_20; }
	inline float* get_address_of_density_20() { return &___density_20; }
	inline void set_density_20(float value)
	{
		___density_20 = value;
	}

	inline static int32_t get_offset_of_distributedAmount_21() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___distributedAmount_21)); }
	inline int32_t get_distributedAmount_21() const { return ___distributedAmount_21; }
	inline int32_t* get_address_of_distributedAmount_21() { return &___distributedAmount_21; }
	inline void set_distributedAmount_21(int32_t value)
	{
		___distributedAmount_21 = value;
	}

	inline static int32_t get_offset_of_distributionPerSegment_22() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___distributionPerSegment_22)); }
	inline bool get_distributionPerSegment_22() const { return ___distributionPerSegment_22; }
	inline bool* get_address_of_distributionPerSegment_22() { return &___distributionPerSegment_22; }
	inline void set_distributionPerSegment_22(bool value)
	{
		___distributionPerSegment_22 = value;
	}

	inline static int32_t get_offset_of_renderOrderType_23() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___renderOrderType_23)); }
	inline int32_t get_renderOrderType_23() const { return ___renderOrderType_23; }
	inline int32_t* get_address_of_renderOrderType_23() { return &___renderOrderType_23; }
	inline void set_renderOrderType_23(int32_t value)
	{
		___renderOrderType_23 = value;
	}

	inline static int32_t get_offset_of_renderZDepthDelta_24() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___renderZDepthDelta_24)); }
	inline float get_renderZDepthDelta_24() const { return ___renderZDepthDelta_24; }
	inline float* get_address_of_renderZDepthDelta_24() { return &___renderZDepthDelta_24; }
	inline void set_renderZDepthDelta_24(float value)
	{
		___renderZDepthDelta_24 = value;
	}

	inline static int32_t get_offset_of_renderZDepthMin_25() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___renderZDepthMin_25)); }
	inline float get_renderZDepthMin_25() const { return ___renderZDepthMin_25; }
	inline float* get_address_of_renderZDepthMin_25() { return &___renderZDepthMin_25; }
	inline void set_renderZDepthMin_25(float value)
	{
		___renderZDepthMin_25 = value;
	}

	inline static int32_t get_offset_of_renderZDepthMax_26() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___renderZDepthMax_26)); }
	inline float get_renderZDepthMax_26() const { return ___renderZDepthMax_26; }
	inline float* get_address_of_renderZDepthMax_26() { return &___renderZDepthMax_26; }
	inline void set_renderZDepthMax_26(float value)
	{
		___renderZDepthMax_26 = value;
	}

	inline static int32_t get_offset_of_setGameObjectLayer_27() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___setGameObjectLayer_27)); }
	inline bool get_setGameObjectLayer_27() const { return ___setGameObjectLayer_27; }
	inline bool* get_address_of_setGameObjectLayer_27() { return &___setGameObjectLayer_27; }
	inline void set_setGameObjectLayer_27(bool value)
	{
		___setGameObjectLayer_27 = value;
	}

	inline static int32_t get_offset_of_tint_28() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___tint_28)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_tint_28() const { return ___tint_28; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_tint_28() { return &___tint_28; }
	inline void set_tint_28(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___tint_28 = value;
	}

	inline static int32_t get_offset_of_useTint_29() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___useTint_29)); }
	inline bool get_useTint_29() const { return ___useTint_29; }
	inline bool* get_address_of_useTint_29() { return &___useTint_29; }
	inline void set_useTint_29(bool value)
	{
		___useTint_29 = value;
	}

	inline static int32_t get_offset_of_draggedObjectsFromInspector_30() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___draggedObjectsFromInspector_30)); }
	inline ObjectU5BU5D_t0989B392BF4AF7D427A68A3A60BD412E54A9573C* get_draggedObjectsFromInspector_30() const { return ___draggedObjectsFromInspector_30; }
	inline ObjectU5BU5D_t0989B392BF4AF7D427A68A3A60BD412E54A9573C** get_address_of_draggedObjectsFromInspector_30() { return &___draggedObjectsFromInspector_30; }
	inline void set_draggedObjectsFromInspector_30(ObjectU5BU5D_t0989B392BF4AF7D427A68A3A60BD412E54A9573C* value)
	{
		___draggedObjectsFromInspector_30 = value;
		Il2CppCodeGenWriteBarrier((&___draggedObjectsFromInspector_30), value);
	}

	inline static int32_t get_offset_of_groupRandomizer_32() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___groupRandomizer_32)); }
	inline DecorationGroupRandomizer_t723A277DE2132DC6F09140A53D0393805D412605 * get_groupRandomizer_32() const { return ___groupRandomizer_32; }
	inline DecorationGroupRandomizer_t723A277DE2132DC6F09140A53D0393805D412605 ** get_address_of_groupRandomizer_32() { return &___groupRandomizer_32; }
	inline void set_groupRandomizer_32(DecorationGroupRandomizer_t723A277DE2132DC6F09140A53D0393805D412605 * value)
	{
		___groupRandomizer_32 = value;
		Il2CppCodeGenWriteBarrier((&___groupRandomizer_32), value);
	}

	inline static int32_t get_offset_of_randomizer_33() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___randomizer_33)); }
	inline SeedRandomizer_tBE46395F312BD0ECAB808CE4572B917DA733887E  get_randomizer_33() const { return ___randomizer_33; }
	inline SeedRandomizer_tBE46395F312BD0ECAB808CE4572B917DA733887E * get_address_of_randomizer_33() { return &___randomizer_33; }
	inline void set_randomizer_33(SeedRandomizer_tBE46395F312BD0ECAB808CE4572B917DA733887E  value)
	{
		___randomizer_33 = value;
	}

	inline static int32_t get_offset_of_orderedIndex_34() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___orderedIndex_34)); }
	inline int32_t get_orderedIndex_34() const { return ___orderedIndex_34; }
	inline int32_t* get_address_of_orderedIndex_34() { return &___orderedIndex_34; }
	inline void set_orderedIndex_34(int32_t value)
	{
		___orderedIndex_34 = value;
	}

	inline static int32_t get_offset_of_createdMaterials_35() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___createdMaterials_35)); }
	inline List_1_t56B616621385F653B408094FBDFA5DEC6C4B47EA * get_createdMaterials_35() const { return ___createdMaterials_35; }
	inline List_1_t56B616621385F653B408094FBDFA5DEC6C4B47EA ** get_address_of_createdMaterials_35() { return &___createdMaterials_35; }
	inline void set_createdMaterials_35(List_1_t56B616621385F653B408094FBDFA5DEC6C4B47EA * value)
	{
		___createdMaterials_35 = value;
		Il2CppCodeGenWriteBarrier((&___createdMaterials_35), value);
	}

	inline static int32_t get_offset_of_indexesDepthZ_36() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___indexesDepthZ_36)); }
	inline List_1_t4A961510635950236678F1B3B2436ECCAF28713A * get_indexesDepthZ_36() const { return ___indexesDepthZ_36; }
	inline List_1_t4A961510635950236678F1B3B2436ECCAF28713A ** get_address_of_indexesDepthZ_36() { return &___indexesDepthZ_36; }
	inline void set_indexesDepthZ_36(List_1_t4A961510635950236678F1B3B2436ECCAF28713A * value)
	{
		___indexesDepthZ_36 = value;
		Il2CppCodeGenWriteBarrier((&___indexesDepthZ_36), value);
	}

	inline static int32_t get_offset_of_maxCreatedObjects_37() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___maxCreatedObjects_37)); }
	inline int32_t get_maxCreatedObjects_37() const { return ___maxCreatedObjects_37; }
	inline int32_t* get_address_of_maxCreatedObjects_37() { return &___maxCreatedObjects_37; }
	inline void set_maxCreatedObjects_37(int32_t value)
	{
		___maxCreatedObjects_37 = value;
	}

	inline static int32_t get_offset_of_facingTowardsSpline_38() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7, ___facingTowardsSpline_38)); }
	inline bool get_facingTowardsSpline_38() const { return ___facingTowardsSpline_38; }
	inline bool* get_address_of_facingTowardsSpline_38() { return &___facingTowardsSpline_38; }
	inline void set_facingTowardsSpline_38(bool value)
	{
		___facingTowardsSpline_38 = value;
	}
};

struct QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7_StaticFields
{
public:
	// System.String Kolibri2d.QuickDecorator::decorationNodeName
	String_t* ___decorationNodeName_31;

public:
	inline static int32_t get_offset_of_decorationNodeName_31() { return static_cast<int32_t>(offsetof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7_StaticFields, ___decorationNodeName_31)); }
	inline String_t* get_decorationNodeName_31() const { return ___decorationNodeName_31; }
	inline String_t** get_address_of_decorationNodeName_31() { return &___decorationNodeName_31; }
	inline void set_decorationNodeName_31(String_t* value)
	{
		___decorationNodeName_31 = value;
		Il2CppCodeGenWriteBarrier((&___decorationNodeName_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUICKDECORATOR_T070F88253DAE60952901A2E8AF53B26EE77163F7_H
#ifndef SPLINEDECORATOR_T742C8164AFC76021C654CF40DED2D581C653453C_H
#define SPLINEDECORATOR_T742C8164AFC76021C654CF40DED2D581C653453C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineDecorator
struct  SplineDecorator_t742C8164AFC76021C654CF40DED2D581C653453C  : public SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232
{
public:
	// Kolibri2d.DecorationMaterial Kolibri2d.SplineDecorator::decoMat
	DecorationMaterial_t0D0DD06B7E2AC4FF1E10BA9FAEAB4FEBDD125A14 * ___decoMat_9;
	// System.Boolean Kolibri2d.SplineDecorator::useTint
	bool ___useTint_10;
	// UnityEngine.Color Kolibri2d.SplineDecorator::tint
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___tint_11;
	// System.Int32 Kolibri2d.SplineDecorator::additiveSortInLayer
	int32_t ___additiveSortInLayer_12;
	// System.Collections.Generic.Dictionary`2<Kolibri2d.DecorationGroup,Kolibri2d.DecorationGroupRandomizer> Kolibri2d.SplineDecorator::decorationGroupRandomizers
	Dictionary_2_tEC082D6EA1509396C080AABEBFC6445FF7640C3C * ___decorationGroupRandomizers_13;
	// UnityEngine.Material Kolibri2d.SplineDecorator::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_14;
	// System.Collections.Generic.List`1<UnityEngine.Material> Kolibri2d.SplineDecorator::createdMaterials
	List_1_t56B616621385F653B408094FBDFA5DEC6C4B47EA * ___createdMaterials_15;
	// System.Collections.Generic.Dictionary`2<Kolibri2d.TerrainType,System.Int32> Kolibri2d.SplineDecorator::orderedIndices
	Dictionary_2_tB362795ED1A6C15ECE1381757FD2C6FA491211AB * ___orderedIndices_16;
	// System.Collections.Generic.List`1<System.Int32> Kolibri2d.SplineDecorator::indexesDepthZ
	List_1_t4A961510635950236678F1B3B2436ECCAF28713A * ___indexesDepthZ_17;

public:
	inline static int32_t get_offset_of_decoMat_9() { return static_cast<int32_t>(offsetof(SplineDecorator_t742C8164AFC76021C654CF40DED2D581C653453C, ___decoMat_9)); }
	inline DecorationMaterial_t0D0DD06B7E2AC4FF1E10BA9FAEAB4FEBDD125A14 * get_decoMat_9() const { return ___decoMat_9; }
	inline DecorationMaterial_t0D0DD06B7E2AC4FF1E10BA9FAEAB4FEBDD125A14 ** get_address_of_decoMat_9() { return &___decoMat_9; }
	inline void set_decoMat_9(DecorationMaterial_t0D0DD06B7E2AC4FF1E10BA9FAEAB4FEBDD125A14 * value)
	{
		___decoMat_9 = value;
		Il2CppCodeGenWriteBarrier((&___decoMat_9), value);
	}

	inline static int32_t get_offset_of_useTint_10() { return static_cast<int32_t>(offsetof(SplineDecorator_t742C8164AFC76021C654CF40DED2D581C653453C, ___useTint_10)); }
	inline bool get_useTint_10() const { return ___useTint_10; }
	inline bool* get_address_of_useTint_10() { return &___useTint_10; }
	inline void set_useTint_10(bool value)
	{
		___useTint_10 = value;
	}

	inline static int32_t get_offset_of_tint_11() { return static_cast<int32_t>(offsetof(SplineDecorator_t742C8164AFC76021C654CF40DED2D581C653453C, ___tint_11)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_tint_11() const { return ___tint_11; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_tint_11() { return &___tint_11; }
	inline void set_tint_11(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___tint_11 = value;
	}

	inline static int32_t get_offset_of_additiveSortInLayer_12() { return static_cast<int32_t>(offsetof(SplineDecorator_t742C8164AFC76021C654CF40DED2D581C653453C, ___additiveSortInLayer_12)); }
	inline int32_t get_additiveSortInLayer_12() const { return ___additiveSortInLayer_12; }
	inline int32_t* get_address_of_additiveSortInLayer_12() { return &___additiveSortInLayer_12; }
	inline void set_additiveSortInLayer_12(int32_t value)
	{
		___additiveSortInLayer_12 = value;
	}

	inline static int32_t get_offset_of_decorationGroupRandomizers_13() { return static_cast<int32_t>(offsetof(SplineDecorator_t742C8164AFC76021C654CF40DED2D581C653453C, ___decorationGroupRandomizers_13)); }
	inline Dictionary_2_tEC082D6EA1509396C080AABEBFC6445FF7640C3C * get_decorationGroupRandomizers_13() const { return ___decorationGroupRandomizers_13; }
	inline Dictionary_2_tEC082D6EA1509396C080AABEBFC6445FF7640C3C ** get_address_of_decorationGroupRandomizers_13() { return &___decorationGroupRandomizers_13; }
	inline void set_decorationGroupRandomizers_13(Dictionary_2_tEC082D6EA1509396C080AABEBFC6445FF7640C3C * value)
	{
		___decorationGroupRandomizers_13 = value;
		Il2CppCodeGenWriteBarrier((&___decorationGroupRandomizers_13), value);
	}

	inline static int32_t get_offset_of_material_14() { return static_cast<int32_t>(offsetof(SplineDecorator_t742C8164AFC76021C654CF40DED2D581C653453C, ___material_14)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_14() const { return ___material_14; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_14() { return &___material_14; }
	inline void set_material_14(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_14 = value;
		Il2CppCodeGenWriteBarrier((&___material_14), value);
	}

	inline static int32_t get_offset_of_createdMaterials_15() { return static_cast<int32_t>(offsetof(SplineDecorator_t742C8164AFC76021C654CF40DED2D581C653453C, ___createdMaterials_15)); }
	inline List_1_t56B616621385F653B408094FBDFA5DEC6C4B47EA * get_createdMaterials_15() const { return ___createdMaterials_15; }
	inline List_1_t56B616621385F653B408094FBDFA5DEC6C4B47EA ** get_address_of_createdMaterials_15() { return &___createdMaterials_15; }
	inline void set_createdMaterials_15(List_1_t56B616621385F653B408094FBDFA5DEC6C4B47EA * value)
	{
		___createdMaterials_15 = value;
		Il2CppCodeGenWriteBarrier((&___createdMaterials_15), value);
	}

	inline static int32_t get_offset_of_orderedIndices_16() { return static_cast<int32_t>(offsetof(SplineDecorator_t742C8164AFC76021C654CF40DED2D581C653453C, ___orderedIndices_16)); }
	inline Dictionary_2_tB362795ED1A6C15ECE1381757FD2C6FA491211AB * get_orderedIndices_16() const { return ___orderedIndices_16; }
	inline Dictionary_2_tB362795ED1A6C15ECE1381757FD2C6FA491211AB ** get_address_of_orderedIndices_16() { return &___orderedIndices_16; }
	inline void set_orderedIndices_16(Dictionary_2_tB362795ED1A6C15ECE1381757FD2C6FA491211AB * value)
	{
		___orderedIndices_16 = value;
		Il2CppCodeGenWriteBarrier((&___orderedIndices_16), value);
	}

	inline static int32_t get_offset_of_indexesDepthZ_17() { return static_cast<int32_t>(offsetof(SplineDecorator_t742C8164AFC76021C654CF40DED2D581C653453C, ___indexesDepthZ_17)); }
	inline List_1_t4A961510635950236678F1B3B2436ECCAF28713A * get_indexesDepthZ_17() const { return ___indexesDepthZ_17; }
	inline List_1_t4A961510635950236678F1B3B2436ECCAF28713A ** get_address_of_indexesDepthZ_17() { return &___indexesDepthZ_17; }
	inline void set_indexesDepthZ_17(List_1_t4A961510635950236678F1B3B2436ECCAF28713A * value)
	{
		___indexesDepthZ_17 = value;
		Il2CppCodeGenWriteBarrier((&___indexesDepthZ_17), value);
	}
};

struct SplineDecorator_t742C8164AFC76021C654CF40DED2D581C653453C_StaticFields
{
public:
	// System.String Kolibri2d.SplineDecorator::decorationNodeName
	String_t* ___decorationNodeName_8;

public:
	inline static int32_t get_offset_of_decorationNodeName_8() { return static_cast<int32_t>(offsetof(SplineDecorator_t742C8164AFC76021C654CF40DED2D581C653453C_StaticFields, ___decorationNodeName_8)); }
	inline String_t* get_decorationNodeName_8() const { return ___decorationNodeName_8; }
	inline String_t** get_address_of_decorationNodeName_8() { return &___decorationNodeName_8; }
	inline void set_decorationNodeName_8(String_t* value)
	{
		___decorationNodeName_8 = value;
		Il2CppCodeGenWriteBarrier((&___decorationNodeName_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINEDECORATOR_T742C8164AFC76021C654CF40DED2D581C653453C_H
#ifndef SPLINETEXTURE_T0E51D252D16938B2BEECA4ED3E11A58E8F463657_H
#define SPLINETEXTURE_T0E51D252D16938B2BEECA4ED3E11A58E8F463657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineTexture
struct  SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657  : public SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232
{
public:
	// System.Single Kolibri2d.SplineTexture::scale
	float ___scale_8;
	// Kolibri2d.MaterialTextureInfo Kolibri2d.SplineTexture::textureBase
	MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  ___textureBase_9;
	// Kolibri2d.MaterialAssetInfo Kolibri2d.SplineTexture::assetBegin
	MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF  ___assetBegin_10;
	// Kolibri2d.MaterialAssetInfo Kolibri2d.SplineTexture::assetEnd
	MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF  ___assetEnd_11;
	// UnityEngine.Color Kolibri2d.SplineTexture::tint
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___tint_12;
	// System.Boolean Kolibri2d.SplineTexture::useTint
	bool ___useTint_13;
	// UnityEngine.Material Kolibri2d.SplineTexture::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_14;
	// UnityEngine.Mesh Kolibri2d.SplineTexture::mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh_15;
	// System.Collections.Generic.List`1<UnityEngine.Material> Kolibri2d.SplineTexture::createdMaterials
	List_1_t56B616621385F653B408094FBDFA5DEC6C4B47EA * ___createdMaterials_16;
	// UnityEngine.GameObject Kolibri2d.SplineTexture::meshObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___meshObject_17;
	// UnityEngine.GameObject Kolibri2d.SplineTexture::createdObjectEnd
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___createdObjectEnd_18;
	// UnityEngine.GameObject Kolibri2d.SplineTexture::createdObjectBegin
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___createdObjectBegin_19;
	// System.Int32 Kolibri2d.SplineTexture::additionalOrderInLayer
	int32_t ___additionalOrderInLayer_20;
	// System.Int32 Kolibri2d.SplineTexture::sortingLayerIDBegin
	int32_t ___sortingLayerIDBegin_21;
	// System.Int32 Kolibri2d.SplineTexture::sortingLayerIDSpline
	int32_t ___sortingLayerIDSpline_22;
	// System.Int32 Kolibri2d.SplineTexture::sortingLayerIDEnd
	int32_t ___sortingLayerIDEnd_23;
	// System.Int32 Kolibri2d.SplineTexture::orderInLayerBegin
	int32_t ___orderInLayerBegin_24;
	// System.Int32 Kolibri2d.SplineTexture::orderInLayerSpline
	int32_t ___orderInLayerSpline_25;
	// System.Int32 Kolibri2d.SplineTexture::orderInLayerEnd
	int32_t ___orderInLayerEnd_26;
	// System.Boolean Kolibri2d.SplineTexture::useGameobjectLayer
	bool ___useGameobjectLayer_27;
	// Kolibri2d.SplineTexture/ControlMode Kolibri2d.SplineTexture::controlMode
	int32_t ___controlMode_28;
	// Kolibri2d.SplineTexture/OrientationMode Kolibri2d.SplineTexture::orientation
	int32_t ___orientation_29;

public:
	inline static int32_t get_offset_of_scale_8() { return static_cast<int32_t>(offsetof(SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657, ___scale_8)); }
	inline float get_scale_8() const { return ___scale_8; }
	inline float* get_address_of_scale_8() { return &___scale_8; }
	inline void set_scale_8(float value)
	{
		___scale_8 = value;
	}

	inline static int32_t get_offset_of_textureBase_9() { return static_cast<int32_t>(offsetof(SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657, ___textureBase_9)); }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  get_textureBase_9() const { return ___textureBase_9; }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F * get_address_of_textureBase_9() { return &___textureBase_9; }
	inline void set_textureBase_9(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  value)
	{
		___textureBase_9 = value;
	}

	inline static int32_t get_offset_of_assetBegin_10() { return static_cast<int32_t>(offsetof(SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657, ___assetBegin_10)); }
	inline MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF  get_assetBegin_10() const { return ___assetBegin_10; }
	inline MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF * get_address_of_assetBegin_10() { return &___assetBegin_10; }
	inline void set_assetBegin_10(MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF  value)
	{
		___assetBegin_10 = value;
	}

	inline static int32_t get_offset_of_assetEnd_11() { return static_cast<int32_t>(offsetof(SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657, ___assetEnd_11)); }
	inline MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF  get_assetEnd_11() const { return ___assetEnd_11; }
	inline MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF * get_address_of_assetEnd_11() { return &___assetEnd_11; }
	inline void set_assetEnd_11(MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF  value)
	{
		___assetEnd_11 = value;
	}

	inline static int32_t get_offset_of_tint_12() { return static_cast<int32_t>(offsetof(SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657, ___tint_12)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_tint_12() const { return ___tint_12; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_tint_12() { return &___tint_12; }
	inline void set_tint_12(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___tint_12 = value;
	}

	inline static int32_t get_offset_of_useTint_13() { return static_cast<int32_t>(offsetof(SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657, ___useTint_13)); }
	inline bool get_useTint_13() const { return ___useTint_13; }
	inline bool* get_address_of_useTint_13() { return &___useTint_13; }
	inline void set_useTint_13(bool value)
	{
		___useTint_13 = value;
	}

	inline static int32_t get_offset_of_material_14() { return static_cast<int32_t>(offsetof(SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657, ___material_14)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_14() const { return ___material_14; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_14() { return &___material_14; }
	inline void set_material_14(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_14 = value;
		Il2CppCodeGenWriteBarrier((&___material_14), value);
	}

	inline static int32_t get_offset_of_mesh_15() { return static_cast<int32_t>(offsetof(SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657, ___mesh_15)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_mesh_15() const { return ___mesh_15; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_mesh_15() { return &___mesh_15; }
	inline void set_mesh_15(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_15), value);
	}

	inline static int32_t get_offset_of_createdMaterials_16() { return static_cast<int32_t>(offsetof(SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657, ___createdMaterials_16)); }
	inline List_1_t56B616621385F653B408094FBDFA5DEC6C4B47EA * get_createdMaterials_16() const { return ___createdMaterials_16; }
	inline List_1_t56B616621385F653B408094FBDFA5DEC6C4B47EA ** get_address_of_createdMaterials_16() { return &___createdMaterials_16; }
	inline void set_createdMaterials_16(List_1_t56B616621385F653B408094FBDFA5DEC6C4B47EA * value)
	{
		___createdMaterials_16 = value;
		Il2CppCodeGenWriteBarrier((&___createdMaterials_16), value);
	}

	inline static int32_t get_offset_of_meshObject_17() { return static_cast<int32_t>(offsetof(SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657, ___meshObject_17)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_meshObject_17() const { return ___meshObject_17; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_meshObject_17() { return &___meshObject_17; }
	inline void set_meshObject_17(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___meshObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___meshObject_17), value);
	}

	inline static int32_t get_offset_of_createdObjectEnd_18() { return static_cast<int32_t>(offsetof(SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657, ___createdObjectEnd_18)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_createdObjectEnd_18() const { return ___createdObjectEnd_18; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_createdObjectEnd_18() { return &___createdObjectEnd_18; }
	inline void set_createdObjectEnd_18(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___createdObjectEnd_18 = value;
		Il2CppCodeGenWriteBarrier((&___createdObjectEnd_18), value);
	}

	inline static int32_t get_offset_of_createdObjectBegin_19() { return static_cast<int32_t>(offsetof(SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657, ___createdObjectBegin_19)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_createdObjectBegin_19() const { return ___createdObjectBegin_19; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_createdObjectBegin_19() { return &___createdObjectBegin_19; }
	inline void set_createdObjectBegin_19(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___createdObjectBegin_19 = value;
		Il2CppCodeGenWriteBarrier((&___createdObjectBegin_19), value);
	}

	inline static int32_t get_offset_of_additionalOrderInLayer_20() { return static_cast<int32_t>(offsetof(SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657, ___additionalOrderInLayer_20)); }
	inline int32_t get_additionalOrderInLayer_20() const { return ___additionalOrderInLayer_20; }
	inline int32_t* get_address_of_additionalOrderInLayer_20() { return &___additionalOrderInLayer_20; }
	inline void set_additionalOrderInLayer_20(int32_t value)
	{
		___additionalOrderInLayer_20 = value;
	}

	inline static int32_t get_offset_of_sortingLayerIDBegin_21() { return static_cast<int32_t>(offsetof(SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657, ___sortingLayerIDBegin_21)); }
	inline int32_t get_sortingLayerIDBegin_21() const { return ___sortingLayerIDBegin_21; }
	inline int32_t* get_address_of_sortingLayerIDBegin_21() { return &___sortingLayerIDBegin_21; }
	inline void set_sortingLayerIDBegin_21(int32_t value)
	{
		___sortingLayerIDBegin_21 = value;
	}

	inline static int32_t get_offset_of_sortingLayerIDSpline_22() { return static_cast<int32_t>(offsetof(SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657, ___sortingLayerIDSpline_22)); }
	inline int32_t get_sortingLayerIDSpline_22() const { return ___sortingLayerIDSpline_22; }
	inline int32_t* get_address_of_sortingLayerIDSpline_22() { return &___sortingLayerIDSpline_22; }
	inline void set_sortingLayerIDSpline_22(int32_t value)
	{
		___sortingLayerIDSpline_22 = value;
	}

	inline static int32_t get_offset_of_sortingLayerIDEnd_23() { return static_cast<int32_t>(offsetof(SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657, ___sortingLayerIDEnd_23)); }
	inline int32_t get_sortingLayerIDEnd_23() const { return ___sortingLayerIDEnd_23; }
	inline int32_t* get_address_of_sortingLayerIDEnd_23() { return &___sortingLayerIDEnd_23; }
	inline void set_sortingLayerIDEnd_23(int32_t value)
	{
		___sortingLayerIDEnd_23 = value;
	}

	inline static int32_t get_offset_of_orderInLayerBegin_24() { return static_cast<int32_t>(offsetof(SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657, ___orderInLayerBegin_24)); }
	inline int32_t get_orderInLayerBegin_24() const { return ___orderInLayerBegin_24; }
	inline int32_t* get_address_of_orderInLayerBegin_24() { return &___orderInLayerBegin_24; }
	inline void set_orderInLayerBegin_24(int32_t value)
	{
		___orderInLayerBegin_24 = value;
	}

	inline static int32_t get_offset_of_orderInLayerSpline_25() { return static_cast<int32_t>(offsetof(SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657, ___orderInLayerSpline_25)); }
	inline int32_t get_orderInLayerSpline_25() const { return ___orderInLayerSpline_25; }
	inline int32_t* get_address_of_orderInLayerSpline_25() { return &___orderInLayerSpline_25; }
	inline void set_orderInLayerSpline_25(int32_t value)
	{
		___orderInLayerSpline_25 = value;
	}

	inline static int32_t get_offset_of_orderInLayerEnd_26() { return static_cast<int32_t>(offsetof(SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657, ___orderInLayerEnd_26)); }
	inline int32_t get_orderInLayerEnd_26() const { return ___orderInLayerEnd_26; }
	inline int32_t* get_address_of_orderInLayerEnd_26() { return &___orderInLayerEnd_26; }
	inline void set_orderInLayerEnd_26(int32_t value)
	{
		___orderInLayerEnd_26 = value;
	}

	inline static int32_t get_offset_of_useGameobjectLayer_27() { return static_cast<int32_t>(offsetof(SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657, ___useGameobjectLayer_27)); }
	inline bool get_useGameobjectLayer_27() const { return ___useGameobjectLayer_27; }
	inline bool* get_address_of_useGameobjectLayer_27() { return &___useGameobjectLayer_27; }
	inline void set_useGameobjectLayer_27(bool value)
	{
		___useGameobjectLayer_27 = value;
	}

	inline static int32_t get_offset_of_controlMode_28() { return static_cast<int32_t>(offsetof(SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657, ___controlMode_28)); }
	inline int32_t get_controlMode_28() const { return ___controlMode_28; }
	inline int32_t* get_address_of_controlMode_28() { return &___controlMode_28; }
	inline void set_controlMode_28(int32_t value)
	{
		___controlMode_28 = value;
	}

	inline static int32_t get_offset_of_orientation_29() { return static_cast<int32_t>(offsetof(SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657, ___orientation_29)); }
	inline int32_t get_orientation_29() const { return ___orientation_29; }
	inline int32_t* get_address_of_orientation_29() { return &___orientation_29; }
	inline void set_orientation_29(int32_t value)
	{
		___orientation_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINETEXTURE_T0E51D252D16938B2BEECA4ED3E11A58E8F463657_H
#ifndef SPLINEWATER_T28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8_H
#define SPLINEWATER_T28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineWater
struct  SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8  : public SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232
{
public:
	// Kolibri2d.SplineWater/WaterEvent Kolibri2d.SplineWater::onEnterEvent
	WaterEvent_t414CD81AE9F28B656088F21ECC14F030B1FDA33B * ___onEnterEvent_8;
	// Kolibri2d.SplineWater/WaterEvent Kolibri2d.SplineWater::onExitEvent
	WaterEvent_t414CD81AE9F28B656088F21ECC14F030B1FDA33B * ___onExitEvent_9;
	// System.Single Kolibri2d.SplineWater::depth
	float ___depth_10;
	// System.Int32 Kolibri2d.SplineWater::edgecount
	int32_t ___edgecount_11;
	// System.Int32 Kolibri2d.SplineWater::nodecount
	int32_t ___nodecount_12;
	// System.Single[] Kolibri2d.SplineWater::x
	SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* ___x_13;
	// System.Single[] Kolibri2d.SplineWater::y
	SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* ___y_14;
	// System.Single[] Kolibri2d.SplineWater::velocities
	SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* ___velocities_15;
	// System.Single[] Kolibri2d.SplineWater::accelerations
	SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* ___accelerations_16;
	// UnityEngine.Vector3[] Kolibri2d.SplineWater::baseVertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___baseVertices_17;
	// UnityEngine.Vector3[] Kolibri2d.SplineWater::meshVertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___meshVertices_18;
	// UnityEngine.Vector2[] Kolibri2d.SplineWater::normals
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___normals_19;
	// UnityEngine.Mesh Kolibri2d.SplineWater::mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh_20;
	// UnityEngine.MeshFilter Kolibri2d.SplineWater::filter
	MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * ___filter_21;
	// UnityEngine.GameObject Kolibri2d.SplineWater::meshObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___meshObject_22;
	// UnityEngine.MeshRenderer Kolibri2d.SplineWater::meshRenderer
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___meshRenderer_23;
	// UnityEngine.PolygonCollider2D Kolibri2d.SplineWater::polygonCollider
	PolygonCollider2D_t360CC66FD7BAF5F4FADE54EB23E58E95D0B8CD81 * ___polygonCollider_24;
	// UnityEngine.Material Kolibri2d.SplineWater::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_25;
	// UnityEngine.Material Kolibri2d.SplineWater::copyMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___copyMaterial_26;
	// Kolibri2d.MaterialTextureInfo Kolibri2d.SplineWater::textureInfo
	MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  ___textureInfo_27;
	// System.Int32 Kolibri2d.SplineWater::orderInLayer
	int32_t ___orderInLayer_28;
	// System.Int32 Kolibri2d.SplineWater::sortingLayer
	int32_t ___sortingLayer_29;
	// System.Boolean Kolibri2d.SplineWater::slowdownOnEnterOrLeave
	bool ___slowdownOnEnterOrLeave_30;
	// UnityEngine.Material Kolibri2d.SplineWater::edgeMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___edgeMaterial_31;
	// UnityEngine.LineRenderer Kolibri2d.SplineWater::line
	LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * ___line_32;
	// System.Boolean Kolibri2d.SplineWater::onEnter
	bool ___onEnter_33;
	// System.Boolean Kolibri2d.SplineWater::onExit
	bool ___onExit_34;
	// System.Single Kolibri2d.SplineWater::mass
	float ___mass_35;
	// System.Single Kolibri2d.SplineWater::spread
	float ___spread_36;
	// System.Single Kolibri2d.SplineWater::springconstant
	float ___springconstant_37;
	// System.Single Kolibri2d.SplineWater::damping
	float ___damping_38;
	// System.Single Kolibri2d.SplineWater::forceMultiplier
	float ___forceMultiplier_39;
	// System.Single Kolibri2d.SplineWater::forceThreshold
	float ___forceThreshold_40;
	// System.Single Kolibri2d.SplineWater::forceClampLow
	float ___forceClampLow_41;
	// System.Single Kolibri2d.SplineWater::forceClampHigh
	float ___forceClampHigh_42;
	// System.Boolean Kolibri2d.SplineWater::waveLimitTop
	bool ___waveLimitTop_43;
	// System.Single Kolibri2d.SplineWater::waveLimitDistance
	float ___waveLimitDistance_44;
	// System.Single Kolibri2d.SplineWater::forceOnEdgesTop
	float ___forceOnEdgesTop_45;
	// System.Single Kolibri2d.SplineWater::forceOnEdgesBottom
	float ___forceOnEdgesBottom_46;
	// System.Int32 Kolibri2d.SplineWater::edgeSize
	int32_t ___edgeSize_47;
	// System.Boolean Kolibri2d.SplineWater::physicsEnabled
	bool ___physicsEnabled_49;
	// System.Boolean Kolibri2d.SplineWater::stayOnEdge
	bool ___stayOnEdge_50;
	// Kolibri2d.SplineWater/ForceDirection Kolibri2d.SplineWater::forceDirection
	int32_t ___forceDirection_51;
	// System.Single Kolibri2d.SplineWater::forceMagnitude
	float ___forceMagnitude_52;
	// System.Single Kolibri2d.SplineWater::floatThreshold
	float ___floatThreshold_53;
	// System.Single Kolibri2d.SplineWater::waterDensity
	float ___waterDensity_54;
	// System.Boolean Kolibri2d.SplineWater::forceDependsOnDistance
	bool ___forceDependsOnDistance_55;
	// System.Boolean Kolibri2d.SplineWater::forceProportionalToMass
	bool ___forceProportionalToMass_56;
	// System.Int32 Kolibri2d.SplineWater::touchingObjects
	int32_t ___touchingObjects_57;
	// UnityEngine.GameObject Kolibri2d.SplineWater::collidersObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___collidersObject_58;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Collider2D,System.Collections.Generic.List`1<System.Int32>> Kolibri2d.SplineWater::touchingColliders
	Dictionary_2_t97FE799FB5630EA306A1F53E2CA192562EF2E217 * ___touchingColliders_59;
	// UnityEngine.PolygonCollider2D[] Kolibri2d.SplineWater::innerColliders
	PolygonCollider2DU5BU5D_t740B9DF43CFB32BA191190CEA2770604FF8C0968* ___innerColliders_60;
	// UnityEngine.GameObject[] Kolibri2d.SplineWater::innerGOs
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___innerGOs_61;
	// System.Int32 Kolibri2d.SplineWater::pointsPerUnit
	int32_t ___pointsPerUnit_62;
	// System.Single Kolibri2d.SplineWater::delta
	float ___delta_63;
	// System.Collections.Generic.List`1<System.Single> Kolibri2d.SplineWater::lengths
	List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * ___lengths_64;
	// System.Collections.Generic.List`1<System.Single> Kolibri2d.SplineWater::lengths01
	List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * ___lengths01_65;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Kolibri2d.SplineWater::points
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___points_66;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Kolibri2d.SplineWater::tangentDirs
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___tangentDirs_67;
	// Kolibri2d.WaterTestPanel Kolibri2d.SplineWater::testPanel
	WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80 * ___testPanel_68;
	// Kolibri2d.SplineWater/MeshSegmentType Kolibri2d.SplineWater::meshSegmentType
	int32_t ___meshSegmentType_70;

public:
	inline static int32_t get_offset_of_onEnterEvent_8() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___onEnterEvent_8)); }
	inline WaterEvent_t414CD81AE9F28B656088F21ECC14F030B1FDA33B * get_onEnterEvent_8() const { return ___onEnterEvent_8; }
	inline WaterEvent_t414CD81AE9F28B656088F21ECC14F030B1FDA33B ** get_address_of_onEnterEvent_8() { return &___onEnterEvent_8; }
	inline void set_onEnterEvent_8(WaterEvent_t414CD81AE9F28B656088F21ECC14F030B1FDA33B * value)
	{
		___onEnterEvent_8 = value;
		Il2CppCodeGenWriteBarrier((&___onEnterEvent_8), value);
	}

	inline static int32_t get_offset_of_onExitEvent_9() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___onExitEvent_9)); }
	inline WaterEvent_t414CD81AE9F28B656088F21ECC14F030B1FDA33B * get_onExitEvent_9() const { return ___onExitEvent_9; }
	inline WaterEvent_t414CD81AE9F28B656088F21ECC14F030B1FDA33B ** get_address_of_onExitEvent_9() { return &___onExitEvent_9; }
	inline void set_onExitEvent_9(WaterEvent_t414CD81AE9F28B656088F21ECC14F030B1FDA33B * value)
	{
		___onExitEvent_9 = value;
		Il2CppCodeGenWriteBarrier((&___onExitEvent_9), value);
	}

	inline static int32_t get_offset_of_depth_10() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___depth_10)); }
	inline float get_depth_10() const { return ___depth_10; }
	inline float* get_address_of_depth_10() { return &___depth_10; }
	inline void set_depth_10(float value)
	{
		___depth_10 = value;
	}

	inline static int32_t get_offset_of_edgecount_11() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___edgecount_11)); }
	inline int32_t get_edgecount_11() const { return ___edgecount_11; }
	inline int32_t* get_address_of_edgecount_11() { return &___edgecount_11; }
	inline void set_edgecount_11(int32_t value)
	{
		___edgecount_11 = value;
	}

	inline static int32_t get_offset_of_nodecount_12() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___nodecount_12)); }
	inline int32_t get_nodecount_12() const { return ___nodecount_12; }
	inline int32_t* get_address_of_nodecount_12() { return &___nodecount_12; }
	inline void set_nodecount_12(int32_t value)
	{
		___nodecount_12 = value;
	}

	inline static int32_t get_offset_of_x_13() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___x_13)); }
	inline SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* get_x_13() const { return ___x_13; }
	inline SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5** get_address_of_x_13() { return &___x_13; }
	inline void set_x_13(SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* value)
	{
		___x_13 = value;
		Il2CppCodeGenWriteBarrier((&___x_13), value);
	}

	inline static int32_t get_offset_of_y_14() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___y_14)); }
	inline SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* get_y_14() const { return ___y_14; }
	inline SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5** get_address_of_y_14() { return &___y_14; }
	inline void set_y_14(SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* value)
	{
		___y_14 = value;
		Il2CppCodeGenWriteBarrier((&___y_14), value);
	}

	inline static int32_t get_offset_of_velocities_15() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___velocities_15)); }
	inline SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* get_velocities_15() const { return ___velocities_15; }
	inline SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5** get_address_of_velocities_15() { return &___velocities_15; }
	inline void set_velocities_15(SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* value)
	{
		___velocities_15 = value;
		Il2CppCodeGenWriteBarrier((&___velocities_15), value);
	}

	inline static int32_t get_offset_of_accelerations_16() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___accelerations_16)); }
	inline SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* get_accelerations_16() const { return ___accelerations_16; }
	inline SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5** get_address_of_accelerations_16() { return &___accelerations_16; }
	inline void set_accelerations_16(SingleU5BU5D_t8C7CE2FD0C4CD3846DEB3BE6DD5564E32A8A27D5* value)
	{
		___accelerations_16 = value;
		Il2CppCodeGenWriteBarrier((&___accelerations_16), value);
	}

	inline static int32_t get_offset_of_baseVertices_17() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___baseVertices_17)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_baseVertices_17() const { return ___baseVertices_17; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_baseVertices_17() { return &___baseVertices_17; }
	inline void set_baseVertices_17(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___baseVertices_17 = value;
		Il2CppCodeGenWriteBarrier((&___baseVertices_17), value);
	}

	inline static int32_t get_offset_of_meshVertices_18() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___meshVertices_18)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_meshVertices_18() const { return ___meshVertices_18; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_meshVertices_18() { return &___meshVertices_18; }
	inline void set_meshVertices_18(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___meshVertices_18 = value;
		Il2CppCodeGenWriteBarrier((&___meshVertices_18), value);
	}

	inline static int32_t get_offset_of_normals_19() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___normals_19)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_normals_19() const { return ___normals_19; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_normals_19() { return &___normals_19; }
	inline void set_normals_19(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___normals_19 = value;
		Il2CppCodeGenWriteBarrier((&___normals_19), value);
	}

	inline static int32_t get_offset_of_mesh_20() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___mesh_20)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_mesh_20() const { return ___mesh_20; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_mesh_20() { return &___mesh_20; }
	inline void set_mesh_20(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___mesh_20 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_20), value);
	}

	inline static int32_t get_offset_of_filter_21() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___filter_21)); }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * get_filter_21() const { return ___filter_21; }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 ** get_address_of_filter_21() { return &___filter_21; }
	inline void set_filter_21(MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * value)
	{
		___filter_21 = value;
		Il2CppCodeGenWriteBarrier((&___filter_21), value);
	}

	inline static int32_t get_offset_of_meshObject_22() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___meshObject_22)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_meshObject_22() const { return ___meshObject_22; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_meshObject_22() { return &___meshObject_22; }
	inline void set_meshObject_22(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___meshObject_22 = value;
		Il2CppCodeGenWriteBarrier((&___meshObject_22), value);
	}

	inline static int32_t get_offset_of_meshRenderer_23() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___meshRenderer_23)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_meshRenderer_23() const { return ___meshRenderer_23; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_meshRenderer_23() { return &___meshRenderer_23; }
	inline void set_meshRenderer_23(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___meshRenderer_23 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_23), value);
	}

	inline static int32_t get_offset_of_polygonCollider_24() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___polygonCollider_24)); }
	inline PolygonCollider2D_t360CC66FD7BAF5F4FADE54EB23E58E95D0B8CD81 * get_polygonCollider_24() const { return ___polygonCollider_24; }
	inline PolygonCollider2D_t360CC66FD7BAF5F4FADE54EB23E58E95D0B8CD81 ** get_address_of_polygonCollider_24() { return &___polygonCollider_24; }
	inline void set_polygonCollider_24(PolygonCollider2D_t360CC66FD7BAF5F4FADE54EB23E58E95D0B8CD81 * value)
	{
		___polygonCollider_24 = value;
		Il2CppCodeGenWriteBarrier((&___polygonCollider_24), value);
	}

	inline static int32_t get_offset_of_material_25() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___material_25)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_25() const { return ___material_25; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_25() { return &___material_25; }
	inline void set_material_25(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_25 = value;
		Il2CppCodeGenWriteBarrier((&___material_25), value);
	}

	inline static int32_t get_offset_of_copyMaterial_26() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___copyMaterial_26)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_copyMaterial_26() const { return ___copyMaterial_26; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_copyMaterial_26() { return &___copyMaterial_26; }
	inline void set_copyMaterial_26(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___copyMaterial_26 = value;
		Il2CppCodeGenWriteBarrier((&___copyMaterial_26), value);
	}

	inline static int32_t get_offset_of_textureInfo_27() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___textureInfo_27)); }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  get_textureInfo_27() const { return ___textureInfo_27; }
	inline MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F * get_address_of_textureInfo_27() { return &___textureInfo_27; }
	inline void set_textureInfo_27(MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F  value)
	{
		___textureInfo_27 = value;
	}

	inline static int32_t get_offset_of_orderInLayer_28() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___orderInLayer_28)); }
	inline int32_t get_orderInLayer_28() const { return ___orderInLayer_28; }
	inline int32_t* get_address_of_orderInLayer_28() { return &___orderInLayer_28; }
	inline void set_orderInLayer_28(int32_t value)
	{
		___orderInLayer_28 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_29() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___sortingLayer_29)); }
	inline int32_t get_sortingLayer_29() const { return ___sortingLayer_29; }
	inline int32_t* get_address_of_sortingLayer_29() { return &___sortingLayer_29; }
	inline void set_sortingLayer_29(int32_t value)
	{
		___sortingLayer_29 = value;
	}

	inline static int32_t get_offset_of_slowdownOnEnterOrLeave_30() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___slowdownOnEnterOrLeave_30)); }
	inline bool get_slowdownOnEnterOrLeave_30() const { return ___slowdownOnEnterOrLeave_30; }
	inline bool* get_address_of_slowdownOnEnterOrLeave_30() { return &___slowdownOnEnterOrLeave_30; }
	inline void set_slowdownOnEnterOrLeave_30(bool value)
	{
		___slowdownOnEnterOrLeave_30 = value;
	}

	inline static int32_t get_offset_of_edgeMaterial_31() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___edgeMaterial_31)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_edgeMaterial_31() const { return ___edgeMaterial_31; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_edgeMaterial_31() { return &___edgeMaterial_31; }
	inline void set_edgeMaterial_31(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___edgeMaterial_31 = value;
		Il2CppCodeGenWriteBarrier((&___edgeMaterial_31), value);
	}

	inline static int32_t get_offset_of_line_32() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___line_32)); }
	inline LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * get_line_32() const { return ___line_32; }
	inline LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 ** get_address_of_line_32() { return &___line_32; }
	inline void set_line_32(LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * value)
	{
		___line_32 = value;
		Il2CppCodeGenWriteBarrier((&___line_32), value);
	}

	inline static int32_t get_offset_of_onEnter_33() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___onEnter_33)); }
	inline bool get_onEnter_33() const { return ___onEnter_33; }
	inline bool* get_address_of_onEnter_33() { return &___onEnter_33; }
	inline void set_onEnter_33(bool value)
	{
		___onEnter_33 = value;
	}

	inline static int32_t get_offset_of_onExit_34() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___onExit_34)); }
	inline bool get_onExit_34() const { return ___onExit_34; }
	inline bool* get_address_of_onExit_34() { return &___onExit_34; }
	inline void set_onExit_34(bool value)
	{
		___onExit_34 = value;
	}

	inline static int32_t get_offset_of_mass_35() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___mass_35)); }
	inline float get_mass_35() const { return ___mass_35; }
	inline float* get_address_of_mass_35() { return &___mass_35; }
	inline void set_mass_35(float value)
	{
		___mass_35 = value;
	}

	inline static int32_t get_offset_of_spread_36() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___spread_36)); }
	inline float get_spread_36() const { return ___spread_36; }
	inline float* get_address_of_spread_36() { return &___spread_36; }
	inline void set_spread_36(float value)
	{
		___spread_36 = value;
	}

	inline static int32_t get_offset_of_springconstant_37() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___springconstant_37)); }
	inline float get_springconstant_37() const { return ___springconstant_37; }
	inline float* get_address_of_springconstant_37() { return &___springconstant_37; }
	inline void set_springconstant_37(float value)
	{
		___springconstant_37 = value;
	}

	inline static int32_t get_offset_of_damping_38() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___damping_38)); }
	inline float get_damping_38() const { return ___damping_38; }
	inline float* get_address_of_damping_38() { return &___damping_38; }
	inline void set_damping_38(float value)
	{
		___damping_38 = value;
	}

	inline static int32_t get_offset_of_forceMultiplier_39() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___forceMultiplier_39)); }
	inline float get_forceMultiplier_39() const { return ___forceMultiplier_39; }
	inline float* get_address_of_forceMultiplier_39() { return &___forceMultiplier_39; }
	inline void set_forceMultiplier_39(float value)
	{
		___forceMultiplier_39 = value;
	}

	inline static int32_t get_offset_of_forceThreshold_40() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___forceThreshold_40)); }
	inline float get_forceThreshold_40() const { return ___forceThreshold_40; }
	inline float* get_address_of_forceThreshold_40() { return &___forceThreshold_40; }
	inline void set_forceThreshold_40(float value)
	{
		___forceThreshold_40 = value;
	}

	inline static int32_t get_offset_of_forceClampLow_41() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___forceClampLow_41)); }
	inline float get_forceClampLow_41() const { return ___forceClampLow_41; }
	inline float* get_address_of_forceClampLow_41() { return &___forceClampLow_41; }
	inline void set_forceClampLow_41(float value)
	{
		___forceClampLow_41 = value;
	}

	inline static int32_t get_offset_of_forceClampHigh_42() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___forceClampHigh_42)); }
	inline float get_forceClampHigh_42() const { return ___forceClampHigh_42; }
	inline float* get_address_of_forceClampHigh_42() { return &___forceClampHigh_42; }
	inline void set_forceClampHigh_42(float value)
	{
		___forceClampHigh_42 = value;
	}

	inline static int32_t get_offset_of_waveLimitTop_43() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___waveLimitTop_43)); }
	inline bool get_waveLimitTop_43() const { return ___waveLimitTop_43; }
	inline bool* get_address_of_waveLimitTop_43() { return &___waveLimitTop_43; }
	inline void set_waveLimitTop_43(bool value)
	{
		___waveLimitTop_43 = value;
	}

	inline static int32_t get_offset_of_waveLimitDistance_44() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___waveLimitDistance_44)); }
	inline float get_waveLimitDistance_44() const { return ___waveLimitDistance_44; }
	inline float* get_address_of_waveLimitDistance_44() { return &___waveLimitDistance_44; }
	inline void set_waveLimitDistance_44(float value)
	{
		___waveLimitDistance_44 = value;
	}

	inline static int32_t get_offset_of_forceOnEdgesTop_45() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___forceOnEdgesTop_45)); }
	inline float get_forceOnEdgesTop_45() const { return ___forceOnEdgesTop_45; }
	inline float* get_address_of_forceOnEdgesTop_45() { return &___forceOnEdgesTop_45; }
	inline void set_forceOnEdgesTop_45(float value)
	{
		___forceOnEdgesTop_45 = value;
	}

	inline static int32_t get_offset_of_forceOnEdgesBottom_46() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___forceOnEdgesBottom_46)); }
	inline float get_forceOnEdgesBottom_46() const { return ___forceOnEdgesBottom_46; }
	inline float* get_address_of_forceOnEdgesBottom_46() { return &___forceOnEdgesBottom_46; }
	inline void set_forceOnEdgesBottom_46(float value)
	{
		___forceOnEdgesBottom_46 = value;
	}

	inline static int32_t get_offset_of_edgeSize_47() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___edgeSize_47)); }
	inline int32_t get_edgeSize_47() const { return ___edgeSize_47; }
	inline int32_t* get_address_of_edgeSize_47() { return &___edgeSize_47; }
	inline void set_edgeSize_47(int32_t value)
	{
		___edgeSize_47 = value;
	}

	inline static int32_t get_offset_of_physicsEnabled_49() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___physicsEnabled_49)); }
	inline bool get_physicsEnabled_49() const { return ___physicsEnabled_49; }
	inline bool* get_address_of_physicsEnabled_49() { return &___physicsEnabled_49; }
	inline void set_physicsEnabled_49(bool value)
	{
		___physicsEnabled_49 = value;
	}

	inline static int32_t get_offset_of_stayOnEdge_50() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___stayOnEdge_50)); }
	inline bool get_stayOnEdge_50() const { return ___stayOnEdge_50; }
	inline bool* get_address_of_stayOnEdge_50() { return &___stayOnEdge_50; }
	inline void set_stayOnEdge_50(bool value)
	{
		___stayOnEdge_50 = value;
	}

	inline static int32_t get_offset_of_forceDirection_51() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___forceDirection_51)); }
	inline int32_t get_forceDirection_51() const { return ___forceDirection_51; }
	inline int32_t* get_address_of_forceDirection_51() { return &___forceDirection_51; }
	inline void set_forceDirection_51(int32_t value)
	{
		___forceDirection_51 = value;
	}

	inline static int32_t get_offset_of_forceMagnitude_52() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___forceMagnitude_52)); }
	inline float get_forceMagnitude_52() const { return ___forceMagnitude_52; }
	inline float* get_address_of_forceMagnitude_52() { return &___forceMagnitude_52; }
	inline void set_forceMagnitude_52(float value)
	{
		___forceMagnitude_52 = value;
	}

	inline static int32_t get_offset_of_floatThreshold_53() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___floatThreshold_53)); }
	inline float get_floatThreshold_53() const { return ___floatThreshold_53; }
	inline float* get_address_of_floatThreshold_53() { return &___floatThreshold_53; }
	inline void set_floatThreshold_53(float value)
	{
		___floatThreshold_53 = value;
	}

	inline static int32_t get_offset_of_waterDensity_54() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___waterDensity_54)); }
	inline float get_waterDensity_54() const { return ___waterDensity_54; }
	inline float* get_address_of_waterDensity_54() { return &___waterDensity_54; }
	inline void set_waterDensity_54(float value)
	{
		___waterDensity_54 = value;
	}

	inline static int32_t get_offset_of_forceDependsOnDistance_55() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___forceDependsOnDistance_55)); }
	inline bool get_forceDependsOnDistance_55() const { return ___forceDependsOnDistance_55; }
	inline bool* get_address_of_forceDependsOnDistance_55() { return &___forceDependsOnDistance_55; }
	inline void set_forceDependsOnDistance_55(bool value)
	{
		___forceDependsOnDistance_55 = value;
	}

	inline static int32_t get_offset_of_forceProportionalToMass_56() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___forceProportionalToMass_56)); }
	inline bool get_forceProportionalToMass_56() const { return ___forceProportionalToMass_56; }
	inline bool* get_address_of_forceProportionalToMass_56() { return &___forceProportionalToMass_56; }
	inline void set_forceProportionalToMass_56(bool value)
	{
		___forceProportionalToMass_56 = value;
	}

	inline static int32_t get_offset_of_touchingObjects_57() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___touchingObjects_57)); }
	inline int32_t get_touchingObjects_57() const { return ___touchingObjects_57; }
	inline int32_t* get_address_of_touchingObjects_57() { return &___touchingObjects_57; }
	inline void set_touchingObjects_57(int32_t value)
	{
		___touchingObjects_57 = value;
	}

	inline static int32_t get_offset_of_collidersObject_58() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___collidersObject_58)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_collidersObject_58() const { return ___collidersObject_58; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_collidersObject_58() { return &___collidersObject_58; }
	inline void set_collidersObject_58(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___collidersObject_58 = value;
		Il2CppCodeGenWriteBarrier((&___collidersObject_58), value);
	}

	inline static int32_t get_offset_of_touchingColliders_59() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___touchingColliders_59)); }
	inline Dictionary_2_t97FE799FB5630EA306A1F53E2CA192562EF2E217 * get_touchingColliders_59() const { return ___touchingColliders_59; }
	inline Dictionary_2_t97FE799FB5630EA306A1F53E2CA192562EF2E217 ** get_address_of_touchingColliders_59() { return &___touchingColliders_59; }
	inline void set_touchingColliders_59(Dictionary_2_t97FE799FB5630EA306A1F53E2CA192562EF2E217 * value)
	{
		___touchingColliders_59 = value;
		Il2CppCodeGenWriteBarrier((&___touchingColliders_59), value);
	}

	inline static int32_t get_offset_of_innerColliders_60() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___innerColliders_60)); }
	inline PolygonCollider2DU5BU5D_t740B9DF43CFB32BA191190CEA2770604FF8C0968* get_innerColliders_60() const { return ___innerColliders_60; }
	inline PolygonCollider2DU5BU5D_t740B9DF43CFB32BA191190CEA2770604FF8C0968** get_address_of_innerColliders_60() { return &___innerColliders_60; }
	inline void set_innerColliders_60(PolygonCollider2DU5BU5D_t740B9DF43CFB32BA191190CEA2770604FF8C0968* value)
	{
		___innerColliders_60 = value;
		Il2CppCodeGenWriteBarrier((&___innerColliders_60), value);
	}

	inline static int32_t get_offset_of_innerGOs_61() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___innerGOs_61)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_innerGOs_61() const { return ___innerGOs_61; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_innerGOs_61() { return &___innerGOs_61; }
	inline void set_innerGOs_61(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___innerGOs_61 = value;
		Il2CppCodeGenWriteBarrier((&___innerGOs_61), value);
	}

	inline static int32_t get_offset_of_pointsPerUnit_62() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___pointsPerUnit_62)); }
	inline int32_t get_pointsPerUnit_62() const { return ___pointsPerUnit_62; }
	inline int32_t* get_address_of_pointsPerUnit_62() { return &___pointsPerUnit_62; }
	inline void set_pointsPerUnit_62(int32_t value)
	{
		___pointsPerUnit_62 = value;
	}

	inline static int32_t get_offset_of_delta_63() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___delta_63)); }
	inline float get_delta_63() const { return ___delta_63; }
	inline float* get_address_of_delta_63() { return &___delta_63; }
	inline void set_delta_63(float value)
	{
		___delta_63 = value;
	}

	inline static int32_t get_offset_of_lengths_64() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___lengths_64)); }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * get_lengths_64() const { return ___lengths_64; }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E ** get_address_of_lengths_64() { return &___lengths_64; }
	inline void set_lengths_64(List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * value)
	{
		___lengths_64 = value;
		Il2CppCodeGenWriteBarrier((&___lengths_64), value);
	}

	inline static int32_t get_offset_of_lengths01_65() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___lengths01_65)); }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * get_lengths01_65() const { return ___lengths01_65; }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E ** get_address_of_lengths01_65() { return &___lengths01_65; }
	inline void set_lengths01_65(List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * value)
	{
		___lengths01_65 = value;
		Il2CppCodeGenWriteBarrier((&___lengths01_65), value);
	}

	inline static int32_t get_offset_of_points_66() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___points_66)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_points_66() const { return ___points_66; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_points_66() { return &___points_66; }
	inline void set_points_66(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___points_66 = value;
		Il2CppCodeGenWriteBarrier((&___points_66), value);
	}

	inline static int32_t get_offset_of_tangentDirs_67() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___tangentDirs_67)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_tangentDirs_67() const { return ___tangentDirs_67; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_tangentDirs_67() { return &___tangentDirs_67; }
	inline void set_tangentDirs_67(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___tangentDirs_67 = value;
		Il2CppCodeGenWriteBarrier((&___tangentDirs_67), value);
	}

	inline static int32_t get_offset_of_testPanel_68() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___testPanel_68)); }
	inline WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80 * get_testPanel_68() const { return ___testPanel_68; }
	inline WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80 ** get_address_of_testPanel_68() { return &___testPanel_68; }
	inline void set_testPanel_68(WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80 * value)
	{
		___testPanel_68 = value;
		Il2CppCodeGenWriteBarrier((&___testPanel_68), value);
	}

	inline static int32_t get_offset_of_meshSegmentType_70() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8, ___meshSegmentType_70)); }
	inline int32_t get_meshSegmentType_70() const { return ___meshSegmentType_70; }
	inline int32_t* get_address_of_meshSegmentType_70() { return &___meshSegmentType_70; }
	inline void set_meshSegmentType_70(int32_t value)
	{
		___meshSegmentType_70 = value;
	}
};

struct SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8_StaticFields
{
public:
	// UnityEngine.Vector2 Kolibri2d.SplineWater::flatNormal
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___flatNormal_69;

public:
	inline static int32_t get_offset_of_flatNormal_69() { return static_cast<int32_t>(offsetof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8_StaticFields, ___flatNormal_69)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_flatNormal_69() const { return ___flatNormal_69; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_flatNormal_69() { return &___flatNormal_69; }
	inline void set_flatNormal_69(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___flatNormal_69 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINEWATER_T28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8_H
#ifndef TERRAIN2D_T0C58931CB54475940DC223D25AB20A9389D0BA4B_H
#define TERRAIN2D_T0C58931CB54475940DC223D25AB20A9389D0BA4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.Terrain2D
struct  Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B  : public SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232
{
public:
	// System.Int32 Kolibri2d.Terrain2D::additiveSortInLayer
	int32_t ___additiveSortInLayer_8;
	// System.Collections.Generic.List`1<Kolibri2d.Terrain2D/Terrain2DMaterialHolder> Kolibri2d.Terrain2D::materialHolders
	List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 * ___materialHolders_9;
	// Kolibri2d.SplineTerrain2DCreator Kolibri2d.Terrain2D::creator
	SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1 * ___creator_10;
	// System.Single Kolibri2d.Terrain2D::skirtOffsetMagnitude
	float ___skirtOffsetMagnitude_11;
	// System.Single Kolibri2d.Terrain2D::skirtOffsetAngle
	float ___skirtOffsetAngle_12;
	// System.Single Kolibri2d.Terrain2D::skirtPosY
	float ___skirtPosY_13;
	// System.Single Kolibri2d.Terrain2D::skirtPosX
	float ___skirtPosX_14;
	// System.Single Kolibri2d.Terrain2D::skirtTopOffsetMagnitude
	float ___skirtTopOffsetMagnitude_15;
	// System.Single Kolibri2d.Terrain2D::skirtBottomOffsetMagnitude
	float ___skirtBottomOffsetMagnitude_16;
	// System.Single Kolibri2d.Terrain2D::skirtLeftOffsetMagnitude
	float ___skirtLeftOffsetMagnitude_17;
	// System.Single Kolibri2d.Terrain2D::skirtRightOffsetMagnitude
	float ___skirtRightOffsetMagnitude_18;
	// Kolibri2d.Terrain2D/SkirtType Kolibri2d.Terrain2D::skirtType
	int32_t ___skirtType_19;
	// System.Single Kolibri2d.Terrain2D::distortion
	float ___distortion_20;
	// System.Int32 Kolibri2d.Terrain2D::meshSubdivision
	int32_t ___meshSubdivision_21;

public:
	inline static int32_t get_offset_of_additiveSortInLayer_8() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___additiveSortInLayer_8)); }
	inline int32_t get_additiveSortInLayer_8() const { return ___additiveSortInLayer_8; }
	inline int32_t* get_address_of_additiveSortInLayer_8() { return &___additiveSortInLayer_8; }
	inline void set_additiveSortInLayer_8(int32_t value)
	{
		___additiveSortInLayer_8 = value;
	}

	inline static int32_t get_offset_of_materialHolders_9() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___materialHolders_9)); }
	inline List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 * get_materialHolders_9() const { return ___materialHolders_9; }
	inline List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 ** get_address_of_materialHolders_9() { return &___materialHolders_9; }
	inline void set_materialHolders_9(List_1_t99BD3A0F2A4B242B1CBE43207D6ADABBA35812D4 * value)
	{
		___materialHolders_9 = value;
		Il2CppCodeGenWriteBarrier((&___materialHolders_9), value);
	}

	inline static int32_t get_offset_of_creator_10() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___creator_10)); }
	inline SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1 * get_creator_10() const { return ___creator_10; }
	inline SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1 ** get_address_of_creator_10() { return &___creator_10; }
	inline void set_creator_10(SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1 * value)
	{
		___creator_10 = value;
		Il2CppCodeGenWriteBarrier((&___creator_10), value);
	}

	inline static int32_t get_offset_of_skirtOffsetMagnitude_11() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___skirtOffsetMagnitude_11)); }
	inline float get_skirtOffsetMagnitude_11() const { return ___skirtOffsetMagnitude_11; }
	inline float* get_address_of_skirtOffsetMagnitude_11() { return &___skirtOffsetMagnitude_11; }
	inline void set_skirtOffsetMagnitude_11(float value)
	{
		___skirtOffsetMagnitude_11 = value;
	}

	inline static int32_t get_offset_of_skirtOffsetAngle_12() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___skirtOffsetAngle_12)); }
	inline float get_skirtOffsetAngle_12() const { return ___skirtOffsetAngle_12; }
	inline float* get_address_of_skirtOffsetAngle_12() { return &___skirtOffsetAngle_12; }
	inline void set_skirtOffsetAngle_12(float value)
	{
		___skirtOffsetAngle_12 = value;
	}

	inline static int32_t get_offset_of_skirtPosY_13() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___skirtPosY_13)); }
	inline float get_skirtPosY_13() const { return ___skirtPosY_13; }
	inline float* get_address_of_skirtPosY_13() { return &___skirtPosY_13; }
	inline void set_skirtPosY_13(float value)
	{
		___skirtPosY_13 = value;
	}

	inline static int32_t get_offset_of_skirtPosX_14() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___skirtPosX_14)); }
	inline float get_skirtPosX_14() const { return ___skirtPosX_14; }
	inline float* get_address_of_skirtPosX_14() { return &___skirtPosX_14; }
	inline void set_skirtPosX_14(float value)
	{
		___skirtPosX_14 = value;
	}

	inline static int32_t get_offset_of_skirtTopOffsetMagnitude_15() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___skirtTopOffsetMagnitude_15)); }
	inline float get_skirtTopOffsetMagnitude_15() const { return ___skirtTopOffsetMagnitude_15; }
	inline float* get_address_of_skirtTopOffsetMagnitude_15() { return &___skirtTopOffsetMagnitude_15; }
	inline void set_skirtTopOffsetMagnitude_15(float value)
	{
		___skirtTopOffsetMagnitude_15 = value;
	}

	inline static int32_t get_offset_of_skirtBottomOffsetMagnitude_16() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___skirtBottomOffsetMagnitude_16)); }
	inline float get_skirtBottomOffsetMagnitude_16() const { return ___skirtBottomOffsetMagnitude_16; }
	inline float* get_address_of_skirtBottomOffsetMagnitude_16() { return &___skirtBottomOffsetMagnitude_16; }
	inline void set_skirtBottomOffsetMagnitude_16(float value)
	{
		___skirtBottomOffsetMagnitude_16 = value;
	}

	inline static int32_t get_offset_of_skirtLeftOffsetMagnitude_17() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___skirtLeftOffsetMagnitude_17)); }
	inline float get_skirtLeftOffsetMagnitude_17() const { return ___skirtLeftOffsetMagnitude_17; }
	inline float* get_address_of_skirtLeftOffsetMagnitude_17() { return &___skirtLeftOffsetMagnitude_17; }
	inline void set_skirtLeftOffsetMagnitude_17(float value)
	{
		___skirtLeftOffsetMagnitude_17 = value;
	}

	inline static int32_t get_offset_of_skirtRightOffsetMagnitude_18() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___skirtRightOffsetMagnitude_18)); }
	inline float get_skirtRightOffsetMagnitude_18() const { return ___skirtRightOffsetMagnitude_18; }
	inline float* get_address_of_skirtRightOffsetMagnitude_18() { return &___skirtRightOffsetMagnitude_18; }
	inline void set_skirtRightOffsetMagnitude_18(float value)
	{
		___skirtRightOffsetMagnitude_18 = value;
	}

	inline static int32_t get_offset_of_skirtType_19() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___skirtType_19)); }
	inline int32_t get_skirtType_19() const { return ___skirtType_19; }
	inline int32_t* get_address_of_skirtType_19() { return &___skirtType_19; }
	inline void set_skirtType_19(int32_t value)
	{
		___skirtType_19 = value;
	}

	inline static int32_t get_offset_of_distortion_20() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___distortion_20)); }
	inline float get_distortion_20() const { return ___distortion_20; }
	inline float* get_address_of_distortion_20() { return &___distortion_20; }
	inline void set_distortion_20(float value)
	{
		___distortion_20 = value;
	}

	inline static int32_t get_offset_of_meshSubdivision_21() { return static_cast<int32_t>(offsetof(Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B, ___meshSubdivision_21)); }
	inline int32_t get_meshSubdivision_21() const { return ___meshSubdivision_21; }
	inline int32_t* get_address_of_meshSubdivision_21() { return &___meshSubdivision_21; }
	inline void set_meshSubdivision_21(int32_t value)
	{
		___meshSubdivision_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERRAIN2D_T0C58931CB54475940DC223D25AB20A9389D0BA4B_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591), -1, sizeof(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2000[1] = 
{
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591_StaticFields::get_offset_of_willRenderCanvases_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (WillRenderCanvases_tBD5AD090B5938021DEAA679A5AEEA790F60A8BEE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (UISystemProfilerApi_tD33E558B2D0176096E5DB375956ACA9F03678F1B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (SampleType_t2144AEAF3447ACAFCE1C13AF669F63192F8E75EC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2003[3] = 
{
	SampleType_t2144AEAF3447ACAFCE1C13AF669F63192F8E75EC::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2006[1] = 
{
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72::get_offset_of_U3CisMaskU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (RectTransformUtility_t9B90669A72B05A33DD88BEBB817BC9CDBB614BBA), -1, sizeof(RectTransformUtility_t9B90669A72B05A33DD88BEBB817BC9CDBB614BBA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2007[1] = 
{
	RectTransformUtility_t9B90669A72B05A33DD88BEBB817BC9CDBB614BBA_StaticFields::get_offset_of_s_Corners_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (U3CModuleU3E_tCD4309F8DDA0F37A98DBCDFE49F6C8F300C242B0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (ContinuousEvent_tBAB6336255F3FC327CBA03CE368CD4D8D027107A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (AnalyticsSessionState_t61CA873937E9A3B881B71B32F518A954A4C8F267)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2010[5] = 
{
	AnalyticsSessionState_t61CA873937E9A3B881B71B32F518A954A4C8F267::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (AnalyticsSessionInfo_tE075F764A74D2B095CFD57F3B179397F504B7D8C), -1, sizeof(AnalyticsSessionInfo_tE075F764A74D2B095CFD57F3B179397F504B7D8C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2011[1] = 
{
	AnalyticsSessionInfo_tE075F764A74D2B095CFD57F3B179397F504B7D8C_StaticFields::get_offset_of_sessionStateChanged_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (SessionStateChanged_t9084549A636BD45086D66CC6765DA8C3DD31066F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED), -1, sizeof(RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2013[3] = 
{
	RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_StaticFields::get_offset_of_Updated_0(),
	RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_StaticFields::get_offset_of_BeforeFetchFromServer_1(),
	RemoteSettings_t3F7E07D15288B0DF84A4A32044592D8AFA6D36ED_StaticFields::get_offset_of_Completed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (UpdatedEventHandler_tB0230BC83686D7126AB4D3800A66351028CA514F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A), sizeof(RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2015[2] = 
{
	RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A::get_offset_of_m_Ptr_0(),
	RemoteConfigSettings_t97154F5546B47CE72257CC2F0B677BDF696AEC4A::get_offset_of_Updated_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (U3CModuleU3E_t2FBFFC67F8D6B1FA13284515F9BBD8C9333B5C86), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (WebRequestUtils_tBE8F8607E3A9633419968F6AF2F706A029AE1296), -1, sizeof(WebRequestUtils_tBE8F8607E3A9633419968F6AF2F706A029AE1296_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2017[1] = 
{
	WebRequestUtils_tBE8F8607E3A9633419968F6AF2F706A029AE1296_StaticFields::get_offset_of_domainRegex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0), sizeof(CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2018[1] = 
{
	CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (U3CModuleU3E_tB308A2384DEB86F8845A4E61970976B8944B5DC4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (U3CModuleU3E_t46A9D89D51A2AC061548E4AC6E72FAFE9714FED8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (Bezier_tC49C92841ACE82C81E44CB3A8B49CD6A8911FE1F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (BezierControlPointMode_t193A371F3CAA9EE612A584D1117BAFCA998D28C1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2022[4] = 
{
	BezierControlPointMode_t193A371F3CAA9EE612A584D1117BAFCA998D28C1::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (ShapeTangentMode_tD59E4B5749266A0BCF5D57B67CD30F5832591CAE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2023[4] = 
{
	ShapeTangentMode_tD59E4B5749266A0BCF5D57B67CD30F5832591CAE::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE), -1, sizeof(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2024[22] = 
{
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE::get_offset_of_points_4(),
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE::get_offset_of_allStepPoints_5(),
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE::get_offset_of_allStepLengths01_6(),
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE::get_offset_of_allStepLengths_7(),
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE::get_offset_of_allStepDirections_8(),
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE::get_offset_of_allStepNormals_9(),
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE::get_offset_of_showDirection_10(),
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE::get_offset_of_isClockwise_11(),
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE::get_offset_of_smoothness_12(),
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE::get_offset_of_onRefreshCallback_13(),
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE::get_offset_of_spline_14(),
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields::get_offset_of_KEpsilon_15(),
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE::get_offset_of_perPointData_16(),
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields::get_offset_of_settings_shapeTangentMode_17(),
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields::get_offset_of_settings_smoothness_18(),
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields::get_offset_of_settings_squareLinearTangents_19(),
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields::get_offset_of_settings_autoSelectPoints_20(),
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields::get_offset_of_settings_pointsAlwaysVisible_21(),
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields::get_offset_of_settings_sceneHandleSize_22(),
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE_StaticFields::get_offset_of_settings_quality_23(),
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE::get_offset_of_adjustTangentsOnMoving_24(),
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE::get_offset_of_quality_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (BezierPoint_t371F386D0EC739B5463360D9BB85CFBE271E9D93), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2025[7] = 
{
	BezierPoint_t371F386D0EC739B5463360D9BB85CFBE271E9D93::get_offset_of_p0_0(),
	BezierPoint_t371F386D0EC739B5463360D9BB85CFBE271E9D93::get_offset_of_p1_1(),
	BezierPoint_t371F386D0EC739B5463360D9BB85CFBE271E9D93::get_offset_of_p2_2(),
	BezierPoint_t371F386D0EC739B5463360D9BB85CFBE271E9D93::get_offset_of_length_3(),
	BezierPoint_t371F386D0EC739B5463360D9BB85CFBE271E9D93::get_offset_of_mode_4(),
	BezierPoint_t371F386D0EC739B5463360D9BB85CFBE271E9D93::get_offset_of_stepPoints_5(),
	BezierPoint_t371F386D0EC739B5463360D9BB85CFBE271E9D93::get_offset_of_stepLengths_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (OnRefreshCallback_t30E4E0D8B612B00EAE2C396E25323C621CF16300), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (PointData_t8282E0CA58CA95B34C07C6A651E1A63EC74662F9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2027[5] = 
{
	PointData_t8282E0CA58CA95B34C07C6A651E1A63EC74662F9::get_offset_of_subdivisions_0(),
	PointData_t8282E0CA58CA95B34C07C6A651E1A63EC74662F9::get_offset_of_positions_1(),
	PointData_t8282E0CA58CA95B34C07C6A651E1A63EC74662F9::get_offset_of_lengths_2(),
	PointData_t8282E0CA58CA95B34C07C6A651E1A63EC74662F9::get_offset_of_smoothness_3(),
	PointData_t8282E0CA58CA95B34C07C6A651E1A63EC74662F9::get_offset_of_manualTangent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (PointType_tA15C051FEF502E0AB63B5CE0330A71DCE2189E34)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2028[5] = 
{
	PointType_tA15C051FEF502E0AB63B5CE0330A71DCE2189E34::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2029[9] = 
{
	BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF::get_offset_of_mode_0(),
	BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF::get_offset_of_level_1(),
	BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF::get_offset_of_userSubdivisionsPerSegment_2(),
	BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF::get_offset_of_fixedSubdivisionsPerSegment_3(),
	BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF::get_offset_of_straigthSegmentMultiplier_4(),
	BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF::get_offset_of_softCurveSegmentMultiplier_5(),
	BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF::get_offset_of_hardCurveSegmentMultiplier_6(),
	BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF::get_offset_of_smallSegmentMultiplier_7(),
	BezierSplineQuality_t3B3DE950D556B5410E2BEAFD1B6DFEA8360B4BBF::get_offset_of_bigSegmentMultiplier_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (QualityLevel_t134FE67DABCBA0D9D03CFC9C993315F0F55122B2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2030[5] = 
{
	QualityLevel_t134FE67DABCBA0D9D03CFC9C993315F0F55122B2::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (SubdivisionMode_t1941AE7CBC2B923FCE3A36DA75996F5B81E4AC61)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2031[3] = 
{
	SubdivisionMode_t1941AE7CBC2B923FCE3A36DA75996F5B81E4AC61::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A), -1, sizeof(SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2032[7] = 
{
	SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A_StaticFields::get_offset_of_decorationNodeName_4(),
	SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A::get_offset_of_spline_5(),
	SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A::get_offset_of_arch_6(),
	SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A::get_offset_of_useGameobjectLayer_7(),
	SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A::get_offset_of_physicsSettings_8(),
	SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A::get_offset_of_refreshOnNextUpdate_9(),
	SplineColliderCreator_tF1A0739E71C3626B1789AA6E68D8BD5244C83B1A::get_offset_of_refreshCallback_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (SplineControlPoint_t5FB60D8FEB29C651E953002ECE34F9BA73654764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2033[6] = 
{
	SplineControlPoint_t5FB60D8FEB29C651E953002ECE34F9BA73654764::get_offset_of_height_0(),
	SplineControlPoint_t5FB60D8FEB29C651E953002ECE34F9BA73654764::get_offset_of_leftTangent_1(),
	SplineControlPoint_t5FB60D8FEB29C651E953002ECE34F9BA73654764::get_offset_of_rightTangent_2(),
	SplineControlPoint_t5FB60D8FEB29C651E953002ECE34F9BA73654764::get_offset_of_position_3(),
	SplineControlPoint_t5FB60D8FEB29C651E953002ECE34F9BA73654764::get_offset_of_mode_4(),
	SplineControlPoint_t5FB60D8FEB29C651E953002ECE34F9BA73654764::get_offset_of_subdivisionsOverride_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (SplineData_t97926CA36C3A0FF54D20743F14165941B0C11747), -1, sizeof(SplineData_t97926CA36C3A0FF54D20743F14165941B0C11747_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2034[3] = 
{
	SplineData_t97926CA36C3A0FF54D20743F14165941B0C11747::get_offset_of_m_IsOpenEnded_0(),
	SplineData_t97926CA36C3A0FF54D20743F14165941B0C11747::get_offset_of_m_ControlPoints_1(),
	SplineData_t97926CA36C3A0FF54D20743F14165941B0C11747_StaticFields::get_offset_of_KEpsilon_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2035[4] = 
{
	SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232::get_offset_of_spline_4(),
	SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232::get_offset_of_arch_5(),
	SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232::get_offset_of_refreshOnNextUpdate_6(),
	SplineModifierWithCallbacks_t6F0DF8A674D295BBD21F986A7733B3E12CAE1232::get_offset_of_refreshCallback_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159), -1, sizeof(Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2036[17] = 
{
	Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159_StaticFields::get_offset_of_DEFAULT_MIN_SCALE_0(),
	Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159_StaticFields::get_offset_of_DEFAULT_MAX_SCALE_1(),
	Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159_StaticFields::get_offset_of_DEFAULT_MAX_ROTATION_2(),
	Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159_StaticFields::get_offset_of_DEFAULT_OFFSET_Y_3(),
	Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159::get_offset_of_name_4(),
	Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159::get_offset_of_allDefault_5(),
	Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159::get_offset_of_spr_6(),
	Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159::get_offset_of_objToInstantiate_7(),
	Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159::get_offset_of_tint_8(),
	Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159::get_offset_of_useTint_9(),
	Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159::get_offset_of_defaultRotation_10(),
	Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159::get_offset_of_maxRotation_11(),
	Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159::get_offset_of_defaultScale_12(),
	Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159::get_offset_of_minScale_13(),
	Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159::get_offset_of_maxScale_14(),
	Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159::get_offset_of_offsetY_15(),
	Decoration_t250E65EB1BF343D4A3EDACAABBC4608D99C2F159::get_offset_of_randomFlipX_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2037[25] = 
{
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_disabledForEveryone_4(),
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_distributionType_5(),
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_orderType_6(),
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_margin_7(),
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_offsetMin_8(),
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_offsetMax_9(),
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_maxObjects_10(),
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_density_11(),
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_distributedAmount_12(),
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_distributionPerSegment_13(),
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_constantScale_14(),
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_randomizeScales_15(),
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_minScale_16(),
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_maxScale_17(),
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_setGameObjectLayer_18(),
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_layer_19(),
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_sortingLayerOptions_20(),
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_grounds_21(),
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_ceilings_22(),
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_walls_23(),
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_DraggedObjectsFromInspector_24(),
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_renderOrderType_25(),
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_renderZDepthDelta_26(),
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_renderZDepthMin_27(),
	DecorationGroup_tA420E574206C36AC498513E58EE588CEC7FA50F0::get_offset_of_renderZDepthMax_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (SortingLayerOptions_t241DB2E522009CEB9D89742D1213F4B22EE99E9E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2038[3] = 
{
	SortingLayerOptions_t241DB2E522009CEB9D89742D1213F4B22EE99E9E::get_offset_of_sortingLayerId_0(),
	SortingLayerOptions_t241DB2E522009CEB9D89742D1213F4B22EE99E9E::get_offset_of_orderInLayerMin_1(),
	SortingLayerOptions_t241DB2E522009CEB9D89742D1213F4B22EE99E9E::get_offset_of_orderInLayerMax_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (DistributionType_tC7078E73ECB6855CFC58B052FB24A0B2B9B31602)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2039[6] = 
{
	DistributionType_tC7078E73ECB6855CFC58B052FB24A0B2B9B31602::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (OrderType_tBDCF237B9AC437D5BEDFD0744D52109576047CB0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2040[3] = 
{
	OrderType_tBDCF237B9AC437D5BEDFD0744D52109576047CB0::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (RenderOrderType_t4DD74BC3AC697D444EEF0D713C65FCB3218866B7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2041[3] = 
{
	RenderOrderType_t4DD74BC3AC697D444EEF0D713C65FCB3218866B7::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (SeedRandomizer_tBE46395F312BD0ECAB808CE4572B917DA733887E)+ sizeof (RuntimeObject), sizeof(SeedRandomizer_tBE46395F312BD0ECAB808CE4572B917DA733887E ), 0, 0 };
extern const int32_t g_FieldOffsetTable2042[3] = 
{
	SeedRandomizer_tBE46395F312BD0ECAB808CE4572B917DA733887E::get_offset_of_currentValue_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SeedRandomizer_tBE46395F312BD0ECAB808CE4572B917DA733887E::get_offset_of_seed_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SeedRandomizer_tBE46395F312BD0ECAB808CE4572B917DA733887E::get_offset_of_seedState_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (DecorationGroupRandomizer_t723A277DE2132DC6F09140A53D0393805D412605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2043[1] = 
{
	DecorationGroupRandomizer_t723A277DE2132DC6F09140A53D0393805D412605::get_offset_of_seeds_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (DecorationMaterial_t0D0DD06B7E2AC4FF1E10BA9FAEAB4FEBDD125A14), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2044[1] = 
{
	DecorationMaterial_t0D0DD06B7E2AC4FF1E10BA9FAEAB4FEBDD125A14::get_offset_of_groupHolders_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (GroupHolder_t6D44EAE647CB3F6269BCB0EF2BF51C40C3E14286), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2045[3] = 
{
	GroupHolder_t6D44EAE647CB3F6269BCB0EF2BF51C40C3E14286::get_offset_of_unfolded_0(),
	GroupHolder_t6D44EAE647CB3F6269BCB0EF2BF51C40C3E14286::get_offset_of_enabled_1(),
	GroupHolder_t6D44EAE647CB3F6269BCB0EF2BF51C40C3E14286::get_offset_of_group_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7), -1, sizeof(QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2046[31] = 
{
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_scale_8(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_sortingLayerOptions_9(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_material_10(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_objects_11(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_decorations_12(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_scaleMin_13(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_scaleMax_14(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_offsetMin_15(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_offsetMax_16(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_distribution_17(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_order_18(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_margin_19(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_density_20(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_distributedAmount_21(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_distributionPerSegment_22(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_renderOrderType_23(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_renderZDepthDelta_24(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_renderZDepthMin_25(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_renderZDepthMax_26(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_setGameObjectLayer_27(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_tint_28(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_useTint_29(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_draggedObjectsFromInspector_30(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7_StaticFields::get_offset_of_decorationNodeName_31(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_groupRandomizer_32(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_randomizer_33(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_orderedIndex_34(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_createdMaterials_35(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_indexesDepthZ_36(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_maxCreatedObjects_37(),
	QuickDecorator_t070F88253DAE60952901A2E8AF53B26EE77163F7::get_offset_of_facingTowardsSpline_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (SplineDecorator_t742C8164AFC76021C654CF40DED2D581C653453C), -1, sizeof(SplineDecorator_t742C8164AFC76021C654CF40DED2D581C653453C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2047[10] = 
{
	SplineDecorator_t742C8164AFC76021C654CF40DED2D581C653453C_StaticFields::get_offset_of_decorationNodeName_8(),
	SplineDecorator_t742C8164AFC76021C654CF40DED2D581C653453C::get_offset_of_decoMat_9(),
	SplineDecorator_t742C8164AFC76021C654CF40DED2D581C653453C::get_offset_of_useTint_10(),
	SplineDecorator_t742C8164AFC76021C654CF40DED2D581C653453C::get_offset_of_tint_11(),
	SplineDecorator_t742C8164AFC76021C654CF40DED2D581C653453C::get_offset_of_additiveSortInLayer_12(),
	SplineDecorator_t742C8164AFC76021C654CF40DED2D581C653453C::get_offset_of_decorationGroupRandomizers_13(),
	SplineDecorator_t742C8164AFC76021C654CF40DED2D581C653453C::get_offset_of_material_14(),
	SplineDecorator_t742C8164AFC76021C654CF40DED2D581C653453C::get_offset_of_createdMaterials_15(),
	SplineDecorator_t742C8164AFC76021C654CF40DED2D581C653453C::get_offset_of_orderedIndices_16(),
	SplineDecorator_t742C8164AFC76021C654CF40DED2D581C653453C::get_offset_of_indexesDepthZ_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (SplineDecorationData_t24E7CE18B8562961F4A230E019E1B7EFFFA6721E)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2048[10] = 
{
	SplineDecorationData_t24E7CE18B8562961F4A230E019E1B7EFFFA6721E::get_offset_of_decoration_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SplineDecorationData_t24E7CE18B8562961F4A230E019E1B7EFFFA6721E::get_offset_of_scale_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SplineDecorationData_t24E7CE18B8562961F4A230E019E1B7EFFFA6721E::get_offset_of_halfWidth_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SplineDecorationData_t24E7CE18B8562961F4A230E019E1B7EFFFA6721E::get_offset_of_halfWidthPlusMargin_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SplineDecorationData_t24E7CE18B8562961F4A230E019E1B7EFFFA6721E::get_offset_of_dir_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SplineDecorationData_t24E7CE18B8562961F4A230E019E1B7EFFFA6721E::get_offset_of_pos_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SplineDecorationData_t24E7CE18B8562961F4A230E019E1B7EFFFA6721E::get_offset_of_angle_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SplineDecorationData_t24E7CE18B8562961F4A230E019E1B7EFFFA6721E::get_offset_of_middleType_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SplineDecorationData_t24E7CE18B8562961F4A230E019E1B7EFFFA6721E::get_offset_of_beginType_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SplineDecorationData_t24E7CE18B8562961F4A230E019E1B7EFFFA6721E::get_offset_of_flipX_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2049[22] = 
{
	SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657::get_offset_of_scale_8(),
	SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657::get_offset_of_textureBase_9(),
	SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657::get_offset_of_assetBegin_10(),
	SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657::get_offset_of_assetEnd_11(),
	SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657::get_offset_of_tint_12(),
	SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657::get_offset_of_useTint_13(),
	SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657::get_offset_of_material_14(),
	SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657::get_offset_of_mesh_15(),
	SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657::get_offset_of_createdMaterials_16(),
	SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657::get_offset_of_meshObject_17(),
	SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657::get_offset_of_createdObjectEnd_18(),
	SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657::get_offset_of_createdObjectBegin_19(),
	SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657::get_offset_of_additionalOrderInLayer_20(),
	SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657::get_offset_of_sortingLayerIDBegin_21(),
	SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657::get_offset_of_sortingLayerIDSpline_22(),
	SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657::get_offset_of_sortingLayerIDEnd_23(),
	SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657::get_offset_of_orderInLayerBegin_24(),
	SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657::get_offset_of_orderInLayerSpline_25(),
	SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657::get_offset_of_orderInLayerEnd_26(),
	SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657::get_offset_of_useGameobjectLayer_27(),
	SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657::get_offset_of_controlMode_28(),
	SplineTexture_t0E51D252D16938B2BEECA4ED3E11A58E8F463657::get_offset_of_orientation_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (OrientationMode_tAE99237272AD796110B94EF70D5129E96516C72B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2050[3] = 
{
	OrientationMode_tAE99237272AD796110B94EF70D5129E96516C72B::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (ControlMode_tE3E10F0DAE6B22C9CA156C8E559E83C5974927DE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2051[3] = 
{
	ControlMode_tE3E10F0DAE6B22C9CA156C8E559E83C5974927DE::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2052[5] = 
{
	PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190::get_offset_of_enabled_0(),
	PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190::get_offset_of_material_1(),
	PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190::get_offset_of_isTrigger_2(),
	PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190::get_offset_of_usedByEffector_3(),
	PhysicsSettings_t78DBD1674A2EAD9BCEC0AEF533EAFF2CED047190::get_offset_of_radius_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2053[7] = 
{
	0,
	SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4::get_offset_of_polygonCollider_1(),
	SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4::get_offset_of_edgeCollider_2(),
	SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4::get_offset_of_groundCollider_3(),
	SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4::get_offset_of_wallCollider_4(),
	SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4::get_offset_of_ceilingCollider_5(),
	SplineColliderSettings_t0665313F90EDA2B7B57385855A06973EA15213A4::get_offset_of_physicsSettingsByTerrainType_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (TerrainType_tCDCA8C4F29F1D3F57A51D0DD84B025709F7BFB19)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2054[6] = 
{
	TerrainType_tCDCA8C4F29F1D3F57A51D0DD84B025709F7BFB19::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2055[14] = 
{
	Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B::get_offset_of_additiveSortInLayer_8(),
	Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B::get_offset_of_materialHolders_9(),
	Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B::get_offset_of_creator_10(),
	Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B::get_offset_of_skirtOffsetMagnitude_11(),
	Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B::get_offset_of_skirtOffsetAngle_12(),
	Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B::get_offset_of_skirtPosY_13(),
	Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B::get_offset_of_skirtPosX_14(),
	Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B::get_offset_of_skirtTopOffsetMagnitude_15(),
	Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B::get_offset_of_skirtBottomOffsetMagnitude_16(),
	Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B::get_offset_of_skirtLeftOffsetMagnitude_17(),
	Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B::get_offset_of_skirtRightOffsetMagnitude_18(),
	Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B::get_offset_of_skirtType_19(),
	Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B::get_offset_of_distortion_20(),
	Terrain2D_t0C58931CB54475940DC223D25AB20A9389D0BA4B::get_offset_of_meshSubdivision_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (SkirtType_tF27DB6D7DFF61A49FB473B5B26C52F1EBED74A99)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2056[9] = 
{
	SkirtType_tF27DB6D7DFF61A49FB473B5B26C52F1EBED74A99::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2057[6] = 
{
	Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F::get_offset_of_enabled_0(),
	Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F::get_offset_of_terrainMaterial_1(),
	Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F::get_offset_of_material_2(),
	Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F::get_offset_of_tint_3(),
	Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F::get_offset_of_useTint_4(),
	Terrain2DMaterialHolder_tBF26B45C9650004249F5363B2D8AB054CB1EE60F::get_offset_of_createColliders_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066)+ sizeof (RuntimeObject), sizeof(SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2058[6] = 
{
	SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066::get_offset_of_begin_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066::get_offset_of_end_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066::get_offset_of_angleBegin_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066::get_offset_of_angleCorner_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066::get_offset_of_dirBegin_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SplineSegmentInfo_tBB34F651D1ED6826933D71C125491ADE7056F066::get_offset_of_type_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2059[9] = 
{
	Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A::get_offset_of_spline_4(),
	Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A::get_offset_of_aspect_5(),
	Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A::get_offset_of_exteriorMargin_6(),
	Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A::get_offset_of_manualFlip_7(),
	Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A::get_offset_of_overrideAngle_8(),
	Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A::get_offset_of_currArchAngle_9(),
	Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A::get_offset_of_stepPointNormals_10(),
	Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A::get_offset_of_stepPointNormalsSmooth_11(),
	Terrain2DArchitecture_t01B2C8A0361E9A7595B6F9267322714F6672DB3A::get_offset_of_segmentsInfo_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (Aspect_t4C69BE83340C4AF890ACE7BB85B0B09491E8C5AC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2060[4] = 
{
	Aspect_t4C69BE83340C4AF890ACE7BB85B0B09491E8C5AC::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208), -1, sizeof(Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2061[7] = 
{
	Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208_StaticFields::get_offset_of__instance_4(),
	Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208::get_offset_of_maxGroundAngle_5(),
	Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208::get_offset_of_maxCeilingAngle_6(),
	Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208::get_offset_of_replaceSegments_7(),
	Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208::get_offset_of_replaceGroundWith_8(),
	Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208::get_offset_of_replaceWallsWith_9(),
	Terrain2DArchitectureAngles_t804E8203AA786D1A464DF24D7EAFC66109AC4208::get_offset_of_replaceCeilingWith_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2062[8] = 
{
	SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1::get_offset_of_spline_0(),
	SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1::get_offset_of_arch_1(),
	SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1::get_offset_of_materialHolders_2(),
	SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1::get_offset_of_terrain_3(),
	0,
	SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1::get_offset_of_createdMeshes_5(),
	SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1::get_offset_of_createdMaterials_6(),
	SplineTerrain2DCreator_tB376515DA574676086ACA2E8C0914DDB27FDFBD1::get_offset_of_distancePerPoint_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D), -1, sizeof(Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2063[21] = 
{
	Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D::get_offset_of_scale_4(),
	Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D::get_offset_of_offset_5(),
	Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D::get_offset_of_textureInfoByType_6(),
	Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D::get_offset_of_infoGround_7(),
	Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D::get_offset_of_infoWall_8(),
	Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D::get_offset_of_infoCeiling_9(),
	Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D::get_offset_of_infoFill_10(),
	Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D_StaticFields::get_offset_of_infoEmpty_11(),
	Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D::get_offset_of_useCorners_12(),
	Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D::get_offset_of_cornersFollowsPath_13(),
	Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D::get_offset_of_cornerSprites_14(),
	Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D::get_offset_of_layersAndSorting_15(),
	Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D::get_offset_of_splitPaths_16(),
	Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D::get_offset_of_rotateTextures_17(),
	Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D::get_offset_of_material_18(),
	Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D::get_offset_of_settings_19(),
	Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D::get_offset_of_objectLayer_20(),
	Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D::get_offset_of_renderOrderType_21(),
	Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D::get_offset_of_z_depthSorting_22(),
	Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D::get_offset_of_ColorFill_23(),
	Terrain2DMaterial_tEEB7D9BF129459584A2806CF311886501F0EE44D::get_offset_of_fillType_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2064[25] = 
{
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_cornerDisplayType_0(),
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_interiorTopLeft_1(),
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_offsetITL_2(),
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_rotationITL_3(),
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_interiorTopRight_4(),
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_offsetITR_5(),
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_rotationITR_6(),
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_interiorBotLeft_7(),
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_offsetIBL_8(),
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_rotationIBL_9(),
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_interiorBotRight_10(),
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_offsetIBR_11(),
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_rotationIBR_12(),
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_exteriorTopLeft_13(),
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_offsetETL_14(),
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_rotationETL_15(),
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_exteriorTopRight_16(),
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_offsetETR_17(),
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_rotationETR_18(),
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_exteriorBotLeft_19(),
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_offsetEBL_20(),
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_exteriorBotRight_21(),
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_rotationEBL_22(),
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_offsetEBR_23(),
	CornerSprites2_t56F48A9FF2507A11F647BF6F8B9F6B163B5A1703::get_offset_of_rotationEBR_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (CornerDisplayType_t371ADA93330AF421D7EC2FBEB92BC9EA01C9815D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2065[3] = 
{
	CornerDisplayType_t371ADA93330AF421D7EC2FBEB92BC9EA01C9815D::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2066[10] = 
{
	SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE::get_offset_of_groundLayer_0(),
	SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE::get_offset_of_groundOrder_1(),
	SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE::get_offset_of_wallLayer_2(),
	SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE::get_offset_of_wallOrder_3(),
	SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE::get_offset_of_ceilingLayer_4(),
	SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE::get_offset_of_ceilingOrder_5(),
	SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE::get_offset_of_fillLayer_6(),
	SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE::get_offset_of_fillOrder_7(),
	SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE::get_offset_of_cornersLayer_8(),
	SortingLayerOptions2_tBAC224D6E96BE8EB7A2B1CCA4A3B7623FB2786AE::get_offset_of_cornerOrder_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2067[5] = 
{
	SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F::get_offset_of_groundZ_0(),
	SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F::get_offset_of_wallZ_1(),
	SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F::get_offset_of_ceilingZ_2(),
	SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F::get_offset_of_fillZ_3(),
	SortingZ_3D_Options_tFCB5831CDF4A89C019D48D3C8BF6366EFFC7A91F::get_offset_of_cornerZ_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (RenderOrderType_t080E9E94A764E427B39096E25C800721F2FDE98A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2068[3] = 
{
	RenderOrderType_t080E9E94A764E427B39096E25C800721F2FDE98A::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (CornerType_tB72616AAD3B5283135F2E892B87F36ECEB748AE7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2069[6] = 
{
	CornerType_tB72616AAD3B5283135F2E892B87F36ECEB748AE7::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (CornerSide_t8D5E50DDEDB6E82A23FCCFA8EFAD995D237D95FC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2070[4] = 
{
	CornerSide_t8D5E50DDEDB6E82A23FCCFA8EFAD995D237D95FC::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (FillType_tBDFB72417EBD74C72808826851F60D35622EFD6E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2071[4] = 
{
	FillType_tBDFB72417EBD74C72808826851F60D35622EFD6E::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (Poly2Mesh_tF3734BC685ACFDF1B9EF78ED4A5549AE34589AE3), -1, sizeof(Poly2Mesh_tF3734BC685ACFDF1B9EF78ED4A5549AE34589AE3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2072[1] = 
{
	Poly2Mesh_tF3734BC685ACFDF1B9EF78ED4A5549AE34589AE3_StaticFields::get_offset_of_fullDebug_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (Polygon_t094AFEF6E70FF97C794634E7C64A409BC965C0DC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2073[6] = 
{
	Polygon_t094AFEF6E70FF97C794634E7C64A409BC965C0DC::get_offset_of_outside_0(),
	Polygon_t094AFEF6E70FF97C794634E7C64A409BC965C0DC::get_offset_of_holes_1(),
	Polygon_t094AFEF6E70FF97C794634E7C64A409BC965C0DC::get_offset_of_outsideUVs_2(),
	Polygon_t094AFEF6E70FF97C794634E7C64A409BC965C0DC::get_offset_of_holesUVs_3(),
	Polygon_t094AFEF6E70FF97C794634E7C64A409BC965C0DC::get_offset_of_planeNormal_4(),
	Polygon_t094AFEF6E70FF97C794634E7C64A409BC965C0DC::get_offset_of_rotation_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (LayerAttribute_t01EB463B71CDEFB2B871266893F7BB44157A1E3E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2075[7] = 
{
	MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF::get_offset_of_sprite_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF::get_offset_of_gameObj_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF::get_offset_of_scale_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF::get_offset_of_offset_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF::get_offset_of_rotationAngle_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF::get_offset_of_orderInLayer_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialAssetInfo_tF3EB527FA4D5B7601D4FA0D196B981502A0E1FBF::get_offset_of_layer2D_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2076[6] = 
{
	MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F::get_offset_of_sprite_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F::get_offset_of_rotation_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F::get_offset_of_stretchType_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F::get_offset_of_stretchValue_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F::get_offset_of_scale_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialTextureInfo_t0706FA1E7C1AF776E7DFB93EE36ADB3BA00E081F::get_offset_of_offset_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (RotationMode_t931B059AC9C559A27F74EAD84F6702CCEA711106)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2077[9] = 
{
	RotationMode_t931B059AC9C559A27F74EAD84F6702CCEA711106::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (SpriteStretchType_tE3931A80B38F8AE955979B13406A9B99BE0F570D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2078[3] = 
{
	SpriteStretchType_tE3931A80B38F8AE955979B13406A9B99BE0F570D::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (Utils_t6B1F49395E2728FC7087CA82F420C8183CECBD71), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2079[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2), -1, sizeof(MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2080[8] = 
{
	MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields::get_offset_of_vertices_0(),
	MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields::get_offset_of_normals_1(),
	MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields::get_offset_of_colors_2(),
	MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields::get_offset_of_uv_3(),
	MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields::get_offset_of_uv1_4(),
	MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields::get_offset_of_uv2_5(),
	MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields::get_offset_of_indices_6(),
	MeshHelper2_t51BC2BE07E689574784F85C458B76F5361726DC2_StaticFields::get_offset_of_newVectices_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8), -1, sizeof(SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2081[63] = 
{
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_onEnterEvent_8(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_onExitEvent_9(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_depth_10(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_edgecount_11(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_nodecount_12(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_x_13(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_y_14(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_velocities_15(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_accelerations_16(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_baseVertices_17(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_meshVertices_18(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_normals_19(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_mesh_20(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_filter_21(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_meshObject_22(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_meshRenderer_23(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_polygonCollider_24(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_material_25(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_copyMaterial_26(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_textureInfo_27(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_orderInLayer_28(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_sortingLayer_29(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_slowdownOnEnterOrLeave_30(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_edgeMaterial_31(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_line_32(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_onEnter_33(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_onExit_34(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_mass_35(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_spread_36(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_springconstant_37(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_damping_38(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_forceMultiplier_39(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_forceThreshold_40(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_forceClampLow_41(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_forceClampHigh_42(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_waveLimitTop_43(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_waveLimitDistance_44(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_forceOnEdgesTop_45(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_forceOnEdgesBottom_46(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_edgeSize_47(),
	0,
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_physicsEnabled_49(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_stayOnEdge_50(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_forceDirection_51(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_forceMagnitude_52(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_floatThreshold_53(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_waterDensity_54(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_forceDependsOnDistance_55(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_forceProportionalToMass_56(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_touchingObjects_57(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_collidersObject_58(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_touchingColliders_59(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_innerColliders_60(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_innerGOs_61(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_pointsPerUnit_62(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_delta_63(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_lengths_64(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_lengths01_65(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_points_66(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_tangentDirs_67(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_testPanel_68(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8_StaticFields::get_offset_of_flatNormal_69(),
	SplineWater_t28C6E17AE3D1A4CEC52AA4C60BCB2B2571CAF1A8::get_offset_of_meshSegmentType_70(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (WaterEvent_t414CD81AE9F28B656088F21ECC14F030B1FDA33B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (ForceDirection_t92D92333E954731283BD44FF051EF85F9FB8413D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2083[3] = 
{
	ForceDirection_t92D92333E954731283BD44FF051EF85F9FB8413D::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (MeshSegmentType_tB60582871C7DE1B80274E63C8FCA2A0E90054B5D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2084[3] = 
{
	MeshSegmentType_tB60582871C7DE1B80274E63C8FCA2A0E90054B5D::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2085[6] = 
{
	WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80::get_offset_of_water_4(),
	WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80::get_offset_of_lastForceRaw_5(),
	WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80::get_offset_of_lastForceAfterMultiplier_6(),
	WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80::get_offset_of_lastForceIgnored_7(),
	WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80::get_offset_of_lastForceClamped_8(),
	WaterTestPanel_tE3F44AF75B167F1003B7110B16C9ACE0D09EDC80::get_offset_of_lastForceAfterClamp_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2086[5] = 
{
	DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278::get_offset_of_Points_0(),
	DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278::get_offset_of_Neighbors_1(),
	DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278::get_offset_of_mEdgeIsConstrained_2(),
	DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278::get_offset_of_EdgeIsDelaunay_3(),
	DelaunayTriangle_tA052B6ED441F17213517B154298074815CA75278::get_offset_of_U3CIsInteriorU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (AdvancingFront_tBCCD1124A9FAD23592008C9B975AA05853B39C44), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2087[3] = 
{
	AdvancingFront_tBCCD1124A9FAD23592008C9B975AA05853B39C44::get_offset_of_Head_0(),
	AdvancingFront_tBCCD1124A9FAD23592008C9B975AA05853B39C44::get_offset_of_Tail_1(),
	AdvancingFront_tBCCD1124A9FAD23592008C9B975AA05853B39C44::get_offset_of_Search_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2088[5] = 
{
	AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357::get_offset_of_Next_0(),
	AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357::get_offset_of_Prev_1(),
	AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357::get_offset_of_Value_2(),
	AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357::get_offset_of_Point_3(),
	AdvancingFrontNode_t8AE38AAE0D87C80EC9F408D43A39F0F4125BF357::get_offset_of_Triangle_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (DTSweep_t34A4AE5A6C924CB276FD4C28198A27B3A251BCED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (DTSweepBasin_t47769C4C8A6508E384E73DCD2ED888AEA2A782CD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2090[5] = 
{
	DTSweepBasin_t47769C4C8A6508E384E73DCD2ED888AEA2A782CD::get_offset_of_leftNode_0(),
	DTSweepBasin_t47769C4C8A6508E384E73DCD2ED888AEA2A782CD::get_offset_of_bottomNode_1(),
	DTSweepBasin_t47769C4C8A6508E384E73DCD2ED888AEA2A782CD::get_offset_of_rightNode_2(),
	DTSweepBasin_t47769C4C8A6508E384E73DCD2ED888AEA2A782CD::get_offset_of_width_3(),
	DTSweepBasin_t47769C4C8A6508E384E73DCD2ED888AEA2A782CD::get_offset_of_leftHighest_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (DTSweepConstraint_t4773D59AAEBA0883F8DA0C42FBD8C92350DBE594), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (DTSweepContext_t8FD4FC643B4645CD9771CF4E3631D75404EE2D06), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2092[7] = 
{
	DTSweepContext_t8FD4FC643B4645CD9771CF4E3631D75404EE2D06::get_offset_of_ALPHA_7(),
	DTSweepContext_t8FD4FC643B4645CD9771CF4E3631D75404EE2D06::get_offset_of_Front_8(),
	DTSweepContext_t8FD4FC643B4645CD9771CF4E3631D75404EE2D06::get_offset_of_U3CHeadU3Ek__BackingField_9(),
	DTSweepContext_t8FD4FC643B4645CD9771CF4E3631D75404EE2D06::get_offset_of_U3CTailU3Ek__BackingField_10(),
	DTSweepContext_t8FD4FC643B4645CD9771CF4E3631D75404EE2D06::get_offset_of_Basin_11(),
	DTSweepContext_t8FD4FC643B4645CD9771CF4E3631D75404EE2D06::get_offset_of_EdgeEvent_12(),
	DTSweepContext_t8FD4FC643B4645CD9771CF4E3631D75404EE2D06::get_offset_of__comparator_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (DTSweepDebugContext_tB6641E7B9644367EFA38339F39BEFE4B8403746F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2093[5] = 
{
	DTSweepDebugContext_tB6641E7B9644367EFA38339F39BEFE4B8403746F::get_offset_of__primaryTriangle_1(),
	DTSweepDebugContext_tB6641E7B9644367EFA38339F39BEFE4B8403746F::get_offset_of__secondaryTriangle_2(),
	DTSweepDebugContext_tB6641E7B9644367EFA38339F39BEFE4B8403746F::get_offset_of__activePoint_3(),
	DTSweepDebugContext_tB6641E7B9644367EFA38339F39BEFE4B8403746F::get_offset_of__activeNode_4(),
	DTSweepDebugContext_tB6641E7B9644367EFA38339F39BEFE4B8403746F::get_offset_of__activeConstraint_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (DTSweepEdgeEvent_t2BF104D3D9F78FA8F6BB2F1B63E5FFAAA9D446BC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2094[2] = 
{
	DTSweepEdgeEvent_t2BF104D3D9F78FA8F6BB2F1B63E5FFAAA9D446BC::get_offset_of_ConstrainedEdge_0(),
	DTSweepEdgeEvent_t2BF104D3D9F78FA8F6BB2F1B63E5FFAAA9D446BC::get_offset_of_Right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (DTSweepPointComparator_t030E4A9D87B78D989BCE53C4756FBC3AA4D7EA1B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (PointOnEdgeException_t410ABBD0AEDC8BEAD316BEC504857280BEC63802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2096[3] = 
{
	PointOnEdgeException_t410ABBD0AEDC8BEAD316BEC504857280BEC63802::get_offset_of_A_11(),
	PointOnEdgeException_t410ABBD0AEDC8BEAD316BEC504857280BEC63802::get_offset_of_B_12(),
	PointOnEdgeException_t410ABBD0AEDC8BEAD316BEC504857280BEC63802::get_offset_of_C_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (Orientation_t644FD42C9B5AA65D53079A04EBF0ACABF42D779D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2098[4] = 
{
	Orientation_t644FD42C9B5AA65D53079A04EBF0ACABF42D779D::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (Polygon_t3EF76BF4F85438E756235CCCE01F1DC3B5E2C99D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2099[6] = 
{
	Polygon_t3EF76BF4F85438E756235CCCE01F1DC3B5E2C99D::get_offset_of_mPointMap_7(),
	Polygon_t3EF76BF4F85438E756235CCCE01F1DC3B5E2C99D::get_offset_of_mTriangles_8(),
	Polygon_t3EF76BF4F85438E756235CCCE01F1DC3B5E2C99D::get_offset_of_mPrecision_9(),
	Polygon_t3EF76BF4F85438E756235CCCE01F1DC3B5E2C99D::get_offset_of_mHoles_10(),
	Polygon_t3EF76BF4F85438E756235CCCE01F1DC3B5E2C99D::get_offset_of_mSteinerPoints_11(),
	Polygon_t3EF76BF4F85438E756235CCCE01F1DC3B5E2C99D::get_offset_of__last_12(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
