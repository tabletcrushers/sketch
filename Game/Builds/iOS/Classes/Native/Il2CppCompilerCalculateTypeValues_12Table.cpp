﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Mono.Security.X509.X509Certificate
struct X509Certificate_t30E33DC87BDE409F02CF57214977DF8381792864;
// Mono.Security.X509.X509Certificate
struct X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7;
// Mono.Security.X509.X509Store
struct X509Store_t754E7C7E6C15F74AA8E7A3F0695EE7E1AD0C4302;
// System.AsyncCallback
struct AsyncCallback_t74ABD1277F711E7FBDCB00E169A63DEFD39E86A2;
// System.Boolean[]
struct BooleanU5BU5D_tBFF3659658EB283EF564C8FFB3A1676CEDE2E66E;
// System.Byte[]
struct ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8;
// System.Char[]
struct CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744;
// System.Collections.ArrayList
struct ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t7C989490AC01A6A5F45395A5CA72FAF4B7D59FF2;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1;
// System.Collections.Hashtable
struct Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82;
// System.Collections.IComparer
struct IComparer_t971EE8726C43A8A086B35A11585E2AEFF2F7C2EB;
// System.Collections.IDictionary
struct IDictionary_tD35B9437F08BE98D1E0B295CC73C48E168CAB316;
// System.Collections.IEnumerator
struct IEnumerator_t5F4AD85C6EA424A50584F741049EA645DBD8EEFC;
// System.Collections.IEqualityComparer
struct IEqualityComparer_tFBE0A1A09BAE01058D6DE65DD4FE16C50CB8E781;
// System.Collections.IHashCodeProvider
struct IHashCodeProvider_t2302C769B9686FD002965B00B8A3704D828517D5;
// System.Collections.Queue
struct Queue_t2B066F5DA52AD958C3B9532F7C88339BA1427EAD;
// System.Collections.Specialized.HybridDictionary
struct HybridDictionary_t5AD529BFF21493C38235716C9AD62F1F7623C747;
// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
struct KeysCollection_t3A1987CE62992069C60E21F2F157579EFBB7FDDB;
// System.Collections.Specialized.NameObjectCollectionBase/_Item
struct _Item_tFD4BAC90D8E7FF5A3713C2052052BAC018E7CEF7;
// System.Collections.Stack
struct Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5;
// System.EventHandler
struct EventHandler_t743A59DD95D50590C5CC9EBBCAA45C7B753C897D;
// System.Exception
struct Exception_t;
// System.IAsyncResult
struct IAsyncResult_tDA33C24465239FB383C4C2CDAAC43B9AD3CB7F05;
// System.IO.MemoryStream
struct MemoryStream_tA2A6655CF733913D13B7AB22E4FF081CB92F5FF0;
// System.IO.Stream
struct Stream_tCFD27E43C18381861212C0288CACF467FB602009;
// System.Int32[]
struct Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074;
// System.IntPtr[]
struct IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659;
// System.Net.BindIPEndPoint
struct BindIPEndPoint_tC8DC0C67D3B4E2155A6ADD8956CBA19A6E02A148;
// System.Net.ChunkStream
struct ChunkStream_t582E82A7D21D03B7A5AF6B1F913012451C28E40F;
// System.Net.HttpWebRequest
struct HttpWebRequest_t03D495C58ABDF0A533A2DABD6D7FDECD6507383E;
// System.Net.HttpWebResponse
struct HttpWebResponse_tC344CEBA03A8B5242160CA3DCC3BB736736F32B7;
// System.Net.ICertificatePolicy
struct ICertificatePolicy_tE6EE9D5AA2222816C6459598D44C4E1909C9BC4C;
// System.Net.ICredentials
struct ICredentials_tD0AB83297BDF32EE92C413F575F62DD096386B58;
// System.Net.IPAddress
struct IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4;
// System.Net.IPAddress[]
struct IPAddressU5BU5D_t7F5ACE8937120A82DD707759F81D2015550DD7DE;
// System.Net.IPHostEntry
struct IPHostEntry_t1EDA9F93303E37E89B6FFDEBD50858A54D423D9E;
// System.Net.IWebProxy
struct IWebProxy_t7E44658B5DCD492983886B8614CE96464347062A;
// System.Net.NetworkCredential
struct NetworkCredential_t82282628121911251495C07B1DA96F1D09367506;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t1276D648C0AECBBE8AF2790937BA182B64D3A489;
// System.Net.ServicePoint
struct ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3;
// System.Net.Sockets.Socket
struct Socket_t78B0CF49A51C5330826C9D2F0D0BF7B959FE2C03;
// System.Net.WebConnection
struct WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28;
// System.Net.WebConnection/AbortHelper
struct AbortHelper_tD2C5F18F172369ACDFA4655DEB034CD04578E6C8;
// System.Net.WebConnectionData
struct WebConnectionData_tDAAF22601B03B750DC57CB05255D88DF0A21DEDA;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_t01C9818A1AB6381C83A59AD104E8F0092AC87B66;
// System.Net.WebResponse
struct WebResponse_t5E9F05BAC005D2105A122CD5973E1D70B9B45325;
// System.Random
struct Random_tE77EE3BF254F50AC5B022C695F4F1DD99B425AE6;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t3C5F8BAEFAC94CF69694273ACCB6CB41355E0B5C;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t778922F6A5AACC38C8F326D3338A91D6D72B11E2;
// System.Security.Cryptography.AsnEncodedData
struct AsnEncodedData_t1B888B0D340736ECF8B35405760DC0FE62235A99;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_tCE2A08658EFEA671D977BC39FA246B4FC5092BB2;
// System.Security.Cryptography.Oid
struct Oid_tF92DE54931499E6C4157FE23D6706AD98E1FDB27;
// System.Security.Cryptography.OidCollection
struct OidCollection_tDA5968A49C496611C36F3E30F8A675D6F57E08D0;
// System.Security.Cryptography.X509Certificates.PublicKey
struct PublicKey_t88CC203B5F644C95B6142BB541AF0D447D2727AB;
// System.Security.Cryptography.X509Certificates.X500DistinguishedName
struct X500DistinguishedName_t4E51A8B638BD9EFBFC71A78F0263716509B878C9;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_tC6D94A8A63C0640494BE76372C6E4C291D2826DE;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5;
// System.Security.Cryptography.X509Certificates.X509Certificate2Collection
struct X509Certificate2Collection_t5F3686D1CB11C6BD038CE9D964F5A4B9BCDF532E;
// System.Security.Cryptography.X509Certificates.X509ChainElement
struct X509ChainElement_tAA74792F43074F2F75B321787C15A7E4388B39A9;
// System.Security.Cryptography.X509Certificates.X509ChainElementCollection
struct X509ChainElementCollection_tDA6F4291F55CC8640B2097C37F408A211B38E4A9;
// System.Security.Cryptography.X509Certificates.X509ChainPolicy
struct X509ChainPolicy_t50512486AAC7530381A567A301A6C39722FF00B4;
// System.Security.Cryptography.X509Certificates.X509ChainStatus[]
struct X509ChainStatusU5BU5D_t6789D0235F31EBCFB0491B55E859CF75FEB806AD;
// System.Security.Cryptography.X509Certificates.X509ExtensionCollection
struct X509ExtensionCollection_tE5717291A64EA1198FD91903AC1E8BFC32BDEC3D;
// System.Security.Cryptography.X509Certificates.X509Store
struct X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B;
// System.Text.RegularExpressions.CaptureCollection
struct CaptureCollection_t5F80E90F8954A443D29D1EBEFC7119831DFB9C20;
// System.Text.RegularExpressions.Capture[]
struct CaptureU5BU5D_tEEED727F6F99D0CF7F24F24E74A8ADD4DBF7E8D3;
// System.Text.RegularExpressions.FactoryCache
struct FactoryCache_t8BE3083C5F3C2185BA9AE97F4886454CB85C06B9;
// System.Text.RegularExpressions.GroupCollection
struct GroupCollection_tBEA4EDB210F04963227CBC58E1434202F3CEC968;
// System.Text.RegularExpressions.Group[]
struct GroupU5BU5D_tD5F0C5F00520A3213D8E6F225B1CF51985D2ACFE;
// System.Text.RegularExpressions.IMachine
struct IMachine_tB6A67B7F7CBCEBFB1EE3CDF0D00B88CD8443A776;
// System.Text.RegularExpressions.IMachineFactory
struct IMachineFactory_t44F4CC962A53B1F37D93579205124A16FD0B1471;
// System.Text.RegularExpressions.MRUList
struct MRUList_t54E602EB72FB2B58075AB7BF8F013121606B1E1C;
// System.Text.RegularExpressions.MRUList/Node
struct Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3;
// System.Text.RegularExpressions.Match
struct Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53;
// System.Text.RegularExpressions.MatchCollection
struct MatchCollection_tD651B345093D8CEC4F8E3302458C17355E017B2E;
// System.Text.RegularExpressions.Regex
struct Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B;
// System.Threading.ManualResetEvent
struct ManualResetEvent_tAFDF6453D9A7F8C3A557771B35E4BBA6DA882DBA;
// System.Threading.WaitCallback
struct WaitCallback_t55F4591597BB1B3F1CC6CEB6C34E712FDF46FD2B;
// System.Type
struct Type_t;
// System.UInt16[]
struct UInt16U5BU5D_tFACC39F5AB94FCCEAD5ED082922B0263437F9ABC;
// System.Uri
struct Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E;
// System.Version
struct Version_tD7D21D8F6BE9CC6473050418AB3D72BA2DB68D15;
// System.Void
struct Void_tDB81A15FA2AB53E2401A76B745D215397B29F783;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef COLLECTIONBASE_TC3F24120B23471E7FDEE72107D1D541E6456744F_H
#define COLLECTIONBASE_TC3F24120B23471E7FDEE72107D1D541E6456744F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.CollectionBase
struct  CollectionBase_tC3F24120B23471E7FDEE72107D1D541E6456744F  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.CollectionBase::list
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(CollectionBase_tC3F24120B23471E7FDEE72107D1D541E6456744F, ___list_0)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_list_0() const { return ___list_0; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONBASE_TC3F24120B23471E7FDEE72107D1D541E6456744F_H
#ifndef NAMEOBJECTCOLLECTIONBASE_TC2EE4FB130214FAE7365D519959A2A34DE56E070_H
#define NAMEOBJECTCOLLECTIONBASE_TC2EE4FB130214FAE7365D519959A2A34DE56E070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameObjectCollectionBase
struct  NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Collections.Specialized.NameObjectCollectionBase::m_ItemsContainer
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___m_ItemsContainer_0;
	// System.Collections.Specialized.NameObjectCollectionBase/_Item System.Collections.Specialized.NameObjectCollectionBase::m_NullKeyItem
	_Item_tFD4BAC90D8E7FF5A3713C2052052BAC018E7CEF7 * ___m_NullKeyItem_1;
	// System.Collections.ArrayList System.Collections.Specialized.NameObjectCollectionBase::m_ItemsArray
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___m_ItemsArray_2;
	// System.Collections.IHashCodeProvider System.Collections.Specialized.NameObjectCollectionBase::m_hashprovider
	RuntimeObject* ___m_hashprovider_3;
	// System.Collections.IComparer System.Collections.Specialized.NameObjectCollectionBase::m_comparer
	RuntimeObject* ___m_comparer_4;
	// System.Int32 System.Collections.Specialized.NameObjectCollectionBase::m_defCapacity
	int32_t ___m_defCapacity_5;
	// System.Boolean System.Collections.Specialized.NameObjectCollectionBase::m_readonly
	bool ___m_readonly_6;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Specialized.NameObjectCollectionBase::infoCopy
	SerializationInfo_t778922F6A5AACC38C8F326D3338A91D6D72B11E2 * ___infoCopy_7;
	// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection System.Collections.Specialized.NameObjectCollectionBase::keyscoll
	KeysCollection_t3A1987CE62992069C60E21F2F157579EFBB7FDDB * ___keyscoll_8;
	// System.Collections.IEqualityComparer System.Collections.Specialized.NameObjectCollectionBase::equality_comparer
	RuntimeObject* ___equality_comparer_9;

public:
	inline static int32_t get_offset_of_m_ItemsContainer_0() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___m_ItemsContainer_0)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_m_ItemsContainer_0() const { return ___m_ItemsContainer_0; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_m_ItemsContainer_0() { return &___m_ItemsContainer_0; }
	inline void set_m_ItemsContainer_0(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___m_ItemsContainer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemsContainer_0), value);
	}

	inline static int32_t get_offset_of_m_NullKeyItem_1() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___m_NullKeyItem_1)); }
	inline _Item_tFD4BAC90D8E7FF5A3713C2052052BAC018E7CEF7 * get_m_NullKeyItem_1() const { return ___m_NullKeyItem_1; }
	inline _Item_tFD4BAC90D8E7FF5A3713C2052052BAC018E7CEF7 ** get_address_of_m_NullKeyItem_1() { return &___m_NullKeyItem_1; }
	inline void set_m_NullKeyItem_1(_Item_tFD4BAC90D8E7FF5A3713C2052052BAC018E7CEF7 * value)
	{
		___m_NullKeyItem_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_NullKeyItem_1), value);
	}

	inline static int32_t get_offset_of_m_ItemsArray_2() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___m_ItemsArray_2)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_m_ItemsArray_2() const { return ___m_ItemsArray_2; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_m_ItemsArray_2() { return &___m_ItemsArray_2; }
	inline void set_m_ItemsArray_2(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___m_ItemsArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemsArray_2), value);
	}

	inline static int32_t get_offset_of_m_hashprovider_3() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___m_hashprovider_3)); }
	inline RuntimeObject* get_m_hashprovider_3() const { return ___m_hashprovider_3; }
	inline RuntimeObject** get_address_of_m_hashprovider_3() { return &___m_hashprovider_3; }
	inline void set_m_hashprovider_3(RuntimeObject* value)
	{
		___m_hashprovider_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_hashprovider_3), value);
	}

	inline static int32_t get_offset_of_m_comparer_4() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___m_comparer_4)); }
	inline RuntimeObject* get_m_comparer_4() const { return ___m_comparer_4; }
	inline RuntimeObject** get_address_of_m_comparer_4() { return &___m_comparer_4; }
	inline void set_m_comparer_4(RuntimeObject* value)
	{
		___m_comparer_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_comparer_4), value);
	}

	inline static int32_t get_offset_of_m_defCapacity_5() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___m_defCapacity_5)); }
	inline int32_t get_m_defCapacity_5() const { return ___m_defCapacity_5; }
	inline int32_t* get_address_of_m_defCapacity_5() { return &___m_defCapacity_5; }
	inline void set_m_defCapacity_5(int32_t value)
	{
		___m_defCapacity_5 = value;
	}

	inline static int32_t get_offset_of_m_readonly_6() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___m_readonly_6)); }
	inline bool get_m_readonly_6() const { return ___m_readonly_6; }
	inline bool* get_address_of_m_readonly_6() { return &___m_readonly_6; }
	inline void set_m_readonly_6(bool value)
	{
		___m_readonly_6 = value;
	}

	inline static int32_t get_offset_of_infoCopy_7() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___infoCopy_7)); }
	inline SerializationInfo_t778922F6A5AACC38C8F326D3338A91D6D72B11E2 * get_infoCopy_7() const { return ___infoCopy_7; }
	inline SerializationInfo_t778922F6A5AACC38C8F326D3338A91D6D72B11E2 ** get_address_of_infoCopy_7() { return &___infoCopy_7; }
	inline void set_infoCopy_7(SerializationInfo_t778922F6A5AACC38C8F326D3338A91D6D72B11E2 * value)
	{
		___infoCopy_7 = value;
		Il2CppCodeGenWriteBarrier((&___infoCopy_7), value);
	}

	inline static int32_t get_offset_of_keyscoll_8() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___keyscoll_8)); }
	inline KeysCollection_t3A1987CE62992069C60E21F2F157579EFBB7FDDB * get_keyscoll_8() const { return ___keyscoll_8; }
	inline KeysCollection_t3A1987CE62992069C60E21F2F157579EFBB7FDDB ** get_address_of_keyscoll_8() { return &___keyscoll_8; }
	inline void set_keyscoll_8(KeysCollection_t3A1987CE62992069C60E21F2F157579EFBB7FDDB * value)
	{
		___keyscoll_8 = value;
		Il2CppCodeGenWriteBarrier((&___keyscoll_8), value);
	}

	inline static int32_t get_offset_of_equality_comparer_9() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___equality_comparer_9)); }
	inline RuntimeObject* get_equality_comparer_9() const { return ___equality_comparer_9; }
	inline RuntimeObject** get_address_of_equality_comparer_9() { return &___equality_comparer_9; }
	inline void set_equality_comparer_9(RuntimeObject* value)
	{
		___equality_comparer_9 = value;
		Il2CppCodeGenWriteBarrier((&___equality_comparer_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEOBJECTCOLLECTIONBASE_TC2EE4FB130214FAE7365D519959A2A34DE56E070_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef STREAM_TCFD27E43C18381861212C0288CACF467FB602009_H
#define STREAM_TCFD27E43C18381861212C0288CACF467FB602009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tCFD27E43C18381861212C0288CACF467FB602009  : public RuntimeObject
{
public:

public:
};

struct Stream_tCFD27E43C18381861212C0288CACF467FB602009_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tCFD27E43C18381861212C0288CACF467FB602009 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_tCFD27E43C18381861212C0288CACF467FB602009_StaticFields, ___Null_0)); }
	inline Stream_tCFD27E43C18381861212C0288CACF467FB602009 * get_Null_0() const { return ___Null_0; }
	inline Stream_tCFD27E43C18381861212C0288CACF467FB602009 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_tCFD27E43C18381861212C0288CACF467FB602009 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TCFD27E43C18381861212C0288CACF467FB602009_H
#ifndef MARSHALBYREFOBJECT_T05F62A8AC86E36BAE3063CA28097945DE9E179C4_H
#define MARSHALBYREFOBJECT_T05F62A8AC86E36BAE3063CA28097945DE9E179C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t05F62A8AC86E36BAE3063CA28097945DE9E179C4  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t3C5F8BAEFAC94CF69694273ACCB6CB41355E0B5C * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t05F62A8AC86E36BAE3063CA28097945DE9E179C4, ____identity_0)); }
	inline ServerIdentity_t3C5F8BAEFAC94CF69694273ACCB6CB41355E0B5C * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t3C5F8BAEFAC94CF69694273ACCB6CB41355E0B5C ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t3C5F8BAEFAC94CF69694273ACCB6CB41355E0B5C * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALBYREFOBJECT_T05F62A8AC86E36BAE3063CA28097945DE9E179C4_H
#ifndef ENDPOINT_T025B464C83C18FB6B01BBA3888ECD9C9168F8DB5_H
#define ENDPOINT_T025B464C83C18FB6B01BBA3888ECD9C9168F8DB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.EndPoint
struct  EndPoint_t025B464C83C18FB6B01BBA3888ECD9C9168F8DB5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDPOINT_T025B464C83C18FB6B01BBA3888ECD9C9168F8DB5_H
#ifndef IPHOSTENTRY_T1EDA9F93303E37E89B6FFDEBD50858A54D423D9E_H
#define IPHOSTENTRY_T1EDA9F93303E37E89B6FFDEBD50858A54D423D9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPHostEntry
struct  IPHostEntry_t1EDA9F93303E37E89B6FFDEBD50858A54D423D9E  : public RuntimeObject
{
public:
	// System.Net.IPAddress[] System.Net.IPHostEntry::addressList
	IPAddressU5BU5D_t7F5ACE8937120A82DD707759F81D2015550DD7DE* ___addressList_0;
	// System.String[] System.Net.IPHostEntry::aliases
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___aliases_1;
	// System.String System.Net.IPHostEntry::hostName
	String_t* ___hostName_2;

public:
	inline static int32_t get_offset_of_addressList_0() { return static_cast<int32_t>(offsetof(IPHostEntry_t1EDA9F93303E37E89B6FFDEBD50858A54D423D9E, ___addressList_0)); }
	inline IPAddressU5BU5D_t7F5ACE8937120A82DD707759F81D2015550DD7DE* get_addressList_0() const { return ___addressList_0; }
	inline IPAddressU5BU5D_t7F5ACE8937120A82DD707759F81D2015550DD7DE** get_address_of_addressList_0() { return &___addressList_0; }
	inline void set_addressList_0(IPAddressU5BU5D_t7F5ACE8937120A82DD707759F81D2015550DD7DE* value)
	{
		___addressList_0 = value;
		Il2CppCodeGenWriteBarrier((&___addressList_0), value);
	}

	inline static int32_t get_offset_of_aliases_1() { return static_cast<int32_t>(offsetof(IPHostEntry_t1EDA9F93303E37E89B6FFDEBD50858A54D423D9E, ___aliases_1)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_aliases_1() const { return ___aliases_1; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_aliases_1() { return &___aliases_1; }
	inline void set_aliases_1(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___aliases_1 = value;
		Il2CppCodeGenWriteBarrier((&___aliases_1), value);
	}

	inline static int32_t get_offset_of_hostName_2() { return static_cast<int32_t>(offsetof(IPHostEntry_t1EDA9F93303E37E89B6FFDEBD50858A54D423D9E, ___hostName_2)); }
	inline String_t* get_hostName_2() const { return ___hostName_2; }
	inline String_t** get_address_of_hostName_2() { return &___hostName_2; }
	inline void set_hostName_2(String_t* value)
	{
		___hostName_2 = value;
		Il2CppCodeGenWriteBarrier((&___hostName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPHOSTENTRY_T1EDA9F93303E37E89B6FFDEBD50858A54D423D9E_H
#ifndef IPV6ADDRESS_T87D61FD60FCF8C5C8ED78875ECB69F9CE1485618_H
#define IPV6ADDRESS_T87D61FD60FCF8C5C8ED78875ECB69F9CE1485618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPv6Address
struct  IPv6Address_t87D61FD60FCF8C5C8ED78875ECB69F9CE1485618  : public RuntimeObject
{
public:
	// System.UInt16[] System.Net.IPv6Address::address
	UInt16U5BU5D_tFACC39F5AB94FCCEAD5ED082922B0263437F9ABC* ___address_0;
	// System.Int32 System.Net.IPv6Address::prefixLength
	int32_t ___prefixLength_1;
	// System.Int64 System.Net.IPv6Address::scopeId
	int64_t ___scopeId_2;

public:
	inline static int32_t get_offset_of_address_0() { return static_cast<int32_t>(offsetof(IPv6Address_t87D61FD60FCF8C5C8ED78875ECB69F9CE1485618, ___address_0)); }
	inline UInt16U5BU5D_tFACC39F5AB94FCCEAD5ED082922B0263437F9ABC* get_address_0() const { return ___address_0; }
	inline UInt16U5BU5D_tFACC39F5AB94FCCEAD5ED082922B0263437F9ABC** get_address_of_address_0() { return &___address_0; }
	inline void set_address_0(UInt16U5BU5D_tFACC39F5AB94FCCEAD5ED082922B0263437F9ABC* value)
	{
		___address_0 = value;
		Il2CppCodeGenWriteBarrier((&___address_0), value);
	}

	inline static int32_t get_offset_of_prefixLength_1() { return static_cast<int32_t>(offsetof(IPv6Address_t87D61FD60FCF8C5C8ED78875ECB69F9CE1485618, ___prefixLength_1)); }
	inline int32_t get_prefixLength_1() const { return ___prefixLength_1; }
	inline int32_t* get_address_of_prefixLength_1() { return &___prefixLength_1; }
	inline void set_prefixLength_1(int32_t value)
	{
		___prefixLength_1 = value;
	}

	inline static int32_t get_offset_of_scopeId_2() { return static_cast<int32_t>(offsetof(IPv6Address_t87D61FD60FCF8C5C8ED78875ECB69F9CE1485618, ___scopeId_2)); }
	inline int64_t get_scopeId_2() const { return ___scopeId_2; }
	inline int64_t* get_address_of_scopeId_2() { return &___scopeId_2; }
	inline void set_scopeId_2(int64_t value)
	{
		___scopeId_2 = value;
	}
};

struct IPv6Address_t87D61FD60FCF8C5C8ED78875ECB69F9CE1485618_StaticFields
{
public:
	// System.Net.IPv6Address System.Net.IPv6Address::Loopback
	IPv6Address_t87D61FD60FCF8C5C8ED78875ECB69F9CE1485618 * ___Loopback_3;
	// System.Net.IPv6Address System.Net.IPv6Address::Unspecified
	IPv6Address_t87D61FD60FCF8C5C8ED78875ECB69F9CE1485618 * ___Unspecified_4;

public:
	inline static int32_t get_offset_of_Loopback_3() { return static_cast<int32_t>(offsetof(IPv6Address_t87D61FD60FCF8C5C8ED78875ECB69F9CE1485618_StaticFields, ___Loopback_3)); }
	inline IPv6Address_t87D61FD60FCF8C5C8ED78875ECB69F9CE1485618 * get_Loopback_3() const { return ___Loopback_3; }
	inline IPv6Address_t87D61FD60FCF8C5C8ED78875ECB69F9CE1485618 ** get_address_of_Loopback_3() { return &___Loopback_3; }
	inline void set_Loopback_3(IPv6Address_t87D61FD60FCF8C5C8ED78875ECB69F9CE1485618 * value)
	{
		___Loopback_3 = value;
		Il2CppCodeGenWriteBarrier((&___Loopback_3), value);
	}

	inline static int32_t get_offset_of_Unspecified_4() { return static_cast<int32_t>(offsetof(IPv6Address_t87D61FD60FCF8C5C8ED78875ECB69F9CE1485618_StaticFields, ___Unspecified_4)); }
	inline IPv6Address_t87D61FD60FCF8C5C8ED78875ECB69F9CE1485618 * get_Unspecified_4() const { return ___Unspecified_4; }
	inline IPv6Address_t87D61FD60FCF8C5C8ED78875ECB69F9CE1485618 ** get_address_of_Unspecified_4() { return &___Unspecified_4; }
	inline void set_Unspecified_4(IPv6Address_t87D61FD60FCF8C5C8ED78875ECB69F9CE1485618 * value)
	{
		___Unspecified_4 = value;
		Il2CppCodeGenWriteBarrier((&___Unspecified_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPV6ADDRESS_T87D61FD60FCF8C5C8ED78875ECB69F9CE1485618_H
#ifndef NETWORKCREDENTIAL_T82282628121911251495C07B1DA96F1D09367506_H
#define NETWORKCREDENTIAL_T82282628121911251495C07B1DA96F1D09367506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.NetworkCredential
struct  NetworkCredential_t82282628121911251495C07B1DA96F1D09367506  : public RuntimeObject
{
public:
	// System.String System.Net.NetworkCredential::userName
	String_t* ___userName_0;
	// System.String System.Net.NetworkCredential::password
	String_t* ___password_1;
	// System.String System.Net.NetworkCredential::domain
	String_t* ___domain_2;

public:
	inline static int32_t get_offset_of_userName_0() { return static_cast<int32_t>(offsetof(NetworkCredential_t82282628121911251495C07B1DA96F1D09367506, ___userName_0)); }
	inline String_t* get_userName_0() const { return ___userName_0; }
	inline String_t** get_address_of_userName_0() { return &___userName_0; }
	inline void set_userName_0(String_t* value)
	{
		___userName_0 = value;
		Il2CppCodeGenWriteBarrier((&___userName_0), value);
	}

	inline static int32_t get_offset_of_password_1() { return static_cast<int32_t>(offsetof(NetworkCredential_t82282628121911251495C07B1DA96F1D09367506, ___password_1)); }
	inline String_t* get_password_1() const { return ___password_1; }
	inline String_t** get_address_of_password_1() { return &___password_1; }
	inline void set_password_1(String_t* value)
	{
		___password_1 = value;
		Il2CppCodeGenWriteBarrier((&___password_1), value);
	}

	inline static int32_t get_offset_of_domain_2() { return static_cast<int32_t>(offsetof(NetworkCredential_t82282628121911251495C07B1DA96F1D09367506, ___domain_2)); }
	inline String_t* get_domain_2() const { return ___domain_2; }
	inline String_t** get_address_of_domain_2() { return &___domain_2; }
	inline void set_domain_2(String_t* value)
	{
		___domain_2 = value;
		Il2CppCodeGenWriteBarrier((&___domain_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKCREDENTIAL_T82282628121911251495C07B1DA96F1D09367506_H
#ifndef SPKEY_TC77E61EA2860044974117BBB287BD2160F46D0F1_H
#define SPKEY_TC77E61EA2860044974117BBB287BD2160F46D0F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServicePointManager/SPKey
struct  SPKey_tC77E61EA2860044974117BBB287BD2160F46D0F1  : public RuntimeObject
{
public:
	// System.Uri System.Net.ServicePointManager/SPKey::uri
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E * ___uri_0;
	// System.Boolean System.Net.ServicePointManager/SPKey::use_connect
	bool ___use_connect_1;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(SPKey_tC77E61EA2860044974117BBB287BD2160F46D0F1, ___uri_0)); }
	inline Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E * get_uri_0() const { return ___uri_0; }
	inline Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E ** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E * value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}

	inline static int32_t get_offset_of_use_connect_1() { return static_cast<int32_t>(offsetof(SPKey_tC77E61EA2860044974117BBB287BD2160F46D0F1, ___use_connect_1)); }
	inline bool get_use_connect_1() const { return ___use_connect_1; }
	inline bool* get_address_of_use_connect_1() { return &___use_connect_1; }
	inline void set_use_connect_1(bool value)
	{
		___use_connect_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPKEY_TC77E61EA2860044974117BBB287BD2160F46D0F1_H
#ifndef SOCKETADDRESS_T4A05CA2B1C8AD3D08F4001F64724F58812C385F5_H
#define SOCKETADDRESS_T4A05CA2B1C8AD3D08F4001F64724F58812C385F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SocketAddress
struct  SocketAddress_t4A05CA2B1C8AD3D08F4001F64724F58812C385F5  : public RuntimeObject
{
public:
	// System.Byte[] System.Net.SocketAddress::data
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___data_0;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(SocketAddress_t4A05CA2B1C8AD3D08F4001F64724F58812C385F5, ___data_0)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_data_0() const { return ___data_0; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETADDRESS_T4A05CA2B1C8AD3D08F4001F64724F58812C385F5_H
#ifndef WEBASYNCRESULT_T6CC7C854DE52CEA490C2EA28857A34E626F8E5E8_H
#define WEBASYNCRESULT_T6CC7C854DE52CEA490C2EA28857A34E626F8E5E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebAsyncResult
struct  WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8  : public RuntimeObject
{
public:
	// System.Threading.ManualResetEvent System.Net.WebAsyncResult::handle
	ManualResetEvent_tAFDF6453D9A7F8C3A557771B35E4BBA6DA882DBA * ___handle_0;
	// System.Boolean System.Net.WebAsyncResult::synch
	bool ___synch_1;
	// System.Boolean System.Net.WebAsyncResult::isCompleted
	bool ___isCompleted_2;
	// System.AsyncCallback System.Net.WebAsyncResult::cb
	AsyncCallback_t74ABD1277F711E7FBDCB00E169A63DEFD39E86A2 * ___cb_3;
	// System.Object System.Net.WebAsyncResult::state
	RuntimeObject * ___state_4;
	// System.Int32 System.Net.WebAsyncResult::nbytes
	int32_t ___nbytes_5;
	// System.IAsyncResult System.Net.WebAsyncResult::innerAsyncResult
	RuntimeObject* ___innerAsyncResult_6;
	// System.Boolean System.Net.WebAsyncResult::callbackDone
	bool ___callbackDone_7;
	// System.Exception System.Net.WebAsyncResult::exc
	Exception_t * ___exc_8;
	// System.Net.HttpWebResponse System.Net.WebAsyncResult::response
	HttpWebResponse_tC344CEBA03A8B5242160CA3DCC3BB736736F32B7 * ___response_9;
	// System.IO.Stream System.Net.WebAsyncResult::writeStream
	Stream_tCFD27E43C18381861212C0288CACF467FB602009 * ___writeStream_10;
	// System.Byte[] System.Net.WebAsyncResult::buffer
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___buffer_11;
	// System.Int32 System.Net.WebAsyncResult::offset
	int32_t ___offset_12;
	// System.Int32 System.Net.WebAsyncResult::size
	int32_t ___size_13;
	// System.Object System.Net.WebAsyncResult::locker
	RuntimeObject * ___locker_14;
	// System.Boolean System.Net.WebAsyncResult::EndCalled
	bool ___EndCalled_15;
	// System.Boolean System.Net.WebAsyncResult::AsyncWriteAll
	bool ___AsyncWriteAll_16;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8, ___handle_0)); }
	inline ManualResetEvent_tAFDF6453D9A7F8C3A557771B35E4BBA6DA882DBA * get_handle_0() const { return ___handle_0; }
	inline ManualResetEvent_tAFDF6453D9A7F8C3A557771B35E4BBA6DA882DBA ** get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(ManualResetEvent_tAFDF6453D9A7F8C3A557771B35E4BBA6DA882DBA * value)
	{
		___handle_0 = value;
		Il2CppCodeGenWriteBarrier((&___handle_0), value);
	}

	inline static int32_t get_offset_of_synch_1() { return static_cast<int32_t>(offsetof(WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8, ___synch_1)); }
	inline bool get_synch_1() const { return ___synch_1; }
	inline bool* get_address_of_synch_1() { return &___synch_1; }
	inline void set_synch_1(bool value)
	{
		___synch_1 = value;
	}

	inline static int32_t get_offset_of_isCompleted_2() { return static_cast<int32_t>(offsetof(WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8, ___isCompleted_2)); }
	inline bool get_isCompleted_2() const { return ___isCompleted_2; }
	inline bool* get_address_of_isCompleted_2() { return &___isCompleted_2; }
	inline void set_isCompleted_2(bool value)
	{
		___isCompleted_2 = value;
	}

	inline static int32_t get_offset_of_cb_3() { return static_cast<int32_t>(offsetof(WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8, ___cb_3)); }
	inline AsyncCallback_t74ABD1277F711E7FBDCB00E169A63DEFD39E86A2 * get_cb_3() const { return ___cb_3; }
	inline AsyncCallback_t74ABD1277F711E7FBDCB00E169A63DEFD39E86A2 ** get_address_of_cb_3() { return &___cb_3; }
	inline void set_cb_3(AsyncCallback_t74ABD1277F711E7FBDCB00E169A63DEFD39E86A2 * value)
	{
		___cb_3 = value;
		Il2CppCodeGenWriteBarrier((&___cb_3), value);
	}

	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8, ___state_4)); }
	inline RuntimeObject * get_state_4() const { return ___state_4; }
	inline RuntimeObject ** get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(RuntimeObject * value)
	{
		___state_4 = value;
		Il2CppCodeGenWriteBarrier((&___state_4), value);
	}

	inline static int32_t get_offset_of_nbytes_5() { return static_cast<int32_t>(offsetof(WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8, ___nbytes_5)); }
	inline int32_t get_nbytes_5() const { return ___nbytes_5; }
	inline int32_t* get_address_of_nbytes_5() { return &___nbytes_5; }
	inline void set_nbytes_5(int32_t value)
	{
		___nbytes_5 = value;
	}

	inline static int32_t get_offset_of_innerAsyncResult_6() { return static_cast<int32_t>(offsetof(WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8, ___innerAsyncResult_6)); }
	inline RuntimeObject* get_innerAsyncResult_6() const { return ___innerAsyncResult_6; }
	inline RuntimeObject** get_address_of_innerAsyncResult_6() { return &___innerAsyncResult_6; }
	inline void set_innerAsyncResult_6(RuntimeObject* value)
	{
		___innerAsyncResult_6 = value;
		Il2CppCodeGenWriteBarrier((&___innerAsyncResult_6), value);
	}

	inline static int32_t get_offset_of_callbackDone_7() { return static_cast<int32_t>(offsetof(WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8, ___callbackDone_7)); }
	inline bool get_callbackDone_7() const { return ___callbackDone_7; }
	inline bool* get_address_of_callbackDone_7() { return &___callbackDone_7; }
	inline void set_callbackDone_7(bool value)
	{
		___callbackDone_7 = value;
	}

	inline static int32_t get_offset_of_exc_8() { return static_cast<int32_t>(offsetof(WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8, ___exc_8)); }
	inline Exception_t * get_exc_8() const { return ___exc_8; }
	inline Exception_t ** get_address_of_exc_8() { return &___exc_8; }
	inline void set_exc_8(Exception_t * value)
	{
		___exc_8 = value;
		Il2CppCodeGenWriteBarrier((&___exc_8), value);
	}

	inline static int32_t get_offset_of_response_9() { return static_cast<int32_t>(offsetof(WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8, ___response_9)); }
	inline HttpWebResponse_tC344CEBA03A8B5242160CA3DCC3BB736736F32B7 * get_response_9() const { return ___response_9; }
	inline HttpWebResponse_tC344CEBA03A8B5242160CA3DCC3BB736736F32B7 ** get_address_of_response_9() { return &___response_9; }
	inline void set_response_9(HttpWebResponse_tC344CEBA03A8B5242160CA3DCC3BB736736F32B7 * value)
	{
		___response_9 = value;
		Il2CppCodeGenWriteBarrier((&___response_9), value);
	}

	inline static int32_t get_offset_of_writeStream_10() { return static_cast<int32_t>(offsetof(WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8, ___writeStream_10)); }
	inline Stream_tCFD27E43C18381861212C0288CACF467FB602009 * get_writeStream_10() const { return ___writeStream_10; }
	inline Stream_tCFD27E43C18381861212C0288CACF467FB602009 ** get_address_of_writeStream_10() { return &___writeStream_10; }
	inline void set_writeStream_10(Stream_tCFD27E43C18381861212C0288CACF467FB602009 * value)
	{
		___writeStream_10 = value;
		Il2CppCodeGenWriteBarrier((&___writeStream_10), value);
	}

	inline static int32_t get_offset_of_buffer_11() { return static_cast<int32_t>(offsetof(WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8, ___buffer_11)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_buffer_11() const { return ___buffer_11; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_buffer_11() { return &___buffer_11; }
	inline void set_buffer_11(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___buffer_11 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_11), value);
	}

	inline static int32_t get_offset_of_offset_12() { return static_cast<int32_t>(offsetof(WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8, ___offset_12)); }
	inline int32_t get_offset_12() const { return ___offset_12; }
	inline int32_t* get_address_of_offset_12() { return &___offset_12; }
	inline void set_offset_12(int32_t value)
	{
		___offset_12 = value;
	}

	inline static int32_t get_offset_of_size_13() { return static_cast<int32_t>(offsetof(WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8, ___size_13)); }
	inline int32_t get_size_13() const { return ___size_13; }
	inline int32_t* get_address_of_size_13() { return &___size_13; }
	inline void set_size_13(int32_t value)
	{
		___size_13 = value;
	}

	inline static int32_t get_offset_of_locker_14() { return static_cast<int32_t>(offsetof(WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8, ___locker_14)); }
	inline RuntimeObject * get_locker_14() const { return ___locker_14; }
	inline RuntimeObject ** get_address_of_locker_14() { return &___locker_14; }
	inline void set_locker_14(RuntimeObject * value)
	{
		___locker_14 = value;
		Il2CppCodeGenWriteBarrier((&___locker_14), value);
	}

	inline static int32_t get_offset_of_EndCalled_15() { return static_cast<int32_t>(offsetof(WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8, ___EndCalled_15)); }
	inline bool get_EndCalled_15() const { return ___EndCalled_15; }
	inline bool* get_address_of_EndCalled_15() { return &___EndCalled_15; }
	inline void set_EndCalled_15(bool value)
	{
		___EndCalled_15 = value;
	}

	inline static int32_t get_offset_of_AsyncWriteAll_16() { return static_cast<int32_t>(offsetof(WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8, ___AsyncWriteAll_16)); }
	inline bool get_AsyncWriteAll_16() const { return ___AsyncWriteAll_16; }
	inline bool* get_address_of_AsyncWriteAll_16() { return &___AsyncWriteAll_16; }
	inline void set_AsyncWriteAll_16(bool value)
	{
		___AsyncWriteAll_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBASYNCRESULT_T6CC7C854DE52CEA490C2EA28857A34E626F8E5E8_H
#ifndef ABORTHELPER_TD2C5F18F172369ACDFA4655DEB034CD04578E6C8_H
#define ABORTHELPER_TD2C5F18F172369ACDFA4655DEB034CD04578E6C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnection/AbortHelper
struct  AbortHelper_tD2C5F18F172369ACDFA4655DEB034CD04578E6C8  : public RuntimeObject
{
public:
	// System.Net.WebConnection System.Net.WebConnection/AbortHelper::Connection
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28 * ___Connection_0;

public:
	inline static int32_t get_offset_of_Connection_0() { return static_cast<int32_t>(offsetof(AbortHelper_tD2C5F18F172369ACDFA4655DEB034CD04578E6C8, ___Connection_0)); }
	inline WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28 * get_Connection_0() const { return ___Connection_0; }
	inline WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28 ** get_address_of_Connection_0() { return &___Connection_0; }
	inline void set_Connection_0(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28 * value)
	{
		___Connection_0 = value;
		Il2CppCodeGenWriteBarrier((&___Connection_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABORTHELPER_TD2C5F18F172369ACDFA4655DEB034CD04578E6C8_H
#ifndef WEBCONNECTIONDATA_TDAAF22601B03B750DC57CB05255D88DF0A21DEDA_H
#define WEBCONNECTIONDATA_TDAAF22601B03B750DC57CB05255D88DF0A21DEDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnectionData
struct  WebConnectionData_tDAAF22601B03B750DC57CB05255D88DF0A21DEDA  : public RuntimeObject
{
public:
	// System.Net.HttpWebRequest System.Net.WebConnectionData::request
	HttpWebRequest_t03D495C58ABDF0A533A2DABD6D7FDECD6507383E * ___request_0;
	// System.Int32 System.Net.WebConnectionData::StatusCode
	int32_t ___StatusCode_1;
	// System.String System.Net.WebConnectionData::StatusDescription
	String_t* ___StatusDescription_2;
	// System.Net.WebHeaderCollection System.Net.WebConnectionData::Headers
	WebHeaderCollection_t01C9818A1AB6381C83A59AD104E8F0092AC87B66 * ___Headers_3;
	// System.Version System.Net.WebConnectionData::Version
	Version_tD7D21D8F6BE9CC6473050418AB3D72BA2DB68D15 * ___Version_4;
	// System.IO.Stream System.Net.WebConnectionData::stream
	Stream_tCFD27E43C18381861212C0288CACF467FB602009 * ___stream_5;
	// System.String System.Net.WebConnectionData::Challenge
	String_t* ___Challenge_6;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(WebConnectionData_tDAAF22601B03B750DC57CB05255D88DF0A21DEDA, ___request_0)); }
	inline HttpWebRequest_t03D495C58ABDF0A533A2DABD6D7FDECD6507383E * get_request_0() const { return ___request_0; }
	inline HttpWebRequest_t03D495C58ABDF0A533A2DABD6D7FDECD6507383E ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(HttpWebRequest_t03D495C58ABDF0A533A2DABD6D7FDECD6507383E * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}

	inline static int32_t get_offset_of_StatusCode_1() { return static_cast<int32_t>(offsetof(WebConnectionData_tDAAF22601B03B750DC57CB05255D88DF0A21DEDA, ___StatusCode_1)); }
	inline int32_t get_StatusCode_1() const { return ___StatusCode_1; }
	inline int32_t* get_address_of_StatusCode_1() { return &___StatusCode_1; }
	inline void set_StatusCode_1(int32_t value)
	{
		___StatusCode_1 = value;
	}

	inline static int32_t get_offset_of_StatusDescription_2() { return static_cast<int32_t>(offsetof(WebConnectionData_tDAAF22601B03B750DC57CB05255D88DF0A21DEDA, ___StatusDescription_2)); }
	inline String_t* get_StatusDescription_2() const { return ___StatusDescription_2; }
	inline String_t** get_address_of_StatusDescription_2() { return &___StatusDescription_2; }
	inline void set_StatusDescription_2(String_t* value)
	{
		___StatusDescription_2 = value;
		Il2CppCodeGenWriteBarrier((&___StatusDescription_2), value);
	}

	inline static int32_t get_offset_of_Headers_3() { return static_cast<int32_t>(offsetof(WebConnectionData_tDAAF22601B03B750DC57CB05255D88DF0A21DEDA, ___Headers_3)); }
	inline WebHeaderCollection_t01C9818A1AB6381C83A59AD104E8F0092AC87B66 * get_Headers_3() const { return ___Headers_3; }
	inline WebHeaderCollection_t01C9818A1AB6381C83A59AD104E8F0092AC87B66 ** get_address_of_Headers_3() { return &___Headers_3; }
	inline void set_Headers_3(WebHeaderCollection_t01C9818A1AB6381C83A59AD104E8F0092AC87B66 * value)
	{
		___Headers_3 = value;
		Il2CppCodeGenWriteBarrier((&___Headers_3), value);
	}

	inline static int32_t get_offset_of_Version_4() { return static_cast<int32_t>(offsetof(WebConnectionData_tDAAF22601B03B750DC57CB05255D88DF0A21DEDA, ___Version_4)); }
	inline Version_tD7D21D8F6BE9CC6473050418AB3D72BA2DB68D15 * get_Version_4() const { return ___Version_4; }
	inline Version_tD7D21D8F6BE9CC6473050418AB3D72BA2DB68D15 ** get_address_of_Version_4() { return &___Version_4; }
	inline void set_Version_4(Version_tD7D21D8F6BE9CC6473050418AB3D72BA2DB68D15 * value)
	{
		___Version_4 = value;
		Il2CppCodeGenWriteBarrier((&___Version_4), value);
	}

	inline static int32_t get_offset_of_stream_5() { return static_cast<int32_t>(offsetof(WebConnectionData_tDAAF22601B03B750DC57CB05255D88DF0A21DEDA, ___stream_5)); }
	inline Stream_tCFD27E43C18381861212C0288CACF467FB602009 * get_stream_5() const { return ___stream_5; }
	inline Stream_tCFD27E43C18381861212C0288CACF467FB602009 ** get_address_of_stream_5() { return &___stream_5; }
	inline void set_stream_5(Stream_tCFD27E43C18381861212C0288CACF467FB602009 * value)
	{
		___stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___stream_5), value);
	}

	inline static int32_t get_offset_of_Challenge_6() { return static_cast<int32_t>(offsetof(WebConnectionData_tDAAF22601B03B750DC57CB05255D88DF0A21DEDA, ___Challenge_6)); }
	inline String_t* get_Challenge_6() const { return ___Challenge_6; }
	inline String_t** get_address_of_Challenge_6() { return &___Challenge_6; }
	inline void set_Challenge_6(String_t* value)
	{
		___Challenge_6 = value;
		Il2CppCodeGenWriteBarrier((&___Challenge_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCONNECTIONDATA_TDAAF22601B03B750DC57CB05255D88DF0A21DEDA_H
#ifndef WEBCONNECTIONGROUP_T096269003F5E1F05AEB6921F2B00E2F8FD66F7EF_H
#define WEBCONNECTIONGROUP_T096269003F5E1F05AEB6921F2B00E2F8FD66F7EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnectionGroup
struct  WebConnectionGroup_t096269003F5E1F05AEB6921F2B00E2F8FD66F7EF  : public RuntimeObject
{
public:
	// System.Net.ServicePoint System.Net.WebConnectionGroup::sPoint
	ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3 * ___sPoint_0;
	// System.String System.Net.WebConnectionGroup::name
	String_t* ___name_1;
	// System.Collections.ArrayList System.Net.WebConnectionGroup::connections
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___connections_2;
	// System.Random System.Net.WebConnectionGroup::rnd
	Random_tE77EE3BF254F50AC5B022C695F4F1DD99B425AE6 * ___rnd_3;
	// System.Collections.Queue System.Net.WebConnectionGroup::queue
	Queue_t2B066F5DA52AD958C3B9532F7C88339BA1427EAD * ___queue_4;

public:
	inline static int32_t get_offset_of_sPoint_0() { return static_cast<int32_t>(offsetof(WebConnectionGroup_t096269003F5E1F05AEB6921F2B00E2F8FD66F7EF, ___sPoint_0)); }
	inline ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3 * get_sPoint_0() const { return ___sPoint_0; }
	inline ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3 ** get_address_of_sPoint_0() { return &___sPoint_0; }
	inline void set_sPoint_0(ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3 * value)
	{
		___sPoint_0 = value;
		Il2CppCodeGenWriteBarrier((&___sPoint_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(WebConnectionGroup_t096269003F5E1F05AEB6921F2B00E2F8FD66F7EF, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_connections_2() { return static_cast<int32_t>(offsetof(WebConnectionGroup_t096269003F5E1F05AEB6921F2B00E2F8FD66F7EF, ___connections_2)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_connections_2() const { return ___connections_2; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_connections_2() { return &___connections_2; }
	inline void set_connections_2(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___connections_2 = value;
		Il2CppCodeGenWriteBarrier((&___connections_2), value);
	}

	inline static int32_t get_offset_of_rnd_3() { return static_cast<int32_t>(offsetof(WebConnectionGroup_t096269003F5E1F05AEB6921F2B00E2F8FD66F7EF, ___rnd_3)); }
	inline Random_tE77EE3BF254F50AC5B022C695F4F1DD99B425AE6 * get_rnd_3() const { return ___rnd_3; }
	inline Random_tE77EE3BF254F50AC5B022C695F4F1DD99B425AE6 ** get_address_of_rnd_3() { return &___rnd_3; }
	inline void set_rnd_3(Random_tE77EE3BF254F50AC5B022C695F4F1DD99B425AE6 * value)
	{
		___rnd_3 = value;
		Il2CppCodeGenWriteBarrier((&___rnd_3), value);
	}

	inline static int32_t get_offset_of_queue_4() { return static_cast<int32_t>(offsetof(WebConnectionGroup_t096269003F5E1F05AEB6921F2B00E2F8FD66F7EF, ___queue_4)); }
	inline Queue_t2B066F5DA52AD958C3B9532F7C88339BA1427EAD * get_queue_4() const { return ___queue_4; }
	inline Queue_t2B066F5DA52AD958C3B9532F7C88339BA1427EAD ** get_address_of_queue_4() { return &___queue_4; }
	inline void set_queue_4(Queue_t2B066F5DA52AD958C3B9532F7C88339BA1427EAD * value)
	{
		___queue_4 = value;
		Il2CppCodeGenWriteBarrier((&___queue_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCONNECTIONGROUP_T096269003F5E1F05AEB6921F2B00E2F8FD66F7EF_H
#ifndef WEBPROXY_T9793DD7C7F74F06D01ED3A0D331E2E9E1579CB9B_H
#define WEBPROXY_T9793DD7C7F74F06D01ED3A0D331E2E9E1579CB9B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebProxy
struct  WebProxy_t9793DD7C7F74F06D01ED3A0D331E2E9E1579CB9B  : public RuntimeObject
{
public:
	// System.Uri System.Net.WebProxy::address
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E * ___address_0;
	// System.Boolean System.Net.WebProxy::bypassOnLocal
	bool ___bypassOnLocal_1;
	// System.Collections.ArrayList System.Net.WebProxy::bypassList
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___bypassList_2;
	// System.Net.ICredentials System.Net.WebProxy::credentials
	RuntimeObject* ___credentials_3;
	// System.Boolean System.Net.WebProxy::useDefaultCredentials
	bool ___useDefaultCredentials_4;

public:
	inline static int32_t get_offset_of_address_0() { return static_cast<int32_t>(offsetof(WebProxy_t9793DD7C7F74F06D01ED3A0D331E2E9E1579CB9B, ___address_0)); }
	inline Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E * get_address_0() const { return ___address_0; }
	inline Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E ** get_address_of_address_0() { return &___address_0; }
	inline void set_address_0(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E * value)
	{
		___address_0 = value;
		Il2CppCodeGenWriteBarrier((&___address_0), value);
	}

	inline static int32_t get_offset_of_bypassOnLocal_1() { return static_cast<int32_t>(offsetof(WebProxy_t9793DD7C7F74F06D01ED3A0D331E2E9E1579CB9B, ___bypassOnLocal_1)); }
	inline bool get_bypassOnLocal_1() const { return ___bypassOnLocal_1; }
	inline bool* get_address_of_bypassOnLocal_1() { return &___bypassOnLocal_1; }
	inline void set_bypassOnLocal_1(bool value)
	{
		___bypassOnLocal_1 = value;
	}

	inline static int32_t get_offset_of_bypassList_2() { return static_cast<int32_t>(offsetof(WebProxy_t9793DD7C7F74F06D01ED3A0D331E2E9E1579CB9B, ___bypassList_2)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_bypassList_2() const { return ___bypassList_2; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_bypassList_2() { return &___bypassList_2; }
	inline void set_bypassList_2(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___bypassList_2 = value;
		Il2CppCodeGenWriteBarrier((&___bypassList_2), value);
	}

	inline static int32_t get_offset_of_credentials_3() { return static_cast<int32_t>(offsetof(WebProxy_t9793DD7C7F74F06D01ED3A0D331E2E9E1579CB9B, ___credentials_3)); }
	inline RuntimeObject* get_credentials_3() const { return ___credentials_3; }
	inline RuntimeObject** get_address_of_credentials_3() { return &___credentials_3; }
	inline void set_credentials_3(RuntimeObject* value)
	{
		___credentials_3 = value;
		Il2CppCodeGenWriteBarrier((&___credentials_3), value);
	}

	inline static int32_t get_offset_of_useDefaultCredentials_4() { return static_cast<int32_t>(offsetof(WebProxy_t9793DD7C7F74F06D01ED3A0D331E2E9E1579CB9B, ___useDefaultCredentials_4)); }
	inline bool get_useDefaultCredentials_4() const { return ___useDefaultCredentials_4; }
	inline bool* get_address_of_useDefaultCredentials_4() { return &___useDefaultCredentials_4; }
	inline void set_useDefaultCredentials_4(bool value)
	{
		___useDefaultCredentials_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPROXY_T9793DD7C7F74F06D01ED3A0D331E2E9E1579CB9B_H
#ifndef ASNENCODEDDATA_T1B888B0D340736ECF8B35405760DC0FE62235A99_H
#define ASNENCODEDDATA_T1B888B0D340736ECF8B35405760DC0FE62235A99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AsnEncodedData
struct  AsnEncodedData_t1B888B0D340736ECF8B35405760DC0FE62235A99  : public RuntimeObject
{
public:
	// System.Security.Cryptography.Oid System.Security.Cryptography.AsnEncodedData::_oid
	Oid_tF92DE54931499E6C4157FE23D6706AD98E1FDB27 * ____oid_0;
	// System.Byte[] System.Security.Cryptography.AsnEncodedData::_raw
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ____raw_1;

public:
	inline static int32_t get_offset_of__oid_0() { return static_cast<int32_t>(offsetof(AsnEncodedData_t1B888B0D340736ECF8B35405760DC0FE62235A99, ____oid_0)); }
	inline Oid_tF92DE54931499E6C4157FE23D6706AD98E1FDB27 * get__oid_0() const { return ____oid_0; }
	inline Oid_tF92DE54931499E6C4157FE23D6706AD98E1FDB27 ** get_address_of__oid_0() { return &____oid_0; }
	inline void set__oid_0(Oid_tF92DE54931499E6C4157FE23D6706AD98E1FDB27 * value)
	{
		____oid_0 = value;
		Il2CppCodeGenWriteBarrier((&____oid_0), value);
	}

	inline static int32_t get_offset_of__raw_1() { return static_cast<int32_t>(offsetof(AsnEncodedData_t1B888B0D340736ECF8B35405760DC0FE62235A99, ____raw_1)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get__raw_1() const { return ____raw_1; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of__raw_1() { return &____raw_1; }
	inline void set__raw_1(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		____raw_1 = value;
		Il2CppCodeGenWriteBarrier((&____raw_1), value);
	}
};

struct AsnEncodedData_t1B888B0D340736ECF8B35405760DC0FE62235A99_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.AsnEncodedData::<>f__switch$mapA
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24mapA_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapA_2() { return static_cast<int32_t>(offsetof(AsnEncodedData_t1B888B0D340736ECF8B35405760DC0FE62235A99_StaticFields, ___U3CU3Ef__switchU24mapA_2)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24mapA_2() const { return ___U3CU3Ef__switchU24mapA_2; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24mapA_2() { return &___U3CU3Ef__switchU24mapA_2; }
	inline void set_U3CU3Ef__switchU24mapA_2(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24mapA_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapA_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASNENCODEDDATA_T1B888B0D340736ECF8B35405760DC0FE62235A99_H
#ifndef OID_TF92DE54931499E6C4157FE23D6706AD98E1FDB27_H
#define OID_TF92DE54931499E6C4157FE23D6706AD98E1FDB27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Oid
struct  Oid_tF92DE54931499E6C4157FE23D6706AD98E1FDB27  : public RuntimeObject
{
public:
	// System.String System.Security.Cryptography.Oid::_value
	String_t* ____value_0;
	// System.String System.Security.Cryptography.Oid::_name
	String_t* ____name_1;

public:
	inline static int32_t get_offset_of__value_0() { return static_cast<int32_t>(offsetof(Oid_tF92DE54931499E6C4157FE23D6706AD98E1FDB27, ____value_0)); }
	inline String_t* get__value_0() const { return ____value_0; }
	inline String_t** get_address_of__value_0() { return &____value_0; }
	inline void set__value_0(String_t* value)
	{
		____value_0 = value;
		Il2CppCodeGenWriteBarrier((&____value_0), value);
	}

	inline static int32_t get_offset_of__name_1() { return static_cast<int32_t>(offsetof(Oid_tF92DE54931499E6C4157FE23D6706AD98E1FDB27, ____name_1)); }
	inline String_t* get__name_1() const { return ____name_1; }
	inline String_t** get_address_of__name_1() { return &____name_1; }
	inline void set__name_1(String_t* value)
	{
		____name_1 = value;
		Il2CppCodeGenWriteBarrier((&____name_1), value);
	}
};

struct Oid_tF92DE54931499E6C4157FE23D6706AD98E1FDB27_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.Oid::<>f__switch$map10
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map10_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map10_2() { return static_cast<int32_t>(offsetof(Oid_tF92DE54931499E6C4157FE23D6706AD98E1FDB27_StaticFields, ___U3CU3Ef__switchU24map10_2)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map10_2() const { return ___U3CU3Ef__switchU24map10_2; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map10_2() { return &___U3CU3Ef__switchU24map10_2; }
	inline void set_U3CU3Ef__switchU24map10_2(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map10_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map10_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OID_TF92DE54931499E6C4157FE23D6706AD98E1FDB27_H
#ifndef OIDCOLLECTION_TDA5968A49C496611C36F3E30F8A675D6F57E08D0_H
#define OIDCOLLECTION_TDA5968A49C496611C36F3E30F8A675D6F57E08D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.OidCollection
struct  OidCollection_tDA5968A49C496611C36F3E30F8A675D6F57E08D0  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Security.Cryptography.OidCollection::_list
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ____list_0;
	// System.Boolean System.Security.Cryptography.OidCollection::_readOnly
	bool ____readOnly_1;

public:
	inline static int32_t get_offset_of__list_0() { return static_cast<int32_t>(offsetof(OidCollection_tDA5968A49C496611C36F3E30F8A675D6F57E08D0, ____list_0)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get__list_0() const { return ____list_0; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of__list_0() { return &____list_0; }
	inline void set__list_0(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		____list_0 = value;
		Il2CppCodeGenWriteBarrier((&____list_0), value);
	}

	inline static int32_t get_offset_of__readOnly_1() { return static_cast<int32_t>(offsetof(OidCollection_tDA5968A49C496611C36F3E30F8A675D6F57E08D0, ____readOnly_1)); }
	inline bool get__readOnly_1() const { return ____readOnly_1; }
	inline bool* get_address_of__readOnly_1() { return &____readOnly_1; }
	inline void set__readOnly_1(bool value)
	{
		____readOnly_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OIDCOLLECTION_TDA5968A49C496611C36F3E30F8A675D6F57E08D0_H
#ifndef OIDENUMERATOR_TD8C4ACFCB867089C9DFC37EE1F2691046228DDE8_H
#define OIDENUMERATOR_TD8C4ACFCB867089C9DFC37EE1F2691046228DDE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.OidEnumerator
struct  OidEnumerator_tD8C4ACFCB867089C9DFC37EE1F2691046228DDE8  : public RuntimeObject
{
public:
	// System.Security.Cryptography.OidCollection System.Security.Cryptography.OidEnumerator::_collection
	OidCollection_tDA5968A49C496611C36F3E30F8A675D6F57E08D0 * ____collection_0;
	// System.Int32 System.Security.Cryptography.OidEnumerator::_position
	int32_t ____position_1;

public:
	inline static int32_t get_offset_of__collection_0() { return static_cast<int32_t>(offsetof(OidEnumerator_tD8C4ACFCB867089C9DFC37EE1F2691046228DDE8, ____collection_0)); }
	inline OidCollection_tDA5968A49C496611C36F3E30F8A675D6F57E08D0 * get__collection_0() const { return ____collection_0; }
	inline OidCollection_tDA5968A49C496611C36F3E30F8A675D6F57E08D0 ** get_address_of__collection_0() { return &____collection_0; }
	inline void set__collection_0(OidCollection_tDA5968A49C496611C36F3E30F8A675D6F57E08D0 * value)
	{
		____collection_0 = value;
		Il2CppCodeGenWriteBarrier((&____collection_0), value);
	}

	inline static int32_t get_offset_of__position_1() { return static_cast<int32_t>(offsetof(OidEnumerator_tD8C4ACFCB867089C9DFC37EE1F2691046228DDE8, ____position_1)); }
	inline int32_t get__position_1() const { return ____position_1; }
	inline int32_t* get_address_of__position_1() { return &____position_1; }
	inline void set__position_1(int32_t value)
	{
		____position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OIDENUMERATOR_TD8C4ACFCB867089C9DFC37EE1F2691046228DDE8_H
#ifndef PUBLICKEY_T88CC203B5F644C95B6142BB541AF0D447D2727AB_H
#define PUBLICKEY_T88CC203B5F644C95B6142BB541AF0D447D2727AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.PublicKey
struct  PublicKey_t88CC203B5F644C95B6142BB541AF0D447D2727AB  : public RuntimeObject
{
public:
	// System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.X509Certificates.PublicKey::_key
	AsymmetricAlgorithm_tCE2A08658EFEA671D977BC39FA246B4FC5092BB2 * ____key_0;
	// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::_keyValue
	AsnEncodedData_t1B888B0D340736ECF8B35405760DC0FE62235A99 * ____keyValue_1;
	// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::_params
	AsnEncodedData_t1B888B0D340736ECF8B35405760DC0FE62235A99 * ____params_2;
	// System.Security.Cryptography.Oid System.Security.Cryptography.X509Certificates.PublicKey::_oid
	Oid_tF92DE54931499E6C4157FE23D6706AD98E1FDB27 * ____oid_3;

public:
	inline static int32_t get_offset_of__key_0() { return static_cast<int32_t>(offsetof(PublicKey_t88CC203B5F644C95B6142BB541AF0D447D2727AB, ____key_0)); }
	inline AsymmetricAlgorithm_tCE2A08658EFEA671D977BC39FA246B4FC5092BB2 * get__key_0() const { return ____key_0; }
	inline AsymmetricAlgorithm_tCE2A08658EFEA671D977BC39FA246B4FC5092BB2 ** get_address_of__key_0() { return &____key_0; }
	inline void set__key_0(AsymmetricAlgorithm_tCE2A08658EFEA671D977BC39FA246B4FC5092BB2 * value)
	{
		____key_0 = value;
		Il2CppCodeGenWriteBarrier((&____key_0), value);
	}

	inline static int32_t get_offset_of__keyValue_1() { return static_cast<int32_t>(offsetof(PublicKey_t88CC203B5F644C95B6142BB541AF0D447D2727AB, ____keyValue_1)); }
	inline AsnEncodedData_t1B888B0D340736ECF8B35405760DC0FE62235A99 * get__keyValue_1() const { return ____keyValue_1; }
	inline AsnEncodedData_t1B888B0D340736ECF8B35405760DC0FE62235A99 ** get_address_of__keyValue_1() { return &____keyValue_1; }
	inline void set__keyValue_1(AsnEncodedData_t1B888B0D340736ECF8B35405760DC0FE62235A99 * value)
	{
		____keyValue_1 = value;
		Il2CppCodeGenWriteBarrier((&____keyValue_1), value);
	}

	inline static int32_t get_offset_of__params_2() { return static_cast<int32_t>(offsetof(PublicKey_t88CC203B5F644C95B6142BB541AF0D447D2727AB, ____params_2)); }
	inline AsnEncodedData_t1B888B0D340736ECF8B35405760DC0FE62235A99 * get__params_2() const { return ____params_2; }
	inline AsnEncodedData_t1B888B0D340736ECF8B35405760DC0FE62235A99 ** get_address_of__params_2() { return &____params_2; }
	inline void set__params_2(AsnEncodedData_t1B888B0D340736ECF8B35405760DC0FE62235A99 * value)
	{
		____params_2 = value;
		Il2CppCodeGenWriteBarrier((&____params_2), value);
	}

	inline static int32_t get_offset_of__oid_3() { return static_cast<int32_t>(offsetof(PublicKey_t88CC203B5F644C95B6142BB541AF0D447D2727AB, ____oid_3)); }
	inline Oid_tF92DE54931499E6C4157FE23D6706AD98E1FDB27 * get__oid_3() const { return ____oid_3; }
	inline Oid_tF92DE54931499E6C4157FE23D6706AD98E1FDB27 ** get_address_of__oid_3() { return &____oid_3; }
	inline void set__oid_3(Oid_tF92DE54931499E6C4157FE23D6706AD98E1FDB27 * value)
	{
		____oid_3 = value;
		Il2CppCodeGenWriteBarrier((&____oid_3), value);
	}
};

struct PublicKey_t88CC203B5F644C95B6142BB541AF0D447D2727AB_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.X509Certificates.PublicKey::<>f__switch$map9
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map9_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map9_4() { return static_cast<int32_t>(offsetof(PublicKey_t88CC203B5F644C95B6142BB541AF0D447D2727AB_StaticFields, ___U3CU3Ef__switchU24map9_4)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map9_4() const { return ___U3CU3Ef__switchU24map9_4; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map9_4() { return &___U3CU3Ef__switchU24map9_4; }
	inline void set_U3CU3Ef__switchU24map9_4(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map9_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map9_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUBLICKEY_T88CC203B5F644C95B6142BB541AF0D447D2727AB_H
#ifndef X509CERTIFICATE_TC6D94A8A63C0640494BE76372C6E4C291D2826DE_H
#define X509CERTIFICATE_TC6D94A8A63C0640494BE76372C6E4C291D2826DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Certificate
struct  X509Certificate_tC6D94A8A63C0640494BE76372C6E4C291D2826DE  : public RuntimeObject
{
public:
	// Mono.Security.X509.X509Certificate System.Security.Cryptography.X509Certificates.X509Certificate::x509
	X509Certificate_t30E33DC87BDE409F02CF57214977DF8381792864 * ___x509_0;
	// System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate::hideDates
	bool ___hideDates_1;
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate::cachedCertificateHash
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___cachedCertificateHash_2;
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate::issuer_name
	String_t* ___issuer_name_3;
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate::subject_name
	String_t* ___subject_name_4;

public:
	inline static int32_t get_offset_of_x509_0() { return static_cast<int32_t>(offsetof(X509Certificate_tC6D94A8A63C0640494BE76372C6E4C291D2826DE, ___x509_0)); }
	inline X509Certificate_t30E33DC87BDE409F02CF57214977DF8381792864 * get_x509_0() const { return ___x509_0; }
	inline X509Certificate_t30E33DC87BDE409F02CF57214977DF8381792864 ** get_address_of_x509_0() { return &___x509_0; }
	inline void set_x509_0(X509Certificate_t30E33DC87BDE409F02CF57214977DF8381792864 * value)
	{
		___x509_0 = value;
		Il2CppCodeGenWriteBarrier((&___x509_0), value);
	}

	inline static int32_t get_offset_of_hideDates_1() { return static_cast<int32_t>(offsetof(X509Certificate_tC6D94A8A63C0640494BE76372C6E4C291D2826DE, ___hideDates_1)); }
	inline bool get_hideDates_1() const { return ___hideDates_1; }
	inline bool* get_address_of_hideDates_1() { return &___hideDates_1; }
	inline void set_hideDates_1(bool value)
	{
		___hideDates_1 = value;
	}

	inline static int32_t get_offset_of_cachedCertificateHash_2() { return static_cast<int32_t>(offsetof(X509Certificate_tC6D94A8A63C0640494BE76372C6E4C291D2826DE, ___cachedCertificateHash_2)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_cachedCertificateHash_2() const { return ___cachedCertificateHash_2; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_cachedCertificateHash_2() { return &___cachedCertificateHash_2; }
	inline void set_cachedCertificateHash_2(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___cachedCertificateHash_2 = value;
		Il2CppCodeGenWriteBarrier((&___cachedCertificateHash_2), value);
	}

	inline static int32_t get_offset_of_issuer_name_3() { return static_cast<int32_t>(offsetof(X509Certificate_tC6D94A8A63C0640494BE76372C6E4C291D2826DE, ___issuer_name_3)); }
	inline String_t* get_issuer_name_3() const { return ___issuer_name_3; }
	inline String_t** get_address_of_issuer_name_3() { return &___issuer_name_3; }
	inline void set_issuer_name_3(String_t* value)
	{
		___issuer_name_3 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_name_3), value);
	}

	inline static int32_t get_offset_of_subject_name_4() { return static_cast<int32_t>(offsetof(X509Certificate_tC6D94A8A63C0640494BE76372C6E4C291D2826DE, ___subject_name_4)); }
	inline String_t* get_subject_name_4() const { return ___subject_name_4; }
	inline String_t** get_address_of_subject_name_4() { return &___subject_name_4; }
	inline void set_subject_name_4(String_t* value)
	{
		___subject_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___subject_name_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATE_TC6D94A8A63C0640494BE76372C6E4C291D2826DE_H
#ifndef X509CERTIFICATE2ENUMERATOR_TC4E44FF809F0EC4F6CE4B31680843853F9863C53_H
#define X509CERTIFICATE2ENUMERATOR_TC4E44FF809F0EC4F6CE4B31680843853F9863C53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator
struct  X509Certificate2Enumerator_tC4E44FF809F0EC4F6CE4B31680843853F9863C53  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::enumerator
	RuntimeObject* ___enumerator_0;

public:
	inline static int32_t get_offset_of_enumerator_0() { return static_cast<int32_t>(offsetof(X509Certificate2Enumerator_tC4E44FF809F0EC4F6CE4B31680843853F9863C53, ___enumerator_0)); }
	inline RuntimeObject* get_enumerator_0() const { return ___enumerator_0; }
	inline RuntimeObject** get_address_of_enumerator_0() { return &___enumerator_0; }
	inline void set_enumerator_0(RuntimeObject* value)
	{
		___enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATE2ENUMERATOR_TC4E44FF809F0EC4F6CE4B31680843853F9863C53_H
#ifndef X509CERTIFICATEENUMERATOR_TCDAD2680885D77DB8F10B66BC925C8F68779A09F_H
#define X509CERTIFICATEENUMERATOR_TCDAD2680885D77DB8F10B66BC925C8F68779A09F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509CertificateCollection/X509CertificateEnumerator
struct  X509CertificateEnumerator_tCDAD2680885D77DB8F10B66BC925C8F68779A09F  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Security.Cryptography.X509Certificates.X509CertificateCollection/X509CertificateEnumerator::enumerator
	RuntimeObject* ___enumerator_0;

public:
	inline static int32_t get_offset_of_enumerator_0() { return static_cast<int32_t>(offsetof(X509CertificateEnumerator_tCDAD2680885D77DB8F10B66BC925C8F68779A09F, ___enumerator_0)); }
	inline RuntimeObject* get_enumerator_0() const { return ___enumerator_0; }
	inline RuntimeObject** get_address_of_enumerator_0() { return &___enumerator_0; }
	inline void set_enumerator_0(RuntimeObject* value)
	{
		___enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATEENUMERATOR_TCDAD2680885D77DB8F10B66BC925C8F68779A09F_H
#ifndef X509CHAINELEMENTCOLLECTION_TDA6F4291F55CC8640B2097C37F408A211B38E4A9_H
#define X509CHAINELEMENTCOLLECTION_TDA6F4291F55CC8640B2097C37F408A211B38E4A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainElementCollection
struct  X509ChainElementCollection_tDA6F4291F55CC8640B2097C37F408A211B38E4A9  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Security.Cryptography.X509Certificates.X509ChainElementCollection::_list
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ____list_0;

public:
	inline static int32_t get_offset_of__list_0() { return static_cast<int32_t>(offsetof(X509ChainElementCollection_tDA6F4291F55CC8640B2097C37F408A211B38E4A9, ____list_0)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get__list_0() const { return ____list_0; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of__list_0() { return &____list_0; }
	inline void set__list_0(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		____list_0 = value;
		Il2CppCodeGenWriteBarrier((&____list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINELEMENTCOLLECTION_TDA6F4291F55CC8640B2097C37F408A211B38E4A9_H
#ifndef X509CHAINELEMENTENUMERATOR_T33C4E24582B01AE8DB3AFA0574C2FE4493053392_H
#define X509CHAINELEMENTENUMERATOR_T33C4E24582B01AE8DB3AFA0574C2FE4493053392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator
struct  X509ChainElementEnumerator_t33C4E24582B01AE8DB3AFA0574C2FE4493053392  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator::enumerator
	RuntimeObject* ___enumerator_0;

public:
	inline static int32_t get_offset_of_enumerator_0() { return static_cast<int32_t>(offsetof(X509ChainElementEnumerator_t33C4E24582B01AE8DB3AFA0574C2FE4493053392, ___enumerator_0)); }
	inline RuntimeObject* get_enumerator_0() const { return ___enumerator_0; }
	inline RuntimeObject** get_address_of_enumerator_0() { return &___enumerator_0; }
	inline void set_enumerator_0(RuntimeObject* value)
	{
		___enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINELEMENTENUMERATOR_T33C4E24582B01AE8DB3AFA0574C2FE4493053392_H
#ifndef X509EXTENSIONCOLLECTION_TE5717291A64EA1198FD91903AC1E8BFC32BDEC3D_H
#define X509EXTENSIONCOLLECTION_TE5717291A64EA1198FD91903AC1E8BFC32BDEC3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ExtensionCollection
struct  X509ExtensionCollection_tE5717291A64EA1198FD91903AC1E8BFC32BDEC3D  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Security.Cryptography.X509Certificates.X509ExtensionCollection::_list
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ____list_0;

public:
	inline static int32_t get_offset_of__list_0() { return static_cast<int32_t>(offsetof(X509ExtensionCollection_tE5717291A64EA1198FD91903AC1E8BFC32BDEC3D, ____list_0)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get__list_0() const { return ____list_0; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of__list_0() { return &____list_0; }
	inline void set__list_0(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		____list_0 = value;
		Il2CppCodeGenWriteBarrier((&____list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSIONCOLLECTION_TE5717291A64EA1198FD91903AC1E8BFC32BDEC3D_H
#ifndef X509EXTENSIONENUMERATOR_T6FA184F43ADA293DF1EB760230EAE425B708B655_H
#define X509EXTENSIONENUMERATOR_T6FA184F43ADA293DF1EB760230EAE425B708B655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ExtensionEnumerator
struct  X509ExtensionEnumerator_t6FA184F43ADA293DF1EB760230EAE425B708B655  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Security.Cryptography.X509Certificates.X509ExtensionEnumerator::enumerator
	RuntimeObject* ___enumerator_0;

public:
	inline static int32_t get_offset_of_enumerator_0() { return static_cast<int32_t>(offsetof(X509ExtensionEnumerator_t6FA184F43ADA293DF1EB760230EAE425B708B655, ___enumerator_0)); }
	inline RuntimeObject* get_enumerator_0() const { return ___enumerator_0; }
	inline RuntimeObject** get_address_of_enumerator_0() { return &___enumerator_0; }
	inline void set_enumerator_0(RuntimeObject* value)
	{
		___enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSIONENUMERATOR_T6FA184F43ADA293DF1EB760230EAE425B708B655_H
#ifndef BASEMACHINE_T7846A209B98125A8E5B5FAFE5258CC5CBFE2F857_H
#define BASEMACHINE_T7846A209B98125A8E5B5FAFE5258CC5CBFE2F857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.BaseMachine
struct  BaseMachine_t7846A209B98125A8E5B5FAFE5258CC5CBFE2F857  : public RuntimeObject
{
public:
	// System.Boolean System.Text.RegularExpressions.BaseMachine::needs_groups_or_captures
	bool ___needs_groups_or_captures_0;

public:
	inline static int32_t get_offset_of_needs_groups_or_captures_0() { return static_cast<int32_t>(offsetof(BaseMachine_t7846A209B98125A8E5B5FAFE5258CC5CBFE2F857, ___needs_groups_or_captures_0)); }
	inline bool get_needs_groups_or_captures_0() const { return ___needs_groups_or_captures_0; }
	inline bool* get_address_of_needs_groups_or_captures_0() { return &___needs_groups_or_captures_0; }
	inline void set_needs_groups_or_captures_0(bool value)
	{
		___needs_groups_or_captures_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMACHINE_T7846A209B98125A8E5B5FAFE5258CC5CBFE2F857_H
#ifndef CAPTURE_T426EB72741D828F4539C131E1DC839DD00F01F22_H
#define CAPTURE_T426EB72741D828F4539C131E1DC839DD00F01F22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Capture
struct  Capture_t426EB72741D828F4539C131E1DC839DD00F01F22  : public RuntimeObject
{
public:
	// System.Int32 System.Text.RegularExpressions.Capture::index
	int32_t ___index_0;
	// System.Int32 System.Text.RegularExpressions.Capture::length
	int32_t ___length_1;
	// System.String System.Text.RegularExpressions.Capture::text
	String_t* ___text_2;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(Capture_t426EB72741D828F4539C131E1DC839DD00F01F22, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(Capture_t426EB72741D828F4539C131E1DC839DD00F01F22, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(Capture_t426EB72741D828F4539C131E1DC839DD00F01F22, ___text_2)); }
	inline String_t* get_text_2() const { return ___text_2; }
	inline String_t** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(String_t* value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier((&___text_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTURE_T426EB72741D828F4539C131E1DC839DD00F01F22_H
#ifndef CAPTURECOLLECTION_T5F80E90F8954A443D29D1EBEFC7119831DFB9C20_H
#define CAPTURECOLLECTION_T5F80E90F8954A443D29D1EBEFC7119831DFB9C20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.CaptureCollection
struct  CaptureCollection_t5F80E90F8954A443D29D1EBEFC7119831DFB9C20  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.Capture[] System.Text.RegularExpressions.CaptureCollection::list
	CaptureU5BU5D_tEEED727F6F99D0CF7F24F24E74A8ADD4DBF7E8D3* ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(CaptureCollection_t5F80E90F8954A443D29D1EBEFC7119831DFB9C20, ___list_0)); }
	inline CaptureU5BU5D_tEEED727F6F99D0CF7F24F24E74A8ADD4DBF7E8D3* get_list_0() const { return ___list_0; }
	inline CaptureU5BU5D_tEEED727F6F99D0CF7F24F24E74A8ADD4DBF7E8D3** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(CaptureU5BU5D_tEEED727F6F99D0CF7F24F24E74A8ADD4DBF7E8D3* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTURECOLLECTION_T5F80E90F8954A443D29D1EBEFC7119831DFB9C20_H
#ifndef CATEGORYUTILS_T4D6CED9206C1B5C61D0D93CCE8E416EBB271DB89_H
#define CATEGORYUTILS_T4D6CED9206C1B5C61D0D93CCE8E416EBB271DB89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.CategoryUtils
struct  CategoryUtils_t4D6CED9206C1B5C61D0D93CCE8E416EBB271DB89  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CATEGORYUTILS_T4D6CED9206C1B5C61D0D93CCE8E416EBB271DB89_H
#ifndef FACTORYCACHE_T8BE3083C5F3C2185BA9AE97F4886454CB85C06B9_H
#define FACTORYCACHE_T8BE3083C5F3C2185BA9AE97F4886454CB85C06B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.FactoryCache
struct  FactoryCache_t8BE3083C5F3C2185BA9AE97F4886454CB85C06B9  : public RuntimeObject
{
public:
	// System.Int32 System.Text.RegularExpressions.FactoryCache::capacity
	int32_t ___capacity_0;
	// System.Collections.Hashtable System.Text.RegularExpressions.FactoryCache::factories
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___factories_1;
	// System.Text.RegularExpressions.MRUList System.Text.RegularExpressions.FactoryCache::mru_list
	MRUList_t54E602EB72FB2B58075AB7BF8F013121606B1E1C * ___mru_list_2;

public:
	inline static int32_t get_offset_of_capacity_0() { return static_cast<int32_t>(offsetof(FactoryCache_t8BE3083C5F3C2185BA9AE97F4886454CB85C06B9, ___capacity_0)); }
	inline int32_t get_capacity_0() const { return ___capacity_0; }
	inline int32_t* get_address_of_capacity_0() { return &___capacity_0; }
	inline void set_capacity_0(int32_t value)
	{
		___capacity_0 = value;
	}

	inline static int32_t get_offset_of_factories_1() { return static_cast<int32_t>(offsetof(FactoryCache_t8BE3083C5F3C2185BA9AE97F4886454CB85C06B9, ___factories_1)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_factories_1() const { return ___factories_1; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_factories_1() { return &___factories_1; }
	inline void set_factories_1(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___factories_1 = value;
		Il2CppCodeGenWriteBarrier((&___factories_1), value);
	}

	inline static int32_t get_offset_of_mru_list_2() { return static_cast<int32_t>(offsetof(FactoryCache_t8BE3083C5F3C2185BA9AE97F4886454CB85C06B9, ___mru_list_2)); }
	inline MRUList_t54E602EB72FB2B58075AB7BF8F013121606B1E1C * get_mru_list_2() const { return ___mru_list_2; }
	inline MRUList_t54E602EB72FB2B58075AB7BF8F013121606B1E1C ** get_address_of_mru_list_2() { return &___mru_list_2; }
	inline void set_mru_list_2(MRUList_t54E602EB72FB2B58075AB7BF8F013121606B1E1C * value)
	{
		___mru_list_2 = value;
		Il2CppCodeGenWriteBarrier((&___mru_list_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACTORYCACHE_T8BE3083C5F3C2185BA9AE97F4886454CB85C06B9_H
#ifndef GROUPCOLLECTION_TBEA4EDB210F04963227CBC58E1434202F3CEC968_H
#define GROUPCOLLECTION_TBEA4EDB210F04963227CBC58E1434202F3CEC968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.GroupCollection
struct  GroupCollection_tBEA4EDB210F04963227CBC58E1434202F3CEC968  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.Group[] System.Text.RegularExpressions.GroupCollection::list
	GroupU5BU5D_tD5F0C5F00520A3213D8E6F225B1CF51985D2ACFE* ___list_0;
	// System.Int32 System.Text.RegularExpressions.GroupCollection::gap
	int32_t ___gap_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(GroupCollection_tBEA4EDB210F04963227CBC58E1434202F3CEC968, ___list_0)); }
	inline GroupU5BU5D_tD5F0C5F00520A3213D8E6F225B1CF51985D2ACFE* get_list_0() const { return ___list_0; }
	inline GroupU5BU5D_tD5F0C5F00520A3213D8E6F225B1CF51985D2ACFE** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(GroupU5BU5D_tD5F0C5F00520A3213D8E6F225B1CF51985D2ACFE* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_gap_1() { return static_cast<int32_t>(offsetof(GroupCollection_tBEA4EDB210F04963227CBC58E1434202F3CEC968, ___gap_1)); }
	inline int32_t get_gap_1() const { return ___gap_1; }
	inline int32_t* get_address_of_gap_1() { return &___gap_1; }
	inline void set_gap_1(int32_t value)
	{
		___gap_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPCOLLECTION_TBEA4EDB210F04963227CBC58E1434202F3CEC968_H
#ifndef INTERPRETERFACTORY_T9E80B031E667166B225D6EBC813F53DB862A7193_H
#define INTERPRETERFACTORY_T9E80B031E667166B225D6EBC813F53DB862A7193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.InterpreterFactory
struct  InterpreterFactory_t9E80B031E667166B225D6EBC813F53DB862A7193  : public RuntimeObject
{
public:
	// System.Collections.IDictionary System.Text.RegularExpressions.InterpreterFactory::mapping
	RuntimeObject* ___mapping_0;
	// System.UInt16[] System.Text.RegularExpressions.InterpreterFactory::pattern
	UInt16U5BU5D_tFACC39F5AB94FCCEAD5ED082922B0263437F9ABC* ___pattern_1;
	// System.String[] System.Text.RegularExpressions.InterpreterFactory::namesMapping
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___namesMapping_2;
	// System.Int32 System.Text.RegularExpressions.InterpreterFactory::gap
	int32_t ___gap_3;

public:
	inline static int32_t get_offset_of_mapping_0() { return static_cast<int32_t>(offsetof(InterpreterFactory_t9E80B031E667166B225D6EBC813F53DB862A7193, ___mapping_0)); }
	inline RuntimeObject* get_mapping_0() const { return ___mapping_0; }
	inline RuntimeObject** get_address_of_mapping_0() { return &___mapping_0; }
	inline void set_mapping_0(RuntimeObject* value)
	{
		___mapping_0 = value;
		Il2CppCodeGenWriteBarrier((&___mapping_0), value);
	}

	inline static int32_t get_offset_of_pattern_1() { return static_cast<int32_t>(offsetof(InterpreterFactory_t9E80B031E667166B225D6EBC813F53DB862A7193, ___pattern_1)); }
	inline UInt16U5BU5D_tFACC39F5AB94FCCEAD5ED082922B0263437F9ABC* get_pattern_1() const { return ___pattern_1; }
	inline UInt16U5BU5D_tFACC39F5AB94FCCEAD5ED082922B0263437F9ABC** get_address_of_pattern_1() { return &___pattern_1; }
	inline void set_pattern_1(UInt16U5BU5D_tFACC39F5AB94FCCEAD5ED082922B0263437F9ABC* value)
	{
		___pattern_1 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_1), value);
	}

	inline static int32_t get_offset_of_namesMapping_2() { return static_cast<int32_t>(offsetof(InterpreterFactory_t9E80B031E667166B225D6EBC813F53DB862A7193, ___namesMapping_2)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_namesMapping_2() const { return ___namesMapping_2; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_namesMapping_2() { return &___namesMapping_2; }
	inline void set_namesMapping_2(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___namesMapping_2 = value;
		Il2CppCodeGenWriteBarrier((&___namesMapping_2), value);
	}

	inline static int32_t get_offset_of_gap_3() { return static_cast<int32_t>(offsetof(InterpreterFactory_t9E80B031E667166B225D6EBC813F53DB862A7193, ___gap_3)); }
	inline int32_t get_gap_3() const { return ___gap_3; }
	inline int32_t* get_address_of_gap_3() { return &___gap_3; }
	inline void set_gap_3(int32_t value)
	{
		___gap_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPRETERFACTORY_T9E80B031E667166B225D6EBC813F53DB862A7193_H
#ifndef LINKREF_T4A317D95D5F6964647AE5BBB13F5CAE3F1C81FAB_H
#define LINKREF_T4A317D95D5F6964647AE5BBB13F5CAE3F1C81FAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.LinkRef
struct  LinkRef_t4A317D95D5F6964647AE5BBB13F5CAE3F1C81FAB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKREF_T4A317D95D5F6964647AE5BBB13F5CAE3F1C81FAB_H
#ifndef MRULIST_T54E602EB72FB2B58075AB7BF8F013121606B1E1C_H
#define MRULIST_T54E602EB72FB2B58075AB7BF8F013121606B1E1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.MRUList
struct  MRUList_t54E602EB72FB2B58075AB7BF8F013121606B1E1C  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.MRUList/Node System.Text.RegularExpressions.MRUList::head
	Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3 * ___head_0;
	// System.Text.RegularExpressions.MRUList/Node System.Text.RegularExpressions.MRUList::tail
	Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3 * ___tail_1;

public:
	inline static int32_t get_offset_of_head_0() { return static_cast<int32_t>(offsetof(MRUList_t54E602EB72FB2B58075AB7BF8F013121606B1E1C, ___head_0)); }
	inline Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3 * get_head_0() const { return ___head_0; }
	inline Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3 ** get_address_of_head_0() { return &___head_0; }
	inline void set_head_0(Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3 * value)
	{
		___head_0 = value;
		Il2CppCodeGenWriteBarrier((&___head_0), value);
	}

	inline static int32_t get_offset_of_tail_1() { return static_cast<int32_t>(offsetof(MRUList_t54E602EB72FB2B58075AB7BF8F013121606B1E1C, ___tail_1)); }
	inline Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3 * get_tail_1() const { return ___tail_1; }
	inline Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3 ** get_address_of_tail_1() { return &___tail_1; }
	inline void set_tail_1(Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3 * value)
	{
		___tail_1 = value;
		Il2CppCodeGenWriteBarrier((&___tail_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MRULIST_T54E602EB72FB2B58075AB7BF8F013121606B1E1C_H
#ifndef NODE_T5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3_H
#define NODE_T5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.MRUList/Node
struct  Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3  : public RuntimeObject
{
public:
	// System.Object System.Text.RegularExpressions.MRUList/Node::value
	RuntimeObject * ___value_0;
	// System.Text.RegularExpressions.MRUList/Node System.Text.RegularExpressions.MRUList/Node::previous
	Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3 * ___previous_1;
	// System.Text.RegularExpressions.MRUList/Node System.Text.RegularExpressions.MRUList/Node::next
	Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3 * ___next_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3, ___value_0)); }
	inline RuntimeObject * get_value_0() const { return ___value_0; }
	inline RuntimeObject ** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(RuntimeObject * value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_previous_1() { return static_cast<int32_t>(offsetof(Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3, ___previous_1)); }
	inline Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3 * get_previous_1() const { return ___previous_1; }
	inline Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3 ** get_address_of_previous_1() { return &___previous_1; }
	inline void set_previous_1(Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3 * value)
	{
		___previous_1 = value;
		Il2CppCodeGenWriteBarrier((&___previous_1), value);
	}

	inline static int32_t get_offset_of_next_2() { return static_cast<int32_t>(offsetof(Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3, ___next_2)); }
	inline Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3 * get_next_2() const { return ___next_2; }
	inline Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3 ** get_address_of_next_2() { return &___next_2; }
	inline void set_next_2(Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3 * value)
	{
		___next_2 = value;
		Il2CppCodeGenWriteBarrier((&___next_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODE_T5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3_H
#ifndef MATCHCOLLECTION_TD651B345093D8CEC4F8E3302458C17355E017B2E_H
#define MATCHCOLLECTION_TD651B345093D8CEC4F8E3302458C17355E017B2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.MatchCollection
struct  MatchCollection_tD651B345093D8CEC4F8E3302458C17355E017B2E  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.MatchCollection::current
	Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53 * ___current_0;
	// System.Collections.ArrayList System.Text.RegularExpressions.MatchCollection::list
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___list_1;

public:
	inline static int32_t get_offset_of_current_0() { return static_cast<int32_t>(offsetof(MatchCollection_tD651B345093D8CEC4F8E3302458C17355E017B2E, ___current_0)); }
	inline Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53 * get_current_0() const { return ___current_0; }
	inline Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53 ** get_address_of_current_0() { return &___current_0; }
	inline void set_current_0(Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53 * value)
	{
		___current_0 = value;
		Il2CppCodeGenWriteBarrier((&___current_0), value);
	}

	inline static int32_t get_offset_of_list_1() { return static_cast<int32_t>(offsetof(MatchCollection_tD651B345093D8CEC4F8E3302458C17355E017B2E, ___list_1)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_list_1() const { return ___list_1; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_list_1() { return &___list_1; }
	inline void set_list_1(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___list_1 = value;
		Il2CppCodeGenWriteBarrier((&___list_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHCOLLECTION_TD651B345093D8CEC4F8E3302458C17355E017B2E_H
#ifndef ENUMERATOR_TEA319EDBF9ADA404733366A75202340DAD456FD8_H
#define ENUMERATOR_TEA319EDBF9ADA404733366A75202340DAD456FD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.MatchCollection/Enumerator
struct  Enumerator_tEA319EDBF9ADA404733366A75202340DAD456FD8  : public RuntimeObject
{
public:
	// System.Int32 System.Text.RegularExpressions.MatchCollection/Enumerator::index
	int32_t ___index_0;
	// System.Text.RegularExpressions.MatchCollection System.Text.RegularExpressions.MatchCollection/Enumerator::coll
	MatchCollection_tD651B345093D8CEC4F8E3302458C17355E017B2E * ___coll_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(Enumerator_tEA319EDBF9ADA404733366A75202340DAD456FD8, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_coll_1() { return static_cast<int32_t>(offsetof(Enumerator_tEA319EDBF9ADA404733366A75202340DAD456FD8, ___coll_1)); }
	inline MatchCollection_tD651B345093D8CEC4F8E3302458C17355E017B2E * get_coll_1() const { return ___coll_1; }
	inline MatchCollection_tD651B345093D8CEC4F8E3302458C17355E017B2E ** get_address_of_coll_1() { return &___coll_1; }
	inline void set_coll_1(MatchCollection_tD651B345093D8CEC4F8E3302458C17355E017B2E * value)
	{
		___coll_1 = value;
		Il2CppCodeGenWriteBarrier((&___coll_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_TEA319EDBF9ADA404733366A75202340DAD456FD8_H
#ifndef PATTERNCOMPILER_TF9296446862317819B849C8703585892EEFFBB91_H
#define PATTERNCOMPILER_TF9296446862317819B849C8703585892EEFFBB91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.PatternCompiler
struct  PatternCompiler_tF9296446862317819B849C8703585892EEFFBB91  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Text.RegularExpressions.PatternCompiler::pgm
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___pgm_0;

public:
	inline static int32_t get_offset_of_pgm_0() { return static_cast<int32_t>(offsetof(PatternCompiler_tF9296446862317819B849C8703585892EEFFBB91, ___pgm_0)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_pgm_0() const { return ___pgm_0; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_pgm_0() { return &___pgm_0; }
	inline void set_pgm_0(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___pgm_0 = value;
		Il2CppCodeGenWriteBarrier((&___pgm_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATTERNCOMPILER_TF9296446862317819B849C8703585892EEFFBB91_H
#ifndef VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#define VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_com
{
};
#endif // VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifndef NAMEVALUECOLLECTION_TB87E7DD2A8341E8E48EEC4D39322C1CE72E103EF_H
#define NAMEVALUECOLLECTION_TB87E7DD2A8341E8E48EEC4D39322C1CE72E103EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameValueCollection
struct  NameValueCollection_tB87E7DD2A8341E8E48EEC4D39322C1CE72E103EF  : public NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070
{
public:
	// System.String[] System.Collections.Specialized.NameValueCollection::cachedAllKeys
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___cachedAllKeys_10;
	// System.String[] System.Collections.Specialized.NameValueCollection::cachedAll
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___cachedAll_11;

public:
	inline static int32_t get_offset_of_cachedAllKeys_10() { return static_cast<int32_t>(offsetof(NameValueCollection_tB87E7DD2A8341E8E48EEC4D39322C1CE72E103EF, ___cachedAllKeys_10)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_cachedAllKeys_10() const { return ___cachedAllKeys_10; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_cachedAllKeys_10() { return &___cachedAllKeys_10; }
	inline void set_cachedAllKeys_10(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___cachedAllKeys_10 = value;
		Il2CppCodeGenWriteBarrier((&___cachedAllKeys_10), value);
	}

	inline static int32_t get_offset_of_cachedAll_11() { return static_cast<int32_t>(offsetof(NameValueCollection_tB87E7DD2A8341E8E48EEC4D39322C1CE72E103EF, ___cachedAll_11)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_cachedAll_11() const { return ___cachedAll_11; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_cachedAll_11() { return &___cachedAll_11; }
	inline void set_cachedAll_11(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___cachedAll_11 = value;
		Il2CppCodeGenWriteBarrier((&___cachedAll_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEVALUECOLLECTION_TB87E7DD2A8341E8E48EEC4D39322C1CE72E103EF_H
#ifndef ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#define ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF  : public ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C
{
public:

public:
};

struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_com
{
};
#endif // ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef IPENDPOINT_TE99679FFDD0135AA3881DF00523DD9D6BE7E6622_H
#define IPENDPOINT_TE99679FFDD0135AA3881DF00523DD9D6BE7E6622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPEndPoint
struct  IPEndPoint_tE99679FFDD0135AA3881DF00523DD9D6BE7E6622  : public EndPoint_t025B464C83C18FB6B01BBA3888ECD9C9168F8DB5
{
public:
	// System.Net.IPAddress System.Net.IPEndPoint::address
	IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 * ___address_0;
	// System.Int32 System.Net.IPEndPoint::port
	int32_t ___port_1;

public:
	inline static int32_t get_offset_of_address_0() { return static_cast<int32_t>(offsetof(IPEndPoint_tE99679FFDD0135AA3881DF00523DD9D6BE7E6622, ___address_0)); }
	inline IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 * get_address_0() const { return ___address_0; }
	inline IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 ** get_address_of_address_0() { return &___address_0; }
	inline void set_address_0(IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 * value)
	{
		___address_0 = value;
		Il2CppCodeGenWriteBarrier((&___address_0), value);
	}

	inline static int32_t get_offset_of_port_1() { return static_cast<int32_t>(offsetof(IPEndPoint_tE99679FFDD0135AA3881DF00523DD9D6BE7E6622, ___port_1)); }
	inline int32_t get_port_1() const { return ___port_1; }
	inline int32_t* get_address_of_port_1() { return &___port_1; }
	inline void set_port_1(int32_t value)
	{
		___port_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPENDPOINT_TE99679FFDD0135AA3881DF00523DD9D6BE7E6622_H
#ifndef WEBCONNECTIONSTREAM_T0E1041BD953CBA895AB48E376B2E8D94CD260A59_H
#define WEBCONNECTIONSTREAM_T0E1041BD953CBA895AB48E376B2E8D94CD260A59_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnectionStream
struct  WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59  : public Stream_tCFD27E43C18381861212C0288CACF467FB602009
{
public:
	// System.Boolean System.Net.WebConnectionStream::isRead
	bool ___isRead_2;
	// System.Net.WebConnection System.Net.WebConnectionStream::cnc
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28 * ___cnc_3;
	// System.Net.HttpWebRequest System.Net.WebConnectionStream::request
	HttpWebRequest_t03D495C58ABDF0A533A2DABD6D7FDECD6507383E * ___request_4;
	// System.Byte[] System.Net.WebConnectionStream::readBuffer
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___readBuffer_5;
	// System.Int32 System.Net.WebConnectionStream::readBufferOffset
	int32_t ___readBufferOffset_6;
	// System.Int32 System.Net.WebConnectionStream::readBufferSize
	int32_t ___readBufferSize_7;
	// System.Int32 System.Net.WebConnectionStream::contentLength
	int32_t ___contentLength_8;
	// System.Int32 System.Net.WebConnectionStream::totalRead
	int32_t ___totalRead_9;
	// System.Int64 System.Net.WebConnectionStream::totalWritten
	int64_t ___totalWritten_10;
	// System.Boolean System.Net.WebConnectionStream::nextReadCalled
	bool ___nextReadCalled_11;
	// System.Int32 System.Net.WebConnectionStream::pendingReads
	int32_t ___pendingReads_12;
	// System.Int32 System.Net.WebConnectionStream::pendingWrites
	int32_t ___pendingWrites_13;
	// System.Threading.ManualResetEvent System.Net.WebConnectionStream::pending
	ManualResetEvent_tAFDF6453D9A7F8C3A557771B35E4BBA6DA882DBA * ___pending_14;
	// System.Boolean System.Net.WebConnectionStream::allowBuffering
	bool ___allowBuffering_15;
	// System.Boolean System.Net.WebConnectionStream::sendChunked
	bool ___sendChunked_16;
	// System.IO.MemoryStream System.Net.WebConnectionStream::writeBuffer
	MemoryStream_tA2A6655CF733913D13B7AB22E4FF081CB92F5FF0 * ___writeBuffer_17;
	// System.Boolean System.Net.WebConnectionStream::requestWritten
	bool ___requestWritten_18;
	// System.Byte[] System.Net.WebConnectionStream::headers
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___headers_19;
	// System.Boolean System.Net.WebConnectionStream::disposed
	bool ___disposed_20;
	// System.Boolean System.Net.WebConnectionStream::headersSent
	bool ___headersSent_21;
	// System.Object System.Net.WebConnectionStream::locker
	RuntimeObject * ___locker_22;
	// System.Boolean System.Net.WebConnectionStream::initRead
	bool ___initRead_23;
	// System.Boolean System.Net.WebConnectionStream::read_eof
	bool ___read_eof_24;
	// System.Boolean System.Net.WebConnectionStream::complete_request_written
	bool ___complete_request_written_25;
	// System.Int32 System.Net.WebConnectionStream::read_timeout
	int32_t ___read_timeout_26;
	// System.Int32 System.Net.WebConnectionStream::write_timeout
	int32_t ___write_timeout_27;

public:
	inline static int32_t get_offset_of_isRead_2() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___isRead_2)); }
	inline bool get_isRead_2() const { return ___isRead_2; }
	inline bool* get_address_of_isRead_2() { return &___isRead_2; }
	inline void set_isRead_2(bool value)
	{
		___isRead_2 = value;
	}

	inline static int32_t get_offset_of_cnc_3() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___cnc_3)); }
	inline WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28 * get_cnc_3() const { return ___cnc_3; }
	inline WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28 ** get_address_of_cnc_3() { return &___cnc_3; }
	inline void set_cnc_3(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28 * value)
	{
		___cnc_3 = value;
		Il2CppCodeGenWriteBarrier((&___cnc_3), value);
	}

	inline static int32_t get_offset_of_request_4() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___request_4)); }
	inline HttpWebRequest_t03D495C58ABDF0A533A2DABD6D7FDECD6507383E * get_request_4() const { return ___request_4; }
	inline HttpWebRequest_t03D495C58ABDF0A533A2DABD6D7FDECD6507383E ** get_address_of_request_4() { return &___request_4; }
	inline void set_request_4(HttpWebRequest_t03D495C58ABDF0A533A2DABD6D7FDECD6507383E * value)
	{
		___request_4 = value;
		Il2CppCodeGenWriteBarrier((&___request_4), value);
	}

	inline static int32_t get_offset_of_readBuffer_5() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___readBuffer_5)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_readBuffer_5() const { return ___readBuffer_5; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_readBuffer_5() { return &___readBuffer_5; }
	inline void set_readBuffer_5(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___readBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___readBuffer_5), value);
	}

	inline static int32_t get_offset_of_readBufferOffset_6() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___readBufferOffset_6)); }
	inline int32_t get_readBufferOffset_6() const { return ___readBufferOffset_6; }
	inline int32_t* get_address_of_readBufferOffset_6() { return &___readBufferOffset_6; }
	inline void set_readBufferOffset_6(int32_t value)
	{
		___readBufferOffset_6 = value;
	}

	inline static int32_t get_offset_of_readBufferSize_7() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___readBufferSize_7)); }
	inline int32_t get_readBufferSize_7() const { return ___readBufferSize_7; }
	inline int32_t* get_address_of_readBufferSize_7() { return &___readBufferSize_7; }
	inline void set_readBufferSize_7(int32_t value)
	{
		___readBufferSize_7 = value;
	}

	inline static int32_t get_offset_of_contentLength_8() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___contentLength_8)); }
	inline int32_t get_contentLength_8() const { return ___contentLength_8; }
	inline int32_t* get_address_of_contentLength_8() { return &___contentLength_8; }
	inline void set_contentLength_8(int32_t value)
	{
		___contentLength_8 = value;
	}

	inline static int32_t get_offset_of_totalRead_9() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___totalRead_9)); }
	inline int32_t get_totalRead_9() const { return ___totalRead_9; }
	inline int32_t* get_address_of_totalRead_9() { return &___totalRead_9; }
	inline void set_totalRead_9(int32_t value)
	{
		___totalRead_9 = value;
	}

	inline static int32_t get_offset_of_totalWritten_10() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___totalWritten_10)); }
	inline int64_t get_totalWritten_10() const { return ___totalWritten_10; }
	inline int64_t* get_address_of_totalWritten_10() { return &___totalWritten_10; }
	inline void set_totalWritten_10(int64_t value)
	{
		___totalWritten_10 = value;
	}

	inline static int32_t get_offset_of_nextReadCalled_11() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___nextReadCalled_11)); }
	inline bool get_nextReadCalled_11() const { return ___nextReadCalled_11; }
	inline bool* get_address_of_nextReadCalled_11() { return &___nextReadCalled_11; }
	inline void set_nextReadCalled_11(bool value)
	{
		___nextReadCalled_11 = value;
	}

	inline static int32_t get_offset_of_pendingReads_12() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___pendingReads_12)); }
	inline int32_t get_pendingReads_12() const { return ___pendingReads_12; }
	inline int32_t* get_address_of_pendingReads_12() { return &___pendingReads_12; }
	inline void set_pendingReads_12(int32_t value)
	{
		___pendingReads_12 = value;
	}

	inline static int32_t get_offset_of_pendingWrites_13() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___pendingWrites_13)); }
	inline int32_t get_pendingWrites_13() const { return ___pendingWrites_13; }
	inline int32_t* get_address_of_pendingWrites_13() { return &___pendingWrites_13; }
	inline void set_pendingWrites_13(int32_t value)
	{
		___pendingWrites_13 = value;
	}

	inline static int32_t get_offset_of_pending_14() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___pending_14)); }
	inline ManualResetEvent_tAFDF6453D9A7F8C3A557771B35E4BBA6DA882DBA * get_pending_14() const { return ___pending_14; }
	inline ManualResetEvent_tAFDF6453D9A7F8C3A557771B35E4BBA6DA882DBA ** get_address_of_pending_14() { return &___pending_14; }
	inline void set_pending_14(ManualResetEvent_tAFDF6453D9A7F8C3A557771B35E4BBA6DA882DBA * value)
	{
		___pending_14 = value;
		Il2CppCodeGenWriteBarrier((&___pending_14), value);
	}

	inline static int32_t get_offset_of_allowBuffering_15() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___allowBuffering_15)); }
	inline bool get_allowBuffering_15() const { return ___allowBuffering_15; }
	inline bool* get_address_of_allowBuffering_15() { return &___allowBuffering_15; }
	inline void set_allowBuffering_15(bool value)
	{
		___allowBuffering_15 = value;
	}

	inline static int32_t get_offset_of_sendChunked_16() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___sendChunked_16)); }
	inline bool get_sendChunked_16() const { return ___sendChunked_16; }
	inline bool* get_address_of_sendChunked_16() { return &___sendChunked_16; }
	inline void set_sendChunked_16(bool value)
	{
		___sendChunked_16 = value;
	}

	inline static int32_t get_offset_of_writeBuffer_17() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___writeBuffer_17)); }
	inline MemoryStream_tA2A6655CF733913D13B7AB22E4FF081CB92F5FF0 * get_writeBuffer_17() const { return ___writeBuffer_17; }
	inline MemoryStream_tA2A6655CF733913D13B7AB22E4FF081CB92F5FF0 ** get_address_of_writeBuffer_17() { return &___writeBuffer_17; }
	inline void set_writeBuffer_17(MemoryStream_tA2A6655CF733913D13B7AB22E4FF081CB92F5FF0 * value)
	{
		___writeBuffer_17 = value;
		Il2CppCodeGenWriteBarrier((&___writeBuffer_17), value);
	}

	inline static int32_t get_offset_of_requestWritten_18() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___requestWritten_18)); }
	inline bool get_requestWritten_18() const { return ___requestWritten_18; }
	inline bool* get_address_of_requestWritten_18() { return &___requestWritten_18; }
	inline void set_requestWritten_18(bool value)
	{
		___requestWritten_18 = value;
	}

	inline static int32_t get_offset_of_headers_19() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___headers_19)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_headers_19() const { return ___headers_19; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_headers_19() { return &___headers_19; }
	inline void set_headers_19(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___headers_19 = value;
		Il2CppCodeGenWriteBarrier((&___headers_19), value);
	}

	inline static int32_t get_offset_of_disposed_20() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___disposed_20)); }
	inline bool get_disposed_20() const { return ___disposed_20; }
	inline bool* get_address_of_disposed_20() { return &___disposed_20; }
	inline void set_disposed_20(bool value)
	{
		___disposed_20 = value;
	}

	inline static int32_t get_offset_of_headersSent_21() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___headersSent_21)); }
	inline bool get_headersSent_21() const { return ___headersSent_21; }
	inline bool* get_address_of_headersSent_21() { return &___headersSent_21; }
	inline void set_headersSent_21(bool value)
	{
		___headersSent_21 = value;
	}

	inline static int32_t get_offset_of_locker_22() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___locker_22)); }
	inline RuntimeObject * get_locker_22() const { return ___locker_22; }
	inline RuntimeObject ** get_address_of_locker_22() { return &___locker_22; }
	inline void set_locker_22(RuntimeObject * value)
	{
		___locker_22 = value;
		Il2CppCodeGenWriteBarrier((&___locker_22), value);
	}

	inline static int32_t get_offset_of_initRead_23() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___initRead_23)); }
	inline bool get_initRead_23() const { return ___initRead_23; }
	inline bool* get_address_of_initRead_23() { return &___initRead_23; }
	inline void set_initRead_23(bool value)
	{
		___initRead_23 = value;
	}

	inline static int32_t get_offset_of_read_eof_24() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___read_eof_24)); }
	inline bool get_read_eof_24() const { return ___read_eof_24; }
	inline bool* get_address_of_read_eof_24() { return &___read_eof_24; }
	inline void set_read_eof_24(bool value)
	{
		___read_eof_24 = value;
	}

	inline static int32_t get_offset_of_complete_request_written_25() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___complete_request_written_25)); }
	inline bool get_complete_request_written_25() const { return ___complete_request_written_25; }
	inline bool* get_address_of_complete_request_written_25() { return &___complete_request_written_25; }
	inline void set_complete_request_written_25(bool value)
	{
		___complete_request_written_25 = value;
	}

	inline static int32_t get_offset_of_read_timeout_26() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___read_timeout_26)); }
	inline int32_t get_read_timeout_26() const { return ___read_timeout_26; }
	inline int32_t* get_address_of_read_timeout_26() { return &___read_timeout_26; }
	inline void set_read_timeout_26(int32_t value)
	{
		___read_timeout_26 = value;
	}

	inline static int32_t get_offset_of_write_timeout_27() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59, ___write_timeout_27)); }
	inline int32_t get_write_timeout_27() const { return ___write_timeout_27; }
	inline int32_t* get_address_of_write_timeout_27() { return &___write_timeout_27; }
	inline void set_write_timeout_27(int32_t value)
	{
		___write_timeout_27 = value;
	}
};

struct WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59_StaticFields
{
public:
	// System.Byte[] System.Net.WebConnectionStream::crlf
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___crlf_1;

public:
	inline static int32_t get_offset_of_crlf_1() { return static_cast<int32_t>(offsetof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59_StaticFields, ___crlf_1)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_crlf_1() const { return ___crlf_1; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_crlf_1() { return &___crlf_1; }
	inline void set_crlf_1(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___crlf_1 = value;
		Il2CppCodeGenWriteBarrier((&___crlf_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCONNECTIONSTREAM_T0E1041BD953CBA895AB48E376B2E8D94CD260A59_H
#ifndef WEBRESPONSE_T5E9F05BAC005D2105A122CD5973E1D70B9B45325_H
#define WEBRESPONSE_T5E9F05BAC005D2105A122CD5973E1D70B9B45325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebResponse
struct  WebResponse_t5E9F05BAC005D2105A122CD5973E1D70B9B45325  : public MarshalByRefObject_t05F62A8AC86E36BAE3063CA28097945DE9E179C4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBRESPONSE_T5E9F05BAC005D2105A122CD5973E1D70B9B45325_H
#ifndef X500DISTINGUISHEDNAME_T4E51A8B638BD9EFBFC71A78F0263716509B878C9_H
#define X500DISTINGUISHEDNAME_T4E51A8B638BD9EFBFC71A78F0263716509B878C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X500DistinguishedName
struct  X500DistinguishedName_t4E51A8B638BD9EFBFC71A78F0263716509B878C9  : public AsnEncodedData_t1B888B0D340736ECF8B35405760DC0FE62235A99
{
public:
	// System.String System.Security.Cryptography.X509Certificates.X500DistinguishedName::name
	String_t* ___name_3;

public:
	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(X500DistinguishedName_t4E51A8B638BD9EFBFC71A78F0263716509B878C9, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X500DISTINGUISHEDNAME_T4E51A8B638BD9EFBFC71A78F0263716509B878C9_H
#ifndef X509CERTIFICATE2_T8488BFC28EE54980505493CD235FDDAC52A6B4A5_H
#define X509CERTIFICATE2_T8488BFC28EE54980505493CD235FDDAC52A6B4A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Certificate2
struct  X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5  : public X509Certificate_tC6D94A8A63C0640494BE76372C6E4C291D2826DE
{
public:
	// System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate2::_archived
	bool ____archived_5;
	// System.Security.Cryptography.X509Certificates.X509ExtensionCollection System.Security.Cryptography.X509Certificates.X509Certificate2::_extensions
	X509ExtensionCollection_tE5717291A64EA1198FD91903AC1E8BFC32BDEC3D * ____extensions_6;
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate2::_name
	String_t* ____name_7;
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate2::_serial
	String_t* ____serial_8;
	// System.Security.Cryptography.X509Certificates.PublicKey System.Security.Cryptography.X509Certificates.X509Certificate2::_publicKey
	PublicKey_t88CC203B5F644C95B6142BB541AF0D447D2727AB * ____publicKey_9;
	// System.Security.Cryptography.X509Certificates.X500DistinguishedName System.Security.Cryptography.X509Certificates.X509Certificate2::issuer_name
	X500DistinguishedName_t4E51A8B638BD9EFBFC71A78F0263716509B878C9 * ___issuer_name_10;
	// System.Security.Cryptography.X509Certificates.X500DistinguishedName System.Security.Cryptography.X509Certificates.X509Certificate2::subject_name
	X500DistinguishedName_t4E51A8B638BD9EFBFC71A78F0263716509B878C9 * ___subject_name_11;
	// System.Security.Cryptography.Oid System.Security.Cryptography.X509Certificates.X509Certificate2::signature_algorithm
	Oid_tF92DE54931499E6C4157FE23D6706AD98E1FDB27 * ___signature_algorithm_12;
	// Mono.Security.X509.X509Certificate System.Security.Cryptography.X509Certificates.X509Certificate2::_cert
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7 * ____cert_13;

public:
	inline static int32_t get_offset_of__archived_5() { return static_cast<int32_t>(offsetof(X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5, ____archived_5)); }
	inline bool get__archived_5() const { return ____archived_5; }
	inline bool* get_address_of__archived_5() { return &____archived_5; }
	inline void set__archived_5(bool value)
	{
		____archived_5 = value;
	}

	inline static int32_t get_offset_of__extensions_6() { return static_cast<int32_t>(offsetof(X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5, ____extensions_6)); }
	inline X509ExtensionCollection_tE5717291A64EA1198FD91903AC1E8BFC32BDEC3D * get__extensions_6() const { return ____extensions_6; }
	inline X509ExtensionCollection_tE5717291A64EA1198FD91903AC1E8BFC32BDEC3D ** get_address_of__extensions_6() { return &____extensions_6; }
	inline void set__extensions_6(X509ExtensionCollection_tE5717291A64EA1198FD91903AC1E8BFC32BDEC3D * value)
	{
		____extensions_6 = value;
		Il2CppCodeGenWriteBarrier((&____extensions_6), value);
	}

	inline static int32_t get_offset_of__name_7() { return static_cast<int32_t>(offsetof(X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5, ____name_7)); }
	inline String_t* get__name_7() const { return ____name_7; }
	inline String_t** get_address_of__name_7() { return &____name_7; }
	inline void set__name_7(String_t* value)
	{
		____name_7 = value;
		Il2CppCodeGenWriteBarrier((&____name_7), value);
	}

	inline static int32_t get_offset_of__serial_8() { return static_cast<int32_t>(offsetof(X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5, ____serial_8)); }
	inline String_t* get__serial_8() const { return ____serial_8; }
	inline String_t** get_address_of__serial_8() { return &____serial_8; }
	inline void set__serial_8(String_t* value)
	{
		____serial_8 = value;
		Il2CppCodeGenWriteBarrier((&____serial_8), value);
	}

	inline static int32_t get_offset_of__publicKey_9() { return static_cast<int32_t>(offsetof(X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5, ____publicKey_9)); }
	inline PublicKey_t88CC203B5F644C95B6142BB541AF0D447D2727AB * get__publicKey_9() const { return ____publicKey_9; }
	inline PublicKey_t88CC203B5F644C95B6142BB541AF0D447D2727AB ** get_address_of__publicKey_9() { return &____publicKey_9; }
	inline void set__publicKey_9(PublicKey_t88CC203B5F644C95B6142BB541AF0D447D2727AB * value)
	{
		____publicKey_9 = value;
		Il2CppCodeGenWriteBarrier((&____publicKey_9), value);
	}

	inline static int32_t get_offset_of_issuer_name_10() { return static_cast<int32_t>(offsetof(X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5, ___issuer_name_10)); }
	inline X500DistinguishedName_t4E51A8B638BD9EFBFC71A78F0263716509B878C9 * get_issuer_name_10() const { return ___issuer_name_10; }
	inline X500DistinguishedName_t4E51A8B638BD9EFBFC71A78F0263716509B878C9 ** get_address_of_issuer_name_10() { return &___issuer_name_10; }
	inline void set_issuer_name_10(X500DistinguishedName_t4E51A8B638BD9EFBFC71A78F0263716509B878C9 * value)
	{
		___issuer_name_10 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_name_10), value);
	}

	inline static int32_t get_offset_of_subject_name_11() { return static_cast<int32_t>(offsetof(X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5, ___subject_name_11)); }
	inline X500DistinguishedName_t4E51A8B638BD9EFBFC71A78F0263716509B878C9 * get_subject_name_11() const { return ___subject_name_11; }
	inline X500DistinguishedName_t4E51A8B638BD9EFBFC71A78F0263716509B878C9 ** get_address_of_subject_name_11() { return &___subject_name_11; }
	inline void set_subject_name_11(X500DistinguishedName_t4E51A8B638BD9EFBFC71A78F0263716509B878C9 * value)
	{
		___subject_name_11 = value;
		Il2CppCodeGenWriteBarrier((&___subject_name_11), value);
	}

	inline static int32_t get_offset_of_signature_algorithm_12() { return static_cast<int32_t>(offsetof(X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5, ___signature_algorithm_12)); }
	inline Oid_tF92DE54931499E6C4157FE23D6706AD98E1FDB27 * get_signature_algorithm_12() const { return ___signature_algorithm_12; }
	inline Oid_tF92DE54931499E6C4157FE23D6706AD98E1FDB27 ** get_address_of_signature_algorithm_12() { return &___signature_algorithm_12; }
	inline void set_signature_algorithm_12(Oid_tF92DE54931499E6C4157FE23D6706AD98E1FDB27 * value)
	{
		___signature_algorithm_12 = value;
		Il2CppCodeGenWriteBarrier((&___signature_algorithm_12), value);
	}

	inline static int32_t get_offset_of__cert_13() { return static_cast<int32_t>(offsetof(X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5, ____cert_13)); }
	inline X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7 * get__cert_13() const { return ____cert_13; }
	inline X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7 ** get_address_of__cert_13() { return &____cert_13; }
	inline void set__cert_13(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7 * value)
	{
		____cert_13 = value;
		Il2CppCodeGenWriteBarrier((&____cert_13), value);
	}
};

struct X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5_StaticFields
{
public:
	// System.String System.Security.Cryptography.X509Certificates.X509Certificate2::empty_error
	String_t* ___empty_error_14;
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2::commonName
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___commonName_15;
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2::email
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___email_16;
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509Certificate2::signedData
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___signedData_17;

public:
	inline static int32_t get_offset_of_empty_error_14() { return static_cast<int32_t>(offsetof(X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5_StaticFields, ___empty_error_14)); }
	inline String_t* get_empty_error_14() const { return ___empty_error_14; }
	inline String_t** get_address_of_empty_error_14() { return &___empty_error_14; }
	inline void set_empty_error_14(String_t* value)
	{
		___empty_error_14 = value;
		Il2CppCodeGenWriteBarrier((&___empty_error_14), value);
	}

	inline static int32_t get_offset_of_commonName_15() { return static_cast<int32_t>(offsetof(X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5_StaticFields, ___commonName_15)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_commonName_15() const { return ___commonName_15; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_commonName_15() { return &___commonName_15; }
	inline void set_commonName_15(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___commonName_15 = value;
		Il2CppCodeGenWriteBarrier((&___commonName_15), value);
	}

	inline static int32_t get_offset_of_email_16() { return static_cast<int32_t>(offsetof(X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5_StaticFields, ___email_16)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_email_16() const { return ___email_16; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_email_16() { return &___email_16; }
	inline void set_email_16(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___email_16 = value;
		Il2CppCodeGenWriteBarrier((&___email_16), value);
	}

	inline static int32_t get_offset_of_signedData_17() { return static_cast<int32_t>(offsetof(X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5_StaticFields, ___signedData_17)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_signedData_17() const { return ___signedData_17; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_signedData_17() { return &___signedData_17; }
	inline void set_signedData_17(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___signedData_17 = value;
		Il2CppCodeGenWriteBarrier((&___signedData_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATE2_T8488BFC28EE54980505493CD235FDDAC52A6B4A5_H
#ifndef X509CERTIFICATECOLLECTION_T4326FD1F39792B6FD659D19F8647F52BE2BBDDC7_H
#define X509CERTIFICATECOLLECTION_T4326FD1F39792B6FD659D19F8647F52BE2BBDDC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct  X509CertificateCollection_t4326FD1F39792B6FD659D19F8647F52BE2BBDDC7  : public CollectionBase_tC3F24120B23471E7FDEE72107D1D541E6456744F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATECOLLECTION_T4326FD1F39792B6FD659D19F8647F52BE2BBDDC7_H
#ifndef X509EXTENSION_TF50D0787240E5674487D37FDE5CCD9ABAA39F2EB_H
#define X509EXTENSION_TF50D0787240E5674487D37FDE5CCD9ABAA39F2EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Extension
struct  X509Extension_tF50D0787240E5674487D37FDE5CCD9ABAA39F2EB  : public AsnEncodedData_t1B888B0D340736ECF8B35405760DC0FE62235A99
{
public:
	// System.Boolean System.Security.Cryptography.X509Certificates.X509Extension::_critical
	bool ____critical_3;

public:
	inline static int32_t get_offset_of__critical_3() { return static_cast<int32_t>(offsetof(X509Extension_tF50D0787240E5674487D37FDE5CCD9ABAA39F2EB, ____critical_3)); }
	inline bool get__critical_3() const { return ____critical_3; }
	inline bool* get_address_of__critical_3() { return &____critical_3; }
	inline void set__critical_3(bool value)
	{
		____critical_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSION_TF50D0787240E5674487D37FDE5CCD9ABAA39F2EB_H
#ifndef SYSTEMEXCEPTION_T81A06ADD519539D252788D75E44AFFBE6580E301_H
#define SYSTEMEXCEPTION_T81A06ADD519539D252788D75E44AFFBE6580E301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t81A06ADD519539D252788D75E44AFFBE6580E301  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T81A06ADD519539D252788D75E44AFFBE6580E301_H
#ifndef GROUP_T4D7149047343E5C195C706AC7222A4492D9D3A4C_H
#define GROUP_T4D7149047343E5C195C706AC7222A4492D9D3A4C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Group
struct  Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C  : public Capture_t426EB72741D828F4539C131E1DC839DD00F01F22
{
public:
	// System.Boolean System.Text.RegularExpressions.Group::success
	bool ___success_4;
	// System.Text.RegularExpressions.CaptureCollection System.Text.RegularExpressions.Group::captures
	CaptureCollection_t5F80E90F8954A443D29D1EBEFC7119831DFB9C20 * ___captures_5;

public:
	inline static int32_t get_offset_of_success_4() { return static_cast<int32_t>(offsetof(Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C, ___success_4)); }
	inline bool get_success_4() const { return ___success_4; }
	inline bool* get_address_of_success_4() { return &___success_4; }
	inline void set_success_4(bool value)
	{
		___success_4 = value;
	}

	inline static int32_t get_offset_of_captures_5() { return static_cast<int32_t>(offsetof(Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C, ___captures_5)); }
	inline CaptureCollection_t5F80E90F8954A443D29D1EBEFC7119831DFB9C20 * get_captures_5() const { return ___captures_5; }
	inline CaptureCollection_t5F80E90F8954A443D29D1EBEFC7119831DFB9C20 ** get_address_of_captures_5() { return &___captures_5; }
	inline void set_captures_5(CaptureCollection_t5F80E90F8954A443D29D1EBEFC7119831DFB9C20 * value)
	{
		___captures_5 = value;
		Il2CppCodeGenWriteBarrier((&___captures_5), value);
	}
};

struct Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C_StaticFields
{
public:
	// System.Text.RegularExpressions.Group System.Text.RegularExpressions.Group::Fail
	Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C * ___Fail_3;

public:
	inline static int32_t get_offset_of_Fail_3() { return static_cast<int32_t>(offsetof(Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C_StaticFields, ___Fail_3)); }
	inline Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C * get_Fail_3() const { return ___Fail_3; }
	inline Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C ** get_address_of_Fail_3() { return &___Fail_3; }
	inline void set_Fail_3(Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C * value)
	{
		___Fail_3 = value;
		Il2CppCodeGenWriteBarrier((&___Fail_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUP_T4D7149047343E5C195C706AC7222A4492D9D3A4C_H
#ifndef LINKSTACK_T71FC86A248572C5736999BDA60C154F0AD923949_H
#define LINKSTACK_T71FC86A248572C5736999BDA60C154F0AD923949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.LinkStack
struct  LinkStack_t71FC86A248572C5736999BDA60C154F0AD923949  : public LinkRef_t4A317D95D5F6964647AE5BBB13F5CAE3F1C81FAB
{
public:
	// System.Collections.Stack System.Text.RegularExpressions.LinkStack::stack
	Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * ___stack_0;

public:
	inline static int32_t get_offset_of_stack_0() { return static_cast<int32_t>(offsetof(LinkStack_t71FC86A248572C5736999BDA60C154F0AD923949, ___stack_0)); }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * get_stack_0() const { return ___stack_0; }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 ** get_address_of_stack_0() { return &___stack_0; }
	inline void set_stack_0(Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * value)
	{
		___stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___stack_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKSTACK_T71FC86A248572C5736999BDA60C154F0AD923949_H
#ifndef MARK_T6F95F4C0F20F9D6B1059C179A2E3940267A6672F_H
#define MARK_T6F95F4C0F20F9D6B1059C179A2E3940267A6672F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Mark
struct  Mark_t6F95F4C0F20F9D6B1059C179A2E3940267A6672F 
{
public:
	// System.Int32 System.Text.RegularExpressions.Mark::Start
	int32_t ___Start_0;
	// System.Int32 System.Text.RegularExpressions.Mark::End
	int32_t ___End_1;
	// System.Int32 System.Text.RegularExpressions.Mark::Previous
	int32_t ___Previous_2;

public:
	inline static int32_t get_offset_of_Start_0() { return static_cast<int32_t>(offsetof(Mark_t6F95F4C0F20F9D6B1059C179A2E3940267A6672F, ___Start_0)); }
	inline int32_t get_Start_0() const { return ___Start_0; }
	inline int32_t* get_address_of_Start_0() { return &___Start_0; }
	inline void set_Start_0(int32_t value)
	{
		___Start_0 = value;
	}

	inline static int32_t get_offset_of_End_1() { return static_cast<int32_t>(offsetof(Mark_t6F95F4C0F20F9D6B1059C179A2E3940267A6672F, ___End_1)); }
	inline int32_t get_End_1() const { return ___End_1; }
	inline int32_t* get_address_of_End_1() { return &___End_1; }
	inline void set_End_1(int32_t value)
	{
		___End_1 = value;
	}

	inline static int32_t get_offset_of_Previous_2() { return static_cast<int32_t>(offsetof(Mark_t6F95F4C0F20F9D6B1059C179A2E3940267A6672F, ___Previous_2)); }
	inline int32_t get_Previous_2() const { return ___Previous_2; }
	inline int32_t* get_address_of_Previous_2() { return &___Previous_2; }
	inline void set_Previous_2(int32_t value)
	{
		___Previous_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARK_T6F95F4C0F20F9D6B1059C179A2E3940267A6672F_H
#ifndef LINK_T52781900883945F8B1D533580C6399C413418639_H
#define LINK_T52781900883945F8B1D533580C6399C413418639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link
struct  Link_t52781900883945F8B1D533580C6399C413418639 
{
public:
	// System.Int32 System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link::base_addr
	int32_t ___base_addr_0;
	// System.Int32 System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link::offset_addr
	int32_t ___offset_addr_1;

public:
	inline static int32_t get_offset_of_base_addr_0() { return static_cast<int32_t>(offsetof(Link_t52781900883945F8B1D533580C6399C413418639, ___base_addr_0)); }
	inline int32_t get_base_addr_0() const { return ___base_addr_0; }
	inline int32_t* get_address_of_base_addr_0() { return &___base_addr_0; }
	inline void set_base_addr_0(int32_t value)
	{
		___base_addr_0 = value;
	}

	inline static int32_t get_offset_of_offset_addr_1() { return static_cast<int32_t>(offsetof(Link_t52781900883945F8B1D533580C6399C413418639, ___offset_addr_1)); }
	inline int32_t get_offset_addr_1() const { return ___offset_addr_1; }
	inline int32_t* get_address_of_offset_addr_1() { return &___offset_addr_1; }
	inline void set_offset_addr_1(int32_t value)
	{
		___offset_addr_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINK_T52781900883945F8B1D533580C6399C413418639_H
#ifndef TIMESPAN_T26DE4373EA745C072A5FBDEAD8373348955FC374_H
#define TIMESPAN_T26DE4373EA745C072A5FBDEAD8373348955FC374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  ___Zero_2;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374_StaticFields, ___MaxValue_0)); }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  get_MaxValue_0() const { return ___MaxValue_0; }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374_StaticFields, ___MinValue_1)); }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  get_MinValue_1() const { return ___MinValue_1; }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  value)
	{
		___MinValue_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374_StaticFields, ___Zero_2)); }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  get_Zero_2() const { return ___Zero_2; }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T26DE4373EA745C072A5FBDEAD8373348955FC374_H
#ifndef OSX509CERTIFICATES_T07B156FA2DE1D5DC37A0503B93560A2A2E920203_H
#define OSX509CERTIFICATES_T07B156FA2DE1D5DC37A0503B93560A2A2E920203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.OSX509Certificates
struct  OSX509Certificates_t07B156FA2DE1D5DC37A0503B93560A2A2E920203  : public RuntimeObject
{
public:

public:
};

struct OSX509Certificates_t07B156FA2DE1D5DC37A0503B93560A2A2E920203_StaticFields
{
public:
	// System.IntPtr Mono.Security.X509.OSX509Certificates::sslsecpolicy
	intptr_t ___sslsecpolicy_0;

public:
	inline static int32_t get_offset_of_sslsecpolicy_0() { return static_cast<int32_t>(offsetof(OSX509Certificates_t07B156FA2DE1D5DC37A0503B93560A2A2E920203_StaticFields, ___sslsecpolicy_0)); }
	inline intptr_t get_sslsecpolicy_0() const { return ___sslsecpolicy_0; }
	inline intptr_t* get_address_of_sslsecpolicy_0() { return &___sslsecpolicy_0; }
	inline void set_sslsecpolicy_0(intptr_t value)
	{
		___sslsecpolicy_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSX509CERTIFICATES_T07B156FA2DE1D5DC37A0503B93560A2A2E920203_H
#ifndef SECTRUSTRESULT_TE77308635F0160DD7AC688C7F82B760500E274C6_H
#define SECTRUSTRESULT_TE77308635F0160DD7AC688C7F82B760500E274C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.OSX509Certificates/SecTrustResult
struct  SecTrustResult_tE77308635F0160DD7AC688C7F82B760500E274C6 
{
public:
	// System.Int32 Mono.Security.X509.OSX509Certificates/SecTrustResult::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SecTrustResult_tE77308635F0160DD7AC688C7F82B760500E274C6, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECTRUSTRESULT_TE77308635F0160DD7AC688C7F82B760500E274C6_H
#ifndef DATETIMEKIND_T922DC40070B8AD59CB650E3FE023C4A58ED4568A_H
#define DATETIMEKIND_T922DC40070B8AD59CB650E3FE023C4A58ED4568A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t922DC40070B8AD59CB650E3FE023C4A58ED4568A 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t922DC40070B8AD59CB650E3FE023C4A58ED4568A, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T922DC40070B8AD59CB650E3FE023C4A58ED4568A_H
#ifndef INVALIDOPERATIONEXCEPTION_TCE0CC6C0C8EE2E780C0736B79AA673751C79CC07_H
#define INVALIDOPERATIONEXCEPTION_TCE0CC6C0C8EE2E780C0736B79AA673751C79CC07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidOperationException
struct  InvalidOperationException_tCE0CC6C0C8EE2E780C0736B79AA673751C79CC07  : public SystemException_t81A06ADD519539D252788D75E44AFFBE6580E301
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDOPERATIONEXCEPTION_TCE0CC6C0C8EE2E780C0736B79AA673751C79CC07_H
#ifndef READSTATE_TBACF66C66CEED1AE759539D70F338F360B83F031_H
#define READSTATE_TBACF66C66CEED1AE759539D70F338F360B83F031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ReadState
struct  ReadState_tBACF66C66CEED1AE759539D70F338F360B83F031 
{
public:
	// System.Int32 System.Net.ReadState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReadState_tBACF66C66CEED1AE759539D70F338F360B83F031, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READSTATE_TBACF66C66CEED1AE759539D70F338F360B83F031_H
#ifndef AUTHENTICATIONLEVEL_TDD7C55ABAF179BD6273AA3DC6AC59A265B006E17_H
#define AUTHENTICATIONLEVEL_TDD7C55ABAF179BD6273AA3DC6AC59A265B006E17_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Security.AuthenticationLevel
struct  AuthenticationLevel_tDD7C55ABAF179BD6273AA3DC6AC59A265B006E17 
{
public:
	// System.Int32 System.Net.Security.AuthenticationLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AuthenticationLevel_tDD7C55ABAF179BD6273AA3DC6AC59A265B006E17, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONLEVEL_TDD7C55ABAF179BD6273AA3DC6AC59A265B006E17_H
#ifndef SECURITYPROTOCOLTYPE_T5B94575EDC844130CC7451E034953844842D19E8_H
#define SECURITYPROTOCOLTYPE_T5B94575EDC844130CC7451E034953844842D19E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.SecurityProtocolType
struct  SecurityProtocolType_t5B94575EDC844130CC7451E034953844842D19E8 
{
public:
	// System.Int32 System.Net.SecurityProtocolType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SecurityProtocolType_t5B94575EDC844130CC7451E034953844842D19E8, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYPROTOCOLTYPE_T5B94575EDC844130CC7451E034953844842D19E8_H
#ifndef ADDRESSFAMILY_T56E41B784F73C1D53BE91A309AB9E5A35F947876_H
#define ADDRESSFAMILY_T56E41B784F73C1D53BE91A309AB9E5A35F947876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.AddressFamily
struct  AddressFamily_t56E41B784F73C1D53BE91A309AB9E5A35F947876 
{
public:
	// System.Int32 System.Net.Sockets.AddressFamily::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AddressFamily_t56E41B784F73C1D53BE91A309AB9E5A35F947876, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDRESSFAMILY_T56E41B784F73C1D53BE91A309AB9E5A35F947876_H
#ifndef WEBEXCEPTIONSTATUS_TD0D1FE14644E00431235FA509F94632D27DCCBA4_H
#define WEBEXCEPTIONSTATUS_TD0D1FE14644E00431235FA509F94632D27DCCBA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebExceptionStatus
struct  WebExceptionStatus_tD0D1FE14644E00431235FA509F94632D27DCCBA4 
{
public:
	// System.Int32 System.Net.WebExceptionStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WebExceptionStatus_tD0D1FE14644E00431235FA509F94632D27DCCBA4, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBEXCEPTIONSTATUS_TD0D1FE14644E00431235FA509F94632D27DCCBA4_H
#ifndef WEBHEADERCOLLECTION_T01C9818A1AB6381C83A59AD104E8F0092AC87B66_H
#define WEBHEADERCOLLECTION_T01C9818A1AB6381C83A59AD104E8F0092AC87B66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebHeaderCollection
struct  WebHeaderCollection_t01C9818A1AB6381C83A59AD104E8F0092AC87B66  : public NameValueCollection_tB87E7DD2A8341E8E48EEC4D39322C1CE72E103EF
{
public:
	// System.Boolean System.Net.WebHeaderCollection::internallyCreated
	bool ___internallyCreated_15;

public:
	inline static int32_t get_offset_of_internallyCreated_15() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t01C9818A1AB6381C83A59AD104E8F0092AC87B66, ___internallyCreated_15)); }
	inline bool get_internallyCreated_15() const { return ___internallyCreated_15; }
	inline bool* get_address_of_internallyCreated_15() { return &___internallyCreated_15; }
	inline void set_internallyCreated_15(bool value)
	{
		___internallyCreated_15 = value;
	}
};

struct WebHeaderCollection_t01C9818A1AB6381C83A59AD104E8F0092AC87B66_StaticFields
{
public:
	// System.Collections.Hashtable System.Net.WebHeaderCollection::restricted
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___restricted_12;
	// System.Collections.Hashtable System.Net.WebHeaderCollection::multiValue
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___multiValue_13;
	// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> System.Net.WebHeaderCollection::restricted_response
	Dictionary_2_t7C989490AC01A6A5F45395A5CA72FAF4B7D59FF2 * ___restricted_response_14;
	// System.Boolean[] System.Net.WebHeaderCollection::allowed_chars
	BooleanU5BU5D_tBFF3659658EB283EF564C8FFB3A1676CEDE2E66E* ___allowed_chars_16;

public:
	inline static int32_t get_offset_of_restricted_12() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t01C9818A1AB6381C83A59AD104E8F0092AC87B66_StaticFields, ___restricted_12)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_restricted_12() const { return ___restricted_12; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_restricted_12() { return &___restricted_12; }
	inline void set_restricted_12(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___restricted_12 = value;
		Il2CppCodeGenWriteBarrier((&___restricted_12), value);
	}

	inline static int32_t get_offset_of_multiValue_13() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t01C9818A1AB6381C83A59AD104E8F0092AC87B66_StaticFields, ___multiValue_13)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_multiValue_13() const { return ___multiValue_13; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_multiValue_13() { return &___multiValue_13; }
	inline void set_multiValue_13(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___multiValue_13 = value;
		Il2CppCodeGenWriteBarrier((&___multiValue_13), value);
	}

	inline static int32_t get_offset_of_restricted_response_14() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t01C9818A1AB6381C83A59AD104E8F0092AC87B66_StaticFields, ___restricted_response_14)); }
	inline Dictionary_2_t7C989490AC01A6A5F45395A5CA72FAF4B7D59FF2 * get_restricted_response_14() const { return ___restricted_response_14; }
	inline Dictionary_2_t7C989490AC01A6A5F45395A5CA72FAF4B7D59FF2 ** get_address_of_restricted_response_14() { return &___restricted_response_14; }
	inline void set_restricted_response_14(Dictionary_2_t7C989490AC01A6A5F45395A5CA72FAF4B7D59FF2 * value)
	{
		___restricted_response_14 = value;
		Il2CppCodeGenWriteBarrier((&___restricted_response_14), value);
	}

	inline static int32_t get_offset_of_allowed_chars_16() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t01C9818A1AB6381C83A59AD104E8F0092AC87B66_StaticFields, ___allowed_chars_16)); }
	inline BooleanU5BU5D_tBFF3659658EB283EF564C8FFB3A1676CEDE2E66E* get_allowed_chars_16() const { return ___allowed_chars_16; }
	inline BooleanU5BU5D_tBFF3659658EB283EF564C8FFB3A1676CEDE2E66E** get_address_of_allowed_chars_16() { return &___allowed_chars_16; }
	inline void set_allowed_chars_16(BooleanU5BU5D_tBFF3659658EB283EF564C8FFB3A1676CEDE2E66E* value)
	{
		___allowed_chars_16 = value;
		Il2CppCodeGenWriteBarrier((&___allowed_chars_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBHEADERCOLLECTION_T01C9818A1AB6381C83A59AD104E8F0092AC87B66_H
#ifndef SSLPROTOCOLS_T6EEE3A59E36DE35F665BE090A90282EB3BDEF3DF_H
#define SSLPROTOCOLS_T6EEE3A59E36DE35F665BE090A90282EB3BDEF3DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Authentication.SslProtocols
struct  SslProtocols_t6EEE3A59E36DE35F665BE090A90282EB3BDEF3DF 
{
public:
	// System.Int32 System.Security.Authentication.SslProtocols::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SslProtocols_t6EEE3A59E36DE35F665BE090A90282EB3BDEF3DF, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLPROTOCOLS_T6EEE3A59E36DE35F665BE090A90282EB3BDEF3DF_H
#ifndef ASNDECODESTATUS_T0649A9770A72159487713F9957A30247B87DE040_H
#define ASNDECODESTATUS_T0649A9770A72159487713F9957A30247B87DE040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AsnDecodeStatus
struct  AsnDecodeStatus_t0649A9770A72159487713F9957A30247B87DE040 
{
public:
	// System.Int32 System.Security.Cryptography.AsnDecodeStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AsnDecodeStatus_t0649A9770A72159487713F9957A30247B87DE040, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASNDECODESTATUS_T0649A9770A72159487713F9957A30247B87DE040_H
#ifndef OPENFLAGS_TCE5CDA06DE32E8C9BCAF8B1B6EC7325C8BDD897F_H
#define OPENFLAGS_TCE5CDA06DE32E8C9BCAF8B1B6EC7325C8BDD897F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.OpenFlags
struct  OpenFlags_tCE5CDA06DE32E8C9BCAF8B1B6EC7325C8BDD897F 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.OpenFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OpenFlags_tCE5CDA06DE32E8C9BCAF8B1B6EC7325C8BDD897F, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENFLAGS_TCE5CDA06DE32E8C9BCAF8B1B6EC7325C8BDD897F_H
#ifndef STORELOCATION_T4442B9177DB4E4A9EF5FDDC197E46358A3B80A05_H
#define STORELOCATION_T4442B9177DB4E4A9EF5FDDC197E46358A3B80A05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.StoreLocation
struct  StoreLocation_t4442B9177DB4E4A9EF5FDDC197E46358A3B80A05 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.StoreLocation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StoreLocation_t4442B9177DB4E4A9EF5FDDC197E46358A3B80A05, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORELOCATION_T4442B9177DB4E4A9EF5FDDC197E46358A3B80A05_H
#ifndef STORENAME_T4AC545078CCC3A79483FAB52CA791DB7DD605441_H
#define STORENAME_T4AC545078CCC3A79483FAB52CA791DB7DD605441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.StoreName
struct  StoreName_t4AC545078CCC3A79483FAB52CA791DB7DD605441 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.StoreName::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StoreName_t4AC545078CCC3A79483FAB52CA791DB7DD605441, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORENAME_T4AC545078CCC3A79483FAB52CA791DB7DD605441_H
#ifndef X500DISTINGUISHEDNAMEFLAGS_T415EAC17471529A4922D15E25AAE8F85B9A419FB_H
#define X500DISTINGUISHEDNAMEFLAGS_T415EAC17471529A4922D15E25AAE8F85B9A419FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags
struct  X500DistinguishedNameFlags_t415EAC17471529A4922D15E25AAE8F85B9A419FB 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X500DistinguishedNameFlags_t415EAC17471529A4922D15E25AAE8F85B9A419FB, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X500DISTINGUISHEDNAMEFLAGS_T415EAC17471529A4922D15E25AAE8F85B9A419FB_H
#ifndef X509CERTIFICATE2COLLECTION_T5F3686D1CB11C6BD038CE9D964F5A4B9BCDF532E_H
#define X509CERTIFICATE2COLLECTION_T5F3686D1CB11C6BD038CE9D964F5A4B9BCDF532E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Certificate2Collection
struct  X509Certificate2Collection_t5F3686D1CB11C6BD038CE9D964F5A4B9BCDF532E  : public X509CertificateCollection_t4326FD1F39792B6FD659D19F8647F52BE2BBDDC7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATE2COLLECTION_T5F3686D1CB11C6BD038CE9D964F5A4B9BCDF532E_H
#ifndef X509CHAINSTATUSFLAGS_T11227BB58FC95D583FF9B1D93FA0335A8310BCB1_H
#define X509CHAINSTATUSFLAGS_T11227BB58FC95D583FF9B1D93FA0335A8310BCB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags
struct  X509ChainStatusFlags_t11227BB58FC95D583FF9B1D93FA0335A8310BCB1 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509ChainStatusFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509ChainStatusFlags_t11227BB58FC95D583FF9B1D93FA0335A8310BCB1, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINSTATUSFLAGS_T11227BB58FC95D583FF9B1D93FA0335A8310BCB1_H
#ifndef X509FINDTYPE_T12DCBE925CCC33AEFE74B273543FE8D71618C9FB_H
#define X509FINDTYPE_T12DCBE925CCC33AEFE74B273543FE8D71618C9FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509FindType
struct  X509FindType_t12DCBE925CCC33AEFE74B273543FE8D71618C9FB 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509FindType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509FindType_t12DCBE925CCC33AEFE74B273543FE8D71618C9FB, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509FINDTYPE_T12DCBE925CCC33AEFE74B273543FE8D71618C9FB_H
#ifndef X509KEYUSAGEFLAGS_TFA401F7CBC9D4D64E760500B3AE3A398203456A5_H
#define X509KEYUSAGEFLAGS_TFA401F7CBC9D4D64E760500B3AE3A398203456A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags
struct  X509KeyUsageFlags_tFA401F7CBC9D4D64E760500B3AE3A398203456A5 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509KeyUsageFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509KeyUsageFlags_tFA401F7CBC9D4D64E760500B3AE3A398203456A5, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509KEYUSAGEFLAGS_TFA401F7CBC9D4D64E760500B3AE3A398203456A5_H
#ifndef X509NAMETYPE_TDBD3206098FC13285BAEAA7ED247C56A080BF071_H
#define X509NAMETYPE_TDBD3206098FC13285BAEAA7ED247C56A080BF071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509NameType
struct  X509NameType_tDBD3206098FC13285BAEAA7ED247C56A080BF071 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509NameType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509NameType_tDBD3206098FC13285BAEAA7ED247C56A080BF071, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509NAMETYPE_TDBD3206098FC13285BAEAA7ED247C56A080BF071_H
#ifndef X509REVOCATIONFLAG_TCCD32E1F66EFE64752DDF4994639BA75A005DF5C_H
#define X509REVOCATIONFLAG_TCCD32E1F66EFE64752DDF4994639BA75A005DF5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509RevocationFlag
struct  X509RevocationFlag_tCCD32E1F66EFE64752DDF4994639BA75A005DF5C 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509RevocationFlag::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509RevocationFlag_tCCD32E1F66EFE64752DDF4994639BA75A005DF5C, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509REVOCATIONFLAG_TCCD32E1F66EFE64752DDF4994639BA75A005DF5C_H
#ifndef X509REVOCATIONMODE_T5866243C7CDD857B4492778EB31086BB85AF8416_H
#define X509REVOCATIONMODE_T5866243C7CDD857B4492778EB31086BB85AF8416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509RevocationMode
struct  X509RevocationMode_t5866243C7CDD857B4492778EB31086BB85AF8416 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509RevocationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509RevocationMode_t5866243C7CDD857B4492778EB31086BB85AF8416, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509REVOCATIONMODE_T5866243C7CDD857B4492778EB31086BB85AF8416_H
#ifndef X509SUBJECTKEYIDENTIFIERHASHALGORITHM_TAAFC5B2151781F2930AA2219EB0D8E3CAD8C6AD8_H
#define X509SUBJECTKEYIDENTIFIERHASHALGORITHM_TAAFC5B2151781F2930AA2219EB0D8E3CAD8C6AD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm
struct  X509SubjectKeyIdentifierHashAlgorithm_tAAFC5B2151781F2930AA2219EB0D8E3CAD8C6AD8 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509SubjectKeyIdentifierHashAlgorithm_tAAFC5B2151781F2930AA2219EB0D8E3CAD8C6AD8, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509SUBJECTKEYIDENTIFIERHASHALGORITHM_TAAFC5B2151781F2930AA2219EB0D8E3CAD8C6AD8_H
#ifndef X509VERIFICATIONFLAGS_T6F8A7F75FAD2E7C9F75AB3F022F19C42BA358F42_H
#define X509VERIFICATIONFLAGS_T6F8A7F75FAD2E7C9F75AB3F022F19C42BA358F42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509VerificationFlags
struct  X509VerificationFlags_t6F8A7F75FAD2E7C9F75AB3F022F19C42BA358F42 
{
public:
	// System.Int32 System.Security.Cryptography.X509Certificates.X509VerificationFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509VerificationFlags_t6F8A7F75FAD2E7C9F75AB3F022F19C42BA358F42, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509VERIFICATIONFLAGS_T6F8A7F75FAD2E7C9F75AB3F022F19C42BA358F42_H
#ifndef CATEGORY_T4811E82728B9344F24776EE98B58FC11546B342C_H
#define CATEGORY_T4811E82728B9344F24776EE98B58FC11546B342C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Category
struct  Category_t4811E82728B9344F24776EE98B58FC11546B342C 
{
public:
	// System.UInt16 System.Text.RegularExpressions.Category::value__
	uint16_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Category_t4811E82728B9344F24776EE98B58FC11546B342C, ___value___1)); }
	inline uint16_t get_value___1() const { return ___value___1; }
	inline uint16_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint16_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CATEGORY_T4811E82728B9344F24776EE98B58FC11546B342C_H
#ifndef MATCH_T2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53_H
#define MATCH_T2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Match
struct  Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53  : public Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C
{
public:
	// System.Text.RegularExpressions.Regex System.Text.RegularExpressions.Match::regex
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B * ___regex_6;
	// System.Text.RegularExpressions.IMachine System.Text.RegularExpressions.Match::machine
	RuntimeObject* ___machine_7;
	// System.Int32 System.Text.RegularExpressions.Match::text_length
	int32_t ___text_length_8;
	// System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::groups
	GroupCollection_tBEA4EDB210F04963227CBC58E1434202F3CEC968 * ___groups_9;

public:
	inline static int32_t get_offset_of_regex_6() { return static_cast<int32_t>(offsetof(Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53, ___regex_6)); }
	inline Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B * get_regex_6() const { return ___regex_6; }
	inline Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B ** get_address_of_regex_6() { return &___regex_6; }
	inline void set_regex_6(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B * value)
	{
		___regex_6 = value;
		Il2CppCodeGenWriteBarrier((&___regex_6), value);
	}

	inline static int32_t get_offset_of_machine_7() { return static_cast<int32_t>(offsetof(Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53, ___machine_7)); }
	inline RuntimeObject* get_machine_7() const { return ___machine_7; }
	inline RuntimeObject** get_address_of_machine_7() { return &___machine_7; }
	inline void set_machine_7(RuntimeObject* value)
	{
		___machine_7 = value;
		Il2CppCodeGenWriteBarrier((&___machine_7), value);
	}

	inline static int32_t get_offset_of_text_length_8() { return static_cast<int32_t>(offsetof(Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53, ___text_length_8)); }
	inline int32_t get_text_length_8() const { return ___text_length_8; }
	inline int32_t* get_address_of_text_length_8() { return &___text_length_8; }
	inline void set_text_length_8(int32_t value)
	{
		___text_length_8 = value;
	}

	inline static int32_t get_offset_of_groups_9() { return static_cast<int32_t>(offsetof(Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53, ___groups_9)); }
	inline GroupCollection_tBEA4EDB210F04963227CBC58E1434202F3CEC968 * get_groups_9() const { return ___groups_9; }
	inline GroupCollection_tBEA4EDB210F04963227CBC58E1434202F3CEC968 ** get_address_of_groups_9() { return &___groups_9; }
	inline void set_groups_9(GroupCollection_tBEA4EDB210F04963227CBC58E1434202F3CEC968 * value)
	{
		___groups_9 = value;
		Il2CppCodeGenWriteBarrier((&___groups_9), value);
	}
};

struct Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53_StaticFields
{
public:
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.Match::empty
	Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53 * ___empty_10;

public:
	inline static int32_t get_offset_of_empty_10() { return static_cast<int32_t>(offsetof(Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53_StaticFields, ___empty_10)); }
	inline Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53 * get_empty_10() const { return ___empty_10; }
	inline Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53 ** get_address_of_empty_10() { return &___empty_10; }
	inline void set_empty_10(Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53 * value)
	{
		___empty_10 = value;
		Il2CppCodeGenWriteBarrier((&___empty_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCH_T2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53_H
#ifndef OPCODE_T5566B50EEFDECA010CAB22202F40D1B5799D23F7_H
#define OPCODE_T5566B50EEFDECA010CAB22202F40D1B5799D23F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.OpCode
struct  OpCode_t5566B50EEFDECA010CAB22202F40D1B5799D23F7 
{
public:
	// System.UInt16 System.Text.RegularExpressions.OpCode::value__
	uint16_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OpCode_t5566B50EEFDECA010CAB22202F40D1B5799D23F7, ___value___1)); }
	inline uint16_t get_value___1() const { return ___value___1; }
	inline uint16_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint16_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPCODE_T5566B50EEFDECA010CAB22202F40D1B5799D23F7_H
#ifndef OPFLAGS_T5688D3EFBB323A2AA84AAABADD74AB885286AD32_H
#define OPFLAGS_T5688D3EFBB323A2AA84AAABADD74AB885286AD32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.OpFlags
struct  OpFlags_t5688D3EFBB323A2AA84AAABADD74AB885286AD32 
{
public:
	// System.UInt16 System.Text.RegularExpressions.OpFlags::value__
	uint16_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OpFlags_t5688D3EFBB323A2AA84AAABADD74AB885286AD32, ___value___1)); }
	inline uint16_t get_value___1() const { return ___value___1; }
	inline uint16_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint16_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPFLAGS_T5688D3EFBB323A2AA84AAABADD74AB885286AD32_H
#ifndef PATTERNLINKSTACK_T984F6CAC34DFB10A38B61E62BAE99F57C35007F5_H
#define PATTERNLINKSTACK_T984F6CAC34DFB10A38B61E62BAE99F57C35007F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack
struct  PatternLinkStack_t984F6CAC34DFB10A38B61E62BAE99F57C35007F5  : public LinkStack_t71FC86A248572C5736999BDA60C154F0AD923949
{
public:
	// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::link
	Link_t52781900883945F8B1D533580C6399C413418639  ___link_1;

public:
	inline static int32_t get_offset_of_link_1() { return static_cast<int32_t>(offsetof(PatternLinkStack_t984F6CAC34DFB10A38B61E62BAE99F57C35007F5, ___link_1)); }
	inline Link_t52781900883945F8B1D533580C6399C413418639  get_link_1() const { return ___link_1; }
	inline Link_t52781900883945F8B1D533580C6399C413418639 * get_address_of_link_1() { return &___link_1; }
	inline void set_link_1(Link_t52781900883945F8B1D533580C6399C413418639  value)
	{
		___link_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATTERNLINKSTACK_T984F6CAC34DFB10A38B61E62BAE99F57C35007F5_H
#ifndef POSITION_T19E037E87BCB1E737603EA15C59A22D1C2E34C1C_H
#define POSITION_T19E037E87BCB1E737603EA15C59A22D1C2E34C1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Position
struct  Position_t19E037E87BCB1E737603EA15C59A22D1C2E34C1C 
{
public:
	// System.UInt16 System.Text.RegularExpressions.Position::value__
	uint16_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Position_t19E037E87BCB1E737603EA15C59A22D1C2E34C1C, ___value___1)); }
	inline uint16_t get_value___1() const { return ___value___1; }
	inline uint16_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint16_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITION_T19E037E87BCB1E737603EA15C59A22D1C2E34C1C_H
#ifndef REGEXOPTIONS_T6DDB25DC8ABB8CFC65500D99B4CDAFF3B26D0624_H
#define REGEXOPTIONS_T6DDB25DC8ABB8CFC65500D99B4CDAFF3B26D0624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexOptions
struct  RegexOptions_t6DDB25DC8ABB8CFC65500D99B4CDAFF3B26D0624 
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RegexOptions_t6DDB25DC8ABB8CFC65500D99B4CDAFF3B26D0624, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXOPTIONS_T6DDB25DC8ABB8CFC65500D99B4CDAFF3B26D0624_H
#ifndef DATETIME_T816BBD9125EA02B42B87CB07643FC5F803391DA2_H
#define DATETIME_T816BBD9125EA02B42B87CB07643FC5F803391DA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2, ___ticks_0)); }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___MaxValue_2)); }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___MinValue_3)); }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T816BBD9125EA02B42B87CB07643FC5F803391DA2_H
#ifndef IPADDRESS_T620453DAA3165B1BFB11794B30FF82CE4D5026D4_H
#define IPADDRESS_T620453DAA3165B1BFB11794B30FF82CE4D5026D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPAddress
struct  IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4  : public RuntimeObject
{
public:
	// System.Int64 System.Net.IPAddress::m_Address
	int64_t ___m_Address_0;
	// System.Net.Sockets.AddressFamily System.Net.IPAddress::m_Family
	int32_t ___m_Family_1;
	// System.UInt16[] System.Net.IPAddress::m_Numbers
	UInt16U5BU5D_tFACC39F5AB94FCCEAD5ED082922B0263437F9ABC* ___m_Numbers_2;
	// System.Int64 System.Net.IPAddress::m_ScopeId
	int64_t ___m_ScopeId_3;
	// System.Int32 System.Net.IPAddress::m_HashCode
	int32_t ___m_HashCode_11;

public:
	inline static int32_t get_offset_of_m_Address_0() { return static_cast<int32_t>(offsetof(IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4, ___m_Address_0)); }
	inline int64_t get_m_Address_0() const { return ___m_Address_0; }
	inline int64_t* get_address_of_m_Address_0() { return &___m_Address_0; }
	inline void set_m_Address_0(int64_t value)
	{
		___m_Address_0 = value;
	}

	inline static int32_t get_offset_of_m_Family_1() { return static_cast<int32_t>(offsetof(IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4, ___m_Family_1)); }
	inline int32_t get_m_Family_1() const { return ___m_Family_1; }
	inline int32_t* get_address_of_m_Family_1() { return &___m_Family_1; }
	inline void set_m_Family_1(int32_t value)
	{
		___m_Family_1 = value;
	}

	inline static int32_t get_offset_of_m_Numbers_2() { return static_cast<int32_t>(offsetof(IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4, ___m_Numbers_2)); }
	inline UInt16U5BU5D_tFACC39F5AB94FCCEAD5ED082922B0263437F9ABC* get_m_Numbers_2() const { return ___m_Numbers_2; }
	inline UInt16U5BU5D_tFACC39F5AB94FCCEAD5ED082922B0263437F9ABC** get_address_of_m_Numbers_2() { return &___m_Numbers_2; }
	inline void set_m_Numbers_2(UInt16U5BU5D_tFACC39F5AB94FCCEAD5ED082922B0263437F9ABC* value)
	{
		___m_Numbers_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Numbers_2), value);
	}

	inline static int32_t get_offset_of_m_ScopeId_3() { return static_cast<int32_t>(offsetof(IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4, ___m_ScopeId_3)); }
	inline int64_t get_m_ScopeId_3() const { return ___m_ScopeId_3; }
	inline int64_t* get_address_of_m_ScopeId_3() { return &___m_ScopeId_3; }
	inline void set_m_ScopeId_3(int64_t value)
	{
		___m_ScopeId_3 = value;
	}

	inline static int32_t get_offset_of_m_HashCode_11() { return static_cast<int32_t>(offsetof(IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4, ___m_HashCode_11)); }
	inline int32_t get_m_HashCode_11() const { return ___m_HashCode_11; }
	inline int32_t* get_address_of_m_HashCode_11() { return &___m_HashCode_11; }
	inline void set_m_HashCode_11(int32_t value)
	{
		___m_HashCode_11 = value;
	}
};

struct IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4_StaticFields
{
public:
	// System.Net.IPAddress System.Net.IPAddress::Any
	IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 * ___Any_4;
	// System.Net.IPAddress System.Net.IPAddress::Broadcast
	IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 * ___Broadcast_5;
	// System.Net.IPAddress System.Net.IPAddress::Loopback
	IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 * ___Loopback_6;
	// System.Net.IPAddress System.Net.IPAddress::None
	IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 * ___None_7;
	// System.Net.IPAddress System.Net.IPAddress::IPv6Any
	IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 * ___IPv6Any_8;
	// System.Net.IPAddress System.Net.IPAddress::IPv6Loopback
	IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 * ___IPv6Loopback_9;
	// System.Net.IPAddress System.Net.IPAddress::IPv6None
	IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 * ___IPv6None_10;

public:
	inline static int32_t get_offset_of_Any_4() { return static_cast<int32_t>(offsetof(IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4_StaticFields, ___Any_4)); }
	inline IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 * get_Any_4() const { return ___Any_4; }
	inline IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 ** get_address_of_Any_4() { return &___Any_4; }
	inline void set_Any_4(IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 * value)
	{
		___Any_4 = value;
		Il2CppCodeGenWriteBarrier((&___Any_4), value);
	}

	inline static int32_t get_offset_of_Broadcast_5() { return static_cast<int32_t>(offsetof(IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4_StaticFields, ___Broadcast_5)); }
	inline IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 * get_Broadcast_5() const { return ___Broadcast_5; }
	inline IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 ** get_address_of_Broadcast_5() { return &___Broadcast_5; }
	inline void set_Broadcast_5(IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 * value)
	{
		___Broadcast_5 = value;
		Il2CppCodeGenWriteBarrier((&___Broadcast_5), value);
	}

	inline static int32_t get_offset_of_Loopback_6() { return static_cast<int32_t>(offsetof(IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4_StaticFields, ___Loopback_6)); }
	inline IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 * get_Loopback_6() const { return ___Loopback_6; }
	inline IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 ** get_address_of_Loopback_6() { return &___Loopback_6; }
	inline void set_Loopback_6(IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 * value)
	{
		___Loopback_6 = value;
		Il2CppCodeGenWriteBarrier((&___Loopback_6), value);
	}

	inline static int32_t get_offset_of_None_7() { return static_cast<int32_t>(offsetof(IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4_StaticFields, ___None_7)); }
	inline IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 * get_None_7() const { return ___None_7; }
	inline IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 ** get_address_of_None_7() { return &___None_7; }
	inline void set_None_7(IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 * value)
	{
		___None_7 = value;
		Il2CppCodeGenWriteBarrier((&___None_7), value);
	}

	inline static int32_t get_offset_of_IPv6Any_8() { return static_cast<int32_t>(offsetof(IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4_StaticFields, ___IPv6Any_8)); }
	inline IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 * get_IPv6Any_8() const { return ___IPv6Any_8; }
	inline IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 ** get_address_of_IPv6Any_8() { return &___IPv6Any_8; }
	inline void set_IPv6Any_8(IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 * value)
	{
		___IPv6Any_8 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6Any_8), value);
	}

	inline static int32_t get_offset_of_IPv6Loopback_9() { return static_cast<int32_t>(offsetof(IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4_StaticFields, ___IPv6Loopback_9)); }
	inline IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 * get_IPv6Loopback_9() const { return ___IPv6Loopback_9; }
	inline IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 ** get_address_of_IPv6Loopback_9() { return &___IPv6Loopback_9; }
	inline void set_IPv6Loopback_9(IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 * value)
	{
		___IPv6Loopback_9 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6Loopback_9), value);
	}

	inline static int32_t get_offset_of_IPv6None_10() { return static_cast<int32_t>(offsetof(IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4_StaticFields, ___IPv6None_10)); }
	inline IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 * get_IPv6None_10() const { return ___IPv6None_10; }
	inline IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 ** get_address_of_IPv6None_10() { return &___IPv6None_10; }
	inline void set_IPv6None_10(IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4 * value)
	{
		___IPv6None_10 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6None_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPADDRESS_T620453DAA3165B1BFB11794B30FF82CE4D5026D4_H
#ifndef PROTOCOLVIOLATIONEXCEPTION_TE3B266A71DF466371447A93361C448E63DFC51EC_H
#define PROTOCOLVIOLATIONEXCEPTION_TE3B266A71DF466371447A93361C448E63DFC51EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ProtocolViolationException
struct  ProtocolViolationException_tE3B266A71DF466371447A93361C448E63DFC51EC  : public InvalidOperationException_tCE0CC6C0C8EE2E780C0736B79AA673751C79CC07
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTOCOLVIOLATIONEXCEPTION_TE3B266A71DF466371447A93361C448E63DFC51EC_H
#ifndef SERVICEPOINTMANAGER_T26671C58FEC625E41B784B57AD46A0822DB68465_H
#define SERVICEPOINTMANAGER_T26671C58FEC625E41B784B57AD46A0822DB68465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServicePointManager
struct  ServicePointManager_t26671C58FEC625E41B784B57AD46A0822DB68465  : public RuntimeObject
{
public:

public:
};

struct ServicePointManager_t26671C58FEC625E41B784B57AD46A0822DB68465_StaticFields
{
public:
	// System.Collections.Specialized.HybridDictionary System.Net.ServicePointManager::servicePoints
	HybridDictionary_t5AD529BFF21493C38235716C9AD62F1F7623C747 * ___servicePoints_0;
	// System.Net.ICertificatePolicy System.Net.ServicePointManager::policy
	RuntimeObject* ___policy_1;
	// System.Int32 System.Net.ServicePointManager::defaultConnectionLimit
	int32_t ___defaultConnectionLimit_2;
	// System.Int32 System.Net.ServicePointManager::maxServicePointIdleTime
	int32_t ___maxServicePointIdleTime_3;
	// System.Int32 System.Net.ServicePointManager::maxServicePoints
	int32_t ___maxServicePoints_4;
	// System.Boolean System.Net.ServicePointManager::_checkCRL
	bool ____checkCRL_5;
	// System.Net.SecurityProtocolType System.Net.ServicePointManager::_securityProtocol
	int32_t ____securityProtocol_6;
	// System.Boolean System.Net.ServicePointManager::expectContinue
	bool ___expectContinue_7;
	// System.Boolean System.Net.ServicePointManager::useNagle
	bool ___useNagle_8;
	// System.Net.Security.RemoteCertificateValidationCallback System.Net.ServicePointManager::server_cert_cb
	RemoteCertificateValidationCallback_t1276D648C0AECBBE8AF2790937BA182B64D3A489 * ___server_cert_cb_9;

public:
	inline static int32_t get_offset_of_servicePoints_0() { return static_cast<int32_t>(offsetof(ServicePointManager_t26671C58FEC625E41B784B57AD46A0822DB68465_StaticFields, ___servicePoints_0)); }
	inline HybridDictionary_t5AD529BFF21493C38235716C9AD62F1F7623C747 * get_servicePoints_0() const { return ___servicePoints_0; }
	inline HybridDictionary_t5AD529BFF21493C38235716C9AD62F1F7623C747 ** get_address_of_servicePoints_0() { return &___servicePoints_0; }
	inline void set_servicePoints_0(HybridDictionary_t5AD529BFF21493C38235716C9AD62F1F7623C747 * value)
	{
		___servicePoints_0 = value;
		Il2CppCodeGenWriteBarrier((&___servicePoints_0), value);
	}

	inline static int32_t get_offset_of_policy_1() { return static_cast<int32_t>(offsetof(ServicePointManager_t26671C58FEC625E41B784B57AD46A0822DB68465_StaticFields, ___policy_1)); }
	inline RuntimeObject* get_policy_1() const { return ___policy_1; }
	inline RuntimeObject** get_address_of_policy_1() { return &___policy_1; }
	inline void set_policy_1(RuntimeObject* value)
	{
		___policy_1 = value;
		Il2CppCodeGenWriteBarrier((&___policy_1), value);
	}

	inline static int32_t get_offset_of_defaultConnectionLimit_2() { return static_cast<int32_t>(offsetof(ServicePointManager_t26671C58FEC625E41B784B57AD46A0822DB68465_StaticFields, ___defaultConnectionLimit_2)); }
	inline int32_t get_defaultConnectionLimit_2() const { return ___defaultConnectionLimit_2; }
	inline int32_t* get_address_of_defaultConnectionLimit_2() { return &___defaultConnectionLimit_2; }
	inline void set_defaultConnectionLimit_2(int32_t value)
	{
		___defaultConnectionLimit_2 = value;
	}

	inline static int32_t get_offset_of_maxServicePointIdleTime_3() { return static_cast<int32_t>(offsetof(ServicePointManager_t26671C58FEC625E41B784B57AD46A0822DB68465_StaticFields, ___maxServicePointIdleTime_3)); }
	inline int32_t get_maxServicePointIdleTime_3() const { return ___maxServicePointIdleTime_3; }
	inline int32_t* get_address_of_maxServicePointIdleTime_3() { return &___maxServicePointIdleTime_3; }
	inline void set_maxServicePointIdleTime_3(int32_t value)
	{
		___maxServicePointIdleTime_3 = value;
	}

	inline static int32_t get_offset_of_maxServicePoints_4() { return static_cast<int32_t>(offsetof(ServicePointManager_t26671C58FEC625E41B784B57AD46A0822DB68465_StaticFields, ___maxServicePoints_4)); }
	inline int32_t get_maxServicePoints_4() const { return ___maxServicePoints_4; }
	inline int32_t* get_address_of_maxServicePoints_4() { return &___maxServicePoints_4; }
	inline void set_maxServicePoints_4(int32_t value)
	{
		___maxServicePoints_4 = value;
	}

	inline static int32_t get_offset_of__checkCRL_5() { return static_cast<int32_t>(offsetof(ServicePointManager_t26671C58FEC625E41B784B57AD46A0822DB68465_StaticFields, ____checkCRL_5)); }
	inline bool get__checkCRL_5() const { return ____checkCRL_5; }
	inline bool* get_address_of__checkCRL_5() { return &____checkCRL_5; }
	inline void set__checkCRL_5(bool value)
	{
		____checkCRL_5 = value;
	}

	inline static int32_t get_offset_of__securityProtocol_6() { return static_cast<int32_t>(offsetof(ServicePointManager_t26671C58FEC625E41B784B57AD46A0822DB68465_StaticFields, ____securityProtocol_6)); }
	inline int32_t get__securityProtocol_6() const { return ____securityProtocol_6; }
	inline int32_t* get_address_of__securityProtocol_6() { return &____securityProtocol_6; }
	inline void set__securityProtocol_6(int32_t value)
	{
		____securityProtocol_6 = value;
	}

	inline static int32_t get_offset_of_expectContinue_7() { return static_cast<int32_t>(offsetof(ServicePointManager_t26671C58FEC625E41B784B57AD46A0822DB68465_StaticFields, ___expectContinue_7)); }
	inline bool get_expectContinue_7() const { return ___expectContinue_7; }
	inline bool* get_address_of_expectContinue_7() { return &___expectContinue_7; }
	inline void set_expectContinue_7(bool value)
	{
		___expectContinue_7 = value;
	}

	inline static int32_t get_offset_of_useNagle_8() { return static_cast<int32_t>(offsetof(ServicePointManager_t26671C58FEC625E41B784B57AD46A0822DB68465_StaticFields, ___useNagle_8)); }
	inline bool get_useNagle_8() const { return ___useNagle_8; }
	inline bool* get_address_of_useNagle_8() { return &___useNagle_8; }
	inline void set_useNagle_8(bool value)
	{
		___useNagle_8 = value;
	}

	inline static int32_t get_offset_of_server_cert_cb_9() { return static_cast<int32_t>(offsetof(ServicePointManager_t26671C58FEC625E41B784B57AD46A0822DB68465_StaticFields, ___server_cert_cb_9)); }
	inline RemoteCertificateValidationCallback_t1276D648C0AECBBE8AF2790937BA182B64D3A489 * get_server_cert_cb_9() const { return ___server_cert_cb_9; }
	inline RemoteCertificateValidationCallback_t1276D648C0AECBBE8AF2790937BA182B64D3A489 ** get_address_of_server_cert_cb_9() { return &___server_cert_cb_9; }
	inline void set_server_cert_cb_9(RemoteCertificateValidationCallback_t1276D648C0AECBBE8AF2790937BA182B64D3A489 * value)
	{
		___server_cert_cb_9 = value;
		Il2CppCodeGenWriteBarrier((&___server_cert_cb_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVICEPOINTMANAGER_T26671C58FEC625E41B784B57AD46A0822DB68465_H
#ifndef CHAINVALIDATIONHELPER_T528BF08A29D7FB5788210430C2F5280EB1C192C2_H
#define CHAINVALIDATIONHELPER_T528BF08A29D7FB5788210430C2F5280EB1C192C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServicePointManager/ChainValidationHelper
struct  ChainValidationHelper_t528BF08A29D7FB5788210430C2F5280EB1C192C2  : public RuntimeObject
{
public:
	// System.Object System.Net.ServicePointManager/ChainValidationHelper::sender
	RuntimeObject * ___sender_0;
	// System.String System.Net.ServicePointManager/ChainValidationHelper::host
	String_t* ___host_1;

public:
	inline static int32_t get_offset_of_sender_0() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t528BF08A29D7FB5788210430C2F5280EB1C192C2, ___sender_0)); }
	inline RuntimeObject * get_sender_0() const { return ___sender_0; }
	inline RuntimeObject ** get_address_of_sender_0() { return &___sender_0; }
	inline void set_sender_0(RuntimeObject * value)
	{
		___sender_0 = value;
		Il2CppCodeGenWriteBarrier((&___sender_0), value);
	}

	inline static int32_t get_offset_of_host_1() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t528BF08A29D7FB5788210430C2F5280EB1C192C2, ___host_1)); }
	inline String_t* get_host_1() const { return ___host_1; }
	inline String_t** get_address_of_host_1() { return &___host_1; }
	inline void set_host_1(String_t* value)
	{
		___host_1 = value;
		Il2CppCodeGenWriteBarrier((&___host_1), value);
	}
};

struct ChainValidationHelper_t528BF08A29D7FB5788210430C2F5280EB1C192C2_StaticFields
{
public:
	// System.Boolean System.Net.ServicePointManager/ChainValidationHelper::is_macosx
	bool ___is_macosx_2;
	// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags System.Net.ServicePointManager/ChainValidationHelper::s_flags
	int32_t ___s_flags_3;

public:
	inline static int32_t get_offset_of_is_macosx_2() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t528BF08A29D7FB5788210430C2F5280EB1C192C2_StaticFields, ___is_macosx_2)); }
	inline bool get_is_macosx_2() const { return ___is_macosx_2; }
	inline bool* get_address_of_is_macosx_2() { return &___is_macosx_2; }
	inline void set_is_macosx_2(bool value)
	{
		___is_macosx_2 = value;
	}

	inline static int32_t get_offset_of_s_flags_3() { return static_cast<int32_t>(offsetof(ChainValidationHelper_t528BF08A29D7FB5788210430C2F5280EB1C192C2_StaticFields, ___s_flags_3)); }
	inline int32_t get_s_flags_3() const { return ___s_flags_3; }
	inline int32_t* get_address_of_s_flags_3() { return &___s_flags_3; }
	inline void set_s_flags_3(int32_t value)
	{
		___s_flags_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAINVALIDATIONHELPER_T528BF08A29D7FB5788210430C2F5280EB1C192C2_H
#ifndef WEBCONNECTION_T5679E1D2D5D8C262E3C68A73D00B7CCB42023F28_H
#define WEBCONNECTION_T5679E1D2D5D8C262E3C68A73D00B7CCB42023F28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebConnection
struct  WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28  : public RuntimeObject
{
public:
	// System.Net.ServicePoint System.Net.WebConnection::sPoint
	ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3 * ___sPoint_0;
	// System.IO.Stream System.Net.WebConnection::nstream
	Stream_tCFD27E43C18381861212C0288CACF467FB602009 * ___nstream_1;
	// System.Net.Sockets.Socket System.Net.WebConnection::socket
	Socket_t78B0CF49A51C5330826C9D2F0D0BF7B959FE2C03 * ___socket_2;
	// System.Object System.Net.WebConnection::socketLock
	RuntimeObject * ___socketLock_3;
	// System.Net.WebExceptionStatus System.Net.WebConnection::status
	int32_t ___status_4;
	// System.Threading.WaitCallback System.Net.WebConnection::initConn
	WaitCallback_t55F4591597BB1B3F1CC6CEB6C34E712FDF46FD2B * ___initConn_5;
	// System.Boolean System.Net.WebConnection::keepAlive
	bool ___keepAlive_6;
	// System.Byte[] System.Net.WebConnection::buffer
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___buffer_7;
	// System.EventHandler System.Net.WebConnection::abortHandler
	EventHandler_t743A59DD95D50590C5CC9EBBCAA45C7B753C897D * ___abortHandler_9;
	// System.Net.WebConnection/AbortHelper System.Net.WebConnection::abortHelper
	AbortHelper_tD2C5F18F172369ACDFA4655DEB034CD04578E6C8 * ___abortHelper_10;
	// System.Net.ReadState System.Net.WebConnection::readState
	int32_t ___readState_11;
	// System.Net.WebConnectionData System.Net.WebConnection::Data
	WebConnectionData_tDAAF22601B03B750DC57CB05255D88DF0A21DEDA * ___Data_12;
	// System.Boolean System.Net.WebConnection::chunkedRead
	bool ___chunkedRead_13;
	// System.Net.ChunkStream System.Net.WebConnection::chunkStream
	ChunkStream_t582E82A7D21D03B7A5AF6B1F913012451C28E40F * ___chunkStream_14;
	// System.Collections.Queue System.Net.WebConnection::queue
	Queue_t2B066F5DA52AD958C3B9532F7C88339BA1427EAD * ___queue_15;
	// System.Boolean System.Net.WebConnection::reused
	bool ___reused_16;
	// System.Int32 System.Net.WebConnection::position
	int32_t ___position_17;
	// System.Boolean System.Net.WebConnection::busy
	bool ___busy_18;
	// System.Net.HttpWebRequest System.Net.WebConnection::priority_request
	HttpWebRequest_t03D495C58ABDF0A533A2DABD6D7FDECD6507383E * ___priority_request_19;
	// System.Net.NetworkCredential System.Net.WebConnection::ntlm_credentials
	NetworkCredential_t82282628121911251495C07B1DA96F1D09367506 * ___ntlm_credentials_20;
	// System.Boolean System.Net.WebConnection::ntlm_authenticated
	bool ___ntlm_authenticated_21;
	// System.Boolean System.Net.WebConnection::unsafe_sharing
	bool ___unsafe_sharing_22;
	// System.Boolean System.Net.WebConnection::ssl
	bool ___ssl_23;
	// System.Boolean System.Net.WebConnection::certsAvailable
	bool ___certsAvailable_24;
	// System.Exception System.Net.WebConnection::connect_exception
	Exception_t * ___connect_exception_25;

public:
	inline static int32_t get_offset_of_sPoint_0() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___sPoint_0)); }
	inline ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3 * get_sPoint_0() const { return ___sPoint_0; }
	inline ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3 ** get_address_of_sPoint_0() { return &___sPoint_0; }
	inline void set_sPoint_0(ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3 * value)
	{
		___sPoint_0 = value;
		Il2CppCodeGenWriteBarrier((&___sPoint_0), value);
	}

	inline static int32_t get_offset_of_nstream_1() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___nstream_1)); }
	inline Stream_tCFD27E43C18381861212C0288CACF467FB602009 * get_nstream_1() const { return ___nstream_1; }
	inline Stream_tCFD27E43C18381861212C0288CACF467FB602009 ** get_address_of_nstream_1() { return &___nstream_1; }
	inline void set_nstream_1(Stream_tCFD27E43C18381861212C0288CACF467FB602009 * value)
	{
		___nstream_1 = value;
		Il2CppCodeGenWriteBarrier((&___nstream_1), value);
	}

	inline static int32_t get_offset_of_socket_2() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___socket_2)); }
	inline Socket_t78B0CF49A51C5330826C9D2F0D0BF7B959FE2C03 * get_socket_2() const { return ___socket_2; }
	inline Socket_t78B0CF49A51C5330826C9D2F0D0BF7B959FE2C03 ** get_address_of_socket_2() { return &___socket_2; }
	inline void set_socket_2(Socket_t78B0CF49A51C5330826C9D2F0D0BF7B959FE2C03 * value)
	{
		___socket_2 = value;
		Il2CppCodeGenWriteBarrier((&___socket_2), value);
	}

	inline static int32_t get_offset_of_socketLock_3() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___socketLock_3)); }
	inline RuntimeObject * get_socketLock_3() const { return ___socketLock_3; }
	inline RuntimeObject ** get_address_of_socketLock_3() { return &___socketLock_3; }
	inline void set_socketLock_3(RuntimeObject * value)
	{
		___socketLock_3 = value;
		Il2CppCodeGenWriteBarrier((&___socketLock_3), value);
	}

	inline static int32_t get_offset_of_status_4() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___status_4)); }
	inline int32_t get_status_4() const { return ___status_4; }
	inline int32_t* get_address_of_status_4() { return &___status_4; }
	inline void set_status_4(int32_t value)
	{
		___status_4 = value;
	}

	inline static int32_t get_offset_of_initConn_5() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___initConn_5)); }
	inline WaitCallback_t55F4591597BB1B3F1CC6CEB6C34E712FDF46FD2B * get_initConn_5() const { return ___initConn_5; }
	inline WaitCallback_t55F4591597BB1B3F1CC6CEB6C34E712FDF46FD2B ** get_address_of_initConn_5() { return &___initConn_5; }
	inline void set_initConn_5(WaitCallback_t55F4591597BB1B3F1CC6CEB6C34E712FDF46FD2B * value)
	{
		___initConn_5 = value;
		Il2CppCodeGenWriteBarrier((&___initConn_5), value);
	}

	inline static int32_t get_offset_of_keepAlive_6() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___keepAlive_6)); }
	inline bool get_keepAlive_6() const { return ___keepAlive_6; }
	inline bool* get_address_of_keepAlive_6() { return &___keepAlive_6; }
	inline void set_keepAlive_6(bool value)
	{
		___keepAlive_6 = value;
	}

	inline static int32_t get_offset_of_buffer_7() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___buffer_7)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_buffer_7() const { return ___buffer_7; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_buffer_7() { return &___buffer_7; }
	inline void set_buffer_7(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___buffer_7 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_7), value);
	}

	inline static int32_t get_offset_of_abortHandler_9() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___abortHandler_9)); }
	inline EventHandler_t743A59DD95D50590C5CC9EBBCAA45C7B753C897D * get_abortHandler_9() const { return ___abortHandler_9; }
	inline EventHandler_t743A59DD95D50590C5CC9EBBCAA45C7B753C897D ** get_address_of_abortHandler_9() { return &___abortHandler_9; }
	inline void set_abortHandler_9(EventHandler_t743A59DD95D50590C5CC9EBBCAA45C7B753C897D * value)
	{
		___abortHandler_9 = value;
		Il2CppCodeGenWriteBarrier((&___abortHandler_9), value);
	}

	inline static int32_t get_offset_of_abortHelper_10() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___abortHelper_10)); }
	inline AbortHelper_tD2C5F18F172369ACDFA4655DEB034CD04578E6C8 * get_abortHelper_10() const { return ___abortHelper_10; }
	inline AbortHelper_tD2C5F18F172369ACDFA4655DEB034CD04578E6C8 ** get_address_of_abortHelper_10() { return &___abortHelper_10; }
	inline void set_abortHelper_10(AbortHelper_tD2C5F18F172369ACDFA4655DEB034CD04578E6C8 * value)
	{
		___abortHelper_10 = value;
		Il2CppCodeGenWriteBarrier((&___abortHelper_10), value);
	}

	inline static int32_t get_offset_of_readState_11() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___readState_11)); }
	inline int32_t get_readState_11() const { return ___readState_11; }
	inline int32_t* get_address_of_readState_11() { return &___readState_11; }
	inline void set_readState_11(int32_t value)
	{
		___readState_11 = value;
	}

	inline static int32_t get_offset_of_Data_12() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___Data_12)); }
	inline WebConnectionData_tDAAF22601B03B750DC57CB05255D88DF0A21DEDA * get_Data_12() const { return ___Data_12; }
	inline WebConnectionData_tDAAF22601B03B750DC57CB05255D88DF0A21DEDA ** get_address_of_Data_12() { return &___Data_12; }
	inline void set_Data_12(WebConnectionData_tDAAF22601B03B750DC57CB05255D88DF0A21DEDA * value)
	{
		___Data_12 = value;
		Il2CppCodeGenWriteBarrier((&___Data_12), value);
	}

	inline static int32_t get_offset_of_chunkedRead_13() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___chunkedRead_13)); }
	inline bool get_chunkedRead_13() const { return ___chunkedRead_13; }
	inline bool* get_address_of_chunkedRead_13() { return &___chunkedRead_13; }
	inline void set_chunkedRead_13(bool value)
	{
		___chunkedRead_13 = value;
	}

	inline static int32_t get_offset_of_chunkStream_14() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___chunkStream_14)); }
	inline ChunkStream_t582E82A7D21D03B7A5AF6B1F913012451C28E40F * get_chunkStream_14() const { return ___chunkStream_14; }
	inline ChunkStream_t582E82A7D21D03B7A5AF6B1F913012451C28E40F ** get_address_of_chunkStream_14() { return &___chunkStream_14; }
	inline void set_chunkStream_14(ChunkStream_t582E82A7D21D03B7A5AF6B1F913012451C28E40F * value)
	{
		___chunkStream_14 = value;
		Il2CppCodeGenWriteBarrier((&___chunkStream_14), value);
	}

	inline static int32_t get_offset_of_queue_15() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___queue_15)); }
	inline Queue_t2B066F5DA52AD958C3B9532F7C88339BA1427EAD * get_queue_15() const { return ___queue_15; }
	inline Queue_t2B066F5DA52AD958C3B9532F7C88339BA1427EAD ** get_address_of_queue_15() { return &___queue_15; }
	inline void set_queue_15(Queue_t2B066F5DA52AD958C3B9532F7C88339BA1427EAD * value)
	{
		___queue_15 = value;
		Il2CppCodeGenWriteBarrier((&___queue_15), value);
	}

	inline static int32_t get_offset_of_reused_16() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___reused_16)); }
	inline bool get_reused_16() const { return ___reused_16; }
	inline bool* get_address_of_reused_16() { return &___reused_16; }
	inline void set_reused_16(bool value)
	{
		___reused_16 = value;
	}

	inline static int32_t get_offset_of_position_17() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___position_17)); }
	inline int32_t get_position_17() const { return ___position_17; }
	inline int32_t* get_address_of_position_17() { return &___position_17; }
	inline void set_position_17(int32_t value)
	{
		___position_17 = value;
	}

	inline static int32_t get_offset_of_busy_18() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___busy_18)); }
	inline bool get_busy_18() const { return ___busy_18; }
	inline bool* get_address_of_busy_18() { return &___busy_18; }
	inline void set_busy_18(bool value)
	{
		___busy_18 = value;
	}

	inline static int32_t get_offset_of_priority_request_19() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___priority_request_19)); }
	inline HttpWebRequest_t03D495C58ABDF0A533A2DABD6D7FDECD6507383E * get_priority_request_19() const { return ___priority_request_19; }
	inline HttpWebRequest_t03D495C58ABDF0A533A2DABD6D7FDECD6507383E ** get_address_of_priority_request_19() { return &___priority_request_19; }
	inline void set_priority_request_19(HttpWebRequest_t03D495C58ABDF0A533A2DABD6D7FDECD6507383E * value)
	{
		___priority_request_19 = value;
		Il2CppCodeGenWriteBarrier((&___priority_request_19), value);
	}

	inline static int32_t get_offset_of_ntlm_credentials_20() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___ntlm_credentials_20)); }
	inline NetworkCredential_t82282628121911251495C07B1DA96F1D09367506 * get_ntlm_credentials_20() const { return ___ntlm_credentials_20; }
	inline NetworkCredential_t82282628121911251495C07B1DA96F1D09367506 ** get_address_of_ntlm_credentials_20() { return &___ntlm_credentials_20; }
	inline void set_ntlm_credentials_20(NetworkCredential_t82282628121911251495C07B1DA96F1D09367506 * value)
	{
		___ntlm_credentials_20 = value;
		Il2CppCodeGenWriteBarrier((&___ntlm_credentials_20), value);
	}

	inline static int32_t get_offset_of_ntlm_authenticated_21() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___ntlm_authenticated_21)); }
	inline bool get_ntlm_authenticated_21() const { return ___ntlm_authenticated_21; }
	inline bool* get_address_of_ntlm_authenticated_21() { return &___ntlm_authenticated_21; }
	inline void set_ntlm_authenticated_21(bool value)
	{
		___ntlm_authenticated_21 = value;
	}

	inline static int32_t get_offset_of_unsafe_sharing_22() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___unsafe_sharing_22)); }
	inline bool get_unsafe_sharing_22() const { return ___unsafe_sharing_22; }
	inline bool* get_address_of_unsafe_sharing_22() { return &___unsafe_sharing_22; }
	inline void set_unsafe_sharing_22(bool value)
	{
		___unsafe_sharing_22 = value;
	}

	inline static int32_t get_offset_of_ssl_23() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___ssl_23)); }
	inline bool get_ssl_23() const { return ___ssl_23; }
	inline bool* get_address_of_ssl_23() { return &___ssl_23; }
	inline void set_ssl_23(bool value)
	{
		___ssl_23 = value;
	}

	inline static int32_t get_offset_of_certsAvailable_24() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___certsAvailable_24)); }
	inline bool get_certsAvailable_24() const { return ___certsAvailable_24; }
	inline bool* get_address_of_certsAvailable_24() { return &___certsAvailable_24; }
	inline void set_certsAvailable_24(bool value)
	{
		___certsAvailable_24 = value;
	}

	inline static int32_t get_offset_of_connect_exception_25() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28, ___connect_exception_25)); }
	inline Exception_t * get_connect_exception_25() const { return ___connect_exception_25; }
	inline Exception_t ** get_address_of_connect_exception_25() { return &___connect_exception_25; }
	inline void set_connect_exception_25(Exception_t * value)
	{
		___connect_exception_25 = value;
		Il2CppCodeGenWriteBarrier((&___connect_exception_25), value);
	}
};

struct WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28_StaticFields
{
public:
	// System.AsyncCallback System.Net.WebConnection::readDoneDelegate
	AsyncCallback_t74ABD1277F711E7FBDCB00E169A63DEFD39E86A2 * ___readDoneDelegate_8;
	// System.Object System.Net.WebConnection::classLock
	RuntimeObject * ___classLock_26;
	// System.Type System.Net.WebConnection::sslStream
	Type_t * ___sslStream_27;
	// System.Reflection.PropertyInfo System.Net.WebConnection::piClient
	PropertyInfo_t * ___piClient_28;
	// System.Reflection.PropertyInfo System.Net.WebConnection::piServer
	PropertyInfo_t * ___piServer_29;
	// System.Reflection.PropertyInfo System.Net.WebConnection::piTrustFailure
	PropertyInfo_t * ___piTrustFailure_30;
	// System.Reflection.MethodInfo System.Net.WebConnection::method_GetSecurityPolicyFromNonMainThread
	MethodInfo_t * ___method_GetSecurityPolicyFromNonMainThread_31;

public:
	inline static int32_t get_offset_of_readDoneDelegate_8() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28_StaticFields, ___readDoneDelegate_8)); }
	inline AsyncCallback_t74ABD1277F711E7FBDCB00E169A63DEFD39E86A2 * get_readDoneDelegate_8() const { return ___readDoneDelegate_8; }
	inline AsyncCallback_t74ABD1277F711E7FBDCB00E169A63DEFD39E86A2 ** get_address_of_readDoneDelegate_8() { return &___readDoneDelegate_8; }
	inline void set_readDoneDelegate_8(AsyncCallback_t74ABD1277F711E7FBDCB00E169A63DEFD39E86A2 * value)
	{
		___readDoneDelegate_8 = value;
		Il2CppCodeGenWriteBarrier((&___readDoneDelegate_8), value);
	}

	inline static int32_t get_offset_of_classLock_26() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28_StaticFields, ___classLock_26)); }
	inline RuntimeObject * get_classLock_26() const { return ___classLock_26; }
	inline RuntimeObject ** get_address_of_classLock_26() { return &___classLock_26; }
	inline void set_classLock_26(RuntimeObject * value)
	{
		___classLock_26 = value;
		Il2CppCodeGenWriteBarrier((&___classLock_26), value);
	}

	inline static int32_t get_offset_of_sslStream_27() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28_StaticFields, ___sslStream_27)); }
	inline Type_t * get_sslStream_27() const { return ___sslStream_27; }
	inline Type_t ** get_address_of_sslStream_27() { return &___sslStream_27; }
	inline void set_sslStream_27(Type_t * value)
	{
		___sslStream_27 = value;
		Il2CppCodeGenWriteBarrier((&___sslStream_27), value);
	}

	inline static int32_t get_offset_of_piClient_28() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28_StaticFields, ___piClient_28)); }
	inline PropertyInfo_t * get_piClient_28() const { return ___piClient_28; }
	inline PropertyInfo_t ** get_address_of_piClient_28() { return &___piClient_28; }
	inline void set_piClient_28(PropertyInfo_t * value)
	{
		___piClient_28 = value;
		Il2CppCodeGenWriteBarrier((&___piClient_28), value);
	}

	inline static int32_t get_offset_of_piServer_29() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28_StaticFields, ___piServer_29)); }
	inline PropertyInfo_t * get_piServer_29() const { return ___piServer_29; }
	inline PropertyInfo_t ** get_address_of_piServer_29() { return &___piServer_29; }
	inline void set_piServer_29(PropertyInfo_t * value)
	{
		___piServer_29 = value;
		Il2CppCodeGenWriteBarrier((&___piServer_29), value);
	}

	inline static int32_t get_offset_of_piTrustFailure_30() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28_StaticFields, ___piTrustFailure_30)); }
	inline PropertyInfo_t * get_piTrustFailure_30() const { return ___piTrustFailure_30; }
	inline PropertyInfo_t ** get_address_of_piTrustFailure_30() { return &___piTrustFailure_30; }
	inline void set_piTrustFailure_30(PropertyInfo_t * value)
	{
		___piTrustFailure_30 = value;
		Il2CppCodeGenWriteBarrier((&___piTrustFailure_30), value);
	}

	inline static int32_t get_offset_of_method_GetSecurityPolicyFromNonMainThread_31() { return static_cast<int32_t>(offsetof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28_StaticFields, ___method_GetSecurityPolicyFromNonMainThread_31)); }
	inline MethodInfo_t * get_method_GetSecurityPolicyFromNonMainThread_31() const { return ___method_GetSecurityPolicyFromNonMainThread_31; }
	inline MethodInfo_t ** get_address_of_method_GetSecurityPolicyFromNonMainThread_31() { return &___method_GetSecurityPolicyFromNonMainThread_31; }
	inline void set_method_GetSecurityPolicyFromNonMainThread_31(MethodInfo_t * value)
	{
		___method_GetSecurityPolicyFromNonMainThread_31 = value;
		Il2CppCodeGenWriteBarrier((&___method_GetSecurityPolicyFromNonMainThread_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCONNECTION_T5679E1D2D5D8C262E3C68A73D00B7CCB42023F28_H
#ifndef WEBEXCEPTION_T21F760AAEE9DBB7B74522CE801488D1AB97EF51A_H
#define WEBEXCEPTION_T21F760AAEE9DBB7B74522CE801488D1AB97EF51A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebException
struct  WebException_t21F760AAEE9DBB7B74522CE801488D1AB97EF51A  : public InvalidOperationException_tCE0CC6C0C8EE2E780C0736B79AA673751C79CC07
{
public:
	// System.Net.WebResponse System.Net.WebException::response
	WebResponse_t5E9F05BAC005D2105A122CD5973E1D70B9B45325 * ___response_12;
	// System.Net.WebExceptionStatus System.Net.WebException::status
	int32_t ___status_13;

public:
	inline static int32_t get_offset_of_response_12() { return static_cast<int32_t>(offsetof(WebException_t21F760AAEE9DBB7B74522CE801488D1AB97EF51A, ___response_12)); }
	inline WebResponse_t5E9F05BAC005D2105A122CD5973E1D70B9B45325 * get_response_12() const { return ___response_12; }
	inline WebResponse_t5E9F05BAC005D2105A122CD5973E1D70B9B45325 ** get_address_of_response_12() { return &___response_12; }
	inline void set_response_12(WebResponse_t5E9F05BAC005D2105A122CD5973E1D70B9B45325 * value)
	{
		___response_12 = value;
		Il2CppCodeGenWriteBarrier((&___response_12), value);
	}

	inline static int32_t get_offset_of_status_13() { return static_cast<int32_t>(offsetof(WebException_t21F760AAEE9DBB7B74522CE801488D1AB97EF51A, ___status_13)); }
	inline int32_t get_status_13() const { return ___status_13; }
	inline int32_t* get_address_of_status_13() { return &___status_13; }
	inline void set_status_13(int32_t value)
	{
		___status_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBEXCEPTION_T21F760AAEE9DBB7B74522CE801488D1AB97EF51A_H
#ifndef WEBREQUEST_T079731BC640578743ADC705570EAC33A9BCFB399_H
#define WEBREQUEST_T079731BC640578743ADC705570EAC33A9BCFB399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequest
struct  WebRequest_t079731BC640578743ADC705570EAC33A9BCFB399  : public MarshalByRefObject_t05F62A8AC86E36BAE3063CA28097945DE9E179C4
{
public:
	// System.Net.Security.AuthenticationLevel System.Net.WebRequest::authentication_level
	int32_t ___authentication_level_4;

public:
	inline static int32_t get_offset_of_authentication_level_4() { return static_cast<int32_t>(offsetof(WebRequest_t079731BC640578743ADC705570EAC33A9BCFB399, ___authentication_level_4)); }
	inline int32_t get_authentication_level_4() const { return ___authentication_level_4; }
	inline int32_t* get_address_of_authentication_level_4() { return &___authentication_level_4; }
	inline void set_authentication_level_4(int32_t value)
	{
		___authentication_level_4 = value;
	}
};

struct WebRequest_t079731BC640578743ADC705570EAC33A9BCFB399_StaticFields
{
public:
	// System.Collections.Specialized.HybridDictionary System.Net.WebRequest::prefixes
	HybridDictionary_t5AD529BFF21493C38235716C9AD62F1F7623C747 * ___prefixes_1;
	// System.Boolean System.Net.WebRequest::isDefaultWebProxySet
	bool ___isDefaultWebProxySet_2;
	// System.Net.IWebProxy System.Net.WebRequest::defaultWebProxy
	RuntimeObject* ___defaultWebProxy_3;
	// System.Object System.Net.WebRequest::lockobj
	RuntimeObject * ___lockobj_5;

public:
	inline static int32_t get_offset_of_prefixes_1() { return static_cast<int32_t>(offsetof(WebRequest_t079731BC640578743ADC705570EAC33A9BCFB399_StaticFields, ___prefixes_1)); }
	inline HybridDictionary_t5AD529BFF21493C38235716C9AD62F1F7623C747 * get_prefixes_1() const { return ___prefixes_1; }
	inline HybridDictionary_t5AD529BFF21493C38235716C9AD62F1F7623C747 ** get_address_of_prefixes_1() { return &___prefixes_1; }
	inline void set_prefixes_1(HybridDictionary_t5AD529BFF21493C38235716C9AD62F1F7623C747 * value)
	{
		___prefixes_1 = value;
		Il2CppCodeGenWriteBarrier((&___prefixes_1), value);
	}

	inline static int32_t get_offset_of_isDefaultWebProxySet_2() { return static_cast<int32_t>(offsetof(WebRequest_t079731BC640578743ADC705570EAC33A9BCFB399_StaticFields, ___isDefaultWebProxySet_2)); }
	inline bool get_isDefaultWebProxySet_2() const { return ___isDefaultWebProxySet_2; }
	inline bool* get_address_of_isDefaultWebProxySet_2() { return &___isDefaultWebProxySet_2; }
	inline void set_isDefaultWebProxySet_2(bool value)
	{
		___isDefaultWebProxySet_2 = value;
	}

	inline static int32_t get_offset_of_defaultWebProxy_3() { return static_cast<int32_t>(offsetof(WebRequest_t079731BC640578743ADC705570EAC33A9BCFB399_StaticFields, ___defaultWebProxy_3)); }
	inline RuntimeObject* get_defaultWebProxy_3() const { return ___defaultWebProxy_3; }
	inline RuntimeObject** get_address_of_defaultWebProxy_3() { return &___defaultWebProxy_3; }
	inline void set_defaultWebProxy_3(RuntimeObject* value)
	{
		___defaultWebProxy_3 = value;
		Il2CppCodeGenWriteBarrier((&___defaultWebProxy_3), value);
	}

	inline static int32_t get_offset_of_lockobj_5() { return static_cast<int32_t>(offsetof(WebRequest_t079731BC640578743ADC705570EAC33A9BCFB399_StaticFields, ___lockobj_5)); }
	inline RuntimeObject * get_lockobj_5() const { return ___lockobj_5; }
	inline RuntimeObject ** get_address_of_lockobj_5() { return &___lockobj_5; }
	inline void set_lockobj_5(RuntimeObject * value)
	{
		___lockobj_5 = value;
		Il2CppCodeGenWriteBarrier((&___lockobj_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUEST_T079731BC640578743ADC705570EAC33A9BCFB399_H
#ifndef X509BASICCONSTRAINTSEXTENSION_T47598D4831B5E7131CBABBEB2AFA6C171341ACEA_H
#define X509BASICCONSTRAINTSEXTENSION_T47598D4831B5E7131CBABBEB2AFA6C171341ACEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension
struct  X509BasicConstraintsExtension_t47598D4831B5E7131CBABBEB2AFA6C171341ACEA  : public X509Extension_tF50D0787240E5674487D37FDE5CCD9ABAA39F2EB
{
public:
	// System.Boolean System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::_certificateAuthority
	bool ____certificateAuthority_6;
	// System.Boolean System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::_hasPathLengthConstraint
	bool ____hasPathLengthConstraint_7;
	// System.Int32 System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::_pathLengthConstraint
	int32_t ____pathLengthConstraint_8;
	// System.Security.Cryptography.AsnDecodeStatus System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension::_status
	int32_t ____status_9;

public:
	inline static int32_t get_offset_of__certificateAuthority_6() { return static_cast<int32_t>(offsetof(X509BasicConstraintsExtension_t47598D4831B5E7131CBABBEB2AFA6C171341ACEA, ____certificateAuthority_6)); }
	inline bool get__certificateAuthority_6() const { return ____certificateAuthority_6; }
	inline bool* get_address_of__certificateAuthority_6() { return &____certificateAuthority_6; }
	inline void set__certificateAuthority_6(bool value)
	{
		____certificateAuthority_6 = value;
	}

	inline static int32_t get_offset_of__hasPathLengthConstraint_7() { return static_cast<int32_t>(offsetof(X509BasicConstraintsExtension_t47598D4831B5E7131CBABBEB2AFA6C171341ACEA, ____hasPathLengthConstraint_7)); }
	inline bool get__hasPathLengthConstraint_7() const { return ____hasPathLengthConstraint_7; }
	inline bool* get_address_of__hasPathLengthConstraint_7() { return &____hasPathLengthConstraint_7; }
	inline void set__hasPathLengthConstraint_7(bool value)
	{
		____hasPathLengthConstraint_7 = value;
	}

	inline static int32_t get_offset_of__pathLengthConstraint_8() { return static_cast<int32_t>(offsetof(X509BasicConstraintsExtension_t47598D4831B5E7131CBABBEB2AFA6C171341ACEA, ____pathLengthConstraint_8)); }
	inline int32_t get__pathLengthConstraint_8() const { return ____pathLengthConstraint_8; }
	inline int32_t* get_address_of__pathLengthConstraint_8() { return &____pathLengthConstraint_8; }
	inline void set__pathLengthConstraint_8(int32_t value)
	{
		____pathLengthConstraint_8 = value;
	}

	inline static int32_t get_offset_of__status_9() { return static_cast<int32_t>(offsetof(X509BasicConstraintsExtension_t47598D4831B5E7131CBABBEB2AFA6C171341ACEA, ____status_9)); }
	inline int32_t get__status_9() const { return ____status_9; }
	inline int32_t* get_address_of__status_9() { return &____status_9; }
	inline void set__status_9(int32_t value)
	{
		____status_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509BASICCONSTRAINTSEXTENSION_T47598D4831B5E7131CBABBEB2AFA6C171341ACEA_H
#ifndef X509CHAIN_T3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56_H
#define X509CHAIN_T3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Chain
struct  X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56  : public RuntimeObject
{
public:
	// System.Security.Cryptography.X509Certificates.StoreLocation System.Security.Cryptography.X509Certificates.X509Chain::location
	int32_t ___location_0;
	// System.Security.Cryptography.X509Certificates.X509ChainElementCollection System.Security.Cryptography.X509Certificates.X509Chain::elements
	X509ChainElementCollection_tDA6F4291F55CC8640B2097C37F408A211B38E4A9 * ___elements_1;
	// System.Security.Cryptography.X509Certificates.X509ChainPolicy System.Security.Cryptography.X509Certificates.X509Chain::policy
	X509ChainPolicy_t50512486AAC7530381A567A301A6C39722FF00B4 * ___policy_2;
	// System.Security.Cryptography.X509Certificates.X509ChainStatus[] System.Security.Cryptography.X509Certificates.X509Chain::status
	X509ChainStatusU5BU5D_t6789D0235F31EBCFB0491B55E859CF75FEB806AD* ___status_3;
	// System.Int32 System.Security.Cryptography.X509Certificates.X509Chain::max_path_length
	int32_t ___max_path_length_5;
	// System.Security.Cryptography.X509Certificates.X500DistinguishedName System.Security.Cryptography.X509Certificates.X509Chain::working_issuer_name
	X500DistinguishedName_t4E51A8B638BD9EFBFC71A78F0263716509B878C9 * ___working_issuer_name_6;
	// System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.X509Certificates.X509Chain::working_public_key
	AsymmetricAlgorithm_tCE2A08658EFEA671D977BC39FA246B4FC5092BB2 * ___working_public_key_7;
	// System.Security.Cryptography.X509Certificates.X509ChainElement System.Security.Cryptography.X509Certificates.X509Chain::bce_restriction
	X509ChainElement_tAA74792F43074F2F75B321787C15A7E4388B39A9 * ___bce_restriction_8;
	// System.Security.Cryptography.X509Certificates.X509Store System.Security.Cryptography.X509Certificates.X509Chain::roots
	X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747 * ___roots_9;
	// System.Security.Cryptography.X509Certificates.X509Store System.Security.Cryptography.X509Certificates.X509Chain::cas
	X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747 * ___cas_10;
	// System.Security.Cryptography.X509Certificates.X509Certificate2Collection System.Security.Cryptography.X509Certificates.X509Chain::collection
	X509Certificate2Collection_t5F3686D1CB11C6BD038CE9D964F5A4B9BCDF532E * ___collection_11;

public:
	inline static int32_t get_offset_of_location_0() { return static_cast<int32_t>(offsetof(X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56, ___location_0)); }
	inline int32_t get_location_0() const { return ___location_0; }
	inline int32_t* get_address_of_location_0() { return &___location_0; }
	inline void set_location_0(int32_t value)
	{
		___location_0 = value;
	}

	inline static int32_t get_offset_of_elements_1() { return static_cast<int32_t>(offsetof(X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56, ___elements_1)); }
	inline X509ChainElementCollection_tDA6F4291F55CC8640B2097C37F408A211B38E4A9 * get_elements_1() const { return ___elements_1; }
	inline X509ChainElementCollection_tDA6F4291F55CC8640B2097C37F408A211B38E4A9 ** get_address_of_elements_1() { return &___elements_1; }
	inline void set_elements_1(X509ChainElementCollection_tDA6F4291F55CC8640B2097C37F408A211B38E4A9 * value)
	{
		___elements_1 = value;
		Il2CppCodeGenWriteBarrier((&___elements_1), value);
	}

	inline static int32_t get_offset_of_policy_2() { return static_cast<int32_t>(offsetof(X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56, ___policy_2)); }
	inline X509ChainPolicy_t50512486AAC7530381A567A301A6C39722FF00B4 * get_policy_2() const { return ___policy_2; }
	inline X509ChainPolicy_t50512486AAC7530381A567A301A6C39722FF00B4 ** get_address_of_policy_2() { return &___policy_2; }
	inline void set_policy_2(X509ChainPolicy_t50512486AAC7530381A567A301A6C39722FF00B4 * value)
	{
		___policy_2 = value;
		Il2CppCodeGenWriteBarrier((&___policy_2), value);
	}

	inline static int32_t get_offset_of_status_3() { return static_cast<int32_t>(offsetof(X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56, ___status_3)); }
	inline X509ChainStatusU5BU5D_t6789D0235F31EBCFB0491B55E859CF75FEB806AD* get_status_3() const { return ___status_3; }
	inline X509ChainStatusU5BU5D_t6789D0235F31EBCFB0491B55E859CF75FEB806AD** get_address_of_status_3() { return &___status_3; }
	inline void set_status_3(X509ChainStatusU5BU5D_t6789D0235F31EBCFB0491B55E859CF75FEB806AD* value)
	{
		___status_3 = value;
		Il2CppCodeGenWriteBarrier((&___status_3), value);
	}

	inline static int32_t get_offset_of_max_path_length_5() { return static_cast<int32_t>(offsetof(X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56, ___max_path_length_5)); }
	inline int32_t get_max_path_length_5() const { return ___max_path_length_5; }
	inline int32_t* get_address_of_max_path_length_5() { return &___max_path_length_5; }
	inline void set_max_path_length_5(int32_t value)
	{
		___max_path_length_5 = value;
	}

	inline static int32_t get_offset_of_working_issuer_name_6() { return static_cast<int32_t>(offsetof(X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56, ___working_issuer_name_6)); }
	inline X500DistinguishedName_t4E51A8B638BD9EFBFC71A78F0263716509B878C9 * get_working_issuer_name_6() const { return ___working_issuer_name_6; }
	inline X500DistinguishedName_t4E51A8B638BD9EFBFC71A78F0263716509B878C9 ** get_address_of_working_issuer_name_6() { return &___working_issuer_name_6; }
	inline void set_working_issuer_name_6(X500DistinguishedName_t4E51A8B638BD9EFBFC71A78F0263716509B878C9 * value)
	{
		___working_issuer_name_6 = value;
		Il2CppCodeGenWriteBarrier((&___working_issuer_name_6), value);
	}

	inline static int32_t get_offset_of_working_public_key_7() { return static_cast<int32_t>(offsetof(X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56, ___working_public_key_7)); }
	inline AsymmetricAlgorithm_tCE2A08658EFEA671D977BC39FA246B4FC5092BB2 * get_working_public_key_7() const { return ___working_public_key_7; }
	inline AsymmetricAlgorithm_tCE2A08658EFEA671D977BC39FA246B4FC5092BB2 ** get_address_of_working_public_key_7() { return &___working_public_key_7; }
	inline void set_working_public_key_7(AsymmetricAlgorithm_tCE2A08658EFEA671D977BC39FA246B4FC5092BB2 * value)
	{
		___working_public_key_7 = value;
		Il2CppCodeGenWriteBarrier((&___working_public_key_7), value);
	}

	inline static int32_t get_offset_of_bce_restriction_8() { return static_cast<int32_t>(offsetof(X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56, ___bce_restriction_8)); }
	inline X509ChainElement_tAA74792F43074F2F75B321787C15A7E4388B39A9 * get_bce_restriction_8() const { return ___bce_restriction_8; }
	inline X509ChainElement_tAA74792F43074F2F75B321787C15A7E4388B39A9 ** get_address_of_bce_restriction_8() { return &___bce_restriction_8; }
	inline void set_bce_restriction_8(X509ChainElement_tAA74792F43074F2F75B321787C15A7E4388B39A9 * value)
	{
		___bce_restriction_8 = value;
		Il2CppCodeGenWriteBarrier((&___bce_restriction_8), value);
	}

	inline static int32_t get_offset_of_roots_9() { return static_cast<int32_t>(offsetof(X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56, ___roots_9)); }
	inline X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747 * get_roots_9() const { return ___roots_9; }
	inline X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747 ** get_address_of_roots_9() { return &___roots_9; }
	inline void set_roots_9(X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747 * value)
	{
		___roots_9 = value;
		Il2CppCodeGenWriteBarrier((&___roots_9), value);
	}

	inline static int32_t get_offset_of_cas_10() { return static_cast<int32_t>(offsetof(X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56, ___cas_10)); }
	inline X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747 * get_cas_10() const { return ___cas_10; }
	inline X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747 ** get_address_of_cas_10() { return &___cas_10; }
	inline void set_cas_10(X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747 * value)
	{
		___cas_10 = value;
		Il2CppCodeGenWriteBarrier((&___cas_10), value);
	}

	inline static int32_t get_offset_of_collection_11() { return static_cast<int32_t>(offsetof(X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56, ___collection_11)); }
	inline X509Certificate2Collection_t5F3686D1CB11C6BD038CE9D964F5A4B9BCDF532E * get_collection_11() const { return ___collection_11; }
	inline X509Certificate2Collection_t5F3686D1CB11C6BD038CE9D964F5A4B9BCDF532E ** get_address_of_collection_11() { return &___collection_11; }
	inline void set_collection_11(X509Certificate2Collection_t5F3686D1CB11C6BD038CE9D964F5A4B9BCDF532E * value)
	{
		___collection_11 = value;
		Il2CppCodeGenWriteBarrier((&___collection_11), value);
	}
};

struct X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56_StaticFields
{
public:
	// System.Security.Cryptography.X509Certificates.X509ChainStatus[] System.Security.Cryptography.X509Certificates.X509Chain::Empty
	X509ChainStatusU5BU5D_t6789D0235F31EBCFB0491B55E859CF75FEB806AD* ___Empty_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.X509Certificates.X509Chain::<>f__switch$mapB
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24mapB_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.X509Certificates.X509Chain::<>f__switch$mapC
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24mapC_13;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.X509Certificates.X509Chain::<>f__switch$mapD
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24mapD_14;

public:
	inline static int32_t get_offset_of_Empty_4() { return static_cast<int32_t>(offsetof(X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56_StaticFields, ___Empty_4)); }
	inline X509ChainStatusU5BU5D_t6789D0235F31EBCFB0491B55E859CF75FEB806AD* get_Empty_4() const { return ___Empty_4; }
	inline X509ChainStatusU5BU5D_t6789D0235F31EBCFB0491B55E859CF75FEB806AD** get_address_of_Empty_4() { return &___Empty_4; }
	inline void set_Empty_4(X509ChainStatusU5BU5D_t6789D0235F31EBCFB0491B55E859CF75FEB806AD* value)
	{
		___Empty_4 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapB_12() { return static_cast<int32_t>(offsetof(X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56_StaticFields, ___U3CU3Ef__switchU24mapB_12)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24mapB_12() const { return ___U3CU3Ef__switchU24mapB_12; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24mapB_12() { return &___U3CU3Ef__switchU24mapB_12; }
	inline void set_U3CU3Ef__switchU24mapB_12(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24mapB_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapB_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapC_13() { return static_cast<int32_t>(offsetof(X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56_StaticFields, ___U3CU3Ef__switchU24mapC_13)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24mapC_13() const { return ___U3CU3Ef__switchU24mapC_13; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24mapC_13() { return &___U3CU3Ef__switchU24mapC_13; }
	inline void set_U3CU3Ef__switchU24mapC_13(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24mapC_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapC_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapD_14() { return static_cast<int32_t>(offsetof(X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56_StaticFields, ___U3CU3Ef__switchU24mapD_14)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24mapD_14() const { return ___U3CU3Ef__switchU24mapD_14; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24mapD_14() { return &___U3CU3Ef__switchU24mapD_14; }
	inline void set_U3CU3Ef__switchU24mapD_14(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24mapD_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapD_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAIN_T3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56_H
#ifndef X509CHAINELEMENT_TAA74792F43074F2F75B321787C15A7E4388B39A9_H
#define X509CHAINELEMENT_TAA74792F43074F2F75B321787C15A7E4388B39A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainElement
struct  X509ChainElement_tAA74792F43074F2F75B321787C15A7E4388B39A9  : public RuntimeObject
{
public:
	// System.Security.Cryptography.X509Certificates.X509Certificate2 System.Security.Cryptography.X509Certificates.X509ChainElement::certificate
	X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5 * ___certificate_0;
	// System.Security.Cryptography.X509Certificates.X509ChainStatus[] System.Security.Cryptography.X509Certificates.X509ChainElement::status
	X509ChainStatusU5BU5D_t6789D0235F31EBCFB0491B55E859CF75FEB806AD* ___status_1;
	// System.String System.Security.Cryptography.X509Certificates.X509ChainElement::info
	String_t* ___info_2;
	// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags System.Security.Cryptography.X509Certificates.X509ChainElement::compressed_status_flags
	int32_t ___compressed_status_flags_3;

public:
	inline static int32_t get_offset_of_certificate_0() { return static_cast<int32_t>(offsetof(X509ChainElement_tAA74792F43074F2F75B321787C15A7E4388B39A9, ___certificate_0)); }
	inline X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5 * get_certificate_0() const { return ___certificate_0; }
	inline X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5 ** get_address_of_certificate_0() { return &___certificate_0; }
	inline void set_certificate_0(X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5 * value)
	{
		___certificate_0 = value;
		Il2CppCodeGenWriteBarrier((&___certificate_0), value);
	}

	inline static int32_t get_offset_of_status_1() { return static_cast<int32_t>(offsetof(X509ChainElement_tAA74792F43074F2F75B321787C15A7E4388B39A9, ___status_1)); }
	inline X509ChainStatusU5BU5D_t6789D0235F31EBCFB0491B55E859CF75FEB806AD* get_status_1() const { return ___status_1; }
	inline X509ChainStatusU5BU5D_t6789D0235F31EBCFB0491B55E859CF75FEB806AD** get_address_of_status_1() { return &___status_1; }
	inline void set_status_1(X509ChainStatusU5BU5D_t6789D0235F31EBCFB0491B55E859CF75FEB806AD* value)
	{
		___status_1 = value;
		Il2CppCodeGenWriteBarrier((&___status_1), value);
	}

	inline static int32_t get_offset_of_info_2() { return static_cast<int32_t>(offsetof(X509ChainElement_tAA74792F43074F2F75B321787C15A7E4388B39A9, ___info_2)); }
	inline String_t* get_info_2() const { return ___info_2; }
	inline String_t** get_address_of_info_2() { return &___info_2; }
	inline void set_info_2(String_t* value)
	{
		___info_2 = value;
		Il2CppCodeGenWriteBarrier((&___info_2), value);
	}

	inline static int32_t get_offset_of_compressed_status_flags_3() { return static_cast<int32_t>(offsetof(X509ChainElement_tAA74792F43074F2F75B321787C15A7E4388B39A9, ___compressed_status_flags_3)); }
	inline int32_t get_compressed_status_flags_3() const { return ___compressed_status_flags_3; }
	inline int32_t* get_address_of_compressed_status_flags_3() { return &___compressed_status_flags_3; }
	inline void set_compressed_status_flags_3(int32_t value)
	{
		___compressed_status_flags_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINELEMENT_TAA74792F43074F2F75B321787C15A7E4388B39A9_H
#ifndef X509CHAINSTATUS_T60E8382F80CC9E801B291F443817D53825719725_H
#define X509CHAINSTATUS_T60E8382F80CC9E801B291F443817D53825719725_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainStatus
struct  X509ChainStatus_t60E8382F80CC9E801B291F443817D53825719725 
{
public:
	// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags System.Security.Cryptography.X509Certificates.X509ChainStatus::status
	int32_t ___status_0;
	// System.String System.Security.Cryptography.X509Certificates.X509ChainStatus::info
	String_t* ___info_1;

public:
	inline static int32_t get_offset_of_status_0() { return static_cast<int32_t>(offsetof(X509ChainStatus_t60E8382F80CC9E801B291F443817D53825719725, ___status_0)); }
	inline int32_t get_status_0() const { return ___status_0; }
	inline int32_t* get_address_of_status_0() { return &___status_0; }
	inline void set_status_0(int32_t value)
	{
		___status_0 = value;
	}

	inline static int32_t get_offset_of_info_1() { return static_cast<int32_t>(offsetof(X509ChainStatus_t60E8382F80CC9E801B291F443817D53825719725, ___info_1)); }
	inline String_t* get_info_1() const { return ___info_1; }
	inline String_t** get_address_of_info_1() { return &___info_1; }
	inline void set_info_1(String_t* value)
	{
		___info_1 = value;
		Il2CppCodeGenWriteBarrier((&___info_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Security.Cryptography.X509Certificates.X509ChainStatus
struct X509ChainStatus_t60E8382F80CC9E801B291F443817D53825719725_marshaled_pinvoke
{
	int32_t ___status_0;
	char* ___info_1;
};
// Native definition for COM marshalling of System.Security.Cryptography.X509Certificates.X509ChainStatus
struct X509ChainStatus_t60E8382F80CC9E801B291F443817D53825719725_marshaled_com
{
	int32_t ___status_0;
	Il2CppChar* ___info_1;
};
#endif // X509CHAINSTATUS_T60E8382F80CC9E801B291F443817D53825719725_H
#ifndef X509ENHANCEDKEYUSAGEEXTENSION_T60BEF6B2641E6D77758CF57269F42113D87B4E12_H
#define X509ENHANCEDKEYUSAGEEXTENSION_T60BEF6B2641E6D77758CF57269F42113D87B4E12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension
struct  X509EnhancedKeyUsageExtension_t60BEF6B2641E6D77758CF57269F42113D87B4E12  : public X509Extension_tF50D0787240E5674487D37FDE5CCD9ABAA39F2EB
{
public:
	// System.Security.Cryptography.OidCollection System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension::_enhKeyUsage
	OidCollection_tDA5968A49C496611C36F3E30F8A675D6F57E08D0 * ____enhKeyUsage_4;
	// System.Security.Cryptography.AsnDecodeStatus System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension::_status
	int32_t ____status_5;

public:
	inline static int32_t get_offset_of__enhKeyUsage_4() { return static_cast<int32_t>(offsetof(X509EnhancedKeyUsageExtension_t60BEF6B2641E6D77758CF57269F42113D87B4E12, ____enhKeyUsage_4)); }
	inline OidCollection_tDA5968A49C496611C36F3E30F8A675D6F57E08D0 * get__enhKeyUsage_4() const { return ____enhKeyUsage_4; }
	inline OidCollection_tDA5968A49C496611C36F3E30F8A675D6F57E08D0 ** get_address_of__enhKeyUsage_4() { return &____enhKeyUsage_4; }
	inline void set__enhKeyUsage_4(OidCollection_tDA5968A49C496611C36F3E30F8A675D6F57E08D0 * value)
	{
		____enhKeyUsage_4 = value;
		Il2CppCodeGenWriteBarrier((&____enhKeyUsage_4), value);
	}

	inline static int32_t get_offset_of__status_5() { return static_cast<int32_t>(offsetof(X509EnhancedKeyUsageExtension_t60BEF6B2641E6D77758CF57269F42113D87B4E12, ____status_5)); }
	inline int32_t get__status_5() const { return ____status_5; }
	inline int32_t* get_address_of__status_5() { return &____status_5; }
	inline void set__status_5(int32_t value)
	{
		____status_5 = value;
	}
};

struct X509EnhancedKeyUsageExtension_t60BEF6B2641E6D77758CF57269F42113D87B4E12_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension::<>f__switch$mapE
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24mapE_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapE_6() { return static_cast<int32_t>(offsetof(X509EnhancedKeyUsageExtension_t60BEF6B2641E6D77758CF57269F42113D87B4E12_StaticFields, ___U3CU3Ef__switchU24mapE_6)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24mapE_6() const { return ___U3CU3Ef__switchU24mapE_6; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24mapE_6() { return &___U3CU3Ef__switchU24mapE_6; }
	inline void set_U3CU3Ef__switchU24mapE_6(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24mapE_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapE_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509ENHANCEDKEYUSAGEEXTENSION_T60BEF6B2641E6D77758CF57269F42113D87B4E12_H
#ifndef X509KEYUSAGEEXTENSION_T63F1A779D02770BDD9506FF39344C559E4406047_H
#define X509KEYUSAGEEXTENSION_T63F1A779D02770BDD9506FF39344C559E4406047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509KeyUsageExtension
struct  X509KeyUsageExtension_t63F1A779D02770BDD9506FF39344C559E4406047  : public X509Extension_tF50D0787240E5674487D37FDE5CCD9ABAA39F2EB
{
public:
	// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags System.Security.Cryptography.X509Certificates.X509KeyUsageExtension::_keyUsages
	int32_t ____keyUsages_7;
	// System.Security.Cryptography.AsnDecodeStatus System.Security.Cryptography.X509Certificates.X509KeyUsageExtension::_status
	int32_t ____status_8;

public:
	inline static int32_t get_offset_of__keyUsages_7() { return static_cast<int32_t>(offsetof(X509KeyUsageExtension_t63F1A779D02770BDD9506FF39344C559E4406047, ____keyUsages_7)); }
	inline int32_t get__keyUsages_7() const { return ____keyUsages_7; }
	inline int32_t* get_address_of__keyUsages_7() { return &____keyUsages_7; }
	inline void set__keyUsages_7(int32_t value)
	{
		____keyUsages_7 = value;
	}

	inline static int32_t get_offset_of__status_8() { return static_cast<int32_t>(offsetof(X509KeyUsageExtension_t63F1A779D02770BDD9506FF39344C559E4406047, ____status_8)); }
	inline int32_t get__status_8() const { return ____status_8; }
	inline int32_t* get_address_of__status_8() { return &____status_8; }
	inline void set__status_8(int32_t value)
	{
		____status_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509KEYUSAGEEXTENSION_T63F1A779D02770BDD9506FF39344C559E4406047_H
#ifndef X509STORE_TD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747_H
#define X509STORE_TD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509Store
struct  X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747  : public RuntimeObject
{
public:
	// System.String System.Security.Cryptography.X509Certificates.X509Store::_name
	String_t* ____name_0;
	// System.Security.Cryptography.X509Certificates.StoreLocation System.Security.Cryptography.X509Certificates.X509Store::_location
	int32_t ____location_1;
	// System.Security.Cryptography.X509Certificates.X509Certificate2Collection System.Security.Cryptography.X509Certificates.X509Store::list
	X509Certificate2Collection_t5F3686D1CB11C6BD038CE9D964F5A4B9BCDF532E * ___list_2;
	// System.Security.Cryptography.X509Certificates.OpenFlags System.Security.Cryptography.X509Certificates.X509Store::_flags
	int32_t ____flags_3;
	// Mono.Security.X509.X509Store System.Security.Cryptography.X509Certificates.X509Store::store
	X509Store_t754E7C7E6C15F74AA8E7A3F0695EE7E1AD0C4302 * ___store_4;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747, ____name_0)); }
	inline String_t* get__name_0() const { return ____name_0; }
	inline String_t** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(String_t* value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier((&____name_0), value);
	}

	inline static int32_t get_offset_of__location_1() { return static_cast<int32_t>(offsetof(X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747, ____location_1)); }
	inline int32_t get__location_1() const { return ____location_1; }
	inline int32_t* get_address_of__location_1() { return &____location_1; }
	inline void set__location_1(int32_t value)
	{
		____location_1 = value;
	}

	inline static int32_t get_offset_of_list_2() { return static_cast<int32_t>(offsetof(X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747, ___list_2)); }
	inline X509Certificate2Collection_t5F3686D1CB11C6BD038CE9D964F5A4B9BCDF532E * get_list_2() const { return ___list_2; }
	inline X509Certificate2Collection_t5F3686D1CB11C6BD038CE9D964F5A4B9BCDF532E ** get_address_of_list_2() { return &___list_2; }
	inline void set_list_2(X509Certificate2Collection_t5F3686D1CB11C6BD038CE9D964F5A4B9BCDF532E * value)
	{
		___list_2 = value;
		Il2CppCodeGenWriteBarrier((&___list_2), value);
	}

	inline static int32_t get_offset_of__flags_3() { return static_cast<int32_t>(offsetof(X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747, ____flags_3)); }
	inline int32_t get__flags_3() const { return ____flags_3; }
	inline int32_t* get_address_of__flags_3() { return &____flags_3; }
	inline void set__flags_3(int32_t value)
	{
		____flags_3 = value;
	}

	inline static int32_t get_offset_of_store_4() { return static_cast<int32_t>(offsetof(X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747, ___store_4)); }
	inline X509Store_t754E7C7E6C15F74AA8E7A3F0695EE7E1AD0C4302 * get_store_4() const { return ___store_4; }
	inline X509Store_t754E7C7E6C15F74AA8E7A3F0695EE7E1AD0C4302 ** get_address_of_store_4() { return &___store_4; }
	inline void set_store_4(X509Store_t754E7C7E6C15F74AA8E7A3F0695EE7E1AD0C4302 * value)
	{
		___store_4 = value;
		Il2CppCodeGenWriteBarrier((&___store_4), value);
	}
};

struct X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.X509Certificates.X509Store::<>f__switch$mapF
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24mapF_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapF_5() { return static_cast<int32_t>(offsetof(X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747_StaticFields, ___U3CU3Ef__switchU24mapF_5)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24mapF_5() const { return ___U3CU3Ef__switchU24mapF_5; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24mapF_5() { return &___U3CU3Ef__switchU24mapF_5; }
	inline void set_U3CU3Ef__switchU24mapF_5(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24mapF_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapF_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509STORE_TD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747_H
#ifndef X509SUBJECTKEYIDENTIFIEREXTENSION_TFE27201132D4053906DB91C199AE1E344146F9A0_H
#define X509SUBJECTKEYIDENTIFIEREXTENSION_TFE27201132D4053906DB91C199AE1E344146F9A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension
struct  X509SubjectKeyIdentifierExtension_tFE27201132D4053906DB91C199AE1E344146F9A0  : public X509Extension_tF50D0787240E5674487D37FDE5CCD9ABAA39F2EB
{
public:
	// System.Byte[] System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::_subjectKeyIdentifier
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ____subjectKeyIdentifier_6;
	// System.String System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::_ski
	String_t* ____ski_7;
	// System.Security.Cryptography.AsnDecodeStatus System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension::_status
	int32_t ____status_8;

public:
	inline static int32_t get_offset_of__subjectKeyIdentifier_6() { return static_cast<int32_t>(offsetof(X509SubjectKeyIdentifierExtension_tFE27201132D4053906DB91C199AE1E344146F9A0, ____subjectKeyIdentifier_6)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get__subjectKeyIdentifier_6() const { return ____subjectKeyIdentifier_6; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of__subjectKeyIdentifier_6() { return &____subjectKeyIdentifier_6; }
	inline void set__subjectKeyIdentifier_6(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		____subjectKeyIdentifier_6 = value;
		Il2CppCodeGenWriteBarrier((&____subjectKeyIdentifier_6), value);
	}

	inline static int32_t get_offset_of__ski_7() { return static_cast<int32_t>(offsetof(X509SubjectKeyIdentifierExtension_tFE27201132D4053906DB91C199AE1E344146F9A0, ____ski_7)); }
	inline String_t* get__ski_7() const { return ____ski_7; }
	inline String_t** get_address_of__ski_7() { return &____ski_7; }
	inline void set__ski_7(String_t* value)
	{
		____ski_7 = value;
		Il2CppCodeGenWriteBarrier((&____ski_7), value);
	}

	inline static int32_t get_offset_of__status_8() { return static_cast<int32_t>(offsetof(X509SubjectKeyIdentifierExtension_tFE27201132D4053906DB91C199AE1E344146F9A0, ____status_8)); }
	inline int32_t get__status_8() const { return ____status_8; }
	inline int32_t* get_address_of__status_8() { return &____status_8; }
	inline void set__status_8(int32_t value)
	{
		____status_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509SUBJECTKEYIDENTIFIEREXTENSION_TFE27201132D4053906DB91C199AE1E344146F9A0_H
#ifndef KEY_TFC08407FC450EA826C414756A28BCA5F5889F3C7_H
#define KEY_TFC08407FC450EA826C414756A28BCA5F5889F3C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.FactoryCache/Key
struct  Key_tFC08407FC450EA826C414756A28BCA5F5889F3C7  : public RuntimeObject
{
public:
	// System.String System.Text.RegularExpressions.FactoryCache/Key::pattern
	String_t* ___pattern_0;
	// System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.FactoryCache/Key::options
	int32_t ___options_1;

public:
	inline static int32_t get_offset_of_pattern_0() { return static_cast<int32_t>(offsetof(Key_tFC08407FC450EA826C414756A28BCA5F5889F3C7, ___pattern_0)); }
	inline String_t* get_pattern_0() const { return ___pattern_0; }
	inline String_t** get_address_of_pattern_0() { return &___pattern_0; }
	inline void set_pattern_0(String_t* value)
	{
		___pattern_0 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_0), value);
	}

	inline static int32_t get_offset_of_options_1() { return static_cast<int32_t>(offsetof(Key_tFC08407FC450EA826C414756A28BCA5F5889F3C7, ___options_1)); }
	inline int32_t get_options_1() const { return ___options_1; }
	inline int32_t* get_address_of_options_1() { return &___options_1; }
	inline void set_options_1(int32_t value)
	{
		___options_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEY_TFC08407FC450EA826C414756A28BCA5F5889F3C7_H
#ifndef REGEX_T829CB33C547AB824BFEAFF320BB8F59B3A76620B_H
#define REGEX_T829CB33C547AB824BFEAFF320BB8F59B3A76620B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Regex
struct  Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.IMachineFactory System.Text.RegularExpressions.Regex::machineFactory
	RuntimeObject* ___machineFactory_1;
	// System.Collections.IDictionary System.Text.RegularExpressions.Regex::mapping
	RuntimeObject* ___mapping_2;
	// System.Int32 System.Text.RegularExpressions.Regex::group_count
	int32_t ___group_count_3;
	// System.Int32 System.Text.RegularExpressions.Regex::gap
	int32_t ___gap_4;
	// System.String[] System.Text.RegularExpressions.Regex::group_names
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___group_names_5;
	// System.Int32[] System.Text.RegularExpressions.Regex::group_numbers
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___group_numbers_6;
	// System.String System.Text.RegularExpressions.Regex::pattern
	String_t* ___pattern_7;
	// System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.Regex::roptions
	int32_t ___roptions_8;

public:
	inline static int32_t get_offset_of_machineFactory_1() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B, ___machineFactory_1)); }
	inline RuntimeObject* get_machineFactory_1() const { return ___machineFactory_1; }
	inline RuntimeObject** get_address_of_machineFactory_1() { return &___machineFactory_1; }
	inline void set_machineFactory_1(RuntimeObject* value)
	{
		___machineFactory_1 = value;
		Il2CppCodeGenWriteBarrier((&___machineFactory_1), value);
	}

	inline static int32_t get_offset_of_mapping_2() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B, ___mapping_2)); }
	inline RuntimeObject* get_mapping_2() const { return ___mapping_2; }
	inline RuntimeObject** get_address_of_mapping_2() { return &___mapping_2; }
	inline void set_mapping_2(RuntimeObject* value)
	{
		___mapping_2 = value;
		Il2CppCodeGenWriteBarrier((&___mapping_2), value);
	}

	inline static int32_t get_offset_of_group_count_3() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B, ___group_count_3)); }
	inline int32_t get_group_count_3() const { return ___group_count_3; }
	inline int32_t* get_address_of_group_count_3() { return &___group_count_3; }
	inline void set_group_count_3(int32_t value)
	{
		___group_count_3 = value;
	}

	inline static int32_t get_offset_of_gap_4() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B, ___gap_4)); }
	inline int32_t get_gap_4() const { return ___gap_4; }
	inline int32_t* get_address_of_gap_4() { return &___gap_4; }
	inline void set_gap_4(int32_t value)
	{
		___gap_4 = value;
	}

	inline static int32_t get_offset_of_group_names_5() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B, ___group_names_5)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_group_names_5() const { return ___group_names_5; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_group_names_5() { return &___group_names_5; }
	inline void set_group_names_5(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___group_names_5 = value;
		Il2CppCodeGenWriteBarrier((&___group_names_5), value);
	}

	inline static int32_t get_offset_of_group_numbers_6() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B, ___group_numbers_6)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_group_numbers_6() const { return ___group_numbers_6; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_group_numbers_6() { return &___group_numbers_6; }
	inline void set_group_numbers_6(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___group_numbers_6 = value;
		Il2CppCodeGenWriteBarrier((&___group_numbers_6), value);
	}

	inline static int32_t get_offset_of_pattern_7() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B, ___pattern_7)); }
	inline String_t* get_pattern_7() const { return ___pattern_7; }
	inline String_t** get_address_of_pattern_7() { return &___pattern_7; }
	inline void set_pattern_7(String_t* value)
	{
		___pattern_7 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_7), value);
	}

	inline static int32_t get_offset_of_roptions_8() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B, ___roptions_8)); }
	inline int32_t get_roptions_8() const { return ___roptions_8; }
	inline int32_t* get_address_of_roptions_8() { return &___roptions_8; }
	inline void set_roptions_8(int32_t value)
	{
		___roptions_8 = value;
	}
};

struct Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B_StaticFields
{
public:
	// System.Text.RegularExpressions.FactoryCache System.Text.RegularExpressions.Regex::cache
	FactoryCache_t8BE3083C5F3C2185BA9AE97F4886454CB85C06B9 * ___cache_0;

public:
	inline static int32_t get_offset_of_cache_0() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B_StaticFields, ___cache_0)); }
	inline FactoryCache_t8BE3083C5F3C2185BA9AE97F4886454CB85C06B9 * get_cache_0() const { return ___cache_0; }
	inline FactoryCache_t8BE3083C5F3C2185BA9AE97F4886454CB85C06B9 ** get_address_of_cache_0() { return &___cache_0; }
	inline void set_cache_0(FactoryCache_t8BE3083C5F3C2185BA9AE97F4886454CB85C06B9 * value)
	{
		___cache_0 = value;
		Il2CppCodeGenWriteBarrier((&___cache_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEX_T829CB33C547AB824BFEAFF320BB8F59B3A76620B_H
#ifndef SERVICEPOINT_TE7D15B7B032D02714D4271B226BB921E2B32D1B3_H
#define SERVICEPOINT_TE7D15B7B032D02714D4271B226BB921E2B32D1B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServicePoint
struct  ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3  : public RuntimeObject
{
public:
	// System.Uri System.Net.ServicePoint::uri
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E * ___uri_0;
	// System.Int32 System.Net.ServicePoint::connectionLimit
	int32_t ___connectionLimit_1;
	// System.Int32 System.Net.ServicePoint::maxIdleTime
	int32_t ___maxIdleTime_2;
	// System.Int32 System.Net.ServicePoint::currentConnections
	int32_t ___currentConnections_3;
	// System.DateTime System.Net.ServicePoint::idleSince
	DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  ___idleSince_4;
	// System.Version System.Net.ServicePoint::protocolVersion
	Version_tD7D21D8F6BE9CC6473050418AB3D72BA2DB68D15 * ___protocolVersion_5;
	// System.Security.Cryptography.X509Certificates.X509Certificate System.Net.ServicePoint::certificate
	X509Certificate_tC6D94A8A63C0640494BE76372C6E4C291D2826DE * ___certificate_6;
	// System.Security.Cryptography.X509Certificates.X509Certificate System.Net.ServicePoint::clientCertificate
	X509Certificate_tC6D94A8A63C0640494BE76372C6E4C291D2826DE * ___clientCertificate_7;
	// System.Net.IPHostEntry System.Net.ServicePoint::host
	IPHostEntry_t1EDA9F93303E37E89B6FFDEBD50858A54D423D9E * ___host_8;
	// System.Boolean System.Net.ServicePoint::usesProxy
	bool ___usesProxy_9;
	// System.Collections.Hashtable System.Net.ServicePoint::groups
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___groups_10;
	// System.Boolean System.Net.ServicePoint::sendContinue
	bool ___sendContinue_11;
	// System.Boolean System.Net.ServicePoint::useConnect
	bool ___useConnect_12;
	// System.Object System.Net.ServicePoint::locker
	RuntimeObject * ___locker_13;
	// System.Object System.Net.ServicePoint::hostE
	RuntimeObject * ___hostE_14;
	// System.Boolean System.Net.ServicePoint::useNagle
	bool ___useNagle_15;
	// System.Net.BindIPEndPoint System.Net.ServicePoint::endPointCallback
	BindIPEndPoint_tC8DC0C67D3B4E2155A6ADD8956CBA19A6E02A148 * ___endPointCallback_16;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3, ___uri_0)); }
	inline Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E * get_uri_0() const { return ___uri_0; }
	inline Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E ** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E * value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}

	inline static int32_t get_offset_of_connectionLimit_1() { return static_cast<int32_t>(offsetof(ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3, ___connectionLimit_1)); }
	inline int32_t get_connectionLimit_1() const { return ___connectionLimit_1; }
	inline int32_t* get_address_of_connectionLimit_1() { return &___connectionLimit_1; }
	inline void set_connectionLimit_1(int32_t value)
	{
		___connectionLimit_1 = value;
	}

	inline static int32_t get_offset_of_maxIdleTime_2() { return static_cast<int32_t>(offsetof(ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3, ___maxIdleTime_2)); }
	inline int32_t get_maxIdleTime_2() const { return ___maxIdleTime_2; }
	inline int32_t* get_address_of_maxIdleTime_2() { return &___maxIdleTime_2; }
	inline void set_maxIdleTime_2(int32_t value)
	{
		___maxIdleTime_2 = value;
	}

	inline static int32_t get_offset_of_currentConnections_3() { return static_cast<int32_t>(offsetof(ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3, ___currentConnections_3)); }
	inline int32_t get_currentConnections_3() const { return ___currentConnections_3; }
	inline int32_t* get_address_of_currentConnections_3() { return &___currentConnections_3; }
	inline void set_currentConnections_3(int32_t value)
	{
		___currentConnections_3 = value;
	}

	inline static int32_t get_offset_of_idleSince_4() { return static_cast<int32_t>(offsetof(ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3, ___idleSince_4)); }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  get_idleSince_4() const { return ___idleSince_4; }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2 * get_address_of_idleSince_4() { return &___idleSince_4; }
	inline void set_idleSince_4(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  value)
	{
		___idleSince_4 = value;
	}

	inline static int32_t get_offset_of_protocolVersion_5() { return static_cast<int32_t>(offsetof(ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3, ___protocolVersion_5)); }
	inline Version_tD7D21D8F6BE9CC6473050418AB3D72BA2DB68D15 * get_protocolVersion_5() const { return ___protocolVersion_5; }
	inline Version_tD7D21D8F6BE9CC6473050418AB3D72BA2DB68D15 ** get_address_of_protocolVersion_5() { return &___protocolVersion_5; }
	inline void set_protocolVersion_5(Version_tD7D21D8F6BE9CC6473050418AB3D72BA2DB68D15 * value)
	{
		___protocolVersion_5 = value;
		Il2CppCodeGenWriteBarrier((&___protocolVersion_5), value);
	}

	inline static int32_t get_offset_of_certificate_6() { return static_cast<int32_t>(offsetof(ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3, ___certificate_6)); }
	inline X509Certificate_tC6D94A8A63C0640494BE76372C6E4C291D2826DE * get_certificate_6() const { return ___certificate_6; }
	inline X509Certificate_tC6D94A8A63C0640494BE76372C6E4C291D2826DE ** get_address_of_certificate_6() { return &___certificate_6; }
	inline void set_certificate_6(X509Certificate_tC6D94A8A63C0640494BE76372C6E4C291D2826DE * value)
	{
		___certificate_6 = value;
		Il2CppCodeGenWriteBarrier((&___certificate_6), value);
	}

	inline static int32_t get_offset_of_clientCertificate_7() { return static_cast<int32_t>(offsetof(ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3, ___clientCertificate_7)); }
	inline X509Certificate_tC6D94A8A63C0640494BE76372C6E4C291D2826DE * get_clientCertificate_7() const { return ___clientCertificate_7; }
	inline X509Certificate_tC6D94A8A63C0640494BE76372C6E4C291D2826DE ** get_address_of_clientCertificate_7() { return &___clientCertificate_7; }
	inline void set_clientCertificate_7(X509Certificate_tC6D94A8A63C0640494BE76372C6E4C291D2826DE * value)
	{
		___clientCertificate_7 = value;
		Il2CppCodeGenWriteBarrier((&___clientCertificate_7), value);
	}

	inline static int32_t get_offset_of_host_8() { return static_cast<int32_t>(offsetof(ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3, ___host_8)); }
	inline IPHostEntry_t1EDA9F93303E37E89B6FFDEBD50858A54D423D9E * get_host_8() const { return ___host_8; }
	inline IPHostEntry_t1EDA9F93303E37E89B6FFDEBD50858A54D423D9E ** get_address_of_host_8() { return &___host_8; }
	inline void set_host_8(IPHostEntry_t1EDA9F93303E37E89B6FFDEBD50858A54D423D9E * value)
	{
		___host_8 = value;
		Il2CppCodeGenWriteBarrier((&___host_8), value);
	}

	inline static int32_t get_offset_of_usesProxy_9() { return static_cast<int32_t>(offsetof(ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3, ___usesProxy_9)); }
	inline bool get_usesProxy_9() const { return ___usesProxy_9; }
	inline bool* get_address_of_usesProxy_9() { return &___usesProxy_9; }
	inline void set_usesProxy_9(bool value)
	{
		___usesProxy_9 = value;
	}

	inline static int32_t get_offset_of_groups_10() { return static_cast<int32_t>(offsetof(ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3, ___groups_10)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_groups_10() const { return ___groups_10; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_groups_10() { return &___groups_10; }
	inline void set_groups_10(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___groups_10 = value;
		Il2CppCodeGenWriteBarrier((&___groups_10), value);
	}

	inline static int32_t get_offset_of_sendContinue_11() { return static_cast<int32_t>(offsetof(ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3, ___sendContinue_11)); }
	inline bool get_sendContinue_11() const { return ___sendContinue_11; }
	inline bool* get_address_of_sendContinue_11() { return &___sendContinue_11; }
	inline void set_sendContinue_11(bool value)
	{
		___sendContinue_11 = value;
	}

	inline static int32_t get_offset_of_useConnect_12() { return static_cast<int32_t>(offsetof(ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3, ___useConnect_12)); }
	inline bool get_useConnect_12() const { return ___useConnect_12; }
	inline bool* get_address_of_useConnect_12() { return &___useConnect_12; }
	inline void set_useConnect_12(bool value)
	{
		___useConnect_12 = value;
	}

	inline static int32_t get_offset_of_locker_13() { return static_cast<int32_t>(offsetof(ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3, ___locker_13)); }
	inline RuntimeObject * get_locker_13() const { return ___locker_13; }
	inline RuntimeObject ** get_address_of_locker_13() { return &___locker_13; }
	inline void set_locker_13(RuntimeObject * value)
	{
		___locker_13 = value;
		Il2CppCodeGenWriteBarrier((&___locker_13), value);
	}

	inline static int32_t get_offset_of_hostE_14() { return static_cast<int32_t>(offsetof(ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3, ___hostE_14)); }
	inline RuntimeObject * get_hostE_14() const { return ___hostE_14; }
	inline RuntimeObject ** get_address_of_hostE_14() { return &___hostE_14; }
	inline void set_hostE_14(RuntimeObject * value)
	{
		___hostE_14 = value;
		Il2CppCodeGenWriteBarrier((&___hostE_14), value);
	}

	inline static int32_t get_offset_of_useNagle_15() { return static_cast<int32_t>(offsetof(ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3, ___useNagle_15)); }
	inline bool get_useNagle_15() const { return ___useNagle_15; }
	inline bool* get_address_of_useNagle_15() { return &___useNagle_15; }
	inline void set_useNagle_15(bool value)
	{
		___useNagle_15 = value;
	}

	inline static int32_t get_offset_of_endPointCallback_16() { return static_cast<int32_t>(offsetof(ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3, ___endPointCallback_16)); }
	inline BindIPEndPoint_tC8DC0C67D3B4E2155A6ADD8956CBA19A6E02A148 * get_endPointCallback_16() const { return ___endPointCallback_16; }
	inline BindIPEndPoint_tC8DC0C67D3B4E2155A6ADD8956CBA19A6E02A148 ** get_address_of_endPointCallback_16() { return &___endPointCallback_16; }
	inline void set_endPointCallback_16(BindIPEndPoint_tC8DC0C67D3B4E2155A6ADD8956CBA19A6E02A148 * value)
	{
		___endPointCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___endPointCallback_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVICEPOINT_TE7D15B7B032D02714D4271B226BB921E2B32D1B3_H
#ifndef X509CHAINPOLICY_T50512486AAC7530381A567A301A6C39722FF00B4_H
#define X509CHAINPOLICY_T50512486AAC7530381A567A301A6C39722FF00B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.X509Certificates.X509ChainPolicy
struct  X509ChainPolicy_t50512486AAC7530381A567A301A6C39722FF00B4  : public RuntimeObject
{
public:
	// System.Security.Cryptography.OidCollection System.Security.Cryptography.X509Certificates.X509ChainPolicy::apps
	OidCollection_tDA5968A49C496611C36F3E30F8A675D6F57E08D0 * ___apps_0;
	// System.Security.Cryptography.OidCollection System.Security.Cryptography.X509Certificates.X509ChainPolicy::cert
	OidCollection_tDA5968A49C496611C36F3E30F8A675D6F57E08D0 * ___cert_1;
	// System.Security.Cryptography.X509Certificates.X509Certificate2Collection System.Security.Cryptography.X509Certificates.X509ChainPolicy::store
	X509Certificate2Collection_t5F3686D1CB11C6BD038CE9D964F5A4B9BCDF532E * ___store_2;
	// System.Security.Cryptography.X509Certificates.X509RevocationFlag System.Security.Cryptography.X509Certificates.X509ChainPolicy::rflag
	int32_t ___rflag_3;
	// System.Security.Cryptography.X509Certificates.X509RevocationMode System.Security.Cryptography.X509Certificates.X509ChainPolicy::mode
	int32_t ___mode_4;
	// System.TimeSpan System.Security.Cryptography.X509Certificates.X509ChainPolicy::timeout
	TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  ___timeout_5;
	// System.Security.Cryptography.X509Certificates.X509VerificationFlags System.Security.Cryptography.X509Certificates.X509ChainPolicy::vflags
	int32_t ___vflags_6;
	// System.DateTime System.Security.Cryptography.X509Certificates.X509ChainPolicy::vtime
	DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  ___vtime_7;

public:
	inline static int32_t get_offset_of_apps_0() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t50512486AAC7530381A567A301A6C39722FF00B4, ___apps_0)); }
	inline OidCollection_tDA5968A49C496611C36F3E30F8A675D6F57E08D0 * get_apps_0() const { return ___apps_0; }
	inline OidCollection_tDA5968A49C496611C36F3E30F8A675D6F57E08D0 ** get_address_of_apps_0() { return &___apps_0; }
	inline void set_apps_0(OidCollection_tDA5968A49C496611C36F3E30F8A675D6F57E08D0 * value)
	{
		___apps_0 = value;
		Il2CppCodeGenWriteBarrier((&___apps_0), value);
	}

	inline static int32_t get_offset_of_cert_1() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t50512486AAC7530381A567A301A6C39722FF00B4, ___cert_1)); }
	inline OidCollection_tDA5968A49C496611C36F3E30F8A675D6F57E08D0 * get_cert_1() const { return ___cert_1; }
	inline OidCollection_tDA5968A49C496611C36F3E30F8A675D6F57E08D0 ** get_address_of_cert_1() { return &___cert_1; }
	inline void set_cert_1(OidCollection_tDA5968A49C496611C36F3E30F8A675D6F57E08D0 * value)
	{
		___cert_1 = value;
		Il2CppCodeGenWriteBarrier((&___cert_1), value);
	}

	inline static int32_t get_offset_of_store_2() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t50512486AAC7530381A567A301A6C39722FF00B4, ___store_2)); }
	inline X509Certificate2Collection_t5F3686D1CB11C6BD038CE9D964F5A4B9BCDF532E * get_store_2() const { return ___store_2; }
	inline X509Certificate2Collection_t5F3686D1CB11C6BD038CE9D964F5A4B9BCDF532E ** get_address_of_store_2() { return &___store_2; }
	inline void set_store_2(X509Certificate2Collection_t5F3686D1CB11C6BD038CE9D964F5A4B9BCDF532E * value)
	{
		___store_2 = value;
		Il2CppCodeGenWriteBarrier((&___store_2), value);
	}

	inline static int32_t get_offset_of_rflag_3() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t50512486AAC7530381A567A301A6C39722FF00B4, ___rflag_3)); }
	inline int32_t get_rflag_3() const { return ___rflag_3; }
	inline int32_t* get_address_of_rflag_3() { return &___rflag_3; }
	inline void set_rflag_3(int32_t value)
	{
		___rflag_3 = value;
	}

	inline static int32_t get_offset_of_mode_4() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t50512486AAC7530381A567A301A6C39722FF00B4, ___mode_4)); }
	inline int32_t get_mode_4() const { return ___mode_4; }
	inline int32_t* get_address_of_mode_4() { return &___mode_4; }
	inline void set_mode_4(int32_t value)
	{
		___mode_4 = value;
	}

	inline static int32_t get_offset_of_timeout_5() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t50512486AAC7530381A567A301A6C39722FF00B4, ___timeout_5)); }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  get_timeout_5() const { return ___timeout_5; }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374 * get_address_of_timeout_5() { return &___timeout_5; }
	inline void set_timeout_5(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  value)
	{
		___timeout_5 = value;
	}

	inline static int32_t get_offset_of_vflags_6() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t50512486AAC7530381A567A301A6C39722FF00B4, ___vflags_6)); }
	inline int32_t get_vflags_6() const { return ___vflags_6; }
	inline int32_t* get_address_of_vflags_6() { return &___vflags_6; }
	inline void set_vflags_6(int32_t value)
	{
		___vflags_6 = value;
	}

	inline static int32_t get_offset_of_vtime_7() { return static_cast<int32_t>(offsetof(X509ChainPolicy_t50512486AAC7530381A567A301A6C39722FF00B4, ___vtime_7)); }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  get_vtime_7() const { return ___vtime_7; }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2 * get_address_of_vtime_7() { return &___vtime_7; }
	inline void set_vtime_7(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  value)
	{
		___vtime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINPOLICY_T50512486AAC7530381A567A301A6C39722FF00B4_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1200 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1201 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1202 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1203 = { sizeof (IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4), -1, sizeof(IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1203[12] = 
{
	IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4::get_offset_of_m_Address_0(),
	IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4::get_offset_of_m_Family_1(),
	IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4::get_offset_of_m_Numbers_2(),
	IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4::get_offset_of_m_ScopeId_3(),
	IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4_StaticFields::get_offset_of_Any_4(),
	IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4_StaticFields::get_offset_of_Broadcast_5(),
	IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4_StaticFields::get_offset_of_Loopback_6(),
	IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4_StaticFields::get_offset_of_None_7(),
	IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4_StaticFields::get_offset_of_IPv6Any_8(),
	IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4_StaticFields::get_offset_of_IPv6Loopback_9(),
	IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4_StaticFields::get_offset_of_IPv6None_10(),
	IPAddress_t620453DAA3165B1BFB11794B30FF82CE4D5026D4::get_offset_of_m_HashCode_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1204 = { sizeof (IPEndPoint_tE99679FFDD0135AA3881DF00523DD9D6BE7E6622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1204[2] = 
{
	IPEndPoint_tE99679FFDD0135AA3881DF00523DD9D6BE7E6622::get_offset_of_address_0(),
	IPEndPoint_tE99679FFDD0135AA3881DF00523DD9D6BE7E6622::get_offset_of_port_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1205 = { sizeof (IPHostEntry_t1EDA9F93303E37E89B6FFDEBD50858A54D423D9E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1205[3] = 
{
	IPHostEntry_t1EDA9F93303E37E89B6FFDEBD50858A54D423D9E::get_offset_of_addressList_0(),
	IPHostEntry_t1EDA9F93303E37E89B6FFDEBD50858A54D423D9E::get_offset_of_aliases_1(),
	IPHostEntry_t1EDA9F93303E37E89B6FFDEBD50858A54D423D9E::get_offset_of_hostName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1206 = { sizeof (IPv6Address_t87D61FD60FCF8C5C8ED78875ECB69F9CE1485618), -1, sizeof(IPv6Address_t87D61FD60FCF8C5C8ED78875ECB69F9CE1485618_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1206[5] = 
{
	IPv6Address_t87D61FD60FCF8C5C8ED78875ECB69F9CE1485618::get_offset_of_address_0(),
	IPv6Address_t87D61FD60FCF8C5C8ED78875ECB69F9CE1485618::get_offset_of_prefixLength_1(),
	IPv6Address_t87D61FD60FCF8C5C8ED78875ECB69F9CE1485618::get_offset_of_scopeId_2(),
	IPv6Address_t87D61FD60FCF8C5C8ED78875ECB69F9CE1485618_StaticFields::get_offset_of_Loopback_3(),
	IPv6Address_t87D61FD60FCF8C5C8ED78875ECB69F9CE1485618_StaticFields::get_offset_of_Unspecified_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1207 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1208 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1209 = { sizeof (NetworkCredential_t82282628121911251495C07B1DA96F1D09367506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1209[3] = 
{
	NetworkCredential_t82282628121911251495C07B1DA96F1D09367506::get_offset_of_userName_0(),
	NetworkCredential_t82282628121911251495C07B1DA96F1D09367506::get_offset_of_password_1(),
	NetworkCredential_t82282628121911251495C07B1DA96F1D09367506::get_offset_of_domain_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1210 = { sizeof (ProtocolViolationException_tE3B266A71DF466371447A93361C448E63DFC51EC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1211 = { sizeof (SecurityProtocolType_t5B94575EDC844130CC7451E034953844842D19E8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1211[3] = 
{
	SecurityProtocolType_t5B94575EDC844130CC7451E034953844842D19E8::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1212 = { sizeof (ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1212[17] = 
{
	ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3::get_offset_of_uri_0(),
	ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3::get_offset_of_connectionLimit_1(),
	ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3::get_offset_of_maxIdleTime_2(),
	ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3::get_offset_of_currentConnections_3(),
	ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3::get_offset_of_idleSince_4(),
	ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3::get_offset_of_protocolVersion_5(),
	ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3::get_offset_of_certificate_6(),
	ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3::get_offset_of_clientCertificate_7(),
	ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3::get_offset_of_host_8(),
	ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3::get_offset_of_usesProxy_9(),
	ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3::get_offset_of_groups_10(),
	ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3::get_offset_of_sendContinue_11(),
	ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3::get_offset_of_useConnect_12(),
	ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3::get_offset_of_locker_13(),
	ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3::get_offset_of_hostE_14(),
	ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3::get_offset_of_useNagle_15(),
	ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3::get_offset_of_endPointCallback_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1213 = { sizeof (ServicePointManager_t26671C58FEC625E41B784B57AD46A0822DB68465), -1, sizeof(ServicePointManager_t26671C58FEC625E41B784B57AD46A0822DB68465_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1213[10] = 
{
	ServicePointManager_t26671C58FEC625E41B784B57AD46A0822DB68465_StaticFields::get_offset_of_servicePoints_0(),
	ServicePointManager_t26671C58FEC625E41B784B57AD46A0822DB68465_StaticFields::get_offset_of_policy_1(),
	ServicePointManager_t26671C58FEC625E41B784B57AD46A0822DB68465_StaticFields::get_offset_of_defaultConnectionLimit_2(),
	ServicePointManager_t26671C58FEC625E41B784B57AD46A0822DB68465_StaticFields::get_offset_of_maxServicePointIdleTime_3(),
	ServicePointManager_t26671C58FEC625E41B784B57AD46A0822DB68465_StaticFields::get_offset_of_maxServicePoints_4(),
	ServicePointManager_t26671C58FEC625E41B784B57AD46A0822DB68465_StaticFields::get_offset_of__checkCRL_5(),
	ServicePointManager_t26671C58FEC625E41B784B57AD46A0822DB68465_StaticFields::get_offset_of__securityProtocol_6(),
	ServicePointManager_t26671C58FEC625E41B784B57AD46A0822DB68465_StaticFields::get_offset_of_expectContinue_7(),
	ServicePointManager_t26671C58FEC625E41B784B57AD46A0822DB68465_StaticFields::get_offset_of_useNagle_8(),
	ServicePointManager_t26671C58FEC625E41B784B57AD46A0822DB68465_StaticFields::get_offset_of_server_cert_cb_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1214 = { sizeof (SPKey_tC77E61EA2860044974117BBB287BD2160F46D0F1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1214[2] = 
{
	SPKey_tC77E61EA2860044974117BBB287BD2160F46D0F1::get_offset_of_uri_0(),
	SPKey_tC77E61EA2860044974117BBB287BD2160F46D0F1::get_offset_of_use_connect_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1215 = { sizeof (ChainValidationHelper_t528BF08A29D7FB5788210430C2F5280EB1C192C2), -1, sizeof(ChainValidationHelper_t528BF08A29D7FB5788210430C2F5280EB1C192C2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1215[4] = 
{
	ChainValidationHelper_t528BF08A29D7FB5788210430C2F5280EB1C192C2::get_offset_of_sender_0(),
	ChainValidationHelper_t528BF08A29D7FB5788210430C2F5280EB1C192C2::get_offset_of_host_1(),
	ChainValidationHelper_t528BF08A29D7FB5788210430C2F5280EB1C192C2_StaticFields::get_offset_of_is_macosx_2(),
	ChainValidationHelper_t528BF08A29D7FB5788210430C2F5280EB1C192C2_StaticFields::get_offset_of_s_flags_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1216 = { sizeof (SocketAddress_t4A05CA2B1C8AD3D08F4001F64724F58812C385F5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1216[1] = 
{
	SocketAddress_t4A05CA2B1C8AD3D08F4001F64724F58812C385F5::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1217 = { sizeof (WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1217[17] = 
{
	WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8::get_offset_of_handle_0(),
	WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8::get_offset_of_synch_1(),
	WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8::get_offset_of_isCompleted_2(),
	WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8::get_offset_of_cb_3(),
	WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8::get_offset_of_state_4(),
	WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8::get_offset_of_nbytes_5(),
	WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8::get_offset_of_innerAsyncResult_6(),
	WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8::get_offset_of_callbackDone_7(),
	WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8::get_offset_of_exc_8(),
	WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8::get_offset_of_response_9(),
	WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8::get_offset_of_writeStream_10(),
	WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8::get_offset_of_buffer_11(),
	WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8::get_offset_of_offset_12(),
	WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8::get_offset_of_size_13(),
	WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8::get_offset_of_locker_14(),
	WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8::get_offset_of_EndCalled_15(),
	WebAsyncResult_t6CC7C854DE52CEA490C2EA28857A34E626F8E5E8::get_offset_of_AsyncWriteAll_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1218 = { sizeof (ReadState_tBACF66C66CEED1AE759539D70F338F360B83F031)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1218[5] = 
{
	ReadState_tBACF66C66CEED1AE759539D70F338F360B83F031::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1219 = { sizeof (WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28), -1, sizeof(WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1219[32] = 
{
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_sPoint_0(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_nstream_1(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_socket_2(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_socketLock_3(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_status_4(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_initConn_5(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_keepAlive_6(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_buffer_7(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28_StaticFields::get_offset_of_readDoneDelegate_8(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_abortHandler_9(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_abortHelper_10(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_readState_11(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_Data_12(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_chunkedRead_13(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_chunkStream_14(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_queue_15(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_reused_16(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_position_17(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_busy_18(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_priority_request_19(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_ntlm_credentials_20(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_ntlm_authenticated_21(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_unsafe_sharing_22(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_ssl_23(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_certsAvailable_24(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28::get_offset_of_connect_exception_25(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28_StaticFields::get_offset_of_classLock_26(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28_StaticFields::get_offset_of_sslStream_27(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28_StaticFields::get_offset_of_piClient_28(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28_StaticFields::get_offset_of_piServer_29(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28_StaticFields::get_offset_of_piTrustFailure_30(),
	WebConnection_t5679E1D2D5D8C262E3C68A73D00B7CCB42023F28_StaticFields::get_offset_of_method_GetSecurityPolicyFromNonMainThread_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1220 = { sizeof (AbortHelper_tD2C5F18F172369ACDFA4655DEB034CD04578E6C8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1220[1] = 
{
	AbortHelper_tD2C5F18F172369ACDFA4655DEB034CD04578E6C8::get_offset_of_Connection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1221 = { sizeof (WebConnectionData_tDAAF22601B03B750DC57CB05255D88DF0A21DEDA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1221[7] = 
{
	WebConnectionData_tDAAF22601B03B750DC57CB05255D88DF0A21DEDA::get_offset_of_request_0(),
	WebConnectionData_tDAAF22601B03B750DC57CB05255D88DF0A21DEDA::get_offset_of_StatusCode_1(),
	WebConnectionData_tDAAF22601B03B750DC57CB05255D88DF0A21DEDA::get_offset_of_StatusDescription_2(),
	WebConnectionData_tDAAF22601B03B750DC57CB05255D88DF0A21DEDA::get_offset_of_Headers_3(),
	WebConnectionData_tDAAF22601B03B750DC57CB05255D88DF0A21DEDA::get_offset_of_Version_4(),
	WebConnectionData_tDAAF22601B03B750DC57CB05255D88DF0A21DEDA::get_offset_of_stream_5(),
	WebConnectionData_tDAAF22601B03B750DC57CB05255D88DF0A21DEDA::get_offset_of_Challenge_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1222 = { sizeof (WebConnectionGroup_t096269003F5E1F05AEB6921F2B00E2F8FD66F7EF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1222[5] = 
{
	WebConnectionGroup_t096269003F5E1F05AEB6921F2B00E2F8FD66F7EF::get_offset_of_sPoint_0(),
	WebConnectionGroup_t096269003F5E1F05AEB6921F2B00E2F8FD66F7EF::get_offset_of_name_1(),
	WebConnectionGroup_t096269003F5E1F05AEB6921F2B00E2F8FD66F7EF::get_offset_of_connections_2(),
	WebConnectionGroup_t096269003F5E1F05AEB6921F2B00E2F8FD66F7EF::get_offset_of_rnd_3(),
	WebConnectionGroup_t096269003F5E1F05AEB6921F2B00E2F8FD66F7EF::get_offset_of_queue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1223 = { sizeof (WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59), -1, sizeof(WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1223[27] = 
{
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59_StaticFields::get_offset_of_crlf_1(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_isRead_2(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_cnc_3(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_request_4(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_readBuffer_5(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_readBufferOffset_6(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_readBufferSize_7(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_contentLength_8(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_totalRead_9(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_totalWritten_10(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_nextReadCalled_11(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_pendingReads_12(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_pendingWrites_13(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_pending_14(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_allowBuffering_15(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_sendChunked_16(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_writeBuffer_17(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_requestWritten_18(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_headers_19(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_disposed_20(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_headersSent_21(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_locker_22(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_initRead_23(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_read_eof_24(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_complete_request_written_25(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_read_timeout_26(),
	WebConnectionStream_t0E1041BD953CBA895AB48E376B2E8D94CD260A59::get_offset_of_write_timeout_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1224 = { sizeof (WebException_t21F760AAEE9DBB7B74522CE801488D1AB97EF51A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1224[2] = 
{
	WebException_t21F760AAEE9DBB7B74522CE801488D1AB97EF51A::get_offset_of_response_12(),
	WebException_t21F760AAEE9DBB7B74522CE801488D1AB97EF51A::get_offset_of_status_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1225 = { sizeof (WebExceptionStatus_tD0D1FE14644E00431235FA509F94632D27DCCBA4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1225[22] = 
{
	WebExceptionStatus_tD0D1FE14644E00431235FA509F94632D27DCCBA4::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1226 = { sizeof (WebHeaderCollection_t01C9818A1AB6381C83A59AD104E8F0092AC87B66), -1, sizeof(WebHeaderCollection_t01C9818A1AB6381C83A59AD104E8F0092AC87B66_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1226[5] = 
{
	WebHeaderCollection_t01C9818A1AB6381C83A59AD104E8F0092AC87B66_StaticFields::get_offset_of_restricted_12(),
	WebHeaderCollection_t01C9818A1AB6381C83A59AD104E8F0092AC87B66_StaticFields::get_offset_of_multiValue_13(),
	WebHeaderCollection_t01C9818A1AB6381C83A59AD104E8F0092AC87B66_StaticFields::get_offset_of_restricted_response_14(),
	WebHeaderCollection_t01C9818A1AB6381C83A59AD104E8F0092AC87B66::get_offset_of_internallyCreated_15(),
	WebHeaderCollection_t01C9818A1AB6381C83A59AD104E8F0092AC87B66_StaticFields::get_offset_of_allowed_chars_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1227 = { sizeof (WebProxy_t9793DD7C7F74F06D01ED3A0D331E2E9E1579CB9B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1227[5] = 
{
	WebProxy_t9793DD7C7F74F06D01ED3A0D331E2E9E1579CB9B::get_offset_of_address_0(),
	WebProxy_t9793DD7C7F74F06D01ED3A0D331E2E9E1579CB9B::get_offset_of_bypassOnLocal_1(),
	WebProxy_t9793DD7C7F74F06D01ED3A0D331E2E9E1579CB9B::get_offset_of_bypassList_2(),
	WebProxy_t9793DD7C7F74F06D01ED3A0D331E2E9E1579CB9B::get_offset_of_credentials_3(),
	WebProxy_t9793DD7C7F74F06D01ED3A0D331E2E9E1579CB9B::get_offset_of_useDefaultCredentials_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1228 = { sizeof (WebRequest_t079731BC640578743ADC705570EAC33A9BCFB399), -1, sizeof(WebRequest_t079731BC640578743ADC705570EAC33A9BCFB399_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1228[5] = 
{
	WebRequest_t079731BC640578743ADC705570EAC33A9BCFB399_StaticFields::get_offset_of_prefixes_1(),
	WebRequest_t079731BC640578743ADC705570EAC33A9BCFB399_StaticFields::get_offset_of_isDefaultWebProxySet_2(),
	WebRequest_t079731BC640578743ADC705570EAC33A9BCFB399_StaticFields::get_offset_of_defaultWebProxy_3(),
	WebRequest_t079731BC640578743ADC705570EAC33A9BCFB399::get_offset_of_authentication_level_4(),
	WebRequest_t079731BC640578743ADC705570EAC33A9BCFB399_StaticFields::get_offset_of_lockobj_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1229 = { sizeof (WebResponse_t5E9F05BAC005D2105A122CD5973E1D70B9B45325), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1230 = { sizeof (SslProtocols_t6EEE3A59E36DE35F665BE090A90282EB3BDEF3DF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1230[6] = 
{
	SslProtocols_t6EEE3A59E36DE35F665BE090A90282EB3BDEF3DF::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1231 = { sizeof (OSX509Certificates_t07B156FA2DE1D5DC37A0503B93560A2A2E920203), -1, sizeof(OSX509Certificates_t07B156FA2DE1D5DC37A0503B93560A2A2E920203_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1231[1] = 
{
	OSX509Certificates_t07B156FA2DE1D5DC37A0503B93560A2A2E920203_StaticFields::get_offset_of_sslsecpolicy_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1232 = { sizeof (SecTrustResult_tE77308635F0160DD7AC688C7F82B760500E274C6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1232[9] = 
{
	SecTrustResult_tE77308635F0160DD7AC688C7F82B760500E274C6::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1233 = { sizeof (OpenFlags_tCE5CDA06DE32E8C9BCAF8B1B6EC7325C8BDD897F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1233[6] = 
{
	OpenFlags_tCE5CDA06DE32E8C9BCAF8B1B6EC7325C8BDD897F::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1234 = { sizeof (PublicKey_t88CC203B5F644C95B6142BB541AF0D447D2727AB), -1, sizeof(PublicKey_t88CC203B5F644C95B6142BB541AF0D447D2727AB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1234[5] = 
{
	PublicKey_t88CC203B5F644C95B6142BB541AF0D447D2727AB::get_offset_of__key_0(),
	PublicKey_t88CC203B5F644C95B6142BB541AF0D447D2727AB::get_offset_of__keyValue_1(),
	PublicKey_t88CC203B5F644C95B6142BB541AF0D447D2727AB::get_offset_of__params_2(),
	PublicKey_t88CC203B5F644C95B6142BB541AF0D447D2727AB::get_offset_of__oid_3(),
	PublicKey_t88CC203B5F644C95B6142BB541AF0D447D2727AB_StaticFields::get_offset_of_U3CU3Ef__switchU24map9_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1235 = { sizeof (StoreLocation_t4442B9177DB4E4A9EF5FDDC197E46358A3B80A05)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1235[3] = 
{
	StoreLocation_t4442B9177DB4E4A9EF5FDDC197E46358A3B80A05::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1236 = { sizeof (StoreName_t4AC545078CCC3A79483FAB52CA791DB7DD605441)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1236[9] = 
{
	StoreName_t4AC545078CCC3A79483FAB52CA791DB7DD605441::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1237 = { sizeof (X500DistinguishedName_t4E51A8B638BD9EFBFC71A78F0263716509B878C9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1237[1] = 
{
	X500DistinguishedName_t4E51A8B638BD9EFBFC71A78F0263716509B878C9::get_offset_of_name_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1238 = { sizeof (X500DistinguishedNameFlags_t415EAC17471529A4922D15E25AAE8F85B9A419FB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1238[11] = 
{
	X500DistinguishedNameFlags_t415EAC17471529A4922D15E25AAE8F85B9A419FB::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1239 = { sizeof (X509BasicConstraintsExtension_t47598D4831B5E7131CBABBEB2AFA6C171341ACEA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1239[6] = 
{
	0,
	0,
	X509BasicConstraintsExtension_t47598D4831B5E7131CBABBEB2AFA6C171341ACEA::get_offset_of__certificateAuthority_6(),
	X509BasicConstraintsExtension_t47598D4831B5E7131CBABBEB2AFA6C171341ACEA::get_offset_of__hasPathLengthConstraint_7(),
	X509BasicConstraintsExtension_t47598D4831B5E7131CBABBEB2AFA6C171341ACEA::get_offset_of__pathLengthConstraint_8(),
	X509BasicConstraintsExtension_t47598D4831B5E7131CBABBEB2AFA6C171341ACEA::get_offset_of__status_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1240 = { sizeof (X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5), -1, sizeof(X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1240[13] = 
{
	X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5::get_offset_of__archived_5(),
	X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5::get_offset_of__extensions_6(),
	X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5::get_offset_of__name_7(),
	X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5::get_offset_of__serial_8(),
	X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5::get_offset_of__publicKey_9(),
	X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5::get_offset_of_issuer_name_10(),
	X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5::get_offset_of_subject_name_11(),
	X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5::get_offset_of_signature_algorithm_12(),
	X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5::get_offset_of__cert_13(),
	X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5_StaticFields::get_offset_of_empty_error_14(),
	X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5_StaticFields::get_offset_of_commonName_15(),
	X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5_StaticFields::get_offset_of_email_16(),
	X509Certificate2_t8488BFC28EE54980505493CD235FDDAC52A6B4A5_StaticFields::get_offset_of_signedData_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1241 = { sizeof (X509Certificate2Collection_t5F3686D1CB11C6BD038CE9D964F5A4B9BCDF532E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1242 = { sizeof (X509Certificate2Enumerator_tC4E44FF809F0EC4F6CE4B31680843853F9863C53), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1242[1] = 
{
	X509Certificate2Enumerator_tC4E44FF809F0EC4F6CE4B31680843853F9863C53::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1243 = { sizeof (X509CertificateCollection_t4326FD1F39792B6FD659D19F8647F52BE2BBDDC7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1244 = { sizeof (X509CertificateEnumerator_tCDAD2680885D77DB8F10B66BC925C8F68779A09F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1244[1] = 
{
	X509CertificateEnumerator_tCDAD2680885D77DB8F10B66BC925C8F68779A09F::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1245 = { sizeof (X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56), -1, sizeof(X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1245[15] = 
{
	X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56::get_offset_of_location_0(),
	X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56::get_offset_of_elements_1(),
	X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56::get_offset_of_policy_2(),
	X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56::get_offset_of_status_3(),
	X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56_StaticFields::get_offset_of_Empty_4(),
	X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56::get_offset_of_max_path_length_5(),
	X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56::get_offset_of_working_issuer_name_6(),
	X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56::get_offset_of_working_public_key_7(),
	X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56::get_offset_of_bce_restriction_8(),
	X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56::get_offset_of_roots_9(),
	X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56::get_offset_of_cas_10(),
	X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56::get_offset_of_collection_11(),
	X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56_StaticFields::get_offset_of_U3CU3Ef__switchU24mapB_12(),
	X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56_StaticFields::get_offset_of_U3CU3Ef__switchU24mapC_13(),
	X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56_StaticFields::get_offset_of_U3CU3Ef__switchU24mapD_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1246 = { sizeof (X509ChainElement_tAA74792F43074F2F75B321787C15A7E4388B39A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1246[4] = 
{
	X509ChainElement_tAA74792F43074F2F75B321787C15A7E4388B39A9::get_offset_of_certificate_0(),
	X509ChainElement_tAA74792F43074F2F75B321787C15A7E4388B39A9::get_offset_of_status_1(),
	X509ChainElement_tAA74792F43074F2F75B321787C15A7E4388B39A9::get_offset_of_info_2(),
	X509ChainElement_tAA74792F43074F2F75B321787C15A7E4388B39A9::get_offset_of_compressed_status_flags_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1247 = { sizeof (X509ChainElementCollection_tDA6F4291F55CC8640B2097C37F408A211B38E4A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1247[1] = 
{
	X509ChainElementCollection_tDA6F4291F55CC8640B2097C37F408A211B38E4A9::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1248 = { sizeof (X509ChainElementEnumerator_t33C4E24582B01AE8DB3AFA0574C2FE4493053392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1248[1] = 
{
	X509ChainElementEnumerator_t33C4E24582B01AE8DB3AFA0574C2FE4493053392::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1249 = { sizeof (X509ChainPolicy_t50512486AAC7530381A567A301A6C39722FF00B4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1249[8] = 
{
	X509ChainPolicy_t50512486AAC7530381A567A301A6C39722FF00B4::get_offset_of_apps_0(),
	X509ChainPolicy_t50512486AAC7530381A567A301A6C39722FF00B4::get_offset_of_cert_1(),
	X509ChainPolicy_t50512486AAC7530381A567A301A6C39722FF00B4::get_offset_of_store_2(),
	X509ChainPolicy_t50512486AAC7530381A567A301A6C39722FF00B4::get_offset_of_rflag_3(),
	X509ChainPolicy_t50512486AAC7530381A567A301A6C39722FF00B4::get_offset_of_mode_4(),
	X509ChainPolicy_t50512486AAC7530381A567A301A6C39722FF00B4::get_offset_of_timeout_5(),
	X509ChainPolicy_t50512486AAC7530381A567A301A6C39722FF00B4::get_offset_of_vflags_6(),
	X509ChainPolicy_t50512486AAC7530381A567A301A6C39722FF00B4::get_offset_of_vtime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1250 = { sizeof (X509ChainStatus_t60E8382F80CC9E801B291F443817D53825719725)+ sizeof (RuntimeObject), sizeof(X509ChainStatus_t60E8382F80CC9E801B291F443817D53825719725_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1250[2] = 
{
	X509ChainStatus_t60E8382F80CC9E801B291F443817D53825719725::get_offset_of_status_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	X509ChainStatus_t60E8382F80CC9E801B291F443817D53825719725::get_offset_of_info_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1251 = { sizeof (X509ChainStatusFlags_t11227BB58FC95D583FF9B1D93FA0335A8310BCB1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1251[24] = 
{
	X509ChainStatusFlags_t11227BB58FC95D583FF9B1D93FA0335A8310BCB1::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1252 = { sizeof (X509EnhancedKeyUsageExtension_t60BEF6B2641E6D77758CF57269F42113D87B4E12), -1, sizeof(X509EnhancedKeyUsageExtension_t60BEF6B2641E6D77758CF57269F42113D87B4E12_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1252[3] = 
{
	X509EnhancedKeyUsageExtension_t60BEF6B2641E6D77758CF57269F42113D87B4E12::get_offset_of__enhKeyUsage_4(),
	X509EnhancedKeyUsageExtension_t60BEF6B2641E6D77758CF57269F42113D87B4E12::get_offset_of__status_5(),
	X509EnhancedKeyUsageExtension_t60BEF6B2641E6D77758CF57269F42113D87B4E12_StaticFields::get_offset_of_U3CU3Ef__switchU24mapE_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1253 = { sizeof (X509Extension_tF50D0787240E5674487D37FDE5CCD9ABAA39F2EB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1253[1] = 
{
	X509Extension_tF50D0787240E5674487D37FDE5CCD9ABAA39F2EB::get_offset_of__critical_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1254 = { sizeof (X509ExtensionCollection_tE5717291A64EA1198FD91903AC1E8BFC32BDEC3D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1254[1] = 
{
	X509ExtensionCollection_tE5717291A64EA1198FD91903AC1E8BFC32BDEC3D::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1255 = { sizeof (X509ExtensionEnumerator_t6FA184F43ADA293DF1EB760230EAE425B708B655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1255[1] = 
{
	X509ExtensionEnumerator_t6FA184F43ADA293DF1EB760230EAE425B708B655::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1256 = { sizeof (X509FindType_t12DCBE925CCC33AEFE74B273543FE8D71618C9FB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1256[16] = 
{
	X509FindType_t12DCBE925CCC33AEFE74B273543FE8D71618C9FB::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1257 = { sizeof (X509KeyUsageExtension_t63F1A779D02770BDD9506FF39344C559E4406047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1257[5] = 
{
	0,
	0,
	0,
	X509KeyUsageExtension_t63F1A779D02770BDD9506FF39344C559E4406047::get_offset_of__keyUsages_7(),
	X509KeyUsageExtension_t63F1A779D02770BDD9506FF39344C559E4406047::get_offset_of__status_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1258 = { sizeof (X509KeyUsageFlags_tFA401F7CBC9D4D64E760500B3AE3A398203456A5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1258[11] = 
{
	X509KeyUsageFlags_tFA401F7CBC9D4D64E760500B3AE3A398203456A5::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1259 = { sizeof (X509NameType_tDBD3206098FC13285BAEAA7ED247C56A080BF071)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1259[7] = 
{
	X509NameType_tDBD3206098FC13285BAEAA7ED247C56A080BF071::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1260 = { sizeof (X509RevocationFlag_tCCD32E1F66EFE64752DDF4994639BA75A005DF5C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1260[4] = 
{
	X509RevocationFlag_tCCD32E1F66EFE64752DDF4994639BA75A005DF5C::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1261 = { sizeof (X509RevocationMode_t5866243C7CDD857B4492778EB31086BB85AF8416)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1261[4] = 
{
	X509RevocationMode_t5866243C7CDD857B4492778EB31086BB85AF8416::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1262 = { sizeof (X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747), -1, sizeof(X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1262[6] = 
{
	X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747::get_offset_of__name_0(),
	X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747::get_offset_of__location_1(),
	X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747::get_offset_of_list_2(),
	X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747::get_offset_of__flags_3(),
	X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747::get_offset_of_store_4(),
	X509Store_tD9BCE765D6BB9ED9C0F2CF0EC62EF8F5EB04C747_StaticFields::get_offset_of_U3CU3Ef__switchU24mapF_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1263 = { sizeof (X509SubjectKeyIdentifierExtension_tFE27201132D4053906DB91C199AE1E344146F9A0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1263[5] = 
{
	0,
	0,
	X509SubjectKeyIdentifierExtension_tFE27201132D4053906DB91C199AE1E344146F9A0::get_offset_of__subjectKeyIdentifier_6(),
	X509SubjectKeyIdentifierExtension_tFE27201132D4053906DB91C199AE1E344146F9A0::get_offset_of__ski_7(),
	X509SubjectKeyIdentifierExtension_tFE27201132D4053906DB91C199AE1E344146F9A0::get_offset_of__status_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1264 = { sizeof (X509SubjectKeyIdentifierHashAlgorithm_tAAFC5B2151781F2930AA2219EB0D8E3CAD8C6AD8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1264[4] = 
{
	X509SubjectKeyIdentifierHashAlgorithm_tAAFC5B2151781F2930AA2219EB0D8E3CAD8C6AD8::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1265 = { sizeof (X509VerificationFlags_t6F8A7F75FAD2E7C9F75AB3F022F19C42BA358F42)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1265[15] = 
{
	X509VerificationFlags_t6F8A7F75FAD2E7C9F75AB3F022F19C42BA358F42::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1266 = { sizeof (AsnDecodeStatus_t0649A9770A72159487713F9957A30247B87DE040)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1266[7] = 
{
	AsnDecodeStatus_t0649A9770A72159487713F9957A30247B87DE040::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1267 = { sizeof (AsnEncodedData_t1B888B0D340736ECF8B35405760DC0FE62235A99), -1, sizeof(AsnEncodedData_t1B888B0D340736ECF8B35405760DC0FE62235A99_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1267[3] = 
{
	AsnEncodedData_t1B888B0D340736ECF8B35405760DC0FE62235A99::get_offset_of__oid_0(),
	AsnEncodedData_t1B888B0D340736ECF8B35405760DC0FE62235A99::get_offset_of__raw_1(),
	AsnEncodedData_t1B888B0D340736ECF8B35405760DC0FE62235A99_StaticFields::get_offset_of_U3CU3Ef__switchU24mapA_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1268 = { sizeof (Oid_tF92DE54931499E6C4157FE23D6706AD98E1FDB27), -1, sizeof(Oid_tF92DE54931499E6C4157FE23D6706AD98E1FDB27_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1268[3] = 
{
	Oid_tF92DE54931499E6C4157FE23D6706AD98E1FDB27::get_offset_of__value_0(),
	Oid_tF92DE54931499E6C4157FE23D6706AD98E1FDB27::get_offset_of__name_1(),
	Oid_tF92DE54931499E6C4157FE23D6706AD98E1FDB27_StaticFields::get_offset_of_U3CU3Ef__switchU24map10_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1269 = { sizeof (OidCollection_tDA5968A49C496611C36F3E30F8A675D6F57E08D0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1269[2] = 
{
	OidCollection_tDA5968A49C496611C36F3E30F8A675D6F57E08D0::get_offset_of__list_0(),
	OidCollection_tDA5968A49C496611C36F3E30F8A675D6F57E08D0::get_offset_of__readOnly_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1270 = { sizeof (OidEnumerator_tD8C4ACFCB867089C9DFC37EE1F2691046228DDE8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1270[2] = 
{
	OidEnumerator_tD8C4ACFCB867089C9DFC37EE1F2691046228DDE8::get_offset_of__collection_0(),
	OidEnumerator_tD8C4ACFCB867089C9DFC37EE1F2691046228DDE8::get_offset_of__position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1271 = { sizeof (BaseMachine_t7846A209B98125A8E5B5FAFE5258CC5CBFE2F857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1271[1] = 
{
	BaseMachine_t7846A209B98125A8E5B5FAFE5258CC5CBFE2F857::get_offset_of_needs_groups_or_captures_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1272 = { sizeof (Capture_t426EB72741D828F4539C131E1DC839DD00F01F22), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1272[3] = 
{
	Capture_t426EB72741D828F4539C131E1DC839DD00F01F22::get_offset_of_index_0(),
	Capture_t426EB72741D828F4539C131E1DC839DD00F01F22::get_offset_of_length_1(),
	Capture_t426EB72741D828F4539C131E1DC839DD00F01F22::get_offset_of_text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1273 = { sizeof (CaptureCollection_t5F80E90F8954A443D29D1EBEFC7119831DFB9C20), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1273[1] = 
{
	CaptureCollection_t5F80E90F8954A443D29D1EBEFC7119831DFB9C20::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1274 = { sizeof (Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C), -1, sizeof(Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1274[3] = 
{
	Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C_StaticFields::get_offset_of_Fail_3(),
	Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C::get_offset_of_success_4(),
	Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C::get_offset_of_captures_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1275 = { sizeof (GroupCollection_tBEA4EDB210F04963227CBC58E1434202F3CEC968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1275[2] = 
{
	GroupCollection_tBEA4EDB210F04963227CBC58E1434202F3CEC968::get_offset_of_list_0(),
	GroupCollection_tBEA4EDB210F04963227CBC58E1434202F3CEC968::get_offset_of_gap_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1276 = { sizeof (Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53), -1, sizeof(Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1276[5] = 
{
	Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53::get_offset_of_regex_6(),
	Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53::get_offset_of_machine_7(),
	Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53::get_offset_of_text_length_8(),
	Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53::get_offset_of_groups_9(),
	Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53_StaticFields::get_offset_of_empty_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1277 = { sizeof (MatchCollection_tD651B345093D8CEC4F8E3302458C17355E017B2E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1277[2] = 
{
	MatchCollection_tD651B345093D8CEC4F8E3302458C17355E017B2E::get_offset_of_current_0(),
	MatchCollection_tD651B345093D8CEC4F8E3302458C17355E017B2E::get_offset_of_list_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1278 = { sizeof (Enumerator_tEA319EDBF9ADA404733366A75202340DAD456FD8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1278[2] = 
{
	Enumerator_tEA319EDBF9ADA404733366A75202340DAD456FD8::get_offset_of_index_0(),
	Enumerator_tEA319EDBF9ADA404733366A75202340DAD456FD8::get_offset_of_coll_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1279 = { sizeof (Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B), -1, sizeof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1279[9] = 
{
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B_StaticFields::get_offset_of_cache_0(),
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B::get_offset_of_machineFactory_1(),
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B::get_offset_of_mapping_2(),
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B::get_offset_of_group_count_3(),
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B::get_offset_of_gap_4(),
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B::get_offset_of_group_names_5(),
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B::get_offset_of_group_numbers_6(),
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B::get_offset_of_pattern_7(),
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B::get_offset_of_roptions_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1280 = { sizeof (RegexOptions_t6DDB25DC8ABB8CFC65500D99B4CDAFF3B26D0624)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1280[10] = 
{
	RegexOptions_t6DDB25DC8ABB8CFC65500D99B4CDAFF3B26D0624::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1281 = { sizeof (OpCode_t5566B50EEFDECA010CAB22202F40D1B5799D23F7)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1281[26] = 
{
	OpCode_t5566B50EEFDECA010CAB22202F40D1B5799D23F7::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1282 = { sizeof (OpFlags_t5688D3EFBB323A2AA84AAABADD74AB885286AD32)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1282[6] = 
{
	OpFlags_t5688D3EFBB323A2AA84AAABADD74AB885286AD32::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1283 = { sizeof (Position_t19E037E87BCB1E737603EA15C59A22D1C2E34C1C)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1283[11] = 
{
	Position_t19E037E87BCB1E737603EA15C59A22D1C2E34C1C::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1284 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1285 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1286 = { sizeof (FactoryCache_t8BE3083C5F3C2185BA9AE97F4886454CB85C06B9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1286[3] = 
{
	FactoryCache_t8BE3083C5F3C2185BA9AE97F4886454CB85C06B9::get_offset_of_capacity_0(),
	FactoryCache_t8BE3083C5F3C2185BA9AE97F4886454CB85C06B9::get_offset_of_factories_1(),
	FactoryCache_t8BE3083C5F3C2185BA9AE97F4886454CB85C06B9::get_offset_of_mru_list_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1287 = { sizeof (Key_tFC08407FC450EA826C414756A28BCA5F5889F3C7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1287[2] = 
{
	Key_tFC08407FC450EA826C414756A28BCA5F5889F3C7::get_offset_of_pattern_0(),
	Key_tFC08407FC450EA826C414756A28BCA5F5889F3C7::get_offset_of_options_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1288 = { sizeof (MRUList_t54E602EB72FB2B58075AB7BF8F013121606B1E1C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1288[2] = 
{
	MRUList_t54E602EB72FB2B58075AB7BF8F013121606B1E1C::get_offset_of_head_0(),
	MRUList_t54E602EB72FB2B58075AB7BF8F013121606B1E1C::get_offset_of_tail_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1289 = { sizeof (Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1289[3] = 
{
	Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3::get_offset_of_value_0(),
	Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3::get_offset_of_previous_1(),
	Node_t5EF0DD2DA92A9C854A50A4ACD8513B5EA2D701A3::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1290 = { sizeof (Category_t4811E82728B9344F24776EE98B58FC11546B342C)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1290[146] = 
{
	Category_t4811E82728B9344F24776EE98B58FC11546B342C::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1291 = { sizeof (CategoryUtils_t4D6CED9206C1B5C61D0D93CCE8E416EBB271DB89), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1292 = { sizeof (LinkRef_t4A317D95D5F6964647AE5BBB13F5CAE3F1C81FAB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1293 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1294 = { sizeof (InterpreterFactory_t9E80B031E667166B225D6EBC813F53DB862A7193), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1294[4] = 
{
	InterpreterFactory_t9E80B031E667166B225D6EBC813F53DB862A7193::get_offset_of_mapping_0(),
	InterpreterFactory_t9E80B031E667166B225D6EBC813F53DB862A7193::get_offset_of_pattern_1(),
	InterpreterFactory_t9E80B031E667166B225D6EBC813F53DB862A7193::get_offset_of_namesMapping_2(),
	InterpreterFactory_t9E80B031E667166B225D6EBC813F53DB862A7193::get_offset_of_gap_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1295 = { sizeof (PatternCompiler_tF9296446862317819B849C8703585892EEFFBB91), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1295[1] = 
{
	PatternCompiler_tF9296446862317819B849C8703585892EEFFBB91::get_offset_of_pgm_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1296 = { sizeof (PatternLinkStack_t984F6CAC34DFB10A38B61E62BAE99F57C35007F5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1296[1] = 
{
	PatternLinkStack_t984F6CAC34DFB10A38B61E62BAE99F57C35007F5::get_offset_of_link_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1297 = { sizeof (Link_t52781900883945F8B1D533580C6399C413418639)+ sizeof (RuntimeObject), sizeof(Link_t52781900883945F8B1D533580C6399C413418639 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1297[2] = 
{
	Link_t52781900883945F8B1D533580C6399C413418639::get_offset_of_base_addr_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Link_t52781900883945F8B1D533580C6399C413418639::get_offset_of_offset_addr_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1298 = { sizeof (LinkStack_t71FC86A248572C5736999BDA60C154F0AD923949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1298[1] = 
{
	LinkStack_t71FC86A248572C5736999BDA60C154F0AD923949::get_offset_of_stack_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1299 = { sizeof (Mark_t6F95F4C0F20F9D6B1059C179A2E3940267A6672F)+ sizeof (RuntimeObject), sizeof(Mark_t6F95F4C0F20F9D6B1059C179A2E3940267A6672F ), 0, 0 };
extern const int32_t g_FieldOffsetTable1299[3] = 
{
	Mark_t6F95F4C0F20F9D6B1059C179A2E3940267A6672F::get_offset_of_Start_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Mark_t6F95F4C0F20F9D6B1059C179A2E3940267A6672F::get_offset_of_End_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Mark_t6F95F4C0F20F9D6B1059C179A2E3940267A6672F::get_offset_of_Previous_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
