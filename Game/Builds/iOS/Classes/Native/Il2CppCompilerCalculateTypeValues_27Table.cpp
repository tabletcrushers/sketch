﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// ClipperLib.LocalMinima
struct LocalMinima_t095BEF49CA303DCA0F6C660B4EFE330C55712D05;
// ClipperLib.OutPt
struct OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0;
// ClipperLib.PolyNode
struct PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB;
// ClipperLib.Scanbeam
struct Scanbeam_t5B92102C6C777A0213DB5CB42DCBE782F0486AA7;
// ClipperLib.TEdge
struct TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335;
// ImportSettings
struct ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3;
// Kolibri2d.BezierSpline
struct BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE;
// Matrix
struct Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78;
// Poly2Tri.AdvancingFront
struct AdvancingFront_t103C9271D0DE07CA7D45EEDBE2ACBD93DC215340;
// Poly2Tri.AdvancingFrontNode
struct AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F;
// Poly2Tri.DTSweepBasin
struct DTSweepBasin_t74AB6315A3629FEF0FC85456015FA9537C67B2DD;
// Poly2Tri.DTSweepConstraint
struct DTSweepConstraint_t8C5C973CF68522DA5D7828F45161FE7A22203819;
// Poly2Tri.DTSweepEdgeEvent
struct DTSweepEdgeEvent_tBD7A886997C86280A3AE982B4E8C86325A28E422;
// Poly2Tri.DTSweepPointComparator
struct DTSweepPointComparator_tFA3FA00C634B6162D20BD76BE5E4C0CDB943ACAC;
// Poly2Tri.DelaunayTriangle
struct DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891;
// Poly2Tri.Edge
struct Edge_t5170DF73FC08083B2752D6AD8F039BF371D0B79B;
// Poly2Tri.ITriangulatable
struct ITriangulatable_t454EDBE51808A5AD41BD5725A706B9E7178A9DBE;
// Poly2Tri.Point2D
struct Point2D_t35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF;
// Poly2Tri.Point2DList
struct Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45;
// Poly2Tri.PolygonPoint
struct PolygonPoint_tAED3172793E804A09A2EB2FEDCB0EBA5159A08FE;
// Poly2Tri.Rect2D
struct Rect2D_t72E37904F5FB0718F0E3D96A0EBC0A77E80F6001;
// Poly2Tri.TriangulationContext
struct TriangulationContext_t0153B91CA7F38202C7CC645BC07A67774802360E;
// Poly2Tri.TriangulationDebugContext
struct TriangulationDebugContext_tF9458C2C5E3E324954C544E9FB4044AC0B1F103C;
// Poly2Tri.TriangulationPoint
struct TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4;
// SimplySVG.CollisionShapeData
struct CollisionShapeData_t09D2AF9F5D9E3B0C513049F97C117CF4EB9B2C6B;
// SimplySVG.GraphicalAttributes
struct GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D;
// SimplySVG.SVGDocument
struct SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7;
// SimplySVG.SVGElement
struct SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888;
// SimplySVG.TransformAttributes
struct TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3;
// System.Char[]
struct CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Int32>>
struct Dictionary_2_t4807FD27415C3FDCCC28B95BCB1284A5DE60B015;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1;
// System.Collections.Generic.Dictionary`2<System.UInt32,Poly2Tri.Point2DList>
struct Dictionary_2_t3F61D5FC60EDBF690FB41A7FCCE46CE484F03DC1;
// System.Collections.Generic.Dictionary`2<System.UInt32,Poly2Tri.TriangulationConstraint>
struct Dictionary_2_tCDE048950A50CBAD73ABA536AFC71671996F63AC;
// System.Collections.Generic.Dictionary`2<System.UInt32,Poly2Tri.TriangulationPoint>
struct Dictionary_2_tD65C9BFCB816ED88CB099B735C202522EE4C155B;
// System.Collections.Generic.IComparer`1<ClipperLib.IntersectNode>
struct IComparer_1_t22A3C5823594F3153869929F975B4ABC1DE64820;
// System.Collections.Generic.IList`1<Poly2Tri.DelaunayTriangle>
struct IList_1_tFB41BEFF1C7781A6A1411E0CA7BA2558D4151842;
// System.Collections.Generic.IList`1<Poly2Tri.Point2D>
struct IList_1_tE057903233C442F3C1D9A42748E5F26F54145761;
// System.Collections.Generic.List`1<ClipperLib.DoublePoint>
struct List_1_t4D3F62B7FE29F08FF7B94BC47C7B803913F1CA69;
// System.Collections.Generic.List`1<ClipperLib.IntPoint>
struct List_1_tD323E55C055A7CA7984664415A9E8332BB24408B;
// System.Collections.Generic.List`1<ClipperLib.IntersectNode>
struct List_1_tBD9A00829C87F33D52CDBA97917C242F86F197DD;
// System.Collections.Generic.List`1<ClipperLib.Join>
struct List_1_t86786B66D45FFA0F79F48798DD1F2D26FBA2B49D;
// System.Collections.Generic.List`1<ClipperLib.OutRec>
struct List_1_tBA88214EB9561433F89AC6CF58821F21467839EC;
// System.Collections.Generic.List`1<ClipperLib.PolyNode>
struct List_1_tAFCC6AECDB60026588FFF5817A80B9CBDB4F349F;
// System.Collections.Generic.List`1<Matrix>
struct List_1_t56C6FF99794FDFD622FFD321BFB93EE419B4D89D;
// System.Collections.Generic.List`1<Poly2Tri.Contour>
struct List_1_t848BD4B8703132660D7117532F3ADD94076BDA26;
// System.Collections.Generic.List`1<Poly2Tri.DTSweepConstraint>
struct List_1_t2A29EEE2F8999DD054E1601D541DCDFD34536C26;
// System.Collections.Generic.List`1<Poly2Tri.DelaunayTriangle>
struct List_1_t674C22B1FE8663E709E7B883A14E3C4B7DE3810B;
// System.Collections.Generic.List`1<Poly2Tri.EdgeIntersectInfo>
struct List_1_tEA9596B52619CADF55F3EDA1C2E36BFE9AA4866C;
// System.Collections.Generic.List`1<Poly2Tri.Point2D>
struct List_1_tE2F7563155465A354D6F465B4909C64C82FC2141;
// System.Collections.Generic.List`1<Poly2Tri.Polygon>
struct List_1_tDE4178F994B399BD69F0374B4BF2077E0A5D8AAB;
// System.Collections.Generic.List`1<Poly2Tri.SplitComplexPolygonNode>
struct List_1_tB7E4690795C8CEB9058BB9A80E863D61B62B269C;
// System.Collections.Generic.List`1<Poly2Tri.TriangulationPoint>
struct List_1_t79DDC71A203AB7658B258674FD0CFCF9340AB5A7;
// System.Collections.Generic.List`1<SimplySVG.CollisionShapeData>
struct List_1_tCBD9667398A994BB4DC992902858F6B45B0C5296;
// System.Collections.Generic.List`1<SimplySVG.SVGDocument>
struct List_1_t8DB9ECA2C04E8B430E3A3CFEC58D2048982C9C8B;
// System.Collections.Generic.List`1<SimplySVG.SVGElement>
struct List_1_tDACA481967F32AB5A83EA055EF5C33BB4E3148ED;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<ClipperLib.IntPoint>>
struct List_1_tAF7BD58C351F1353D1522DB1DC6B35AA5269CC6E;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<ClipperLib.TEdge>>
struct List_1_tF55F2A34915862FD0A4D334AE81ACF3173EB550D;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t4A961510635950236678F1B3B2436ECCAF28713A;
// System.Collections.Generic.List`1<System.String>
struct List_1_t7600B3EA144F32C2120D420AB63C9E014D113CF1;
// System.Collections.Generic.List`1<UnityEngine.Mesh>
struct List_1_tBE402A53CD5331E0CB483C591CFDC8C6827F35B8;
// System.Collections.Generic.Stack`1<SimplySVG.SVGElement>
struct Stack_1_t9C7F23AAE760B11BBC2B4E0504A4CBBC785159CC;
// System.Collections.IDictionary
struct IDictionary_tD35B9437F08BE98D1E0B295CC73C48E168CAB316;
// System.Double[0...,0...]
struct DoubleU5B0___U2C0___U5D_t73D98A29699CA200BF5BF3747873CA1F70FD3478;
// System.Int32[]
struct Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074;
// System.IntPtr[]
struct IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659;
// System.Random
struct Random_tE77EE3BF254F50AC5B022C695F4F1DD99B425AE6;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B;
// System.Void
struct Void_tDB81A15FA2AB53E2401A76B745D215397B29F783;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef CLIPPERBASE_TBFEE1941825FBF171B1F5EDB6DFC3247136BC4DD_H
#define CLIPPERBASE_TBFEE1941825FBF171B1F5EDB6DFC3247136BC4DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.ClipperBase
struct  ClipperBase_tBFEE1941825FBF171B1F5EDB6DFC3247136BC4DD  : public RuntimeObject
{
public:
	// ClipperLib.LocalMinima ClipperLib.ClipperBase::m_MinimaList
	LocalMinima_t095BEF49CA303DCA0F6C660B4EFE330C55712D05 * ___m_MinimaList_6;
	// ClipperLib.LocalMinima ClipperLib.ClipperBase::m_CurrentLM
	LocalMinima_t095BEF49CA303DCA0F6C660B4EFE330C55712D05 * ___m_CurrentLM_7;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<ClipperLib.TEdge>> ClipperLib.ClipperBase::m_edges
	List_1_tF55F2A34915862FD0A4D334AE81ACF3173EB550D * ___m_edges_8;
	// System.Boolean ClipperLib.ClipperBase::m_UseFullRange
	bool ___m_UseFullRange_9;
	// System.Boolean ClipperLib.ClipperBase::m_HasOpenPaths
	bool ___m_HasOpenPaths_10;
	// System.Boolean ClipperLib.ClipperBase::<PreserveCollinear>k__BackingField
	bool ___U3CPreserveCollinearU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_m_MinimaList_6() { return static_cast<int32_t>(offsetof(ClipperBase_tBFEE1941825FBF171B1F5EDB6DFC3247136BC4DD, ___m_MinimaList_6)); }
	inline LocalMinima_t095BEF49CA303DCA0F6C660B4EFE330C55712D05 * get_m_MinimaList_6() const { return ___m_MinimaList_6; }
	inline LocalMinima_t095BEF49CA303DCA0F6C660B4EFE330C55712D05 ** get_address_of_m_MinimaList_6() { return &___m_MinimaList_6; }
	inline void set_m_MinimaList_6(LocalMinima_t095BEF49CA303DCA0F6C660B4EFE330C55712D05 * value)
	{
		___m_MinimaList_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_MinimaList_6), value);
	}

	inline static int32_t get_offset_of_m_CurrentLM_7() { return static_cast<int32_t>(offsetof(ClipperBase_tBFEE1941825FBF171B1F5EDB6DFC3247136BC4DD, ___m_CurrentLM_7)); }
	inline LocalMinima_t095BEF49CA303DCA0F6C660B4EFE330C55712D05 * get_m_CurrentLM_7() const { return ___m_CurrentLM_7; }
	inline LocalMinima_t095BEF49CA303DCA0F6C660B4EFE330C55712D05 ** get_address_of_m_CurrentLM_7() { return &___m_CurrentLM_7; }
	inline void set_m_CurrentLM_7(LocalMinima_t095BEF49CA303DCA0F6C660B4EFE330C55712D05 * value)
	{
		___m_CurrentLM_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentLM_7), value);
	}

	inline static int32_t get_offset_of_m_edges_8() { return static_cast<int32_t>(offsetof(ClipperBase_tBFEE1941825FBF171B1F5EDB6DFC3247136BC4DD, ___m_edges_8)); }
	inline List_1_tF55F2A34915862FD0A4D334AE81ACF3173EB550D * get_m_edges_8() const { return ___m_edges_8; }
	inline List_1_tF55F2A34915862FD0A4D334AE81ACF3173EB550D ** get_address_of_m_edges_8() { return &___m_edges_8; }
	inline void set_m_edges_8(List_1_tF55F2A34915862FD0A4D334AE81ACF3173EB550D * value)
	{
		___m_edges_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_edges_8), value);
	}

	inline static int32_t get_offset_of_m_UseFullRange_9() { return static_cast<int32_t>(offsetof(ClipperBase_tBFEE1941825FBF171B1F5EDB6DFC3247136BC4DD, ___m_UseFullRange_9)); }
	inline bool get_m_UseFullRange_9() const { return ___m_UseFullRange_9; }
	inline bool* get_address_of_m_UseFullRange_9() { return &___m_UseFullRange_9; }
	inline void set_m_UseFullRange_9(bool value)
	{
		___m_UseFullRange_9 = value;
	}

	inline static int32_t get_offset_of_m_HasOpenPaths_10() { return static_cast<int32_t>(offsetof(ClipperBase_tBFEE1941825FBF171B1F5EDB6DFC3247136BC4DD, ___m_HasOpenPaths_10)); }
	inline bool get_m_HasOpenPaths_10() const { return ___m_HasOpenPaths_10; }
	inline bool* get_address_of_m_HasOpenPaths_10() { return &___m_HasOpenPaths_10; }
	inline void set_m_HasOpenPaths_10(bool value)
	{
		___m_HasOpenPaths_10 = value;
	}

	inline static int32_t get_offset_of_U3CPreserveCollinearU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ClipperBase_tBFEE1941825FBF171B1F5EDB6DFC3247136BC4DD, ___U3CPreserveCollinearU3Ek__BackingField_11)); }
	inline bool get_U3CPreserveCollinearU3Ek__BackingField_11() const { return ___U3CPreserveCollinearU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CPreserveCollinearU3Ek__BackingField_11() { return &___U3CPreserveCollinearU3Ek__BackingField_11; }
	inline void set_U3CPreserveCollinearU3Ek__BackingField_11(bool value)
	{
		___U3CPreserveCollinearU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPERBASE_TBFEE1941825FBF171B1F5EDB6DFC3247136BC4DD_H
#ifndef LOCALMINIMA_T095BEF49CA303DCA0F6C660B4EFE330C55712D05_H
#define LOCALMINIMA_T095BEF49CA303DCA0F6C660B4EFE330C55712D05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.LocalMinima
struct  LocalMinima_t095BEF49CA303DCA0F6C660B4EFE330C55712D05  : public RuntimeObject
{
public:
	// System.Int64 ClipperLib.LocalMinima::Y
	int64_t ___Y_0;
	// ClipperLib.TEdge ClipperLib.LocalMinima::LeftBound
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * ___LeftBound_1;
	// ClipperLib.TEdge ClipperLib.LocalMinima::RightBound
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * ___RightBound_2;
	// ClipperLib.LocalMinima ClipperLib.LocalMinima::Next
	LocalMinima_t095BEF49CA303DCA0F6C660B4EFE330C55712D05 * ___Next_3;

public:
	inline static int32_t get_offset_of_Y_0() { return static_cast<int32_t>(offsetof(LocalMinima_t095BEF49CA303DCA0F6C660B4EFE330C55712D05, ___Y_0)); }
	inline int64_t get_Y_0() const { return ___Y_0; }
	inline int64_t* get_address_of_Y_0() { return &___Y_0; }
	inline void set_Y_0(int64_t value)
	{
		___Y_0 = value;
	}

	inline static int32_t get_offset_of_LeftBound_1() { return static_cast<int32_t>(offsetof(LocalMinima_t095BEF49CA303DCA0F6C660B4EFE330C55712D05, ___LeftBound_1)); }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * get_LeftBound_1() const { return ___LeftBound_1; }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 ** get_address_of_LeftBound_1() { return &___LeftBound_1; }
	inline void set_LeftBound_1(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * value)
	{
		___LeftBound_1 = value;
		Il2CppCodeGenWriteBarrier((&___LeftBound_1), value);
	}

	inline static int32_t get_offset_of_RightBound_2() { return static_cast<int32_t>(offsetof(LocalMinima_t095BEF49CA303DCA0F6C660B4EFE330C55712D05, ___RightBound_2)); }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * get_RightBound_2() const { return ___RightBound_2; }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 ** get_address_of_RightBound_2() { return &___RightBound_2; }
	inline void set_RightBound_2(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * value)
	{
		___RightBound_2 = value;
		Il2CppCodeGenWriteBarrier((&___RightBound_2), value);
	}

	inline static int32_t get_offset_of_Next_3() { return static_cast<int32_t>(offsetof(LocalMinima_t095BEF49CA303DCA0F6C660B4EFE330C55712D05, ___Next_3)); }
	inline LocalMinima_t095BEF49CA303DCA0F6C660B4EFE330C55712D05 * get_Next_3() const { return ___Next_3; }
	inline LocalMinima_t095BEF49CA303DCA0F6C660B4EFE330C55712D05 ** get_address_of_Next_3() { return &___Next_3; }
	inline void set_Next_3(LocalMinima_t095BEF49CA303DCA0F6C660B4EFE330C55712D05 * value)
	{
		___Next_3 = value;
		Il2CppCodeGenWriteBarrier((&___Next_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALMINIMA_T095BEF49CA303DCA0F6C660B4EFE330C55712D05_H
#ifndef MYINTERSECTNODESORT_T331A7E1CE22C08205336AB3C973DFEE141F7E04A_H
#define MYINTERSECTNODESORT_T331A7E1CE22C08205336AB3C973DFEE141F7E04A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.MyIntersectNodeSort
struct  MyIntersectNodeSort_t331A7E1CE22C08205336AB3C973DFEE141F7E04A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MYINTERSECTNODESORT_T331A7E1CE22C08205336AB3C973DFEE141F7E04A_H
#ifndef OUTREC_T18F55EE4090D9FE89ED98D5BFE06E49149E05B82_H
#define OUTREC_T18F55EE4090D9FE89ED98D5BFE06E49149E05B82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.OutRec
struct  OutRec_t18F55EE4090D9FE89ED98D5BFE06E49149E05B82  : public RuntimeObject
{
public:
	// System.Int32 ClipperLib.OutRec::Idx
	int32_t ___Idx_0;
	// System.Boolean ClipperLib.OutRec::IsHole
	bool ___IsHole_1;
	// System.Boolean ClipperLib.OutRec::IsOpen
	bool ___IsOpen_2;
	// ClipperLib.OutRec ClipperLib.OutRec::FirstLeft
	OutRec_t18F55EE4090D9FE89ED98D5BFE06E49149E05B82 * ___FirstLeft_3;
	// ClipperLib.OutPt ClipperLib.OutRec::Pts
	OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0 * ___Pts_4;
	// ClipperLib.OutPt ClipperLib.OutRec::BottomPt
	OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0 * ___BottomPt_5;
	// ClipperLib.PolyNode ClipperLib.OutRec::PolyNode
	PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB * ___PolyNode_6;

public:
	inline static int32_t get_offset_of_Idx_0() { return static_cast<int32_t>(offsetof(OutRec_t18F55EE4090D9FE89ED98D5BFE06E49149E05B82, ___Idx_0)); }
	inline int32_t get_Idx_0() const { return ___Idx_0; }
	inline int32_t* get_address_of_Idx_0() { return &___Idx_0; }
	inline void set_Idx_0(int32_t value)
	{
		___Idx_0 = value;
	}

	inline static int32_t get_offset_of_IsHole_1() { return static_cast<int32_t>(offsetof(OutRec_t18F55EE4090D9FE89ED98D5BFE06E49149E05B82, ___IsHole_1)); }
	inline bool get_IsHole_1() const { return ___IsHole_1; }
	inline bool* get_address_of_IsHole_1() { return &___IsHole_1; }
	inline void set_IsHole_1(bool value)
	{
		___IsHole_1 = value;
	}

	inline static int32_t get_offset_of_IsOpen_2() { return static_cast<int32_t>(offsetof(OutRec_t18F55EE4090D9FE89ED98D5BFE06E49149E05B82, ___IsOpen_2)); }
	inline bool get_IsOpen_2() const { return ___IsOpen_2; }
	inline bool* get_address_of_IsOpen_2() { return &___IsOpen_2; }
	inline void set_IsOpen_2(bool value)
	{
		___IsOpen_2 = value;
	}

	inline static int32_t get_offset_of_FirstLeft_3() { return static_cast<int32_t>(offsetof(OutRec_t18F55EE4090D9FE89ED98D5BFE06E49149E05B82, ___FirstLeft_3)); }
	inline OutRec_t18F55EE4090D9FE89ED98D5BFE06E49149E05B82 * get_FirstLeft_3() const { return ___FirstLeft_3; }
	inline OutRec_t18F55EE4090D9FE89ED98D5BFE06E49149E05B82 ** get_address_of_FirstLeft_3() { return &___FirstLeft_3; }
	inline void set_FirstLeft_3(OutRec_t18F55EE4090D9FE89ED98D5BFE06E49149E05B82 * value)
	{
		___FirstLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___FirstLeft_3), value);
	}

	inline static int32_t get_offset_of_Pts_4() { return static_cast<int32_t>(offsetof(OutRec_t18F55EE4090D9FE89ED98D5BFE06E49149E05B82, ___Pts_4)); }
	inline OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0 * get_Pts_4() const { return ___Pts_4; }
	inline OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0 ** get_address_of_Pts_4() { return &___Pts_4; }
	inline void set_Pts_4(OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0 * value)
	{
		___Pts_4 = value;
		Il2CppCodeGenWriteBarrier((&___Pts_4), value);
	}

	inline static int32_t get_offset_of_BottomPt_5() { return static_cast<int32_t>(offsetof(OutRec_t18F55EE4090D9FE89ED98D5BFE06E49149E05B82, ___BottomPt_5)); }
	inline OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0 * get_BottomPt_5() const { return ___BottomPt_5; }
	inline OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0 ** get_address_of_BottomPt_5() { return &___BottomPt_5; }
	inline void set_BottomPt_5(OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0 * value)
	{
		___BottomPt_5 = value;
		Il2CppCodeGenWriteBarrier((&___BottomPt_5), value);
	}

	inline static int32_t get_offset_of_PolyNode_6() { return static_cast<int32_t>(offsetof(OutRec_t18F55EE4090D9FE89ED98D5BFE06E49149E05B82, ___PolyNode_6)); }
	inline PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB * get_PolyNode_6() const { return ___PolyNode_6; }
	inline PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB ** get_address_of_PolyNode_6() { return &___PolyNode_6; }
	inline void set_PolyNode_6(PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB * value)
	{
		___PolyNode_6 = value;
		Il2CppCodeGenWriteBarrier((&___PolyNode_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTREC_T18F55EE4090D9FE89ED98D5BFE06E49149E05B82_H
#ifndef SCANBEAM_T5B92102C6C777A0213DB5CB42DCBE782F0486AA7_H
#define SCANBEAM_T5B92102C6C777A0213DB5CB42DCBE782F0486AA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.Scanbeam
struct  Scanbeam_t5B92102C6C777A0213DB5CB42DCBE782F0486AA7  : public RuntimeObject
{
public:
	// System.Int64 ClipperLib.Scanbeam::Y
	int64_t ___Y_0;
	// ClipperLib.Scanbeam ClipperLib.Scanbeam::Next
	Scanbeam_t5B92102C6C777A0213DB5CB42DCBE782F0486AA7 * ___Next_1;

public:
	inline static int32_t get_offset_of_Y_0() { return static_cast<int32_t>(offsetof(Scanbeam_t5B92102C6C777A0213DB5CB42DCBE782F0486AA7, ___Y_0)); }
	inline int64_t get_Y_0() const { return ___Y_0; }
	inline int64_t* get_address_of_Y_0() { return &___Y_0; }
	inline void set_Y_0(int64_t value)
	{
		___Y_0 = value;
	}

	inline static int32_t get_offset_of_Next_1() { return static_cast<int32_t>(offsetof(Scanbeam_t5B92102C6C777A0213DB5CB42DCBE782F0486AA7, ___Next_1)); }
	inline Scanbeam_t5B92102C6C777A0213DB5CB42DCBE782F0486AA7 * get_Next_1() const { return ___Next_1; }
	inline Scanbeam_t5B92102C6C777A0213DB5CB42DCBE782F0486AA7 ** get_address_of_Next_1() { return &___Next_1; }
	inline void set_Next_1(Scanbeam_t5B92102C6C777A0213DB5CB42DCBE782F0486AA7 * value)
	{
		___Next_1 = value;
		Il2CppCodeGenWriteBarrier((&___Next_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCANBEAM_T5B92102C6C777A0213DB5CB42DCBE782F0486AA7_H
#ifndef GENERALUTILITIES_T7F02D838AFBDB4D3D1DA618DA65567911600B235_H
#define GENERALUTILITIES_T7F02D838AFBDB4D3D1DA618DA65567911600B235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GeneralUtilities
struct  GeneralUtilities_t7F02D838AFBDB4D3D1DA618DA65567911600B235  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERALUTILITIES_T7F02D838AFBDB4D3D1DA618DA65567911600B235_H
#ifndef MATRIX_T4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78_H
#define MATRIX_T4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Matrix
struct  Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78  : public RuntimeObject
{
public:
	// System.Int32 Matrix::rows
	int32_t ___rows_0;
	// System.Int32 Matrix::cols
	int32_t ___cols_1;
	// System.Double[0...,0...] Matrix::mat
	DoubleU5B0___U2C0___U5D_t73D98A29699CA200BF5BF3747873CA1F70FD3478* ___mat_2;
	// Matrix Matrix::L
	Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78 * ___L_3;
	// Matrix Matrix::U
	Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78 * ___U_4;
	// System.Int32[] Matrix::pi
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___pi_5;
	// System.Double Matrix::detOfP
	double ___detOfP_6;

public:
	inline static int32_t get_offset_of_rows_0() { return static_cast<int32_t>(offsetof(Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78, ___rows_0)); }
	inline int32_t get_rows_0() const { return ___rows_0; }
	inline int32_t* get_address_of_rows_0() { return &___rows_0; }
	inline void set_rows_0(int32_t value)
	{
		___rows_0 = value;
	}

	inline static int32_t get_offset_of_cols_1() { return static_cast<int32_t>(offsetof(Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78, ___cols_1)); }
	inline int32_t get_cols_1() const { return ___cols_1; }
	inline int32_t* get_address_of_cols_1() { return &___cols_1; }
	inline void set_cols_1(int32_t value)
	{
		___cols_1 = value;
	}

	inline static int32_t get_offset_of_mat_2() { return static_cast<int32_t>(offsetof(Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78, ___mat_2)); }
	inline DoubleU5B0___U2C0___U5D_t73D98A29699CA200BF5BF3747873CA1F70FD3478* get_mat_2() const { return ___mat_2; }
	inline DoubleU5B0___U2C0___U5D_t73D98A29699CA200BF5BF3747873CA1F70FD3478** get_address_of_mat_2() { return &___mat_2; }
	inline void set_mat_2(DoubleU5B0___U2C0___U5D_t73D98A29699CA200BF5BF3747873CA1F70FD3478* value)
	{
		___mat_2 = value;
		Il2CppCodeGenWriteBarrier((&___mat_2), value);
	}

	inline static int32_t get_offset_of_L_3() { return static_cast<int32_t>(offsetof(Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78, ___L_3)); }
	inline Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78 * get_L_3() const { return ___L_3; }
	inline Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78 ** get_address_of_L_3() { return &___L_3; }
	inline void set_L_3(Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78 * value)
	{
		___L_3 = value;
		Il2CppCodeGenWriteBarrier((&___L_3), value);
	}

	inline static int32_t get_offset_of_U_4() { return static_cast<int32_t>(offsetof(Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78, ___U_4)); }
	inline Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78 * get_U_4() const { return ___U_4; }
	inline Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78 ** get_address_of_U_4() { return &___U_4; }
	inline void set_U_4(Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78 * value)
	{
		___U_4 = value;
		Il2CppCodeGenWriteBarrier((&___U_4), value);
	}

	inline static int32_t get_offset_of_pi_5() { return static_cast<int32_t>(offsetof(Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78, ___pi_5)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_pi_5() const { return ___pi_5; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_pi_5() { return &___pi_5; }
	inline void set_pi_5(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___pi_5 = value;
		Il2CppCodeGenWriteBarrier((&___pi_5), value);
	}

	inline static int32_t get_offset_of_detOfP_6() { return static_cast<int32_t>(offsetof(Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78, ___detOfP_6)); }
	inline double get_detOfP_6() const { return ___detOfP_6; }
	inline double* get_address_of_detOfP_6() { return &___detOfP_6; }
	inline void set_detOfP_6(double value)
	{
		___detOfP_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX_T4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78_H
#ifndef MATRIXUTILS_TB189C49C53E0A4EEA0359B5FCFF168F406FE8F53_H
#define MATRIXUTILS_TB189C49C53E0A4EEA0359B5FCFF168F406FE8F53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MatrixUtils
struct  MatrixUtils_tB189C49C53E0A4EEA0359B5FCFF168F406FE8F53  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIXUTILS_TB189C49C53E0A4EEA0359B5FCFF168F406FE8F53_H
#ifndef ADVANCINGFRONT_T103C9271D0DE07CA7D45EEDBE2ACBD93DC215340_H
#define ADVANCINGFRONT_T103C9271D0DE07CA7D45EEDBE2ACBD93DC215340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.AdvancingFront
struct  AdvancingFront_t103C9271D0DE07CA7D45EEDBE2ACBD93DC215340  : public RuntimeObject
{
public:
	// Poly2Tri.AdvancingFrontNode Poly2Tri.AdvancingFront::Head
	AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * ___Head_0;
	// Poly2Tri.AdvancingFrontNode Poly2Tri.AdvancingFront::Tail
	AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * ___Tail_1;
	// Poly2Tri.AdvancingFrontNode Poly2Tri.AdvancingFront::Search
	AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * ___Search_2;

public:
	inline static int32_t get_offset_of_Head_0() { return static_cast<int32_t>(offsetof(AdvancingFront_t103C9271D0DE07CA7D45EEDBE2ACBD93DC215340, ___Head_0)); }
	inline AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * get_Head_0() const { return ___Head_0; }
	inline AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F ** get_address_of_Head_0() { return &___Head_0; }
	inline void set_Head_0(AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * value)
	{
		___Head_0 = value;
		Il2CppCodeGenWriteBarrier((&___Head_0), value);
	}

	inline static int32_t get_offset_of_Tail_1() { return static_cast<int32_t>(offsetof(AdvancingFront_t103C9271D0DE07CA7D45EEDBE2ACBD93DC215340, ___Tail_1)); }
	inline AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * get_Tail_1() const { return ___Tail_1; }
	inline AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F ** get_address_of_Tail_1() { return &___Tail_1; }
	inline void set_Tail_1(AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * value)
	{
		___Tail_1 = value;
		Il2CppCodeGenWriteBarrier((&___Tail_1), value);
	}

	inline static int32_t get_offset_of_Search_2() { return static_cast<int32_t>(offsetof(AdvancingFront_t103C9271D0DE07CA7D45EEDBE2ACBD93DC215340, ___Search_2)); }
	inline AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * get_Search_2() const { return ___Search_2; }
	inline AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F ** get_address_of_Search_2() { return &___Search_2; }
	inline void set_Search_2(AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * value)
	{
		___Search_2 = value;
		Il2CppCodeGenWriteBarrier((&___Search_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVANCINGFRONT_T103C9271D0DE07CA7D45EEDBE2ACBD93DC215340_H
#ifndef ADVANCINGFRONTNODE_TFE256DFD285C81E1B62206CA862BDA8244B0320F_H
#define ADVANCINGFRONTNODE_TFE256DFD285C81E1B62206CA862BDA8244B0320F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.AdvancingFrontNode
struct  AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F  : public RuntimeObject
{
public:
	// Poly2Tri.AdvancingFrontNode Poly2Tri.AdvancingFrontNode::Next
	AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * ___Next_0;
	// Poly2Tri.AdvancingFrontNode Poly2Tri.AdvancingFrontNode::Prev
	AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * ___Prev_1;
	// System.Double Poly2Tri.AdvancingFrontNode::Value
	double ___Value_2;
	// Poly2Tri.TriangulationPoint Poly2Tri.AdvancingFrontNode::Point
	TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * ___Point_3;
	// Poly2Tri.DelaunayTriangle Poly2Tri.AdvancingFrontNode::Triangle
	DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891 * ___Triangle_4;

public:
	inline static int32_t get_offset_of_Next_0() { return static_cast<int32_t>(offsetof(AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F, ___Next_0)); }
	inline AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * get_Next_0() const { return ___Next_0; }
	inline AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F ** get_address_of_Next_0() { return &___Next_0; }
	inline void set_Next_0(AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * value)
	{
		___Next_0 = value;
		Il2CppCodeGenWriteBarrier((&___Next_0), value);
	}

	inline static int32_t get_offset_of_Prev_1() { return static_cast<int32_t>(offsetof(AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F, ___Prev_1)); }
	inline AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * get_Prev_1() const { return ___Prev_1; }
	inline AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F ** get_address_of_Prev_1() { return &___Prev_1; }
	inline void set_Prev_1(AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * value)
	{
		___Prev_1 = value;
		Il2CppCodeGenWriteBarrier((&___Prev_1), value);
	}

	inline static int32_t get_offset_of_Value_2() { return static_cast<int32_t>(offsetof(AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F, ___Value_2)); }
	inline double get_Value_2() const { return ___Value_2; }
	inline double* get_address_of_Value_2() { return &___Value_2; }
	inline void set_Value_2(double value)
	{
		___Value_2 = value;
	}

	inline static int32_t get_offset_of_Point_3() { return static_cast<int32_t>(offsetof(AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F, ___Point_3)); }
	inline TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * get_Point_3() const { return ___Point_3; }
	inline TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 ** get_address_of_Point_3() { return &___Point_3; }
	inline void set_Point_3(TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * value)
	{
		___Point_3 = value;
		Il2CppCodeGenWriteBarrier((&___Point_3), value);
	}

	inline static int32_t get_offset_of_Triangle_4() { return static_cast<int32_t>(offsetof(AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F, ___Triangle_4)); }
	inline DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891 * get_Triangle_4() const { return ___Triangle_4; }
	inline DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891 ** get_address_of_Triangle_4() { return &___Triangle_4; }
	inline void set_Triangle_4(DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891 * value)
	{
		___Triangle_4 = value;
		Il2CppCodeGenWriteBarrier((&___Triangle_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVANCINGFRONTNODE_TFE256DFD285C81E1B62206CA862BDA8244B0320F_H
#ifndef DTSWEEP_TAAC956F1CCE370C975CF22F70BC6A973A388B5AE_H
#define DTSWEEP_TAAC956F1CCE370C975CF22F70BC6A973A388B5AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.DTSweep
struct  DTSweep_tAAC956F1CCE370C975CF22F70BC6A973A388B5AE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTSWEEP_TAAC956F1CCE370C975CF22F70BC6A973A388B5AE_H
#ifndef DTSWEEPBASIN_T74AB6315A3629FEF0FC85456015FA9537C67B2DD_H
#define DTSWEEPBASIN_T74AB6315A3629FEF0FC85456015FA9537C67B2DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.DTSweepBasin
struct  DTSweepBasin_t74AB6315A3629FEF0FC85456015FA9537C67B2DD  : public RuntimeObject
{
public:
	// Poly2Tri.AdvancingFrontNode Poly2Tri.DTSweepBasin::leftNode
	AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * ___leftNode_0;
	// Poly2Tri.AdvancingFrontNode Poly2Tri.DTSweepBasin::bottomNode
	AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * ___bottomNode_1;
	// Poly2Tri.AdvancingFrontNode Poly2Tri.DTSweepBasin::rightNode
	AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * ___rightNode_2;
	// System.Double Poly2Tri.DTSweepBasin::width
	double ___width_3;
	// System.Boolean Poly2Tri.DTSweepBasin::leftHighest
	bool ___leftHighest_4;

public:
	inline static int32_t get_offset_of_leftNode_0() { return static_cast<int32_t>(offsetof(DTSweepBasin_t74AB6315A3629FEF0FC85456015FA9537C67B2DD, ___leftNode_0)); }
	inline AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * get_leftNode_0() const { return ___leftNode_0; }
	inline AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F ** get_address_of_leftNode_0() { return &___leftNode_0; }
	inline void set_leftNode_0(AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * value)
	{
		___leftNode_0 = value;
		Il2CppCodeGenWriteBarrier((&___leftNode_0), value);
	}

	inline static int32_t get_offset_of_bottomNode_1() { return static_cast<int32_t>(offsetof(DTSweepBasin_t74AB6315A3629FEF0FC85456015FA9537C67B2DD, ___bottomNode_1)); }
	inline AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * get_bottomNode_1() const { return ___bottomNode_1; }
	inline AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F ** get_address_of_bottomNode_1() { return &___bottomNode_1; }
	inline void set_bottomNode_1(AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * value)
	{
		___bottomNode_1 = value;
		Il2CppCodeGenWriteBarrier((&___bottomNode_1), value);
	}

	inline static int32_t get_offset_of_rightNode_2() { return static_cast<int32_t>(offsetof(DTSweepBasin_t74AB6315A3629FEF0FC85456015FA9537C67B2DD, ___rightNode_2)); }
	inline AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * get_rightNode_2() const { return ___rightNode_2; }
	inline AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F ** get_address_of_rightNode_2() { return &___rightNode_2; }
	inline void set_rightNode_2(AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * value)
	{
		___rightNode_2 = value;
		Il2CppCodeGenWriteBarrier((&___rightNode_2), value);
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(DTSweepBasin_t74AB6315A3629FEF0FC85456015FA9537C67B2DD, ___width_3)); }
	inline double get_width_3() const { return ___width_3; }
	inline double* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(double value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_leftHighest_4() { return static_cast<int32_t>(offsetof(DTSweepBasin_t74AB6315A3629FEF0FC85456015FA9537C67B2DD, ___leftHighest_4)); }
	inline bool get_leftHighest_4() const { return ___leftHighest_4; }
	inline bool* get_address_of_leftHighest_4() { return &___leftHighest_4; }
	inline void set_leftHighest_4(bool value)
	{
		___leftHighest_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTSWEEPBASIN_T74AB6315A3629FEF0FC85456015FA9537C67B2DD_H
#ifndef DTSWEEPEDGEEVENT_TBD7A886997C86280A3AE982B4E8C86325A28E422_H
#define DTSWEEPEDGEEVENT_TBD7A886997C86280A3AE982B4E8C86325A28E422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.DTSweepEdgeEvent
struct  DTSweepEdgeEvent_tBD7A886997C86280A3AE982B4E8C86325A28E422  : public RuntimeObject
{
public:
	// Poly2Tri.DTSweepConstraint Poly2Tri.DTSweepEdgeEvent::ConstrainedEdge
	DTSweepConstraint_t8C5C973CF68522DA5D7828F45161FE7A22203819 * ___ConstrainedEdge_0;
	// System.Boolean Poly2Tri.DTSweepEdgeEvent::Right
	bool ___Right_1;

public:
	inline static int32_t get_offset_of_ConstrainedEdge_0() { return static_cast<int32_t>(offsetof(DTSweepEdgeEvent_tBD7A886997C86280A3AE982B4E8C86325A28E422, ___ConstrainedEdge_0)); }
	inline DTSweepConstraint_t8C5C973CF68522DA5D7828F45161FE7A22203819 * get_ConstrainedEdge_0() const { return ___ConstrainedEdge_0; }
	inline DTSweepConstraint_t8C5C973CF68522DA5D7828F45161FE7A22203819 ** get_address_of_ConstrainedEdge_0() { return &___ConstrainedEdge_0; }
	inline void set_ConstrainedEdge_0(DTSweepConstraint_t8C5C973CF68522DA5D7828F45161FE7A22203819 * value)
	{
		___ConstrainedEdge_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConstrainedEdge_0), value);
	}

	inline static int32_t get_offset_of_Right_1() { return static_cast<int32_t>(offsetof(DTSweepEdgeEvent_tBD7A886997C86280A3AE982B4E8C86325A28E422, ___Right_1)); }
	inline bool get_Right_1() const { return ___Right_1; }
	inline bool* get_address_of_Right_1() { return &___Right_1; }
	inline void set_Right_1(bool value)
	{
		___Right_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTSWEEPEDGEEVENT_TBD7A886997C86280A3AE982B4E8C86325A28E422_H
#ifndef DTSWEEPPOINTCOMPARATOR_TFA3FA00C634B6162D20BD76BE5E4C0CDB943ACAC_H
#define DTSWEEPPOINTCOMPARATOR_TFA3FA00C634B6162D20BD76BE5E4C0CDB943ACAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.DTSweepPointComparator
struct  DTSweepPointComparator_tFA3FA00C634B6162D20BD76BE5E4C0CDB943ACAC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTSWEEPPOINTCOMPARATOR_TFA3FA00C634B6162D20BD76BE5E4C0CDB943ACAC_H
#ifndef EDGE_T5170DF73FC08083B2752D6AD8F039BF371D0B79B_H
#define EDGE_T5170DF73FC08083B2752D6AD8F039BF371D0B79B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.Edge
struct  Edge_t5170DF73FC08083B2752D6AD8F039BF371D0B79B  : public RuntimeObject
{
public:
	// Poly2Tri.Point2D Poly2Tri.Edge::mP
	Point2D_t35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF * ___mP_0;
	// Poly2Tri.Point2D Poly2Tri.Edge::mQ
	Point2D_t35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF * ___mQ_1;

public:
	inline static int32_t get_offset_of_mP_0() { return static_cast<int32_t>(offsetof(Edge_t5170DF73FC08083B2752D6AD8F039BF371D0B79B, ___mP_0)); }
	inline Point2D_t35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF * get_mP_0() const { return ___mP_0; }
	inline Point2D_t35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF ** get_address_of_mP_0() { return &___mP_0; }
	inline void set_mP_0(Point2D_t35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF * value)
	{
		___mP_0 = value;
		Il2CppCodeGenWriteBarrier((&___mP_0), value);
	}

	inline static int32_t get_offset_of_mQ_1() { return static_cast<int32_t>(offsetof(Edge_t5170DF73FC08083B2752D6AD8F039BF371D0B79B, ___mQ_1)); }
	inline Point2D_t35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF * get_mQ_1() const { return ___mQ_1; }
	inline Point2D_t35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF ** get_address_of_mQ_1() { return &___mQ_1; }
	inline void set_mQ_1(Point2D_t35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF * value)
	{
		___mQ_1 = value;
		Il2CppCodeGenWriteBarrier((&___mQ_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDGE_T5170DF73FC08083B2752D6AD8F039BF371D0B79B_H
#ifndef EDGEINTERSECTINFO_T1E5A929A2A5E7DD8E210A1D3244811BCC136D3E1_H
#define EDGEINTERSECTINFO_T1E5A929A2A5E7DD8E210A1D3244811BCC136D3E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.EdgeIntersectInfo
struct  EdgeIntersectInfo_t1E5A929A2A5E7DD8E210A1D3244811BCC136D3E1  : public RuntimeObject
{
public:
	// Poly2Tri.Edge Poly2Tri.EdgeIntersectInfo::<EdgeOne>k__BackingField
	Edge_t5170DF73FC08083B2752D6AD8F039BF371D0B79B * ___U3CEdgeOneU3Ek__BackingField_0;
	// Poly2Tri.Edge Poly2Tri.EdgeIntersectInfo::<EdgeTwo>k__BackingField
	Edge_t5170DF73FC08083B2752D6AD8F039BF371D0B79B * ___U3CEdgeTwoU3Ek__BackingField_1;
	// Poly2Tri.Point2D Poly2Tri.EdgeIntersectInfo::<IntersectionPoint>k__BackingField
	Point2D_t35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF * ___U3CIntersectionPointU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CEdgeOneU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EdgeIntersectInfo_t1E5A929A2A5E7DD8E210A1D3244811BCC136D3E1, ___U3CEdgeOneU3Ek__BackingField_0)); }
	inline Edge_t5170DF73FC08083B2752D6AD8F039BF371D0B79B * get_U3CEdgeOneU3Ek__BackingField_0() const { return ___U3CEdgeOneU3Ek__BackingField_0; }
	inline Edge_t5170DF73FC08083B2752D6AD8F039BF371D0B79B ** get_address_of_U3CEdgeOneU3Ek__BackingField_0() { return &___U3CEdgeOneU3Ek__BackingField_0; }
	inline void set_U3CEdgeOneU3Ek__BackingField_0(Edge_t5170DF73FC08083B2752D6AD8F039BF371D0B79B * value)
	{
		___U3CEdgeOneU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEdgeOneU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CEdgeTwoU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(EdgeIntersectInfo_t1E5A929A2A5E7DD8E210A1D3244811BCC136D3E1, ___U3CEdgeTwoU3Ek__BackingField_1)); }
	inline Edge_t5170DF73FC08083B2752D6AD8F039BF371D0B79B * get_U3CEdgeTwoU3Ek__BackingField_1() const { return ___U3CEdgeTwoU3Ek__BackingField_1; }
	inline Edge_t5170DF73FC08083B2752D6AD8F039BF371D0B79B ** get_address_of_U3CEdgeTwoU3Ek__BackingField_1() { return &___U3CEdgeTwoU3Ek__BackingField_1; }
	inline void set_U3CEdgeTwoU3Ek__BackingField_1(Edge_t5170DF73FC08083B2752D6AD8F039BF371D0B79B * value)
	{
		___U3CEdgeTwoU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEdgeTwoU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CIntersectionPointU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(EdgeIntersectInfo_t1E5A929A2A5E7DD8E210A1D3244811BCC136D3E1, ___U3CIntersectionPointU3Ek__BackingField_2)); }
	inline Point2D_t35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF * get_U3CIntersectionPointU3Ek__BackingField_2() const { return ___U3CIntersectionPointU3Ek__BackingField_2; }
	inline Point2D_t35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF ** get_address_of_U3CIntersectionPointU3Ek__BackingField_2() { return &___U3CIntersectionPointU3Ek__BackingField_2; }
	inline void set_U3CIntersectionPointU3Ek__BackingField_2(Point2D_t35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF * value)
	{
		___U3CIntersectionPointU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIntersectionPointU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDGEINTERSECTINFO_T1E5A929A2A5E7DD8E210A1D3244811BCC136D3E1_H
#ifndef MATHUTIL_T392B236AFF869A2A4CA195686E23ECC003EEA716_H
#define MATHUTIL_T392B236AFF869A2A4CA195686E23ECC003EEA716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.MathUtil
struct  MathUtil_t392B236AFF869A2A4CA195686E23ECC003EEA716  : public RuntimeObject
{
public:

public:
};

struct MathUtil_t392B236AFF869A2A4CA195686E23ECC003EEA716_StaticFields
{
public:
	// System.Double Poly2Tri.MathUtil::EPSILON
	double ___EPSILON_0;

public:
	inline static int32_t get_offset_of_EPSILON_0() { return static_cast<int32_t>(offsetof(MathUtil_t392B236AFF869A2A4CA195686E23ECC003EEA716_StaticFields, ___EPSILON_0)); }
	inline double get_EPSILON_0() const { return ___EPSILON_0; }
	inline double* get_address_of_EPSILON_0() { return &___EPSILON_0; }
	inline void set_EPSILON_0(double value)
	{
		___EPSILON_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHUTIL_T392B236AFF869A2A4CA195686E23ECC003EEA716_H
#ifndef POINT2D_T35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF_H
#define POINT2D_T35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.Point2D
struct  Point2D_t35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF  : public RuntimeObject
{
public:
	// System.Double Poly2Tri.Point2D::mX
	double ___mX_0;
	// System.Double Poly2Tri.Point2D::mY
	double ___mY_1;

public:
	inline static int32_t get_offset_of_mX_0() { return static_cast<int32_t>(offsetof(Point2D_t35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF, ___mX_0)); }
	inline double get_mX_0() const { return ___mX_0; }
	inline double* get_address_of_mX_0() { return &___mX_0; }
	inline void set_mX_0(double value)
	{
		___mX_0 = value;
	}

	inline static int32_t get_offset_of_mY_1() { return static_cast<int32_t>(offsetof(Point2D_t35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF, ___mY_1)); }
	inline double get_mY_1() const { return ___mY_1; }
	inline double* get_address_of_mY_1() { return &___mY_1; }
	inline void set_mY_1(double value)
	{
		___mY_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINT2D_T35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF_H
#ifndef POINT2DENUMERATOR_TBD1ADF39FDB96DFB355BF956B6E37A79D89A516A_H
#define POINT2DENUMERATOR_TBD1ADF39FDB96DFB355BF956B6E37A79D89A516A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.Point2DEnumerator
struct  Point2DEnumerator_tBD1ADF39FDB96DFB355BF956B6E37A79D89A516A  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<Poly2Tri.Point2D> Poly2Tri.Point2DEnumerator::mPoints
	RuntimeObject* ___mPoints_0;
	// System.Int32 Poly2Tri.Point2DEnumerator::position
	int32_t ___position_1;

public:
	inline static int32_t get_offset_of_mPoints_0() { return static_cast<int32_t>(offsetof(Point2DEnumerator_tBD1ADF39FDB96DFB355BF956B6E37A79D89A516A, ___mPoints_0)); }
	inline RuntimeObject* get_mPoints_0() const { return ___mPoints_0; }
	inline RuntimeObject** get_address_of_mPoints_0() { return &___mPoints_0; }
	inline void set_mPoints_0(RuntimeObject* value)
	{
		___mPoints_0 = value;
		Il2CppCodeGenWriteBarrier((&___mPoints_0), value);
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(Point2DEnumerator_tBD1ADF39FDB96DFB355BF956B6E37A79D89A516A, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINT2DENUMERATOR_TBD1ADF39FDB96DFB355BF956B6E37A79D89A516A_H
#ifndef POINTGENERATOR_T57149E147F6F1DBB274762EA8C64BA73EF735A2B_H
#define POINTGENERATOR_T57149E147F6F1DBB274762EA8C64BA73EF735A2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.PointGenerator
struct  PointGenerator_t57149E147F6F1DBB274762EA8C64BA73EF735A2B  : public RuntimeObject
{
public:

public:
};

struct PointGenerator_t57149E147F6F1DBB274762EA8C64BA73EF735A2B_StaticFields
{
public:
	// System.Random Poly2Tri.PointGenerator::RNG
	Random_tE77EE3BF254F50AC5B022C695F4F1DD99B425AE6 * ___RNG_0;

public:
	inline static int32_t get_offset_of_RNG_0() { return static_cast<int32_t>(offsetof(PointGenerator_t57149E147F6F1DBB274762EA8C64BA73EF735A2B_StaticFields, ___RNG_0)); }
	inline Random_tE77EE3BF254F50AC5B022C695F4F1DD99B425AE6 * get_RNG_0() const { return ___RNG_0; }
	inline Random_tE77EE3BF254F50AC5B022C695F4F1DD99B425AE6 ** get_address_of_RNG_0() { return &___RNG_0; }
	inline void set_RNG_0(Random_tE77EE3BF254F50AC5B022C695F4F1DD99B425AE6 * value)
	{
		___RNG_0 = value;
		Il2CppCodeGenWriteBarrier((&___RNG_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTGENERATOR_T57149E147F6F1DBB274762EA8C64BA73EF735A2B_H
#ifndef POLYGONGENERATOR_TFE93FBCD78073D16685E5685256C52EF35089A0F_H
#define POLYGONGENERATOR_TFE93FBCD78073D16685E5685256C52EF35089A0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.PolygonGenerator
struct  PolygonGenerator_tFE93FBCD78073D16685E5685256C52EF35089A0F  : public RuntimeObject
{
public:

public:
};

struct PolygonGenerator_tFE93FBCD78073D16685E5685256C52EF35089A0F_StaticFields
{
public:
	// System.Random Poly2Tri.PolygonGenerator::RNG
	Random_tE77EE3BF254F50AC5B022C695F4F1DD99B425AE6 * ___RNG_0;
	// System.Double Poly2Tri.PolygonGenerator::PI_2
	double ___PI_2_1;

public:
	inline static int32_t get_offset_of_RNG_0() { return static_cast<int32_t>(offsetof(PolygonGenerator_tFE93FBCD78073D16685E5685256C52EF35089A0F_StaticFields, ___RNG_0)); }
	inline Random_tE77EE3BF254F50AC5B022C695F4F1DD99B425AE6 * get_RNG_0() const { return ___RNG_0; }
	inline Random_tE77EE3BF254F50AC5B022C695F4F1DD99B425AE6 ** get_address_of_RNG_0() { return &___RNG_0; }
	inline void set_RNG_0(Random_tE77EE3BF254F50AC5B022C695F4F1DD99B425AE6 * value)
	{
		___RNG_0 = value;
		Il2CppCodeGenWriteBarrier((&___RNG_0), value);
	}

	inline static int32_t get_offset_of_PI_2_1() { return static_cast<int32_t>(offsetof(PolygonGenerator_tFE93FBCD78073D16685E5685256C52EF35089A0F_StaticFields, ___PI_2_1)); }
	inline double get_PI_2_1() const { return ___PI_2_1; }
	inline double* get_address_of_PI_2_1() { return &___PI_2_1; }
	inline void set_PI_2_1(double value)
	{
		___PI_2_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGONGENERATOR_TFE93FBCD78073D16685E5685256C52EF35089A0F_H
#ifndef POLYGONSET_T66D579451ECDA6DEB2755D1383A06FE8914FD6C3_H
#define POLYGONSET_T66D579451ECDA6DEB2755D1383A06FE8914FD6C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.PolygonSet
struct  PolygonSet_t66D579451ECDA6DEB2755D1383A06FE8914FD6C3  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Poly2Tri.Polygon> Poly2Tri.PolygonSet::_polygons
	List_1_tDE4178F994B399BD69F0374B4BF2077E0A5D8AAB * ____polygons_0;

public:
	inline static int32_t get_offset_of__polygons_0() { return static_cast<int32_t>(offsetof(PolygonSet_t66D579451ECDA6DEB2755D1383A06FE8914FD6C3, ____polygons_0)); }
	inline List_1_tDE4178F994B399BD69F0374B4BF2077E0A5D8AAB * get__polygons_0() const { return ____polygons_0; }
	inline List_1_tDE4178F994B399BD69F0374B4BF2077E0A5D8AAB ** get_address_of__polygons_0() { return &____polygons_0; }
	inline void set__polygons_0(List_1_tDE4178F994B399BD69F0374B4BF2077E0A5D8AAB * value)
	{
		____polygons_0 = value;
		Il2CppCodeGenWriteBarrier((&____polygons_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGONSET_T66D579451ECDA6DEB2755D1383A06FE8914FD6C3_H
#ifndef POLYGONUTIL_T854384351A9165F158DDB413A69F665AD048FF00_H
#define POLYGONUTIL_T854384351A9165F158DDB413A69F665AD048FF00_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.PolygonUtil
struct  PolygonUtil_t854384351A9165F158DDB413A69F665AD048FF00  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGONUTIL_T854384351A9165F158DDB413A69F665AD048FF00_H
#ifndef RECT2D_T72E37904F5FB0718F0E3D96A0EBC0A77E80F6001_H
#define RECT2D_T72E37904F5FB0718F0E3D96A0EBC0A77E80F6001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.Rect2D
struct  Rect2D_t72E37904F5FB0718F0E3D96A0EBC0A77E80F6001  : public RuntimeObject
{
public:
	// System.Double Poly2Tri.Rect2D::mMinX
	double ___mMinX_0;
	// System.Double Poly2Tri.Rect2D::mMaxX
	double ___mMaxX_1;
	// System.Double Poly2Tri.Rect2D::mMinY
	double ___mMinY_2;
	// System.Double Poly2Tri.Rect2D::mMaxY
	double ___mMaxY_3;

public:
	inline static int32_t get_offset_of_mMinX_0() { return static_cast<int32_t>(offsetof(Rect2D_t72E37904F5FB0718F0E3D96A0EBC0A77E80F6001, ___mMinX_0)); }
	inline double get_mMinX_0() const { return ___mMinX_0; }
	inline double* get_address_of_mMinX_0() { return &___mMinX_0; }
	inline void set_mMinX_0(double value)
	{
		___mMinX_0 = value;
	}

	inline static int32_t get_offset_of_mMaxX_1() { return static_cast<int32_t>(offsetof(Rect2D_t72E37904F5FB0718F0E3D96A0EBC0A77E80F6001, ___mMaxX_1)); }
	inline double get_mMaxX_1() const { return ___mMaxX_1; }
	inline double* get_address_of_mMaxX_1() { return &___mMaxX_1; }
	inline void set_mMaxX_1(double value)
	{
		___mMaxX_1 = value;
	}

	inline static int32_t get_offset_of_mMinY_2() { return static_cast<int32_t>(offsetof(Rect2D_t72E37904F5FB0718F0E3D96A0EBC0A77E80F6001, ___mMinY_2)); }
	inline double get_mMinY_2() const { return ___mMinY_2; }
	inline double* get_address_of_mMinY_2() { return &___mMinY_2; }
	inline void set_mMinY_2(double value)
	{
		___mMinY_2 = value;
	}

	inline static int32_t get_offset_of_mMaxY_3() { return static_cast<int32_t>(offsetof(Rect2D_t72E37904F5FB0718F0E3D96A0EBC0A77E80F6001, ___mMaxY_3)); }
	inline double get_mMaxY_3() const { return ___mMaxY_3; }
	inline double* get_address_of_mMaxY_3() { return &___mMaxY_3; }
	inline void set_mMaxY_3(double value)
	{
		___mMaxY_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT2D_T72E37904F5FB0718F0E3D96A0EBC0A77E80F6001_H
#ifndef SPLITCOMPLEXPOLYGONNODE_T070AE02F70843A40F704E20203026E42EF3DC7DB_H
#define SPLITCOMPLEXPOLYGONNODE_T070AE02F70843A40F704E20203026E42EF3DC7DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.SplitComplexPolygonNode
struct  SplitComplexPolygonNode_t070AE02F70843A40F704E20203026E42EF3DC7DB  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Poly2Tri.SplitComplexPolygonNode> Poly2Tri.SplitComplexPolygonNode::mConnected
	List_1_tB7E4690795C8CEB9058BB9A80E863D61B62B269C * ___mConnected_0;
	// Poly2Tri.Point2D Poly2Tri.SplitComplexPolygonNode::mPosition
	Point2D_t35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF * ___mPosition_1;

public:
	inline static int32_t get_offset_of_mConnected_0() { return static_cast<int32_t>(offsetof(SplitComplexPolygonNode_t070AE02F70843A40F704E20203026E42EF3DC7DB, ___mConnected_0)); }
	inline List_1_tB7E4690795C8CEB9058BB9A80E863D61B62B269C * get_mConnected_0() const { return ___mConnected_0; }
	inline List_1_tB7E4690795C8CEB9058BB9A80E863D61B62B269C ** get_address_of_mConnected_0() { return &___mConnected_0; }
	inline void set_mConnected_0(List_1_tB7E4690795C8CEB9058BB9A80E863D61B62B269C * value)
	{
		___mConnected_0 = value;
		Il2CppCodeGenWriteBarrier((&___mConnected_0), value);
	}

	inline static int32_t get_offset_of_mPosition_1() { return static_cast<int32_t>(offsetof(SplitComplexPolygonNode_t070AE02F70843A40F704E20203026E42EF3DC7DB, ___mPosition_1)); }
	inline Point2D_t35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF * get_mPosition_1() const { return ___mPosition_1; }
	inline Point2D_t35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF ** get_address_of_mPosition_1() { return &___mPosition_1; }
	inline void set_mPosition_1(Point2D_t35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF * value)
	{
		___mPosition_1 = value;
		Il2CppCodeGenWriteBarrier((&___mPosition_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLITCOMPLEXPOLYGONNODE_T070AE02F70843A40F704E20203026E42EF3DC7DB_H
#ifndef TRIANGULATIONDEBUGCONTEXT_TF9458C2C5E3E324954C544E9FB4044AC0B1F103C_H
#define TRIANGULATIONDEBUGCONTEXT_TF9458C2C5E3E324954C544E9FB4044AC0B1F103C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.TriangulationDebugContext
struct  TriangulationDebugContext_tF9458C2C5E3E324954C544E9FB4044AC0B1F103C  : public RuntimeObject
{
public:
	// Poly2Tri.TriangulationContext Poly2Tri.TriangulationDebugContext::_tcx
	TriangulationContext_t0153B91CA7F38202C7CC645BC07A67774802360E * ____tcx_0;

public:
	inline static int32_t get_offset_of__tcx_0() { return static_cast<int32_t>(offsetof(TriangulationDebugContext_tF9458C2C5E3E324954C544E9FB4044AC0B1F103C, ____tcx_0)); }
	inline TriangulationContext_t0153B91CA7F38202C7CC645BC07A67774802360E * get__tcx_0() const { return ____tcx_0; }
	inline TriangulationContext_t0153B91CA7F38202C7CC645BC07A67774802360E ** get_address_of__tcx_0() { return &____tcx_0; }
	inline void set__tcx_0(TriangulationContext_t0153B91CA7F38202C7CC645BC07A67774802360E * value)
	{
		____tcx_0 = value;
		Il2CppCodeGenWriteBarrier((&____tcx_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATIONDEBUGCONTEXT_TF9458C2C5E3E324954C544E9FB4044AC0B1F103C_H
#ifndef TRIANGULATIONPOINTENUMERATOR_TC5A4E04C5682F9A1BD78808A42042C9969A10609_H
#define TRIANGULATIONPOINTENUMERATOR_TC5A4E04C5682F9A1BD78808A42042C9969A10609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.TriangulationPointEnumerator
struct  TriangulationPointEnumerator_tC5A4E04C5682F9A1BD78808A42042C9969A10609  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<Poly2Tri.Point2D> Poly2Tri.TriangulationPointEnumerator::mPoints
	RuntimeObject* ___mPoints_0;
	// System.Int32 Poly2Tri.TriangulationPointEnumerator::position
	int32_t ___position_1;

public:
	inline static int32_t get_offset_of_mPoints_0() { return static_cast<int32_t>(offsetof(TriangulationPointEnumerator_tC5A4E04C5682F9A1BD78808A42042C9969A10609, ___mPoints_0)); }
	inline RuntimeObject* get_mPoints_0() const { return ___mPoints_0; }
	inline RuntimeObject** get_address_of_mPoints_0() { return &___mPoints_0; }
	inline void set_mPoints_0(RuntimeObject* value)
	{
		___mPoints_0 = value;
		Il2CppCodeGenWriteBarrier((&___mPoints_0), value);
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(TriangulationPointEnumerator_tC5A4E04C5682F9A1BD78808A42042C9969A10609, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATIONPOINTENUMERATOR_TC5A4E04C5682F9A1BD78808A42042C9969A10609_H
#ifndef TRIANGULATIONUTIL_T078C0A44D64E4A422214F18F979B5215B4ED525F_H
#define TRIANGULATIONUTIL_T078C0A44D64E4A422214F18F979B5215B4ED525F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.TriangulationUtil
struct  TriangulationUtil_t078C0A44D64E4A422214F18F979B5215B4ED525F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATIONUTIL_T078C0A44D64E4A422214F18F979B5215B4ED525F_H
#ifndef DOCUMENTPARSER_T80E81C53F90D9CD57A8868BD9237537C9822E78E_H
#define DOCUMENTPARSER_T80E81C53F90D9CD57A8868BD9237537C9822E78E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.DocumentParser
struct  DocumentParser_t80E81C53F90D9CD57A8868BD9237537C9822E78E  : public RuntimeObject
{
public:
	// SimplySVG.SVGDocument SimplySVG.DocumentParser::currentDocument
	SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7 * ___currentDocument_0;
	// System.Collections.Generic.Stack`1<SimplySVG.SVGElement> SimplySVG.DocumentParser::buildStack
	Stack_1_t9C7F23AAE760B11BBC2B4E0504A4CBBC785159CC * ___buildStack_1;

public:
	inline static int32_t get_offset_of_currentDocument_0() { return static_cast<int32_t>(offsetof(DocumentParser_t80E81C53F90D9CD57A8868BD9237537C9822E78E, ___currentDocument_0)); }
	inline SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7 * get_currentDocument_0() const { return ___currentDocument_0; }
	inline SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7 ** get_address_of_currentDocument_0() { return &___currentDocument_0; }
	inline void set_currentDocument_0(SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7 * value)
	{
		___currentDocument_0 = value;
		Il2CppCodeGenWriteBarrier((&___currentDocument_0), value);
	}

	inline static int32_t get_offset_of_buildStack_1() { return static_cast<int32_t>(offsetof(DocumentParser_t80E81C53F90D9CD57A8868BD9237537C9822E78E, ___buildStack_1)); }
	inline Stack_1_t9C7F23AAE760B11BBC2B4E0504A4CBBC785159CC * get_buildStack_1() const { return ___buildStack_1; }
	inline Stack_1_t9C7F23AAE760B11BBC2B4E0504A4CBBC785159CC ** get_address_of_buildStack_1() { return &___buildStack_1; }
	inline void set_buildStack_1(Stack_1_t9C7F23AAE760B11BBC2B4E0504A4CBBC785159CC * value)
	{
		___buildStack_1 = value;
		Il2CppCodeGenWriteBarrier((&___buildStack_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOCUMENTPARSER_T80E81C53F90D9CD57A8868BD9237537C9822E78E_H
#ifndef IMPORTUTILITIES_T2410B5A4A6B89F70236A9E0910E77646D16B1249_H
#define IMPORTUTILITIES_T2410B5A4A6B89F70236A9E0910E77646D16B1249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.ImportUtilities
struct  ImportUtilities_t2410B5A4A6B89F70236A9E0910E77646D16B1249  : public RuntimeObject
{
public:

public:
};

struct ImportUtilities_t2410B5A4A6B89F70236A9E0910E77646D16B1249_StaticFields
{
public:
	// System.Char[] SimplySVG.ImportUtilities::wps
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___wps_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> SimplySVG.ImportUtilities::<>f__switch$map0
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map0_1;

public:
	inline static int32_t get_offset_of_wps_0() { return static_cast<int32_t>(offsetof(ImportUtilities_t2410B5A4A6B89F70236A9E0910E77646D16B1249_StaticFields, ___wps_0)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_wps_0() const { return ___wps_0; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_wps_0() { return &___wps_0; }
	inline void set_wps_0(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___wps_0 = value;
		Il2CppCodeGenWriteBarrier((&___wps_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_1() { return static_cast<int32_t>(offsetof(ImportUtilities_t2410B5A4A6B89F70236A9E0910E77646D16B1249_StaticFields, ___U3CU3Ef__switchU24map0_1)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map0_1() const { return ___U3CU3Ef__switchU24map0_1; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map0_1() { return &___U3CU3Ef__switchU24map0_1; }
	inline void set_U3CU3Ef__switchU24map0_1(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMPORTUTILITIES_T2410B5A4A6B89F70236A9E0910E77646D16B1249_H
#ifndef SVGELEMENT_T83C50C9210FA55853195E3B39B9E5AC78E9A9888_H
#define SVGELEMENT_T83C50C9210FA55853195E3B39B9E5AC78E9A9888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.SVGElement
struct  SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888  : public RuntimeObject
{
public:
	// System.String SimplySVG.SVGElement::id
	String_t* ___id_0;
	// SimplySVG.SVGDocument SimplySVG.SVGElement::ownerDocument
	SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7 * ___ownerDocument_1;
	// SimplySVG.SVGElement SimplySVG.SVGElement::parent
	SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888 * ___parent_2;
	// System.Collections.Generic.List`1<SimplySVG.SVGElement> SimplySVG.SVGElement::children
	List_1_tDACA481967F32AB5A83EA055EF5C33BB4E3148ED * ___children_3;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_ownerDocument_1() { return static_cast<int32_t>(offsetof(SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888, ___ownerDocument_1)); }
	inline SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7 * get_ownerDocument_1() const { return ___ownerDocument_1; }
	inline SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7 ** get_address_of_ownerDocument_1() { return &___ownerDocument_1; }
	inline void set_ownerDocument_1(SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7 * value)
	{
		___ownerDocument_1 = value;
		Il2CppCodeGenWriteBarrier((&___ownerDocument_1), value);
	}

	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888, ___parent_2)); }
	inline SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888 * get_parent_2() const { return ___parent_2; }
	inline SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((&___parent_2), value);
	}

	inline static int32_t get_offset_of_children_3() { return static_cast<int32_t>(offsetof(SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888, ___children_3)); }
	inline List_1_tDACA481967F32AB5A83EA055EF5C33BB4E3148ED * get_children_3() const { return ___children_3; }
	inline List_1_tDACA481967F32AB5A83EA055EF5C33BB4E3148ED ** get_address_of_children_3() { return &___children_3; }
	inline void set_children_3(List_1_tDACA481967F32AB5A83EA055EF5C33BB4E3148ED * value)
	{
		___children_3 = value;
		Il2CppCodeGenWriteBarrier((&___children_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVGELEMENT_T83C50C9210FA55853195E3B39B9E5AC78E9A9888_H
#ifndef SIMPLYSVGIMPORTER_TEC08895B75B240823232FABEEC86D3F31F13D9FC_H
#define SIMPLYSVGIMPORTER_TEC08895B75B240823232FABEEC86D3F31F13D9FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.SimplySVGImporter
struct  SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC  : public RuntimeObject
{
public:
	// System.String SimplySVG.SimplySVGImporter::name
	String_t* ___name_0;
	// ImportSettings SimplySVG.SimplySVGImporter::importSettings
	ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3 * ___importSettings_1;
	// UnityEngine.Object SimplySVG.SimplySVGImporter::svgFile
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___svgFile_2;
	// SimplySVG.SVGDocument SimplySVG.SimplySVGImporter::document
	SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7 * ___document_3;
	// System.Collections.Generic.List`1<SimplySVG.SVGDocument> SimplySVG.SimplySVGImporter::svgDocumentLayers
	List_1_t8DB9ECA2C04E8B430E3A3CFEC58D2048982C9C8B * ___svgDocumentLayers_4;
	// System.String SimplySVG.SimplySVGImporter::svgData
	String_t* ___svgData_5;
	// UnityEngine.Mesh SimplySVG.SimplySVGImporter::mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh_6;
	// System.Collections.Generic.List`1<UnityEngine.Mesh> SimplySVG.SimplySVGImporter::meshLayers
	List_1_tBE402A53CD5331E0CB483C591CFDC8C6827F35B8 * ___meshLayers_7;
	// System.Collections.Generic.List`1<System.String> SimplySVG.SimplySVGImporter::names
	List_1_t7600B3EA144F32C2120D420AB63C9E014D113CF1 * ___names_8;
	// SimplySVG.CollisionShapeData SimplySVG.SimplySVGImporter::collisionShapeData
	CollisionShapeData_t09D2AF9F5D9E3B0C513049F97C117CF4EB9B2C6B * ___collisionShapeData_9;
	// System.Collections.Generic.List`1<SimplySVG.CollisionShapeData> SimplySVG.SimplySVGImporter::collisionShapeDataLayers
	List_1_tCBD9667398A994BB4DC992902858F6B45B0C5296 * ___collisionShapeDataLayers_10;
	// System.String SimplySVG.SimplySVGImporter::errors
	String_t* ___errors_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Int32>> SimplySVG.SimplySVGImporter::unsupportedElements
	Dictionary_2_t4807FD27415C3FDCCC28B95BCB1284A5DE60B015 * ___unsupportedElements_12;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_importSettings_1() { return static_cast<int32_t>(offsetof(SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC, ___importSettings_1)); }
	inline ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3 * get_importSettings_1() const { return ___importSettings_1; }
	inline ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3 ** get_address_of_importSettings_1() { return &___importSettings_1; }
	inline void set_importSettings_1(ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3 * value)
	{
		___importSettings_1 = value;
		Il2CppCodeGenWriteBarrier((&___importSettings_1), value);
	}

	inline static int32_t get_offset_of_svgFile_2() { return static_cast<int32_t>(offsetof(SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC, ___svgFile_2)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get_svgFile_2() const { return ___svgFile_2; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of_svgFile_2() { return &___svgFile_2; }
	inline void set_svgFile_2(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		___svgFile_2 = value;
		Il2CppCodeGenWriteBarrier((&___svgFile_2), value);
	}

	inline static int32_t get_offset_of_document_3() { return static_cast<int32_t>(offsetof(SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC, ___document_3)); }
	inline SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7 * get_document_3() const { return ___document_3; }
	inline SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7 ** get_address_of_document_3() { return &___document_3; }
	inline void set_document_3(SVGDocument_tC755869E81E64D4CBB5A30C21524F4176A3850C7 * value)
	{
		___document_3 = value;
		Il2CppCodeGenWriteBarrier((&___document_3), value);
	}

	inline static int32_t get_offset_of_svgDocumentLayers_4() { return static_cast<int32_t>(offsetof(SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC, ___svgDocumentLayers_4)); }
	inline List_1_t8DB9ECA2C04E8B430E3A3CFEC58D2048982C9C8B * get_svgDocumentLayers_4() const { return ___svgDocumentLayers_4; }
	inline List_1_t8DB9ECA2C04E8B430E3A3CFEC58D2048982C9C8B ** get_address_of_svgDocumentLayers_4() { return &___svgDocumentLayers_4; }
	inline void set_svgDocumentLayers_4(List_1_t8DB9ECA2C04E8B430E3A3CFEC58D2048982C9C8B * value)
	{
		___svgDocumentLayers_4 = value;
		Il2CppCodeGenWriteBarrier((&___svgDocumentLayers_4), value);
	}

	inline static int32_t get_offset_of_svgData_5() { return static_cast<int32_t>(offsetof(SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC, ___svgData_5)); }
	inline String_t* get_svgData_5() const { return ___svgData_5; }
	inline String_t** get_address_of_svgData_5() { return &___svgData_5; }
	inline void set_svgData_5(String_t* value)
	{
		___svgData_5 = value;
		Il2CppCodeGenWriteBarrier((&___svgData_5), value);
	}

	inline static int32_t get_offset_of_mesh_6() { return static_cast<int32_t>(offsetof(SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC, ___mesh_6)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_mesh_6() const { return ___mesh_6; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_mesh_6() { return &___mesh_6; }
	inline void set_mesh_6(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___mesh_6 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_6), value);
	}

	inline static int32_t get_offset_of_meshLayers_7() { return static_cast<int32_t>(offsetof(SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC, ___meshLayers_7)); }
	inline List_1_tBE402A53CD5331E0CB483C591CFDC8C6827F35B8 * get_meshLayers_7() const { return ___meshLayers_7; }
	inline List_1_tBE402A53CD5331E0CB483C591CFDC8C6827F35B8 ** get_address_of_meshLayers_7() { return &___meshLayers_7; }
	inline void set_meshLayers_7(List_1_tBE402A53CD5331E0CB483C591CFDC8C6827F35B8 * value)
	{
		___meshLayers_7 = value;
		Il2CppCodeGenWriteBarrier((&___meshLayers_7), value);
	}

	inline static int32_t get_offset_of_names_8() { return static_cast<int32_t>(offsetof(SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC, ___names_8)); }
	inline List_1_t7600B3EA144F32C2120D420AB63C9E014D113CF1 * get_names_8() const { return ___names_8; }
	inline List_1_t7600B3EA144F32C2120D420AB63C9E014D113CF1 ** get_address_of_names_8() { return &___names_8; }
	inline void set_names_8(List_1_t7600B3EA144F32C2120D420AB63C9E014D113CF1 * value)
	{
		___names_8 = value;
		Il2CppCodeGenWriteBarrier((&___names_8), value);
	}

	inline static int32_t get_offset_of_collisionShapeData_9() { return static_cast<int32_t>(offsetof(SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC, ___collisionShapeData_9)); }
	inline CollisionShapeData_t09D2AF9F5D9E3B0C513049F97C117CF4EB9B2C6B * get_collisionShapeData_9() const { return ___collisionShapeData_9; }
	inline CollisionShapeData_t09D2AF9F5D9E3B0C513049F97C117CF4EB9B2C6B ** get_address_of_collisionShapeData_9() { return &___collisionShapeData_9; }
	inline void set_collisionShapeData_9(CollisionShapeData_t09D2AF9F5D9E3B0C513049F97C117CF4EB9B2C6B * value)
	{
		___collisionShapeData_9 = value;
		Il2CppCodeGenWriteBarrier((&___collisionShapeData_9), value);
	}

	inline static int32_t get_offset_of_collisionShapeDataLayers_10() { return static_cast<int32_t>(offsetof(SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC, ___collisionShapeDataLayers_10)); }
	inline List_1_tCBD9667398A994BB4DC992902858F6B45B0C5296 * get_collisionShapeDataLayers_10() const { return ___collisionShapeDataLayers_10; }
	inline List_1_tCBD9667398A994BB4DC992902858F6B45B0C5296 ** get_address_of_collisionShapeDataLayers_10() { return &___collisionShapeDataLayers_10; }
	inline void set_collisionShapeDataLayers_10(List_1_tCBD9667398A994BB4DC992902858F6B45B0C5296 * value)
	{
		___collisionShapeDataLayers_10 = value;
		Il2CppCodeGenWriteBarrier((&___collisionShapeDataLayers_10), value);
	}

	inline static int32_t get_offset_of_errors_11() { return static_cast<int32_t>(offsetof(SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC, ___errors_11)); }
	inline String_t* get_errors_11() const { return ___errors_11; }
	inline String_t** get_address_of_errors_11() { return &___errors_11; }
	inline void set_errors_11(String_t* value)
	{
		___errors_11 = value;
		Il2CppCodeGenWriteBarrier((&___errors_11), value);
	}

	inline static int32_t get_offset_of_unsupportedElements_12() { return static_cast<int32_t>(offsetof(SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC, ___unsupportedElements_12)); }
	inline Dictionary_2_t4807FD27415C3FDCCC28B95BCB1284A5DE60B015 * get_unsupportedElements_12() const { return ___unsupportedElements_12; }
	inline Dictionary_2_t4807FD27415C3FDCCC28B95BCB1284A5DE60B015 ** get_address_of_unsupportedElements_12() { return &___unsupportedElements_12; }
	inline void set_unsupportedElements_12(Dictionary_2_t4807FD27415C3FDCCC28B95BCB1284A5DE60B015 * value)
	{
		___unsupportedElements_12 = value;
		Il2CppCodeGenWriteBarrier((&___unsupportedElements_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLYSVGIMPORTER_TEC08895B75B240823232FABEEC86D3F31F13D9FC_H
#ifndef TRANSFORMATTRIBUTES_T02EDCB35162729433B60C3A5765296C0EC86D5A3_H
#define TRANSFORMATTRIBUTES_T02EDCB35162729433B60C3A5765296C0EC86D5A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.TransformAttributes
struct  TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Matrix> SimplySVG.TransformAttributes::transforms
	List_1_t56C6FF99794FDFD622FFD321BFB93EE419B4D89D * ___transforms_0;
	// Matrix SimplySVG.TransformAttributes::_combinedTransform
	Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78 * ____combinedTransform_1;

public:
	inline static int32_t get_offset_of_transforms_0() { return static_cast<int32_t>(offsetof(TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3, ___transforms_0)); }
	inline List_1_t56C6FF99794FDFD622FFD321BFB93EE419B4D89D * get_transforms_0() const { return ___transforms_0; }
	inline List_1_t56C6FF99794FDFD622FFD321BFB93EE419B4D89D ** get_address_of_transforms_0() { return &___transforms_0; }
	inline void set_transforms_0(List_1_t56C6FF99794FDFD622FFD321BFB93EE419B4D89D * value)
	{
		___transforms_0 = value;
		Il2CppCodeGenWriteBarrier((&___transforms_0), value);
	}

	inline static int32_t get_offset_of__combinedTransform_1() { return static_cast<int32_t>(offsetof(TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3, ____combinedTransform_1)); }
	inline Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78 * get__combinedTransform_1() const { return ____combinedTransform_1; }
	inline Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78 ** get_address_of__combinedTransform_1() { return &____combinedTransform_1; }
	inline void set__combinedTransform_1(Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78 * value)
	{
		____combinedTransform_1 = value;
		Il2CppCodeGenWriteBarrier((&____combinedTransform_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMATTRIBUTES_T02EDCB35162729433B60C3A5765296C0EC86D5A3_H
#ifndef TRIANGULATIONUTILITY_T8B071A46EB7BE50305CF04A1C97737D7887F3F0F_H
#define TRIANGULATIONUTILITY_T8B071A46EB7BE50305CF04A1C97737D7887F3F0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.TriangulationUtility
struct  TriangulationUtility_t8B071A46EB7BE50305CF04A1C97737D7887F3F0F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATIONUTILITY_T8B071A46EB7BE50305CF04A1C97737D7887F3F0F_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#define VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_com
{
};
#endif // VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifndef U24ARRAYTYPEU3D1028_TBEAF9EA97A5E11573FD162B05E20BE1EBF3CBB76_H
#define U24ARRAYTYPEU3D1028_TBEAF9EA97A5E11573FD162B05E20BE1EBF3CBB76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=1028
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D1028_tBEAF9EA97A5E11573FD162B05E20BE1EBF3CBB76 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D1028_tBEAF9EA97A5E11573FD162B05E20BE1EBF3CBB76__padding[1028];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D1028_TBEAF9EA97A5E11573FD162B05E20BE1EBF3CBB76_H
#ifndef U24ARRAYTYPEU3D8_T20F7F847C95E66FB8D8AF41A24396DBF4B4A58D1_H
#define U24ARRAYTYPEU3D8_T20F7F847C95E66FB8D8AF41A24396DBF4B4A58D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=8
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D8_t20F7F847C95E66FB8D8AF41A24396DBF4B4A58D1 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D8_t20F7F847C95E66FB8D8AF41A24396DBF4B4A58D1__padding[8];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D8_T20F7F847C95E66FB8D8AF41A24396DBF4B4A58D1_H
#ifndef CLIPPEREXCEPTION_TBB767771C8FA84C664383A6C5392C34BAC2FB72C_H
#define CLIPPEREXCEPTION_TBB767771C8FA84C664383A6C5392C34BAC2FB72C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.ClipperException
struct  ClipperException_tBB767771C8FA84C664383A6C5392C34BAC2FB72C  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPEREXCEPTION_TBB767771C8FA84C664383A6C5392C34BAC2FB72C_H
#ifndef DOUBLEPOINT_TD124BFF02A4B482F5C86257187A388BDD3033CAC_H
#define DOUBLEPOINT_TD124BFF02A4B482F5C86257187A388BDD3033CAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.DoublePoint
struct  DoublePoint_tD124BFF02A4B482F5C86257187A388BDD3033CAC 
{
public:
	// System.Double ClipperLib.DoublePoint::X
	double ___X_0;
	// System.Double ClipperLib.DoublePoint::Y
	double ___Y_1;

public:
	inline static int32_t get_offset_of_X_0() { return static_cast<int32_t>(offsetof(DoublePoint_tD124BFF02A4B482F5C86257187A388BDD3033CAC, ___X_0)); }
	inline double get_X_0() const { return ___X_0; }
	inline double* get_address_of_X_0() { return &___X_0; }
	inline void set_X_0(double value)
	{
		___X_0 = value;
	}

	inline static int32_t get_offset_of_Y_1() { return static_cast<int32_t>(offsetof(DoublePoint_tD124BFF02A4B482F5C86257187A388BDD3033CAC, ___Y_1)); }
	inline double get_Y_1() const { return ___Y_1; }
	inline double* get_address_of_Y_1() { return &___Y_1; }
	inline void set_Y_1(double value)
	{
		___Y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLEPOINT_TD124BFF02A4B482F5C86257187A388BDD3033CAC_H
#ifndef INT128_T04B631F57681B0E656E6183DD9EF432AECB915BB_H
#define INT128_T04B631F57681B0E656E6183DD9EF432AECB915BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.Int128
struct  Int128_t04B631F57681B0E656E6183DD9EF432AECB915BB 
{
public:
	// System.Int64 ClipperLib.Int128::hi
	int64_t ___hi_0;
	// System.UInt64 ClipperLib.Int128::lo
	uint64_t ___lo_1;

public:
	inline static int32_t get_offset_of_hi_0() { return static_cast<int32_t>(offsetof(Int128_t04B631F57681B0E656E6183DD9EF432AECB915BB, ___hi_0)); }
	inline int64_t get_hi_0() const { return ___hi_0; }
	inline int64_t* get_address_of_hi_0() { return &___hi_0; }
	inline void set_hi_0(int64_t value)
	{
		___hi_0 = value;
	}

	inline static int32_t get_offset_of_lo_1() { return static_cast<int32_t>(offsetof(Int128_t04B631F57681B0E656E6183DD9EF432AECB915BB, ___lo_1)); }
	inline uint64_t get_lo_1() const { return ___lo_1; }
	inline uint64_t* get_address_of_lo_1() { return &___lo_1; }
	inline void set_lo_1(uint64_t value)
	{
		___lo_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT128_T04B631F57681B0E656E6183DD9EF432AECB915BB_H
#ifndef INTPOINT_TD93C7E113CF7FC6B665570E1DCBA8E258BC354EB_H
#define INTPOINT_TD93C7E113CF7FC6B665570E1DCBA8E258BC354EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.IntPoint
struct  IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB 
{
public:
	// System.Int64 ClipperLib.IntPoint::X
	int64_t ___X_0;
	// System.Int64 ClipperLib.IntPoint::Y
	int64_t ___Y_1;

public:
	inline static int32_t get_offset_of_X_0() { return static_cast<int32_t>(offsetof(IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB, ___X_0)); }
	inline int64_t get_X_0() const { return ___X_0; }
	inline int64_t* get_address_of_X_0() { return &___X_0; }
	inline void set_X_0(int64_t value)
	{
		___X_0 = value;
	}

	inline static int32_t get_offset_of_Y_1() { return static_cast<int32_t>(offsetof(IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB, ___Y_1)); }
	inline int64_t get_Y_1() const { return ___Y_1; }
	inline int64_t* get_address_of_Y_1() { return &___Y_1; }
	inline void set_Y_1(int64_t value)
	{
		___Y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPOINT_TD93C7E113CF7FC6B665570E1DCBA8E258BC354EB_H
#ifndef INTRECT_T8BD1DF410700349DD0195EBDEB436F5E36333B6D_H
#define INTRECT_T8BD1DF410700349DD0195EBDEB436F5E36333B6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.IntRect
struct  IntRect_t8BD1DF410700349DD0195EBDEB436F5E36333B6D 
{
public:
	// System.Int64 ClipperLib.IntRect::left
	int64_t ___left_0;
	// System.Int64 ClipperLib.IntRect::top
	int64_t ___top_1;
	// System.Int64 ClipperLib.IntRect::right
	int64_t ___right_2;
	// System.Int64 ClipperLib.IntRect::bottom
	int64_t ___bottom_3;

public:
	inline static int32_t get_offset_of_left_0() { return static_cast<int32_t>(offsetof(IntRect_t8BD1DF410700349DD0195EBDEB436F5E36333B6D, ___left_0)); }
	inline int64_t get_left_0() const { return ___left_0; }
	inline int64_t* get_address_of_left_0() { return &___left_0; }
	inline void set_left_0(int64_t value)
	{
		___left_0 = value;
	}

	inline static int32_t get_offset_of_top_1() { return static_cast<int32_t>(offsetof(IntRect_t8BD1DF410700349DD0195EBDEB436F5E36333B6D, ___top_1)); }
	inline int64_t get_top_1() const { return ___top_1; }
	inline int64_t* get_address_of_top_1() { return &___top_1; }
	inline void set_top_1(int64_t value)
	{
		___top_1 = value;
	}

	inline static int32_t get_offset_of_right_2() { return static_cast<int32_t>(offsetof(IntRect_t8BD1DF410700349DD0195EBDEB436F5E36333B6D, ___right_2)); }
	inline int64_t get_right_2() const { return ___right_2; }
	inline int64_t* get_address_of_right_2() { return &___right_2; }
	inline void set_right_2(int64_t value)
	{
		___right_2 = value;
	}

	inline static int32_t get_offset_of_bottom_3() { return static_cast<int32_t>(offsetof(IntRect_t8BD1DF410700349DD0195EBDEB436F5E36333B6D, ___bottom_3)); }
	inline int64_t get_bottom_3() const { return ___bottom_3; }
	inline int64_t* get_address_of_bottom_3() { return &___bottom_3; }
	inline void set_bottom_3(int64_t value)
	{
		___bottom_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTRECT_T8BD1DF410700349DD0195EBDEB436F5E36333B6D_H
#ifndef MEXCEPTION_TC61D13FC7CF7B038EB7ADA1E1750BC0DA5D3B2E1_H
#define MEXCEPTION_TC61D13FC7CF7B038EB7ADA1E1750BC0DA5D3B2E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MException
struct  MException_tC61D13FC7CF7B038EB7ADA1E1750BC0DA5D3B2E1  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEXCEPTION_TC61D13FC7CF7B038EB7ADA1E1750BC0DA5D3B2E1_H
#ifndef DTSWEEPDEBUGCONTEXT_TC024E42403443A0C8F8B910E52B07246494C19AD_H
#define DTSWEEPDEBUGCONTEXT_TC024E42403443A0C8F8B910E52B07246494C19AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.DTSweepDebugContext
struct  DTSweepDebugContext_tC024E42403443A0C8F8B910E52B07246494C19AD  : public TriangulationDebugContext_tF9458C2C5E3E324954C544E9FB4044AC0B1F103C
{
public:
	// Poly2Tri.DelaunayTriangle Poly2Tri.DTSweepDebugContext::_primaryTriangle
	DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891 * ____primaryTriangle_1;
	// Poly2Tri.DelaunayTriangle Poly2Tri.DTSweepDebugContext::_secondaryTriangle
	DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891 * ____secondaryTriangle_2;
	// Poly2Tri.TriangulationPoint Poly2Tri.DTSweepDebugContext::_activePoint
	TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * ____activePoint_3;
	// Poly2Tri.AdvancingFrontNode Poly2Tri.DTSweepDebugContext::_activeNode
	AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * ____activeNode_4;
	// Poly2Tri.DTSweepConstraint Poly2Tri.DTSweepDebugContext::_activeConstraint
	DTSweepConstraint_t8C5C973CF68522DA5D7828F45161FE7A22203819 * ____activeConstraint_5;

public:
	inline static int32_t get_offset_of__primaryTriangle_1() { return static_cast<int32_t>(offsetof(DTSweepDebugContext_tC024E42403443A0C8F8B910E52B07246494C19AD, ____primaryTriangle_1)); }
	inline DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891 * get__primaryTriangle_1() const { return ____primaryTriangle_1; }
	inline DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891 ** get_address_of__primaryTriangle_1() { return &____primaryTriangle_1; }
	inline void set__primaryTriangle_1(DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891 * value)
	{
		____primaryTriangle_1 = value;
		Il2CppCodeGenWriteBarrier((&____primaryTriangle_1), value);
	}

	inline static int32_t get_offset_of__secondaryTriangle_2() { return static_cast<int32_t>(offsetof(DTSweepDebugContext_tC024E42403443A0C8F8B910E52B07246494C19AD, ____secondaryTriangle_2)); }
	inline DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891 * get__secondaryTriangle_2() const { return ____secondaryTriangle_2; }
	inline DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891 ** get_address_of__secondaryTriangle_2() { return &____secondaryTriangle_2; }
	inline void set__secondaryTriangle_2(DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891 * value)
	{
		____secondaryTriangle_2 = value;
		Il2CppCodeGenWriteBarrier((&____secondaryTriangle_2), value);
	}

	inline static int32_t get_offset_of__activePoint_3() { return static_cast<int32_t>(offsetof(DTSweepDebugContext_tC024E42403443A0C8F8B910E52B07246494C19AD, ____activePoint_3)); }
	inline TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * get__activePoint_3() const { return ____activePoint_3; }
	inline TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 ** get_address_of__activePoint_3() { return &____activePoint_3; }
	inline void set__activePoint_3(TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * value)
	{
		____activePoint_3 = value;
		Il2CppCodeGenWriteBarrier((&____activePoint_3), value);
	}

	inline static int32_t get_offset_of__activeNode_4() { return static_cast<int32_t>(offsetof(DTSweepDebugContext_tC024E42403443A0C8F8B910E52B07246494C19AD, ____activeNode_4)); }
	inline AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * get__activeNode_4() const { return ____activeNode_4; }
	inline AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F ** get_address_of__activeNode_4() { return &____activeNode_4; }
	inline void set__activeNode_4(AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F * value)
	{
		____activeNode_4 = value;
		Il2CppCodeGenWriteBarrier((&____activeNode_4), value);
	}

	inline static int32_t get_offset_of__activeConstraint_5() { return static_cast<int32_t>(offsetof(DTSweepDebugContext_tC024E42403443A0C8F8B910E52B07246494C19AD, ____activeConstraint_5)); }
	inline DTSweepConstraint_t8C5C973CF68522DA5D7828F45161FE7A22203819 * get__activeConstraint_5() const { return ____activeConstraint_5; }
	inline DTSweepConstraint_t8C5C973CF68522DA5D7828F45161FE7A22203819 ** get_address_of__activeConstraint_5() { return &____activeConstraint_5; }
	inline void set__activeConstraint_5(DTSweepConstraint_t8C5C973CF68522DA5D7828F45161FE7A22203819 * value)
	{
		____activeConstraint_5 = value;
		Il2CppCodeGenWriteBarrier((&____activeConstraint_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTSWEEPDEBUGCONTEXT_TC024E42403443A0C8F8B910E52B07246494C19AD_H
#ifndef FIXEDARRAY3_1_T0825630D0BE588202967D839253E872C4CBCC88B_H
#define FIXEDARRAY3_1_T0825630D0BE588202967D839253E872C4CBCC88B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.FixedArray3`1<Poly2Tri.DelaunayTriangle>
struct  FixedArray3_1_t0825630D0BE588202967D839253E872C4CBCC88B 
{
public:
	// T Poly2Tri.FixedArray3`1::_0
	DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891 * ____0_0;
	// T Poly2Tri.FixedArray3`1::_1
	DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891 * ____1_1;
	// T Poly2Tri.FixedArray3`1::_2
	DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891 * ____2_2;

public:
	inline static int32_t get_offset_of__0_0() { return static_cast<int32_t>(offsetof(FixedArray3_1_t0825630D0BE588202967D839253E872C4CBCC88B, ____0_0)); }
	inline DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891 * get__0_0() const { return ____0_0; }
	inline DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891 ** get_address_of__0_0() { return &____0_0; }
	inline void set__0_0(DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891 * value)
	{
		____0_0 = value;
		Il2CppCodeGenWriteBarrier((&____0_0), value);
	}

	inline static int32_t get_offset_of__1_1() { return static_cast<int32_t>(offsetof(FixedArray3_1_t0825630D0BE588202967D839253E872C4CBCC88B, ____1_1)); }
	inline DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891 * get__1_1() const { return ____1_1; }
	inline DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891 ** get_address_of__1_1() { return &____1_1; }
	inline void set__1_1(DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891 * value)
	{
		____1_1 = value;
		Il2CppCodeGenWriteBarrier((&____1_1), value);
	}

	inline static int32_t get_offset_of__2_2() { return static_cast<int32_t>(offsetof(FixedArray3_1_t0825630D0BE588202967D839253E872C4CBCC88B, ____2_2)); }
	inline DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891 * get__2_2() const { return ____2_2; }
	inline DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891 ** get_address_of__2_2() { return &____2_2; }
	inline void set__2_2(DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891 * value)
	{
		____2_2 = value;
		Il2CppCodeGenWriteBarrier((&____2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDARRAY3_1_T0825630D0BE588202967D839253E872C4CBCC88B_H
#ifndef FIXEDARRAY3_1_T0F7FC5C3079E0A6D350ACEC061B23ECAB8EFADA4_H
#define FIXEDARRAY3_1_T0F7FC5C3079E0A6D350ACEC061B23ECAB8EFADA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.FixedArray3`1<Poly2Tri.TriangulationPoint>
struct  FixedArray3_1_t0F7FC5C3079E0A6D350ACEC061B23ECAB8EFADA4 
{
public:
	// T Poly2Tri.FixedArray3`1::_0
	TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * ____0_0;
	// T Poly2Tri.FixedArray3`1::_1
	TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * ____1_1;
	// T Poly2Tri.FixedArray3`1::_2
	TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * ____2_2;

public:
	inline static int32_t get_offset_of__0_0() { return static_cast<int32_t>(offsetof(FixedArray3_1_t0F7FC5C3079E0A6D350ACEC061B23ECAB8EFADA4, ____0_0)); }
	inline TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * get__0_0() const { return ____0_0; }
	inline TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 ** get_address_of__0_0() { return &____0_0; }
	inline void set__0_0(TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * value)
	{
		____0_0 = value;
		Il2CppCodeGenWriteBarrier((&____0_0), value);
	}

	inline static int32_t get_offset_of__1_1() { return static_cast<int32_t>(offsetof(FixedArray3_1_t0F7FC5C3079E0A6D350ACEC061B23ECAB8EFADA4, ____1_1)); }
	inline TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * get__1_1() const { return ____1_1; }
	inline TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 ** get_address_of__1_1() { return &____1_1; }
	inline void set__1_1(TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * value)
	{
		____1_1 = value;
		Il2CppCodeGenWriteBarrier((&____1_1), value);
	}

	inline static int32_t get_offset_of__2_2() { return static_cast<int32_t>(offsetof(FixedArray3_1_t0F7FC5C3079E0A6D350ACEC061B23ECAB8EFADA4, ____2_2)); }
	inline TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * get__2_2() const { return ____2_2; }
	inline TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 ** get_address_of__2_2() { return &____2_2; }
	inline void set__2_2(TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * value)
	{
		____2_2 = value;
		Il2CppCodeGenWriteBarrier((&____2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDARRAY3_1_T0F7FC5C3079E0A6D350ACEC061B23ECAB8EFADA4_H
#ifndef FIXEDBITARRAY3_TB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB_H
#define FIXEDBITARRAY3_TB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.FixedBitArray3
struct  FixedBitArray3_tB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB 
{
public:
	// System.Boolean Poly2Tri.FixedBitArray3::_0
	bool ____0_0;
	// System.Boolean Poly2Tri.FixedBitArray3::_1
	bool ____1_1;
	// System.Boolean Poly2Tri.FixedBitArray3::_2
	bool ____2_2;

public:
	inline static int32_t get_offset_of__0_0() { return static_cast<int32_t>(offsetof(FixedBitArray3_tB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB, ____0_0)); }
	inline bool get__0_0() const { return ____0_0; }
	inline bool* get_address_of__0_0() { return &____0_0; }
	inline void set__0_0(bool value)
	{
		____0_0 = value;
	}

	inline static int32_t get_offset_of__1_1() { return static_cast<int32_t>(offsetof(FixedBitArray3_tB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB, ____1_1)); }
	inline bool get__1_1() const { return ____1_1; }
	inline bool* get_address_of__1_1() { return &____1_1; }
	inline void set__1_1(bool value)
	{
		____1_1 = value;
	}

	inline static int32_t get_offset_of__2_2() { return static_cast<int32_t>(offsetof(FixedBitArray3_tB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB, ____2_2)); }
	inline bool get__2_2() const { return ____2_2; }
	inline bool* get_address_of__2_2() { return &____2_2; }
	inline void set__2_2(bool value)
	{
		____2_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Poly2Tri.FixedBitArray3
struct FixedBitArray3_tB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB_marshaled_pinvoke
{
	int32_t ____0_0;
	int32_t ____1_1;
	int32_t ____2_2;
};
// Native definition for COM marshalling of Poly2Tri.FixedBitArray3
struct FixedBitArray3_tB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB_marshaled_com
{
	int32_t ____0_0;
	int32_t ____1_1;
	int32_t ____2_2;
};
#endif // FIXEDBITARRAY3_TB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB_H
#ifndef TRIANGULATIONCONSTRAINT_T420CE8B0E6C488B8F08766FBC3E6FAC5E5D6EC56_H
#define TRIANGULATIONCONSTRAINT_T420CE8B0E6C488B8F08766FBC3E6FAC5E5D6EC56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.TriangulationConstraint
struct  TriangulationConstraint_t420CE8B0E6C488B8F08766FBC3E6FAC5E5D6EC56  : public Edge_t5170DF73FC08083B2752D6AD8F039BF371D0B79B
{
public:
	// System.UInt32 Poly2Tri.TriangulationConstraint::mContraintCode
	uint32_t ___mContraintCode_2;

public:
	inline static int32_t get_offset_of_mContraintCode_2() { return static_cast<int32_t>(offsetof(TriangulationConstraint_t420CE8B0E6C488B8F08766FBC3E6FAC5E5D6EC56, ___mContraintCode_2)); }
	inline uint32_t get_mContraintCode_2() const { return ___mContraintCode_2; }
	inline uint32_t* get_address_of_mContraintCode_2() { return &___mContraintCode_2; }
	inline void set_mContraintCode_2(uint32_t value)
	{
		___mContraintCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATIONCONSTRAINT_T420CE8B0E6C488B8F08766FBC3E6FAC5E5D6EC56_H
#ifndef TRIANGULATIONPOINT_TA0795727BB94BE27AD7C04A99743A8F08EA31BE4_H
#define TRIANGULATIONPOINT_TA0795727BB94BE27AD7C04A99743A8F08EA31BE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.TriangulationPoint
struct  TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4  : public Point2D_t35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF
{
public:
	// System.UInt32 Poly2Tri.TriangulationPoint::mVertexCode
	uint32_t ___mVertexCode_3;
	// System.Collections.Generic.List`1<Poly2Tri.DTSweepConstraint> Poly2Tri.TriangulationPoint::<Edges>k__BackingField
	List_1_t2A29EEE2F8999DD054E1601D541DCDFD34536C26 * ___U3CEdgesU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_mVertexCode_3() { return static_cast<int32_t>(offsetof(TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4, ___mVertexCode_3)); }
	inline uint32_t get_mVertexCode_3() const { return ___mVertexCode_3; }
	inline uint32_t* get_address_of_mVertexCode_3() { return &___mVertexCode_3; }
	inline void set_mVertexCode_3(uint32_t value)
	{
		___mVertexCode_3 = value;
	}

	inline static int32_t get_offset_of_U3CEdgesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4, ___U3CEdgesU3Ek__BackingField_4)); }
	inline List_1_t2A29EEE2F8999DD054E1601D541DCDFD34536C26 * get_U3CEdgesU3Ek__BackingField_4() const { return ___U3CEdgesU3Ek__BackingField_4; }
	inline List_1_t2A29EEE2F8999DD054E1601D541DCDFD34536C26 ** get_address_of_U3CEdgesU3Ek__BackingField_4() { return &___U3CEdgesU3Ek__BackingField_4; }
	inline void set_U3CEdgesU3Ek__BackingField_4(List_1_t2A29EEE2F8999DD054E1601D541DCDFD34536C26 * value)
	{
		___U3CEdgesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEdgesU3Ek__BackingField_4), value);
	}
};

struct TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4_StaticFields
{
public:
	// System.Double Poly2Tri.TriangulationPoint::kVertexCodeDefaultPrecision
	double ___kVertexCodeDefaultPrecision_2;

public:
	inline static int32_t get_offset_of_kVertexCodeDefaultPrecision_2() { return static_cast<int32_t>(offsetof(TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4_StaticFields, ___kVertexCodeDefaultPrecision_2)); }
	inline double get_kVertexCodeDefaultPrecision_2() const { return ___kVertexCodeDefaultPrecision_2; }
	inline double* get_address_of_kVertexCodeDefaultPrecision_2() { return &___kVertexCodeDefaultPrecision_2; }
	inline void set_kVertexCodeDefaultPrecision_2(double value)
	{
		___kVertexCodeDefaultPrecision_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATIONPOINT_TA0795727BB94BE27AD7C04A99743A8F08EA31BE4_H
#ifndef USEELEMENT_TFD190524412A3AD968B4BF37A4DB4AF17ED316E9_H
#define USEELEMENT_TFD190524412A3AD968B4BF37A4DB4AF17ED316E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.UseElement
struct  UseElement_tFD190524412A3AD968B4BF37A4DB4AF17ED316E9  : public SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888
{
public:
	// SimplySVG.GraphicalAttributes SimplySVG.UseElement::localGraphicalAttributes
	GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D * ___localGraphicalAttributes_4;
	// SimplySVG.TransformAttributes SimplySVG.UseElement::localTransformAttributes
	TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3 * ___localTransformAttributes_5;
	// SimplySVG.SVGElement SimplySVG.UseElement::surrogateForElement
	SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888 * ___surrogateForElement_6;

public:
	inline static int32_t get_offset_of_localGraphicalAttributes_4() { return static_cast<int32_t>(offsetof(UseElement_tFD190524412A3AD968B4BF37A4DB4AF17ED316E9, ___localGraphicalAttributes_4)); }
	inline GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D * get_localGraphicalAttributes_4() const { return ___localGraphicalAttributes_4; }
	inline GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D ** get_address_of_localGraphicalAttributes_4() { return &___localGraphicalAttributes_4; }
	inline void set_localGraphicalAttributes_4(GraphicalAttributes_t22A39FF65774E208391CDCA2A02C776F6E2DF80D * value)
	{
		___localGraphicalAttributes_4 = value;
		Il2CppCodeGenWriteBarrier((&___localGraphicalAttributes_4), value);
	}

	inline static int32_t get_offset_of_localTransformAttributes_5() { return static_cast<int32_t>(offsetof(UseElement_tFD190524412A3AD968B4BF37A4DB4AF17ED316E9, ___localTransformAttributes_5)); }
	inline TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3 * get_localTransformAttributes_5() const { return ___localTransformAttributes_5; }
	inline TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3 ** get_address_of_localTransformAttributes_5() { return &___localTransformAttributes_5; }
	inline void set_localTransformAttributes_5(TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3 * value)
	{
		___localTransformAttributes_5 = value;
		Il2CppCodeGenWriteBarrier((&___localTransformAttributes_5), value);
	}

	inline static int32_t get_offset_of_surrogateForElement_6() { return static_cast<int32_t>(offsetof(UseElement_tFD190524412A3AD968B4BF37A4DB4AF17ED316E9, ___surrogateForElement_6)); }
	inline SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888 * get_surrogateForElement_6() const { return ___surrogateForElement_6; }
	inline SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888 ** get_address_of_surrogateForElement_6() { return &___surrogateForElement_6; }
	inline void set_surrogateForElement_6(SVGElement_t83C50C9210FA55853195E3B39B9E5AC78E9A9888 * value)
	{
		___surrogateForElement_6 = value;
		Il2CppCodeGenWriteBarrier((&___surrogateForElement_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USEELEMENT_TFD190524412A3AD968B4BF37A4DB4AF17ED316E9_H
#ifndef ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#define ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF  : public ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C
{
public:

public:
};

struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_com
{
};
#endif // ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SYSTEMEXCEPTION_T81A06ADD519539D252788D75E44AFFBE6580E301_H
#define SYSTEMEXCEPTION_T81A06ADD519539D252788D75E44AFFBE6580E301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t81A06ADD519539D252788D75E44AFFBE6580E301  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T81A06ADD519539D252788D75E44AFFBE6580E301_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=1028 <PrivateImplementationDetails>::$field-6DE680C155B94A30FA53CADC8E86B835051FE7E8
	U24ArrayTypeU3D1028_tBEAF9EA97A5E11573FD162B05E20BE1EBF3CBB76  ___U24fieldU2D6DE680C155B94A30FA53CADC8E86B835051FE7E8_0;
	// <PrivateImplementationDetails>/$ArrayType=8 <PrivateImplementationDetails>::$field-92DC78AF759B423C04558A0433EDDBBA32C620F2
	U24ArrayTypeU3D8_t20F7F847C95E66FB8D8AF41A24396DBF4B4A58D1  ___U24fieldU2D92DC78AF759B423C04558A0433EDDBBA32C620F2_1;

public:
	inline static int32_t get_offset_of_U24fieldU2D6DE680C155B94A30FA53CADC8E86B835051FE7E8_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___U24fieldU2D6DE680C155B94A30FA53CADC8E86B835051FE7E8_0)); }
	inline U24ArrayTypeU3D1028_tBEAF9EA97A5E11573FD162B05E20BE1EBF3CBB76  get_U24fieldU2D6DE680C155B94A30FA53CADC8E86B835051FE7E8_0() const { return ___U24fieldU2D6DE680C155B94A30FA53CADC8E86B835051FE7E8_0; }
	inline U24ArrayTypeU3D1028_tBEAF9EA97A5E11573FD162B05E20BE1EBF3CBB76 * get_address_of_U24fieldU2D6DE680C155B94A30FA53CADC8E86B835051FE7E8_0() { return &___U24fieldU2D6DE680C155B94A30FA53CADC8E86B835051FE7E8_0; }
	inline void set_U24fieldU2D6DE680C155B94A30FA53CADC8E86B835051FE7E8_0(U24ArrayTypeU3D1028_tBEAF9EA97A5E11573FD162B05E20BE1EBF3CBB76  value)
	{
		___U24fieldU2D6DE680C155B94A30FA53CADC8E86B835051FE7E8_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D92DC78AF759B423C04558A0433EDDBBA32C620F2_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___U24fieldU2D92DC78AF759B423C04558A0433EDDBBA32C620F2_1)); }
	inline U24ArrayTypeU3D8_t20F7F847C95E66FB8D8AF41A24396DBF4B4A58D1  get_U24fieldU2D92DC78AF759B423C04558A0433EDDBBA32C620F2_1() const { return ___U24fieldU2D92DC78AF759B423C04558A0433EDDBBA32C620F2_1; }
	inline U24ArrayTypeU3D8_t20F7F847C95E66FB8D8AF41A24396DBF4B4A58D1 * get_address_of_U24fieldU2D92DC78AF759B423C04558A0433EDDBBA32C620F2_1() { return &___U24fieldU2D92DC78AF759B423C04558A0433EDDBBA32C620F2_1; }
	inline void set_U24fieldU2D92DC78AF759B423C04558A0433EDDBBA32C620F2_1(U24ArrayTypeU3D8_t20F7F847C95E66FB8D8AF41A24396DBF4B4A58D1  value)
	{
		___U24fieldU2D92DC78AF759B423C04558A0433EDDBBA32C620F2_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#ifndef CLIPTYPE_T2491F122FD55AC747C5F6FAA007683A23BCA1DA3_H
#define CLIPTYPE_T2491F122FD55AC747C5F6FAA007683A23BCA1DA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.ClipType
struct  ClipType_t2491F122FD55AC747C5F6FAA007683A23BCA1DA3 
{
public:
	// System.Int32 ClipperLib.ClipType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ClipType_t2491F122FD55AC747C5F6FAA007683A23BCA1DA3, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPTYPE_T2491F122FD55AC747C5F6FAA007683A23BCA1DA3_H
#ifndef NODETYPE_TDD65844235B568E0ED7138B6AEFA77B6EAB5C325_H
#define NODETYPE_TDD65844235B568E0ED7138B6AEFA77B6EAB5C325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.Clipper/NodeType
struct  NodeType_tDD65844235B568E0ED7138B6AEFA77B6EAB5C325 
{
public:
	// System.Int32 ClipperLib.Clipper/NodeType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NodeType_tDD65844235B568E0ED7138B6AEFA77B6EAB5C325, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODETYPE_TDD65844235B568E0ED7138B6AEFA77B6EAB5C325_H
#ifndef CLIPPEROFFSET_T9C6F09F33078744685F1BAA19177374D27FBD404_H
#define CLIPPEROFFSET_T9C6F09F33078744685F1BAA19177374D27FBD404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.ClipperOffset
struct  ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<ClipperLib.IntPoint>> ClipperLib.ClipperOffset::m_destPolys
	List_1_tAF7BD58C351F1353D1522DB1DC6B35AA5269CC6E * ___m_destPolys_0;
	// System.Collections.Generic.List`1<ClipperLib.IntPoint> ClipperLib.ClipperOffset::m_srcPoly
	List_1_tD323E55C055A7CA7984664415A9E8332BB24408B * ___m_srcPoly_1;
	// System.Collections.Generic.List`1<ClipperLib.IntPoint> ClipperLib.ClipperOffset::m_destPoly
	List_1_tD323E55C055A7CA7984664415A9E8332BB24408B * ___m_destPoly_2;
	// System.Collections.Generic.List`1<ClipperLib.DoublePoint> ClipperLib.ClipperOffset::m_normals
	List_1_t4D3F62B7FE29F08FF7B94BC47C7B803913F1CA69 * ___m_normals_3;
	// System.Double ClipperLib.ClipperOffset::m_delta
	double ___m_delta_4;
	// System.Double ClipperLib.ClipperOffset::m_sinA
	double ___m_sinA_5;
	// System.Double ClipperLib.ClipperOffset::m_sin
	double ___m_sin_6;
	// System.Double ClipperLib.ClipperOffset::m_cos
	double ___m_cos_7;
	// System.Double ClipperLib.ClipperOffset::m_miterLim
	double ___m_miterLim_8;
	// System.Double ClipperLib.ClipperOffset::m_StepsPerRad
	double ___m_StepsPerRad_9;
	// ClipperLib.IntPoint ClipperLib.ClipperOffset::m_lowest
	IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB  ___m_lowest_10;
	// ClipperLib.PolyNode ClipperLib.ClipperOffset::m_polyNodes
	PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB * ___m_polyNodes_11;
	// System.Double ClipperLib.ClipperOffset::<ArcTolerance>k__BackingField
	double ___U3CArcToleranceU3Ek__BackingField_12;
	// System.Double ClipperLib.ClipperOffset::<MiterLimit>k__BackingField
	double ___U3CMiterLimitU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_m_destPolys_0() { return static_cast<int32_t>(offsetof(ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404, ___m_destPolys_0)); }
	inline List_1_tAF7BD58C351F1353D1522DB1DC6B35AA5269CC6E * get_m_destPolys_0() const { return ___m_destPolys_0; }
	inline List_1_tAF7BD58C351F1353D1522DB1DC6B35AA5269CC6E ** get_address_of_m_destPolys_0() { return &___m_destPolys_0; }
	inline void set_m_destPolys_0(List_1_tAF7BD58C351F1353D1522DB1DC6B35AA5269CC6E * value)
	{
		___m_destPolys_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_destPolys_0), value);
	}

	inline static int32_t get_offset_of_m_srcPoly_1() { return static_cast<int32_t>(offsetof(ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404, ___m_srcPoly_1)); }
	inline List_1_tD323E55C055A7CA7984664415A9E8332BB24408B * get_m_srcPoly_1() const { return ___m_srcPoly_1; }
	inline List_1_tD323E55C055A7CA7984664415A9E8332BB24408B ** get_address_of_m_srcPoly_1() { return &___m_srcPoly_1; }
	inline void set_m_srcPoly_1(List_1_tD323E55C055A7CA7984664415A9E8332BB24408B * value)
	{
		___m_srcPoly_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_srcPoly_1), value);
	}

	inline static int32_t get_offset_of_m_destPoly_2() { return static_cast<int32_t>(offsetof(ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404, ___m_destPoly_2)); }
	inline List_1_tD323E55C055A7CA7984664415A9E8332BB24408B * get_m_destPoly_2() const { return ___m_destPoly_2; }
	inline List_1_tD323E55C055A7CA7984664415A9E8332BB24408B ** get_address_of_m_destPoly_2() { return &___m_destPoly_2; }
	inline void set_m_destPoly_2(List_1_tD323E55C055A7CA7984664415A9E8332BB24408B * value)
	{
		___m_destPoly_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_destPoly_2), value);
	}

	inline static int32_t get_offset_of_m_normals_3() { return static_cast<int32_t>(offsetof(ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404, ___m_normals_3)); }
	inline List_1_t4D3F62B7FE29F08FF7B94BC47C7B803913F1CA69 * get_m_normals_3() const { return ___m_normals_3; }
	inline List_1_t4D3F62B7FE29F08FF7B94BC47C7B803913F1CA69 ** get_address_of_m_normals_3() { return &___m_normals_3; }
	inline void set_m_normals_3(List_1_t4D3F62B7FE29F08FF7B94BC47C7B803913F1CA69 * value)
	{
		___m_normals_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_normals_3), value);
	}

	inline static int32_t get_offset_of_m_delta_4() { return static_cast<int32_t>(offsetof(ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404, ___m_delta_4)); }
	inline double get_m_delta_4() const { return ___m_delta_4; }
	inline double* get_address_of_m_delta_4() { return &___m_delta_4; }
	inline void set_m_delta_4(double value)
	{
		___m_delta_4 = value;
	}

	inline static int32_t get_offset_of_m_sinA_5() { return static_cast<int32_t>(offsetof(ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404, ___m_sinA_5)); }
	inline double get_m_sinA_5() const { return ___m_sinA_5; }
	inline double* get_address_of_m_sinA_5() { return &___m_sinA_5; }
	inline void set_m_sinA_5(double value)
	{
		___m_sinA_5 = value;
	}

	inline static int32_t get_offset_of_m_sin_6() { return static_cast<int32_t>(offsetof(ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404, ___m_sin_6)); }
	inline double get_m_sin_6() const { return ___m_sin_6; }
	inline double* get_address_of_m_sin_6() { return &___m_sin_6; }
	inline void set_m_sin_6(double value)
	{
		___m_sin_6 = value;
	}

	inline static int32_t get_offset_of_m_cos_7() { return static_cast<int32_t>(offsetof(ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404, ___m_cos_7)); }
	inline double get_m_cos_7() const { return ___m_cos_7; }
	inline double* get_address_of_m_cos_7() { return &___m_cos_7; }
	inline void set_m_cos_7(double value)
	{
		___m_cos_7 = value;
	}

	inline static int32_t get_offset_of_m_miterLim_8() { return static_cast<int32_t>(offsetof(ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404, ___m_miterLim_8)); }
	inline double get_m_miterLim_8() const { return ___m_miterLim_8; }
	inline double* get_address_of_m_miterLim_8() { return &___m_miterLim_8; }
	inline void set_m_miterLim_8(double value)
	{
		___m_miterLim_8 = value;
	}

	inline static int32_t get_offset_of_m_StepsPerRad_9() { return static_cast<int32_t>(offsetof(ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404, ___m_StepsPerRad_9)); }
	inline double get_m_StepsPerRad_9() const { return ___m_StepsPerRad_9; }
	inline double* get_address_of_m_StepsPerRad_9() { return &___m_StepsPerRad_9; }
	inline void set_m_StepsPerRad_9(double value)
	{
		___m_StepsPerRad_9 = value;
	}

	inline static int32_t get_offset_of_m_lowest_10() { return static_cast<int32_t>(offsetof(ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404, ___m_lowest_10)); }
	inline IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB  get_m_lowest_10() const { return ___m_lowest_10; }
	inline IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB * get_address_of_m_lowest_10() { return &___m_lowest_10; }
	inline void set_m_lowest_10(IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB  value)
	{
		___m_lowest_10 = value;
	}

	inline static int32_t get_offset_of_m_polyNodes_11() { return static_cast<int32_t>(offsetof(ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404, ___m_polyNodes_11)); }
	inline PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB * get_m_polyNodes_11() const { return ___m_polyNodes_11; }
	inline PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB ** get_address_of_m_polyNodes_11() { return &___m_polyNodes_11; }
	inline void set_m_polyNodes_11(PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB * value)
	{
		___m_polyNodes_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_polyNodes_11), value);
	}

	inline static int32_t get_offset_of_U3CArcToleranceU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404, ___U3CArcToleranceU3Ek__BackingField_12)); }
	inline double get_U3CArcToleranceU3Ek__BackingField_12() const { return ___U3CArcToleranceU3Ek__BackingField_12; }
	inline double* get_address_of_U3CArcToleranceU3Ek__BackingField_12() { return &___U3CArcToleranceU3Ek__BackingField_12; }
	inline void set_U3CArcToleranceU3Ek__BackingField_12(double value)
	{
		___U3CArcToleranceU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CMiterLimitU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404, ___U3CMiterLimitU3Ek__BackingField_13)); }
	inline double get_U3CMiterLimitU3Ek__BackingField_13() const { return ___U3CMiterLimitU3Ek__BackingField_13; }
	inline double* get_address_of_U3CMiterLimitU3Ek__BackingField_13() { return &___U3CMiterLimitU3Ek__BackingField_13; }
	inline void set_U3CMiterLimitU3Ek__BackingField_13(double value)
	{
		___U3CMiterLimitU3Ek__BackingField_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPEROFFSET_T9C6F09F33078744685F1BAA19177374D27FBD404_H
#ifndef DIRECTION_TFBEFA40C69E60C31BFB92F3869B4DB5D378D0E5F_H
#define DIRECTION_TFBEFA40C69E60C31BFB92F3869B4DB5D378D0E5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.Direction
struct  Direction_tFBEFA40C69E60C31BFB92F3869B4DB5D378D0E5F 
{
public:
	// System.Int32 ClipperLib.Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_tFBEFA40C69E60C31BFB92F3869B4DB5D378D0E5F, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_TFBEFA40C69E60C31BFB92F3869B4DB5D378D0E5F_H
#ifndef EDGESIDE_T7D9DB3581AC689985CF1A7CD6906A316B075C0BD_H
#define EDGESIDE_T7D9DB3581AC689985CF1A7CD6906A316B075C0BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.EdgeSide
struct  EdgeSide_t7D9DB3581AC689985CF1A7CD6906A316B075C0BD 
{
public:
	// System.Int32 ClipperLib.EdgeSide::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EdgeSide_t7D9DB3581AC689985CF1A7CD6906A316B075C0BD, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDGESIDE_T7D9DB3581AC689985CF1A7CD6906A316B075C0BD_H
#ifndef ENDTYPE_TC3D012CB12023E5FBBB34076C630B50B23636AD9_H
#define ENDTYPE_TC3D012CB12023E5FBBB34076C630B50B23636AD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.EndType
struct  EndType_tC3D012CB12023E5FBBB34076C630B50B23636AD9 
{
public:
	// System.Int32 ClipperLib.EndType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EndType_tC3D012CB12023E5FBBB34076C630B50B23636AD9, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDTYPE_TC3D012CB12023E5FBBB34076C630B50B23636AD9_H
#ifndef INTERSECTNODE_TEE9AFAC0E8E407CC9A0601D7E3BD6907CDAC94D7_H
#define INTERSECTNODE_TEE9AFAC0E8E407CC9A0601D7E3BD6907CDAC94D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.IntersectNode
struct  IntersectNode_tEE9AFAC0E8E407CC9A0601D7E3BD6907CDAC94D7  : public RuntimeObject
{
public:
	// ClipperLib.TEdge ClipperLib.IntersectNode::Edge1
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * ___Edge1_0;
	// ClipperLib.TEdge ClipperLib.IntersectNode::Edge2
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * ___Edge2_1;
	// ClipperLib.IntPoint ClipperLib.IntersectNode::Pt
	IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB  ___Pt_2;

public:
	inline static int32_t get_offset_of_Edge1_0() { return static_cast<int32_t>(offsetof(IntersectNode_tEE9AFAC0E8E407CC9A0601D7E3BD6907CDAC94D7, ___Edge1_0)); }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * get_Edge1_0() const { return ___Edge1_0; }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 ** get_address_of_Edge1_0() { return &___Edge1_0; }
	inline void set_Edge1_0(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * value)
	{
		___Edge1_0 = value;
		Il2CppCodeGenWriteBarrier((&___Edge1_0), value);
	}

	inline static int32_t get_offset_of_Edge2_1() { return static_cast<int32_t>(offsetof(IntersectNode_tEE9AFAC0E8E407CC9A0601D7E3BD6907CDAC94D7, ___Edge2_1)); }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * get_Edge2_1() const { return ___Edge2_1; }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 ** get_address_of_Edge2_1() { return &___Edge2_1; }
	inline void set_Edge2_1(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * value)
	{
		___Edge2_1 = value;
		Il2CppCodeGenWriteBarrier((&___Edge2_1), value);
	}

	inline static int32_t get_offset_of_Pt_2() { return static_cast<int32_t>(offsetof(IntersectNode_tEE9AFAC0E8E407CC9A0601D7E3BD6907CDAC94D7, ___Pt_2)); }
	inline IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB  get_Pt_2() const { return ___Pt_2; }
	inline IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB * get_address_of_Pt_2() { return &___Pt_2; }
	inline void set_Pt_2(IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB  value)
	{
		___Pt_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERSECTNODE_TEE9AFAC0E8E407CC9A0601D7E3BD6907CDAC94D7_H
#ifndef JOIN_T06D45E32B54B183FB11898114F5C4ACFE43D18AA_H
#define JOIN_T06D45E32B54B183FB11898114F5C4ACFE43D18AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.Join
struct  Join_t06D45E32B54B183FB11898114F5C4ACFE43D18AA  : public RuntimeObject
{
public:
	// ClipperLib.OutPt ClipperLib.Join::OutPt1
	OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0 * ___OutPt1_0;
	// ClipperLib.OutPt ClipperLib.Join::OutPt2
	OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0 * ___OutPt2_1;
	// ClipperLib.IntPoint ClipperLib.Join::OffPt
	IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB  ___OffPt_2;

public:
	inline static int32_t get_offset_of_OutPt1_0() { return static_cast<int32_t>(offsetof(Join_t06D45E32B54B183FB11898114F5C4ACFE43D18AA, ___OutPt1_0)); }
	inline OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0 * get_OutPt1_0() const { return ___OutPt1_0; }
	inline OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0 ** get_address_of_OutPt1_0() { return &___OutPt1_0; }
	inline void set_OutPt1_0(OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0 * value)
	{
		___OutPt1_0 = value;
		Il2CppCodeGenWriteBarrier((&___OutPt1_0), value);
	}

	inline static int32_t get_offset_of_OutPt2_1() { return static_cast<int32_t>(offsetof(Join_t06D45E32B54B183FB11898114F5C4ACFE43D18AA, ___OutPt2_1)); }
	inline OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0 * get_OutPt2_1() const { return ___OutPt2_1; }
	inline OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0 ** get_address_of_OutPt2_1() { return &___OutPt2_1; }
	inline void set_OutPt2_1(OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0 * value)
	{
		___OutPt2_1 = value;
		Il2CppCodeGenWriteBarrier((&___OutPt2_1), value);
	}

	inline static int32_t get_offset_of_OffPt_2() { return static_cast<int32_t>(offsetof(Join_t06D45E32B54B183FB11898114F5C4ACFE43D18AA, ___OffPt_2)); }
	inline IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB  get_OffPt_2() const { return ___OffPt_2; }
	inline IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB * get_address_of_OffPt_2() { return &___OffPt_2; }
	inline void set_OffPt_2(IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB  value)
	{
		___OffPt_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOIN_T06D45E32B54B183FB11898114F5C4ACFE43D18AA_H
#ifndef JOINTYPE_T2C94F67B2AC5A82B6C9F1E0A91FC5D592D54C6EE_H
#define JOINTYPE_T2C94F67B2AC5A82B6C9F1E0A91FC5D592D54C6EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.JoinType
struct  JoinType_t2C94F67B2AC5A82B6C9F1E0A91FC5D592D54C6EE 
{
public:
	// System.Int32 ClipperLib.JoinType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JoinType_t2C94F67B2AC5A82B6C9F1E0A91FC5D592D54C6EE, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINTYPE_T2C94F67B2AC5A82B6C9F1E0A91FC5D592D54C6EE_H
#ifndef OUTPT_TE6765AB6CADCB00B2D65E07B470AD9FE451522F0_H
#define OUTPT_TE6765AB6CADCB00B2D65E07B470AD9FE451522F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.OutPt
struct  OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0  : public RuntimeObject
{
public:
	// System.Int32 ClipperLib.OutPt::Idx
	int32_t ___Idx_0;
	// ClipperLib.IntPoint ClipperLib.OutPt::Pt
	IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB  ___Pt_1;
	// ClipperLib.OutPt ClipperLib.OutPt::Next
	OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0 * ___Next_2;
	// ClipperLib.OutPt ClipperLib.OutPt::Prev
	OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0 * ___Prev_3;

public:
	inline static int32_t get_offset_of_Idx_0() { return static_cast<int32_t>(offsetof(OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0, ___Idx_0)); }
	inline int32_t get_Idx_0() const { return ___Idx_0; }
	inline int32_t* get_address_of_Idx_0() { return &___Idx_0; }
	inline void set_Idx_0(int32_t value)
	{
		___Idx_0 = value;
	}

	inline static int32_t get_offset_of_Pt_1() { return static_cast<int32_t>(offsetof(OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0, ___Pt_1)); }
	inline IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB  get_Pt_1() const { return ___Pt_1; }
	inline IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB * get_address_of_Pt_1() { return &___Pt_1; }
	inline void set_Pt_1(IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB  value)
	{
		___Pt_1 = value;
	}

	inline static int32_t get_offset_of_Next_2() { return static_cast<int32_t>(offsetof(OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0, ___Next_2)); }
	inline OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0 * get_Next_2() const { return ___Next_2; }
	inline OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0 ** get_address_of_Next_2() { return &___Next_2; }
	inline void set_Next_2(OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0 * value)
	{
		___Next_2 = value;
		Il2CppCodeGenWriteBarrier((&___Next_2), value);
	}

	inline static int32_t get_offset_of_Prev_3() { return static_cast<int32_t>(offsetof(OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0, ___Prev_3)); }
	inline OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0 * get_Prev_3() const { return ___Prev_3; }
	inline OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0 ** get_address_of_Prev_3() { return &___Prev_3; }
	inline void set_Prev_3(OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0 * value)
	{
		___Prev_3 = value;
		Il2CppCodeGenWriteBarrier((&___Prev_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTPT_TE6765AB6CADCB00B2D65E07B470AD9FE451522F0_H
#ifndef POLYFILLTYPE_T2EDC6B8C9F60F9513818AA9D517973836E0623B7_H
#define POLYFILLTYPE_T2EDC6B8C9F60F9513818AA9D517973836E0623B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.PolyFillType
struct  PolyFillType_t2EDC6B8C9F60F9513818AA9D517973836E0623B7 
{
public:
	// System.Int32 ClipperLib.PolyFillType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PolyFillType_t2EDC6B8C9F60F9513818AA9D517973836E0623B7, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYFILLTYPE_T2EDC6B8C9F60F9513818AA9D517973836E0623B7_H
#ifndef POLYTYPE_T3DE7C2E2777C89C827976C47D2E9E97A5F761AAB_H
#define POLYTYPE_T3DE7C2E2777C89C827976C47D2E9E97A5F761AAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.PolyType
struct  PolyType_t3DE7C2E2777C89C827976C47D2E9E97A5F761AAB 
{
public:
	// System.Int32 ClipperLib.PolyType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PolyType_t3DE7C2E2777C89C827976C47D2E9E97A5F761AAB, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYTYPE_T3DE7C2E2777C89C827976C47D2E9E97A5F761AAB_H
#ifndef SPLINEWALKERMODE_T33BC6B2D75BA1E2A80FC5EA299FB63258ED3FD63_H
#define SPLINEWALKERMODE_T33BC6B2D75BA1E2A80FC5EA299FB63258ED3FD63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineWalker/SplineWalkerMode
struct  SplineWalkerMode_t33BC6B2D75BA1E2A80FC5EA299FB63258ED3FD63 
{
public:
	// System.Int32 Kolibri2d.SplineWalker/SplineWalkerMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SplineWalkerMode_t33BC6B2D75BA1E2A80FC5EA299FB63258ED3FD63, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINEWALKERMODE_T33BC6B2D75BA1E2A80FC5EA299FB63258ED3FD63_H
#ifndef DTSWEEPCONSTRAINT_T8C5C973CF68522DA5D7828F45161FE7A22203819_H
#define DTSWEEPCONSTRAINT_T8C5C973CF68522DA5D7828F45161FE7A22203819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.DTSweepConstraint
struct  DTSweepConstraint_t8C5C973CF68522DA5D7828F45161FE7A22203819  : public TriangulationConstraint_t420CE8B0E6C488B8F08766FBC3E6FAC5E5D6EC56
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTSWEEPCONSTRAINT_T8C5C973CF68522DA5D7828F45161FE7A22203819_H
#ifndef DELAUNAYTRIANGLE_TE26AE625198E80D12E2C9BFAE4A5C39CE8E86891_H
#define DELAUNAYTRIANGLE_TE26AE625198E80D12E2C9BFAE4A5C39CE8E86891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.DelaunayTriangle
struct  DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891  : public RuntimeObject
{
public:
	// Poly2Tri.FixedArray3`1<Poly2Tri.TriangulationPoint> Poly2Tri.DelaunayTriangle::Points
	FixedArray3_1_t0F7FC5C3079E0A6D350ACEC061B23ECAB8EFADA4  ___Points_0;
	// Poly2Tri.FixedArray3`1<Poly2Tri.DelaunayTriangle> Poly2Tri.DelaunayTriangle::Neighbors
	FixedArray3_1_t0825630D0BE588202967D839253E872C4CBCC88B  ___Neighbors_1;
	// Poly2Tri.FixedBitArray3 Poly2Tri.DelaunayTriangle::mEdgeIsConstrained
	FixedBitArray3_tB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB  ___mEdgeIsConstrained_2;
	// Poly2Tri.FixedBitArray3 Poly2Tri.DelaunayTriangle::EdgeIsDelaunay
	FixedBitArray3_tB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB  ___EdgeIsDelaunay_3;
	// System.Boolean Poly2Tri.DelaunayTriangle::<IsInterior>k__BackingField
	bool ___U3CIsInteriorU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_Points_0() { return static_cast<int32_t>(offsetof(DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891, ___Points_0)); }
	inline FixedArray3_1_t0F7FC5C3079E0A6D350ACEC061B23ECAB8EFADA4  get_Points_0() const { return ___Points_0; }
	inline FixedArray3_1_t0F7FC5C3079E0A6D350ACEC061B23ECAB8EFADA4 * get_address_of_Points_0() { return &___Points_0; }
	inline void set_Points_0(FixedArray3_1_t0F7FC5C3079E0A6D350ACEC061B23ECAB8EFADA4  value)
	{
		___Points_0 = value;
	}

	inline static int32_t get_offset_of_Neighbors_1() { return static_cast<int32_t>(offsetof(DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891, ___Neighbors_1)); }
	inline FixedArray3_1_t0825630D0BE588202967D839253E872C4CBCC88B  get_Neighbors_1() const { return ___Neighbors_1; }
	inline FixedArray3_1_t0825630D0BE588202967D839253E872C4CBCC88B * get_address_of_Neighbors_1() { return &___Neighbors_1; }
	inline void set_Neighbors_1(FixedArray3_1_t0825630D0BE588202967D839253E872C4CBCC88B  value)
	{
		___Neighbors_1 = value;
	}

	inline static int32_t get_offset_of_mEdgeIsConstrained_2() { return static_cast<int32_t>(offsetof(DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891, ___mEdgeIsConstrained_2)); }
	inline FixedBitArray3_tB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB  get_mEdgeIsConstrained_2() const { return ___mEdgeIsConstrained_2; }
	inline FixedBitArray3_tB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB * get_address_of_mEdgeIsConstrained_2() { return &___mEdgeIsConstrained_2; }
	inline void set_mEdgeIsConstrained_2(FixedBitArray3_tB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB  value)
	{
		___mEdgeIsConstrained_2 = value;
	}

	inline static int32_t get_offset_of_EdgeIsDelaunay_3() { return static_cast<int32_t>(offsetof(DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891, ___EdgeIsDelaunay_3)); }
	inline FixedBitArray3_tB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB  get_EdgeIsDelaunay_3() const { return ___EdgeIsDelaunay_3; }
	inline FixedBitArray3_tB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB * get_address_of_EdgeIsDelaunay_3() { return &___EdgeIsDelaunay_3; }
	inline void set_EdgeIsDelaunay_3(FixedBitArray3_tB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB  value)
	{
		___EdgeIsDelaunay_3 = value;
	}

	inline static int32_t get_offset_of_U3CIsInteriorU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891, ___U3CIsInteriorU3Ek__BackingField_4)); }
	inline bool get_U3CIsInteriorU3Ek__BackingField_4() const { return ___U3CIsInteriorU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsInteriorU3Ek__BackingField_4() { return &___U3CIsInteriorU3Ek__BackingField_4; }
	inline void set_U3CIsInteriorU3Ek__BackingField_4(bool value)
	{
		___U3CIsInteriorU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELAUNAYTRIANGLE_TE26AE625198E80D12E2C9BFAE4A5C39CE8E86891_H
#ifndef U3CENUMERATEU3EC__ITERATOR0_TD1CF558864545C490E7676298C409E31FCBC6302_H
#define U3CENUMERATEU3EC__ITERATOR0_TD1CF558864545C490E7676298C409E31FCBC6302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.FixedBitArray3/<Enumerate>c__Iterator0
struct  U3CEnumerateU3Ec__Iterator0_tD1CF558864545C490E7676298C409E31FCBC6302  : public RuntimeObject
{
public:
	// System.Int32 Poly2Tri.FixedBitArray3/<Enumerate>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// Poly2Tri.FixedBitArray3 Poly2Tri.FixedBitArray3/<Enumerate>c__Iterator0::$this
	FixedBitArray3_tB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB  ___U24this_1;
	// System.Boolean Poly2Tri.FixedBitArray3/<Enumerate>c__Iterator0::$current
	bool ___U24current_2;
	// System.Boolean Poly2Tri.FixedBitArray3/<Enumerate>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Poly2Tri.FixedBitArray3/<Enumerate>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CEnumerateU3Ec__Iterator0_tD1CF558864545C490E7676298C409E31FCBC6302, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CEnumerateU3Ec__Iterator0_tD1CF558864545C490E7676298C409E31FCBC6302, ___U24this_1)); }
	inline FixedBitArray3_tB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB  get_U24this_1() const { return ___U24this_1; }
	inline FixedBitArray3_tB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB * get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(FixedBitArray3_tB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB  value)
	{
		___U24this_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CEnumerateU3Ec__Iterator0_tD1CF558864545C490E7676298C409E31FCBC6302, ___U24current_2)); }
	inline bool get_U24current_2() const { return ___U24current_2; }
	inline bool* get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(bool value)
	{
		___U24current_2 = value;
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CEnumerateU3Ec__Iterator0_tD1CF558864545C490E7676298C409E31FCBC6302, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CEnumerateU3Ec__Iterator0_tD1CF558864545C490E7676298C409E31FCBC6302, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CENUMERATEU3EC__ITERATOR0_TD1CF558864545C490E7676298C409E31FCBC6302_H
#ifndef ORIENTATION_T30EEBFB4BCE8E9AE35551FCD92D698CA8A2A6ADC_H
#define ORIENTATION_T30EEBFB4BCE8E9AE35551FCD92D698CA8A2A6ADC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.Orientation
struct  Orientation_t30EEBFB4BCE8E9AE35551FCD92D698CA8A2A6ADC 
{
public:
	// System.Int32 Poly2Tri.Orientation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Orientation_t30EEBFB4BCE8E9AE35551FCD92D698CA8A2A6ADC, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTATION_T30EEBFB4BCE8E9AE35551FCD92D698CA8A2A6ADC_H
#ifndef POLYGONERROR_T7B4ACD61D08175DFF8B72E05C1DD743FFF8D3D1D_H
#define POLYGONERROR_T7B4ACD61D08175DFF8B72E05C1DD743FFF8D3D1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.Point2DList/PolygonError
struct  PolygonError_t7B4ACD61D08175DFF8B72E05C1DD743FFF8D3D1D 
{
public:
	// System.UInt32 Poly2Tri.Point2DList/PolygonError::value__
	uint32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PolygonError_t7B4ACD61D08175DFF8B72E05C1DD743FFF8D3D1D, ___value___1)); }
	inline uint32_t get_value___1() const { return ___value___1; }
	inline uint32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGONERROR_T7B4ACD61D08175DFF8B72E05C1DD743FFF8D3D1D_H
#ifndef WINDINGORDERTYPE_T48C8C779542915FBAAC9F555D87454615A9D01C3_H
#define WINDINGORDERTYPE_T48C8C779542915FBAAC9F555D87454615A9D01C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.Point2DList/WindingOrderType
struct  WindingOrderType_t48C8C779542915FBAAC9F555D87454615A9D01C3 
{
public:
	// System.Int32 Poly2Tri.Point2DList/WindingOrderType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WindingOrderType_t48C8C779542915FBAAC9F555D87454615A9D01C3, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDINGORDERTYPE_T48C8C779542915FBAAC9F555D87454615A9D01C3_H
#ifndef POLYGONPOINT_TAED3172793E804A09A2EB2FEDCB0EBA5159A08FE_H
#define POLYGONPOINT_TAED3172793E804A09A2EB2FEDCB0EBA5159A08FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.PolygonPoint
struct  PolygonPoint_tAED3172793E804A09A2EB2FEDCB0EBA5159A08FE  : public TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4
{
public:
	// Poly2Tri.PolygonPoint Poly2Tri.PolygonPoint::<Next>k__BackingField
	PolygonPoint_tAED3172793E804A09A2EB2FEDCB0EBA5159A08FE * ___U3CNextU3Ek__BackingField_5;
	// Poly2Tri.PolygonPoint Poly2Tri.PolygonPoint::<Previous>k__BackingField
	PolygonPoint_tAED3172793E804A09A2EB2FEDCB0EBA5159A08FE * ___U3CPreviousU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CNextU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PolygonPoint_tAED3172793E804A09A2EB2FEDCB0EBA5159A08FE, ___U3CNextU3Ek__BackingField_5)); }
	inline PolygonPoint_tAED3172793E804A09A2EB2FEDCB0EBA5159A08FE * get_U3CNextU3Ek__BackingField_5() const { return ___U3CNextU3Ek__BackingField_5; }
	inline PolygonPoint_tAED3172793E804A09A2EB2FEDCB0EBA5159A08FE ** get_address_of_U3CNextU3Ek__BackingField_5() { return &___U3CNextU3Ek__BackingField_5; }
	inline void set_U3CNextU3Ek__BackingField_5(PolygonPoint_tAED3172793E804A09A2EB2FEDCB0EBA5159A08FE * value)
	{
		___U3CNextU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNextU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CPreviousU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PolygonPoint_tAED3172793E804A09A2EB2FEDCB0EBA5159A08FE, ___U3CPreviousU3Ek__BackingField_6)); }
	inline PolygonPoint_tAED3172793E804A09A2EB2FEDCB0EBA5159A08FE * get_U3CPreviousU3Ek__BackingField_6() const { return ___U3CPreviousU3Ek__BackingField_6; }
	inline PolygonPoint_tAED3172793E804A09A2EB2FEDCB0EBA5159A08FE ** get_address_of_U3CPreviousU3Ek__BackingField_6() { return &___U3CPreviousU3Ek__BackingField_6; }
	inline void set_U3CPreviousU3Ek__BackingField_6(PolygonPoint_tAED3172793E804A09A2EB2FEDCB0EBA5159A08FE * value)
	{
		___U3CPreviousU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPreviousU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGONPOINT_TAED3172793E804A09A2EB2FEDCB0EBA5159A08FE_H
#ifndef POLYOPERATION_T5DF9AFFF3649735ED0CB9E58180D6DBC69B798E8_H
#define POLYOPERATION_T5DF9AFFF3649735ED0CB9E58180D6DBC69B798E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.PolygonUtil/PolyOperation
struct  PolyOperation_t5DF9AFFF3649735ED0CB9E58180D6DBC69B798E8 
{
public:
	// System.UInt32 Poly2Tri.PolygonUtil/PolyOperation::value__
	uint32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PolyOperation_t5DF9AFFF3649735ED0CB9E58180D6DBC69B798E8, ___value___1)); }
	inline uint32_t get_value___1() const { return ___value___1; }
	inline uint32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYOPERATION_T5DF9AFFF3649735ED0CB9E58180D6DBC69B798E8_H
#ifndef POLYUNIONERROR_TCAD15B7CD739741931652066204CC031F2030830_H
#define POLYUNIONERROR_TCAD15B7CD739741931652066204CC031F2030830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.PolygonUtil/PolyUnionError
struct  PolyUnionError_tCAD15B7CD739741931652066204CC031F2030830 
{
public:
	// System.Int32 Poly2Tri.PolygonUtil/PolyUnionError::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PolyUnionError_tCAD15B7CD739741931652066204CC031F2030830, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYUNIONERROR_TCAD15B7CD739741931652066204CC031F2030830_H
#ifndef TRIANGULATIONALGORITHM_T163C57AE860101458013AB67507A951DDCC64358_H
#define TRIANGULATIONALGORITHM_T163C57AE860101458013AB67507A951DDCC64358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.TriangulationAlgorithm
struct  TriangulationAlgorithm_t163C57AE860101458013AB67507A951DDCC64358 
{
public:
	// System.Int32 Poly2Tri.TriangulationAlgorithm::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriangulationAlgorithm_t163C57AE860101458013AB67507A951DDCC64358, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATIONALGORITHM_T163C57AE860101458013AB67507A951DDCC64358_H
#ifndef TRIANGULATIONMODE_T17461F5B4793A10FA487438CB6D800E8E2A71364_H
#define TRIANGULATIONMODE_T17461F5B4793A10FA487438CB6D800E8E2A71364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.TriangulationMode
struct  TriangulationMode_t17461F5B4793A10FA487438CB6D800E8E2A71364 
{
public:
	// System.Int32 Poly2Tri.TriangulationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriangulationMode_t17461F5B4793A10FA487438CB6D800E8E2A71364, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATIONMODE_T17461F5B4793A10FA487438CB6D800E8E2A71364_H
#ifndef LOGLEVEL_TED853AD350810F77C640D354230906298D398A41_H
#define LOGLEVEL_TED853AD350810F77C640D354230906298D398A41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.LogLevel
struct  LogLevel_tED853AD350810F77C640D354230906298D398A41 
{
public:
	// System.Int32 SimplySVG.LogLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogLevel_tED853AD350810F77C640D354230906298D398A41, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGLEVEL_TED853AD350810F77C640D354230906298D398A41_H
#ifndef TRANSFORMCOMMAND_T601BE25036B48F21884351AC0F5D1F039FC829E2_H
#define TRANSFORMCOMMAND_T601BE25036B48F21884351AC0F5D1F039FC829E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.TransformAttributes/TransformCommand
struct  TransformCommand_t601BE25036B48F21884351AC0F5D1F039FC829E2 
{
public:
	// System.Int32 SimplySVG.TransformAttributes/TransformCommand::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TransformCommand_t601BE25036B48F21884351AC0F5D1F039FC829E2, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMCOMMAND_T601BE25036B48F21884351AC0F5D1F039FC829E2_H
#ifndef NOTIMPLEMENTEDEXCEPTION_TAF5A82631683648991DEF6809680B0DF7A4A4767_H
#define NOTIMPLEMENTEDEXCEPTION_TAF5A82631683648991DEF6809680B0DF7A4A4767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotImplementedException
struct  NotImplementedException_tAF5A82631683648991DEF6809680B0DF7A4A4767  : public SystemException_t81A06ADD519539D252788D75E44AFFBE6580E301
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIMPLEMENTEDEXCEPTION_TAF5A82631683648991DEF6809680B0DF7A4A4767_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef CLIPPER_T673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F_H
#define CLIPPER_T673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.Clipper
struct  Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F  : public ClipperBase_tBFEE1941825FBF171B1F5EDB6DFC3247136BC4DD
{
public:
	// System.Collections.Generic.List`1<ClipperLib.OutRec> ClipperLib.Clipper::m_PolyOuts
	List_1_tBA88214EB9561433F89AC6CF58821F21467839EC * ___m_PolyOuts_15;
	// ClipperLib.ClipType ClipperLib.Clipper::m_ClipType
	int32_t ___m_ClipType_16;
	// ClipperLib.Scanbeam ClipperLib.Clipper::m_Scanbeam
	Scanbeam_t5B92102C6C777A0213DB5CB42DCBE782F0486AA7 * ___m_Scanbeam_17;
	// ClipperLib.TEdge ClipperLib.Clipper::m_ActiveEdges
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * ___m_ActiveEdges_18;
	// ClipperLib.TEdge ClipperLib.Clipper::m_SortedEdges
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * ___m_SortedEdges_19;
	// System.Collections.Generic.List`1<ClipperLib.IntersectNode> ClipperLib.Clipper::m_IntersectList
	List_1_tBD9A00829C87F33D52CDBA97917C242F86F197DD * ___m_IntersectList_20;
	// System.Collections.Generic.IComparer`1<ClipperLib.IntersectNode> ClipperLib.Clipper::m_IntersectNodeComparer
	RuntimeObject* ___m_IntersectNodeComparer_21;
	// System.Boolean ClipperLib.Clipper::m_ExecuteLocked
	bool ___m_ExecuteLocked_22;
	// ClipperLib.PolyFillType ClipperLib.Clipper::m_ClipFillType
	int32_t ___m_ClipFillType_23;
	// ClipperLib.PolyFillType ClipperLib.Clipper::m_SubjFillType
	int32_t ___m_SubjFillType_24;
	// System.Collections.Generic.List`1<ClipperLib.Join> ClipperLib.Clipper::m_Joins
	List_1_t86786B66D45FFA0F79F48798DD1F2D26FBA2B49D * ___m_Joins_25;
	// System.Collections.Generic.List`1<ClipperLib.Join> ClipperLib.Clipper::m_GhostJoins
	List_1_t86786B66D45FFA0F79F48798DD1F2D26FBA2B49D * ___m_GhostJoins_26;
	// System.Boolean ClipperLib.Clipper::m_UsingPolyTree
	bool ___m_UsingPolyTree_27;
	// System.Boolean ClipperLib.Clipper::<ReverseSolution>k__BackingField
	bool ___U3CReverseSolutionU3Ek__BackingField_28;
	// System.Boolean ClipperLib.Clipper::<StrictlySimple>k__BackingField
	bool ___U3CStrictlySimpleU3Ek__BackingField_29;

public:
	inline static int32_t get_offset_of_m_PolyOuts_15() { return static_cast<int32_t>(offsetof(Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F, ___m_PolyOuts_15)); }
	inline List_1_tBA88214EB9561433F89AC6CF58821F21467839EC * get_m_PolyOuts_15() const { return ___m_PolyOuts_15; }
	inline List_1_tBA88214EB9561433F89AC6CF58821F21467839EC ** get_address_of_m_PolyOuts_15() { return &___m_PolyOuts_15; }
	inline void set_m_PolyOuts_15(List_1_tBA88214EB9561433F89AC6CF58821F21467839EC * value)
	{
		___m_PolyOuts_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_PolyOuts_15), value);
	}

	inline static int32_t get_offset_of_m_ClipType_16() { return static_cast<int32_t>(offsetof(Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F, ___m_ClipType_16)); }
	inline int32_t get_m_ClipType_16() const { return ___m_ClipType_16; }
	inline int32_t* get_address_of_m_ClipType_16() { return &___m_ClipType_16; }
	inline void set_m_ClipType_16(int32_t value)
	{
		___m_ClipType_16 = value;
	}

	inline static int32_t get_offset_of_m_Scanbeam_17() { return static_cast<int32_t>(offsetof(Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F, ___m_Scanbeam_17)); }
	inline Scanbeam_t5B92102C6C777A0213DB5CB42DCBE782F0486AA7 * get_m_Scanbeam_17() const { return ___m_Scanbeam_17; }
	inline Scanbeam_t5B92102C6C777A0213DB5CB42DCBE782F0486AA7 ** get_address_of_m_Scanbeam_17() { return &___m_Scanbeam_17; }
	inline void set_m_Scanbeam_17(Scanbeam_t5B92102C6C777A0213DB5CB42DCBE782F0486AA7 * value)
	{
		___m_Scanbeam_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_Scanbeam_17), value);
	}

	inline static int32_t get_offset_of_m_ActiveEdges_18() { return static_cast<int32_t>(offsetof(Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F, ___m_ActiveEdges_18)); }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * get_m_ActiveEdges_18() const { return ___m_ActiveEdges_18; }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 ** get_address_of_m_ActiveEdges_18() { return &___m_ActiveEdges_18; }
	inline void set_m_ActiveEdges_18(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * value)
	{
		___m_ActiveEdges_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActiveEdges_18), value);
	}

	inline static int32_t get_offset_of_m_SortedEdges_19() { return static_cast<int32_t>(offsetof(Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F, ___m_SortedEdges_19)); }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * get_m_SortedEdges_19() const { return ___m_SortedEdges_19; }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 ** get_address_of_m_SortedEdges_19() { return &___m_SortedEdges_19; }
	inline void set_m_SortedEdges_19(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * value)
	{
		___m_SortedEdges_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_SortedEdges_19), value);
	}

	inline static int32_t get_offset_of_m_IntersectList_20() { return static_cast<int32_t>(offsetof(Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F, ___m_IntersectList_20)); }
	inline List_1_tBD9A00829C87F33D52CDBA97917C242F86F197DD * get_m_IntersectList_20() const { return ___m_IntersectList_20; }
	inline List_1_tBD9A00829C87F33D52CDBA97917C242F86F197DD ** get_address_of_m_IntersectList_20() { return &___m_IntersectList_20; }
	inline void set_m_IntersectList_20(List_1_tBD9A00829C87F33D52CDBA97917C242F86F197DD * value)
	{
		___m_IntersectList_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_IntersectList_20), value);
	}

	inline static int32_t get_offset_of_m_IntersectNodeComparer_21() { return static_cast<int32_t>(offsetof(Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F, ___m_IntersectNodeComparer_21)); }
	inline RuntimeObject* get_m_IntersectNodeComparer_21() const { return ___m_IntersectNodeComparer_21; }
	inline RuntimeObject** get_address_of_m_IntersectNodeComparer_21() { return &___m_IntersectNodeComparer_21; }
	inline void set_m_IntersectNodeComparer_21(RuntimeObject* value)
	{
		___m_IntersectNodeComparer_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_IntersectNodeComparer_21), value);
	}

	inline static int32_t get_offset_of_m_ExecuteLocked_22() { return static_cast<int32_t>(offsetof(Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F, ___m_ExecuteLocked_22)); }
	inline bool get_m_ExecuteLocked_22() const { return ___m_ExecuteLocked_22; }
	inline bool* get_address_of_m_ExecuteLocked_22() { return &___m_ExecuteLocked_22; }
	inline void set_m_ExecuteLocked_22(bool value)
	{
		___m_ExecuteLocked_22 = value;
	}

	inline static int32_t get_offset_of_m_ClipFillType_23() { return static_cast<int32_t>(offsetof(Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F, ___m_ClipFillType_23)); }
	inline int32_t get_m_ClipFillType_23() const { return ___m_ClipFillType_23; }
	inline int32_t* get_address_of_m_ClipFillType_23() { return &___m_ClipFillType_23; }
	inline void set_m_ClipFillType_23(int32_t value)
	{
		___m_ClipFillType_23 = value;
	}

	inline static int32_t get_offset_of_m_SubjFillType_24() { return static_cast<int32_t>(offsetof(Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F, ___m_SubjFillType_24)); }
	inline int32_t get_m_SubjFillType_24() const { return ___m_SubjFillType_24; }
	inline int32_t* get_address_of_m_SubjFillType_24() { return &___m_SubjFillType_24; }
	inline void set_m_SubjFillType_24(int32_t value)
	{
		___m_SubjFillType_24 = value;
	}

	inline static int32_t get_offset_of_m_Joins_25() { return static_cast<int32_t>(offsetof(Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F, ___m_Joins_25)); }
	inline List_1_t86786B66D45FFA0F79F48798DD1F2D26FBA2B49D * get_m_Joins_25() const { return ___m_Joins_25; }
	inline List_1_t86786B66D45FFA0F79F48798DD1F2D26FBA2B49D ** get_address_of_m_Joins_25() { return &___m_Joins_25; }
	inline void set_m_Joins_25(List_1_t86786B66D45FFA0F79F48798DD1F2D26FBA2B49D * value)
	{
		___m_Joins_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_Joins_25), value);
	}

	inline static int32_t get_offset_of_m_GhostJoins_26() { return static_cast<int32_t>(offsetof(Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F, ___m_GhostJoins_26)); }
	inline List_1_t86786B66D45FFA0F79F48798DD1F2D26FBA2B49D * get_m_GhostJoins_26() const { return ___m_GhostJoins_26; }
	inline List_1_t86786B66D45FFA0F79F48798DD1F2D26FBA2B49D ** get_address_of_m_GhostJoins_26() { return &___m_GhostJoins_26; }
	inline void set_m_GhostJoins_26(List_1_t86786B66D45FFA0F79F48798DD1F2D26FBA2B49D * value)
	{
		___m_GhostJoins_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_GhostJoins_26), value);
	}

	inline static int32_t get_offset_of_m_UsingPolyTree_27() { return static_cast<int32_t>(offsetof(Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F, ___m_UsingPolyTree_27)); }
	inline bool get_m_UsingPolyTree_27() const { return ___m_UsingPolyTree_27; }
	inline bool* get_address_of_m_UsingPolyTree_27() { return &___m_UsingPolyTree_27; }
	inline void set_m_UsingPolyTree_27(bool value)
	{
		___m_UsingPolyTree_27 = value;
	}

	inline static int32_t get_offset_of_U3CReverseSolutionU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F, ___U3CReverseSolutionU3Ek__BackingField_28)); }
	inline bool get_U3CReverseSolutionU3Ek__BackingField_28() const { return ___U3CReverseSolutionU3Ek__BackingField_28; }
	inline bool* get_address_of_U3CReverseSolutionU3Ek__BackingField_28() { return &___U3CReverseSolutionU3Ek__BackingField_28; }
	inline void set_U3CReverseSolutionU3Ek__BackingField_28(bool value)
	{
		___U3CReverseSolutionU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_U3CStrictlySimpleU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F, ___U3CStrictlySimpleU3Ek__BackingField_29)); }
	inline bool get_U3CStrictlySimpleU3Ek__BackingField_29() const { return ___U3CStrictlySimpleU3Ek__BackingField_29; }
	inline bool* get_address_of_U3CStrictlySimpleU3Ek__BackingField_29() { return &___U3CStrictlySimpleU3Ek__BackingField_29; }
	inline void set_U3CStrictlySimpleU3Ek__BackingField_29(bool value)
	{
		___U3CStrictlySimpleU3Ek__BackingField_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPER_T673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F_H
#ifndef POLYNODE_TEF7F0D00AB30829F935C4B6889A5C1BC79D654CB_H
#define POLYNODE_TEF7F0D00AB30829F935C4B6889A5C1BC79D654CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.PolyNode
struct  PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB  : public RuntimeObject
{
public:
	// ClipperLib.PolyNode ClipperLib.PolyNode::m_Parent
	PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB * ___m_Parent_0;
	// System.Collections.Generic.List`1<ClipperLib.IntPoint> ClipperLib.PolyNode::m_polygon
	List_1_tD323E55C055A7CA7984664415A9E8332BB24408B * ___m_polygon_1;
	// System.Int32 ClipperLib.PolyNode::m_Index
	int32_t ___m_Index_2;
	// ClipperLib.JoinType ClipperLib.PolyNode::m_jointype
	int32_t ___m_jointype_3;
	// ClipperLib.EndType ClipperLib.PolyNode::m_endtype
	int32_t ___m_endtype_4;
	// System.Collections.Generic.List`1<ClipperLib.PolyNode> ClipperLib.PolyNode::m_Childs
	List_1_tAFCC6AECDB60026588FFF5817A80B9CBDB4F349F * ___m_Childs_5;
	// System.Boolean ClipperLib.PolyNode::<IsOpen>k__BackingField
	bool ___U3CIsOpenU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_m_Parent_0() { return static_cast<int32_t>(offsetof(PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB, ___m_Parent_0)); }
	inline PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB * get_m_Parent_0() const { return ___m_Parent_0; }
	inline PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB ** get_address_of_m_Parent_0() { return &___m_Parent_0; }
	inline void set_m_Parent_0(PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB * value)
	{
		___m_Parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parent_0), value);
	}

	inline static int32_t get_offset_of_m_polygon_1() { return static_cast<int32_t>(offsetof(PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB, ___m_polygon_1)); }
	inline List_1_tD323E55C055A7CA7984664415A9E8332BB24408B * get_m_polygon_1() const { return ___m_polygon_1; }
	inline List_1_tD323E55C055A7CA7984664415A9E8332BB24408B ** get_address_of_m_polygon_1() { return &___m_polygon_1; }
	inline void set_m_polygon_1(List_1_tD323E55C055A7CA7984664415A9E8332BB24408B * value)
	{
		___m_polygon_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_polygon_1), value);
	}

	inline static int32_t get_offset_of_m_Index_2() { return static_cast<int32_t>(offsetof(PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB, ___m_Index_2)); }
	inline int32_t get_m_Index_2() const { return ___m_Index_2; }
	inline int32_t* get_address_of_m_Index_2() { return &___m_Index_2; }
	inline void set_m_Index_2(int32_t value)
	{
		___m_Index_2 = value;
	}

	inline static int32_t get_offset_of_m_jointype_3() { return static_cast<int32_t>(offsetof(PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB, ___m_jointype_3)); }
	inline int32_t get_m_jointype_3() const { return ___m_jointype_3; }
	inline int32_t* get_address_of_m_jointype_3() { return &___m_jointype_3; }
	inline void set_m_jointype_3(int32_t value)
	{
		___m_jointype_3 = value;
	}

	inline static int32_t get_offset_of_m_endtype_4() { return static_cast<int32_t>(offsetof(PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB, ___m_endtype_4)); }
	inline int32_t get_m_endtype_4() const { return ___m_endtype_4; }
	inline int32_t* get_address_of_m_endtype_4() { return &___m_endtype_4; }
	inline void set_m_endtype_4(int32_t value)
	{
		___m_endtype_4 = value;
	}

	inline static int32_t get_offset_of_m_Childs_5() { return static_cast<int32_t>(offsetof(PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB, ___m_Childs_5)); }
	inline List_1_tAFCC6AECDB60026588FFF5817A80B9CBDB4F349F * get_m_Childs_5() const { return ___m_Childs_5; }
	inline List_1_tAFCC6AECDB60026588FFF5817A80B9CBDB4F349F ** get_address_of_m_Childs_5() { return &___m_Childs_5; }
	inline void set_m_Childs_5(List_1_tAFCC6AECDB60026588FFF5817A80B9CBDB4F349F * value)
	{
		___m_Childs_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Childs_5), value);
	}

	inline static int32_t get_offset_of_U3CIsOpenU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB, ___U3CIsOpenU3Ek__BackingField_6)); }
	inline bool get_U3CIsOpenU3Ek__BackingField_6() const { return ___U3CIsOpenU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CIsOpenU3Ek__BackingField_6() { return &___U3CIsOpenU3Ek__BackingField_6; }
	inline void set_U3CIsOpenU3Ek__BackingField_6(bool value)
	{
		___U3CIsOpenU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYNODE_TEF7F0D00AB30829F935C4B6889A5C1BC79D654CB_H
#ifndef TEDGE_TA9D6BA578B80A7A1EEF29EDC76C629D9802C0335_H
#define TEDGE_TA9D6BA578B80A7A1EEF29EDC76C629D9802C0335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.TEdge
struct  TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335  : public RuntimeObject
{
public:
	// ClipperLib.IntPoint ClipperLib.TEdge::Bot
	IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB  ___Bot_0;
	// ClipperLib.IntPoint ClipperLib.TEdge::Curr
	IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB  ___Curr_1;
	// ClipperLib.IntPoint ClipperLib.TEdge::Top
	IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB  ___Top_2;
	// ClipperLib.IntPoint ClipperLib.TEdge::Delta
	IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB  ___Delta_3;
	// System.Double ClipperLib.TEdge::Dx
	double ___Dx_4;
	// ClipperLib.PolyType ClipperLib.TEdge::PolyTyp
	int32_t ___PolyTyp_5;
	// ClipperLib.EdgeSide ClipperLib.TEdge::Side
	int32_t ___Side_6;
	// System.Int32 ClipperLib.TEdge::WindDelta
	int32_t ___WindDelta_7;
	// System.Int32 ClipperLib.TEdge::WindCnt
	int32_t ___WindCnt_8;
	// System.Int32 ClipperLib.TEdge::WindCnt2
	int32_t ___WindCnt2_9;
	// System.Int32 ClipperLib.TEdge::OutIdx
	int32_t ___OutIdx_10;
	// ClipperLib.TEdge ClipperLib.TEdge::Next
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * ___Next_11;
	// ClipperLib.TEdge ClipperLib.TEdge::Prev
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * ___Prev_12;
	// ClipperLib.TEdge ClipperLib.TEdge::NextInLML
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * ___NextInLML_13;
	// ClipperLib.TEdge ClipperLib.TEdge::NextInAEL
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * ___NextInAEL_14;
	// ClipperLib.TEdge ClipperLib.TEdge::PrevInAEL
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * ___PrevInAEL_15;
	// ClipperLib.TEdge ClipperLib.TEdge::NextInSEL
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * ___NextInSEL_16;
	// ClipperLib.TEdge ClipperLib.TEdge::PrevInSEL
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * ___PrevInSEL_17;

public:
	inline static int32_t get_offset_of_Bot_0() { return static_cast<int32_t>(offsetof(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335, ___Bot_0)); }
	inline IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB  get_Bot_0() const { return ___Bot_0; }
	inline IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB * get_address_of_Bot_0() { return &___Bot_0; }
	inline void set_Bot_0(IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB  value)
	{
		___Bot_0 = value;
	}

	inline static int32_t get_offset_of_Curr_1() { return static_cast<int32_t>(offsetof(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335, ___Curr_1)); }
	inline IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB  get_Curr_1() const { return ___Curr_1; }
	inline IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB * get_address_of_Curr_1() { return &___Curr_1; }
	inline void set_Curr_1(IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB  value)
	{
		___Curr_1 = value;
	}

	inline static int32_t get_offset_of_Top_2() { return static_cast<int32_t>(offsetof(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335, ___Top_2)); }
	inline IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB  get_Top_2() const { return ___Top_2; }
	inline IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB * get_address_of_Top_2() { return &___Top_2; }
	inline void set_Top_2(IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB  value)
	{
		___Top_2 = value;
	}

	inline static int32_t get_offset_of_Delta_3() { return static_cast<int32_t>(offsetof(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335, ___Delta_3)); }
	inline IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB  get_Delta_3() const { return ___Delta_3; }
	inline IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB * get_address_of_Delta_3() { return &___Delta_3; }
	inline void set_Delta_3(IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB  value)
	{
		___Delta_3 = value;
	}

	inline static int32_t get_offset_of_Dx_4() { return static_cast<int32_t>(offsetof(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335, ___Dx_4)); }
	inline double get_Dx_4() const { return ___Dx_4; }
	inline double* get_address_of_Dx_4() { return &___Dx_4; }
	inline void set_Dx_4(double value)
	{
		___Dx_4 = value;
	}

	inline static int32_t get_offset_of_PolyTyp_5() { return static_cast<int32_t>(offsetof(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335, ___PolyTyp_5)); }
	inline int32_t get_PolyTyp_5() const { return ___PolyTyp_5; }
	inline int32_t* get_address_of_PolyTyp_5() { return &___PolyTyp_5; }
	inline void set_PolyTyp_5(int32_t value)
	{
		___PolyTyp_5 = value;
	}

	inline static int32_t get_offset_of_Side_6() { return static_cast<int32_t>(offsetof(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335, ___Side_6)); }
	inline int32_t get_Side_6() const { return ___Side_6; }
	inline int32_t* get_address_of_Side_6() { return &___Side_6; }
	inline void set_Side_6(int32_t value)
	{
		___Side_6 = value;
	}

	inline static int32_t get_offset_of_WindDelta_7() { return static_cast<int32_t>(offsetof(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335, ___WindDelta_7)); }
	inline int32_t get_WindDelta_7() const { return ___WindDelta_7; }
	inline int32_t* get_address_of_WindDelta_7() { return &___WindDelta_7; }
	inline void set_WindDelta_7(int32_t value)
	{
		___WindDelta_7 = value;
	}

	inline static int32_t get_offset_of_WindCnt_8() { return static_cast<int32_t>(offsetof(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335, ___WindCnt_8)); }
	inline int32_t get_WindCnt_8() const { return ___WindCnt_8; }
	inline int32_t* get_address_of_WindCnt_8() { return &___WindCnt_8; }
	inline void set_WindCnt_8(int32_t value)
	{
		___WindCnt_8 = value;
	}

	inline static int32_t get_offset_of_WindCnt2_9() { return static_cast<int32_t>(offsetof(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335, ___WindCnt2_9)); }
	inline int32_t get_WindCnt2_9() const { return ___WindCnt2_9; }
	inline int32_t* get_address_of_WindCnt2_9() { return &___WindCnt2_9; }
	inline void set_WindCnt2_9(int32_t value)
	{
		___WindCnt2_9 = value;
	}

	inline static int32_t get_offset_of_OutIdx_10() { return static_cast<int32_t>(offsetof(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335, ___OutIdx_10)); }
	inline int32_t get_OutIdx_10() const { return ___OutIdx_10; }
	inline int32_t* get_address_of_OutIdx_10() { return &___OutIdx_10; }
	inline void set_OutIdx_10(int32_t value)
	{
		___OutIdx_10 = value;
	}

	inline static int32_t get_offset_of_Next_11() { return static_cast<int32_t>(offsetof(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335, ___Next_11)); }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * get_Next_11() const { return ___Next_11; }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 ** get_address_of_Next_11() { return &___Next_11; }
	inline void set_Next_11(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * value)
	{
		___Next_11 = value;
		Il2CppCodeGenWriteBarrier((&___Next_11), value);
	}

	inline static int32_t get_offset_of_Prev_12() { return static_cast<int32_t>(offsetof(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335, ___Prev_12)); }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * get_Prev_12() const { return ___Prev_12; }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 ** get_address_of_Prev_12() { return &___Prev_12; }
	inline void set_Prev_12(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * value)
	{
		___Prev_12 = value;
		Il2CppCodeGenWriteBarrier((&___Prev_12), value);
	}

	inline static int32_t get_offset_of_NextInLML_13() { return static_cast<int32_t>(offsetof(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335, ___NextInLML_13)); }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * get_NextInLML_13() const { return ___NextInLML_13; }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 ** get_address_of_NextInLML_13() { return &___NextInLML_13; }
	inline void set_NextInLML_13(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * value)
	{
		___NextInLML_13 = value;
		Il2CppCodeGenWriteBarrier((&___NextInLML_13), value);
	}

	inline static int32_t get_offset_of_NextInAEL_14() { return static_cast<int32_t>(offsetof(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335, ___NextInAEL_14)); }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * get_NextInAEL_14() const { return ___NextInAEL_14; }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 ** get_address_of_NextInAEL_14() { return &___NextInAEL_14; }
	inline void set_NextInAEL_14(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * value)
	{
		___NextInAEL_14 = value;
		Il2CppCodeGenWriteBarrier((&___NextInAEL_14), value);
	}

	inline static int32_t get_offset_of_PrevInAEL_15() { return static_cast<int32_t>(offsetof(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335, ___PrevInAEL_15)); }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * get_PrevInAEL_15() const { return ___PrevInAEL_15; }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 ** get_address_of_PrevInAEL_15() { return &___PrevInAEL_15; }
	inline void set_PrevInAEL_15(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * value)
	{
		___PrevInAEL_15 = value;
		Il2CppCodeGenWriteBarrier((&___PrevInAEL_15), value);
	}

	inline static int32_t get_offset_of_NextInSEL_16() { return static_cast<int32_t>(offsetof(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335, ___NextInSEL_16)); }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * get_NextInSEL_16() const { return ___NextInSEL_16; }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 ** get_address_of_NextInSEL_16() { return &___NextInSEL_16; }
	inline void set_NextInSEL_16(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * value)
	{
		___NextInSEL_16 = value;
		Il2CppCodeGenWriteBarrier((&___NextInSEL_16), value);
	}

	inline static int32_t get_offset_of_PrevInSEL_17() { return static_cast<int32_t>(offsetof(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335, ___PrevInSEL_17)); }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * get_PrevInSEL_17() const { return ___PrevInSEL_17; }
	inline TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 ** get_address_of_PrevInSEL_17() { return &___PrevInSEL_17; }
	inline void set_PrevInSEL_17(TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335 * value)
	{
		___PrevInSEL_17 = value;
		Il2CppCodeGenWriteBarrier((&___PrevInSEL_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEDGE_TA9D6BA578B80A7A1EEF29EDC76C629D9802C0335_H
#ifndef P2T_T995DE20560740D68D8F96C08687068A108F5C43D_H
#define P2T_T995DE20560740D68D8F96C08687068A108F5C43D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.P2T
struct  P2T_t995DE20560740D68D8F96C08687068A108F5C43D  : public RuntimeObject
{
public:

public:
};

struct P2T_t995DE20560740D68D8F96C08687068A108F5C43D_StaticFields
{
public:
	// Poly2Tri.TriangulationAlgorithm Poly2Tri.P2T::_defaultAlgorithm
	int32_t ____defaultAlgorithm_0;

public:
	inline static int32_t get_offset_of__defaultAlgorithm_0() { return static_cast<int32_t>(offsetof(P2T_t995DE20560740D68D8F96C08687068A108F5C43D_StaticFields, ____defaultAlgorithm_0)); }
	inline int32_t get__defaultAlgorithm_0() const { return ____defaultAlgorithm_0; }
	inline int32_t* get_address_of__defaultAlgorithm_0() { return &____defaultAlgorithm_0; }
	inline void set__defaultAlgorithm_0(int32_t value)
	{
		____defaultAlgorithm_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // P2T_T995DE20560740D68D8F96C08687068A108F5C43D_H
#ifndef POINT2DLIST_TDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45_H
#define POINT2DLIST_TDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.Point2DList
struct  Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Poly2Tri.Point2D> Poly2Tri.Point2DList::mPoints
	List_1_tE2F7563155465A354D6F465B4909C64C82FC2141 * ___mPoints_3;
	// Poly2Tri.Rect2D Poly2Tri.Point2DList::mBoundingBox
	Rect2D_t72E37904F5FB0718F0E3D96A0EBC0A77E80F6001 * ___mBoundingBox_4;
	// Poly2Tri.Point2DList/WindingOrderType Poly2Tri.Point2DList::mWindingOrder
	int32_t ___mWindingOrder_5;
	// System.Double Poly2Tri.Point2DList::mEpsilon
	double ___mEpsilon_6;

public:
	inline static int32_t get_offset_of_mPoints_3() { return static_cast<int32_t>(offsetof(Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45, ___mPoints_3)); }
	inline List_1_tE2F7563155465A354D6F465B4909C64C82FC2141 * get_mPoints_3() const { return ___mPoints_3; }
	inline List_1_tE2F7563155465A354D6F465B4909C64C82FC2141 ** get_address_of_mPoints_3() { return &___mPoints_3; }
	inline void set_mPoints_3(List_1_tE2F7563155465A354D6F465B4909C64C82FC2141 * value)
	{
		___mPoints_3 = value;
		Il2CppCodeGenWriteBarrier((&___mPoints_3), value);
	}

	inline static int32_t get_offset_of_mBoundingBox_4() { return static_cast<int32_t>(offsetof(Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45, ___mBoundingBox_4)); }
	inline Rect2D_t72E37904F5FB0718F0E3D96A0EBC0A77E80F6001 * get_mBoundingBox_4() const { return ___mBoundingBox_4; }
	inline Rect2D_t72E37904F5FB0718F0E3D96A0EBC0A77E80F6001 ** get_address_of_mBoundingBox_4() { return &___mBoundingBox_4; }
	inline void set_mBoundingBox_4(Rect2D_t72E37904F5FB0718F0E3D96A0EBC0A77E80F6001 * value)
	{
		___mBoundingBox_4 = value;
		Il2CppCodeGenWriteBarrier((&___mBoundingBox_4), value);
	}

	inline static int32_t get_offset_of_mWindingOrder_5() { return static_cast<int32_t>(offsetof(Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45, ___mWindingOrder_5)); }
	inline int32_t get_mWindingOrder_5() const { return ___mWindingOrder_5; }
	inline int32_t* get_address_of_mWindingOrder_5() { return &___mWindingOrder_5; }
	inline void set_mWindingOrder_5(int32_t value)
	{
		___mWindingOrder_5 = value;
	}

	inline static int32_t get_offset_of_mEpsilon_6() { return static_cast<int32_t>(offsetof(Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45, ___mEpsilon_6)); }
	inline double get_mEpsilon_6() const { return ___mEpsilon_6; }
	inline double* get_address_of_mEpsilon_6() { return &___mEpsilon_6; }
	inline void set_mEpsilon_6(double value)
	{
		___mEpsilon_6 = value;
	}
};

struct Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45_StaticFields
{
public:
	// System.Int32 Poly2Tri.Point2DList::kMaxPolygonVertices
	int32_t ___kMaxPolygonVertices_0;
	// System.Double Poly2Tri.Point2DList::kLinearSlop
	double ___kLinearSlop_1;
	// System.Double Poly2Tri.Point2DList::kAngularSlop
	double ___kAngularSlop_2;

public:
	inline static int32_t get_offset_of_kMaxPolygonVertices_0() { return static_cast<int32_t>(offsetof(Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45_StaticFields, ___kMaxPolygonVertices_0)); }
	inline int32_t get_kMaxPolygonVertices_0() const { return ___kMaxPolygonVertices_0; }
	inline int32_t* get_address_of_kMaxPolygonVertices_0() { return &___kMaxPolygonVertices_0; }
	inline void set_kMaxPolygonVertices_0(int32_t value)
	{
		___kMaxPolygonVertices_0 = value;
	}

	inline static int32_t get_offset_of_kLinearSlop_1() { return static_cast<int32_t>(offsetof(Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45_StaticFields, ___kLinearSlop_1)); }
	inline double get_kLinearSlop_1() const { return ___kLinearSlop_1; }
	inline double* get_address_of_kLinearSlop_1() { return &___kLinearSlop_1; }
	inline void set_kLinearSlop_1(double value)
	{
		___kLinearSlop_1 = value;
	}

	inline static int32_t get_offset_of_kAngularSlop_2() { return static_cast<int32_t>(offsetof(Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45_StaticFields, ___kAngularSlop_2)); }
	inline double get_kAngularSlop_2() const { return ___kAngularSlop_2; }
	inline double* get_address_of_kAngularSlop_2() { return &___kAngularSlop_2; }
	inline void set_kAngularSlop_2(double value)
	{
		___kAngularSlop_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINT2DLIST_TDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45_H
#ifndef POINTONEDGEEXCEPTION_T5FCE6A85ADB94A50C5B3276A26B1AC1ADE0ACDE7_H
#define POINTONEDGEEXCEPTION_T5FCE6A85ADB94A50C5B3276A26B1AC1ADE0ACDE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.PointOnEdgeException
struct  PointOnEdgeException_t5FCE6A85ADB94A50C5B3276A26B1AC1ADE0ACDE7  : public NotImplementedException_tAF5A82631683648991DEF6809680B0DF7A4A4767
{
public:
	// Poly2Tri.TriangulationPoint Poly2Tri.PointOnEdgeException::A
	TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * ___A_11;
	// Poly2Tri.TriangulationPoint Poly2Tri.PointOnEdgeException::B
	TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * ___B_12;
	// Poly2Tri.TriangulationPoint Poly2Tri.PointOnEdgeException::C
	TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * ___C_13;

public:
	inline static int32_t get_offset_of_A_11() { return static_cast<int32_t>(offsetof(PointOnEdgeException_t5FCE6A85ADB94A50C5B3276A26B1AC1ADE0ACDE7, ___A_11)); }
	inline TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * get_A_11() const { return ___A_11; }
	inline TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 ** get_address_of_A_11() { return &___A_11; }
	inline void set_A_11(TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * value)
	{
		___A_11 = value;
		Il2CppCodeGenWriteBarrier((&___A_11), value);
	}

	inline static int32_t get_offset_of_B_12() { return static_cast<int32_t>(offsetof(PointOnEdgeException_t5FCE6A85ADB94A50C5B3276A26B1AC1ADE0ACDE7, ___B_12)); }
	inline TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * get_B_12() const { return ___B_12; }
	inline TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 ** get_address_of_B_12() { return &___B_12; }
	inline void set_B_12(TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * value)
	{
		___B_12 = value;
		Il2CppCodeGenWriteBarrier((&___B_12), value);
	}

	inline static int32_t get_offset_of_C_13() { return static_cast<int32_t>(offsetof(PointOnEdgeException_t5FCE6A85ADB94A50C5B3276A26B1AC1ADE0ACDE7, ___C_13)); }
	inline TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * get_C_13() const { return ___C_13; }
	inline TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 ** get_address_of_C_13() { return &___C_13; }
	inline void set_C_13(TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * value)
	{
		___C_13 = value;
		Il2CppCodeGenWriteBarrier((&___C_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTONEDGEEXCEPTION_T5FCE6A85ADB94A50C5B3276A26B1AC1ADE0ACDE7_H
#ifndef POLYGONOPERATIONCONTEXT_T2916CE11B6DF5A42CCE77760F90B1EF258621A11_H
#define POLYGONOPERATIONCONTEXT_T2916CE11B6DF5A42CCE77760F90B1EF258621A11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.PolygonOperationContext
struct  PolygonOperationContext_t2916CE11B6DF5A42CCE77760F90B1EF258621A11  : public RuntimeObject
{
public:
	// Poly2Tri.PolygonUtil/PolyOperation Poly2Tri.PolygonOperationContext::mOperations
	uint32_t ___mOperations_0;
	// Poly2Tri.Point2DList Poly2Tri.PolygonOperationContext::mOriginalPolygon1
	Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45 * ___mOriginalPolygon1_1;
	// Poly2Tri.Point2DList Poly2Tri.PolygonOperationContext::mOriginalPolygon2
	Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45 * ___mOriginalPolygon2_2;
	// Poly2Tri.Point2DList Poly2Tri.PolygonOperationContext::mPoly1
	Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45 * ___mPoly1_3;
	// Poly2Tri.Point2DList Poly2Tri.PolygonOperationContext::mPoly2
	Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45 * ___mPoly2_4;
	// System.Collections.Generic.List`1<Poly2Tri.EdgeIntersectInfo> Poly2Tri.PolygonOperationContext::mIntersections
	List_1_tEA9596B52619CADF55F3EDA1C2E36BFE9AA4866C * ___mIntersections_5;
	// System.Int32 Poly2Tri.PolygonOperationContext::mStartingIndex
	int32_t ___mStartingIndex_6;
	// Poly2Tri.PolygonUtil/PolyUnionError Poly2Tri.PolygonOperationContext::mError
	int32_t ___mError_7;
	// System.Collections.Generic.List`1<System.Int32> Poly2Tri.PolygonOperationContext::mPoly1VectorAngles
	List_1_t4A961510635950236678F1B3B2436ECCAF28713A * ___mPoly1VectorAngles_8;
	// System.Collections.Generic.List`1<System.Int32> Poly2Tri.PolygonOperationContext::mPoly2VectorAngles
	List_1_t4A961510635950236678F1B3B2436ECCAF28713A * ___mPoly2VectorAngles_9;
	// System.Collections.Generic.Dictionary`2<System.UInt32,Poly2Tri.Point2DList> Poly2Tri.PolygonOperationContext::mOutput
	Dictionary_2_t3F61D5FC60EDBF690FB41A7FCCE46CE484F03DC1 * ___mOutput_10;

public:
	inline static int32_t get_offset_of_mOperations_0() { return static_cast<int32_t>(offsetof(PolygonOperationContext_t2916CE11B6DF5A42CCE77760F90B1EF258621A11, ___mOperations_0)); }
	inline uint32_t get_mOperations_0() const { return ___mOperations_0; }
	inline uint32_t* get_address_of_mOperations_0() { return &___mOperations_0; }
	inline void set_mOperations_0(uint32_t value)
	{
		___mOperations_0 = value;
	}

	inline static int32_t get_offset_of_mOriginalPolygon1_1() { return static_cast<int32_t>(offsetof(PolygonOperationContext_t2916CE11B6DF5A42CCE77760F90B1EF258621A11, ___mOriginalPolygon1_1)); }
	inline Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45 * get_mOriginalPolygon1_1() const { return ___mOriginalPolygon1_1; }
	inline Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45 ** get_address_of_mOriginalPolygon1_1() { return &___mOriginalPolygon1_1; }
	inline void set_mOriginalPolygon1_1(Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45 * value)
	{
		___mOriginalPolygon1_1 = value;
		Il2CppCodeGenWriteBarrier((&___mOriginalPolygon1_1), value);
	}

	inline static int32_t get_offset_of_mOriginalPolygon2_2() { return static_cast<int32_t>(offsetof(PolygonOperationContext_t2916CE11B6DF5A42CCE77760F90B1EF258621A11, ___mOriginalPolygon2_2)); }
	inline Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45 * get_mOriginalPolygon2_2() const { return ___mOriginalPolygon2_2; }
	inline Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45 ** get_address_of_mOriginalPolygon2_2() { return &___mOriginalPolygon2_2; }
	inline void set_mOriginalPolygon2_2(Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45 * value)
	{
		___mOriginalPolygon2_2 = value;
		Il2CppCodeGenWriteBarrier((&___mOriginalPolygon2_2), value);
	}

	inline static int32_t get_offset_of_mPoly1_3() { return static_cast<int32_t>(offsetof(PolygonOperationContext_t2916CE11B6DF5A42CCE77760F90B1EF258621A11, ___mPoly1_3)); }
	inline Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45 * get_mPoly1_3() const { return ___mPoly1_3; }
	inline Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45 ** get_address_of_mPoly1_3() { return &___mPoly1_3; }
	inline void set_mPoly1_3(Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45 * value)
	{
		___mPoly1_3 = value;
		Il2CppCodeGenWriteBarrier((&___mPoly1_3), value);
	}

	inline static int32_t get_offset_of_mPoly2_4() { return static_cast<int32_t>(offsetof(PolygonOperationContext_t2916CE11B6DF5A42CCE77760F90B1EF258621A11, ___mPoly2_4)); }
	inline Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45 * get_mPoly2_4() const { return ___mPoly2_4; }
	inline Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45 ** get_address_of_mPoly2_4() { return &___mPoly2_4; }
	inline void set_mPoly2_4(Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45 * value)
	{
		___mPoly2_4 = value;
		Il2CppCodeGenWriteBarrier((&___mPoly2_4), value);
	}

	inline static int32_t get_offset_of_mIntersections_5() { return static_cast<int32_t>(offsetof(PolygonOperationContext_t2916CE11B6DF5A42CCE77760F90B1EF258621A11, ___mIntersections_5)); }
	inline List_1_tEA9596B52619CADF55F3EDA1C2E36BFE9AA4866C * get_mIntersections_5() const { return ___mIntersections_5; }
	inline List_1_tEA9596B52619CADF55F3EDA1C2E36BFE9AA4866C ** get_address_of_mIntersections_5() { return &___mIntersections_5; }
	inline void set_mIntersections_5(List_1_tEA9596B52619CADF55F3EDA1C2E36BFE9AA4866C * value)
	{
		___mIntersections_5 = value;
		Il2CppCodeGenWriteBarrier((&___mIntersections_5), value);
	}

	inline static int32_t get_offset_of_mStartingIndex_6() { return static_cast<int32_t>(offsetof(PolygonOperationContext_t2916CE11B6DF5A42CCE77760F90B1EF258621A11, ___mStartingIndex_6)); }
	inline int32_t get_mStartingIndex_6() const { return ___mStartingIndex_6; }
	inline int32_t* get_address_of_mStartingIndex_6() { return &___mStartingIndex_6; }
	inline void set_mStartingIndex_6(int32_t value)
	{
		___mStartingIndex_6 = value;
	}

	inline static int32_t get_offset_of_mError_7() { return static_cast<int32_t>(offsetof(PolygonOperationContext_t2916CE11B6DF5A42CCE77760F90B1EF258621A11, ___mError_7)); }
	inline int32_t get_mError_7() const { return ___mError_7; }
	inline int32_t* get_address_of_mError_7() { return &___mError_7; }
	inline void set_mError_7(int32_t value)
	{
		___mError_7 = value;
	}

	inline static int32_t get_offset_of_mPoly1VectorAngles_8() { return static_cast<int32_t>(offsetof(PolygonOperationContext_t2916CE11B6DF5A42CCE77760F90B1EF258621A11, ___mPoly1VectorAngles_8)); }
	inline List_1_t4A961510635950236678F1B3B2436ECCAF28713A * get_mPoly1VectorAngles_8() const { return ___mPoly1VectorAngles_8; }
	inline List_1_t4A961510635950236678F1B3B2436ECCAF28713A ** get_address_of_mPoly1VectorAngles_8() { return &___mPoly1VectorAngles_8; }
	inline void set_mPoly1VectorAngles_8(List_1_t4A961510635950236678F1B3B2436ECCAF28713A * value)
	{
		___mPoly1VectorAngles_8 = value;
		Il2CppCodeGenWriteBarrier((&___mPoly1VectorAngles_8), value);
	}

	inline static int32_t get_offset_of_mPoly2VectorAngles_9() { return static_cast<int32_t>(offsetof(PolygonOperationContext_t2916CE11B6DF5A42CCE77760F90B1EF258621A11, ___mPoly2VectorAngles_9)); }
	inline List_1_t4A961510635950236678F1B3B2436ECCAF28713A * get_mPoly2VectorAngles_9() const { return ___mPoly2VectorAngles_9; }
	inline List_1_t4A961510635950236678F1B3B2436ECCAF28713A ** get_address_of_mPoly2VectorAngles_9() { return &___mPoly2VectorAngles_9; }
	inline void set_mPoly2VectorAngles_9(List_1_t4A961510635950236678F1B3B2436ECCAF28713A * value)
	{
		___mPoly2VectorAngles_9 = value;
		Il2CppCodeGenWriteBarrier((&___mPoly2VectorAngles_9), value);
	}

	inline static int32_t get_offset_of_mOutput_10() { return static_cast<int32_t>(offsetof(PolygonOperationContext_t2916CE11B6DF5A42CCE77760F90B1EF258621A11, ___mOutput_10)); }
	inline Dictionary_2_t3F61D5FC60EDBF690FB41A7FCCE46CE484F03DC1 * get_mOutput_10() const { return ___mOutput_10; }
	inline Dictionary_2_t3F61D5FC60EDBF690FB41A7FCCE46CE484F03DC1 ** get_address_of_mOutput_10() { return &___mOutput_10; }
	inline void set_mOutput_10(Dictionary_2_t3F61D5FC60EDBF690FB41A7FCCE46CE484F03DC1 * value)
	{
		___mOutput_10 = value;
		Il2CppCodeGenWriteBarrier((&___mOutput_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGONOPERATIONCONTEXT_T2916CE11B6DF5A42CCE77760F90B1EF258621A11_H
#ifndef TRIANGULATIONCONTEXT_T0153B91CA7F38202C7CC645BC07A67774802360E_H
#define TRIANGULATIONCONTEXT_T0153B91CA7F38202C7CC645BC07A67774802360E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.TriangulationContext
struct  TriangulationContext_t0153B91CA7F38202C7CC645BC07A67774802360E  : public RuntimeObject
{
public:
	// Poly2Tri.TriangulationDebugContext Poly2Tri.TriangulationContext::<DebugContext>k__BackingField
	TriangulationDebugContext_tF9458C2C5E3E324954C544E9FB4044AC0B1F103C * ___U3CDebugContextU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<Poly2Tri.DelaunayTriangle> Poly2Tri.TriangulationContext::Triangles
	List_1_t674C22B1FE8663E709E7B883A14E3C4B7DE3810B * ___Triangles_1;
	// System.Collections.Generic.List`1<Poly2Tri.TriangulationPoint> Poly2Tri.TriangulationContext::Points
	List_1_t79DDC71A203AB7658B258674FD0CFCF9340AB5A7 * ___Points_2;
	// Poly2Tri.TriangulationMode Poly2Tri.TriangulationContext::<TriangulationMode>k__BackingField
	int32_t ___U3CTriangulationModeU3Ek__BackingField_3;
	// Poly2Tri.ITriangulatable Poly2Tri.TriangulationContext::<Triangulatable>k__BackingField
	RuntimeObject* ___U3CTriangulatableU3Ek__BackingField_4;
	// System.Int32 Poly2Tri.TriangulationContext::<StepCount>k__BackingField
	int32_t ___U3CStepCountU3Ek__BackingField_5;
	// System.Boolean Poly2Tri.TriangulationContext::<IsDebugEnabled>k__BackingField
	bool ___U3CIsDebugEnabledU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CDebugContextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TriangulationContext_t0153B91CA7F38202C7CC645BC07A67774802360E, ___U3CDebugContextU3Ek__BackingField_0)); }
	inline TriangulationDebugContext_tF9458C2C5E3E324954C544E9FB4044AC0B1F103C * get_U3CDebugContextU3Ek__BackingField_0() const { return ___U3CDebugContextU3Ek__BackingField_0; }
	inline TriangulationDebugContext_tF9458C2C5E3E324954C544E9FB4044AC0B1F103C ** get_address_of_U3CDebugContextU3Ek__BackingField_0() { return &___U3CDebugContextU3Ek__BackingField_0; }
	inline void set_U3CDebugContextU3Ek__BackingField_0(TriangulationDebugContext_tF9458C2C5E3E324954C544E9FB4044AC0B1F103C * value)
	{
		___U3CDebugContextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDebugContextU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_Triangles_1() { return static_cast<int32_t>(offsetof(TriangulationContext_t0153B91CA7F38202C7CC645BC07A67774802360E, ___Triangles_1)); }
	inline List_1_t674C22B1FE8663E709E7B883A14E3C4B7DE3810B * get_Triangles_1() const { return ___Triangles_1; }
	inline List_1_t674C22B1FE8663E709E7B883A14E3C4B7DE3810B ** get_address_of_Triangles_1() { return &___Triangles_1; }
	inline void set_Triangles_1(List_1_t674C22B1FE8663E709E7B883A14E3C4B7DE3810B * value)
	{
		___Triangles_1 = value;
		Il2CppCodeGenWriteBarrier((&___Triangles_1), value);
	}

	inline static int32_t get_offset_of_Points_2() { return static_cast<int32_t>(offsetof(TriangulationContext_t0153B91CA7F38202C7CC645BC07A67774802360E, ___Points_2)); }
	inline List_1_t79DDC71A203AB7658B258674FD0CFCF9340AB5A7 * get_Points_2() const { return ___Points_2; }
	inline List_1_t79DDC71A203AB7658B258674FD0CFCF9340AB5A7 ** get_address_of_Points_2() { return &___Points_2; }
	inline void set_Points_2(List_1_t79DDC71A203AB7658B258674FD0CFCF9340AB5A7 * value)
	{
		___Points_2 = value;
		Il2CppCodeGenWriteBarrier((&___Points_2), value);
	}

	inline static int32_t get_offset_of_U3CTriangulationModeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TriangulationContext_t0153B91CA7F38202C7CC645BC07A67774802360E, ___U3CTriangulationModeU3Ek__BackingField_3)); }
	inline int32_t get_U3CTriangulationModeU3Ek__BackingField_3() const { return ___U3CTriangulationModeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CTriangulationModeU3Ek__BackingField_3() { return &___U3CTriangulationModeU3Ek__BackingField_3; }
	inline void set_U3CTriangulationModeU3Ek__BackingField_3(int32_t value)
	{
		___U3CTriangulationModeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CTriangulatableU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TriangulationContext_t0153B91CA7F38202C7CC645BC07A67774802360E, ___U3CTriangulatableU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CTriangulatableU3Ek__BackingField_4() const { return ___U3CTriangulatableU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CTriangulatableU3Ek__BackingField_4() { return &___U3CTriangulatableU3Ek__BackingField_4; }
	inline void set_U3CTriangulatableU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CTriangulatableU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTriangulatableU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CStepCountU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TriangulationContext_t0153B91CA7F38202C7CC645BC07A67774802360E, ___U3CStepCountU3Ek__BackingField_5)); }
	inline int32_t get_U3CStepCountU3Ek__BackingField_5() const { return ___U3CStepCountU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CStepCountU3Ek__BackingField_5() { return &___U3CStepCountU3Ek__BackingField_5; }
	inline void set_U3CStepCountU3Ek__BackingField_5(int32_t value)
	{
		___U3CStepCountU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CIsDebugEnabledU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TriangulationContext_t0153B91CA7F38202C7CC645BC07A67774802360E, ___U3CIsDebugEnabledU3Ek__BackingField_6)); }
	inline bool get_U3CIsDebugEnabledU3Ek__BackingField_6() const { return ___U3CIsDebugEnabledU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CIsDebugEnabledU3Ek__BackingField_6() { return &___U3CIsDebugEnabledU3Ek__BackingField_6; }
	inline void set_U3CIsDebugEnabledU3Ek__BackingField_6(bool value)
	{
		___U3CIsDebugEnabledU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATIONCONTEXT_T0153B91CA7F38202C7CC645BC07A67774802360E_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef POLYTREE_TDB7C36AE94547FD9C335828F75E16585F0A8CCA5_H
#define POLYTREE_TDB7C36AE94547FD9C335828F75E16585F0A8CCA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClipperLib.PolyTree
struct  PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5  : public PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB
{
public:
	// System.Collections.Generic.List`1<ClipperLib.PolyNode> ClipperLib.PolyTree::m_AllPolys
	List_1_tAFCC6AECDB60026588FFF5817A80B9CBDB4F349F * ___m_AllPolys_7;

public:
	inline static int32_t get_offset_of_m_AllPolys_7() { return static_cast<int32_t>(offsetof(PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5, ___m_AllPolys_7)); }
	inline List_1_tAFCC6AECDB60026588FFF5817A80B9CBDB4F349F * get_m_AllPolys_7() const { return ___m_AllPolys_7; }
	inline List_1_tAFCC6AECDB60026588FFF5817A80B9CBDB4F349F ** get_address_of_m_AllPolys_7() { return &___m_AllPolys_7; }
	inline void set_m_AllPolys_7(List_1_tAFCC6AECDB60026588FFF5817A80B9CBDB4F349F * value)
	{
		___m_AllPolys_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AllPolys_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYTREE_TDB7C36AE94547FD9C335828F75E16585F0A8CCA5_H
#ifndef IMPORTSETTINGS_T35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3_H
#define IMPORTSETTINGS_T35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImportSettings
struct  ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// UnityEngine.Object ImportSettings::svgFile
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___svgFile_4;
	// System.Boolean ImportSettings::splitMeshesByLayers
	bool ___splitMeshesByLayers_5;
	// System.Boolean ImportSettings::_splitCollidersByLayers
	bool ____splitCollidersByLayers_6;
	// System.Single ImportSettings::scale
	float ___scale_7;
	// System.Int32 ImportSettings::maxSubdivisonDepth
	int32_t ___maxSubdivisonDepth_8;
	// System.Single ImportSettings::minSubdivisionDistanceDelta
	float ___minSubdivisionDistanceDelta_9;
	// UnityEngine.Vector2 ImportSettings::pivot
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___pivot_10;

public:
	inline static int32_t get_offset_of_svgFile_4() { return static_cast<int32_t>(offsetof(ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3, ___svgFile_4)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get_svgFile_4() const { return ___svgFile_4; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of_svgFile_4() { return &___svgFile_4; }
	inline void set_svgFile_4(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		___svgFile_4 = value;
		Il2CppCodeGenWriteBarrier((&___svgFile_4), value);
	}

	inline static int32_t get_offset_of_splitMeshesByLayers_5() { return static_cast<int32_t>(offsetof(ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3, ___splitMeshesByLayers_5)); }
	inline bool get_splitMeshesByLayers_5() const { return ___splitMeshesByLayers_5; }
	inline bool* get_address_of_splitMeshesByLayers_5() { return &___splitMeshesByLayers_5; }
	inline void set_splitMeshesByLayers_5(bool value)
	{
		___splitMeshesByLayers_5 = value;
	}

	inline static int32_t get_offset_of__splitCollidersByLayers_6() { return static_cast<int32_t>(offsetof(ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3, ____splitCollidersByLayers_6)); }
	inline bool get__splitCollidersByLayers_6() const { return ____splitCollidersByLayers_6; }
	inline bool* get_address_of__splitCollidersByLayers_6() { return &____splitCollidersByLayers_6; }
	inline void set__splitCollidersByLayers_6(bool value)
	{
		____splitCollidersByLayers_6 = value;
	}

	inline static int32_t get_offset_of_scale_7() { return static_cast<int32_t>(offsetof(ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3, ___scale_7)); }
	inline float get_scale_7() const { return ___scale_7; }
	inline float* get_address_of_scale_7() { return &___scale_7; }
	inline void set_scale_7(float value)
	{
		___scale_7 = value;
	}

	inline static int32_t get_offset_of_maxSubdivisonDepth_8() { return static_cast<int32_t>(offsetof(ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3, ___maxSubdivisonDepth_8)); }
	inline int32_t get_maxSubdivisonDepth_8() const { return ___maxSubdivisonDepth_8; }
	inline int32_t* get_address_of_maxSubdivisonDepth_8() { return &___maxSubdivisonDepth_8; }
	inline void set_maxSubdivisonDepth_8(int32_t value)
	{
		___maxSubdivisonDepth_8 = value;
	}

	inline static int32_t get_offset_of_minSubdivisionDistanceDelta_9() { return static_cast<int32_t>(offsetof(ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3, ___minSubdivisionDistanceDelta_9)); }
	inline float get_minSubdivisionDistanceDelta_9() const { return ___minSubdivisionDistanceDelta_9; }
	inline float* get_address_of_minSubdivisionDistanceDelta_9() { return &___minSubdivisionDistanceDelta_9; }
	inline void set_minSubdivisionDistanceDelta_9(float value)
	{
		___minSubdivisionDistanceDelta_9 = value;
	}

	inline static int32_t get_offset_of_pivot_10() { return static_cast<int32_t>(offsetof(ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3, ___pivot_10)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_pivot_10() const { return ___pivot_10; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_pivot_10() { return &___pivot_10; }
	inline void set_pivot_10(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___pivot_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMPORTSETTINGS_T35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3_H
#ifndef CONTOUR_T920290958781337055636452F24A9DB2E3EDE246_H
#define CONTOUR_T920290958781337055636452F24A9DB2E3EDE246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.Contour
struct  Contour_t920290958781337055636452F24A9DB2E3EDE246  : public Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45
{
public:
	// System.Collections.Generic.List`1<Poly2Tri.Contour> Poly2Tri.Contour::mHoles
	List_1_t848BD4B8703132660D7117532F3ADD94076BDA26 * ___mHoles_7;
	// Poly2Tri.ITriangulatable Poly2Tri.Contour::mParent
	RuntimeObject* ___mParent_8;
	// System.String Poly2Tri.Contour::mName
	String_t* ___mName_9;

public:
	inline static int32_t get_offset_of_mHoles_7() { return static_cast<int32_t>(offsetof(Contour_t920290958781337055636452F24A9DB2E3EDE246, ___mHoles_7)); }
	inline List_1_t848BD4B8703132660D7117532F3ADD94076BDA26 * get_mHoles_7() const { return ___mHoles_7; }
	inline List_1_t848BD4B8703132660D7117532F3ADD94076BDA26 ** get_address_of_mHoles_7() { return &___mHoles_7; }
	inline void set_mHoles_7(List_1_t848BD4B8703132660D7117532F3ADD94076BDA26 * value)
	{
		___mHoles_7 = value;
		Il2CppCodeGenWriteBarrier((&___mHoles_7), value);
	}

	inline static int32_t get_offset_of_mParent_8() { return static_cast<int32_t>(offsetof(Contour_t920290958781337055636452F24A9DB2E3EDE246, ___mParent_8)); }
	inline RuntimeObject* get_mParent_8() const { return ___mParent_8; }
	inline RuntimeObject** get_address_of_mParent_8() { return &___mParent_8; }
	inline void set_mParent_8(RuntimeObject* value)
	{
		___mParent_8 = value;
		Il2CppCodeGenWriteBarrier((&___mParent_8), value);
	}

	inline static int32_t get_offset_of_mName_9() { return static_cast<int32_t>(offsetof(Contour_t920290958781337055636452F24A9DB2E3EDE246, ___mName_9)); }
	inline String_t* get_mName_9() const { return ___mName_9; }
	inline String_t** get_address_of_mName_9() { return &___mName_9; }
	inline void set_mName_9(String_t* value)
	{
		___mName_9 = value;
		Il2CppCodeGenWriteBarrier((&___mName_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTOUR_T920290958781337055636452F24A9DB2E3EDE246_H
#ifndef DTSWEEPCONTEXT_T3B606C92EEA406F37559300636C1AFE7E1D9FD7F_H
#define DTSWEEPCONTEXT_T3B606C92EEA406F37559300636C1AFE7E1D9FD7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.DTSweepContext
struct  DTSweepContext_t3B606C92EEA406F37559300636C1AFE7E1D9FD7F  : public TriangulationContext_t0153B91CA7F38202C7CC645BC07A67774802360E
{
public:
	// System.Single Poly2Tri.DTSweepContext::ALPHA
	float ___ALPHA_7;
	// Poly2Tri.AdvancingFront Poly2Tri.DTSweepContext::Front
	AdvancingFront_t103C9271D0DE07CA7D45EEDBE2ACBD93DC215340 * ___Front_8;
	// Poly2Tri.TriangulationPoint Poly2Tri.DTSweepContext::<Head>k__BackingField
	TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * ___U3CHeadU3Ek__BackingField_9;
	// Poly2Tri.TriangulationPoint Poly2Tri.DTSweepContext::<Tail>k__BackingField
	TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * ___U3CTailU3Ek__BackingField_10;
	// Poly2Tri.DTSweepBasin Poly2Tri.DTSweepContext::Basin
	DTSweepBasin_t74AB6315A3629FEF0FC85456015FA9537C67B2DD * ___Basin_11;
	// Poly2Tri.DTSweepEdgeEvent Poly2Tri.DTSweepContext::EdgeEvent
	DTSweepEdgeEvent_tBD7A886997C86280A3AE982B4E8C86325A28E422 * ___EdgeEvent_12;
	// Poly2Tri.DTSweepPointComparator Poly2Tri.DTSweepContext::_comparator
	DTSweepPointComparator_tFA3FA00C634B6162D20BD76BE5E4C0CDB943ACAC * ____comparator_13;

public:
	inline static int32_t get_offset_of_ALPHA_7() { return static_cast<int32_t>(offsetof(DTSweepContext_t3B606C92EEA406F37559300636C1AFE7E1D9FD7F, ___ALPHA_7)); }
	inline float get_ALPHA_7() const { return ___ALPHA_7; }
	inline float* get_address_of_ALPHA_7() { return &___ALPHA_7; }
	inline void set_ALPHA_7(float value)
	{
		___ALPHA_7 = value;
	}

	inline static int32_t get_offset_of_Front_8() { return static_cast<int32_t>(offsetof(DTSweepContext_t3B606C92EEA406F37559300636C1AFE7E1D9FD7F, ___Front_8)); }
	inline AdvancingFront_t103C9271D0DE07CA7D45EEDBE2ACBD93DC215340 * get_Front_8() const { return ___Front_8; }
	inline AdvancingFront_t103C9271D0DE07CA7D45EEDBE2ACBD93DC215340 ** get_address_of_Front_8() { return &___Front_8; }
	inline void set_Front_8(AdvancingFront_t103C9271D0DE07CA7D45EEDBE2ACBD93DC215340 * value)
	{
		___Front_8 = value;
		Il2CppCodeGenWriteBarrier((&___Front_8), value);
	}

	inline static int32_t get_offset_of_U3CHeadU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(DTSweepContext_t3B606C92EEA406F37559300636C1AFE7E1D9FD7F, ___U3CHeadU3Ek__BackingField_9)); }
	inline TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * get_U3CHeadU3Ek__BackingField_9() const { return ___U3CHeadU3Ek__BackingField_9; }
	inline TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 ** get_address_of_U3CHeadU3Ek__BackingField_9() { return &___U3CHeadU3Ek__BackingField_9; }
	inline void set_U3CHeadU3Ek__BackingField_9(TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * value)
	{
		___U3CHeadU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CTailU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(DTSweepContext_t3B606C92EEA406F37559300636C1AFE7E1D9FD7F, ___U3CTailU3Ek__BackingField_10)); }
	inline TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * get_U3CTailU3Ek__BackingField_10() const { return ___U3CTailU3Ek__BackingField_10; }
	inline TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 ** get_address_of_U3CTailU3Ek__BackingField_10() { return &___U3CTailU3Ek__BackingField_10; }
	inline void set_U3CTailU3Ek__BackingField_10(TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4 * value)
	{
		___U3CTailU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTailU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_Basin_11() { return static_cast<int32_t>(offsetof(DTSweepContext_t3B606C92EEA406F37559300636C1AFE7E1D9FD7F, ___Basin_11)); }
	inline DTSweepBasin_t74AB6315A3629FEF0FC85456015FA9537C67B2DD * get_Basin_11() const { return ___Basin_11; }
	inline DTSweepBasin_t74AB6315A3629FEF0FC85456015FA9537C67B2DD ** get_address_of_Basin_11() { return &___Basin_11; }
	inline void set_Basin_11(DTSweepBasin_t74AB6315A3629FEF0FC85456015FA9537C67B2DD * value)
	{
		___Basin_11 = value;
		Il2CppCodeGenWriteBarrier((&___Basin_11), value);
	}

	inline static int32_t get_offset_of_EdgeEvent_12() { return static_cast<int32_t>(offsetof(DTSweepContext_t3B606C92EEA406F37559300636C1AFE7E1D9FD7F, ___EdgeEvent_12)); }
	inline DTSweepEdgeEvent_tBD7A886997C86280A3AE982B4E8C86325A28E422 * get_EdgeEvent_12() const { return ___EdgeEvent_12; }
	inline DTSweepEdgeEvent_tBD7A886997C86280A3AE982B4E8C86325A28E422 ** get_address_of_EdgeEvent_12() { return &___EdgeEvent_12; }
	inline void set_EdgeEvent_12(DTSweepEdgeEvent_tBD7A886997C86280A3AE982B4E8C86325A28E422 * value)
	{
		___EdgeEvent_12 = value;
		Il2CppCodeGenWriteBarrier((&___EdgeEvent_12), value);
	}

	inline static int32_t get_offset_of__comparator_13() { return static_cast<int32_t>(offsetof(DTSweepContext_t3B606C92EEA406F37559300636C1AFE7E1D9FD7F, ____comparator_13)); }
	inline DTSweepPointComparator_tFA3FA00C634B6162D20BD76BE5E4C0CDB943ACAC * get__comparator_13() const { return ____comparator_13; }
	inline DTSweepPointComparator_tFA3FA00C634B6162D20BD76BE5E4C0CDB943ACAC ** get_address_of__comparator_13() { return &____comparator_13; }
	inline void set__comparator_13(DTSweepPointComparator_tFA3FA00C634B6162D20BD76BE5E4C0CDB943ACAC * value)
	{
		____comparator_13 = value;
		Il2CppCodeGenWriteBarrier((&____comparator_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTSWEEPCONTEXT_T3B606C92EEA406F37559300636C1AFE7E1D9FD7F_H
#ifndef POINTSET_T9993025F4B37963DECB948F2ED89346430BA5EDD_H
#define POINTSET_T9993025F4B37963DECB948F2ED89346430BA5EDD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.PointSet
struct  PointSet_t9993025F4B37963DECB948F2ED89346430BA5EDD  : public Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45
{
public:
	// System.Collections.Generic.Dictionary`2<System.UInt32,Poly2Tri.TriangulationPoint> Poly2Tri.PointSet::mPointMap
	Dictionary_2_tD65C9BFCB816ED88CB099B735C202522EE4C155B * ___mPointMap_7;
	// System.Collections.Generic.IList`1<Poly2Tri.DelaunayTriangle> Poly2Tri.PointSet::<Triangles>k__BackingField
	RuntimeObject* ___U3CTrianglesU3Ek__BackingField_8;
	// System.String Poly2Tri.PointSet::<FileName>k__BackingField
	String_t* ___U3CFileNameU3Ek__BackingField_9;
	// System.Boolean Poly2Tri.PointSet::<DisplayFlipX>k__BackingField
	bool ___U3CDisplayFlipXU3Ek__BackingField_10;
	// System.Boolean Poly2Tri.PointSet::<DisplayFlipY>k__BackingField
	bool ___U3CDisplayFlipYU3Ek__BackingField_11;
	// System.Single Poly2Tri.PointSet::<DisplayRotate>k__BackingField
	float ___U3CDisplayRotateU3Ek__BackingField_12;
	// System.Double Poly2Tri.PointSet::mPrecision
	double ___mPrecision_13;

public:
	inline static int32_t get_offset_of_mPointMap_7() { return static_cast<int32_t>(offsetof(PointSet_t9993025F4B37963DECB948F2ED89346430BA5EDD, ___mPointMap_7)); }
	inline Dictionary_2_tD65C9BFCB816ED88CB099B735C202522EE4C155B * get_mPointMap_7() const { return ___mPointMap_7; }
	inline Dictionary_2_tD65C9BFCB816ED88CB099B735C202522EE4C155B ** get_address_of_mPointMap_7() { return &___mPointMap_7; }
	inline void set_mPointMap_7(Dictionary_2_tD65C9BFCB816ED88CB099B735C202522EE4C155B * value)
	{
		___mPointMap_7 = value;
		Il2CppCodeGenWriteBarrier((&___mPointMap_7), value);
	}

	inline static int32_t get_offset_of_U3CTrianglesU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PointSet_t9993025F4B37963DECB948F2ED89346430BA5EDD, ___U3CTrianglesU3Ek__BackingField_8)); }
	inline RuntimeObject* get_U3CTrianglesU3Ek__BackingField_8() const { return ___U3CTrianglesU3Ek__BackingField_8; }
	inline RuntimeObject** get_address_of_U3CTrianglesU3Ek__BackingField_8() { return &___U3CTrianglesU3Ek__BackingField_8; }
	inline void set_U3CTrianglesU3Ek__BackingField_8(RuntimeObject* value)
	{
		___U3CTrianglesU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrianglesU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CFileNameU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PointSet_t9993025F4B37963DECB948F2ED89346430BA5EDD, ___U3CFileNameU3Ek__BackingField_9)); }
	inline String_t* get_U3CFileNameU3Ek__BackingField_9() const { return ___U3CFileNameU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CFileNameU3Ek__BackingField_9() { return &___U3CFileNameU3Ek__BackingField_9; }
	inline void set_U3CFileNameU3Ek__BackingField_9(String_t* value)
	{
		___U3CFileNameU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFileNameU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CDisplayFlipXU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PointSet_t9993025F4B37963DECB948F2ED89346430BA5EDD, ___U3CDisplayFlipXU3Ek__BackingField_10)); }
	inline bool get_U3CDisplayFlipXU3Ek__BackingField_10() const { return ___U3CDisplayFlipXU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CDisplayFlipXU3Ek__BackingField_10() { return &___U3CDisplayFlipXU3Ek__BackingField_10; }
	inline void set_U3CDisplayFlipXU3Ek__BackingField_10(bool value)
	{
		___U3CDisplayFlipXU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CDisplayFlipYU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PointSet_t9993025F4B37963DECB948F2ED89346430BA5EDD, ___U3CDisplayFlipYU3Ek__BackingField_11)); }
	inline bool get_U3CDisplayFlipYU3Ek__BackingField_11() const { return ___U3CDisplayFlipYU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CDisplayFlipYU3Ek__BackingField_11() { return &___U3CDisplayFlipYU3Ek__BackingField_11; }
	inline void set_U3CDisplayFlipYU3Ek__BackingField_11(bool value)
	{
		___U3CDisplayFlipYU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CDisplayRotateU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PointSet_t9993025F4B37963DECB948F2ED89346430BA5EDD, ___U3CDisplayRotateU3Ek__BackingField_12)); }
	inline float get_U3CDisplayRotateU3Ek__BackingField_12() const { return ___U3CDisplayRotateU3Ek__BackingField_12; }
	inline float* get_address_of_U3CDisplayRotateU3Ek__BackingField_12() { return &___U3CDisplayRotateU3Ek__BackingField_12; }
	inline void set_U3CDisplayRotateU3Ek__BackingField_12(float value)
	{
		___U3CDisplayRotateU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_mPrecision_13() { return static_cast<int32_t>(offsetof(PointSet_t9993025F4B37963DECB948F2ED89346430BA5EDD, ___mPrecision_13)); }
	inline double get_mPrecision_13() const { return ___mPrecision_13; }
	inline double* get_address_of_mPrecision_13() { return &___mPrecision_13; }
	inline void set_mPrecision_13(double value)
	{
		___mPrecision_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTSET_T9993025F4B37963DECB948F2ED89346430BA5EDD_H
#ifndef POLYGON_T84A5698D8CA59521DC90C4FF460F21DAB85C0F67_H
#define POLYGON_T84A5698D8CA59521DC90C4FF460F21DAB85C0F67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.Polygon
struct  Polygon_t84A5698D8CA59521DC90C4FF460F21DAB85C0F67  : public Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45
{
public:
	// System.Collections.Generic.Dictionary`2<System.UInt32,Poly2Tri.TriangulationPoint> Poly2Tri.Polygon::mPointMap
	Dictionary_2_tD65C9BFCB816ED88CB099B735C202522EE4C155B * ___mPointMap_7;
	// System.Collections.Generic.List`1<Poly2Tri.DelaunayTriangle> Poly2Tri.Polygon::mTriangles
	List_1_t674C22B1FE8663E709E7B883A14E3C4B7DE3810B * ___mTriangles_8;
	// System.String Poly2Tri.Polygon::<FileName>k__BackingField
	String_t* ___U3CFileNameU3Ek__BackingField_9;
	// System.Boolean Poly2Tri.Polygon::<DisplayFlipX>k__BackingField
	bool ___U3CDisplayFlipXU3Ek__BackingField_10;
	// System.Boolean Poly2Tri.Polygon::<DisplayFlipY>k__BackingField
	bool ___U3CDisplayFlipYU3Ek__BackingField_11;
	// System.Single Poly2Tri.Polygon::<DisplayRotate>k__BackingField
	float ___U3CDisplayRotateU3Ek__BackingField_12;
	// System.Double Poly2Tri.Polygon::mPrecision
	double ___mPrecision_13;
	// System.Collections.Generic.List`1<Poly2Tri.Polygon> Poly2Tri.Polygon::mHoles
	List_1_tDE4178F994B399BD69F0374B4BF2077E0A5D8AAB * ___mHoles_14;
	// System.Collections.Generic.List`1<Poly2Tri.TriangulationPoint> Poly2Tri.Polygon::mSteinerPoints
	List_1_t79DDC71A203AB7658B258674FD0CFCF9340AB5A7 * ___mSteinerPoints_15;
	// Poly2Tri.PolygonPoint Poly2Tri.Polygon::_last
	PolygonPoint_tAED3172793E804A09A2EB2FEDCB0EBA5159A08FE * ____last_16;

public:
	inline static int32_t get_offset_of_mPointMap_7() { return static_cast<int32_t>(offsetof(Polygon_t84A5698D8CA59521DC90C4FF460F21DAB85C0F67, ___mPointMap_7)); }
	inline Dictionary_2_tD65C9BFCB816ED88CB099B735C202522EE4C155B * get_mPointMap_7() const { return ___mPointMap_7; }
	inline Dictionary_2_tD65C9BFCB816ED88CB099B735C202522EE4C155B ** get_address_of_mPointMap_7() { return &___mPointMap_7; }
	inline void set_mPointMap_7(Dictionary_2_tD65C9BFCB816ED88CB099B735C202522EE4C155B * value)
	{
		___mPointMap_7 = value;
		Il2CppCodeGenWriteBarrier((&___mPointMap_7), value);
	}

	inline static int32_t get_offset_of_mTriangles_8() { return static_cast<int32_t>(offsetof(Polygon_t84A5698D8CA59521DC90C4FF460F21DAB85C0F67, ___mTriangles_8)); }
	inline List_1_t674C22B1FE8663E709E7B883A14E3C4B7DE3810B * get_mTriangles_8() const { return ___mTriangles_8; }
	inline List_1_t674C22B1FE8663E709E7B883A14E3C4B7DE3810B ** get_address_of_mTriangles_8() { return &___mTriangles_8; }
	inline void set_mTriangles_8(List_1_t674C22B1FE8663E709E7B883A14E3C4B7DE3810B * value)
	{
		___mTriangles_8 = value;
		Il2CppCodeGenWriteBarrier((&___mTriangles_8), value);
	}

	inline static int32_t get_offset_of_U3CFileNameU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Polygon_t84A5698D8CA59521DC90C4FF460F21DAB85C0F67, ___U3CFileNameU3Ek__BackingField_9)); }
	inline String_t* get_U3CFileNameU3Ek__BackingField_9() const { return ___U3CFileNameU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CFileNameU3Ek__BackingField_9() { return &___U3CFileNameU3Ek__BackingField_9; }
	inline void set_U3CFileNameU3Ek__BackingField_9(String_t* value)
	{
		___U3CFileNameU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFileNameU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CDisplayFlipXU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Polygon_t84A5698D8CA59521DC90C4FF460F21DAB85C0F67, ___U3CDisplayFlipXU3Ek__BackingField_10)); }
	inline bool get_U3CDisplayFlipXU3Ek__BackingField_10() const { return ___U3CDisplayFlipXU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CDisplayFlipXU3Ek__BackingField_10() { return &___U3CDisplayFlipXU3Ek__BackingField_10; }
	inline void set_U3CDisplayFlipXU3Ek__BackingField_10(bool value)
	{
		___U3CDisplayFlipXU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CDisplayFlipYU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Polygon_t84A5698D8CA59521DC90C4FF460F21DAB85C0F67, ___U3CDisplayFlipYU3Ek__BackingField_11)); }
	inline bool get_U3CDisplayFlipYU3Ek__BackingField_11() const { return ___U3CDisplayFlipYU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CDisplayFlipYU3Ek__BackingField_11() { return &___U3CDisplayFlipYU3Ek__BackingField_11; }
	inline void set_U3CDisplayFlipYU3Ek__BackingField_11(bool value)
	{
		___U3CDisplayFlipYU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CDisplayRotateU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Polygon_t84A5698D8CA59521DC90C4FF460F21DAB85C0F67, ___U3CDisplayRotateU3Ek__BackingField_12)); }
	inline float get_U3CDisplayRotateU3Ek__BackingField_12() const { return ___U3CDisplayRotateU3Ek__BackingField_12; }
	inline float* get_address_of_U3CDisplayRotateU3Ek__BackingField_12() { return &___U3CDisplayRotateU3Ek__BackingField_12; }
	inline void set_U3CDisplayRotateU3Ek__BackingField_12(float value)
	{
		___U3CDisplayRotateU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_mPrecision_13() { return static_cast<int32_t>(offsetof(Polygon_t84A5698D8CA59521DC90C4FF460F21DAB85C0F67, ___mPrecision_13)); }
	inline double get_mPrecision_13() const { return ___mPrecision_13; }
	inline double* get_address_of_mPrecision_13() { return &___mPrecision_13; }
	inline void set_mPrecision_13(double value)
	{
		___mPrecision_13 = value;
	}

	inline static int32_t get_offset_of_mHoles_14() { return static_cast<int32_t>(offsetof(Polygon_t84A5698D8CA59521DC90C4FF460F21DAB85C0F67, ___mHoles_14)); }
	inline List_1_tDE4178F994B399BD69F0374B4BF2077E0A5D8AAB * get_mHoles_14() const { return ___mHoles_14; }
	inline List_1_tDE4178F994B399BD69F0374B4BF2077E0A5D8AAB ** get_address_of_mHoles_14() { return &___mHoles_14; }
	inline void set_mHoles_14(List_1_tDE4178F994B399BD69F0374B4BF2077E0A5D8AAB * value)
	{
		___mHoles_14 = value;
		Il2CppCodeGenWriteBarrier((&___mHoles_14), value);
	}

	inline static int32_t get_offset_of_mSteinerPoints_15() { return static_cast<int32_t>(offsetof(Polygon_t84A5698D8CA59521DC90C4FF460F21DAB85C0F67, ___mSteinerPoints_15)); }
	inline List_1_t79DDC71A203AB7658B258674FD0CFCF9340AB5A7 * get_mSteinerPoints_15() const { return ___mSteinerPoints_15; }
	inline List_1_t79DDC71A203AB7658B258674FD0CFCF9340AB5A7 ** get_address_of_mSteinerPoints_15() { return &___mSteinerPoints_15; }
	inline void set_mSteinerPoints_15(List_1_t79DDC71A203AB7658B258674FD0CFCF9340AB5A7 * value)
	{
		___mSteinerPoints_15 = value;
		Il2CppCodeGenWriteBarrier((&___mSteinerPoints_15), value);
	}

	inline static int32_t get_offset_of__last_16() { return static_cast<int32_t>(offsetof(Polygon_t84A5698D8CA59521DC90C4FF460F21DAB85C0F67, ____last_16)); }
	inline PolygonPoint_tAED3172793E804A09A2EB2FEDCB0EBA5159A08FE * get__last_16() const { return ____last_16; }
	inline PolygonPoint_tAED3172793E804A09A2EB2FEDCB0EBA5159A08FE ** get_address_of__last_16() { return &____last_16; }
	inline void set__last_16(PolygonPoint_tAED3172793E804A09A2EB2FEDCB0EBA5159A08FE * value)
	{
		____last_16 = value;
		Il2CppCodeGenWriteBarrier((&____last_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGON_T84A5698D8CA59521DC90C4FF460F21DAB85C0F67_H
#ifndef TRIANGULATIONPOINTLIST_T3E00E8C9DBA64904AF6C882161F4F5523C74DAD6_H
#define TRIANGULATIONPOINTLIST_T3E00E8C9DBA64904AF6C882161F4F5523C74DAD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.TriangulationPointList
struct  TriangulationPointList_t3E00E8C9DBA64904AF6C882161F4F5523C74DAD6  : public Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATIONPOINTLIST_T3E00E8C9DBA64904AF6C882161F4F5523C74DAD6_H
#ifndef GLOBALSETTINGS_T344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1_H
#define GLOBALSETTINGS_T344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.GlobalSettings
struct  GlobalSettings_t344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// ImportSettings SimplySVG.GlobalSettings::defaultImportSettings
	ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3 * ___defaultImportSettings_6;
	// UnityEngine.Material SimplySVG.GlobalSettings::defaultMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___defaultMaterial_7;
	// SimplySVG.LogLevel SimplySVG.GlobalSettings::levelOfLog
	int32_t ___levelOfLog_8;
	// System.Int32 SimplySVG.GlobalSettings::maxUnsupportedFeatureWarningCount
	int32_t ___maxUnsupportedFeatureWarningCount_9;
	// System.Boolean SimplySVG.GlobalSettings::extraDevelopementChecks
	bool ___extraDevelopementChecks_10;

public:
	inline static int32_t get_offset_of_defaultImportSettings_6() { return static_cast<int32_t>(offsetof(GlobalSettings_t344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1, ___defaultImportSettings_6)); }
	inline ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3 * get_defaultImportSettings_6() const { return ___defaultImportSettings_6; }
	inline ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3 ** get_address_of_defaultImportSettings_6() { return &___defaultImportSettings_6; }
	inline void set_defaultImportSettings_6(ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3 * value)
	{
		___defaultImportSettings_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultImportSettings_6), value);
	}

	inline static int32_t get_offset_of_defaultMaterial_7() { return static_cast<int32_t>(offsetof(GlobalSettings_t344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1, ___defaultMaterial_7)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_defaultMaterial_7() const { return ___defaultMaterial_7; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_defaultMaterial_7() { return &___defaultMaterial_7; }
	inline void set_defaultMaterial_7(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___defaultMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___defaultMaterial_7), value);
	}

	inline static int32_t get_offset_of_levelOfLog_8() { return static_cast<int32_t>(offsetof(GlobalSettings_t344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1, ___levelOfLog_8)); }
	inline int32_t get_levelOfLog_8() const { return ___levelOfLog_8; }
	inline int32_t* get_address_of_levelOfLog_8() { return &___levelOfLog_8; }
	inline void set_levelOfLog_8(int32_t value)
	{
		___levelOfLog_8 = value;
	}

	inline static int32_t get_offset_of_maxUnsupportedFeatureWarningCount_9() { return static_cast<int32_t>(offsetof(GlobalSettings_t344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1, ___maxUnsupportedFeatureWarningCount_9)); }
	inline int32_t get_maxUnsupportedFeatureWarningCount_9() const { return ___maxUnsupportedFeatureWarningCount_9; }
	inline int32_t* get_address_of_maxUnsupportedFeatureWarningCount_9() { return &___maxUnsupportedFeatureWarningCount_9; }
	inline void set_maxUnsupportedFeatureWarningCount_9(int32_t value)
	{
		___maxUnsupportedFeatureWarningCount_9 = value;
	}

	inline static int32_t get_offset_of_extraDevelopementChecks_10() { return static_cast<int32_t>(offsetof(GlobalSettings_t344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1, ___extraDevelopementChecks_10)); }
	inline bool get_extraDevelopementChecks_10() const { return ___extraDevelopementChecks_10; }
	inline bool* get_address_of_extraDevelopementChecks_10() { return &___extraDevelopementChecks_10; }
	inline void set_extraDevelopementChecks_10(bool value)
	{
		___extraDevelopementChecks_10 = value;
	}
};

struct GlobalSettings_t344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1_StaticFields
{
public:
	// SimplySVG.GlobalSettings SimplySVG.GlobalSettings::current
	GlobalSettings_t344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1 * ___current_4;
	// System.String[] SimplySVG.GlobalSettings::searchPaths
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___searchPaths_5;

public:
	inline static int32_t get_offset_of_current_4() { return static_cast<int32_t>(offsetof(GlobalSettings_t344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1_StaticFields, ___current_4)); }
	inline GlobalSettings_t344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1 * get_current_4() const { return ___current_4; }
	inline GlobalSettings_t344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1 ** get_address_of_current_4() { return &___current_4; }
	inline void set_current_4(GlobalSettings_t344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1 * value)
	{
		___current_4 = value;
		Il2CppCodeGenWriteBarrier((&___current_4), value);
	}

	inline static int32_t get_offset_of_searchPaths_5() { return static_cast<int32_t>(offsetof(GlobalSettings_t344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1_StaticFields, ___searchPaths_5)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_searchPaths_5() const { return ___searchPaths_5; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_searchPaths_5() { return &___searchPaths_5; }
	inline void set_searchPaths_5(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___searchPaths_5 = value;
		Il2CppCodeGenWriteBarrier((&___searchPaths_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALSETTINGS_T344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef CONSTRAINEDPOINTSET_T03BDE7B20AE739D12D322FF59BE13768C23A50D8_H
#define CONSTRAINEDPOINTSET_T03BDE7B20AE739D12D322FF59BE13768C23A50D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Poly2Tri.ConstrainedPointSet
struct  ConstrainedPointSet_t03BDE7B20AE739D12D322FF59BE13768C23A50D8  : public PointSet_t9993025F4B37963DECB948F2ED89346430BA5EDD
{
public:
	// System.Collections.Generic.Dictionary`2<System.UInt32,Poly2Tri.TriangulationConstraint> Poly2Tri.ConstrainedPointSet::mConstraintMap
	Dictionary_2_tCDE048950A50CBAD73ABA536AFC71671996F63AC * ___mConstraintMap_14;
	// System.Collections.Generic.List`1<Poly2Tri.Contour> Poly2Tri.ConstrainedPointSet::mHoles
	List_1_t848BD4B8703132660D7117532F3ADD94076BDA26 * ___mHoles_15;

public:
	inline static int32_t get_offset_of_mConstraintMap_14() { return static_cast<int32_t>(offsetof(ConstrainedPointSet_t03BDE7B20AE739D12D322FF59BE13768C23A50D8, ___mConstraintMap_14)); }
	inline Dictionary_2_tCDE048950A50CBAD73ABA536AFC71671996F63AC * get_mConstraintMap_14() const { return ___mConstraintMap_14; }
	inline Dictionary_2_tCDE048950A50CBAD73ABA536AFC71671996F63AC ** get_address_of_mConstraintMap_14() { return &___mConstraintMap_14; }
	inline void set_mConstraintMap_14(Dictionary_2_tCDE048950A50CBAD73ABA536AFC71671996F63AC * value)
	{
		___mConstraintMap_14 = value;
		Il2CppCodeGenWriteBarrier((&___mConstraintMap_14), value);
	}

	inline static int32_t get_offset_of_mHoles_15() { return static_cast<int32_t>(offsetof(ConstrainedPointSet_t03BDE7B20AE739D12D322FF59BE13768C23A50D8, ___mHoles_15)); }
	inline List_1_t848BD4B8703132660D7117532F3ADD94076BDA26 * get_mHoles_15() const { return ___mHoles_15; }
	inline List_1_t848BD4B8703132660D7117532F3ADD94076BDA26 ** get_address_of_mHoles_15() { return &___mHoles_15; }
	inline void set_mHoles_15(List_1_t848BD4B8703132660D7117532F3ADD94076BDA26 * value)
	{
		___mHoles_15 = value;
		Il2CppCodeGenWriteBarrier((&___mHoles_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINEDPOINTSET_T03BDE7B20AE739D12D322FF59BE13768C23A50D8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef CLICKSPAWN_TC6E5B0D9B30AF893059782AE19FE6B7967896689_H
#define CLICKSPAWN_TC6E5B0D9B30AF893059782AE19FE6B7967896689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClickSpawn
struct  ClickSpawn_tC6E5B0D9B30AF893059782AE19FE6B7967896689  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject ClickSpawn::Brick
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Brick_4;

public:
	inline static int32_t get_offset_of_Brick_4() { return static_cast<int32_t>(offsetof(ClickSpawn_tC6E5B0D9B30AF893059782AE19FE6B7967896689, ___Brick_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Brick_4() const { return ___Brick_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Brick_4() { return &___Brick_4; }
	inline void set_Brick_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Brick_4 = value;
		Il2CppCodeGenWriteBarrier((&___Brick_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLICKSPAWN_TC6E5B0D9B30AF893059782AE19FE6B7967896689_H
#ifndef SPLINEWALKER_T3A16C6F00F7DF0CC8A3C9089CB38C6C1108E32ED_H
#define SPLINEWALKER_T3A16C6F00F7DF0CC8A3C9089CB38C6C1108E32ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kolibri2d.SplineWalker
struct  SplineWalker_t3A16C6F00F7DF0CC8A3C9089CB38C6C1108E32ED  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Kolibri2d.BezierSpline Kolibri2d.SplineWalker::spline
	BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * ___spline_4;
	// System.Single Kolibri2d.SplineWalker::duration
	float ___duration_5;
	// System.Boolean Kolibri2d.SplineWalker::lookForward
	bool ___lookForward_6;
	// System.Boolean Kolibri2d.SplineWalker::useLength
	bool ___useLength_7;
	// UnityEngine.Quaternion Kolibri2d.SplineWalker::startRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___startRotation_8;
	// Kolibri2d.SplineWalker/SplineWalkerMode Kolibri2d.SplineWalker::mode
	int32_t ___mode_9;
	// System.Boolean Kolibri2d.SplineWalker::goingForward
	bool ___goingForward_10;
	// System.Single Kolibri2d.SplineWalker::progress
	float ___progress_11;

public:
	inline static int32_t get_offset_of_spline_4() { return static_cast<int32_t>(offsetof(SplineWalker_t3A16C6F00F7DF0CC8A3C9089CB38C6C1108E32ED, ___spline_4)); }
	inline BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * get_spline_4() const { return ___spline_4; }
	inline BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE ** get_address_of_spline_4() { return &___spline_4; }
	inline void set_spline_4(BezierSpline_t869F2A9EF49C18DFF4B8549A50F7D2C1608713CE * value)
	{
		___spline_4 = value;
		Il2CppCodeGenWriteBarrier((&___spline_4), value);
	}

	inline static int32_t get_offset_of_duration_5() { return static_cast<int32_t>(offsetof(SplineWalker_t3A16C6F00F7DF0CC8A3C9089CB38C6C1108E32ED, ___duration_5)); }
	inline float get_duration_5() const { return ___duration_5; }
	inline float* get_address_of_duration_5() { return &___duration_5; }
	inline void set_duration_5(float value)
	{
		___duration_5 = value;
	}

	inline static int32_t get_offset_of_lookForward_6() { return static_cast<int32_t>(offsetof(SplineWalker_t3A16C6F00F7DF0CC8A3C9089CB38C6C1108E32ED, ___lookForward_6)); }
	inline bool get_lookForward_6() const { return ___lookForward_6; }
	inline bool* get_address_of_lookForward_6() { return &___lookForward_6; }
	inline void set_lookForward_6(bool value)
	{
		___lookForward_6 = value;
	}

	inline static int32_t get_offset_of_useLength_7() { return static_cast<int32_t>(offsetof(SplineWalker_t3A16C6F00F7DF0CC8A3C9089CB38C6C1108E32ED, ___useLength_7)); }
	inline bool get_useLength_7() const { return ___useLength_7; }
	inline bool* get_address_of_useLength_7() { return &___useLength_7; }
	inline void set_useLength_7(bool value)
	{
		___useLength_7 = value;
	}

	inline static int32_t get_offset_of_startRotation_8() { return static_cast<int32_t>(offsetof(SplineWalker_t3A16C6F00F7DF0CC8A3C9089CB38C6C1108E32ED, ___startRotation_8)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_startRotation_8() const { return ___startRotation_8; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_startRotation_8() { return &___startRotation_8; }
	inline void set_startRotation_8(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___startRotation_8 = value;
	}

	inline static int32_t get_offset_of_mode_9() { return static_cast<int32_t>(offsetof(SplineWalker_t3A16C6F00F7DF0CC8A3C9089CB38C6C1108E32ED, ___mode_9)); }
	inline int32_t get_mode_9() const { return ___mode_9; }
	inline int32_t* get_address_of_mode_9() { return &___mode_9; }
	inline void set_mode_9(int32_t value)
	{
		___mode_9 = value;
	}

	inline static int32_t get_offset_of_goingForward_10() { return static_cast<int32_t>(offsetof(SplineWalker_t3A16C6F00F7DF0CC8A3C9089CB38C6C1108E32ED, ___goingForward_10)); }
	inline bool get_goingForward_10() const { return ___goingForward_10; }
	inline bool* get_address_of_goingForward_10() { return &___goingForward_10; }
	inline void set_goingForward_10(bool value)
	{
		___goingForward_10 = value;
	}

	inline static int32_t get_offset_of_progress_11() { return static_cast<int32_t>(offsetof(SplineWalker_t3A16C6F00F7DF0CC8A3C9089CB38C6C1108E32ED, ___progress_11)); }
	inline float get_progress_11() const { return ___progress_11; }
	inline float* get_address_of_progress_11() { return &___progress_11; }
	inline void set_progress_11(float value)
	{
		___progress_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINEWALKER_T3A16C6F00F7DF0CC8A3C9089CB38C6C1108E32ED_H
#ifndef RENDERSORTER_T1995B4D6FD25F439BD094F743357CA5837D3EFF5_H
#define RENDERSORTER_T1995B4D6FD25F439BD094F743357CA5837D3EFF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.RenderSorter
struct  RenderSorter_t1995B4D6FD25F439BD094F743357CA5837D3EFF5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean SimplySVG.RenderSorter::autoUpdate
	bool ___autoUpdate_4;
	// System.Int32 SimplySVG.RenderSorter::sortingLayerID
	int32_t ___sortingLayerID_5;

public:
	inline static int32_t get_offset_of_autoUpdate_4() { return static_cast<int32_t>(offsetof(RenderSorter_t1995B4D6FD25F439BD094F743357CA5837D3EFF5, ___autoUpdate_4)); }
	inline bool get_autoUpdate_4() const { return ___autoUpdate_4; }
	inline bool* get_address_of_autoUpdate_4() { return &___autoUpdate_4; }
	inline void set_autoUpdate_4(bool value)
	{
		___autoUpdate_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayerID_5() { return static_cast<int32_t>(offsetof(RenderSorter_t1995B4D6FD25F439BD094F743357CA5837D3EFF5, ___sortingLayerID_5)); }
	inline int32_t get_sortingLayerID_5() const { return ___sortingLayerID_5; }
	inline int32_t* get_address_of_sortingLayerID_5() { return &___sortingLayerID_5; }
	inline void set_sortingLayerID_5(int32_t value)
	{
		___sortingLayerID_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERSORTER_T1995B4D6FD25F439BD094F743357CA5837D3EFF5_H
#ifndef RENDERERPROPERTIES_T555C76E6A95AE44EDB8A6E11DE8E158538E6E9C1_H
#define RENDERERPROPERTIES_T555C76E6A95AE44EDB8A6E11DE8E158538E6E9C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.RendererProperties
struct  RendererProperties_t555C76E6A95AE44EDB8A6E11DE8E158538E6E9C1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Renderer SimplySVG.RendererProperties::render
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___render_4;
	// System.Int32 SimplySVG.RendererProperties::layerId
	int32_t ___layerId_5;
	// System.Int32 SimplySVG.RendererProperties::order
	int32_t ___order_6;

public:
	inline static int32_t get_offset_of_render_4() { return static_cast<int32_t>(offsetof(RendererProperties_t555C76E6A95AE44EDB8A6E11DE8E158538E6E9C1, ___render_4)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_render_4() const { return ___render_4; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_render_4() { return &___render_4; }
	inline void set_render_4(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___render_4 = value;
		Il2CppCodeGenWriteBarrier((&___render_4), value);
	}

	inline static int32_t get_offset_of_layerId_5() { return static_cast<int32_t>(offsetof(RendererProperties_t555C76E6A95AE44EDB8A6E11DE8E158538E6E9C1, ___layerId_5)); }
	inline int32_t get_layerId_5() const { return ___layerId_5; }
	inline int32_t* get_address_of_layerId_5() { return &___layerId_5; }
	inline void set_layerId_5(int32_t value)
	{
		___layerId_5 = value;
	}

	inline static int32_t get_offset_of_order_6() { return static_cast<int32_t>(offsetof(RendererProperties_t555C76E6A95AE44EDB8A6E11DE8E158538E6E9C1, ___order_6)); }
	inline int32_t get_order_6() const { return ___order_6; }
	inline int32_t* get_address_of_order_6() { return &___order_6; }
	inline void set_order_6(int32_t value)
	{
		___order_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERERPROPERTIES_T555C76E6A95AE44EDB8A6E11DE8E158538E6E9C1_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#define GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Color_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RectTransform_9)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Canvas_11)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_DefaultUI_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifndef MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#define MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F  : public Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_MaskMaterial_22)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ParentMask_23)); }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Corners_29)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifndef SIMPLYSVGIMAGE_T247E560C3F0876C74DC8D235FECBDCB2F374BBE4_H
#define SIMPLYSVGIMAGE_T247E560C3F0876C74DC8D235FECBDCB2F374BBE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimplySVG.SimplySVGImage
struct  SimplySVGImage_t247E560C3F0876C74DC8D235FECBDCB2F374BBE4  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.Mesh SimplySVG.SimplySVGImage::graphicMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___graphicMesh_30;
	// System.Boolean SimplySVG.SimplySVGImage::preserveAspectRatio
	bool ___preserveAspectRatio_31;
	// System.Boolean SimplySVG.SimplySVGImage::useComplexHitCheck
	bool ___useComplexHitCheck_32;

public:
	inline static int32_t get_offset_of_graphicMesh_30() { return static_cast<int32_t>(offsetof(SimplySVGImage_t247E560C3F0876C74DC8D235FECBDCB2F374BBE4, ___graphicMesh_30)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_graphicMesh_30() const { return ___graphicMesh_30; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_graphicMesh_30() { return &___graphicMesh_30; }
	inline void set_graphicMesh_30(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___graphicMesh_30 = value;
		Il2CppCodeGenWriteBarrier((&___graphicMesh_30), value);
	}

	inline static int32_t get_offset_of_preserveAspectRatio_31() { return static_cast<int32_t>(offsetof(SimplySVGImage_t247E560C3F0876C74DC8D235FECBDCB2F374BBE4, ___preserveAspectRatio_31)); }
	inline bool get_preserveAspectRatio_31() const { return ___preserveAspectRatio_31; }
	inline bool* get_address_of_preserveAspectRatio_31() { return &___preserveAspectRatio_31; }
	inline void set_preserveAspectRatio_31(bool value)
	{
		___preserveAspectRatio_31 = value;
	}

	inline static int32_t get_offset_of_useComplexHitCheck_32() { return static_cast<int32_t>(offsetof(SimplySVGImage_t247E560C3F0876C74DC8D235FECBDCB2F374BBE4, ___useComplexHitCheck_32)); }
	inline bool get_useComplexHitCheck_32() const { return ___useComplexHitCheck_32; }
	inline bool* get_address_of_useComplexHitCheck_32() { return &___useComplexHitCheck_32; }
	inline void set_useComplexHitCheck_32(bool value)
	{
		___useComplexHitCheck_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLYSVGIMAGE_T247E560C3F0876C74DC8D235FECBDCB2F374BBE4_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2700[2] = 
{
	TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3::get_offset_of_transforms_0(),
	TransformAttributes_t02EDCB35162729433B60C3A5765296C0EC86D5A3::get_offset_of__combinedTransform_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (TransformCommand_t601BE25036B48F21884351AC0F5D1F039FC829E2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2701[8] = 
{
	TransformCommand_t601BE25036B48F21884351AC0F5D1F039FC829E2::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (UseElement_tFD190524412A3AD968B4BF37A4DB4AF17ED316E9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2702[3] = 
{
	UseElement_tFD190524412A3AD968B4BF37A4DB4AF17ED316E9::get_offset_of_localGraphicalAttributes_4(),
	UseElement_tFD190524412A3AD968B4BF37A4DB4AF17ED316E9::get_offset_of_localTransformAttributes_5(),
	UseElement_tFD190524412A3AD968B4BF37A4DB4AF17ED316E9::get_offset_of_surrogateForElement_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (DocumentParser_t80E81C53F90D9CD57A8868BD9237537C9822E78E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2703[2] = 
{
	DocumentParser_t80E81C53F90D9CD57A8868BD9237537C9822E78E::get_offset_of_currentDocument_0(),
	DocumentParser_t80E81C53F90D9CD57A8868BD9237537C9822E78E::get_offset_of_buildStack_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (GeneralUtilities_t7F02D838AFBDB4D3D1DA618DA65567911600B235), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (GlobalSettings_t344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1), -1, sizeof(GlobalSettings_t344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2705[7] = 
{
	GlobalSettings_t344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1_StaticFields::get_offset_of_current_4(),
	GlobalSettings_t344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1_StaticFields::get_offset_of_searchPaths_5(),
	GlobalSettings_t344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1::get_offset_of_defaultImportSettings_6(),
	GlobalSettings_t344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1::get_offset_of_defaultMaterial_7(),
	GlobalSettings_t344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1::get_offset_of_levelOfLog_8(),
	GlobalSettings_t344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1::get_offset_of_maxUnsupportedFeatureWarningCount_9(),
	GlobalSettings_t344D76E3D8ED60BBF90F3ADCBB8D018C29F511B1::get_offset_of_extraDevelopementChecks_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (LogLevel_tED853AD350810F77C640D354230906298D398A41)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2706[5] = 
{
	LogLevel_tED853AD350810F77C640D354230906298D398A41::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2707[7] = 
{
	ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3::get_offset_of_svgFile_4(),
	ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3::get_offset_of_splitMeshesByLayers_5(),
	ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3::get_offset_of__splitCollidersByLayers_6(),
	ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3::get_offset_of_scale_7(),
	ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3::get_offset_of_maxSubdivisonDepth_8(),
	ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3::get_offset_of_minSubdivisionDistanceDelta_9(),
	ImportSettings_t35FBA24C6BEC9D3DF27CC66281FDC5C93D7DB0E3::get_offset_of_pivot_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (DoublePoint_tD124BFF02A4B482F5C86257187A388BDD3033CAC)+ sizeof (RuntimeObject), sizeof(DoublePoint_tD124BFF02A4B482F5C86257187A388BDD3033CAC ), 0, 0 };
extern const int32_t g_FieldOffsetTable2708[2] = 
{
	DoublePoint_tD124BFF02A4B482F5C86257187A388BDD3033CAC::get_offset_of_X_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DoublePoint_tD124BFF02A4B482F5C86257187A388BDD3033CAC::get_offset_of_Y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2709[1] = 
{
	PolyTree_tDB7C36AE94547FD9C335828F75E16585F0A8CCA5::get_offset_of_m_AllPolys_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2710[7] = 
{
	PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB::get_offset_of_m_Parent_0(),
	PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB::get_offset_of_m_polygon_1(),
	PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB::get_offset_of_m_Index_2(),
	PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB::get_offset_of_m_jointype_3(),
	PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB::get_offset_of_m_endtype_4(),
	PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB::get_offset_of_m_Childs_5(),
	PolyNode_tEF7F0D00AB30829F935C4B6889A5C1BC79D654CB::get_offset_of_U3CIsOpenU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (Int128_t04B631F57681B0E656E6183DD9EF432AECB915BB)+ sizeof (RuntimeObject), sizeof(Int128_t04B631F57681B0E656E6183DD9EF432AECB915BB ), 0, 0 };
extern const int32_t g_FieldOffsetTable2711[2] = 
{
	Int128_t04B631F57681B0E656E6183DD9EF432AECB915BB::get_offset_of_hi_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Int128_t04B631F57681B0E656E6183DD9EF432AECB915BB::get_offset_of_lo_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB)+ sizeof (RuntimeObject), sizeof(IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB ), 0, 0 };
extern const int32_t g_FieldOffsetTable2712[2] = 
{
	IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB::get_offset_of_X_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IntPoint_tD93C7E113CF7FC6B665570E1DCBA8E258BC354EB::get_offset_of_Y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (IntRect_t8BD1DF410700349DD0195EBDEB436F5E36333B6D)+ sizeof (RuntimeObject), sizeof(IntRect_t8BD1DF410700349DD0195EBDEB436F5E36333B6D ), 0, 0 };
extern const int32_t g_FieldOffsetTable2713[4] = 
{
	IntRect_t8BD1DF410700349DD0195EBDEB436F5E36333B6D::get_offset_of_left_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IntRect_t8BD1DF410700349DD0195EBDEB436F5E36333B6D::get_offset_of_top_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IntRect_t8BD1DF410700349DD0195EBDEB436F5E36333B6D::get_offset_of_right_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IntRect_t8BD1DF410700349DD0195EBDEB436F5E36333B6D::get_offset_of_bottom_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (ClipType_t2491F122FD55AC747C5F6FAA007683A23BCA1DA3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2714[5] = 
{
	ClipType_t2491F122FD55AC747C5F6FAA007683A23BCA1DA3::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (PolyType_t3DE7C2E2777C89C827976C47D2E9E97A5F761AAB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2715[3] = 
{
	PolyType_t3DE7C2E2777C89C827976C47D2E9E97A5F761AAB::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (PolyFillType_t2EDC6B8C9F60F9513818AA9D517973836E0623B7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2716[5] = 
{
	PolyFillType_t2EDC6B8C9F60F9513818AA9D517973836E0623B7::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (JoinType_t2C94F67B2AC5A82B6C9F1E0A91FC5D592D54C6EE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2717[4] = 
{
	JoinType_t2C94F67B2AC5A82B6C9F1E0A91FC5D592D54C6EE::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (EndType_tC3D012CB12023E5FBBB34076C630B50B23636AD9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2718[6] = 
{
	EndType_tC3D012CB12023E5FBBB34076C630B50B23636AD9::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (EdgeSide_t7D9DB3581AC689985CF1A7CD6906A316B075C0BD)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2719[3] = 
{
	EdgeSide_t7D9DB3581AC689985CF1A7CD6906A316B075C0BD::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (Direction_tFBEFA40C69E60C31BFB92F3869B4DB5D378D0E5F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2720[3] = 
{
	Direction_tFBEFA40C69E60C31BFB92F3869B4DB5D378D0E5F::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2721[18] = 
{
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335::get_offset_of_Bot_0(),
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335::get_offset_of_Curr_1(),
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335::get_offset_of_Top_2(),
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335::get_offset_of_Delta_3(),
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335::get_offset_of_Dx_4(),
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335::get_offset_of_PolyTyp_5(),
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335::get_offset_of_Side_6(),
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335::get_offset_of_WindDelta_7(),
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335::get_offset_of_WindCnt_8(),
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335::get_offset_of_WindCnt2_9(),
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335::get_offset_of_OutIdx_10(),
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335::get_offset_of_Next_11(),
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335::get_offset_of_Prev_12(),
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335::get_offset_of_NextInLML_13(),
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335::get_offset_of_NextInAEL_14(),
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335::get_offset_of_PrevInAEL_15(),
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335::get_offset_of_NextInSEL_16(),
	TEdge_tA9D6BA578B80A7A1EEF29EDC76C629D9802C0335::get_offset_of_PrevInSEL_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (IntersectNode_tEE9AFAC0E8E407CC9A0601D7E3BD6907CDAC94D7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2722[3] = 
{
	IntersectNode_tEE9AFAC0E8E407CC9A0601D7E3BD6907CDAC94D7::get_offset_of_Edge1_0(),
	IntersectNode_tEE9AFAC0E8E407CC9A0601D7E3BD6907CDAC94D7::get_offset_of_Edge2_1(),
	IntersectNode_tEE9AFAC0E8E407CC9A0601D7E3BD6907CDAC94D7::get_offset_of_Pt_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (MyIntersectNodeSort_t331A7E1CE22C08205336AB3C973DFEE141F7E04A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (LocalMinima_t095BEF49CA303DCA0F6C660B4EFE330C55712D05), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2724[4] = 
{
	LocalMinima_t095BEF49CA303DCA0F6C660B4EFE330C55712D05::get_offset_of_Y_0(),
	LocalMinima_t095BEF49CA303DCA0F6C660B4EFE330C55712D05::get_offset_of_LeftBound_1(),
	LocalMinima_t095BEF49CA303DCA0F6C660B4EFE330C55712D05::get_offset_of_RightBound_2(),
	LocalMinima_t095BEF49CA303DCA0F6C660B4EFE330C55712D05::get_offset_of_Next_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (Scanbeam_t5B92102C6C777A0213DB5CB42DCBE782F0486AA7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2725[2] = 
{
	Scanbeam_t5B92102C6C777A0213DB5CB42DCBE782F0486AA7::get_offset_of_Y_0(),
	Scanbeam_t5B92102C6C777A0213DB5CB42DCBE782F0486AA7::get_offset_of_Next_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (OutRec_t18F55EE4090D9FE89ED98D5BFE06E49149E05B82), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2726[7] = 
{
	OutRec_t18F55EE4090D9FE89ED98D5BFE06E49149E05B82::get_offset_of_Idx_0(),
	OutRec_t18F55EE4090D9FE89ED98D5BFE06E49149E05B82::get_offset_of_IsHole_1(),
	OutRec_t18F55EE4090D9FE89ED98D5BFE06E49149E05B82::get_offset_of_IsOpen_2(),
	OutRec_t18F55EE4090D9FE89ED98D5BFE06E49149E05B82::get_offset_of_FirstLeft_3(),
	OutRec_t18F55EE4090D9FE89ED98D5BFE06E49149E05B82::get_offset_of_Pts_4(),
	OutRec_t18F55EE4090D9FE89ED98D5BFE06E49149E05B82::get_offset_of_BottomPt_5(),
	OutRec_t18F55EE4090D9FE89ED98D5BFE06E49149E05B82::get_offset_of_PolyNode_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2727[4] = 
{
	OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0::get_offset_of_Idx_0(),
	OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0::get_offset_of_Pt_1(),
	OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0::get_offset_of_Next_2(),
	OutPt_tE6765AB6CADCB00B2D65E07B470AD9FE451522F0::get_offset_of_Prev_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (Join_t06D45E32B54B183FB11898114F5C4ACFE43D18AA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2728[3] = 
{
	Join_t06D45E32B54B183FB11898114F5C4ACFE43D18AA::get_offset_of_OutPt1_0(),
	Join_t06D45E32B54B183FB11898114F5C4ACFE43D18AA::get_offset_of_OutPt2_1(),
	Join_t06D45E32B54B183FB11898114F5C4ACFE43D18AA::get_offset_of_OffPt_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (ClipperBase_tBFEE1941825FBF171B1F5EDB6DFC3247136BC4DD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2729[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	ClipperBase_tBFEE1941825FBF171B1F5EDB6DFC3247136BC4DD::get_offset_of_m_MinimaList_6(),
	ClipperBase_tBFEE1941825FBF171B1F5EDB6DFC3247136BC4DD::get_offset_of_m_CurrentLM_7(),
	ClipperBase_tBFEE1941825FBF171B1F5EDB6DFC3247136BC4DD::get_offset_of_m_edges_8(),
	ClipperBase_tBFEE1941825FBF171B1F5EDB6DFC3247136BC4DD::get_offset_of_m_UseFullRange_9(),
	ClipperBase_tBFEE1941825FBF171B1F5EDB6DFC3247136BC4DD::get_offset_of_m_HasOpenPaths_10(),
	ClipperBase_tBFEE1941825FBF171B1F5EDB6DFC3247136BC4DD::get_offset_of_U3CPreserveCollinearU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2730[18] = 
{
	0,
	0,
	0,
	Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F::get_offset_of_m_PolyOuts_15(),
	Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F::get_offset_of_m_ClipType_16(),
	Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F::get_offset_of_m_Scanbeam_17(),
	Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F::get_offset_of_m_ActiveEdges_18(),
	Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F::get_offset_of_m_SortedEdges_19(),
	Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F::get_offset_of_m_IntersectList_20(),
	Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F::get_offset_of_m_IntersectNodeComparer_21(),
	Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F::get_offset_of_m_ExecuteLocked_22(),
	Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F::get_offset_of_m_ClipFillType_23(),
	Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F::get_offset_of_m_SubjFillType_24(),
	Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F::get_offset_of_m_Joins_25(),
	Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F::get_offset_of_m_GhostJoins_26(),
	Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F::get_offset_of_m_UsingPolyTree_27(),
	Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F::get_offset_of_U3CReverseSolutionU3Ek__BackingField_28(),
	Clipper_t673A2B6C958A1A265B9ABDDF23C11DAFDC2B890F::get_offset_of_U3CStrictlySimpleU3Ek__BackingField_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (NodeType_tDD65844235B568E0ED7138B6AEFA77B6EAB5C325)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2731[4] = 
{
	NodeType_tDD65844235B568E0ED7138B6AEFA77B6EAB5C325::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2732[16] = 
{
	ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404::get_offset_of_m_destPolys_0(),
	ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404::get_offset_of_m_srcPoly_1(),
	ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404::get_offset_of_m_destPoly_2(),
	ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404::get_offset_of_m_normals_3(),
	ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404::get_offset_of_m_delta_4(),
	ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404::get_offset_of_m_sinA_5(),
	ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404::get_offset_of_m_sin_6(),
	ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404::get_offset_of_m_cos_7(),
	ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404::get_offset_of_m_miterLim_8(),
	ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404::get_offset_of_m_StepsPerRad_9(),
	ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404::get_offset_of_m_lowest_10(),
	ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404::get_offset_of_m_polyNodes_11(),
	ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404::get_offset_of_U3CArcToleranceU3Ek__BackingField_12(),
	ClipperOffset_t9C6F09F33078744685F1BAA19177374D27FBD404::get_offset_of_U3CMiterLimitU3Ek__BackingField_13(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (ClipperException_tBB767771C8FA84C664383A6C5392C34BAC2FB72C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2734[7] = 
{
	Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78::get_offset_of_rows_0(),
	Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78::get_offset_of_cols_1(),
	Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78::get_offset_of_mat_2(),
	Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78::get_offset_of_L_3(),
	Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78::get_offset_of_U_4(),
	Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78::get_offset_of_pi_5(),
	Matrix_t4D8BDA2D6D897306CFB8B5F3959E06BBF6CF4B78::get_offset_of_detOfP_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (MException_tC61D13FC7CF7B038EB7ADA1E1750BC0DA5D3B2E1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (P2T_t995DE20560740D68D8F96C08687068A108F5C43D), -1, sizeof(P2T_t995DE20560740D68D8F96C08687068A108F5C43D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2736[1] = 
{
	P2T_t995DE20560740D68D8F96C08687068A108F5C43D_StaticFields::get_offset_of__defaultAlgorithm_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2737[5] = 
{
	DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891::get_offset_of_Points_0(),
	DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891::get_offset_of_Neighbors_1(),
	DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891::get_offset_of_mEdgeIsConstrained_2(),
	DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891::get_offset_of_EdgeIsDelaunay_3(),
	DelaunayTriangle_tE26AE625198E80D12E2C9BFAE4A5C39CE8E86891::get_offset_of_U3CIsInteriorU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (AdvancingFront_t103C9271D0DE07CA7D45EEDBE2ACBD93DC215340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2738[3] = 
{
	AdvancingFront_t103C9271D0DE07CA7D45EEDBE2ACBD93DC215340::get_offset_of_Head_0(),
	AdvancingFront_t103C9271D0DE07CA7D45EEDBE2ACBD93DC215340::get_offset_of_Tail_1(),
	AdvancingFront_t103C9271D0DE07CA7D45EEDBE2ACBD93DC215340::get_offset_of_Search_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2739[5] = 
{
	AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F::get_offset_of_Next_0(),
	AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F::get_offset_of_Prev_1(),
	AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F::get_offset_of_Value_2(),
	AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F::get_offset_of_Point_3(),
	AdvancingFrontNode_tFE256DFD285C81E1B62206CA862BDA8244B0320F::get_offset_of_Triangle_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (DTSweep_tAAC956F1CCE370C975CF22F70BC6A973A388B5AE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2740[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (DTSweepBasin_t74AB6315A3629FEF0FC85456015FA9537C67B2DD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2741[5] = 
{
	DTSweepBasin_t74AB6315A3629FEF0FC85456015FA9537C67B2DD::get_offset_of_leftNode_0(),
	DTSweepBasin_t74AB6315A3629FEF0FC85456015FA9537C67B2DD::get_offset_of_bottomNode_1(),
	DTSweepBasin_t74AB6315A3629FEF0FC85456015FA9537C67B2DD::get_offset_of_rightNode_2(),
	DTSweepBasin_t74AB6315A3629FEF0FC85456015FA9537C67B2DD::get_offset_of_width_3(),
	DTSweepBasin_t74AB6315A3629FEF0FC85456015FA9537C67B2DD::get_offset_of_leftHighest_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (DTSweepConstraint_t8C5C973CF68522DA5D7828F45161FE7A22203819), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (DTSweepContext_t3B606C92EEA406F37559300636C1AFE7E1D9FD7F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2743[7] = 
{
	DTSweepContext_t3B606C92EEA406F37559300636C1AFE7E1D9FD7F::get_offset_of_ALPHA_7(),
	DTSweepContext_t3B606C92EEA406F37559300636C1AFE7E1D9FD7F::get_offset_of_Front_8(),
	DTSweepContext_t3B606C92EEA406F37559300636C1AFE7E1D9FD7F::get_offset_of_U3CHeadU3Ek__BackingField_9(),
	DTSweepContext_t3B606C92EEA406F37559300636C1AFE7E1D9FD7F::get_offset_of_U3CTailU3Ek__BackingField_10(),
	DTSweepContext_t3B606C92EEA406F37559300636C1AFE7E1D9FD7F::get_offset_of_Basin_11(),
	DTSweepContext_t3B606C92EEA406F37559300636C1AFE7E1D9FD7F::get_offset_of_EdgeEvent_12(),
	DTSweepContext_t3B606C92EEA406F37559300636C1AFE7E1D9FD7F::get_offset_of__comparator_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (DTSweepDebugContext_tC024E42403443A0C8F8B910E52B07246494C19AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2744[5] = 
{
	DTSweepDebugContext_tC024E42403443A0C8F8B910E52B07246494C19AD::get_offset_of__primaryTriangle_1(),
	DTSweepDebugContext_tC024E42403443A0C8F8B910E52B07246494C19AD::get_offset_of__secondaryTriangle_2(),
	DTSweepDebugContext_tC024E42403443A0C8F8B910E52B07246494C19AD::get_offset_of__activePoint_3(),
	DTSweepDebugContext_tC024E42403443A0C8F8B910E52B07246494C19AD::get_offset_of__activeNode_4(),
	DTSweepDebugContext_tC024E42403443A0C8F8B910E52B07246494C19AD::get_offset_of__activeConstraint_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (DTSweepEdgeEvent_tBD7A886997C86280A3AE982B4E8C86325A28E422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2745[2] = 
{
	DTSweepEdgeEvent_tBD7A886997C86280A3AE982B4E8C86325A28E422::get_offset_of_ConstrainedEdge_0(),
	DTSweepEdgeEvent_tBD7A886997C86280A3AE982B4E8C86325A28E422::get_offset_of_Right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (DTSweepPointComparator_tFA3FA00C634B6162D20BD76BE5E4C0CDB943ACAC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (PointOnEdgeException_t5FCE6A85ADB94A50C5B3276A26B1AC1ADE0ACDE7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2747[3] = 
{
	PointOnEdgeException_t5FCE6A85ADB94A50C5B3276A26B1AC1ADE0ACDE7::get_offset_of_A_11(),
	PointOnEdgeException_t5FCE6A85ADB94A50C5B3276A26B1AC1ADE0ACDE7::get_offset_of_B_12(),
	PointOnEdgeException_t5FCE6A85ADB94A50C5B3276A26B1AC1ADE0ACDE7::get_offset_of_C_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (Orientation_t30EEBFB4BCE8E9AE35551FCD92D698CA8A2A6ADC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2749[4] = 
{
	Orientation_t30EEBFB4BCE8E9AE35551FCD92D698CA8A2A6ADC::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (Contour_t920290958781337055636452F24A9DB2E3EDE246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2750[3] = 
{
	Contour_t920290958781337055636452F24A9DB2E3EDE246::get_offset_of_mHoles_7(),
	Contour_t920290958781337055636452F24A9DB2E3EDE246::get_offset_of_mParent_8(),
	Contour_t920290958781337055636452F24A9DB2E3EDE246::get_offset_of_mName_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (Polygon_t84A5698D8CA59521DC90C4FF460F21DAB85C0F67), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2751[10] = 
{
	Polygon_t84A5698D8CA59521DC90C4FF460F21DAB85C0F67::get_offset_of_mPointMap_7(),
	Polygon_t84A5698D8CA59521DC90C4FF460F21DAB85C0F67::get_offset_of_mTriangles_8(),
	Polygon_t84A5698D8CA59521DC90C4FF460F21DAB85C0F67::get_offset_of_U3CFileNameU3Ek__BackingField_9(),
	Polygon_t84A5698D8CA59521DC90C4FF460F21DAB85C0F67::get_offset_of_U3CDisplayFlipXU3Ek__BackingField_10(),
	Polygon_t84A5698D8CA59521DC90C4FF460F21DAB85C0F67::get_offset_of_U3CDisplayFlipYU3Ek__BackingField_11(),
	Polygon_t84A5698D8CA59521DC90C4FF460F21DAB85C0F67::get_offset_of_U3CDisplayRotateU3Ek__BackingField_12(),
	Polygon_t84A5698D8CA59521DC90C4FF460F21DAB85C0F67::get_offset_of_mPrecision_13(),
	Polygon_t84A5698D8CA59521DC90C4FF460F21DAB85C0F67::get_offset_of_mHoles_14(),
	Polygon_t84A5698D8CA59521DC90C4FF460F21DAB85C0F67::get_offset_of_mSteinerPoints_15(),
	Polygon_t84A5698D8CA59521DC90C4FF460F21DAB85C0F67::get_offset_of__last_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (PolygonPoint_tAED3172793E804A09A2EB2FEDCB0EBA5159A08FE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2752[2] = 
{
	PolygonPoint_tAED3172793E804A09A2EB2FEDCB0EBA5159A08FE::get_offset_of_U3CNextU3Ek__BackingField_5(),
	PolygonPoint_tAED3172793E804A09A2EB2FEDCB0EBA5159A08FE::get_offset_of_U3CPreviousU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (PolygonSet_t66D579451ECDA6DEB2755D1383A06FE8914FD6C3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2753[1] = 
{
	PolygonSet_t66D579451ECDA6DEB2755D1383A06FE8914FD6C3::get_offset_of__polygons_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (PolygonUtil_t854384351A9165F158DDB413A69F665AD048FF00), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { sizeof (PolyUnionError_tCAD15B7CD739741931652066204CC031F2030830)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2755[5] = 
{
	PolyUnionError_tCAD15B7CD739741931652066204CC031F2030830::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (PolyOperation_t5DF9AFFF3649735ED0CB9E58180D6DBC69B798E8)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2756[5] = 
{
	PolyOperation_t5DF9AFFF3649735ED0CB9E58180D6DBC69B798E8::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (EdgeIntersectInfo_t1E5A929A2A5E7DD8E210A1D3244811BCC136D3E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2757[3] = 
{
	EdgeIntersectInfo_t1E5A929A2A5E7DD8E210A1D3244811BCC136D3E1::get_offset_of_U3CEdgeOneU3Ek__BackingField_0(),
	EdgeIntersectInfo_t1E5A929A2A5E7DD8E210A1D3244811BCC136D3E1::get_offset_of_U3CEdgeTwoU3Ek__BackingField_1(),
	EdgeIntersectInfo_t1E5A929A2A5E7DD8E210A1D3244811BCC136D3E1::get_offset_of_U3CIntersectionPointU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (SplitComplexPolygonNode_t070AE02F70843A40F704E20203026E42EF3DC7DB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2758[2] = 
{
	SplitComplexPolygonNode_t070AE02F70843A40F704E20203026E42EF3DC7DB::get_offset_of_mConnected_0(),
	SplitComplexPolygonNode_t070AE02F70843A40F704E20203026E42EF3DC7DB::get_offset_of_mPosition_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (PolygonOperationContext_t2916CE11B6DF5A42CCE77760F90B1EF258621A11), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2759[11] = 
{
	PolygonOperationContext_t2916CE11B6DF5A42CCE77760F90B1EF258621A11::get_offset_of_mOperations_0(),
	PolygonOperationContext_t2916CE11B6DF5A42CCE77760F90B1EF258621A11::get_offset_of_mOriginalPolygon1_1(),
	PolygonOperationContext_t2916CE11B6DF5A42CCE77760F90B1EF258621A11::get_offset_of_mOriginalPolygon2_2(),
	PolygonOperationContext_t2916CE11B6DF5A42CCE77760F90B1EF258621A11::get_offset_of_mPoly1_3(),
	PolygonOperationContext_t2916CE11B6DF5A42CCE77760F90B1EF258621A11::get_offset_of_mPoly2_4(),
	PolygonOperationContext_t2916CE11B6DF5A42CCE77760F90B1EF258621A11::get_offset_of_mIntersections_5(),
	PolygonOperationContext_t2916CE11B6DF5A42CCE77760F90B1EF258621A11::get_offset_of_mStartingIndex_6(),
	PolygonOperationContext_t2916CE11B6DF5A42CCE77760F90B1EF258621A11::get_offset_of_mError_7(),
	PolygonOperationContext_t2916CE11B6DF5A42CCE77760F90B1EF258621A11::get_offset_of_mPoly1VectorAngles_8(),
	PolygonOperationContext_t2916CE11B6DF5A42CCE77760F90B1EF258621A11::get_offset_of_mPoly2VectorAngles_9(),
	PolygonOperationContext_t2916CE11B6DF5A42CCE77760F90B1EF258621A11::get_offset_of_mOutput_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (ConstrainedPointSet_t03BDE7B20AE739D12D322FF59BE13768C23A50D8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2760[2] = 
{
	ConstrainedPointSet_t03BDE7B20AE739D12D322FF59BE13768C23A50D8::get_offset_of_mConstraintMap_14(),
	ConstrainedPointSet_t03BDE7B20AE739D12D322FF59BE13768C23A50D8::get_offset_of_mHoles_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (PointSet_t9993025F4B37963DECB948F2ED89346430BA5EDD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2761[7] = 
{
	PointSet_t9993025F4B37963DECB948F2ED89346430BA5EDD::get_offset_of_mPointMap_7(),
	PointSet_t9993025F4B37963DECB948F2ED89346430BA5EDD::get_offset_of_U3CTrianglesU3Ek__BackingField_8(),
	PointSet_t9993025F4B37963DECB948F2ED89346430BA5EDD::get_offset_of_U3CFileNameU3Ek__BackingField_9(),
	PointSet_t9993025F4B37963DECB948F2ED89346430BA5EDD::get_offset_of_U3CDisplayFlipXU3Ek__BackingField_10(),
	PointSet_t9993025F4B37963DECB948F2ED89346430BA5EDD::get_offset_of_U3CDisplayFlipYU3Ek__BackingField_11(),
	PointSet_t9993025F4B37963DECB948F2ED89346430BA5EDD::get_offset_of_U3CDisplayRotateU3Ek__BackingField_12(),
	PointSet_t9993025F4B37963DECB948F2ED89346430BA5EDD::get_offset_of_mPrecision_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (TriangulationAlgorithm_t163C57AE860101458013AB67507A951DDCC64358)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2762[2] = 
{
	TriangulationAlgorithm_t163C57AE860101458013AB67507A951DDCC64358::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (Edge_t5170DF73FC08083B2752D6AD8F039BF371D0B79B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2763[2] = 
{
	Edge_t5170DF73FC08083B2752D6AD8F039BF371D0B79B::get_offset_of_mP_0(),
	Edge_t5170DF73FC08083B2752D6AD8F039BF371D0B79B::get_offset_of_mQ_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (TriangulationConstraint_t420CE8B0E6C488B8F08766FBC3E6FAC5E5D6EC56), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2764[1] = 
{
	TriangulationConstraint_t420CE8B0E6C488B8F08766FBC3E6FAC5E5D6EC56::get_offset_of_mContraintCode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (TriangulationContext_t0153B91CA7F38202C7CC645BC07A67774802360E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2765[7] = 
{
	TriangulationContext_t0153B91CA7F38202C7CC645BC07A67774802360E::get_offset_of_U3CDebugContextU3Ek__BackingField_0(),
	TriangulationContext_t0153B91CA7F38202C7CC645BC07A67774802360E::get_offset_of_Triangles_1(),
	TriangulationContext_t0153B91CA7F38202C7CC645BC07A67774802360E::get_offset_of_Points_2(),
	TriangulationContext_t0153B91CA7F38202C7CC645BC07A67774802360E::get_offset_of_U3CTriangulationModeU3Ek__BackingField_3(),
	TriangulationContext_t0153B91CA7F38202C7CC645BC07A67774802360E::get_offset_of_U3CTriangulatableU3Ek__BackingField_4(),
	TriangulationContext_t0153B91CA7F38202C7CC645BC07A67774802360E::get_offset_of_U3CStepCountU3Ek__BackingField_5(),
	TriangulationContext_t0153B91CA7F38202C7CC645BC07A67774802360E::get_offset_of_U3CIsDebugEnabledU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (TriangulationDebugContext_tF9458C2C5E3E324954C544E9FB4044AC0B1F103C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2766[1] = 
{
	TriangulationDebugContext_tF9458C2C5E3E324954C544E9FB4044AC0B1F103C::get_offset_of__tcx_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (TriangulationMode_t17461F5B4793A10FA487438CB6D800E8E2A71364)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2767[4] = 
{
	TriangulationMode_t17461F5B4793A10FA487438CB6D800E8E2A71364::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4), -1, sizeof(TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2768[3] = 
{
	TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4_StaticFields::get_offset_of_kVertexCodeDefaultPrecision_2(),
	TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4::get_offset_of_mVertexCode_3(),
	TriangulationPoint_tA0795727BB94BE27AD7C04A99743A8F08EA31BE4::get_offset_of_U3CEdgesU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { sizeof (TriangulationPointEnumerator_tC5A4E04C5682F9A1BD78808A42042C9969A10609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2769[2] = 
{
	TriangulationPointEnumerator_tC5A4E04C5682F9A1BD78808A42042C9969A10609::get_offset_of_mPoints_0(),
	TriangulationPointEnumerator_tC5A4E04C5682F9A1BD78808A42042C9969A10609::get_offset_of_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { sizeof (TriangulationPointList_t3E00E8C9DBA64904AF6C882161F4F5523C74DAD6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { sizeof (PointGenerator_t57149E147F6F1DBB274762EA8C64BA73EF735A2B), -1, sizeof(PointGenerator_t57149E147F6F1DBB274762EA8C64BA73EF735A2B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2771[1] = 
{
	PointGenerator_t57149E147F6F1DBB274762EA8C64BA73EF735A2B_StaticFields::get_offset_of_RNG_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { sizeof (PolygonGenerator_tFE93FBCD78073D16685E5685256C52EF35089A0F), -1, sizeof(PolygonGenerator_tFE93FBCD78073D16685E5685256C52EF35089A0F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2772[2] = 
{
	PolygonGenerator_tFE93FBCD78073D16685E5685256C52EF35089A0F_StaticFields::get_offset_of_RNG_0(),
	PolygonGenerator_tFE93FBCD78073D16685E5685256C52EF35089A0F_StaticFields::get_offset_of_PI_2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { sizeof (TriangulationUtil_t078C0A44D64E4A422214F18F979B5215B4ED525F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2774[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2775[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { sizeof (FixedBitArray3_tB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB)+ sizeof (RuntimeObject), sizeof(FixedBitArray3_tB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2776[3] = 
{
	FixedBitArray3_tB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB::get_offset_of__0_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FixedBitArray3_tB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB::get_offset_of__1_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FixedBitArray3_tB39B6760CA6D5F0FA0E02B523B27BE1AECBCECCB::get_offset_of__2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (U3CEnumerateU3Ec__Iterator0_tD1CF558864545C490E7676298C409E31FCBC6302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2777[5] = 
{
	U3CEnumerateU3Ec__Iterator0_tD1CF558864545C490E7676298C409E31FCBC6302::get_offset_of_U3CiU3E__1_0(),
	U3CEnumerateU3Ec__Iterator0_tD1CF558864545C490E7676298C409E31FCBC6302::get_offset_of_U24this_1(),
	U3CEnumerateU3Ec__Iterator0_tD1CF558864545C490E7676298C409E31FCBC6302::get_offset_of_U24current_2(),
	U3CEnumerateU3Ec__Iterator0_tD1CF558864545C490E7676298C409E31FCBC6302::get_offset_of_U24disposing_3(),
	U3CEnumerateU3Ec__Iterator0_tD1CF558864545C490E7676298C409E31FCBC6302::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { sizeof (MathUtil_t392B236AFF869A2A4CA195686E23ECC003EEA716), -1, sizeof(MathUtil_t392B236AFF869A2A4CA195686E23ECC003EEA716_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2778[1] = 
{
	MathUtil_t392B236AFF869A2A4CA195686E23ECC003EEA716_StaticFields::get_offset_of_EPSILON_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { sizeof (Point2D_t35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2779[2] = 
{
	Point2D_t35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF::get_offset_of_mX_0(),
	Point2D_t35FEB0FD013ABBDB96E2C6C837B34C803FB30FBF::get_offset_of_mY_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { sizeof (Point2DEnumerator_tBD1ADF39FDB96DFB355BF956B6E37A79D89A516A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2780[2] = 
{
	Point2DEnumerator_tBD1ADF39FDB96DFB355BF956B6E37A79D89A516A::get_offset_of_mPoints_0(),
	Point2DEnumerator_tBD1ADF39FDB96DFB355BF956B6E37A79D89A516A::get_offset_of_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { sizeof (Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45), -1, sizeof(Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2781[7] = 
{
	Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45_StaticFields::get_offset_of_kMaxPolygonVertices_0(),
	Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45_StaticFields::get_offset_of_kLinearSlop_1(),
	Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45_StaticFields::get_offset_of_kAngularSlop_2(),
	Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45::get_offset_of_mPoints_3(),
	Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45::get_offset_of_mBoundingBox_4(),
	Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45::get_offset_of_mWindingOrder_5(),
	Point2DList_tDDD9EF404EEA2A75C8CB6AC6CE4F26E10A672C45::get_offset_of_mEpsilon_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (WindingOrderType_t48C8C779542915FBAAC9F555D87454615A9D01C3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2782[5] = 
{
	WindingOrderType_t48C8C779542915FBAAC9F555D87454615A9D01C3::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (PolygonError_t7B4ACD61D08175DFF8B72E05C1DD743FFF8D3D1D)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2783[10] = 
{
	PolygonError_t7B4ACD61D08175DFF8B72E05C1DD743FFF8D3D1D::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (Rect2D_t72E37904F5FB0718F0E3D96A0EBC0A77E80F6001), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2784[4] = 
{
	Rect2D_t72E37904F5FB0718F0E3D96A0EBC0A77E80F6001::get_offset_of_mMinX_0(),
	Rect2D_t72E37904F5FB0718F0E3D96A0EBC0A77E80F6001::get_offset_of_mMaxX_1(),
	Rect2D_t72E37904F5FB0718F0E3D96A0EBC0A77E80F6001::get_offset_of_mMinY_2(),
	Rect2D_t72E37904F5FB0718F0E3D96A0EBC0A77E80F6001::get_offset_of_mMaxY_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (MatrixUtils_tB189C49C53E0A4EEA0359B5FCFF168F406FE8F53), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { sizeof (ImportUtilities_t2410B5A4A6B89F70236A9E0910E77646D16B1249), -1, sizeof(ImportUtilities_t2410B5A4A6B89F70236A9E0910E77646D16B1249_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2786[2] = 
{
	ImportUtilities_t2410B5A4A6B89F70236A9E0910E77646D16B1249_StaticFields::get_offset_of_wps_0(),
	ImportUtilities_t2410B5A4A6B89F70236A9E0910E77646D16B1249_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { sizeof (SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2787[13] = 
{
	SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC::get_offset_of_name_0(),
	SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC::get_offset_of_importSettings_1(),
	SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC::get_offset_of_svgFile_2(),
	SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC::get_offset_of_document_3(),
	SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC::get_offset_of_svgDocumentLayers_4(),
	SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC::get_offset_of_svgData_5(),
	SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC::get_offset_of_mesh_6(),
	SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC::get_offset_of_meshLayers_7(),
	SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC::get_offset_of_names_8(),
	SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC::get_offset_of_collisionShapeData_9(),
	SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC::get_offset_of_collisionShapeDataLayers_10(),
	SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC::get_offset_of_errors_11(),
	SimplySVGImporter_tEC08895B75B240823232FABEEC86D3F31F13D9FC::get_offset_of_unsupportedElements_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { sizeof (TriangulationUtility_t8B071A46EB7BE50305CF04A1C97737D7887F3F0F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (RendererProperties_t555C76E6A95AE44EDB8A6E11DE8E158538E6E9C1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2789[3] = 
{
	RendererProperties_t555C76E6A95AE44EDB8A6E11DE8E158538E6E9C1::get_offset_of_render_4(),
	RendererProperties_t555C76E6A95AE44EDB8A6E11DE8E158538E6E9C1::get_offset_of_layerId_5(),
	RendererProperties_t555C76E6A95AE44EDB8A6E11DE8E158538E6E9C1::get_offset_of_order_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { sizeof (RenderSorter_t1995B4D6FD25F439BD094F743357CA5837D3EFF5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2790[2] = 
{
	RenderSorter_t1995B4D6FD25F439BD094F743357CA5837D3EFF5::get_offset_of_autoUpdate_4(),
	RenderSorter_t1995B4D6FD25F439BD094F743357CA5837D3EFF5::get_offset_of_sortingLayerID_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (SimplySVGImage_t247E560C3F0876C74DC8D235FECBDCB2F374BBE4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2791[3] = 
{
	SimplySVGImage_t247E560C3F0876C74DC8D235FECBDCB2F374BBE4::get_offset_of_graphicMesh_30(),
	SimplySVGImage_t247E560C3F0876C74DC8D235FECBDCB2F374BBE4::get_offset_of_preserveAspectRatio_31(),
	SimplySVGImage_t247E560C3F0876C74DC8D235FECBDCB2F374BBE4::get_offset_of_useComplexHitCheck_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (ClickSpawn_tC6E5B0D9B30AF893059782AE19FE6B7967896689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2792[1] = 
{
	ClickSpawn_tC6E5B0D9B30AF893059782AE19FE6B7967896689::get_offset_of_Brick_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (SplineWalker_t3A16C6F00F7DF0CC8A3C9089CB38C6C1108E32ED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2793[8] = 
{
	SplineWalker_t3A16C6F00F7DF0CC8A3C9089CB38C6C1108E32ED::get_offset_of_spline_4(),
	SplineWalker_t3A16C6F00F7DF0CC8A3C9089CB38C6C1108E32ED::get_offset_of_duration_5(),
	SplineWalker_t3A16C6F00F7DF0CC8A3C9089CB38C6C1108E32ED::get_offset_of_lookForward_6(),
	SplineWalker_t3A16C6F00F7DF0CC8A3C9089CB38C6C1108E32ED::get_offset_of_useLength_7(),
	SplineWalker_t3A16C6F00F7DF0CC8A3C9089CB38C6C1108E32ED::get_offset_of_startRotation_8(),
	SplineWalker_t3A16C6F00F7DF0CC8A3C9089CB38C6C1108E32ED::get_offset_of_mode_9(),
	SplineWalker_t3A16C6F00F7DF0CC8A3C9089CB38C6C1108E32ED::get_offset_of_goingForward_10(),
	SplineWalker_t3A16C6F00F7DF0CC8A3C9089CB38C6C1108E32ED::get_offset_of_progress_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (SplineWalkerMode_t33BC6B2D75BA1E2A80FC5EA299FB63258ED3FD63)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2794[4] = 
{
	SplineWalkerMode_t33BC6B2D75BA1E2A80FC5EA299FB63258ED3FD63::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A), -1, sizeof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2795[2] = 
{
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U24fieldU2D6DE680C155B94A30FA53CADC8E86B835051FE7E8_0(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U24fieldU2D92DC78AF759B423C04558A0433EDDBBA32C620F2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (U24ArrayTypeU3D1028_tBEAF9EA97A5E11573FD162B05E20BE1EBF3CBB76)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D1028_tBEAF9EA97A5E11573FD162B05E20BE1EBF3CBB76 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (U24ArrayTypeU3D8_t20F7F847C95E66FB8D8AF41A24396DBF4B4A58D1)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D8_t20F7F847C95E66FB8D8AF41A24396DBF4B4A58D1 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
