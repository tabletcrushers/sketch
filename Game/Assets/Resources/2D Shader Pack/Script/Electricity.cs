﻿using UnityEngine;

public class Electricity : MonoBehaviour
{
	public enum ETransparentMode { ETM_Blend = 0, ETM_Add };
	public ETransparentMode m_TransMode = ETransparentMode.ETM_Add;
	public Color m_Color = Color.white;
	Noise3D m_Noise = new Noise3D ();
	Renderer m_Rd;
	
    void Start ()
	{
		m_Rd = GetComponent<Renderer> ();
		m_Noise.Create (64, 2);
	}
	void Update ()
	{
		if (m_TransMode == ETransparentMode.ETM_Add)
		{
			m_Rd.material.SetInt ("_BlendSrc", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
			m_Rd.material.SetInt ("_BlendDst", (int)UnityEngine.Rendering.BlendMode.One);
		}
		else if (m_TransMode == ETransparentMode.ETM_Blend)
		{
			m_Rd.material.SetInt ("_BlendSrc", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
			m_Rd.material.SetInt ("_BlendDst", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
		}
		m_Rd.material.SetTexture ("_NoiseTex", m_Noise.Get());
		m_Rd.material.SetColor ("_Color", m_Color);
	}
}
