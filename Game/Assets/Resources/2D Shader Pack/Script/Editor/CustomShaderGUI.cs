﻿using UnityEngine;
using UnityEditor;

public class CustomShaderGUI : ShaderGUI
{
	enum EBlendMode
	{
		SpriteBlend,
		TraditionalBlend,
		Unknown,
	}
	EBlendMode m_Func = EBlendMode.SpriteBlend;

	public override void OnGUI (MaterialEditor me, MaterialProperty[] props)
	{
		Material mat = me.target as Material;
		UnityEngine.Rendering.BlendMode src = (UnityEngine.Rendering.BlendMode)mat.GetInt ("_BlendSrc");
		UnityEngine.Rendering.BlendMode dst = (UnityEngine.Rendering.BlendMode)mat.GetInt ("_BlendDst");
		if (src == UnityEngine.Rendering.BlendMode.One && dst == UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha)
			m_Func = EBlendMode.SpriteBlend;
		else if (src == UnityEngine.Rendering.BlendMode.SrcAlpha && dst == UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha)
			m_Func = EBlendMode.TraditionalBlend;
		else
			m_Func = EBlendMode.Unknown;
	
		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Blend Mode:");
		EditorGUI.BeginChangeCheck ();
		m_Func = (EBlendMode)EditorGUILayout.EnumPopup (m_Func);
		if (EditorGUI.EndChangeCheck())
		{
			if (m_Func == EBlendMode.SpriteBlend)
			{
				mat.SetInt ("_BlendSrc", (int)UnityEngine.Rendering.BlendMode.One);
				mat.SetInt ("_BlendDst", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
			}
			else if (m_Func == EBlendMode.TraditionalBlend)
			{
				mat.SetInt ("_BlendSrc", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
				mat.SetInt ("_BlendDst", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
			}
        }
		EditorGUILayout.EndHorizontal ();
		if (m_Func == EBlendMode.Unknown)
		{
			GUI.color = Color.yellow;
			EditorGUILayout.LabelField ("Unknown Blend Mode !!");
			GUI.color = Color.white;
		}
		EditorGUILayout.Space ();
		EditorGUILayout.Space ();
		base.OnGUI (me, props);
	}
}