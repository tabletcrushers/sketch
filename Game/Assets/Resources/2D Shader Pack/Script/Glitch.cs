﻿using UnityEngine;

public class Glitch : MonoBehaviour
{
	public float m_IntervalSeconds = 0.05f;
	[Range(0f, 1f)] public float m_DispProbability = 0.3f;
	public float m_DispIntensity = 0.05f;
	[Range(0f, 1f)] public float m_ColorProbability = 0.3f;
	public float m_ColorIntensity = 0.03f;
	Material m_Mat;

	void Awake ()
	{
		m_Mat = GetComponent<SpriteRenderer>().material;
		m_Mat.shader = Shader.Find ("2D Shader Pack/Glitch");
	}
	void Start ()
	{}
	void Update ()
	{
		m_Mat.SetFloat ("_GlitchInterval", m_IntervalSeconds);
		m_Mat.SetFloat ("_DispProbability", m_DispProbability);
		m_Mat.SetFloat ("_DispIntensity", m_DispIntensity);
		m_Mat.SetFloat ("_ColorProbability", m_ColorProbability);
		m_Mat.SetFloat ("_ColorIntensity", m_ColorIntensity);
	}
}