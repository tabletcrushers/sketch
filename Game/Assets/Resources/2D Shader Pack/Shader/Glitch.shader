﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "2D Shader Pack/Glitch" {
	Properties {
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		[MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
		[Header(Glitch)]
		_GlitchInterval   ("Glitch interval time [seconds]", Float) = 0.16
		[Header(Displacement Glitch)]
		_DispProbability  ("Displacement Glitch Probability", Float) = 0.022
		_DispIntensity    ("Displacement Glitch Intensity", Float) = 0.09
		[Header(Color Glitch)]
		_ColorProbability ("Color Glitch Probability", Float) = 0.02
		_ColorIntensity   ("Color Glitch Intensity", Float) = 0.07
	}
	SubShader {
		Tags {
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}
		Cull Off Lighting Off ZWrite Off Fog { Mode Off }
		Blend One OneMinusSrcAlpha
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile _ PIXELSNAP_ON
			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			float _GlitchInterval;
			float _DispIntensity, _DispProbability;
			float _ColorIntensity, _ColorProbability;
			
			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
			};
			v2f vert(appdata_full IN)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(IN.vertex);
				o.texcoord = IN.texcoord;
				o.color = IN.color;
#ifdef PIXELSNAP_ON
				o.vertex = UnityPixelSnap(o.vertex);
#endif
				return o;
			}
			float rand (float x, float y)
			{
				return frac(sin(x * 12.9898 + y * 78.233) * 43758.5453);  // 0~1
			}
			fixed4 frag(v2f IN) : SV_Target
			{
				float intervalTime = floor(_Time.y / _GlitchInterval) * _GlitchInterval;
				float intervalTime2 = intervalTime + 2.793;   // make value more random
				
				// position based random
				float t = intervalTime + UNITY_MATRIX_MV[0][3] + UNITY_MATRIX_MV[1][3];
				float t2 = intervalTime2 + UNITY_MATRIX_MV[0][3] + UNITY_MATRIX_MV[1][3];

				float rnd1 = rand(t, -t);
				float rnd2 = rand(t, t);

				float uOffset = float((rand(t2, t2) - 0.5) / 50.0);
				if (rnd1 < _DispProbability)
				{
					IN.texcoord.x += (rand(floor(IN.texcoord.y / (0.2 + uOffset)) - t, floor(IN.texcoord.y / (0.2 + uOffset)) + t) - 0.5) * _DispIntensity;
//					IN.texcoord.x = fmod(IN.texcoord.x, 1);
					IN.texcoord.x = clamp(IN.texcoord.x, 0, 1);
				}

				// color shift
				float rShift = (rand(-t, t) - 0.5) * _ColorIntensity;
				float gShift = (rand(-t, -t) - 0.5) * _ColorIntensity;
				float bShift = (rand(-t2, -t2) - 0.5) * _ColorIntensity;
				
				fixed4 cc = tex2D(_MainTex, IN.texcoord);   // center color
				fixed4 r = tex2D(_MainTex, float2(IN.texcoord.x + rShift, IN.texcoord.y + rShift));
				fixed4 g = tex2D(_MainTex, float2(IN.texcoord.x + gShift, IN.texcoord.y + gShift));
				fixed4 b = tex2D(_MainTex, float2(IN.texcoord.x + bShift, IN.texcoord.y + bShift));
				fixed4 c = 0.0;
				if (rnd2 < _ColorProbability)
				{
					c.r = r.r;
					c.g = g.g;
					c.b = b.b;
					c.a = (r.a + g.a + b.a) / 3.0;
				}
				else
				{
					c = cc;
				}
				c.rgb *= IN.color;
				c.a *= IN.color.a;
				c.rgb *= c.a;
				return c;
			}
			ENDCG
		}
	}
	Fallback Off
}