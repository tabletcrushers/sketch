2D Shader Pack is a collection of shaders which can make 2d game more beautiful.

There are 5 demos in package, each of them demonstrate a theme of effect.

=> "DemoColorful"
   In this demo, every "Girl" sprite use "2D Shader Pack/Colorful" shader.
   It let user tweak the "hue/saturation/value" of pixels freely, also support tweak specific range of color.
   You can get different colorful sprites without use any extra textures.

=> "DemoMisc"
   In demo misc, we demonstrate 3 kinds of special shaders for 2d graphics.
   At the top of scene, there are 3 dynamic electricity line based on shader "2D Shader Pack/Electricity".
   At the middle of scene, there are 4 dynamic fire based on shader "2D Shader Pack/Fire".
   At the bottom of scene, there are 2 glitch girl based on shader "2D Shader Pack/Glitch".

=> "DemoRetro"
   A camera filter to simulate old fashion art style rendering.
   Support features include: color quantize, tv curvature, pixel mask, rolling flicker, gameboy, pixelate.

=> "DemoSea"
   2D sea(water) rendering shader. Useful if you need a sea/water background in your game. 

=> "DemoSky"
   Full procedural 2d sky shader. Support time of day rendering.
   Features include: atmospheric scattering sky, sun, dynamic cloud, dynamic star.

All features are demonstrated in demo scenes. Please refer the them as example usage.

If you like it, please give it a good review on asset store. Thanks so much !
Any suggestion or improvement you want, please contact qq_d_y@163.com.
We'd like to help more and more unity3d developers.