﻿Shader "Custom/SimpleTexture" {
    Properties {
        _MainTex ("Main Texture",2D) = "white" {}
    }
    SubShader {
        Tags { "Queue"="Transparent"}
        Lighting Off
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha
 
        Pass
        {
            SetTexture[_MainTex] {combine texture}
        }
    }
   
}