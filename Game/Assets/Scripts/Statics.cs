﻿using UnityEngine;

public class Statics: MonoBehaviour
{
    public static int currentLevel = 0;
    public static bool isRoundWin;
    public static bool backHome;
    public static GameObject levelToDestroy;
    public static float camZ = -14f;
    public static float camDeltaX = 5;
    public static float camYRotation = 14;
    public static bool isRideStarted;
    public static float defaultVelocity = 18;
    public static float currentVelocity = 0;
    public static float tvCurvate = .22f;
    public static float addVelReduser = .4f;
    public static float maxActiveDistance = 5;

    public static void RoundWin()
    {
        currentLevel++;
        isRoundWin = true;

        PlayerPrefs.SetInt("Level", currentLevel);
        PlayerPrefs.Save();
    }

    public static int GetLastLevel()
    {
        return PlayerPrefs.HasKey("Level") ? PlayerPrefs.GetInt("Level") : 0;
    }

    public void Awake()
    {
        currentLevel = GetLastLevel();
        currentLevel = 0;

        float s = Screen.width * Screen.height / Screen.dpi;

        /*camZ = -6 - s / 700;
        camZ = - s / 777;8*/
        return;

        if (s < 2100)
        {
            camZ = -7;
        }
        else if (s < 3000)
        {
            camZ = -8;
        }
        else if (s < 4000)
        {
            camZ = -9;
        }
        else if (s < 5000)
        {
            camZ = -10;
        }
        else camZ = -11;
    }
}
