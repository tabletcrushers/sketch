﻿using UnityEngine;
public class SpriteTexture : MonoBehaviour
{
    public Sprite sprite;
    private MeshRenderer renderer;

    public void Awake()
    {
        renderer = GetComponent<MeshRenderer>();
        Apply();

    }

    public void Apply()
    {
        renderer.material.mainTexture = sprite.texture;
        Debug.Log(sprite.texture.width + " " + sprite.texture.height);
        Debug.Log(sprite.textureRect);

        renderer.material.mainTextureScale = new Vector2(
            sprite.textureRect.width / sprite.texture.width,
            sprite.textureRect.height / sprite.texture.height
        );

        renderer.material.mainTextureOffset = new Vector2(
            sprite.textureRect.x / sprite.texture.width,
            sprite.textureRect.y / sprite.texture.height
        );
    }
}