﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum ActionTypes
{
    Scale,
    Move,
    Rotate,
    Alpha2D
}

public class EasingManager : MonoBehaviour
{
    private class EasingSet
    {
        public Transform transform;
        public EasingAction[] easings = new EasingAction[3];
        public UnityAction callback;
        public ActionTypes actionType;
        public Vector3 rotation;
    }

    private List<EasingSet> easingActions = new List<EasingSet>();
    private Dictionary<SpriteRenderer, EasingAction> alphaActions = new Dictionary<SpriteRenderer, EasingAction>();

    /*public static float WrapAngle(float angle)
    {
        angle %= 360;
        if (angle > 180)
            return angle - 360;

        return angle;
    }

    public static float UnwrapAngle(float angle)
    {
        if (angle >= 0)
            return angle;

        angle = -angle % 360;

        return 360 - angle;
    }*/

    public void EaseAlpha(EasingTypes easingType, EasingDirection easingDirection, float totalTime, float finish, GameObject affectedObject, uint delay = 0, UnityAction callBack = null)
    {
        SpriteRenderer r = affectedObject.GetComponent<SpriteRenderer>();
        alphaActions.Add(r, new EasingAction(easingType, easingDirection, totalTime, r.color.a, finish, delay));
    }

    public void StartEasingAction(ActionTypes whatToEase, EasingTypes easingType, EasingDirection easingDirection, float totalTime, Vector3 finish, GameObject affectedObject, uint delay = 0, UnityAction callBack = null)
    {
        EasingSet set = new EasingSet();

        Vector3 startPos = Vector3.zero;
        switch (whatToEase)
        {
            case ActionTypes.Scale: startPos = affectedObject.transform.localScale; break;
            case ActionTypes.Move: startPos = affectedObject.transform.localPosition; break;
            case ActionTypes.Rotate: startPos = affectedObject.transform.localEulerAngles; break;
        }

        set.actionType = whatToEase;
        set.easings = GetActions(easingType, easingDirection, totalTime, startPos, finish, delay);
        set.transform = affectedObject.transform;
        set.callback = callBack;

        easingActions.Add(set);
    }

    private EasingAction[] GetActions(EasingTypes easingType, EasingDirection easingDirection, float totalTime, Vector3 start, Vector3 finish, uint delay = 0)
    {
        EasingAction[] actions = new EasingAction[3];

        actions[0] = (Mathf.Approximately(start.x, finish.x)) ? null : new EasingAction(easingType, easingDirection, totalTime, start.x, finish.x, delay);
        actions[1] = (Mathf.Approximately(start.y, finish.y)) ? null : new EasingAction(easingType, easingDirection, totalTime, start.y, finish.y, delay);
        actions[2] = (Mathf.Approximately(start.z, finish.z)) ? null : new EasingAction(easingType, easingDirection, totalTime, start.z, finish.z, delay);

        return actions;
    }

    private bool CheckForFinishAndApplyEasingValues(EasingAction[] actions, ref Vector3 target) 
    {
        bool isFinished = true;

        if (actions[0] != null && !actions[0].finished)
        {
            target.x = actions[0].Update();
            isFinished = false;
        }
        if (actions[1] != null && !actions[1].finished)
        {
            target.y = actions[1].Update();
            isFinished = false;
        }
        if (actions[2] != null && !actions[2].finished)
        {
            target.z = actions[2].Update();
            isFinished = false;
        }

        return isFinished;
    }

    private void Update()
    {
        foreach (KeyValuePair<SpriteRenderer, EasingAction> keyValue in alphaActions)
        {
            if (keyValue.Value.finished) 
            {
                alphaActions.Remove(keyValue.Key);
                break;
            }

            keyValue.Key.color = new Color(keyValue.Key.color.r, keyValue.Key.color.g, keyValue.Key.color.b, keyValue.Value.Update());
        }

        foreach(EasingSet entry in easingActions)
        {
            Vector3 target = Vector3.zero;
            switch (entry.actionType)
            {
                case ActionTypes.Scale: target = entry.transform.localScale; break;
                case ActionTypes.Move: target = entry.transform.localPosition; break;
                case ActionTypes.Rotate: target = entry.rotation; break;    
            }
            if (CheckForFinishAndApplyEasingValues(entry.easings, ref target))
            {
                this.easingActions.Remove(entry);
                if (entry.callback != null)
                {
                    entry.callback.Invoke();
                }
                return;
            }

            switch (entry.actionType)
            {
                case ActionTypes.Scale: entry.transform.localScale = target; break;
                case ActionTypes.Move: entry.transform.localPosition = target; break;
                case ActionTypes.Rotate: entry.transform.localEulerAngles = target; break;    
            }
        }
    }
}
