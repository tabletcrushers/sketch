﻿using UnityEngine;
using System.Collections;
using System;

public class TimeUtils : MonoBehaviour {

    public static IEnumerator Delay(float time, Action func)
    {
        yield return new WaitForSeconds(time);

        func();
    }

    public static IEnumerator DelayFrames(short frames, Action func)
    {
        while (frames > 0)
        {
            frames--;
            yield return new WaitForEndOfFrame();
        }

        func();
    }

    public static bool IsOnFront(GameObject origin, GameObject target)
    {
        float checkResult = Vector3.Dot(origin.transform.TransformDirection(Vector3.forward), target.transform.position - origin.transform.position);
        if (checkResult < 0) return false;
        else return true;
    }
}
