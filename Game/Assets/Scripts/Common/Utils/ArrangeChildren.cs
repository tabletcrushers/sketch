﻿using UnityEngine;

public static class ArrangeChildren
{
    public static void RandomizeXPosition(Transform parent, float range)
    {
        foreach (Transform children in parent)
        {
            children.Translate(new Vector3(Random.Range(-range, range), 0, 0));
        }
    }

    public static void RandomizeYPosition(Transform parent, float range)
    {
        foreach (Transform children in parent)
        {
            children.Translate(new Vector3(0, Random.Range(-range, range), 0));
        }
    }

    public static void RandomizeRotation(Transform parent, float range)
    {
        foreach (Transform children in parent)
        {
            children.Rotate(new Vector3(0, 0, 1), Random.Range(-range, range));
        }
    }

    public static void ExtendXGapRelatively(Transform parent, float additionalGap)
    {
        for (int i = 0; i < parent.childCount; i++)
        {
            Transform children = parent.GetChild(i);

            children.Translate(new Vector3(additionalGap * i, 0, 0));
        }
    }

    public static void RandomizeXScale(Transform parent, float from, float to)
    {
        foreach (Transform children in parent)
        {
            children.localScale = new Vector3(Random.Range(from, to), children.localScale.y, 1);
        }
    }

    public static void RandomizeYScale(Transform parent, float from, float to)
    {
        foreach (Transform children in parent)
        {
            children.localScale = new Vector3(children.localScale.x, Random.Range(from, to), 1);
        }
    }

    public static void ArrangeChildrenConsistently(Transform parent, float offsetX, float offsetY)
    {
        for (int i = 0; i < parent.childCount; i++)
        {
            Transform children = parent.GetChild(i);

            children.Translate(new Vector3(offsetX * i, offsetY * i, 0));
        }
    }

    public static GameObject MakeQueue(GameObject middleInstance, int amount, GameObject extremeInstance, bool xDirection, Vector3 customBounds, float offset = 0)
    {
        GameObject container = new GameObject();
        GameObject obj;
        Renderer renderer = null;

        float position = 0;

        for (int i = 0; i < amount; i++)
        {
            if (i > 0)
            {
                Vector3 bounds;
                if (customBounds.x > 0 && customBounds.y > 0 && customBounds.z > 0) bounds = customBounds;
                else bounds = renderer.bounds.size;
                position += xDirection ? bounds.x / 2 : bounds.y / 2;
            }

            obj = GameObject.Instantiate(middleInstance, container.transform);
            renderer = obj.GetComponent<Renderer>();

            if (i > 0)
            {
                Vector3 bounds;
                if (customBounds.x > 0 && customBounds.y > 0 && customBounds.z > 0) bounds = customBounds;
                else bounds = renderer.bounds.size;
                position += xDirection ? bounds.x / 2 : bounds.y / 2;
            }

            position += offset;
            obj.transform.localPosition = xDirection ? new Vector3(position, 0, 0) : new Vector3(0, position, 0);
        }

        AddExtremeParts(container.transform, extremeInstance, xDirection, customBounds, offset);

        return container;
    }

    private static void AddExtremeParts(Transform parent, GameObject extremeInstance, bool xDirection, Vector3 customBounds,float offset = 0)
    {
        GameObject extremePart = GameObject.Instantiate(extremeInstance, parent);
        float position = xDirection? parent.GetChild(0).transform.localPosition.x : parent.GetChild(0).transform.localPosition.y;

        Vector3 bounds; 
        if (customBounds.x > 0 && customBounds.y > 0 && customBounds.z > 0) bounds = customBounds;
        else bounds = parent.GetChild(0).GetComponent<Renderer>().bounds.size;

        position -= xDirection ? bounds.x / 2 : bounds.y / 2;

        if (customBounds.x > 0 && customBounds.y > 0 && customBounds.z > 0) bounds = customBounds;
        else bounds = extremePart.GetComponent<Renderer>().bounds.size;

        position -= xDirection ? bounds.x / 2 : bounds.y / 2;
        position -= offset;
        extremePart.transform.localPosition = xDirection ? new Vector3(position, 0, 0) : new Vector3(0, position, 0);



        position = xDirection ? parent.GetChild(parent.childCount - 2).transform.localPosition.x : parent.GetChild(parent.childCount - 2).transform.localPosition.y;
        extremePart = GameObject.Instantiate(extremeInstance, parent);

        if (customBounds.x > 0 && customBounds.y > 0 && customBounds.z > 0) bounds = customBounds;
        else bounds = parent.GetChild(parent.childCount - 2).GetComponent<Renderer>().bounds.size;

        position += xDirection ? bounds.x / 2 : bounds.y / 2;

        if (customBounds.x > 0 && customBounds.y > 0 && customBounds.z > 0) bounds = customBounds;
        else bounds = extremePart.GetComponent<Renderer>().bounds.size;

        position += xDirection ? bounds.x / 2 : bounds.y / 2;
        position += offset;
        extremePart.transform.localScale = new Vector3(extremePart.transform.localScale.x, extremePart.transform.localScale.y, extremePart.transform.localScale.z);
        extremePart.transform.localPosition = xDirection ? new Vector3(position, 0, 0) : new Vector3(0, position, 0);
        Vector3 scale = extremePart.transform.localScale;
        extremePart.transform.localScale = xDirection ? new Vector3(-scale.x, scale.y, scale.z) : new Vector3(scale.x, -scale.y, scale.z);
    }

    public static GameObject MakeTable(GameObject instance, float rows, float columns, float offsetX, float offSetY, float margin = 0)
    {
        GameObject container = new GameObject();

        for (int j = 0; j < columns; j++)
        {
            for (int i = 0; i < rows; i++)
            {
                GameObject obj = GameObject.Instantiate(instance, container.transform);
                obj.transform.localPosition = new Vector3(margin * j + offsetX * j, margin * i + offSetY * i, obj.transform.localPosition.z);
            }
        }

        return container;
    }

    public static Vector3 CalculateLocalBounds(GameObject instance)
    {
        Quaternion currentRotation = instance.transform.rotation;
        instance.transform.rotation = Quaternion.Euler(0f, 0f, 0f);

        Bounds bounds = new Bounds(instance.transform.position, Vector3.zero);

        foreach (Renderer renderer in instance.GetComponentsInChildren<Renderer>())
        {
            bounds.Encapsulate(renderer.bounds);
        }

        Vector3 localCenter = bounds.center - instance.transform.position;
        bounds.center = localCenter;

        instance.transform.rotation = currentRotation;

        return bounds.size;
    }
}
