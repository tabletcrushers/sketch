﻿using UnityEngine;
using UnityEngine.Events;

public class TriggerEnter : MonoBehaviour
{
    public bool disableColliderAfterEnter = false;
    public UnityEvent callback;

    void OnTriggerEnter2D(Collider2D other)
    {
        callback.Invoke();

        if (disableColliderAfterEnter) gameObject.GetComponent<Collider2D>().enabled = false;
    }
}
