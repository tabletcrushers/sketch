﻿using UnityEngine;
using System.Collections.Generic;

public class CircleTriggerCollector : TriggerCollector {

    public float radius;
    public Vector2 offset;

    private void Start()
    {
        CircleCollider2D trigger = gameObject.AddComponent<CircleCollider2D>();
        trigger.radius = radius;
        trigger.offset = offset;
        trigger.isTrigger = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        collidersIn.Add(collision);
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        collidersIn.Remove(collision);
    }
}
