﻿using UnityEngine;
using UnityEngine.Events;

public class Trigger : MonoBehaviour {

	public UnityAction<Collider2D> onEnter;
	public UnityAction<Collider2D> onExit;

	private void On(Collider2D collision)
	{
		onEnter.Invoke(collision);
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(onEnter != null) onEnter.Invoke(collision);
	}


	private void OnTriggerExit2D(Collider2D collision)
	{
		if(onExit != null) onExit.Invoke(collision);
	}
}
