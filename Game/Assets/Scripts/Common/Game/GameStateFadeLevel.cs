﻿using UnityEngine;

public class GameStateFadeLevel : GameStateWithCurtain
{
    public GameObject gameGUI;

    private float cameraVelocity;

    protected override void Awake()
    {
        base.Awake();

        onEnterAction += () =>
        {
            cameraVelocity = Statics.defaultVelocity;
        };
    }

    override protected void OnFadeFinished()
    {
        GameObject levelToDestroy = Statics.levelToDestroy;
        Statics.isRoundWin = false;

        if(levelToDestroy)
        {
            Destroy(levelToDestroy);
        }

        stateMachine.SwitchState("GameStateLevel");

        gameGUI.SetActive(true);

        foreach (Transform child in gameGUI.transform)
        {
            child.gameObject.SetActive(true);
        }
    }

	private void Update()
	{
        Camera.main.transform.Translate(new Vector3(cameraVelocity * Time.deltaTime, .04f, .1f));
        Camera.main.transform.Rotate(new Vector3(.07f, -.14f, .02f));
        cameraVelocity -= .02f;
	}
}