﻿using UnityEngine;

public class GameStatePause : State
{
    override protected void Awake()
    {
        base.Awake();

        onEnterAction = () =>
        {
            //Time.timeScale = .2f;
            Physics2D.autoSimulation = false;
                
        };
    }
}
