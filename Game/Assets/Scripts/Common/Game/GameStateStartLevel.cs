﻿using UnityEngine;

public class GameStateStartLevel : GameStateWithCurtain
{
    override protected void Awake()
    {
        base.Awake();

        onEnterAction += () =>
        {
            GameObject n = Instantiate(Resources.Load("Levels/Level" + Statics.currentLevel, typeof(GameObject)) as GameObject);
            n.transform.position = new Vector3();
        };
    }

    override protected void OnFadeFinished()
    {
        curtain.gameObject.SetActive(false);
    }
}
