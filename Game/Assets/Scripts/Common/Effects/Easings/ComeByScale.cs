﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComeByScale : MonoBehaviour
{
    private List<Vector3> cachedScale;
    private List<Vector3> cachedRotation;
    private List<float> delays;

    public void Init()
    {
        cachedScale = new List<Vector3>();
        cachedRotation = new List<Vector3>();
        delays = new List<float>();

        float delay = 0;
        foreach (Transform ct in transform)
        {
            cachedScale.Add(ct.localScale);
            cachedRotation.Add(ct.rotation.eulerAngles);
            delays.Add(delay);
            delay += .1f;
            delays.Shuffle();
            ct.localScale = new Vector3(0, 0, 1);
        }
    }

    public void Show(float showAfterSeconds = 0, float hideAfterSeconds = 0)
    {
        if (showAfterSeconds > 0) EasyTwinner.Instance.Delay(transform, showAfterSeconds, Show_);
        else Show_();

        if(hideAfterSeconds > showAfterSeconds) EasyTwinner.Instance.Delay(transform, hideAfterSeconds, Hide);
    }

    public void Hide()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform ct = transform.GetChild(i);

            int ind = i;

            EasyTwinner.Instance.Delay(ct, delays[i] / 2, () =>
            {
                EasyTwinner.Instance.TweenScale(ct, cachedScale[ind] * 1.4f, .2f, Easing.Linear, () =>
                {
                    EasyTwinner.Instance.TweenRotation(ct, new Vector3(0, 0, Random.value * 360), 4, Easing.Elastic.Out);
                    EasyTwinner.Instance.TweenScale(ct, Vector3.zero, .4f, Easing.Linear);
                });
                EasyTwinner.Instance.OscilatePosition(ct, new Vector3(0, .5f, 0), new Vector3(0, 2, 0), new Vector3(0, 2, 0));
            });

            EasyTwinner.Instance.Delay(transform, 3, () =>
            {
                EasyTwinner.Instance.StopAllOfCertain(ct);
                Destroy(gameObject);
            });
        }
    }

    private void Show_()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform ct = transform.GetChild(i);

            int ind = i;

            EasyTwinner.Instance.Delay(ct, delays[i], () =>
            {

                EasyTwinner.Instance.TweenScale(ct, cachedScale[ind], .4f, Easing.Linear, () =>
                {
                    EasyTwinner.Instance.OscilateScale(ct, new Vector3(1, 1, 0), new Vector3(2, 2, 0), new Vector3(4, 4, 0));
                });

                EasyTwinner.Instance.OscilateRotation(ct, new Vector3(0, 0, 140), new Vector3(0, 0, 4), new Vector3(0, 0, 8));

                EasyTwinner.Instance.OscilatePosition(ct, new Vector3(0, 2, 0), new Vector3(0, 4, 0), new Vector3(0, 1, 0));

            });
        }
    }
}
