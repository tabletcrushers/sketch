﻿using UnityEngine;

public class PauseEffects : MonoBehaviour
{
    EasingManager m;

    void Start()
    {
        m = GameObject.Find("Manager").GetComponent<EasingManager>();

        for (int i = 0; i < transform.childCount; i++)
        {
            GameObject obj = transform.GetChild(i).gameObject;
            obj.transform.Rotate(new Vector3(0, 0, 360 - Random.value * 720));
            obj.transform.localPosition = new Vector3(obj.transform.localPosition.x, -14 + i * 4, 0);
            obj.transform.localScale = new Vector3(0, 0, 1);
        }
    }

    public void Show()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            GameObject obj = transform.GetChild(i).gameObject;
            m.StartEasingAction(ActionTypes.Rotate, EasingTypes.Elastic, EasingDirection.Out, 2, new Vector3(0, 0, 360), obj);
            m.StartEasingAction(ActionTypes.Scale, EasingTypes.Circular, EasingDirection.Out, .7f, new Vector3(7, 7, 1), obj);
            m.StartEasingAction(ActionTypes.Move, EasingTypes.Elastic, EasingDirection.Out, 2, new Vector3(obj.transform.localPosition.x, 0, 0), obj);
        }
    }

    public void Hide()
    {
        enabled = false;
        for (int i = 0; i < transform.childCount; i++)
        {
            GameObject obj = transform.GetChild(i).gameObject;

            m.StartEasingAction(ActionTypes.Scale, EasingTypes.Elastic, EasingDirection.In, .7f, new Vector3(0, 0, 0), obj, (uint)i * 4);
        }
    }
}
