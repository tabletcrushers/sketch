﻿using UnityEngine;

public class SpriteAlphaBlink : MonoBehaviour
{
    public float[] alphaValues;
    public float invokeRate = 0.07f;

    private SpriteRenderer blinkSpriteRender;

    void Start()
    {
        blinkSpriteRender = GetComponent<SpriteRenderer>();
        InvokeRepeating("Blink", 0, invokeRate);
    }

    void Blink()
    {
        float alpha = alphaValues[Random.Range(0, alphaValues.Length - 1)];
        blinkSpriteRender.color = new Color(blinkSpriteRender.color.r, blinkSpriteRender.color.g, blinkSpriteRender.color.b, alpha);
    }
}
