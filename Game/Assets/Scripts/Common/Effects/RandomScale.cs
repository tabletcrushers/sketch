﻿using UnityEngine;

public class RandomScale : MonoBehaviour
{
    public float invokeRate = 0.07f;
    public float maxDtScaleX = 70;
    public float maxDtScaleY = 70;
    private Vector3 startScale;

    void Start()
    {
        startScale = gameObject.transform.localScale;
        InvokeRepeating("Scale", 0, invokeRate);
    }

    void Scale()
    {
        float multScaleY = 1;
        if (Random.value > .5f) multScaleY = -1;

        float multScaleX = 1;
        if (Random.value > .5f) multScaleX = -1;

        gameObject.transform.localScale = new Vector3((startScale.x + Random.value * maxDtScaleX) * multScaleX, (startScale.y + Random.value * maxDtScaleY) * multScaleY, 1);
    }
}
