﻿using UnityEngine;

public class RotateObject : MonoBehaviour
{
    public float rotationVelocity;
    public float randomRotationFluctuation = 0;

    void Update()
    {
        gameObject.transform.Rotate(new Vector3(0, 0, rotationVelocity + Random.value * randomRotationFluctuation));
    }
}
