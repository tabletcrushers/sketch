﻿using UnityEngine;

public class FallingSequence : MonoBehaviour
{
    public float delay;

    private void Start()
    {
        foreach (Transform ct in transform)
        {
            ct.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer != 11) return;

        int i = 0;

        foreach (Transform ct in transform)
        {
            EasyTwinner.Instance.Delay(transform, delay * i, () => { ct.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic; });
            i++;
        }
    }
}
