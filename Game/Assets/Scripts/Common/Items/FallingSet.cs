﻿using UnityEngine;

public class FallingSet : MonoBehaviour
{
    public Rigidbody2D[] forcedObjects;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer != 11) return;

        foreach (Rigidbody2D o in transform.gameObject.GetComponentsInChildren<Rigidbody2D>())
        {
            if(Random.value > .5f) o.AddForce(new Vector2(170 + 170 * Random.value, 0));
        }

        foreach (Transform ct in transform)
        {
            //ct.gameObject.layer = 12;
        }
    }
}
