﻿using UnityEngine;
using UnityEngine.Events;

public enum TouchTypes
{
	LeftTouchBegan,
	LeftTouchEnded,
	RightTouchBegan,
	RightTouchEnded,
	TouchMoved
}


public class TouchControlsManager : MonoBehaviour
{
	private UnityAction<Vector2> _leftTouchBeganCallbacks;
	private UnityAction<Vector2> _leftTouchEndedCallbacks;
	private UnityAction<Vector2> _rightTouchBeganCallbacks;
	private UnityAction<Vector2> _rightTouchEndedCallbacks;
	private UnityAction<Vector2> _touchMovedCallbacks;

	public bool isLeftTouch;
	public bool isRightTouch;

    public bool debug;

	public TouchControlsManager AddCallback(TouchTypes type, UnityAction<Vector2> callback)
	{
		switch (type)
		{
			case TouchTypes.LeftTouchBegan:
				_leftTouchBeganCallbacks += callback;
				break;
			case TouchTypes.LeftTouchEnded:
				_leftTouchEndedCallbacks += callback;
				break;
			case TouchTypes.RightTouchBegan:
				_rightTouchBeganCallbacks += callback;
				break;
			case TouchTypes.RightTouchEnded:
				_rightTouchEndedCallbacks += callback;
				break;
			case TouchTypes.TouchMoved:
				_touchMovedCallbacks += callback;
				break;
		}

		return this;
	}

	public TouchControlsManager RemoveCallback(TouchTypes type, UnityAction<Vector2> callback)
	{
		switch (type)
		{
			case TouchTypes.LeftTouchBegan:
				_leftTouchBeganCallbacks -= callback;
				break;
			case TouchTypes.LeftTouchEnded:
				_leftTouchEndedCallbacks -= callback;
				break;
			case TouchTypes.RightTouchBegan:
				_rightTouchBeganCallbacks -= callback;
				break;
			case TouchTypes.RightTouchEnded:
				_rightTouchEndedCallbacks -= callback;
				break;
		}

		return this;
	}

    private void Update()
	{
        if (debug)
        {
            if (Input.GetMouseButtonDown(0))
            {
                isLeftTouch = true;
                if (_leftTouchBeganCallbacks != null)
                {
                    _leftTouchBeganCallbacks.Invoke(Input.mousePosition);
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                isLeftTouch = false;
                if (_leftTouchEndedCallbacks != null) _leftTouchEndedCallbacks.Invoke(Input.mousePosition);
            }

            if (Input.GetMouseButtonDown(1))
            {
                isRightTouch = true;
                if (_rightTouchBeganCallbacks != null) _rightTouchBeganCallbacks.Invoke(Input.mousePosition);
            }
            else if (Input.GetMouseButtonUp(1))
            {
                isRightTouch = false;
                if (_rightTouchEndedCallbacks != null) _rightTouchEndedCallbacks.Invoke(Input.mousePosition);
            }

            return;
        }

        for (int i = 0; i < Input.touchCount; ++i)
		{
			Touch touch = Input.GetTouch(i);

			if (touch.phase == TouchPhase.Began)
			{
				if (touch.position.x <= Screen.width / 2)
				{
					isLeftTouch = true;
					if (_leftTouchBeganCallbacks != null) _leftTouchBeganCallbacks.Invoke(touch.position);
				}
				else
				{
					isRightTouch = true;
					if (_rightTouchBeganCallbacks != null) _rightTouchBeganCallbacks.Invoke(touch.position);
				}
			}
			else if (touch.phase == TouchPhase.Ended)
			{
				if (touch.position.x < Screen.width / 2)
				{
					isLeftTouch = false;
					if (_leftTouchEndedCallbacks != null) _leftTouchEndedCallbacks.Invoke(touch.position);
				}
				else
				{
					isRightTouch = false;
					if (_rightTouchEndedCallbacks != null) _rightTouchEndedCallbacks.Invoke(touch.position);
				}
			}
			else if (touch.phase == TouchPhase.Moved)
			{
				if (_touchMovedCallbacks != null) _touchMovedCallbacks.Invoke(touch.position);
			}
		}
	}
}
