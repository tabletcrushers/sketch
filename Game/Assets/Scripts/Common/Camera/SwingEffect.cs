﻿using UnityEngine;

public class SwingEffect : MonoBehaviour
{
    private CameraContainerSwing swingEffect;

    private void Start()
    {
        swingEffect = GameObject.Find("CameraContainer").GetComponent<CameraContainerSwing>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer != 13)
        {
            return;
        }
    }
}
