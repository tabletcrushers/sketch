﻿using UnityEngine;
using UnityEngine.Events;

public abstract class State : MonoBehaviour
{
    public UnityAction onEnterAction;
    public UnityAction onExitAction;
	public string previousState;
	public StateMachine stateMachine;
	public readonly Object sharedData = new Object();

	virtual protected void Awake()
	{
		stateMachine = GetComponent<StateMachine>();
	}

	private void Update()
	{

	}
}
