﻿using UnityEngine;
using System.Collections;
using System;

public class HeroStateInAir : HeroState
{
    public bool _forceDown;
    ParticleSystem landingEffect;
    ParticleSystem bigFireEffect;
    private System.Random random;

    private GameStateLevel stateLevel;

    override protected void Awake()
	{
		base.Awake();

        stateLevel = GameObject.Find("Manager").GetComponent<EasingManager>().GetComponent<GameStateLevel>();

        bigFireEffect = GameObject.Find("Hero/ParticlesFireBig").GetComponent<ParticleSystem>();
        landingEffect = GameObject.Find("Hero/ParticlesLanding").GetComponent<ParticleSystem>();

        bigFireEffect.Stop();
        landingEffect.Stop();

        random = new System.Random();

		onEnterAction = () =>
		{
            if (previousState != "HeroStateJump")
            {
                _hero.SetAnimTrigger("FreeFalling");
                _hero.jumperForce = 0;
            }

            if (_hero.spring != null && _hero.jumperForce > 0)
            {
                EasyTwinner.Instance.OscilateRotation(_hero.spring.parent, new Vector3(0, 0, 7), new Vector3(0, 0, 2), new Vector3(0, 0, 7));
                _hero.spring.GetComponent<Collider2D>().enabled = false;
                _hero.jumperForce = 0;
                _hero.spring = null;
            }

            _hero.soundManager.bgAudioSource.Stop();

			_forceDown = false;
			_touchControls
				.AddCallback(TouchTypes.RightTouchBegan, OnRightTouchBegan);
            _bordTriggerChecker.enterActions += BordTriggerEnter;
		};

		onExitAction = () =>
		{
            _touchControls
				.RemoveCallback(TouchTypes.RightTouchBegan, OnRightTouchBegan);

            _bordTriggerChecker.enterActions -= BordTriggerEnter;

			_body.angularDrag = 4f;
			_body.velocity = new Vector2(_body.velocity.x, _body.velocity.y / 3);

		};
	}

    private void BordTriggerEnter (Collider2D c)
    {

        if (c.gameObject.layer == 14 || c.gameObject.layer == 17 || c.gameObject.layer == 22) return;

        _hero.rotating = false;

        if (_hero.energeticEffect.activeSelf)
        {
            _hero.energeticEffect.SetActive(false);
        }

        if(c.gameObject.layer == 23)
        {
            _hero.stateMachine.SwitchState("HeroStateRun");
            _hero.SetAnimTrigger("Slide");
            return;
        }


        _hero.soundManager.PlayCollisionSound(_hero.soundManager.landingRegular);

        _hero.stateMachine.SwitchState("HeroStateRun");
        _hero.SetAnimTrigger("Landing0");


        if (_body.velocity.y < -3.5f)
        {
            landingEffect.Play();
            swingCamera.Landing();
        }
        else if (_body.velocity.y < -2.5f)
        {
            swingCamera.SoftLanding();
        }
    }

	private void OnRightTouchBegan(Vector2 touchPosition)
	{
        if (!_hero) return;

        if (_hero.accelerator)
        {
            _hero.additionalVelocity = 42;
            Statics.addVelReduser = .4f;
            _hero.accelerator.SetTrigger("Fire");
            _hero.soundManager.PlayEffect(_hero.soundManager.acceleratorFire);

            return;
        }

        if (Time.time - _hero.rightTouchMoment < .28f)
        {
            string trickID = random.Next(0, 2).ToString();
            _hero.SetAnimTrigger("Trick" + trickID);
            StartCoroutine(Delay(.14f, () => {
            try {
                if (_hero) _forceDown = true;
            }
                catch { }
            }));
            _hero.soundManager.PlayHeroJumpWoop();
            return;
        }

        if (_hero.energeticEffect.activeSelf)
        {
            _hero.additionalVelocity = 42;
            Statics.addVelReduser = .4f;
            // _hero.additionalVelocity = 2100 * Time.fixedDeltaTime;
            _body.rotation = 21;
            _body.angularVelocity = 0;
            _hero.energeticEffect.SetActive(false);
            bigFireEffect.Play();
            stateLevel.Zoom();

            return;
        }
    }

    private IEnumerator Delay(float time, Action func)
    {
        yield return new WaitForSeconds(time);

        func();
    }

	private void FixedUpdate()
	{
        if(_body.velocity.y < 0) _hero.SetAnimTrigger("Falling0");

        if (_hero.additionalVelocity > 0) _body.velocity = new Vector2(_hero.runVelocity, _body.velocity.y);

        float absRotation = Mathf.Abs(_body.rotation);

        if (absRotation > 11) _body.rotation /= 2;

        if (absRotation > 4) _body.angularVelocity -= _body.rotation;

		if (_forceDown)
		{
            if(_body.velocity.y > - 14) _body.AddForce(new Vector2(0, -42f));
		}
	}
}
