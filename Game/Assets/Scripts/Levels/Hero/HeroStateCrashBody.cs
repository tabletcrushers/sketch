﻿using UnityEngine;

public class HeroStateCrashBody : HeroStatesCrash
{
    override protected void ApplyCrashEffects()
    {
        boarder.GetComponent<Rigidbody2D>().rotation = 21;
        boarder.GetComponent<Rigidbody2D>().velocity = new Vector2(-_hero.runVelocity / 2, _body.velocity.y + 7);
        boarder.GetComponent<Rigidbody2D>().angularVelocity = 35;
        board.GetComponent<Rigidbody2D>().velocity = new Vector2(-_hero.runVelocity / 2, _body.velocity.y + 7);
        board.GetComponent<Rigidbody2D>().angularVelocity = -2000 * Random.value;
        Debug.Log("Body!!!");
        swingCamera.Crush();
    }   
}
