﻿using UnityEngine;

public class HeroStateCrash : HeroStatesCrash
{
    override protected void ApplyCrashEffects()
    {
        boarder.GetComponent<Rigidbody2D>().rotation = -45;
        boarder.GetComponent<Rigidbody2D>().velocity = new Vector2(_hero.runVelocity, _body.velocity.y + 7);
        boarder.GetComponent<Rigidbody2D>().angularVelocity = -70;
        board.GetComponent<Rigidbody2D>().velocity = new Vector2(-_hero.runVelocity / 2, _body.velocity.y + 7);
        board.GetComponent<Rigidbody2D>().angularVelocity = 2000 * Random.value;
        _hero.soundManager.PlayCollisionSound(_hero.soundManager.regularCrash);
        _hero.soundManager.PlayHeroCrashWoop();
        Debug.Log("REGULAR CRUSH!!!");
        swingCamera.Crush();
    }
}
