﻿using UnityEngine;

public class PushEffect : MonoBehaviour
{
    Transform heroTransform;
    Hero hero;
    private SoundManager soundManager;

    public void Start()
	{
        heroTransform = gameObject.transform.parent.gameObject.transform;
        hero = gameObject.transform.parent.gameObject.GetComponent<Hero>();
        soundManager = GameObject.Find("Manager").GetComponent<SoundManager>();
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        Rigidbody2D attachedBody = other.attachedRigidbody;
        if (!attachedBody) return;
        if (other.gameObject.layer == 12) 
        {
            if (attachedBody.bodyType == RigidbodyType2D.Static) attachedBody.bodyType = RigidbodyType2D.Dynamic;
            Push(attachedBody, 700, 1);

            if (hero.additionalVelocity > 0) attachedBody.AddForce(new Vector2(hero.additionalVelocity * 63, 0));
        }
        //other.enabled = false;
    }

    private void OnTriggerStay2D(Collider2D other)
	{

        if (other.gameObject.layer != 12) return;

        Rigidbody2D attachedBody = other.attachedRigidbody;
        if (!attachedBody) return;

        Push(attachedBody, 28 + hero.additionalVelocity * 3);
	}

    private void Push(Rigidbody2D afecttedBody, float force, float maxMagnitude = 28)
    {
        if (afecttedBody.velocity.magnitude < maxMagnitude)
        {
            float a = Mathf.Atan2(afecttedBody.position.y - heroTransform.position.y, afecttedBody.position.x - heroTransform.position.x);
            afecttedBody.AddForce(new Vector2(force * Mathf.Cos(a), force * Mathf.Sin(a)));
            afecttedBody.angularVelocity += 400 - Random.value * 800;
        }
    }
}
