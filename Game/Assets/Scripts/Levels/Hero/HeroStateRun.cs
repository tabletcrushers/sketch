﻿using UnityEngine;

public class HeroStateRun : HeroState
{
    override protected void Awake()
	{
		base.Awake();

        _body.velocity = new Vector2(_hero.runVelocity, _body.velocity.y);

        onExitAction = () =>
		{
			_touchControls
				.RemoveCallback(TouchTypes.LeftTouchBegan, OnLeftTouchBegan)
				.RemoveCallback(TouchTypes.RightTouchBegan, OnRightTouchBegan);

			_bordTriggerChecker.exitActions = null;
		};
		

		onEnterAction = () =>
		{
            _hero.soundManager.bgAudioSource.Play();

            if (_touchControls.isLeftTouch)
			{
				//_hero.stateMachine.SwitchState("HeroStateWalk");
				return;
			}

			_touchControls
				.AddCallback(TouchTypes.LeftTouchBegan, OnLeftTouchBegan)
				.AddCallback(TouchTypes.RightTouchBegan, OnRightTouchBegan);
		};
	}

	protected void OnLeftTouchBegan(Vector2 touchPosition)
	{
        //_hero.stateMachine.SwitchState("HeroStateWalk");

	}

	protected void OnRightTouchBegan(Vector2 touchPosition)
	{
        if (!_hero) return;
		_hero.stateMachine.SwitchState("HeroStateJump");
        _hero.rightTouchMoment = Time.time;

        if(_hero.jumper)
        {
            _hero.jumper.SetTrigger("Fire");
            _hero.soundManager.PlayEffect(_hero.soundManager.springFire);
            _body.AddForce(new Vector2(0, _hero.jumperForce));
        }
	}

	protected void FixedUpdate()
	{
        _body.velocity = new Vector2(_hero.runVelocity, _body.velocity.y);
	}
}
