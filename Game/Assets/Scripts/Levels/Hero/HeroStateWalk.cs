﻿using UnityEngine;

public class HeroStateWalk : HeroState
{
	override protected void Awake()
	{
		base.Awake();

		onExitAction = () => _touchControls.RemoveCallback(TouchTypes.RightTouchBegan, OnRightTouchBegin).RemoveCallback(TouchTypes.LeftTouchEnded, OnLefttTouchEnd);

		onEnterAction = () =>
		{
			_body.gravityScale = 1;
			_touchControls
				.AddCallback(TouchTypes.RightTouchBegan, OnRightTouchBegin)
				.AddCallback(TouchTypes.LeftTouchEnded, OnLefttTouchEnd);
		};
	}

	protected void OnRightTouchBegin(Vector2 touchPosition)
	{
		_hero.stateMachine.SwitchState("HeroStateJump");
	}

	protected void OnLefttTouchEnd(Vector2 touchPosition)
	{
		_hero.stateMachine.SwitchState("HeroStateRun");
	}

	protected void FixedUpdate()
	{
		_body.velocity = new Vector2(_hero.walkVelocity, _body.velocity.y);
	}
}
