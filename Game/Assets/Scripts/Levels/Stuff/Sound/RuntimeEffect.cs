﻿using UnityEngine;

public class RuntimeEffect : MonoBehaviour
{
    public AudioClip sound;
    private SoundManager soundManager;

    private void Start()
    {
        soundManager = GameObject.Find("Manager").GetComponent<SoundManager>();
    }

    public void PlaySound()
    {
        soundManager.PlayEffect(sound);
    }
}
