﻿using UnityEngine;

public class BreakWall : MonoBehaviour
{
    private SoundManager soundManager;

    private void Start()
    {
        soundManager = GameObject.Find("Manager").GetComponent<SoundManager>();
    }

    public void PlayEffect()
    {
        soundManager.PlayBrakeWallEffect();
    }
}
