﻿public class PlaySteelFallingEffect : PlayCollisionEffect
{
    override protected void PlayEffect()
    {
        soundManager.PlaySteelFalling();
    }
}