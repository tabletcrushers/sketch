﻿using UnityEngine;
using UnityEngine.Events;

public class DistanceChecker : MonoBehaviour
{
    public Transform target;
    public float distance;
    public UnityEvent callBack;
    void Update()
    {
        if(transform.position.x - target.position.x > distance)
        {
            callBack.Invoke();
            enabled = false;
        }
    }
}
