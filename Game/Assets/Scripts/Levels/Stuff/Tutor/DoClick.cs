﻿using UnityEngine;

public class DoClick : MonoBehaviour
{
    public Transform cameraContainer;
    public bool doubleClick = false;

    public void Click()
    {
        GameObject n = Instantiate(Resources.Load("Tutor/Click", typeof(GameObject)) as GameObject, cameraContainer);
        n.transform.localPosition = new Vector3(0, 0, 7);

        if (!doubleClick) return;

        StartCoroutine(TimeUtils.Delay(.2f, () =>
        {
            GameObject nn = Instantiate(Resources.Load("Tutor/Click", typeof(GameObject)) as GameObject, cameraContainer);
            nn.transform.localPosition = new Vector3(-.02f, 0, 7);
        }));
    }
}
