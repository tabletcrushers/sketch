﻿#if UNITY_EDITOR
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

class FFontCreator : EditorWindow
{
    private string phrase;

    private float additionalKerning;

    private float xNoizeRange;
    private float yNoizeRange;

    private float xScaleFrom = 1;
    private float xScaleTo = 1;

    private float yScaleFrom = 1;
    private float yScaleTo = 1;

    private float rotationNoizeRange;

    private string sortingLayer = "Text";

    Object fDescriptor;
    Object fTexture;

    GameObject phraseObj;

    [MenuItem("Window/FPhrase Cretor/Create phrase")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        FFontCreator window = (FFontCreator)EditorWindow.GetWindow(typeof(FFontCreator));
        window.Show();
    }

    private void OnGUI()
    {
        //int controlID = GUIUtility.GetControlID(FocusType.);

        phrase = EditorGUILayout.TextField("Phrase", phrase);
        fDescriptor = EditorGUILayout.ObjectField("Font Descriptor (fnt)", fDescriptor, typeof(TextAsset), false);
        fTexture = EditorGUILayout.ObjectField("Font Atlas", fTexture, typeof(Texture2D), false);
        sortingLayer = EditorGUILayout.TextField("Sorting layer", sortingLayer);

        EditorGUILayout.LabelField("ADDITIONAL KERNING");
        additionalKerning = EditorGUILayout.FloatField("additional kerning:", additionalKerning);

        EditorGUILayout.LabelField("RANDOMIZE POSITION");
        yNoizeRange = EditorGUILayout.FloatField("y range:", yNoizeRange);
        xNoizeRange = EditorGUILayout.FloatField("x range:", xNoizeRange);

        EditorGUILayout.LabelField("RANDOMIZE SCALE");
        xScaleFrom = EditorGUILayout.FloatField("xScale from:", xScaleFrom);
        xScaleTo = EditorGUILayout.FloatField("xScale to:", xScaleTo);
        yScaleFrom = EditorGUILayout.FloatField("yScale from:", yScaleFrom);
        yScaleTo = EditorGUILayout.FloatField("yScale to:", yScaleTo);


        EditorGUILayout.LabelField("RANDOMIZE ROTATION");
        rotationNoizeRange = EditorGUILayout.FloatField("Range:", rotationNoizeRange);

        if (yNoizeRange < 0) yNoizeRange = 0;
        if (xNoizeRange < 0) xNoizeRange = 0;
        if (rotationNoizeRange < 0) rotationNoizeRange = 0;
        if (rotationNoizeRange > 180) rotationNoizeRange = 180;
        if (yScaleFrom > yScaleTo) yScaleTo = yScaleFrom + .1f;
        if (xScaleFrom > xScaleTo) xScaleTo = xScaleFrom + .1f;
        if (Mathf.Abs(additionalKerning) > 1) additionalKerning /= Mathf.Abs(additionalKerning);


        if (GUILayout.Button(phraseObj == null ? "Create phrase" : "Rearrange"))
        {
            DestroyImmediate(phraseObj);
            OnClickMakePhrase();
            GUIUtility.ExitGUI();
        }

        if (GUILayout.Button("Reset"))
        {
            additionalKerning = 0;

            xNoizeRange = 0;
            yNoizeRange = 0;

            xScaleFrom = 1;
            xScaleTo = 1;

            yScaleFrom = 1;
            yScaleTo = 1;

            rotationNoizeRange = 0;

            DestroyImmediate(phraseObj);
            OnClickMakePhrase();
        }

        if (GUILayout.Button("Fix it!"))
        {
            phraseObj = null;
            GUIUtility.ExitGUI();
        }
    }

    private void OnClickMakePhrase()
    {

        if (string.IsNullOrEmpty(phrase))
        {
            EditorUtility.DisplayDialog("Phrase can not be empty", "Please specify a valid phrase.", "Close");
            return;
        }

        Dictionary<string, FGlyph> fFont = FFontImporter.CreateFFont(fDescriptor as TextAsset, fTexture as Texture2D, sortingLayer);

        phraseObj = FFontImporter.CreatePhrase(phrase, fFont);

        if (Mathf.Abs(additionalKerning) > 0)
        {
            ArrangeChildren.ExtendXGapRelatively(phraseObj.transform, additionalKerning);
        }

        if (yNoizeRange > 0)
        {
            ArrangeChildren.RandomizeYPosition(phraseObj.transform, yNoizeRange);
        }

        if (xNoizeRange > 0)
        {
            ArrangeChildren.RandomizeXPosition(phraseObj.transform, xNoizeRange);
        }

        if (rotationNoizeRange > 0)
        {
            ArrangeChildren.RandomizeRotation(phraseObj.transform, rotationNoizeRange);
        }

        if (yScaleTo > yScaleFrom)
        {
            ArrangeChildren.RandomizeYScale(phraseObj.transform, yScaleFrom, yScaleTo);
        }

        if (xScaleTo > xScaleFrom)
        {
            ArrangeChildren.RandomizeXScale(phraseObj.transform, xScaleFrom, xScaleTo);
        }

        foreach (FGlyph g in fFont.Values)
        {
            DestroyImmediate(g.GlyphObj);
        }

        fFont.Clear();
    }

}

#endif