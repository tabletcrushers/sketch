﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;

class ObjectTableMaker : EditorWindow
{

    private GameObject instance;
    private int rows;
    private int columns;
    private float xOffset;
    private float yOffset;
    private float margin;


    GameObject parentContainer;

    [MenuItem("Window/GameObject management/Make objects table")]
    static void Init()
    {
        ObjectTableMaker window = (ObjectTableMaker)EditorWindow.GetWindow(typeof(ObjectTableMaker));
        window.Show();
    }

    private void OnGUI()
    {
        EditorGUILayout.LabelField("CREATE TABLE");

        instance = (GameObject)EditorGUILayout.ObjectField("Instance", instance, typeof(GameObject), false);
        columns = EditorGUILayout.IntField("columns:", columns);
        rows = EditorGUILayout.IntField("rows:", rows);
        xOffset = EditorGUILayout.FloatField("x offset:", xOffset);
        yOffset = EditorGUILayout.FloatField("y offset:", yOffset);
        margin = EditorGUILayout.FloatField("margin:", margin);

        if (GUILayout.Button("Put offsets from instance"))
        {
            Vector3 bs = ArrangeChildren.CalculateLocalBounds(instance);
            xOffset = bs.x;
            yOffset = bs.y;
            GUIUtility.ExitGUI();
        }

        if (GUILayout.Button("Create table!"))
        {
            if (parentContainer != null) DestroyImmediate(parentContainer);
            parentContainer = ArrangeChildren.MakeTable(instance, rows, columns, xOffset, yOffset, margin);
            parentContainer.transform.position = new Vector3(SceneView.lastActiveSceneView.pivot.x, SceneView.lastActiveSceneView.pivot.y);
            GUIUtility.ExitGUI();
        }

        if (GUILayout.Button("Fix it!"))
        {
            parentContainer = null;
            GUIUtility.ExitGUI();
        }
    }

}

#endif