﻿#if UNITY_EDITOR
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using UnityEngine.Windows;
using System.IO;

public class FFGlyphInfo
{
    public float Xadvance { get; set; }
    public float Xoffset { get; set; }
    public float Yoffset { get; set; }
}

public class FGlyph
{
    public GameObject GlyphObj { get; set; }
    public float Xadvance { get; set; }
    public float Xoffset { get; set; }
    public float Yoffset { get; set; }
}

class FGlyphsCollection
{
    internal Dictionary<string, FGlyph> collection;
}




public static class FFontImporter
{
    public static Dictionary<string, FGlyph> CreateFFont(TextAsset fntDescriptor, Texture2D atlas, string sortingLayer = "")
    {
        Dictionary<string, FGlyph> fFont = new Dictionary<string, FGlyph>();

        AntFont fontData = new AntFont(fntDescriptor.text);

        string fontFolder = "Assets/FFontsAssets/" + fntDescriptor.name;

        Debug.Log(AssetDatabase.IsValidFolder(fontFolder));

        if (!AssetDatabase.IsValidFolder("Assets/FFontsAssets"))
        {
            string ff = AssetDatabase.CreateFolder("Assets", "FFontsAssets");
            AssetDatabase.CreateFolder(AssetDatabase.GUIDToAssetPath(ff), fntDescriptor.name);

            Debug.Log(AssetDatabase.GUIDToAssetPath(ff));
        }

        if (!AssetDatabase.IsValidFolder(fontFolder))
        {
            AssetDatabase.CreateFolder("Assets/FFontsAssets", fntDescriptor.name);

            Debug.Log(AssetDatabase.IsValidFolder(fontFolder));
        }


        foreach (AntFontItem c in fontData.GetAllBy("char"))
        {
            float dx = c["xoffset"].AsFloat / 100 + c["width"].AsFloat / 200;
            float dy = -c["yoffset"].AsFloat / 100 - c["height"].AsFloat / 200;
            float xadv = c["xadvance"].AsFloat / 100;
            string id = c["id"].AsString;

            GameObject g = new GameObject(id, typeof(SpriteRenderer));
            Sprite s = Sprite.Create(atlas, new Rect(c["x"].AsFloat, atlas.height - c["height"].AsFloat - c["y"].AsFloat, c["width"].AsFloat, c["height"].AsFloat), new Vector2(.5f, .5f));

            Sprite temp = AssetDatabase.LoadAssetAtPath<Sprite>(fontFolder + "/" + id + ".asset");

            if (temp == null)
            {
                AssetDatabase.CreateAsset(s, fontFolder + "/" + id + ".asset");
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
                s = AssetDatabase.LoadAssetAtPath<Sprite>(fontFolder + "/" + id + ".asset");
            }
            else s = temp;

            SpriteRenderer cc = g.GetComponent<SpriteRenderer>();
            cc.sprite = s;

            cc.sortingLayerName = string.IsNullOrEmpty(sortingLayer) ? "Default" : sortingLayer;



            FGlyph glyph = new FGlyph
            {
                Xadvance = xadv,
                Xoffset = dx,
                Yoffset = dy,
                GlyphObj = g
            };

            fFont.Add(id, glyph);
        }

        return fFont;
    }

    public static GameObject CreatePhrase(string phrase, Dictionary<string, FGlyph> fFont)
    {
        float xPos = SceneView.lastActiveSceneView.pivot.x;
        float yPos = SceneView.lastActiveSceneView.pivot.y;
        float zPos = SceneView.lastActiveSceneView.pivot.z;

        GameObject phraseGameObj = new GameObject(phrase);
        phraseGameObj.transform.position = new Vector3(xPos, yPos, zPos);

        float nextGlyphPosition = 0; 
        foreach (char c in phrase)
        {
            fFont.TryGetValue((int)c + "", out FGlyph glyph);

            GameObject g = GameObject.Instantiate(glyph.GlyphObj, new Vector3(), Quaternion.identity);

            g.transform.parent = phraseGameObj.transform;
            g.transform.localPosition = new Vector3(0, 0, 0);
            g.transform.Translate(new Vector3(nextGlyphPosition + glyph.Xoffset, glyph.Yoffset, 0));

            nextGlyphPosition += glyph.Xadvance;
        }



        return phraseGameObj;
    }

    private static Texture2D GetTexture(string aFileName)
    {
        Texture2D texture = AssetDatabase.LoadAssetAtPath(aFileName, typeof(Texture2D)) as Texture2D;
        if (texture == null)
        {
            throw new UnityException("FFontImporter: Bitmap \"" + aFileName + "\" not found.");
        }

        return texture;
    }

    #region Helpers for parsing plain text format
    public class AntFont
    {
        public List<AntFontItem> _items;

        public AntFont(string aData)
        {
            _items = new List<AntFontItem>();
            AntFontItem item;
            string[] lines = Regex.Split(aData, "\n|\r|\r\n");
            if (lines.Length > 0)
            {
                string[] header = Regex.Split(lines[0], " ");
                if (header.Length > 0 && header[0] == "info")
                {
                    for (int i = 0; i < lines.Length; i++)
                    {
                        item = new AntFontItem(lines[i]);
                        _items.Add(item);
                    }
                }
                else
                {
                    throw new UnityException("FFontImporter: fDescriptor file is not FNT data.");
                }
            }
            else
            {
                throw new UnityException("FFontImporter: fDescriptor file is empty.");
            }
        }

        public bool Contains(string aKey)
        {
            return (Get(aKey) != null);
        }

        public List<AntFontItem> GetAllBy(string aKey)
        {
            var result = new List<AntFontItem>();
            for (int i = 0; i < _items.Count; i++)
            {
                if (_items[i].Key == aKey)
                {
                    result.Add(_items[i]);
                }
            }

            return result;
        }

        public AntFontItem Get(string aKey)
        {
            for (int i = 0; i < _items.Count; i++)
            {
                if (_items[i].Key == aKey)
                {
                    return _items[i];
                }
            }

            return null;
        }

        public AntFontItem this[string aKey]
        {
            get { return Get(aKey); }
        }
    }

    public class AntFontItem
    {
        public string Key { get; private set; }
        private Dictionary<string, AntFontData> _values;

        public AntFontItem(string aData)
        {
            Key = null;
            _values = new Dictionary<string, AntFontData>();
            string token = "";
            bool quoteMode = false;
            for (int i = 0; i < aData.Length; i++)
            {
                switch (aData[i])
                {
                    case ' ' :
                        if (quoteMode)
                        {
                            token += aData[i];
                            break;
                        }

                        if (Key == null)
                        {
                            Key = token;
                            token = "";
                        }
                        else
                        {
                            AddData(token);
                            token = "";
                        }
                    break;

                    case '"' :
                        token += aData[i];
                        quoteMode = !quoteMode;
                    break;

                    default :
                        token += aData[i];
                    break;
                }
            }

            if (token != "")
            {
                AddData(token);
            }
        }

        private void AddData(string aData)
        {
            var data = new AntFontData(aData);
            if (data.Key != null)
            {
                _values.Add (data.Key, data);
            }
        }

        public AntFontData this[string aKey]
        {
            get
            {
                if (!_values.ContainsKey(aKey))
                {
                    Debug.LogWarning("FFontImporter: Property with key \"" + aKey + "\" not found!");
                    return new AntFontData(aKey + "=");
                }

                return _values[aKey];
            }
        }
    }

    public class AntFontData
    {
        public string Key { get; private set; }
        public string Value { get; private set; }

        public AntFontData(string aData)
        {
            string token = "";
            bool quoteMode = false;
            for (int i = 0; i < aData.Length; i++)
            {
                switch (aData[i])
                {
                    case '=' :
                        if (quoteMode)
                        {
                            token += aData[i];
                            break;
                        }

                        Key = token;
                        token = "";
                    break;

                    case '/' :
                    case '\\' :
                        if (quoteMode)
                        {
                            token = "";
                        }
                    break;

                    case '"' :
                        quoteMode = !quoteMode;
                    break;

                    default:
                        token += aData[i];
                    break;
                }
            }

            Value = token;
        }

        public int AsInt
        {
            get
            {
                int v = 0;
                return (int.TryParse(Value, out v)) ? v : 0;
            }
        }

        public float AsFloat
        {
            get
            {
                float v = 0.0f;
                return (float.TryParse(Value, out v)) ? v : 0.0f;
            }
        }

        public string AsString
        {
            get
            {
                return Value;
            }
        }
    }
    #endregion Helpers for parsing plain text format
}
#endif