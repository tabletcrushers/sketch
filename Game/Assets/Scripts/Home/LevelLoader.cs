﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    public FadeEffect faderIn;

    public void Awake()
    {
        Statics.currentLevel = PlayerPrefs.GetInt("Level");
    }

    public void LoadLevel()
    {
        faderIn.gameObject.SetActive(true);
        faderIn.onEndCallback = () => { SceneManager.LoadScene("Between"); };
    }
}
