﻿using UnityEngine;

public class Restart : MonoBehaviour
{
    private StateMachine gameStateMachine;
    public PauseEffects pause;

    private void Start()
    {
        gameStateMachine = GameObject.Find("Manager").GetComponent<StateMachine>();
    }

    public void OnMouseDown()
    {
        pause.Hide();
        gameStateMachine.SwitchState("GameStateFadeLevel");
    }
}
