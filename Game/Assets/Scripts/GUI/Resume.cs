﻿using UnityEngine;

public class Resume : MonoBehaviour
{
    public StateMachine gameStateMachine;
    public PauseEffects pauseEffects;
    public GameObject pauseButton;
    public Animator heroA;

    void OnMouseDown()
    {
        //Time.timeScale = 1;
        heroA.enabled = true;
        Physics2D.autoSimulation = true;
        pauseEffects.Hide();
        pauseButton.SetActive(true);
    }
}
