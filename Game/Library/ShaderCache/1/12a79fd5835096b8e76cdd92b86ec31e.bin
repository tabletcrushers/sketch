<Q                             ���$      0                       xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct FGlobals_Type
{
    float4 _Time;
    float4 _ScreenParams;
    float4 _Sun;
    float4 _CloudColor;
    float4 _SkyTop;
    float4 _SkyBottom;
    float _SunSize;
    float _BottomSunLevel;
    float _TopDarkLevel;
    float _CloudDensity;
    float _CloudSharpness;
    float _StarDensity;
    float _StarStrength;
};

struct Mtl_FragmentIn
{
    float2 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float4 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    float4 TEXCOORD2 [[ user(TEXCOORD2) ]] ;
};

struct Mtl_FragmentOut
{
    float4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

fragment Mtl_FragmentOut xlatMtlMain(
    constant FGlobals_Type& FGlobals [[ buffer(0) ]],
    sampler sampler_Noise0Tex [[ sampler (0) ]],
    sampler sampler_Noise1Tex [[ sampler (1) ]],
    sampler sampler_Noise2Tex [[ sampler (2) ]],
    sampler sampler_Noise3Tex [[ sampler (3) ]],
    texture2d<float, access::sample > _Noise0Tex [[ texture(0) ]] ,
    texture2d<float, access::sample > _Noise1Tex [[ texture(1) ]] ,
    texture2d<float, access::sample > _Noise2Tex [[ texture(2) ]] ,
    texture2d<float, access::sample > _Noise3Tex [[ texture(3) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float4 u_xlat0;
    half4 u_xlat16_0;
    half4 u_xlat10_0;
    float4 u_xlat1;
    half4 u_xlat10_1;
    bool u_xlatb1;
    float3 u_xlat2;
    float3 u_xlat3;
    float u_xlat4;
    float u_xlat7;
    float u_xlat9;
    float u_xlat10;
    u_xlat10_0 = half4(_Noise1Tex.sample(sampler_Noise1Tex, input.TEXCOORD1.zw));
    u_xlat16_0 = half4(float4(u_xlat10_0) * float4(0.25, 0.25, 0.25, 0.25));
    u_xlat10_1 = half4(_Noise0Tex.sample(sampler_Noise0Tex, input.TEXCOORD1.xy));
    u_xlat16_0 = half4(fma(float4(u_xlat10_1), float4(0.5, 0.5, 0.5, 0.5), float4(u_xlat16_0)));
    u_xlat10_1 = half4(_Noise2Tex.sample(sampler_Noise2Tex, input.TEXCOORD2.xy));
    u_xlat16_0 = half4(fma(float4(u_xlat10_1), float4(0.125, 0.125, 0.125, 0.125), float4(u_xlat16_0)));
    u_xlat10_1 = half4(_Noise3Tex.sample(sampler_Noise3Tex, input.TEXCOORD2.zw));
    u_xlat16_0 = half4(fma(float4(u_xlat10_1), float4(0.0625, 0.0625, 0.0625, 0.0625), float4(u_xlat16_0)));
    u_xlat0 = max(float4(u_xlat16_0), float4(FGlobals._CloudDensity));
    u_xlat0 = min(u_xlat0, float4(FGlobals._CloudSharpness));
    u_xlat0 = u_xlat0 + (-float4(FGlobals._CloudDensity));
    u_xlat1.x = (-FGlobals._CloudDensity) + FGlobals._CloudSharpness;
    u_xlat0 = u_xlat0 / u_xlat1.xxxx;
    u_xlat0 = u_xlat0 + float4(-0.0, -0.200000003, -0.400000006, -0.600000024);
    u_xlat0 = max(u_xlat0, float4(0.0, 0.0, 0.0, 0.0));
    u_xlat0.x = dot(u_xlat0, float4(0.25, 0.25, 0.25, 0.25));
    u_xlat3.x = (-u_xlat0.x) + 1.0;
    u_xlat3.x = u_xlat3.x * 0.800000012;
    u_xlat3.xyz = fma(u_xlat0.xxx, FGlobals._CloudColor.xyz, u_xlat3.xxx);
    u_xlat1.xy = input.TEXCOORD0.xy + (-FGlobals._Sun.xy);
    u_xlat1.x = dot(u_xlat1.xy, u_xlat1.xy);
    u_xlat1.x = sqrt(u_xlat1.x);
    u_xlat4 = fma(FGlobals._SunSize, FGlobals._Sun.y, input.TEXCOORD0.y);
    u_xlat4 = u_xlat4 + 1.0;
    u_xlat1.x = fma((-u_xlat4), u_xlat1.x, 1.0);
    u_xlat1.x = max(u_xlat1.x, 0.0);
    u_xlat4 = (-input.TEXCOORD0.y) + 1.0;
    u_xlat4 = log2(u_xlat4);
    u_xlat4 = u_xlat4 * 12.0;
    u_xlat4 = exp2(u_xlat4);
    u_xlat4 = u_xlat4 * FGlobals._BottomSunLevel;
    u_xlat7 = (-FGlobals._Sun.y) + 1.60000002;
    u_xlat1.x = fma(u_xlat4, u_xlat7, u_xlat1.x);
    u_xlat4 = log2(u_xlat1.x);
    u_xlat4 = u_xlat4 * 5.19999981;
    u_xlat4 = exp2(u_xlat4);
    u_xlat4 = u_xlat4 * FGlobals._Sun.y;
    u_xlat7 = fma(FGlobals._Sun.y, 15.0, 5.0);
    u_xlat4 = u_xlat7 * u_xlat4;
    u_xlat7 = log2(FGlobals._Sun.y);
    u_xlat7 = u_xlat7 * 0.400000006;
    u_xlat7 = exp2(u_xlat7);
    u_xlat7 = u_xlat7 + 0.5;
    u_xlat10 = (-input.TEXCOORD0.y) + FGlobals._TopDarkLevel;
    u_xlat4 = fma(u_xlat7, u_xlat10, u_xlat4);
    u_xlat2.xyz = (-FGlobals._SkyTop.xyz) + FGlobals._SkyBottom.xyz;
    u_xlat1.xzw = fma(u_xlat1.xxx, u_xlat2.xyz, FGlobals._SkyTop.xyz);
    u_xlat3.xyz = fma((-u_xlat1.xzw), float3(u_xlat4), u_xlat3.xyz);
    u_xlat1.xyz = float3(u_xlat4) * u_xlat1.xzw;
    u_xlat0.xyz = fma(u_xlat0.xxx, u_xlat3.xyz, u_xlat1.xyz);
    u_xlat1.xy = input.TEXCOORD0.xy * FGlobals._ScreenParams.xy;
    u_xlat9 = dot(u_xlat1.xy, float2(12.9898005, 78.2330017));
    u_xlat9 = sin(u_xlat9);
    u_xlat9 = u_xlat9 * 43758.5469;
    u_xlat9 = fract(u_xlat9);
    u_xlat1.x = u_xlat9 * FGlobals._Time.y;
    u_xlat4 = u_xlat9 * 720.0;
    u_xlat1.x = fma(u_xlat1.x, 5.0, u_xlat4);
    u_xlat1.x = sin(u_xlat1.x);
    u_xlat1.x = fma(u_xlat1.x, 0.850000024, 0.949999988);
    u_xlat9 = u_xlat9 * u_xlat1.x;
    u_xlat1.x = dot(input.TEXCOORD0.xy, float2(12.9898005, 78.2330017));
    u_xlat1.x = sin(u_xlat1.x);
    u_xlat1.x = u_xlat1.x * 43758.5469;
    u_xlat1.x = fract(u_xlat1.x);
    u_xlatb1 = FGlobals._StarDensity<u_xlat1.x;
    u_xlat9 = u_xlatb1 ? u_xlat9 : float(0.0);
    output.SV_Target0.xyz = fma(float3(u_xlat9), float3(FGlobals._StarStrength), u_xlat0.xyz);
    output.SV_Target0.w = 1.0;
    return output;
}
                              FGlobals|         _Time                            _ScreenParams                           _Sun                         _CloudColor                   0      _SkyTop                   @   
   _SkyBottom                    P      _SunSize                  `      _BottomSunLevel                   d      _TopDarkLevel                     h      _CloudDensity                     l      _CloudSharpness                   p      _StarDensity                  t      _StarStrength                     x          
   _Noise0Tex                 
   _Noise1Tex               
   _Noise2Tex               
   _Noise3Tex                  FGlobals           